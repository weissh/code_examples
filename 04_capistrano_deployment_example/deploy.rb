# config valid only for Capistrano 3.1
#lock '3.2.1' # discrimination !!!!

set :stages, ["clientX-uat", "clientY-production",
			  "clientX-uat", "clientX-qa", "clientX-production",
			  "clientY-uat", "clientY-qa", "clientY-production"]

set :application, 'PIM'
set :repo_url, 'git@xxxx.com:PIM/project.git'

set :branch, ENV['BRANCH'] || "master"
if ENV['branch']
    set :branch, ENV['branch']
end

namespace :deploy do

  desc "Update composer dependencies"
  task :composer_install do
    on roles(:all) do

        within release_path do
            info "Running composer"
            execute 'composer', 'install', '--no-dev', '--optimize-autoloader'
        end

    end
  end

  desc "Update bower dependencies"
  task :bower_install do
    on roles(:all) do

        info "Running bower"
        execute "cd #{release_path} && bower install --no-color"

    end
  end

  desc "Setup DB config file"
  task :local_db_config do
    on roles(:all) do

        info "Setting up local.php file"
        execute "cd #{release_path}/config/autoload/db && mv #{fetch(:stage)}.php #{release_path}/config/autoload/local.php"

        info "Setting up MediaServer.global.php file"
        execute "cd #{release_path}/config/autoload/db/media && mv #{fetch(:stage)}.php #{release_path}/config/autoload/MediaServer.global.php"

    end
  end

  desc "Change permissions on upload folder"
    task :folder_permissions do
      on roles(:all) do

          info "Setting upload folder with permissions 755 on upload folder"
          execute "mkdir #{release_path}/public/uploads"
          execute "chmod -R 755 #{release_path}/public/uploads"

    end
  end

    desc "Change doctrine cache permissions"
      task :doctrine_cache_folder do
        on roles(:all) do

            info "Changing doctrine cache folder permissions"
            execute "mkdir -p #{release_path}/data/DoctrineORMModule && mkdir -p #{release_path}/data/DoctrineORMModule/Proxy && chmod 755 #{release_path}/data/DoctrineORMModule -R"

      end
    end

  desc "Create upload symlink for files"
    task :folder_permissions do
      on roles(:all) do

          info "Creating symlink to folder in var/www"
          execute "ln -s #{release_path}/../../shared #{release_path}/public/uploads/available_files"

    end
  end

  desc "Update the crontab for this user"
      task :update_crontab do
        on roles(:'pim2') do

            info "Setting crontab"
            execute "crontab #{release_path}/config/crontab/crontab-#{fetch(:stage)}.txt"

      end
    end

   desc "Execute Doctrine Migration"
      task :doctrine_migration do
         on roles(:'pim2') do
            info "Doctrine Migration"
            execute "php #{release_path}/vendor/bin/doctrine-module migrations:migrate --no-interaction"
      end
   end

  desc "Update logs permissions"
    task :update_error_logs_permissions do
      on roles(:all) do

          within release_path do
               info "Update logs permissions"
               execute "mkdir -p /var/log/pim-logs"
          end

      end
    end

   desc "Setup Error Handler Config"
     task :error_handler_config do
       on roles(:all) do

           within release_path do
                info "Setting up error handler config"
                execute "cd #{release_path}/config/autoload && cp #{fetch(:stage)}.error_handler.local.php.dist #{release_path}/config/autoload/#{fetch(:stage)}.error_handler.local.php"
           end

       end
     end

end

before "deploy:publishing", "deploy:composer_install"
before "deploy:publishing", "deploy:bower_install"
before "deploy:publishing", "deploy:doctrine_cache_folder"
before "deploy:publishing", "deploy:local_db_config"
before "deploy:publishing", "deploy:update_crontab"
before "deploy:publishing", "deploy:folder_permissions"
before "deploy:publishing", "deploy:error_handler_config"
before "deploy:publishing", "deploy:doctrine_migration"
before "deploy:publishing", "deploy:update_error_logs_permissions"
