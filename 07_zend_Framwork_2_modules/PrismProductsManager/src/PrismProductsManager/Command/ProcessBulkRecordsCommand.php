<?php

namespace PrismProductsManager\Command;

use PrismProductsManager\Model\BulkActionsMapper;
use Zend\Mvc\Controller\AbstractConsoleController;
use Zend\Text\Table;
use Zend\View\Model\ConsoleModel;

class ProcessBulkRecordsCommand extends AbstractConsoleController
{
    /**
     * @var BulkActionsMapper
     */
    private $model;
    
    /**
     * @method __construct
     * @param  BulkActionsMapper $model
     */
    public function __construct(BulkActionsMapper $model)
    {
        $this->model = $model;
    }
    
    public function processAction()
    {
        $table = new Table\Table([
            'columnWidths' => [150]
        ]);
        $table->setDecorator('ascii');
        
        $this->model->processBulkRecords();
        $table->appendRow(['Process Bulk Records Executed.']);

        return (new ConsoleModel())->setResult($table);
    }
    
    public function processNotifySipAndSpectrumAction()
    {
        $table = new Table\Table([
            'columnWidths' => [150]
        ]);
        $table->setDecorator('ascii');
        
        $this->model->notifySipAndSpectrum();
        $table->appendRow(['Process Notify Sip and Spectrum Executed.']);

        return (new ConsoleModel())->setResult($table);
    }
}