<?php

namespace PrismProductsManager\Command;

use PrismProductsManager\Service\HistoricalService;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class AutomaticFromDateCommand
 * @package PrismProductsManager\Command
 */
class AutomaticFromDateCommand extends AbstractActionController
{
    /** @var HistoricalService $historicalService */
    private $historicalService;

    /**
     * AutomaticFromDateCommand constructor.
     * @param HistoricalService $historicalService
     */
    public function __construct(HistoricalService $historicalService)
    {
        $this->historicalService = $historicalService;
    }

    /**
     *
     */
    public function processAction()
    {
        $currentDate = new \DateTime(date('Y-m-d', time())." 00:00:00");
        $threeDaysBefore = $currentDate->modify('-3 day');
        $validFromDatesToChange = $this->historicalService->retrieveCurrentFromDate($threeDaysBefore);
        $filteredValuesToChange = $this->historicalService->filterEntitiesAllowedToChange($validFromDatesToChange);

        $changedFilterDates = $this->historicalService->changeFilterDates($filteredValuesToChange);

        if ($this->historicalService->markDatesAsProcessed($changedFilterDates)) {
            echo "Processed successfully.\n";
        } else {
            echo "Processed unsuccessfully.\n";
        }
    }

}