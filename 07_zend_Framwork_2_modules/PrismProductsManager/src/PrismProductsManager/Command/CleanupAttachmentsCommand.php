<?php

namespace PrismProductsManager\Command;

use PrismProductsManager\Service\ProductImportService;
use Zend\Code\Scanner\DirectoryScanner;
use Zend\Mvc\Controller\AbstractConsoleController;
use Zend\Text\Table;
use Zend\Validator\Date;
use Zend\View\Model\ConsoleModel;

class CleanupAttachmentsCommand extends AbstractConsoleController
{
    public function bulkAction()
    {
        $table = new Table\Table([
            'columnWidths' => [150]
        ]);
        $table->setDecorator('ascii');

        $entries = scandir(realpath("./public/uploads"));

        $found_file_removed = false;
        foreach($entries as $entry) {
            $check = strpos($entry, "_");
            if ($check !== false) {
                // check has YmdHis format before extension
                $explodeUnderscore = explode("_", $entry);
                $dateformatwithExt = $explodeUnderscore[count($explodeUnderscore) - 1];

                $temp = explode('.',$dateformatwithExt);
                array_pop($temp);

                $filenameformat = $temp;

                if (! isset($filenameformat[0])) {
                    continue;
                }

                $cutHis = substr($filenameformat[0], 0, -6);
                $year   = substr($cutHis, 0, 4);
                $month  = substr($cutHis, 4, 2);
                $day    = substr($cutHis, 6, 2);

                $date = date($year . '-' . $month . '-' . $day);

                if (! $this->isValidDate($date)) {
                    continue;
                }

                $date1 = new \DateTime($date);
                $date2 = new \DateTime(date('Y-m-d'));
                $diffFileName = $date2->diff($date1)->format("%a");

                $filectime = new \DateTime(
                    date("Y-m-d", filectime(realpath("./public/uploads") . '/' . $entry))
                );
                $diffFilecTime =  $date2->diff($filectime)->format("%a");

                if ($diffFileName >= 180 &&
                    $diffFilecTime >= 180
                ) {
                    @unlink(realpath("./public/uploads") . '/' . $entry);
                    $found_file_removed = true;
                }
            }
        }


        if ($found_file_removed) {
            $table->appendRow(['File clean-ed up']);
        } else {
            $table->appendRow(['No file found to be clean-ed up']);
        }

        return (new ConsoleModel())->setResult($table);
    }

    /**
     * Check date if is valid
     *
     * @param $date
     * @return bool
     */
    private function isValidDate($date)
    {
        $dateValidator = new Date();
        return $dateValidator->isValid($date);
    }
}