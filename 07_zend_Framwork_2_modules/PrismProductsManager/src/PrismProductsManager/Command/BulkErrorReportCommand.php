<?php

namespace PrismProductsManager\Command;

use PrismProductsManager\Entity\Repository\BulkProcessingRepository;
use PrismCommon\Service\MailSender;
use PHPExcel;
use PHPExcel_IOFactory;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractConsoleController;
use Zend\Text\Table;
use Zend\View\Model\ConsoleModel;

/**
 * Class BulkErrorReportCommand
 *
 * Controller command to report bulk error.
 */
class BulkErrorReportCommand extends AbstractConsoleController
{
    private $bulkProcessingRepository;
    private $mailSender;
    private $config;

    public function __construct(
        BulkProcessingRepository $bulkProcessingRepository,
        MailSender $mailSender,
        array $config
    ) {
        $this->bulkProcessingRepository = $bulkProcessingRepository;
        $this->mailSender               = $mailSender;
        $this->config                   = $config;
    }

    /**
     * @return ConsoleModel
     * @throws \PHPExcel_Reader_Exception
     */
    public function sendAction()
    {
        $objPHPExcel = new PHPExcel();
        $column = 'A';
        $headings= [
            'PROCESS TYPE',
            'ORIGINAL FILE NAME',
            'STYLE OR OPTION',
            'PROCESSING',
            'PROCESSED',
            'SCHEDULE',
            'PROCESSED RESPONSE',
            'PROCESS KEY',
            'PROCESS VALUE',
            'SUBMITTED BY',
            'CREATED',
            'MODIFIED',
            'CHANGED FILE NAME',
        ];

        for ($c=0;$c<count($headings);$c++) {
            $objPHPExcel->getActiveSheet()->setCellValue($column.'1', $headings[$c]); // Add column heading data
            if ($c === count($headings)-1) {
                break;
            }
            $column++;
        }

        $table = new Table\Table([
            'columnWidths' => [150]
        ]);
        $table->setDecorator('ascii');

        $errror_reports = $this->bulkProcessingRepository->getErrorReport();

        if (count($errror_reports) > 0 ) {

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(150);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);

            $j = 1;
            foreach ($errror_reports as $key => $row) {
                $j +=1;
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$j, $row['processType'], \PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$j, $row['orginalFileName'], \PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$j, $row['styleoroption'], \PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$j, $row['processing'], \PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row['processed'], \PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$j, $row['schedule']->format('d m Y H i'), \PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$j, $row['processedResponse'], \PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$j, $row['processKey'], \PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$j, $row['processValue'], \PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$j, $row['submittedBy'], \PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('K'.$j, $row['created']->format('d m Y H i'), \PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('L'.$j, $row['modified']->format('d m Y H i'), \PHPExcel_Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$j, $row['changedFileName'], \PHPExcel_Cell_DataType::TYPE_STRING);
            }

            $date = date('Y-m-d-H-i-s');

            $report =  $date . '_bulk_error_report.xlsx';
            $pathWithReport = 'data/' . $report;
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'EXCEL2007');
            $objWriter->save($pathWithReport);

            // send mail...
            $headers = [
                'subject' => $this->config['subject'],
                'from' => 'contact@whistl.co.uk',
                'to' => [$this->config['email_to_send'], 'hani.weiss@whistl.co.uk'],
            ];
            $template = 'prism-products-manager/email/bulk-error-report';
            $attachments = [
                0 => [
                    'path' => realpath('data'),
                    'name' => $report,
                ],
            ];

            $variables = [];

            try {
                $this->mailSender->sendMail($headers, $template, $variables, $attachments);
            } catch (\Exception $e) {
                // sending mail failed do not take any other action the error will be logged in error.log
            }

            // remove report file locally
            @unlink($pathWithReport);

            $table->appendRow(['Succeed! report generated to ' .$report. ' and sent to email ' . $this->config['email_to_send']]);

        } else {
            $table->appendRow(['Congrats!, no error found']);
        }

        return (new ConsoleModel())->setResult($table);
    }
}
