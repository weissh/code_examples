<?php

namespace PrismProductsManager\Command;

use PrismProductsManager\Service\ProductImportService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Console\Request as ConsoleRequest;

/**
 * Class ProductImportCommand
 * @package PrismProductsManager\Command
 */
class ProductImportCommand extends AbstractActionController
{
    /**
     * @var ProductImportService
     */
    private $productImportService;

    /**
     * @param ProductImportService $productImportService
     */
    public function __construct(ProductImportService $productImportService)
    {
        $this->productImportService = $productImportService;
    }

    /**
     *
     */
    public function productImportAction()
    {
        $request = $this->getRequest();

        // Make sure that we are running in a console and the user has not tricked our
        // application into running this action from a public web server.
        if (!$request instanceof ConsoleRequest){
            throw new \RuntimeException('You can only use this action from a console!');
        }

        $filePath = $request->getParam('filePath');
        $this->productImportService->verbose(true);

        echo "Starting validation \n";
        $success = $this->productImportService->validateExcelFile($filePath);

        if ($success === false) {
            echo "\033[0;31m [!] Validation failed! Stopping import... \033[0m"."\n";
            die;
        } else if (is_numeric($success) && $success > 0) {
            echo "Validation partially passed. {$success} products will be imported \n";
        } else {
            echo "Validation passed \n";
        }

        echo "Starting import \n";
        $success = $this->productImportService->import($filePath);
        if ($success !== true) {
            echo "\033[0;31m [!] Import failed!... \033[0m"."\n";
        } else {
            echo "Import was successful \n";
        }
    }

    /**
     *
     */
    public function productImportTruncateAction()
    {
        $request = $this->getRequest();

        // Make sure that we are running in a console and the user has not tricked our
        // application into running this action from a public web server.
        if (!$request instanceof ConsoleRequest){
            throw new \RuntimeException('You can only use this action from a console!');
        }

        $confirm = $request->getParam('confirm');

        if ($confirm === 'yes') {
            $result = $this->productImportService->truncateTablesForImport();

            echo $result;
        } else {
            echo 'Please confirm script to truncate tables using "confirm" as a parameter.' . "\n";
        }
    }

    public function coloursImportAction()
    {
        $request = $this->getRequest();

        // Make sure that we are running in a console and the user has not tricked our
        // application into running this action from a public web server.
        if (!$request instanceof ConsoleRequest){
            throw new \RuntimeException('You can only use this action from a console!');
        }

        $this->productImportService->importColours();
    }

    public function updateAbbrevCodeAction()
    {
        $request = $this->getRequest();

        // Make sure that we are running in a console and the user has not tricked our
        // application into running this action from a public web server.
        if (!$request instanceof ConsoleRequest){
            throw new \RuntimeException('You can only use this action from a console!');
        }

        $this->productImportService->updateAbbrevCodeValues();
    }
}