<?php

namespace PrismProductsManager\View\Helper;
use Zend\View\Helper\AbstractHelper;

/**
 *
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class OutputTableHeaderViewHelper extends AbstractHelper
{
    /**
     * @var
     */
    protected $config;

    /**
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function __invoke()
    {
        if (!empty($this->config['clientConfig']['table-header'])) {
            return $this->config['clientConfig']['table-header'];
        }
        return '<th>Product Name</th>
                           <th>Season</th>
                           <th>Category</th>
                           <th>Sub-Category</th>
                           <th>Web-Department</th>
                           <th>Web-SubDepartment</th>
                           <th>Style/Option Code</th>
                           <th>Nr Of Options</th>
                           <th>WorkFlow</th>
                           <th>Modified Date</th>
                           <th>Created Date</th>
                           <th>Actions</th>
                           <th></th>
                           <th style="display:none">Completed-Workflows</th>
                           <th style="display:none">Not-Completed-Workflows</th>';
    }
}