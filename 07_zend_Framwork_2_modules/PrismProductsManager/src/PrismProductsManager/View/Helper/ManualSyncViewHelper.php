<?php

namespace PrismProductsManager\View\Helper;
use Zend\View\Helper\AbstractHelper;

/**
 *
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class ManualSyncViewHelper extends AbstractHelper
{
    /**
     * @var
     */
    protected $config;

    /**
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * @return bool
     */
    public function __invoke()
    {
        if (!empty($this->config['clientConfig']['settings']['manualSync'])) {
            return $this->config['clientConfig']['settings']['manualSync'];
        }
        return false;
    }
}