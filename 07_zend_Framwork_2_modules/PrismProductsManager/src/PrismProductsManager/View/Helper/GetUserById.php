<?php

namespace PrismProductsManager\View\Helper;

use PrismUsersManager\Model\UsersTable;
use Zend\View\Helper\AbstractHelper;

class GetUserById extends AbstractHelper
{
    private $users;

    public function __construct(UsersTable $users)
    {
        $this->users  = $users;
    }
    
    public function __invoke($id)
    {
        $row = $this->users->getUserBy('userId', $id);
        if ($row->count() === 1) {
            return $row
                        ->current()
                        ->userName;
        }

        return '';
    }
}