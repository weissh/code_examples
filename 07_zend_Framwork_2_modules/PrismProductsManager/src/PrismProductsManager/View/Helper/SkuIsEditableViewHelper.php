<?php

namespace PrismProductsManager\View\Helper;
use PrismProductsManager\Model\ProductsMapper;
use Zend\View\Helper\AbstractHelper;

/**
 *
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class SkuIsEditableViewHelper extends AbstractHelper
{
    /**
     * @var
     */
    protected $config;

    /**
     * @param $config
     */
    public function __construct($config, ProductsMapper $productsMapper)
    {
        $this->config = $config;
        $this->productsMapper = $productsMapper;
    }

    /**
     * @return bool
     */
    public function __invoke($variantId)
    {
        if (empty($this->config['clientConfig']['features']['editableSku']) || $this->productsMapper->variantIsSavedInSpectrum($variantId)) {
            return false;
        }
        return true;
    }

}