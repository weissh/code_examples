<?php
namespace PrismProductsManager\Form;

use PrismProductsManager\Model\AttributesGroupMapper;
use PrismProductsManager\Model\CategoriesMapper;
use PrismProductsManager\Model\CommonAttributes\CommonAttributesTable;
use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class AddCategoryForm
 * @package PrismProductsManager\Form
 */
class AddMerchantCategoryForm extends Form implements InputFilterProviderInterface
{

    /**
     * @var object
     */
    protected $_categoriesMapper;
    /**
     * @var object
     */
    protected $_attributesMapper;
    /**
     *
     */
    protected $_uniqueName = false;
    /**
     *
     */
    public $parentFlag = 0;
    /**
     * @var int
     */
    public $languageId;
    /**
     * @var CommonAttributesTable
     */
    public $commonAttributesTable;
    /**
     *
     */
    public function __construct(
        CategoriesMapper $categoriesMapper,
        AttributesGroupMapper $attributesMapper ,
        $dbAdapter,
        CommonAttributesTable $commonAttributesTable)
    {
        $this->_categoriesMapper = $categoriesMapper;
        $this->_attributesMapper = $attributesMapper;
        $this->commonAttributesTable = $commonAttributesTable;
        $this->languageId = 1;
        $this->_dbAdapter = $dbAdapter;
        $this->init();
    }

    /**
     * Build add / edit category form
     */
    public function prepareElements()
    {
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'productMerchantCategoryId',
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'skuRulesNr',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'parentId',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'group-parent',
                'required' => 'required',
            ),
            'options' => array(
                'label' => "Parent *",
                'label_attributes' => array(
                    'for' => 'group-parent'
                ),
                'value_options' => $this->_categoriesMapper->fetchAllMerchantCategories('list')
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'categoryName',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'category-name',
                'placeholder' => 'Product group name',
            //'required' => 'required',
            ),
            'options' => array(
                'label' => 'Product Category Name',
                'label_attributes' => array(
                    'for' => 'category-name'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'status',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'status',
            ),
            'options' => array(
                'label' => 'Status',
                'label_attributes' => array(
                    'for' => 'status'
                ),
                'value_options' => array(
                    'ACTIVE' => 'ACTIVE',
                    'INACTIVE' => 'INACTIVE',
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'shortDescription',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'short-description',
                'rows' => 3
            ),
            'options' => array(
                'label' => 'Short Description',
                'label_attributes' => array(
                    'for' => 'short-description'
                ),
            ),
        ));

        /**
         * Get old formatted merchant attributes
         */
        $availableMerchantAttributesFromOld = $this->_attributesMapper->fetchTopMerchantGroups();
        /**
         * Get new common attributes that are available and format them
         */
        $availableMerchantAttributesFromNew = $this->commonAttributesTable->getCommonAttributesDetailsBy(
            array('usedForMerchantCategories = 1')
        );


        $availableMerchantAttributesFromNew = $this->commonAttributesTable->listResultsWithKeyAs(
            $availableMerchantAttributesFromNew,
            '_common'
        );

        /**
         * Merge the old merchant attributes and the new ones and keep the keys
         */
        $availableMerchantAttributes = $availableMerchantAttributesFromOld + $availableMerchantAttributesFromNew;

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'skuRules',
            'attributes' => array(
                'multiple' => 'multiple',
                'id' => 'sku-rules',
                'class' => 'form-control'
            ),
            'options' => array(
                'label' => 'Sku Building Rule',
                'label_attributes' => array(
                    'for' => 'sku-rules'
                ),
                'value_options' => $availableMerchantAttributes
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'fullDescription',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'full-description',
                'rows' => 3
            ),
            'options' => array(
                'label' => 'Full Description',
                'label_attributes' => array(
                    'for' => 'full-description'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));

        $this->add(array(
            'name' => 'submitAddMerchantCategoryForm',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'class' => 'btn btn-primary pull-right',
            ),
        ));

        $this->add(array(
            'name' => 'cancelAddMerchantCategoryForm',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Cancel',
                'data-dismiss' => 'modal',
                'class' => 'btn btn-default pull-right',
            ),
        ));
    }

    /**
     *
     */
    public function init()
    {
        parent::__construct('add-merchant-category');
        $this->setAttributes(array(
            'name' => 'newMerchantCategoryForm',
            'role' => 'form',
            'action' => '/merchant-categories',
        ));
        $this->prepareElements();
        //already used in __construct because eventManager usage
    }

    /**
     * Validate the form input
     * @see \Zend\InputFilter\InputFilterProviderInterface::getInputFilterSpecification()
     */
    public function getInputFilterSpecification()
    {
        $result = array(
            array(
                'name' => 'parentId',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities')
                ),
            ),

            array(
                'name' => 'status',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
            ),
            array(
                'name' => 'shortDescription',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 30,
                        )
                    ),
                )
            ),
            array(
                'name' => 'fullDescription',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 500,
                        )
                    ),
                )
            ),
            array(
                'name' => 'skuRules',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
            ),


            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );

        if ($this->parentFlag == 0) {
            $result[] = array(
                'name' => 'skuRulesNr',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Between',
                        'options' => array(
                            'min' => 2,
                            'max' => 2,
                            'messages' => array('notBetween' => 'You must select 2 options from sku rules.')
                        ),
                    ),
                )
            );
        } else {
            $result[] = array(
                'name' => 'skuRulesNr',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Between',
                        'options' => array(
                            'min' => 0,
                            'max' => 2,
                            'messages' => array('notBetween' => 'You must select max  2 options from sku rules.')
                        )
                    ),
                )
            );
        }

        if ($this->_uniqueName) {
            $result[] = array(
                'name' => 'categoryName',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 30,
                        )
                    ),
                    array(
                        'name'    => 'Zend\Validator\Db\NoRecordExists',
                        'options' => array(
                            'table' => 'PRODUCTS_Merchant_Categories_Definitions',
                            'field' => 'categoryName',
                            'adapter' => $this->_dbAdapter,
                            'messages' => array(
                                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'The Category Name is already is used.'
                            ),
                        ),
                    ),
                )
            );
        } else {
            $result[] = array(
                'name' => 'categoryName',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 30,
                        )
                    ),
                )
            );

        }
        return $result;
    }
    /**
     * Set unique name
     * Note : Used to validate the category Name for the edit form
     */
    public function setUniqueName($uniqueName)
    {
        $this->_uniqueName = $uniqueName;
    }
}
