<?php

namespace PrismProductsManager\Form;

use phpDocumentor\Reflection\Type;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Zend\Form\Element\Submit;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

class SearchBulkProcessingForm extends Form implements InputFilterProviderInterface
{
    /**
     * SearchBulkProcessingForm constructor.
     */
    public function __construct()
    {
        parent::__construct('search-bulk-processing-form');
        $this->setAttribute('method', 'GET');
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->add([
            'type' => Text::class,
            'name' => 'keyword',
            'attributes' => [
                'placeholder' => 'Keyword',
                'class' => 'form-control pull-left dash-search',
            ],
        ]);

        $this->add([
            'type' => Select::class,
            'name' => 'process_type',
            'attributes' => [
                'class' => 'form-control pull-left dash-search',
            ],
            'options' => [
                'value_options' => [
                    0 => 'ALL TYPES',
                    'COMMON_CASCADE' => 'COMMON_CASCADE',
                    'PRICE_CASCADE' => 'PRICE_CASCADE',
                    'COMMON_ALL' => 'COMMON_ALL',
                    'PRICE_ALL' => 'PRICE_ALL'
                ],
            ],
        ]);
        
        $this->add([
            'type' => Select::class,
            'name' => 'status',
            'attributes' => [
                'class' => 'form-control pull-left dash-search',
            ],
            'options' => [
                'value_options' => [
                    0 => 'ALL STATUS',
                    'PROCESSED' => 'PROCESSED',
                    'PROCESSED_WITH_ERROR' => 'PROCESSED WITH ERROR',
                    'IN_PROCESS' => 'IN PROCESS',
                    'NOT_PROCESSED' => 'NOT PROCESSED',
                ],
            ],
        ]);

        $this->add([
            'type' => Hidden::class,
            'name' => 'new-search',
            'attributes' => array(
                'value' => '1'
            )
        ]);


        $this->add([
            'type' => Submit::class,
            'name' => 'search-button',
            'attributes' => [
                'value' => 'Search',
                'class' => 'form-control pull-left dash-search btn btn-default',
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getInputFilterSpecification()
    {
        return [
            [
                'name'     => 'keyword',
                'required' => false,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [],            
            ],
            [
                'name'     => 'process_type',
                'required' => false,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [],            
            ],
            [
                'name'     => 'status',
                'required' => false,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [],            
            ],
        ];
    }
}