<?php

namespace PrismProductsManager\Form;

use Zend\Form\ElementInterface;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class BulkDownloadForm
 * @package PrismProductsManager\Form
 * @author Vilius Vaitiekunas vilius.vaitiekunas@whistl.co.uk
 */
class BulkDownloadForm extends Form implements  InputFilterProviderInterface
{
    /**
     * @var EventManagerInterface
     */
    protected $events;

    /**
     * @var
     */
    public $formData;

    /**
     *
     */
    public function __construct()

    {

        parent::__construct('bulk-download');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'downloadType',
            'options' => array(
                'value_options' => array (
                    '' => '',
                )
            ),
            'attributes' => array (
                'class' => 'selectize selectized',
                'tabindex' => '-1',
                'style' => 'display:none'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'currencyType',
            'options' => array(
                'value_options' => array (
                    '' => '',
                )
            ),
            'attributes' => array (
                'class' => 'selectize selectized',
                'tabindex' => '-1',
                'style' => 'display:none'
            )
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));

        $this->add (array(
            'name' => 'submit',
            'type' => 'button',
            'attributes' => array (
                'class' => 'btn btn-primary download-template'
            )
        ));

    }


    /**
     *
     */
    public function init()
    {
        //already used in __construct because eventManager usage
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $result  =  array(
            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );
        return $result;
    }

    /**
     * Set a single option for an element
     *
     * @param  string $key
     * @param  mixed $value
     * @return self
     *  @codeCoverageIgnoreStart no need to test empty method
     */
    public function setOption($key, $value)
    {

    }
    // @codeCoverageIgnoreEnd
}
