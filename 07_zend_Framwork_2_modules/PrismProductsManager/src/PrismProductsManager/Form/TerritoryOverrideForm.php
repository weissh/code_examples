<?php

namespace PrismProductsManager\Form;

use PrismProductsManager\Service\CommonAttributesService;
use Zend\Form\Form;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;


/**
 * Class TerritoryOverrideForm
 * @package PrismProductsManager\Form
 */
class TerritoryOverrideForm extends Form implements InputFilterProviderInterface
{
    protected $commonAttributesService;
    protected $territories;
    public function __construct(CommonAttributesService $commonAttributesService)
    {
        parent::__construct('territory-overrides-form');

        $this->commonAttributesService = $commonAttributesService;
        $this->territories = $this->commonAttributesService->getAllTerritories(true);

        $this->createFormElements();
    }
    private function createFormElements()
    {
        $this->add(array(
            'name' => 'commonAttributeValueId',
            'type' => 'hidden'
        ));
        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'id' => 'submit-territory-override',
                'type' => 'submit',
                'value' => 'Save',
                'class' => 'btn btn-success pull-right',
            ),
        ));

        /** Build territories inputs */
        foreach ($this->territories as $territory) {
            /** Create value sku code for territory */
            $this->add(array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'skuCode['.$territory['territoryId'].']',
                'attributes' => array(
                    'class' => 'form-control' ,
                    'id' => 'skuCode['.$territory['territoryId'].']',
                    'placeholder' => 'Sku Code Value',
                ),
                'options' => array(
                    'label' => 'Sku Value Code (' . $territory['iso2Code'] . ')',
                    'label_attributes' => array(
                        'for' => 'skuCode['.$territory['territoryId'].']'
                    ),
                ),
            ));
            /** Create value for territory */
            $this->add(array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'value['.$territory['territoryId'].']',
                'attributes' => array(
                    'class' => 'form-control' ,
                    'id' => 'value['.$territory['territoryId'].']',
                    'placeholder' => 'Value',
                ),
                'options' => array(
                    'label' => 'Value (' . $territory['iso2Code'] . ')',
                    'label_attributes' => array(
                        'for' => 'value['.$territory['territoryId'].']'
                    ),
                ),
            ));
        }
    }

    public function init()
    {

    }
    public function getInputFilterSpecification()
    {
        return array();
    }
}
