<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class AddAttributesMarketingGroupForm
 * @package PrismProductsManager\Form
 */
class AddAttributesMarketingGroupForm extends Form implements  InputFilterProviderInterface
{
    /**
     * @var EventManagerInterface
     */
    protected $events;

    /**
     * @var
     */
    public $formData;

    /**
     *
     */
    public function __construct()
    {
        parent::__construct('attributes-grop-add');

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'attributeId',
        ));
        $this->add (array(
            'name' => 'attributeInternalName',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Attribute Internal Name'
            ),
        ));

        $this->add (array(
            'name' => 'attributePublicName',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Attribute Public Name'
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'attributeType',
            'options' => array(
                'value_options' => array (
                    'TEXT' => 'Text ( Single Line )',
                    'CHECKBOX' => 'Checkbox',
                    'RADIO' => 'Radio Button',
                    'SELECT' => 'Dropdown Select'
                )
            ),
            'attributes' => array (
                'class' => 'selectize selectized',
                'tabindex' => '-1',
                'style' => 'display:none'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'validationType',
            'options' => array(
                'value_options' => array (
                    'STRING' => 'Alfanumeric Characters',
                    'INT' => 'Numeric Only',
                    'FLOAT' => 'Decimal values'
                )
            ),
            'attributes' => array (
                'class' => 'selectize selectized',
                'tabindex' => '-1',
                'style' => 'display:none'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'isRequired',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'attributeGroupParent',
            'options' => array (
                'disable_inarray_validator' => true,
            ),
            'attributes' => array (
                'class' => 'selectize selectized',
                'tabindex' => '-1',
                'style' => 'display:none'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'attributeValues',
            'attributes' => array (
                'id' => 'attribute-options',
                'rows' => '7',
                'class' => 'full-width'
            )
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));

        $this->add (array(
            'name' => 'submit',
            'type' => 'button',
            'attributes' => array (
                'class' => 'btn btn-primary save-attribute'
            )
        ));
    }


    /**
     *
     */
    public function init()
    {
        //already used in __construct because eventManager usage
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $result  =  array(
                        array(
                            'name' => 'attributeInternalName',
                            'required' => true,
                            'filters' => array(
                                array ('name' => 'StripTags'),
                                array ('name' => 'StringTrim'),
                                array ('name' => 'HtmlEntities')
                            ),
                            'validators' => array(
                                array (
                                    'name' => 'StringLength',
                                    'options' => array(
                                        'encoding' => 'UTF-8',
                                        'min' => 3,
                                        'max' => 60,
                                    ),
                                ),
                            ),
                        ),
                        array(
                            'name' => 'attributeGroupParent',
                            'required' => true,
                            'filters' => array(
                                array ('name' => 'Int')
                            ),
                            'validators' => array(
                                array(
                                    'name' => 'GreaterThan',
                                    'options' => array(
                                        'min' => 0,
                                        'inclusive' => true
                                    ),
                                ),
                            ),
                        ),
                        array(
                            'name' => 'attributePublicName',
                            'required' => true,
                            'filters' => array(
                                array ('name' => 'StripTags'),
                                array ('name' => 'StringTrim'),
                                array ('name' => 'HtmlEntities')
                            ),
                            'validators' => array(
                                array (
                                    'name' => 'StringLength',
                                    'options' => array(
                                        'encoding' => 'UTF-8',
                                        'min' => 3,
                                        'max' => 60,
                                    ),
                                ),
                            ),
                        ),
                        array(
                            'name'     => 'csrf',
                            'required' => true,
                            'validators' => array(
                                array(
                                    'name' => 'csrf',
                                    'options' => array(
                                        'messages' => array(
                                            \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    );

        if ( isset($this->formData) && ($this->formData['attributeType'] != 'TEXT' && $this->formData['attributeType'] != 'CHECKBOX') &&  $this->formData['attributeId'] == '') {

            $result[] = array(
                            'name' => 'attributeValues',
                             'required' => true,
                            'filters' => array(
                                array ('name' => 'StripTags'),
                                array ('name' => 'StringTrim'),
                                array ('name' => 'HtmlEntities')
                            ),
                            'validators' => array(
                                array (
                                    'name' => 'StringLength',
                                    'options' => array(
                                        'encoding' => 'UTF-8',
                                        'min' => 3,
                                    ),
                                ),
                            ),
                        );
        }
        return $result;
    }
}
