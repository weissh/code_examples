<?php

namespace PrismProductsManager\Form\Element;

use PrismProductsManager\Validator\Step;
use Zend\Form\Element\Number as ZendNumber;
use Zend\Validator\Step as ZendStep;

/**
 * Number
 *
 * Number form element.
 *
 * @package PrismProductsManager\Form\Element
 */
class Number extends ZendNumber
{
    /**
     * getValidators
     *
     * Return the form element validators.
     *
     * @return \PrismProductsManager\Validator\Number
     */
    protected function getValidators ()
    {
        $validators = parent::getValidators();

        foreach($validators as $index => $validator) {
            if ($validator instanceof ZendStep) {
                $options = [
                    'baseValue' => (isset($this->attributes['min'])  ? $this->attributes['min'] : 0),
                    'step'      => (isset($this->attributes['step']) ? $this->attributes['step'] : 1),
                ];

                $validators[$index] = new Step($options);
            }
        }

        return $validators;
    }


}