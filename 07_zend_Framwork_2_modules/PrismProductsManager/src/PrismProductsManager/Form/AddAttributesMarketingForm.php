<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class AddAttributesMarketingForm
 * @package PrismProductsManager\Form
 */
class AddAttributesMarketingForm extends Form implements  InputFilterProviderInterface
{
    /**
     * @var EventManagerInterface
     */
    protected $events;

    /**
     * @var
     */
    public $formData;

    protected  $_dbAdapter;

    /**
     *
     */
    public function __construct($dbAdapter)
    {
        parent::__construct('attributes-values-add');

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'attributeGroupId',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'attributeValueId',
        ));

        $this->add (array(
            'name' => 'attributeValue',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Attribute Value'
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));

        $this->add (array(
            'name' => 'submit',
            'type' => 'button',
            'attributes' => array (
                'class' => 'btn btn-primary save-attribute'
            ),

        ));

        $this->_dbAdapter = $dbAdapter;
    }


    /**
     *
     */
    public function init()
    {
        //already used in __construct because eventManager usage
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $result  =  array(
                        array(
                            'name' => 'attributeValue',
                            'required' => true,
                            'filters' => array(
                                array ('name' => 'StripTags'),
                                array ('name' => 'StringTrim'),
                                array ('name' => 'HtmlEntities')
                            ),
                            'validators' => array(
                                array (
                                    'name' => 'StringLength',
                                    'options' => array(
                                        'encoding' => 'UTF-8',
                                        'min' => 1,
                                        'max' => 60,
                                    ),
                                ),
                                array(
                                    'name'    => 'Zend\Validator\Db\NoRecordExists',
                                    'options' => array(
                                        'table' => 'ATTRIBUTES_Definitions',
                                        'field' => 'attributeValue',
                                        'adapter' => $this->_dbAdapter,
                                        'exclude' => array(
                                            'field' => 'status',
                                            'value' => 'DELETED'
                                        ),
                                        'messages' => array(
                                            \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'The attribute already exists in database'
                                        ),
                                    ),
                                ),
                            ),
                        ),
                        array(
                            'name'     => 'csrf',
                            'required' => true,
                            'validators' => array(
                                array(
                                    'name' => 'csrf',
                                    'options' => array(
                                        'messages' => array(
                                            \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    );
        return $result;
    }
}
