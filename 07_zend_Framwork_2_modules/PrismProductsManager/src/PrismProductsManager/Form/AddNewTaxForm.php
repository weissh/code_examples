<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;

class AddNewTaxForm extends Form implements EventManagerAwareInterface, InputFilterProviderInterface
{
    
    /**
     * @var EventManagerInterface
     */
    protected $events;
    
    /**
     * Declare the tax mapper
     * @var type 
     */
    protected $_taxMapper;
    protected $_countriesList;

    /**
     * Declare the service locator
     * @var type 
     */
    protected $_serviceLocator;
    
    //protected $_productTypes;

    protected $_parent = array();



    /**
     * Initialize the form
     */
    public function init()
    {
        
    }

    /**
     * Pass the service locator and product mapper to the constructor.
     * 
     * @param $serviceLocator
     * @param $taxMapper
     * @param $countriesList
     * @param $currentLanguage
     */
    public function __construct($serviceLocator, $taxMapper, $countriesList, $currentLanguage)
    {
        $this->_taxMapper = $taxMapper;
        $this->_serviceLocator = $serviceLocator;
        $this->_countriesList = $countriesList;
        $this->_taxTypes = $taxMapper->getTaxTypes($currentLanguage);

        parent::__construct('add-new-tax-form');
        $this->setAttributes(array(
            'name' => 'addnewTaxForm',
            'id' => 'add-new-tax-form',
            'role' => 'form',
            'action' => '/tax/add',
        ));
        $this->setHydrator(new ClassMethods());
        $this->prepareElements();
    }
    
    /**
     * Prepare the form to be built.
     */        
    public function prepareElements()
    {

        $this->add(array(
            'name' => 'id',
            'type' => 'hidden'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tax_country',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'tax-country',
                'required' => 'required',
            ),
            'options' => array(
                'label' => "Tax country *",
                'label_attributes' => array(
                    'for' => 'tax-country'
                ),
                'value_options' => $this->_countriesList,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tax_type',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'tax-type',
                'required' => 'required',
            ),
            'options' => array(
                'label' => "Tax type *",
                'label_attributes' => array(
                    'for' => 'tax-type'
                ),
                'value_options' => $this->_taxTypes,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'tax_value',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'tax-value',
                'value' => ''
            ),
            'options' => array(
                'label' => 'Tax value',
                'label_attributes' => array(
                    'for' => 'tax-value'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'name',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'name',
                'value' => ''
            ),
            'options' => array(
                'label' => 'Name',
                'label_attributes' => array(
                    'for' => 'name'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'description',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'description',
                'rows' => 3,
            ),
            'options' => array(
                'label' => 'Description',
                'label_attributes' => array(
                    'for' => 'description'
                ),
            ),
        ));
        
        $this->add(array(
            'name' => 'saveNewTax',
            'attributes' => array(
                'id' => 'save-new-tax',
                'type' => 'submit',
                'value' => ' Save ',
                'class' => 'btn btn-success pull-right',
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
        
    }
    
    /**
     * Input filter for validation
     * 
     * @return type
     */
    public function getInputFilterSpecification()
    {
        return array(
            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );
    }

    /**
     * Set the event manager instance used by this context
     *
     * @param EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;

        return $this;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }
}
