<?php
namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;


class CurrencyConverterForm extends Form implements InputFilterProviderInterface, ServiceManagerAwareInterface
{

    /**
     * Zend\ServiceManager\ServiceManager Object
     */
    protected $serviceManager;
    /**
     * array of active currencies
     * @var type array 
     */
    protected $_currencies;
    
    /*
     * PrismProductsManager\Model\CurrencyMapper Object
     */
    protected $_currencyMapper;
    
    /**
     * Init the form
     */
    public function init()
    {
        
        //Get Data object to interact with database
        $this->_currencyMapper = $this->serviceManager->get('PrismProductsManager\Model\CurrencyMapper');
        
        parent::__construct('currency-converter');
        $this->setAttributes(array(
            'name' =>  'currencyConverterForm',
            'role' => 'form',
            'action' => '/currency/converter',
        ));
        $this->prepareElements();
        
    }
    
     /**
     * @param ServiceManager $serviceManager
     * @return Form
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;

        // Call the init function of the form once the service manager is set
        $this->init();

        return $this;
    }
    
    public function getServiceManager() {
        return $this->serviceManager;
    }
    
    /**
     * Build add / edit category form
     */
    public function prepareElements()
    {
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'parent_group',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'groupParent',
                'required' => 'required',
            ),
            'options' => array(
                    'label' => 'Parent',
                    'label_attributes' => array (
                            'for' => 'groupParent'
                    ),
                    // this date will come from DB
                    'value_options' => array(
                        '1' => 'Root',
                        '2' => 'New Arrivals',
                        '3' => 'Shoes',
                        '4' => 'Formal Shoes',
                        '5' => 'Casual Shoes',
                        '6' => 'Men\'s Shoes',
                        '7' => 'Women\'s Shoes',
                        '8' => 'Sale',
                 ),

            ),
                
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'group_name', 
            'attributes' => array(
                    'class' => 'form-control',
                    'id' => 'groupName',
                    'placeholder' => 'Product group name',
                    //'required' => 'required',
            ),
            'options' => array(
                    'label' => 'Product Group Name',
                    'label_attributes' => array (
                            'for' => 'groupName'
                    ), 
            ),
        ));
        
        $this->add(array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'meta_title',
                'attributes' => array(
                        'class' => 'form-control',
                        'id' => 'metaTitle',
                        'placeholder' => 'Meta title',
                        //'required' => 'required',
                ),
                'options' => array(
                        'label' => 'Meta Title',
                        'label_attributes' => array (
                                'for' => 'groupName'
                        ),
                ),
        ));
        
        $this->add(array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'meta_keywords',
                'attributes' => array(
                        'class' => 'form-control',
                        'id' => 'metaKeywords',
                        'placeholder' => 'Meta keywords',
                        //'required' => 'required',
                ),
                'options' => array(
                        'label' => 'Meta Keywords',
                        'label_attributes' => array (
                                'for' => 'groupName'
                        ),
                ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
      
        
        $this->add(array(
                'name' => 'submitAddCategoryForm',
                'attributes' => array(
                        'type'  => 'submit',
                        'value' => 'Save',
                        //'class' => 'btn',
                ),
        ));
       
    }
    
    /**
     * Validate the form input
     * @see \Zend\InputFilter\InputFilterProviderInterface::getInputFilterSpecification()
     */   
    public function getInputFilterSpecification()
    {
        return array(
                array(
                        'name'     => 'parent_group',
                        //'required' => true,
                        'filters'  => array(
                                array('name' => 'StripTags'),
                                array('name' => 'StringTrim'),
                        ),
                ),
                array(
                        'name'     => 'group_name',
                        //'required' => true,
                        'filters'  => array(
                                array('name' => 'StripTags'),
                                array('name' => 'StringTrim'),
                        ),
                ),
                
                array(
                        'name'     => 'csrf',
                        'validators' => array(
                                array(
                                        'name'    => 'Csrf',
                                ),
                        ),
                ),
      );          
        
    }
    
    
}