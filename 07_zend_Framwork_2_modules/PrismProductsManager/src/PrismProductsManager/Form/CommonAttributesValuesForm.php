<?php

namespace PrismProductsManager\Form;

use Zend\Db\Adapter\Adapter;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use PrismProductsManager\Service\CommonAttributesService;

/**
 * Class CommonAttributesValuesForm
 * @package PrismProductsManager\Form
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class CommonAttributesValuesForm extends Form implements  InputFilterProviderInterface
{
    /**
     * @var EventManagerInterface
     */
    protected $events;
    /**
     * @var Adapter
     */
    protected $dbAdapter;

    /**
     * @var CommonAttributesService
     */
    protected $commonAttributesService;

    protected $listSizePropertyCommonAttributes;

    /**
     * @param Adapter $dbAdapter
     */
    public function __construct(Adapter $dbAdapter, CommonAttributesService $commonAttributesService, $attributeId)
    {
        $this->dbAdapter = $dbAdapter;
        $this->commonAttributesService = $commonAttributesService;

        if($attributeId !== null){
            $commonAttributeDetails = $this->commonAttributesService->getCommonAttribute($attributeId);
            $this->listSizePropertyCommonAttributes = $this->sizePropertyCommonAttributes($commonAttributeDetails);
        }


        parent::__construct('common-attributes-values-form');

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'commonAttributeValueId',
        ));
        $this->add (array(
            'name' => 'commonAttributeValueSkuCode',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'SKU Code'
            ),
            'options' => array(
                'label' => 'SKU code value',
                'label_attributes' => array(
                    'for' => 'commonAttributeValueSkuCode',
                    'class' => 'control-label'
                ),
            )
        ));
        $this->add (array(
            'name' => 'commonAttributeValueAbbrev',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Abbreviation Code'
            ),
            'options' => array(
                'label' => 'Abbreviation Code',
                'label_attributes' => array(
                    'for' => 'commonAttributeValueAbbrev',
                    'class' => 'control-label'
                ),
            )
        ));
        $this->add (array(
            'name' => 'commonAttributeValueBaseSize',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Base Size Code'
            ),
            'options' => array(
                'label' => 'Base Size Code',
                'label_attributes' => array(
                    'for' => 'commonAttributeValueBaseSize',
                    'class' => 'control-label'
                ),
            )
        ));

        $this->add (array(
            'name' => 'value',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Common Attribute Value'
            ),
            'options' => array(
                'label' => 'Common Attribute Value *',
                'label_attributes' => array(
                    'for' => 'value',
                    'class' => 'control-label'
                ),
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'validators' => array(
                array(
                    'name' => 'NotSame',
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\Csrf::NOT_SAME => 'Form has timeout out. Please submit the form again.',
                        ),
                    ),
                ),
            ),
        ));
        $this->add (array(
            'name' => 'submit',
            'type' => 'button',
            'attributes' => array (
                'type' => 'submit',
                'value' => 'Save',
                'class' => 'btn btn-primary save-common-attribute-value'
            )
        ));

        $count = 0;
        if(!empty($this->listSizePropertyCommonAttributes)){
            foreach ($this->listSizePropertyCommonAttributes as $select){
                $this->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => 'sizePropertyCommonAttribute_' . $count,
                    'required' => false,
                    'allow_empty' => true,
                    'continue_if_empty' => true,
                    'options' => array(
                        'disable_inarray_validator' => true,
                        'empty_option' => '-- Select value --',
                        'value_options' => $select['commonAttributeVaules'],
                        'label' => $select['description'],
                        'label_attributes' => array(
                            'for' => 'sizePropertyCommonAttribute_' . $count,
                            'class' => 'control-label',
                            'data-common-attribute-id' => $commonAttributeDetails->getCommonattributeid(),
                            'data-common-attribute-size-property-id' => $select['commonAttributeId'],
                        ),
                    ),
                    'attributes' => array (
                        'class' => 'selectize selectized',
                        'tabindex' => '-1',
                        //'style' => 'display:none'
                    )
                ));

                $this->add(array(
                    'type' => 'Zend\Form\Element\Hidden',
                    'name' => 'sizePropertyCommonAttributeId_' . $count,
                    'required' => false,
                    'allow_empty' => true,
                    'continue_if_empty' => true,
                    'attributes' => array(
                        'value' => $select['commonAttributeId']
                    )
                ));

                $count++;
            }
        }

        // get count of size property realted common attributes used for looping
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'sizePropertyCommonAttributeCount',
            'attributes' => array(
                'value' => $count
            )
        ));
    }

    /**
     *
     */
    public function init()
    {
        //already used in __construct because eventManager usage
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $result  =  array(
                        array(
                            'name' => 'commonAttributeValueId',
                            'required' => false,
                            'filters' => array(
                                array('name' => 'StripTags'),
                                array('name' => 'StringTrim'),
                                array ('name' => 'HtmlEntities')
                            ),
                            'validators' => array(
                                array(
                                    'name' => 'Int'
                                ),
                            )
                        ),
                        array(
                            'name' => 'sizePropertyCommonAttributeCount',
                            'required' => false,
                            'allow_empty' => true,
                            'continue_if_empty' => true,
                            'filters' => array(
                                array('name' => 'StripTags'),
                                array('name' => 'StringTrim'),
                                array ('name' => 'HtmlEntities')
                            ),
                            'validators' => array(
                                array(
                                    'name' => 'Int'
                                ),
                            )
                        ),
                        array(
                            'name' => 'commonAttributeValueOrder',
                            'required' => false,
                            'filters' => array(
                                array('name' => 'StripTags'),
                                array('name' => 'StringTrim'),
                                array ('name' => 'HtmlEntities')
                            ),
                            'validators' => array(
                                array(
                                    'name' => 'Int'
                                ),
                            )
                        ),
                        array(
                            'name' => 'commonAttributeValueSkuCode',
                            'required' => false,
                            'filters' => array(
                                array ('name' => 'StripTags'),
                                array ('name' => 'StringTrim')
                            ),
                            'validators' => array(
                                array (
                                    'name' => 'StringLength',
                                    'options' => array(
                                        'encoding' => 'UTF-8',
                                        'min' => 1,
                                        'max' => 4,
                                    ),
                                ),
                            ),
                        ),
                        array(
                            'name' => 'commonAttributeValueAbbrev',
                            'required' => false,
                            'filters' => array(
                                array ('name' => 'StripTags'),
                                array ('name' => 'StringTrim')
                            ),
                            'validators' => array(
                                array (
                                    'name' => 'StringLength',
                                    'options' => array(
                                        'encoding' => 'UTF-8',
                                        'min' => 1,
                                        'max' => 255,
                                    ),
                                ),
                            ),
                        ),
                        array(
                            'name' => 'commonAttributeValueBaseSize',
                            'required' => false,
                            'filters' => array(
                                array ('name' => 'StripTags'),
                                array ('name' => 'StringTrim')
                            ),
                            'validators' => array(
                                array (
                                    'name' => 'StringLength',
                                    'options' => array(
                                        'encoding' => 'UTF-8',
                                        'min' => 1,
                                        'max' => 15,
                                    ),
                                ),
                            ),
                        ),
                        array(
                            'name' => 'value',
                            'required' => true,
                            'filters' => array(
                                array ('name' => 'StripTags'),
                                array ('name' => 'StringTrim'),
                            ),
                            'validators' => array(
                                array (
                                    'name' => 'StringLength',
                                    'options' => array(
                                        'encoding' => 'UTF-8',
                                        'min' => 1,
                                        'max' => 150,
                                    ),
                                ),
                            ),
                        ),
                        array(
                            'name'     => 'csrf',
                            'required' => true,
                            'validators' => array(
                                array(
                                    'name' => 'csrf',
                                    'options' => array(
                                        'messages' => array(
                                            \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    );

        $inputFilterForSizePropertyValues = $this->inputFilterForSizePropertyValues();
        $inputFilterForsizePropertyCommonAttributeId = $this->inputFilterForsizePropertyCommonAttributeId();
        $result = array_merge($result, $inputFilterForSizePropertyValues, $inputFilterForsizePropertyCommonAttributeId);
        return $result;
    }

    protected function inputFilterForSizePropertyValues()
    {
        $result = [];
        $count = 0;
        foreach ($this->listSizePropertyCommonAttributes as $select){
            $result[] = [
                'name' => 'sizePropertyCommonAttribute_' . $count,
                'required' => false,
                'allow_empty' => true,
                'continue_if_empty' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim']
                ],
                'validators' => [],
            ];
        }

        if(!empty($result)){
            return $result;
        }
    }

    protected function inputFilterForsizePropertyCommonAttributeId()
    {
        $result = [];
        $count = 0;
        foreach ($this->listSizePropertyCommonAttributes as $select){
            $result[] = [
                'name' => 'sizePropertyCommonAttributeId_' . $count,
                'required' => false,
                'allow_empty' => true,
                'continue_if_empty' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim']
                ],
                'validators' => [],
            ];
        }

        if(!empty($result)){
            return $result;
        }
    }

    /**
     * @param Adapter $dbAdapter
     */
    public function setDbAdapter(Adapter $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    /**
     * Set a single option for an element
     *
     * @param  string $key
     * @param  mixed $value
     * @return self
     */
    public function setOption($key, $value)
    {
        // TODO: Implement setOption() method.
    }

    public function sizePropertyCommonAttributes($commonAttributeDetails)
    {
        $sizePropertyList = [];

        if($commonAttributeDetails->getIssize() === true ){
            $sizePropertyList = $this->commonAttributesService->getSizePropertyCommonAttributesWithValues($commonAttributeDetails->getCommonattributeid());
        }

        return $sizePropertyList;
    }
}