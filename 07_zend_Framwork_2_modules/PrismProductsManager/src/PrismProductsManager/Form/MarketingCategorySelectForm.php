<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;

class MarketingCategorySelectForm extends Form implements EventManagerAwareInterface, InputFilterProviderInterface
{

    /**
     * @var EventManagerInterface
     */
    protected $events;
    
    /**
     * Declare the products mapper
     * @var type 
     */
    protected $_productsMapper;
    
    /**
     *  Declare the parent categories
     * @var type 
     */
    protected $_parentCategories = array();
    
    /**
     * Declare the service locator
     * @var type 
     */
    protected $_serviceLocator;

    /**
     * Does a category has children
     * @var type 
     */
    protected $_childrenCount = 0;

    /**
     * Does a category has children
     * @var type 
     */
    protected $_treeDepth = 1;

    /**
     * Does a category has children
     * @var type 
     */
    protected $_position = 1;


    /**
     * Initialize the form
     */
    public function init()
    {
        
    }

    /**
     * Pass the service locator and product mapper to the constructor.
     * 
     * @param type $serviceLocator
     * @param type $productMapper
     */
    public function __construct($serviceLocator, $productMapper)
    {
        $this->_productsMapper = $productMapper;
        $this->_serviceLocator = $serviceLocator;
        
        $parentId = 1;
        $excludeRootCategory = true;
        $includeTreeDepth = false;
        $treeDepth = 1;
        $position = 1;
        $parentCategories = $this->_productsMapper->fetchMarketingCategories($parentId, $treeDepth, $position, $excludeRootCategory, $includeTreeDepth);
        if ($parentCategories['dataCount'] > 0 && count($parentCategories['data']) > 0) {
            foreach ($parentCategories['data'] as $categoryId => $categoryDetails) {
                if ($categoryId > 0) {
                    $this->_parentCategories[$categoryId] = $categoryDetails['categoryName'];
                } else {
                    $this->_parentCategories[$categoryId] = $categoryDetails;
                }
            }
        }
        
        $this->_childrenCount = $parentCategories['childrenCount'];
        $this->_treeDepth = isset($parentCategories['treeDepth']) ? $parentCategories['treeDepth'] : 1;
        $this->_position = isset($parentCategories['position']) ? $parentCategories['position'] : 1;
        
        parent::__construct('select-marketing-category-form');
        $this->setAttributes(array(
            'name' => 'selectMarketingCategoryForm',
            'id' => 'select-marketing-category-form',
            'role' => 'form',
            'action' => '/products/add',
        ));
        $this->setHydrator(new ClassMethods());
        $this->prepareElements();
    }

    /**
     * Prepare the form to be built.
     */        
    public function prepareElements()
    {

        $this->add(array(
            'name' => 'id',
            'type' => 'hidden'
        ));
        
         $this->add(array(
             'type' => 'Zend\Form\Element\Hidden',
             'name' => 'productMarketingCategoryCount',
             'attributes' => array(
                     'value' => 0
             )
         ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'saveVariantMarketingCategory',
            'attributes' => array(
                'value' => 1
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'variantId',
            'attributes' => array(
                 'value' => 0
            )
        ));
         
        $this->add(array(
             'type' => 'Zend\Form\Element\Hidden',
             'name' => 'productId',
             'attributes' => array(
                     'value' => 0
             )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'marketingCategory1',
            'attributes' => array(
                'class' => 'selectize selectized parent-category marketing-category',
                'id' => 'marketing-category-1',
                'required' => 'required',
                'data-children-count' => $this->_childrenCount,
                'data-tree-depth' => $this->_treeDepth,
                'data-position' => $this->_position,
                
            ),
            'options' => array(
                'label' => "Parent Category *",
                'label_attributes' => array(
                    'for' => 'group-parent'
                ),
                // this date will come from DB
                'value_options' => $this->_parentCategories,
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
    
        $this->add(array(
            'name' => 'addSelectedCategory',
            'attributes' => array(
                'id' => 'select-marketing-category',
                'type' => 'button',
                'value' => 'Add selected categories',
                'class' => 'btn btn-primary btn-xs hide',
            ),
        ));
    
        $this->add(array(
            'name' => 'addSelectedCategoryForVariant',
            'attributes' => array(
                'id' => 'select-marketing-category',
                'type' => 'button',
                'value' => 'Add selected categories',
                'class' => 'btn btn-primary btn-xs hide',
            ),
        ));
    
        $this->add(array(
            'name' => 'submitMarketingCategoryForm',
            'attributes' => array(
                'id' => 'submit-marketing-category',
                'type' => 'submit',
                'value' => 'Select and continue',
                'class' => 'btn btn-success pull-right',
            ),
        ));
        
    }
    
    /**
     * Input filter for validation
     * 
     * @return type
     */
    public function getInputFilterSpecification()
    {
        return array(
            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );
    }

    /**
     * Set the event manager instance used by this context
     *
     * @param EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;

        return $this;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }

}
