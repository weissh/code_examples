<?php

namespace PrismProductsManager\Form;

use Zend\Form\ElementInterface;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\File\MimeType;

/**
 * Class BulkUploadForm
 * @package PrismProductsManager\Form
 */
class BulkUploadForm extends Form implements  InputFilterProviderInterface
{
    /**
     * @var EventManagerInterface
     */
    protected $events;

    /**
     * @var
     */
    public $formData;

    /**
     *
     */
    public function __construct()
    {
        parent::__construct('bulk-upload');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');

        $this->add(array(
            'type' => 'Zend\Form\Element\File',
            'name' => 'feedAttachment',
            'attributes' => array(
                'class' => 'hide',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'uploadType',
            'options' => array(
                'value_options' => array (
                    '' => ' ',
                )
            ),
            'attributes' => array (
                'class' => 'selectize selectized',
                'tabindex' => '-1',
                'style' => 'display:none'
            )
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));

        $this->add (array(
            'name' => 'submit',
            'type' => 'button',
            'value' => 'Upload Template',
            'attributes' => array (
                'class' => 'btn btn-primary upload-template'
            )
        ));

    }


    /**
     *
     */
    public function init()
    {
        //already used in __construct because eventManager usage
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $result  =  array(
            array(
                'name' => 'feedAttachment',
                'required' => true,
                'validators' => array(
                    array(
                        'name'    => '\Zend\Validator\File\UploadFile',
                    ),
                    array(
                        'name'    => '\Zend\Validator\File\Size',
                        'options' => ['max' => '20MB', 'message' => 'Maximum file size for image is 2MB.'],
                    ),
                    array(
                        'name'    => '\Zend\Validator\File\Extension',
                        'options' => ['extension' => array('xlsx'), 'message' => 'Invalid File. Only xlsx files allowed.']
                    ),
                ),
            ),
            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );
        return $result;
    }

    /**
     * Set a single option for an element
     *
     * @param  string $key
     * @param  mixed $value
     * @return self
     *  @codeCoverageIgnoreStart no need to test empty method
     */
    public function setOption($key, $value)
    {

    }
    //  @codeCoverageIgnoreEnd no need to test empty method
}
