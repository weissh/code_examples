<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;

class SearchForm extends Form implements InputFilterProviderInterface
{
    /**
      * @var EventManagerInterface
    */
    protected $events;

    public function __construct()
    {

        parent::__construct('search-form');

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'search',
            'attributes' => array(
                'placeholder' => 'Search Here',
                'class' => 'form-control pull-left dash-search'
            )
        ));

    }

    public function init()
    {
        //already used in __construct because eventManager usage
    }

    public function getInputFilterSpecification()
    {
        return array(
            array(
                'name' => 'search',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 50,
                        )
                    ),
                )
            ),
            array(
              'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name'    => 'Csrf',
                    ),
                ),
            ),
        );
    }
}
