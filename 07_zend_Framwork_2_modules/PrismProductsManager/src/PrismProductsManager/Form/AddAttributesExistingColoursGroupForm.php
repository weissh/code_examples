<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class AddAttributesColoursForm
 * @property mixed _dbAdapter
 * @package PrismProductsManager\Form
 */
class AddAttributesExistingColoursGroupForm extends Form implements InputFilterProviderInterface
{
    /**
     * @var \Zend\EventManager\EventManagerInterface
     */
    protected $events;

    public $formData;

    protected $_dbAdapter;

    public function __construct()
    {
        parent::__construct('attributes-existing-colours-add');

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'colourGroups',
            'options' => array(
                'disable_inarray_validator' => true, // <-- disable
            ),
            'attributes' => array (
                'class' => 'selectize selectized existing-groups-select',
                'tabindex' => '-1',
                'multiple' => 'multiple',
            )
        ));
        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'attributes' => array (
                'class' => 'btn btn-primary save-attribute'
            )
        ));
    }

    /**
     *
     */
    public function init()
    {
        //already used in __construct because eventManager usage
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $result =
        array(
            array(
                'name' => 'colourGroups',
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Explode',
                        'options' => array(
                            'validator' => new \Zend\I18n\Validator\IsInt()
                        )
                    )
                )
            ),
            array(
                'name' => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );
        return $result;
    }

    /**
     * @param mixed $dbAdapter
     */
    public function setDbAdapter($dbAdapter)
    {
        $this->_dbAdapter = $dbAdapter;
    }
}
