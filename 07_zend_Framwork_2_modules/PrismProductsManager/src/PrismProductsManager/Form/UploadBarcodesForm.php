<?php

namespace PrismProductsManager\Form;

use Zend\Form\ElementInterface;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class DownloadBulkForm
 * @package PrismProductsManager\Form
 */
class UploadBarcodesForm extends Form implements  InputFilterProviderInterface
{
    public function init()
    {
        parent::__construct('barcode-upload');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/barcode-upload');
        $this->setAttribute('enctype','multipart/form-data');

        $this->add(array(
            'type' => 'Zend\Form\Element\File',
            'name' => 'barcodesFile',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
        ));

        $this->add (array(
            'name' => 'submit',
            'type' => 'button',
            'attributes' => array (
                'class' => 'btn btn-primary',
                'value' => 'Upload',
            )
        ));
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $result  =  array(
            array(
                'name' => 'barcodesFile',
                'required' => true,
                'validators' => array(
                    new \Zend\Validator\File\UploadFile(),
                    new \Zend\Validator\File\Extension(array('xls','xlsx'), true)
                ),
            ),
            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );
        return $result;
    }
}
