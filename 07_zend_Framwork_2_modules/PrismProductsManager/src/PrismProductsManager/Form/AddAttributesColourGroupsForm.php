<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class AddAttributesColoursForm
 * @property mixed _dbAdapter
 * @package PrismProductsManager\Form
 */
class AddAttributesColourGroupsForm extends Form implements  InputFilterProviderInterface
{
    /**
     * @var \Zend\EventManager\EventManagerInterface
     */
    protected $events;

    public $formData;

    protected $_dbAdapter;
    private $_submitedData;

    public function __construct()
    {
        parent::__construct('attributes-colours-add');

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'colourGroupId',
        ));
        $this->add(array(
            'name' => 'colourGroupCode',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Colour Code',
                'maxlength' => '5'
            ),
        ));
        $this->add(array(
            'name' => 'hex',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control pull-left',
                'placeholder' => 'Hex Code',
                'maxlength' => '7',
                'style' => 'width:75%',
                'id' => 'hex'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'hasImage',
            'attributes' => array (
                'class' =>"checkbox-select-image"
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'imageId',
            'attributes' => array (
                'id' =>"image-id"
            )
        ));
        $this->add(array(
            'name' => 'colourGroupName',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Colour Name'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'colours',
            'attributes' => array (
                'class' => 'selectize selectized colours_in_group_select',
                'tabindex' => '-1',
                'multiple' => 'multiple',
            )
        ));
        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'attributes' => array (
                'class' => 'btn btn-primary save-attribute'
            )
        ));
    }

    /**
     *
     */
    public function init()
    {
        //already used in __construct because eventManager usage
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $result =
        array(
            array(
                'name' => 'colourGroupId',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities')
                ),
                'validators' => array(
                    array(
                        'name' => 'Int'
                    ),
                )
            ),
            array(
                'name' => 'colourGroupCode',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 5,
                        )
                    ),
                )
            ),
            array(
                'name' => 'colourGroupName',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 30,
                        )
                    ),
                )
            ),
            array(
                'name' => 'colours',
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Explode',
                        'options' => array(
                            'validator' => new \Zend\I18n\Validator\IsInt()
                        )
                    )
                )
            ),
            array(
                'name' => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );
        if (empty($this->_submitedData['hex'])) {
            $result[] = array(
                'name' => 'hasImage',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                )
            );
        } else {
            $result[] = array(
                'name' => 'hex',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 7,
                            'max' => 7,
                        )
                    ),
                )
            );
        }
        return $result;
    }

    /**
     * @param mixed $dbAdapter
     */
    public function setDbAdapter($dbAdapter)
    {
        $this->_dbAdapter = $dbAdapter;
    }

    /**
     * @param mixed $submitedData
     */
    public function setSubmitedData($submitedData)
    {
        $this->_submitedData = $submitedData;
    }

    /**
     * Set a single option for an element
     *
     * @param  string $key
     * @param  mixed $value
     * @return self
     */
    public function setOption($key, $value)
    {
        // TODO: Implement setOption() method.
    }
}
