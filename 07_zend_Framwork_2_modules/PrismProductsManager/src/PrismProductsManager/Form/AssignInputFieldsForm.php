<?php

namespace PrismProductsManager\Form;

use PrismProductsManager\Model\AttributesGroupMapper;
use PrismProductsManager\Service\AssignFieldToTabsService;
use PrismProductsManager\Service\CommonAttributesService;
use Zend\Form\Form;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Class AssignInputFieldsForm
 * @package PrismProductsManager\Form
 */
class AssignInputFieldsForm extends Form implements InputFilterProviderInterface
{
    /**
     * @var AssignFieldToTabsService
     */
    protected $assignFieldToTabsService;
    /**
     * @var AttributesGroupMapper
     */
    protected $attributesGroupMapper;
    /**
     * @var array
     */
    protected $inputPagesSelectArray = array();
    /**
     * @var array
     */
    protected $attributeTypes = array();
    /**
     * @var CommonAttributesService
     */
    protected $commonAttributesService;

    /**
     * @var array
     */
    protected $commonAttributes = array();

    /**
     * @var array
     */
    protected $coreAttributes = array();

    /**
     * @var array
     */
    protected $marketingAttributes = array();

    /**
     * @var array
     */
    protected $merchantAttributes = array();

    /**
     * @param AssignFieldToTabsService $assignFieldToTabsService
     * @param CommonAttributesService $commonAttributesService
     * @param AttributesGroupMapper $attributesGroupMapper
     */
    public function __construct(
        AssignFieldToTabsService $assignFieldToTabsService,
        CommonAttributesService $commonAttributesService,
        AttributesGroupMapper $attributesGroupMapper
    )
    {
        /** Services */
        $this->assignFieldToTabsService = $assignFieldToTabsService;
        $this->commonAttributesService = $commonAttributesService;
        /** Mappers */
        $this->attributesGroupMapper = $attributesGroupMapper;
        /** Set form params */
        parent::__construct('assign-input-fields-form');
        $this->setAttributes(array(
            'name' => 'assign-input-fields-form',
            'id' => 'assign-input-fields-form',
            'role' => 'form',
        ));

        if ($this->retrieveDataForElements()) {
            /** Generate form elements */
            $this->prepareElements();
        }
    }

    /**
     * @return bool
     */
    public function retrieveDataForElements()
    {
        try {
            /** Data for inputPages select */
            $inputPagesValues = $this->assignFieldToTabsService->getAllInputPages(array('allownewfields' => '1'), array('pageOrder' => 'ASC'));
            $this->inputPagesSelectArray = array('0' => 'Please select product tab');
            foreach ($inputPagesValues as $inputPageValue) {
                $this->inputPagesSelectArray[$inputPageValue->getInputPageId()] = $inputPageValue->getPageName();
            }
            /** Data for attributeType */
            $this->attributeTypes = array(
                '0' => 'Please select attribute Type',
                'COMMON_ATTRIBUTES' => 'Common Attributes',
                'CORE_ATTRIBUTES' => 'Core Attributes',
//                'MARKETING_ATTRIBUTES' => 'Marketing Attributes',
//                'MERCHANT_ATTRIBUTES' => 'Merchant Attributes'
            );
            /** Data for commonAttributes */
            $commonAttributes = $this->commonAttributesService->getCommonAttributes(1, null);
            $this->commonAttributes[0] = 'Select Common Attribute';
            foreach ($commonAttributes as $commonAttribute) {
                $value =  html_entity_decode($commonAttribute->getCommonAttributeDefinitions()->getDescription(), ENT_COMPAT, "iso-8859-1");
                $test = htmlspecialchars_decode($value);
                $this->commonAttributes[$commonAttribute->getCommonAttributeId()] = $test;
            }
            /** Data for core Attributes */
            $coreAttributes = $this->attributesGroupMapper->fetchAllCoreAttributes();
            foreach ($coreAttributes as $coreAttribute) {
                $this->coreAttributes[$coreAttribute['attributeGroupId']] = $coreAttribute['name'];
            }
            /** Data for marketing Attributes */
            $marketingAttributes = $this->attributesGroupMapper->fetchAllMarketingAttributes();
            foreach ($marketingAttributes as $marketingAttribute) {
                $this->marketingAttributes[$marketingAttribute['attributeGroupId']] = $marketingAttribute['name'];
            }
            /** Data for merchant attributes */
            $merchantAttributes = $this->attributesGroupMapper->fetchAllMerchantsAttributes();
            foreach ($merchantAttributes as $merchantAttribute) {
                $this->merchantAttributes[$merchantAttribute['attributeGroupId']] = $merchantAttribute['name'];
            }

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }
    /**
     * Prepare the form to be built.
     */        
    public function prepareElements()
    {
        $this->add(array(
            'name' => 'inputFieldId',
            'type' => 'hidden'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'inputPages',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'inputPages',
            ),
            'options' => array(
                'label' => "Product tab *",
                'label_attributes' => array(
                    'for' => 'inputPages'
                ),
                'value_options' => $this->inputPagesSelectArray,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'inputFieldsets',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'inputFieldsets',
            ),
            'options' => array(
                'label' => "FieldSet *",
                'label_attributes' => array(
                    'for' => 'inputFieldsets'
                ),
                'value_options' => array('Select product tab before selecting the fieldset.'),
                'disable_inarray_validator' => true
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'attributeType',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'attributeType',
            ),
            'options' => array(
                'label' => "Attribute Type *",
                'label_attributes' => array(
                    'for' => 'attributeType'
                ),
                'value_options' => $this->attributeTypes,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'commonAttributes',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'commonAttributes',
            ),
            'options' => array(
                'label' => "Please Select Attribute",
                'label_attributes' => array(
                    'for' => 'commonAttributes'
                ),
                'value_options' => $this->commonAttributes,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'coreAttributes',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'coreAttributes',
            ),
            'options' => array(
                'label' => "Please Select Attribute",
                'label_attributes' => array(
                    'for' => 'coreAttributes'
                ),
                'value_options' => $this->coreAttributes,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'marketingAttributes',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'marketingAttributes',
            ),
            'options' => array(
                'label' => "Please Select Attribute",
                'label_attributes' => array(
                    'for' => 'marketingAttributes'
                ),
                'value_options' => $this->marketingAttributes,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'merchantAttributes',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'merchantAttributes',
            ),
            'options' => array(
                'label' => "Please Select Attribute",
                'label_attributes' => array(
                    'for' => 'merchantAttributes'
                ),
                'value_options' => $this->merchantAttributes,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'editableAtLevel',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'editableAtLevel',
            ),
            'options' => array(
                'label' => "Field is editable at ",
                'label_attributes' => array(
                    'for' => 'editableAtLevel'
                ),
                'value_options' => array('OPTION' => 'OPTION LEVEL', 'STYLE' => 'STYLE LEVEL'),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'newInputFieldset',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'new-input-field',
                'placeholder' => 'New Fieldset',
                'required' => 'required',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'display-only-value[]',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'display-only-value',
                'placeholder' => 'test',
                'required' => 'required',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'isSchedulable',
            'attributes' => array (
                'class' => "checkbox-select-image"
            ),
            'options' => array(
                'label' => "Is this field schedulable ?",
                'label_attributes' => array(
                    'for' => 'isSchedulable'
                )
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'isHidden',
            'attributes' => array (
                'class' => "checkbox-select-image"
            ),
            'options' => array(
                'label' => "Is this field hidden ?",
                'label_attributes' => array(
                    'for' => 'isHidden'
                )
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));

        $this->add(array(
            'name' => 'create-new-field',
            'attributes' => array(
                'id' => 'create-new-field',
                'type' => 'submit',
                'value' => 'Create',
                'class' => 'btn btn-success pull-right',
            ),
        ));
        $this->add(array(
            'name' => 'edit-field',
            'attributes' => array(
                'id' => 'edit-field',
                'type' => 'submit',
                'value' => 'Edit',
                'class' => 'btn btn-success pull-right',
            ),
        ));
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            array(
                'name' => 'merchantAttributes',
                'required' => false,
                'allow_empty' => true,
            ),
            array(
                'name' => 'marketingAttributes',
                'required' => false,
                'allow_empty' => true,
            ),
            array(
                'name' => 'coreAttributes',
                'required' => false,
                'allow_empty' => true,
            ),
            array(
                'name' => 'commonAttributes',
                'required' => false,
                'allow_empty' => true,
            ),
            array(
                'name' => 'attributeType',
                'required' => false,
                'allow_empty' => true,
            ),
            array(
                'name' => 'inputPages',
                'required' => false,
                'allow_empty' => true,
            ),
            array(
                'name' => 'inputFieldsets',
                'required' => false,
                'allow_empty' => true,
            ),

            array(
                'name' => 'newInputFieldset',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                        )
                    ),
                )
            ),
            array(
                'name' => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            )
        );
    }
}
