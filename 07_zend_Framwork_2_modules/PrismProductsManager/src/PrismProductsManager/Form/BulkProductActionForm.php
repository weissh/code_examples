<?php
namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class BulkProductActionForm
 * @package PrismProductsManager\Form
 */
class BulkProductActionForm extends Form implements InputFilterProviderInterface
{
    public function __construct()
    {
        parent::__construct('bulk-update');
        $this->setAttributes(array(
            'name' => 'bulkProductActionForm',
            'role' => 'form',
            'action' => '/products',
        ));
        $this->prepareElements();
    }
    
    public function prepareElements()
    {
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'showBulkProductActionPopup',
            'attributes' => array(
                'value' => 0
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'popupType',
            'attributes' => array(
                'value' => ''
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'productCodes',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'product-codes',
                'rows' => 10
            ),
            'options' => array(
                'label' => 'Product Codes (One per line)',
                'label_attributes' => array(
                    'for' => 'product-codes',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => '<p>Product codes can be at</p><p><ul><li>Style level</li></ul></p>',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
        ));

        $this->add(array(
            'name' => 'enableProductCodes',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit',
                'id' => 'enable-product-codes',
                'class' => 'btn btn-primary pull-right',
            ),
        ));

        $this->add(array(
            'name' => 'disableProductCodes',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit',
                'id' => 'disable-product-codes',
                'class' => 'btn btn-primary pull-right',
            ),
        ));

        $this->add(array(
            'name' => 'deleteProductCodes',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit',
                'id' => 'delete-product-codes',
                'class' => 'btn btn-primary pull-right',
            ),
        ));
    }
    
    /**
     * Validate the form input
     * @see \Zend\InputFilter\InputFilterProviderInterface::getInputFilterSpecification()
     */
    public function getInputFilterSpecification()
    {
        $result = array(            
            array(
                'name' => 'productCodes',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 6,
                        )
                    ),
                )
            ),
            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );
        
        return $result;
    }
    
}
