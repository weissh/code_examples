<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;

class ProductsBasicInformationForm extends Form implements EventManagerAwareInterface, InputFilterProviderInterface
{

    /**
     * @var EventManagerInterface
     */
    protected $events;

    /**
     * @var
     */
    protected $_dbAdapter;

    /**
     * Declare the products mapper
     * @var type
     */
    protected $_productsMapper;

    /**
     * Declare the service locator
     * @var type
     */
    protected $_serviceLocator;
    protected $_channels = array();
    protected $_websites = array();
    protected $_defaultWebsite = array();
    protected $_status = array();
    protected $_manufacturer = array();
    protected $_currenciesPriceTypeMatrix = array();
    protected $_languageId;
    protected $_careInformation = array();

    protected $_supplierCurrency = array('GBP', 'USD', 'EUR');

    protected $_productTypes = array();

    /**
     * Initialize the form
     */
    public function init()
    {

    }

    /**
     * Pass the service locator and product mapper to the constructor.
     *
     * @param type $serviceLocator
     * @param type $productMapper
     */
    public function __construct($serviceLocator, $productMapper)
    {
        $this->_productsMapper = $productMapper;
        $this->_serviceLocator = $serviceLocator;

        $this->_dbAdapter = $serviceLocator->get('db-pms');

        $config = $serviceLocator->get('config');
        $this->_languageId = $config['siteConfig']['languages']['current-language'];

        $basicDetails = $this->_productsMapper->fetchBasicDetails();
        $this->_websites = $basicDetails['websites'];
        $this->_defaultWebsite = $basicDetails['defaultWebsite'];
        $this->_channels = $basicDetails['channels'];
        $this->_manufacturer = $basicDetails['manufacturers'];
        $this->_currenciesPriceTypeMatrix = $basicDetails['currenciesPriceTypeMatrix'];
        $this->_careInformation = $basicDetails['careInformation'];
        $this->_suppliers = $this->_productsMapper->getSuppliers($this->_languageId);

        $this->_supplierCurrency = $this->_productsMapper->getSuppliersCurrency(true);

        $productTypes = $this->_productsMapper->getProductTypes();
        if (count($productTypes) > 0) {
            foreach ($productTypes as $key => $productTypeArray) {
                $this->_productTypes[$productTypeArray['productTypeId']] = $productTypeArray['name'];
            }
        }

        parent::__construct('products-basic-information-form');
        $this->setAttributes(array(
            'name' => 'productsBasicInformationForm',
            'id' => 'products-basic-information-form',
            'role' => 'form',
            'action' => '/products/add',
        ));
        $this->setHydrator(new ClassMethods());
        $this->prepareElements();
    }

    public function getCurrenciesPriceTypeMatrix()
    {
        return $this->_currenciesPriceTypeMatrix;
    }

    public function setCurrenciesPriceTypeMatrix($_currenciesPriceTypeMatrix)
    {
        $this->_currenciesPriceTypeMatrix = $_currenciesPriceTypeMatrix;
    }

    public function getDefaultWebsite()
    {
        return $this->_defaultWebsite;
    }

    public function setDefaultWebsite($_defaultWebsite)
    {
        $this->_defaultWebsite = $_defaultWebsite;
    }

    /**
     * Prepare the form to be built.
     */
    public function prepareElements()
    {

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'productId',
            'attributes' => array(
                'value' => 0
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'productDefinitionsId',
            'attributes' => array(
                'value' => 0
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'isVariant',
            'attributes' => array(
                'value' => 1
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'variantId',
            'attributes' => array(
                'value' => 0
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'productTypeId',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'product-type-id',
                'required' => 'required',
            ),
            'options' => array(
                'label' => "Product Type *",
                'label_attributes' => array(
                    'for' => 'product-type-id'
                ),
                'value_options' => $this->_productTypes,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'shortProductName',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'short-product-name',
                'placeholder' => 'Short Product Name',
                'maxLength' => 80
            ),
            'options' => array(
                'label' => 'Short Product Name *',
                'label_attributes' => array(
                    'for' => 'short-product-name',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => '<p>The short name for the product</p><p>It is a mandatory field and allows a maximum length of 30 characters</p>',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'productName',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'product-name',
                'placeholder' => 'Product Name',
                'maxLength' => 180
            ),
            'options' => array(
                'label' => 'Product Name *',
                'label_attributes' => array(
                    'for' => 'product-name',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => '<p>The full name for the product</p><p>It is a mandatory field and allows a maximum length of 50 characters</p>',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'productSupplierPrice',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'product-name',
                'placeholder' => 'Amount',
                'maxLength' => 10
            ),
            'options' => array(
                'label' => 'Supplier Price',
                'label_attributes' => array(
                    'for' => 'supplier-price',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => '<p>Please add the product supplier price</p>',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'productSupplierCurrency',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'supplier-reference',
                //'required' => 'required'
            ),
            'options' => array(
                'label' => 'Product Supplier Currency',
                'label_attributes' => array(
                    'for' => 'product-supplier-currency',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => 'The code which the supplier supplies this product.',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
                'value_options' => $this->_supplierCurrency,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'style',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'style',
                'placeholder' => 'Style',
                'maxLength' => 30
            ),
            'options' => array(
                'label' => 'Style *',
                'label_attributes' => array(
                    'for' => 'style',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => '<p>The style of product.</p><p>We will auto-generate it from the first 6 characters of the product name but you can change it if you want.</p>',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'ean13',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'ean13',
                'placeholder' => 'Barcode',
                'maxLength' => 30,
                'readonly' => true
            ),
            'options' => array(
                'label' => 'Barcode',
                'label_attributes' => array(
                    'for' => 'ean13',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => 'The product barcode.',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'supplierReference',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'supplier-reference',
                'required' => 'required'
            ),
            'options' => array(
                'label' => 'Supplier Reference',
                'label_attributes' => array(
                    'for' => 'supplier-reference',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => 'The code which the supplier supplies this product.',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
                'value_options' => $this->_suppliers,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'makeLive',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'make-live',
                'placeholder' => 'Make Live Date',
                'maxLength' => 25
            ),
            'options' => array(
                'label' => 'Make Live Date',
                'label_attributes' => array(
                    'for' => 'make-live',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => 'The date the product will be enabled to be sold.',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'urlName',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'url-name',
                'placeholder' => 'URL Name',
                'maxLength' => 180
            ),
            'options' => array(
                'label' => 'URL Name *',
                'label_attributes' => array(
                    'for' => 'url-name',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => '<p>The product url</p><p>we will auto-generate this but you can change it</p><p>Please note that spaces are not allowed and must be replaced with hyphens</p>.',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'tags',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'tags',
                'placeholder' => 'Tags',
            ),
            'options' => array(
                'label' => 'Tags (comma separated)',
                'label_attributes' => array(
                    'for' => 'tags',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => '<p>Possible search words for this product.</p><p>Please separate with commas</p>',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'misSpells',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'mis-spells',
                'placeholder' => 'Mis Spells',
            ),
            'options' => array(
                'label' => 'Mis Spells (comma separated)',
                'label_attributes' => array(
                    'for' => 'mis-spells',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => '<p>Possible mis spelt words for this product.</p><p>Please separate with commas</p>',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'sku',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'sku',
                'placeholder' => 'Sku',
                'maxLength' => 32
            ),
            'options' => array(
                'label' => 'Sku',
                'label_attributes' => array(
                    'for' => 'sku',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => '<p>The product sku</p><p>This MUST be set if this product doesn\'t have variants</p>',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'shortDescription',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'short-description',
                'placeholder' => 'Short Description',
                'maxLength' => 160
            ),
            'options' => array(
                'label' => 'Short Description (Maximum 160 characters) *',
                'label_attributes' => array(
                    'for' => 'short-description',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => '<p>A short description for the product</p><p>It allows a maximum of 160 characters</p>',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'longDescription',
            'attributes' => array(
                'class' => 'form-control tinymce',
                'id' => 'long-description',
                'rows' => 3
            ),
            'options' => array(
                'label' => 'Long Description *',
                'label_attributes' => array(
                    'for' => 'long-description',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => 'The full description of the product',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'tax',
            'attributes' => array(
                'value' => ''
            )
        ));

        if (count($this->_manufacturer) > 0) {
            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'manufacturerId',
                'attributes' => array(
                    'class' => 'selectize selectized',
                    'id' => 'manufacturer-id',
                    'required' => 'required'
                ),
                'options' => array(
                    'label' => "Manufacturer",
                    'label_attributes' => array(
                        'for' => 'manufacturer-id',
                        'class' => 'moreInfo',
                        'data-container' => 'body',
                        'data-toggle' => 'popover',
                        'data-placement' => 'bottom',
                        'data-content' => 'The product manufacturer',
                        'data-trigger' => 'hover',
                        'data-html' => 'true'
                    ),
                    'value_options' => $this->_manufacturer,
                ),
            ));
        }

        if (count($this->_careInformation) > 0) {
            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'careInformation',
                'attributes' => array(
                    'class' => 'selectize selectized',
                    'id' => 'care-information',
                    'required' => 'required'
                ),
                'options' => array(
                    'label' => "Care Information",
                    'label_attributes' => array(
                        'for' => 'care-information',
                        'class' => 'moreInfo',
                        'data-container' => 'body',
                        'data-toggle' => 'popover',
                        'data-placement' => 'bottom',
                        'data-content' => 'Information showing how this product can be taken care of',
                        'data-trigger' => 'hover',
                        'data-html' => 'true'
                    ),
                    'value_options' => $this->_careInformation,
                ),
            ));
        }

        if (count($this->_websites) > 0) {
            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'websites',
                'attributes' => array(
                    'multiple' => 'multiple',
                    'class' => 'form-control',
                    'id' => 'websites',
                    'placeholder' => 'Websites'
                ),
                'options' => array(
                    'label' => 'Websites available *',
                    'label_attributes' => array(
                        'for' => 'websites',
                        'class' => 'moreInfo',
                        'data-container' => 'body',
                        'data-toggle' => 'popover',
                        'data-placement' => 'bottom',
                        'data-content' => 'The websites where the product can be sold',
                        'data-trigger' => 'hover',
                        'data-html' => 'true'
                    ),
                    'value_options' => $this->_websites,
                ),
            ));
        }
        if (count($this->_channels) > 0) {
            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'channels',
                'attributes' => array(
                    'multiple' => 'multiple',
                    'class' => 'form-control',
                    'id' => 'channels',
                    'required' => 'required',
                ),
                'options' => array(
                    'label' => "Channels Available *",
                    'label_attributes' => array(
                        'for' => 'channels',
                        'class' => 'moreInfo',
                        'data-container' => 'body',
                        'data-toggle' => 'popover',
                        'data-placement' => 'bottom',
                        'data-content' => 'The channels where the product can be sold',
                        'data-trigger' => 'hover',
                        'data-html' => 'true'
                    ),
                    'value_options' => $this->_channels,
                ),
            ));
        }

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'metaTitle',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'meta-title',
                'placeholder' => 'META Title',
                'maxLength' => 180
            ),
            'options' => array(
                'label' => 'META Title (Maximum 180 characters)',
                'label_attributes' => array(
                    'for' => 'meta-title',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => 'This will be displayed as a link title when search engines display this products in their search results.',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'metaKeyword',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'meta-keyword',
                'placeholder' => 'META Keywords',
                'maxLength' => 160
            ),
            'options' => array(
                'label' => 'META Keywords (Maximum 160 characters)',
                'label_attributes' => array(
                    'for' => 'meta-keyword',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => 'A list of keywords used to indicate what the product is about',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'metaDescription',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'meta-description',
                'rows' => 3,
                'maxLength' => 160
            ),
            'options' => array(
                'label' => 'META Description (Maximum 160 characters)',
                'label_attributes' => array(
                    'for' => 'meta-description',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => 'Describes your page to search engines',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        //Create the form builder for product prices
        if (count($this->_currenciesPriceTypeMatrix) > 0) {
            foreach ($this->_currenciesPriceTypeMatrix as $currency => $priceTypes) {
                if (is_array($priceTypes) && count($priceTypes) > 0) {
                    foreach ($priceTypes as $priceTypeKey => $priceTypeVal) {
                        $priceTypeValue = $priceTypeVal['name'];
                        if (trim($priceTypeValue) != '') {
                            $elementName = $priceTypeKey . ucfirst(strtolower($currency));
                            $id = str_replace(' ', '-', strtolower($priceTypeValue));
                            $this->add(array(
                                'type' => 'Zend\Form\Element\Text',
                                'name' => $elementName,
                                'attributes' => array(
                                    'class' => 'form-control product-price',
                                    'id' => $id,
                                    'placeholder' => $priceTypeValue
                                ),
                            ));
                        }
                    }
                }
            }
        }

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));

        $this->add(array(
            'name' => 'createProductSaveAndClose',
            'attributes' => array(
                'id' => 'create-product-save-and-close',
                'type' => 'submit',
                'value' => 'Save and close',
                'class' => 'btn btn-success pull-right',
            ),
        ));

        $this->add(array(
            'name' => 'createProductSaveAndContinue',
            'attributes' => array(
                'id' => 'create-product-save-and-continue',
                'type' => 'submit',
                'value' => 'Save and continue',
                'class' => 'btn btn-success margin-left pull-right',
            ),
        ));

        $this->add(array(
            'name' => 'updateProductSaveAndClose',
            'attributes' => array(
                'id' => 'update-product-save-and-close',
                'type' => 'submit',
                'value' => 'Update and close',
                'class' => 'btn btn-success pull-right',
            ),
        ));

        $this->add(array(
            'name' => 'updateProductSaveAndContinue',
            'attributes' => array(
                'id' => 'update-product-save-and-continue',
                'type' => 'submit',
                'value' => 'Update and continue',
                'class' => 'btn btn-success margin-left pull-right',
            ),
        ));

        $this->add(array(
            'name' => 'closeModal',
            'attributes' => array(
                'id' => 'close-modal',
                'type' => 'button',
                'value' => 'Close',
                'data-dismiss' => 'modal',
                'class' => 'btn btn-default margin-left pull-right',
            ),
        ));
    }

    /**
     * Input filter for validation
     *
     * @return type
     */
    public function getInputFilterSpecification()
    {
        //Declare some validation array which will be added to the main validation array later
        //Reason for separating them: We don't want to validate them for edit process
        $styleValidationArray = array(
            'name' => 'style',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 30,
                    )
                ),
                array(
                    'name' => 'Zend\Validator\Db\NoRecordExists',
                    'options' => array(
                        'table' => 'PRODUCTS_R_SPECTRUM',
                        'field' => 'style',
                        'adapter' => $this->_dbAdapter,
                        'messages' => array(
                            \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'This style has been used.'
                        ),
                    ),
                )
            ),
        );

        $skuValidationArray = array(
            'name' => 'sku',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 30,
                    )
                ),
               array(
                    'name' => 'Regex',
                    'options' => array(
                       'pattern' => '/^[0-9a-zA-Z_-]{1,14}.$/', // New Expression, works & required sku to be between 1-14 chars long
                        // OLD - 'pattern' => '/^[a-zA-Z0-9-_]+$/',
                        'messages' => array(
                            \Zend\Validator\Regex::NOT_MATCH => 'The input can contain only alfa-numeric characters, - or _ , 1-14 character long'
                        ),
                    )
                ),
                array(
                    'name' => 'Zend\Validator\Db\NoRecordExists',
                    'options' => array(
                        'table' => 'PRODUCTS',
                        'field' => 'sku',
                        'exclude' => array(
                            'field' => 'status',
                            'value' => 'DELETED'
                        ),
                        'adapter' => $this->_dbAdapter,
                        'messages' => array(
                            \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'This sku has been used.'
                        ),
                    ),
                )
            ),
        );

        $urlValidationArray = array(
            'name' => 'urlName',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 180,
                    )
                ),
                array(
                    'name' => 'Zend\Validator\Db\NoRecordExists',
                    'options' => array(
                        'table' => 'PRODUCTS_Definitions',
                        'field' => 'urlName',
                        'adapter' => $this->_dbAdapter,
                        'exclude' => array(
                            'field' => 'status',
                            'value' => 'DELETED'
                        ),
                        'messages' => array(
                            \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'This url has been used.'
                        ),
                    ),
                ),
                array(
                    'name' => 'Regex',
                    'options' => array(
                        'pattern' => '/^[\w.-]*$/i',
                        'message' => array(
                            \Zend\Validator\Regex::INVALID => 'This field accepts only alphanumeric characters and hyphens',
                        ),
                    ),
                ),
            ),
        );

        $shortProductNameValidationArray = array(
            'name' => 'shortProductName',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
                array('name' => 'HtmlEntities')
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 30,
                        'messages' => array(
                            \Zend\Validator\StringLength::TOO_SHORT => 'This field expects minimum 3 characters',
                            \Zend\Validator\StringLength::TOO_LONG => 'This field expects maximum 30 characters',
                            \Zend\Validator\StringLength::INVALID => 'The length should be between 3 and 30 characters',
                        ),
                    ),
                ),
                array(
                    'name' => 'Zend\Validator\Db\NoRecordExists',
                    'options' => array(
                        'table' => 'PRODUCTS_Definitions',
                        'field' => 'shortProductName',
                        'adapter' => $this->_dbAdapter,
                        'exclude' => array(
                            'field' => 'status',
                            'value' => 'DELETED'
                        ),
                        'messages' => array(
                            \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'This short name has been used'
                        ),
                    ),
                ),
            ),
        );

        $productNameValidationArray = array(
            'name' => 'productName',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
                array('name' => 'HtmlEntities')
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 50,
                    ),
                ),
                array(
                    'name' => 'Zend\Validator\Db\NoRecordExists',
                    'options' => array(
                        'table' => 'PRODUCTS_Definitions',
                        'field' => 'productName',
                        'adapter' => $this->_dbAdapter,
                        'exclude' => array(
                            'field' => 'status',
                            'value' => 'DELETED'
                        ),
                        'messages' => array(
                            \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'A product with this name already exists'
                        ),
                    ),
                ),
            ),
        );

        $productBarcodeValidation = array(
            'name' => 'ean13',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'Barcode cannot be empty! Please upload more barcodes in the system!'
                        ),
                    ),
                ),
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 30,
                        'messages' => array(
                            \Zend\Validator\StringLength::TOO_SHORT => 'The barcode must be at least 2 characters long!',
                            \Zend\Validator\StringLength::TOO_LONG => 'The barcode must have maximum 30 characters!'
                        ),
                    )
                ),
                array(
                    'name' => 'Zend\Validator\Regex',
                    'options' => array(
                        'pattern' => '/^[a-zA-Z0-9]+$/',
                        'messages' => array(
                            \Zend\Validator\Regex::NOT_MATCH => 'The barcode must contain only letters and numbers!'
                        ),
                    ),
                ),
            )
        );

        //By default we want to validate these fields
        $validateStyle = true;
        $validateSku= true;
        $validateShortProductName = true;
        $validateProductName = true;
        $validateProductUrl = true;
        $validateProductBarcode = true;

        //Get the product id
        $productId = $this->get('productId')->getValue();
        $productTypeId = $this->get('productTypeId')->getValue();

        if (!empty($productTypeId) && $productTypeId == 3) {
            $validateProductBarcode = false;
        }
        //If product id > 0, it must be from an edit form
        //We don't want to validate matching fields (from form and database)
        if ($productId > 0) {

            //Get the values of the fields we want to check if we should validate
            $style = $this->get('style')->getValue();
            $sku = $this->get('sku')->getValue();
            $urlName = $this->get('urlName')->getValue();
            $shortProductName = $this->get('shortProductName')->getValue();
            $productName = $this->get('productName')->getValue();

            //Get the existing product data from the database.
            $existingProductData = $this->_productsMapper->getProductDetailsById($productId, $this->_languageId);

            //If style value submitted from the form matches the value from database, skip validation
            if (isset($existingProductData[$productId]['style']) && trim($existingProductData[$productId]['style']) != '' && trim($style) != '') {
                if ($existingProductData[$productId]['style'] == $style) {
                    $validateStyle = false;
                }
            }

            //If sku value submitted from the form matches the value from database, skip validation
            if (isset($existingProductData[$productId]['sku']) && trim($existingProductData[$productId]['sku']) != '' && trim($sku) != '') {
                if ($existingProductData[$productId]['sku'] == $sku) {
                    $validateSku = false;
                }
            }

            //If url value submitted from the form matches the value from database, skip validation
            if (isset($existingProductData[$productId]['urlName']) && trim($existingProductData[$productId]['urlName']) != '' && trim($urlName) != '') {
                if ($existingProductData[$productId]['urlName'] == $urlName) {
                    $validateProductUrl = false;
                }
            }

            //If short product name value submitted from the form matches the value from database, skip validation
            if (isset($existingProductData[$productId]['shortProductName']) && trim($existingProductData[$productId]['shortProductName']) != '' && trim($shortProductName) != '') {
                if (strtolower($existingProductData[$productId]['shortProductName']) == strtolower($shortProductName)) {
                    $validateShortProductName = false;
                }
            }

            //If product name value submitted from the form matches the value from database, skip validation
            if (isset($existingProductData[$productId]['productName']) && trim($existingProductData[$productId]['productName']) != '' && trim($productName) != '') {
                if (strtolower($existingProductData[$productId]['productName']) == strtolower($productName)) {
                    $validateProductName = false;
                }
            }

        }

        $return = array(
            array(
                'name' => 'supplierReference',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 32,
                        )
                    ),
                )
            ),
            array(
                'name' => 'metaTitle',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 180,
                        )
                    ),
                )
            ),
            array(
                'name' => 'metaKeyword',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 160,
                        )
                    ),
                )
            ),
            array(
                'name' => 'metaDescription',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 160,
                        )
                    ),
                )
            ),
            array(
                'name' => 'shortDescription',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 160,
                        )
                    ),
                )
            ),
            array(
                'name' => 'longDescription',
                'required' => false,
                'filters' => array(
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                        )
                    ),
                )
            ),
            array(
                'name' => 'tax',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                )
            ),
            array(
                'name' => 'websites',
                'required' => true,
            ),
            array(
                'name' => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            )
        );

        //if the product is an e-voucher we can skip barcode validation
        if ($validateProductBarcode) {
            $return[] = $productBarcodeValidation;
        }

        //if we are to validate style, add the style validation to the validation array
        if ($validateStyle === true) {
            $return[] = $styleValidationArray;
        }

        //if we are to validate short product name, add the short product name validation to the validation array
        if ($validateShortProductName === true) {
            $return[] = $shortProductNameValidationArray;
        }

        //if we are to validate product name, add the product name validation to the validation array
        if ($validateProductName === true) {
            $return[] = $productNameValidationArray;
        }

        if ($validateSku === true) {
            $return[] = $skuValidationArray;
        }

        if ($validateProductUrl === true) {
            $return[] = $urlValidationArray;
        }

        return $return;
    }

    /**
     * Set the event manager instance used by this context
     *
     * @param EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;

        return $this;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }

}
