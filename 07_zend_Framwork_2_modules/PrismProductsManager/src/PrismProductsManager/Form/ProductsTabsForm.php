<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;

class ProductsTabsForm extends Form implements InputFilterProviderInterface
{
    /**
      * @var EventManagerInterface
    */
    protected $events;

    public function __construct()
    {

        parent::__construct('products-tabs-form');
        $this->setAttributes(array(
            'name' => 'products-tabs-form',
            'role' => 'form',

        ));

        $this->add(array(
            'name' => 'productId',
            'type' => 'hidden'
        ));
        $this->add(array(
            'name' => 'productTabId',
            'type' => 'hidden'
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'product-tab-title',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'category-name',
                'placeholder' => 'Product group name',
                //'required' => 'required',
            ),
            'options' => array(
                'label' => 'Tab Title *',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'product-tab-content',
            'attributes' => array(
                'class' => 'form-control tinymce',
                'id' => 'product-tab-content',
                'rows' => 3
            ),
            'options' => array(
                'label' => 'Tab Content *',
                'label_attributes' => array(
                    'for' => 'product-tab-content',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => 'The full content for the tab',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'save-product-tab',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'id' => 'submit-button',
                'class' => 'btn btn-success ',
                'value' => 'Save'
            ),
        ));
    }

    public function init()
    {

    }

    public function getInputFilterSpecification()
    {
        return array(

            'product-tab-title' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'Zend\Filter\StringTrim'),
                ),
            ),
            array(
                'name' => 'product-tab-content',
                'required' => false,
                'filters' => array(
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                        )
                    ),
                )
            ),
            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),

        );
    }

    /**
     * Set the event manager instance used by this context
     *
     * @param EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;

        return $this;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }

}
