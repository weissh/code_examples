<?php

namespace PrismProductsManager\Form;

use PrismProductsManager\Entity\InputPages;
use PrismProductsManager\Model\ProductsMapper;
use Zend\Form\Form;
use Zend\EventManager\EventManagerInterface;
use Zend\InputFilter\InputFilterProviderInterface;


/**
 * Class InputFieldsForm
 * @package PrismProductsManager\Form
 */
class InputFieldsForm extends Form implements InputFilterProviderInterface
{
    /**
      * @var EventManagerInterface
    */
    protected $events;

    /**
     * @var array
     */
    protected $dynamicPageFields;
    /**
     * @var
     */
    protected $_websites;
    /**
     * @var
     */
    protected $_defaultWebsite;
    /**
     * @var
     */
    protected $_channels;
    /**
     * @var
     */
    protected $_manufacturer;
    /**
     * @var
     */
    protected $_currenciesPriceTypeMatrix;
    /**
     * @var
     */
    protected $_careInformation;
    /**
     * @var
     */
    protected $_suppliers;
    /**
     * @var
     */
    protected $_productsMapper;
    /**
     * @var
     */
    protected $_languageId;
    /**
     * @var array
     */
    protected $inputFilterSpecification = array();

    /**
     * @var
     */
    protected $style;

    /**
     * @var array
     */
    private $originalDynamicPageFields;
    /**
     * @var
     */
    protected $option;
    /**
     * @var boolean
     */
    protected $pricesFlag;

    protected $landedCostSettings;

    protected $nowPriceSettings;

    protected $wasPriceSettings;

    protected $supplierPriceSettings;
    /**
     * @var bool
     */
    protected $enableWorkflows ;

    /**
     * @var
     */
    protected $cachedOptionsData;

    /**
     * InputFieldsForm constructor.
     * @param null $serviceLocator
     * @param array $dynamicPageFields
     * @param ProductsMapper $productsMapper
     * @param $inputPagesRepository
     * @param $pageEntity
     * @param $style
     * @param $option
     * @param array $clientConfig
     * @param $cachedOptionsData
     */
    public function __construct(
        $serviceLocator,
        $dynamicPageFields,
        ProductsMapper $productsMapper,
        $inputPagesRepository,
        $pageEntity,
        $style,
        $option,
        array $clientConfig,
        $cachedOptionsData
    )
    {
        parent::__construct('product-creation-tabs');
        $this->_productsMapper = $productsMapper;
        $this->originalDynamicPageFields = $dynamicPageFields;
        $this->dynamicPageFields = $dynamicPageFields;
        $config = $serviceLocator->get('config');
        $this->_languageId = $config['siteConfig']['languages']['current-language'];
        $basicDetails = $this->_productsMapper->fetchBasicDetails();
        $this->_websites = $basicDetails['websites'];
        $this->_defaultWebsite = $basicDetails['defaultWebsite'];
        $this->_channels = $basicDetails['channels'];
        $this->_manufacturer = $basicDetails['manufacturers'];
        $this->_currenciesPriceTypeMatrix = $basicDetails['currenciesPriceTypeMatrix'];
        $this->_careInformation = $basicDetails['careInformation'];

        $this->_supplierCurrency = $this->_productsMapper->getSuppliersCurrency(true);
        $this->pageEntity = $pageEntity;
        $this->style = $style;
        $this->option = $option;

        $this->enableWorkflows = isset($config['clientConfig']['settings']['enableWorkflows']) ? $config['clientConfig']['settings']['enableWorkflows'] : true;

        $this->setAttribute('novalidate', 'novalidate');
        $this->cachedOptionsData = $cachedOptionsData;
        $this->optionsHaveDifferentPrices = $this->checkIfOptionsHaveDifferentPrices();

        $this->landedCostSettings = isset($clientConfig['settings']['landedCost']) ? $clientConfig['settings']['landedCost'] : false;
        $this->nowPriceSettings = isset($clientConfig['settings']['nowPrice']) ? $clientConfig['settings']['nowPrice'] : false;
        $this->wasPriceSettings = isset($clientConfig['settings']['wasPrice']) ? $clientConfig['settings']['wasPrice'] : false;
        $this->supplierPriceSettings = isset($clientConfig['settings']['supplierPrice']) ? $clientConfig['settings']['supplierPrice'] : false;
    }

    /**
     *
     */
    public function init()
    {
        $this->prepareElements($this->dynamicPageFields);
    }


    /**
     * @return array
     */
    public function checkIfOptionsHaveDifferentPrices()
    {
        if (!empty($this->style) && empty($this->option)) {
            //$options = $this->_productsMapper->getAllOptionsForStyle($this->style, true);
            $options = $this->getOptionsData($this->style);
            $pricesData = array();

            $stylePriceData = $this->_productsMapper->getProductPricesForStyleOrOption($this->style, null);

            foreach ($options as $optionCode => $optionData) {
                $pricesData[$optionCode] = $this->_productsMapper->getProductPricesForStyleOrOption($this->style, $optionCode);
            }

            $firstArray = current($pricesData);
            /**
             * This was used by Cristian where he set style price to first option
             * I don not know why ?
             *  $valuesDoNotMatch =  $this->loopArrayAndCheckOptions($firstArray, $pricesData, '', false);
             */
            $valuesDoNotMatch =  $this->loopArrayAndCheckOptions($stylePriceData, $pricesData, '', false);

            $returnArray = array();
            foreach ($valuesDoNotMatch as $values) {
                $arrayKey = isset($values[0]) ? $values[0] : false;
                 unset($values[0]);

                if (count($values) > 1) {
                    if (isset($values[1]) && isset($values[2]) && $arrayKey !== false) {
                        $returnArray[$arrayKey][$values[1]][$values[2]] = true;
                    }
                } else {
                    if (isset($values[1]) && $arrayKey !== false) {
                        $returnArray[$arrayKey][$values[1]] = true;
                    }
                }
            }
            return $returnArray;
        }
        return array();
    }

    /**
     * @param $loopArray
     * @param array $allPricesData
     * @param string $recursivePath
     * @param bool|false $recursive
     * @return array
     */
    private function loopArrayAndCheckOptions($loopArray, $allPricesData = array(), $recursivePath = '', $recursive = false)
    {
        $valuesDoNotMatch = array();
        if (!empty($loopArray)) {
            foreach ($loopArray as $pricePath => $valuesData) {

                if (is_array($valuesData)) {
                    $recursivePathForLoop = $recursivePath . '-' . $pricePath;
                    $valuesDoNotMatchFromLoop = $this->loopArrayAndCheckOptions($valuesData, $allPricesData, $recursivePathForLoop, true);
                    $valuesDoNotMatch = array_merge($valuesDoNotMatch, $valuesDoNotMatchFromLoop);
                } else {
                    $secondRecursivePath = $recursivePath . '-' . $pricePath;
                    $secondRecursivePath = trim($secondRecursivePath, '-');

                    foreach ($allPricesData as $option) {
                        $explodedRecursivePathArray = explode('-', $secondRecursivePath);
                        $currentOptionValue = null;
                        foreach ($explodedRecursivePathArray as $explodedRecursivePath) {

                            if (is_null($currentOptionValue)) {
                                $currentOptionValue = $option;
                            }
                            if (isset($currentOptionValue[$explodedRecursivePath])) {
                                $currentOptionValue = $currentOptionValue[$explodedRecursivePath];
                            }

                        }
                        if ($currentOptionValue != $valuesData) {
                            /** Values do not match for this option */
                            $valuesDoNotMatch[] = $explodedRecursivePathArray;
                        }
                    }

                }
            }
        }

        return $valuesDoNotMatch;
    }

    /**
     * @return array
     */
    public function getOriginalDynamicPageFields()
    {
        return $this->originalDynamicPageFields;
    }
    /**
     * @param $dynamicPageFields
     */
    public function setDynamicPageFields($dynamicPageFields) {
        $this->dynamicPageFields = $dynamicPageFields;
    }

    /**
     *
     */
    public function prepareElements()
    {
        $this->pricesFlag = false;
        /** Generate dynamic elements from InputService **/
        if (count($this->dynamicPageFields) > 0) {
            foreach ($this->dynamicPageFields as $dynamicPage) {
                foreach($dynamicPage as $pageName => $dynamicFieldSet){
                    foreach ($dynamicFieldSet as $inputField => $dynamicField) {
                        if ($inputField != 'isModule') {
                            if (is_array($dynamicField) && isset($dynamicField['inputType'])) {
                                switch ($dynamicField['inputType']) {
                                    case 'isAttribute':
                                        if (!isset($dynamicField['values'])) {
                                            $dynamicField['values'][1] = 1;
                                        }
                                        break;
                                }

                                $this->_addElementBasedOnType($dynamicField);
                                $this->addInputFieldSpecification($dynamicField);
                            }
                        } else {
                            if ($dynamicField->getInputModuleRef() == 'prices-module') {
                                $this->createPricesElements();
                                $this->createPricesInputSpecification();
                                $this->pricesFlag = true;
                            }
                        }
                    }
                }
            }
        }

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'style-level',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'option-level',
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'form-error',
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'options-to-copy',
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
    }


    /**
     * Set up prices input specification
     */
    public function createPricesInputSpecification()
    {
        $this->inputFilterSpecification[] = array(
            'name' => 'prices[supplier-price]',
            'required' => false ,
            'allow_empty' => true,
        );
        $this->inputFilterSpecification[] = array(
            'name' => 'prices[supplier-currency]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[costPrice-GBP]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[costPrice-USD]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[costPrice-CAD]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[costPrice-AUD]',
            'required' => false ,
            'allow_empty' => true,
        );


        $this->inputFilterSpecification[] = array(
            'name' => 'prices[costPrice-EUR]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[nowPrice-GBP-GB]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[nowPrice-GBP-ROW]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[nowPrice-GBP-EU]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[nowPrice-EUR-EU]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[nowPrice-EUR-DE]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[nowPrice-CAD-CA]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[nowPrice-USD-US]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[nowPrice-USD-GB]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[nowPrice-AUD-GB]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[wasPrice-GBP-GB]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[wasPrice-GBP-ROW]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[wasPrice-GBP-EU]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[wasPrice-EUR-EU]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[wasPrice-EUR-DE]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[wasPrice-USD-US]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[wasPrice-USD-GB]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[wasPrice-CAD-CA]',
            'required' => false ,
            'allow_empty' => true,
        );

        $this->inputFilterSpecification[] = array(
            'name' => 'prices[wasPrice-AUD-GB]',
            'required' => false ,
            'allow_empty' => true,
        );
    }

    /**
     *
     */
    private function createPricesElements()
    {
        /** CREATE PRICES ELEMENTS */
        if (isset($this->optionsHaveDifferentPrices['supplierPrice']['currencyCode'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[supplier-currency]',
                'type' => 'button',
                'attributes' => array(
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'supplier-currency',
                    'data-territory' => '',
                    'data-currency' => ''
                ),
                'options' => array(
                    'label' => 'Trading Currency' .  ($this->enableWorkflows ? '<small><span data-original-title="Mandatory for Ready for PO" data-toggle="tooltip" data-placement="top" title="" class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#ffff00"></span></small>' : ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Supplier currency */
            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'prices[supplier-currency]',
                'attributes' => array(
                    'class' => 'form-control',
                    'id' => 'supplier-reference',
                ),
                'options' => array(
                    'label' => 'Trading Currency' . ($this->enableWorkflows ? '<small><span data-original-title="Mandatory for Ready for PO" data-toggle="tooltip" data-placement="top" title="" class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#ffff00"></span></small>' : ''),
                    'label_attributes' => array(
                        'for' => 'prices[supplier-currency]',
                        'class' => 'moreInfo',
                        'data-container' => 'body',
                        'data-toggle' => 'popover',
                        'data-placement' => 'bottom',
                        'data-content' => 'The code which the supplier supplies this product.',
                        'data-trigger' => 'hover',
                        'data-html' => 'true'
                    ),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                    'value_options' => $this->_supplierCurrency,
                ),
            ));
        }
        if (isset($this->optionsHaveDifferentPrices['supplierPrice']['price'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[supplier-price]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'supplier-price',
                    'data-territory' => '',
                    'data-currency' => ''
                ),
                'options' => array(
                    'label' => 'Trading Currency Cost' . ($this->enableWorkflows ? '<small><span data-original-title="Mandatory for Ready for PO" data-toggle="tooltip" data-placement="top" title="" class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#ffff00"></span></small>' : ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Supplier Price */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[supplier-price]',
                'attributes' => array(
                    'class' => 'form-control',
                    'min' => isset($this->supplierPriceSettings['min']) ? $this->supplierPriceSettings['min'] : '0',
                    'max' => isset($this->supplierPriceSettings['max']) ? $this->supplierPriceSettings['max'] : '1999',
                    'step' => '0.01',
                    'id' => 'supplierPrice',
                    'placeholder' => 'Amount',
                    'maxLength' => 10
                ),
                'options' => array(
                    'label' => 'Trading Currency Cost' . ($this->enableWorkflows ? '<small><span data-original-title="Mandatory for Ready for PO" data-toggle="tooltip" data-placement="top" title="" class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#ffff00"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                    'label_attributes' => array(
                        'for' => 'prices[supplier-price]',
                        'class' => 'moreInfo',
                        'data-container' => 'body',
                        'data-toggle' => 'popover',
                        'data-placement' => 'bottom',
                        'data-content' => '<p>Please add the product supplier price</p>',
                        'data-trigger' => 'hover',
                        'data-html' => 'true'
                    ),
                ),
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['costPrice']['GBP'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[costPrice-GBP]',
                'type' => 'button',
                'attributes' => array(
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'costPrice',
                    'data-territory' => '',
                    'data-currency' => 'GBP'
                ),
                'options' => array(
                    'label' => 'Landed Cost (GBP)' . ($this->enableWorkflows ? '<small><span data-original-title="Mandatory for Ready for PO" data-toggle="tooltip" data-placement="top" title="" class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#ffff00"></span></small>' : ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Landed Cost (GBP) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[costPrice-GBP]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Landed Cost (GBP)' . ($this->enableWorkflows ? '<small><span data-original-title="Mandatory for Ready for PO" data-toggle="tooltip" data-placement="top" title="" class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#ffff00"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    )
                ),
                'attributes' => array(
                    'min' => isset($this->landedCostSettings['GBP']['min']) ? $this->landedCostSettings['GBP']['min'] : '0',
                    'max' => isset($this->landedCostSettings['GBP']['max']) ? $this->landedCostSettings['GBP']['max'] : '199',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['costPrice']['USD'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[costPrice-USD]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'costPrice',
                    'data-territory' => '',
                    'data-currency' => 'USD'
                ),
                'options' => array(
                    'label' => 'Landed Cost (USD)' . ($this->enableWorkflows ? '<small><span data-original-title="Mandatory for Ready for PO" data-toggle="tooltip" data-placement="top" title="" class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#ffff00"></span></small>' : ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Landed Cost (USD) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[costPrice-USD]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Landed Cost (USD)' . ($this->enableWorkflows ? '<small><span data-original-title="Mandatory for Ready for PO" data-toggle="tooltip" data-placement="top" title="" class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#ffff00"></span></small>' : ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->landedCostSettings['USD']['min']) ? $this->landedCostSettings['USD']['min'] : '0',
                    'max' => isset($this->landedCostSettings['USD']['max']) ? $this->landedCostSettings['USD']['max'] : '499',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }
        if (isset($this->optionsHaveDifferentPrices['costPrice']['CAD'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[costPrice-CAD]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'costPrice',
                    'data-territory' => '',
                    'data-currency' => 'CAD'
                ),
                'options' => array(
                    'label' => 'Landed Cost (CAD)' . ($this->enableWorkflows ? '<small><span data-original-title="Mandatory for Ready for PO" data-toggle="tooltip" data-placement="top" title="" class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#ffff00"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Landed Cost (CAD) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[costPrice-CAD]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Landed Cost (CAD)' . ($this->enableWorkflows ? '<small><span data-original-title="Mandatory for Ready for PO" data-toggle="tooltip" data-placement="top" title="" class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#ffff00"></span></small>' : ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->landedCostSettings['CAD']['min']) ? $this->landedCostSettings['CAD']['min'] : '0',
                    'max' => isset($this->landedCostSettings['CAD']['max']) ? $this->landedCostSettings['CAD']['max'] : '499',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['costPrice']['AUD'])) {
            #Add the button element in case the options do not match
            $this->add(array(
                'name' => 'prices[costPrice-AUD]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'costPrice',
                    'data-territory' => '',
                    'data-currency' => 'AUD'
                ),
                'options' => array(
                    'label' => 'Landed Cost (AUD)',
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            # Landed Cost (AUD)
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[costPrice-AUD]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Landed Cost (AUD)',
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                    'label_attributes' => array(
                        'class'  => 'hide'
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->landedCostSettings['AUD']['min']) ? $this->landedCostSettings['AUD']['min'] : '0',
                    'max' => isset($this->landedCostSettings['AUD']['max']) ? $this->landedCostSettings['AUD']['max'] : '499',
                    'step' => '0.01',
                    'class' => 'hide form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['costPrice']['EUR'])) {
            # Add the button element in case the options do not match
            $this->add(array(
                'name' => 'prices[costPrice-EUR]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'costPrice',
                    'data-territory' => '',
                    'data-currency' => 'EUR'
                ),
                'options' => array(
                    'label' => 'Landed Cost (EUR)',
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                    'label_attributes' => array(
                        'class'  => 'hide'
                    ),
                ),
            ));
        } else {
            # Landed Cost (EUR)
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[costPrice-EUR]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Landed Cost (EUR)',
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                    'label_attributes' => array(
                        'class'  => 'hide'
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->landedCostSettings['EUR']['min']) ? $this->landedCostSettings['EUR']['min'] : '0',
                    'max' => isset($this->landedCostSettings['EUR']['max']) ? $this->landedCostSettings['EUR']['max'] : '499',
                    'step' => '0.01',
                    'class' => 'hide form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }
        if (isset($this->optionsHaveDifferentPrices['nowPrice']['GBP']['GB'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[nowPrice-GBP-GB]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'nowPrice',
                    'data-territory' => 'GB',
                    'data-currency' => 'GBP'
                ),
                'options' => array(
                    'label' => 'Current Price (GBP-GB)' .  ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>' : ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Current Price (GBP-GB) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[nowPrice-GBP-GB]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Current Price (GBP-GB)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),

                'attributes' => array(
                    'min' => isset($this->nowPriceSettings['GBP-GB']['min']) ? $this->nowPriceSettings['GBP-GB']['min'] : '0',
                    'max' => isset($this->nowPriceSettings['GBP-GB']['max']) ? $this->nowPriceSettings['GBP-GB']['max'] : '999',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }
        if (isset($this->optionsHaveDifferentPrices['nowPrice']['GBP']['ROW'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[nowPrice-GBP-ROW]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'nowPrice',
                    'data-territory' => 'ROW',
                    'data-currency' => 'GBP'
                ),
                'options' => array(
                    'label' => 'Current Price (GBP-ROW)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Current Price (GBP-ROW) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[nowPrice-GBP-ROW]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Current Price (GBP-ROW)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->nowPriceSettings['GBP-ROW']['min']) ? $this->nowPriceSettings['GBP-ROW']['min'] : '0',
                    'max' => isset($this->nowPriceSettings['GBP-ROW']['max']) ? $this->nowPriceSettings['GBP-ROW']['max'] : '999',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }
        if (isset($this->optionsHaveDifferentPrices['nowPrice']['GBP']['EU'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[nowPrice-GBP-EU]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'nowPrice',
                    'data-territory' => 'EU',
                    'data-currency' => 'GBP'
                ),
                'options' => array(
                    'label' => 'Current Price (GBP-EU)' .  ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>' : ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Current Price (GBP-EU) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[nowPrice-GBP-EU]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Current Price (GBP-EU)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),

                'attributes' => array(
                    'min' => isset($this->nowPriceSettings['GBP-EU']['min']) ? $this->nowPriceSettings['GBP-EU']['min'] : '0',
                    'max' => isset($this->nowPriceSettings['GBP-EU']['max']) ? $this->nowPriceSettings['GBP-EU']['max'] : '999',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }
        if (isset($this->optionsHaveDifferentPrices['nowPrice']['EUR']['EU'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[nowPrice-EUR-EU]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'nowPrice',
                    'data-territory' => 'EU',
                    'data-currency' => 'EUR'
                ),
                'options' => array(
                    'label' => 'Current Price (EUR-EU)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Current Price (EUR-EU) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[nowPrice-EUR-EU]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Current Price (EUR-EU)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->nowPriceSettings['EUR-EU']['min']) ? $this->nowPriceSettings['EUR-EU']['min'] : '0',
                    'max' => isset($this->nowPriceSettings['EUR-EU']['max']) ? $this->nowPriceSettings['EUR-EU']['max'] : '1599',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }
        if (isset($this->optionsHaveDifferentPrices['nowPrice']['EUR']['DE'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[nowPrice-EUR-DE]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'nowPrice',
                    'data-territory' => 'DE',
                    'data-currency' => 'EUR'
                ),
                'options' => array(
                    'label' => 'Current Price (EUR-DE)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Current Price (EUR-DE) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[nowPrice-EUR-DE]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Current Price (EUR-DE)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->nowPriceSettings['EUR-DE']['min']) ? $this->nowPriceSettings['EUR-DE']['min'] : '0',
                    'max' => isset($this->nowPriceSettings['EUR-DE']['max']) ? $this->nowPriceSettings['EUR-DE']['max'] : '1599',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['nowPrice']['USD']['US'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[nowPrice-USD-US]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'nowPrice',
                    'data-territory' => 'US',
                    'data-currency' => 'USD'
                ),
                'options' => array(
                    'label' => 'Current Price (USD-US)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Current Price (USD-US) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[nowPrice-USD-US]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Current Price (USD-US)' . ($this->enableWorkflows ?  '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->nowPriceSettings['USD-US']['min']) ? $this->nowPriceSettings['USD-US']['min'] : '0',
                    'max' => isset($this->nowPriceSettings['USD-US']['max']) ? $this->nowPriceSettings['USD-US']['max'] : '1599',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['nowPrice']['USD']['GB'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[nowPrice-USD-GB]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'nowPrice',
                    'data-territory' => 'GB',
                    'data-currency' => 'USD'
                ),
                'options' => array(
                    'label' => 'Current Price (USD-GB)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Current Price (USD-GB) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[nowPrice-USD-GB]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Current Price (USD-GB)' . ($this->enableWorkflows ?  '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->nowPriceSettings['USD-GB']['min']) ? $this->nowPriceSettings['USD-GB']['min'] : '0',
                    'max' => isset($this->nowPriceSettings['USD-GB']['max']) ? $this->nowPriceSettings['USD-GB']['max'] : '1599',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }


        if (isset($this->optionsHaveDifferentPrices['nowPrice']['CAD']['CA'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[nowPrice-CAD-CA]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'nowPrice',
                    'data-territory' => 'CA',
                    'data-currency' => 'CAD'
                ),
                'options' => array(
                    'label' => 'Current Price (CAD-CA)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Current Price (CAD-CA) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[nowPrice-CAD-CA]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Current Price (CAD-CA)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->nowPriceSettings['CAD-CA']['min']) ? $this->nowPriceSettings['CAD-CA']['min'] : '0',
                    'max' => isset($this->nowPriceSettings['CAD-CA']['max']) ? $this->nowPriceSettings['CAD-CA']['max'] : '1599',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['nowPrice']['AUD']['GB'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[nowPrice-AUD-GB]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'nowPrice',
                    'data-territory' => 'GB',
                    'data-currency' => 'AUD'
                ),
                'options' => array(
                    'label' => 'Current Price (AUD-GB)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Current Price (AUD-GB) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[nowPrice-AUD-GB]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Current Price (AUD-GB)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->nowPriceSettings['AUD-GB']['min']) ? $this->nowPriceSettings['AUD-GB']['min'] : '0',
                    'max' => isset($this->nowPriceSettings['AUD-GB']['max']) ? $this->nowPriceSettings['AUD-GB']['max'] : '1599',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['wasPrice']['GBP']['GB'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[wasPrice-GBP-GB]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'wasPrice',
                    'data-territory' => 'GB',
                    'data-currency' => 'GBP'
                ),
                'options' => array(
                    'label' => 'Original Price (GBP-GB)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Original Price (GBP-GB) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[wasPrice-GBP-GB]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Original Price (GBP-GB)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->wasPriceSettings['GBP-GB']['min']) ? $this->wasPriceSettings['GBP-GB']['min'] : '0',
                    'max' => isset($this->wasPriceSettings['GBP-GB']['max']) ? $this->wasPriceSettings['GBP-GB']['max'] : '999',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['wasPrice']['USD']['US'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[wasPrice-USD-US]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'wasPrice',
                    'data-territory' => 'US',
                    'data-currency' => 'USD'
                ),
                'options' => array(
                    'label' => 'Original Price (USD-US)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Original Price (USD-US) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[wasPrice-USD-US]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Original Price (USD-US)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->wasPriceSettings['USD-US']['min']) ? $this->wasPriceSettings['USD-US']['min'] : '0',
                    'max' => isset($this->wasPriceSettings['USD-US']['max']) ? $this->wasPriceSettings['USD-US']['max'] : '999',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['wasPrice']['USD']['GB'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[wasPrice-USD-GB]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'wasPrice',
                    'data-territory' => 'GB',
                    'data-currency' => 'USD'
                ),
                'options' => array(
                    'label' => 'Original Price (USD-GB)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Original Price (USD-GB) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[wasPrice-USD-GB]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Original Price (USD-GB)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->wasPriceSettings['USD-GB']['min']) ? $this->wasPriceSettings['USD-GB']['min'] : '0',
                    'max' => isset($this->wasPriceSettings['USD-GB']['max']) ? $this->wasPriceSettings['USD-GB']['max'] : '999',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['wasPrice']['GBP']['ROW'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[wasPrice-GBP-ROW]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'wasPrice',
                    'data-territory' => 'ROW',
                    'data-currency' => 'GBP'
                ),
                'options' => array(
                    'label' => 'Original Price (GBP-ROW)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Original Price (GBP-ROW) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[wasPrice-GBP-ROW]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Original Price (GBP-ROW)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->wasPriceSettings['GBP-ROW']['min']) ? $this->wasPriceSettings['GBP-ROW']['min'] : '0',
                    'max' => isset($this->wasPriceSettings['GBP-ROW']['max']) ? $this->wasPriceSettings['GBP-ROW']['max'] : '999',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['wasPrice']['GBP']['EU'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[wasPrice-GBP-EU]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'wasPrice',
                    'data-territory' => 'EU',
                    'data-currency' => 'GBP'
                ),
                'options' => array(
                    'label' => 'Original Price (GBP-EU)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Original Price (GBP-EU) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[wasPrice-GBP-EU]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Original Price (GBP-EU)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->wasPriceSettings['GBP-EU']['min']) ? $this->wasPriceSettings['GBP-EU']['min'] : '0',
                    'max' => isset($this->wasPriceSettings['GBP-EU']['max']) ? $this->wasPriceSettings['GBP-EU']['max'] : '999',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['wasPrice']['EUR']['EU'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[wasPrice-EUR-EU]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'wasPrice',
                    'data-territory' => 'EU',
                    'data-currency' => 'EUR'
                ),
                'options' => array(
                    'label' => 'Original Price (EUR-EU)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Original Price (EUR-EU) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[wasPrice-EUR-EU]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Original Price (EUR-EU)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->wasPriceSettings['EUR-EU']['min']) ? $this->wasPriceSettings['EUR-EU']['min'] : '0',
                    'max' => isset($this->wasPriceSettings['EUR-EU']['max']) ? $this->wasPriceSettings['EUR-EU']['max'] : '1599',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['wasPrice']['EUR']['DE'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[wasPrice-EUR-DE]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'wasPrice',
                    'data-territory' => 'DE',
                    'data-currency' => 'EUR'
                ),
                'options' => array(
                    'label' => 'Original Price (EUR-DE)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Original Price (EUR-DE) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[wasPrice-EUR-DE]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Original Price (EUR-DE)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->wasPriceSettings['EUR-DE']['min']) ? $this->wasPriceSettings['EUR-DE']['min'] : '0',
                    'max' => isset($this->wasPriceSettings['EUR-DE']['max']) ? $this->wasPriceSettings['EUR-DE']['max'] : '1599',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['wasPrice']['CAD']['CA'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[wasPrice-CAD-CA]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'wasPrice',
                    'data-territory' => 'CA',
                    'data-currency' => 'CAD'
                ),
                'options' => array(
                    'label' => 'Original Price (CAD-CA)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {

            /** Original Price (CAD-CA) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[wasPrice-CAD-CA]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Original Price (CAD-CA)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->wasPriceSettings['CAD-CA']['min']) ? $this->wasPriceSettings['CAD-CA']['min'] : '0',
                    'max' => isset($this->wasPriceSettings['CAD-CA']['max']) ? $this->wasPriceSettings['CAD-CA']['max'] : '1599',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

        if (isset($this->optionsHaveDifferentPrices['wasPrice']['AUD']['GB'])) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => 'prices[wasPrice-AUD-GB]',
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values-for-prices',
                    'data-price-type' => 'wasPrice',
                    'data-territory' => 'GB',
                    'data-currency' => 'AUD'
                ),
                'options' => array(
                    'label' => 'Original Price (AUD-GB)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'required' => false,
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
            ));
        } else {
            /** Original Price (AUD-GB) */
            $this->add(array(
                'type' => 'Zend\Form\Element\Number',
                'name' => 'prices[wasPrice-AUD-GB]',
                'required' => false,
                'allow_empty' => true,
                'options' => array(
                    'label' => 'Original Price (AUD-GB)' . ($this->enableWorkflows ? '<small><span class="glyphicon glyphicon-asterisk mandatory-workflow" style="font-size: 0.8em;top:-5px;color:#0066ff"></span></small>': ''),
                    'label_options' => array(
                        'disable_html_escape' => true,
                    ),
                ),
                'attributes' => array(
                    'min' => isset($this->wasPriceSettings['AUD-GB']['min']) ? $this->wasPriceSettings['AUD-GB']['min'] : '0',
                    'max' => isset($this->wasPriceSettings['AUD-GB']['max']) ? $this->wasPriceSettings['AUD-GB']['max'] : '1599',
                    'step' => '0.01',
                    'class' => 'form-control',
                    'placeholder' => 'Amount',
                )
            ));
        }

    }
    /**
     * @param $formElement
     */
    private function _addElementBasedOnType($formElement)
    {

        $elementName = $formElement['name'];
        $label = $formElement['label'];
        $type = $formElement['type'];
        $chain = !empty($formElement['values-chain']) ? $formElement['values-chain'] : array();

        $autoFromDateFlag = false;
        $inputFieldAttributesField = $formElement['inputFieldId']->getinputFieldRAttributesInputfieldrattributeid();
        if (!empty($inputFieldAttributesField)) {
            $commonAttribute = $inputFieldAttributesField->getCommonAttributes();
            if (!empty($commonAttribute)) {
                $autoFromDateFlag = $commonAttribute->isAutomaticFromDate();
            }
        }

        if (isset($formElement['optionsHaveDifferentValues']) && $formElement['optionsHaveDifferentValues']) {
            /** Add the button element in case the options do not match */
            $this->add(array(
                'name' => $elementName,
                'type' => 'button',
                'attributes' => array (
                    'class' => 'btn btn-primary get-all-option-values',
                    'data-input-field' => $formElement['inputFieldId']->getInputFieldId(),
                    'data-chain-allowed-values' => '',
                ),
                'options' => array(
                    'label' => $label,
                    'required' => false,
                ),
            ));
        } else {
            switch ($type) {
                case 'checkbox':
                    $this->add(array(
                        'type' => 'Zend\Form\Element\Checkbox',
                        'name' => $elementName,
                        'required' => false,
                        'allow_empty' => true,
                        'options' => array(
                            'label' => $label,
                            'use_hidden_element' => true,
                            'checked_value' => 1,
                            'unchecked_value' => 0
                        ),
                        'attributes' => array(
                            'data-chain' => json_encode($chain),
                            'required' => false,
                            'disabled' => !empty($this->option) && $formElement['inputFieldId']->getEditableAtLevel() == 'STYLE' ? true : false,
                            'data-default-checked' => !empty($formElement['tickedByDefault']) ? $formElement['tickedByDefault'] : 0
                        )
                    ));
                    break;
                case 'text':
                    $this->add(array(
                        'type' => 'Zend\Form\Element\Text',
                        'name' => $elementName,
                        'required' => false,
                        'allow_empty' => true,
                        'attributes' => array(
                            'class' => 'form-control chain-selector' ,
                            'id' => $elementName,
                            'placeholder' => $label,
                            'data-chain' => json_encode($chain),
                            'required' => false,
                            'readonly' => !empty($this->option) && $formElement['inputFieldId']->getEditableAtLevel() == 'STYLE' ? true : false
                        ),
                        'options' => array(
                            'label' => $label,
                            'label_attributes' => array(
                                'for' => $elementName
                            ),
                        ),
                    ));
                    break;
                case 'textarea':
                    $this->add(array(
                        'type' => 'Zend\Form\Element\Textarea',
                        'name' => $elementName,
                        'required' => false,
                        'allow_empty' => true,
                        'attributes' => array(
                            'class' => 'form-control chain-selector' ,
                            'id' => $elementName,
                            'placeholder' => $label,
                            'data-chain' => json_encode($chain),
                            'required' => false,
                            'rows' => 10,
                            'disabled' => !empty($this->option) && $formElement['inputFieldId']->getEditableAtLevel() == 'STYLE' ? true : false
                        ),
                        'options' => array(
                            'label' => $label,
                            'label_attributes' => array(
                                'for' => $elementName
                            ),
                        ),
                    ));
                    break;
                case 'datepicker':
                    $this->add(array(
                        'type' => 'Zend\Form\Element\Text',
                        'name' => $elementName,
                        'required' => false,
                        'allow_empty' => true,
                        'attributes' => array(
                            'class' => 'form-control chain-selector' ,
                            'id' => $elementName,
                            'placeholder' => $label,
                            'data-chain' => json_encode($chain),
                            'required' => false,
                            'disabled' => !empty($this->option) && $formElement['inputFieldId']->getEditableAtLevel() == 'STYLE' ? true : false
                        ),
                        'options' => array(
                            'label' => $label,
                            'label_attributes' => array(
                                'for' => $elementName
                            ),
                        ),
                    ));
                    break;
                case 'datepicker - date only':
                    $this->add(array(
                        'type' => 'Zend\Form\Element\Text',
                        'name' => $elementName,
                        'required' => false,
                        'allow_empty' => true,
                        'attributes' => array(
                            'class' => 'form-control chain-selector publish-date-only',
                            'id' => $elementName,
                            'placeholder' => $label,
                            'data-chain' => json_encode($chain),
                            'required' => false,
                            'disabled' => !empty($this->option) && $formElement['inputFieldId']->getEditableAtLevel() == 'STYLE' ? true : false
                        ),
                        'options' => array(
                            'label' => $label,
                            'label_attributes' => array(
                                'for' => $elementName
                            ),
                        ),
                    ));
                    break;
                case 'button':
                    $this->add(array(
                        'name' => $elementName,
                        'required' => false,
                        'allow_empty' => true,
                        'attributes' => array(
                            'type' => 'button',
                            'value' => $label,
                            'class' => 'btn btn-primary',
                            'required' => false,
                            'disabled' => !empty($this->option) && $formElement['inputFieldId']->getEditableAtLevel() == 'STYLE' ? true : false
                        ),
                    ));
                    break;
                case 'select':
                    if (count($formElement['values']) > 0) {
                        $this->add(array(
                            'type' => 'Zend\Form\Element\Select',
                            'name' => $elementName,
                            'required' => false,
                            'allow_empty' => true,
                            'attributes' => array(
                                'class' => 'selectize selectized chain-selector',
                                'id' => $elementName,
                                'data-chain' => json_encode($chain),
                                'required' => false,
                                'disabled' => !empty($this->option) && $formElement['inputFieldId']->getEditableAtLevel() == 'STYLE' ? true : false,
                                'value' => (isset($formElement['defaultValue']) && !empty($formElement['defaultValue']) ) ? $formElement['defaultValue'] : '',
                                'data-auto-from-date-flag' => $autoFromDateFlag,
                            ),
                            'options' => array(
                                'label' => $label,
                                'label_attributes' => array(
                                    'for' => $elementName
                                ),
                                'value_options' => $formElement['values'],
                            ),
                        ));
                    }
                    break;
                case 'multiselect':
                    if (count($formElement['values']) > 0) {
                        $this->add(array(
                            'type' => 'Zend\Form\Element\Select',
                            'name' => $elementName,
                            'required' => false,
                            'allow_empty' => true,
                            'attributes' => array(
                                'class' => 'chain-selector form-control multiselect-selector',
                                'id' => $elementName,
                                'data-chain' => json_encode($chain),
                                'required' => false,
                                'multiple' => 'multiple',
                                'disabled' => !empty($this->option) && $formElement['inputFieldId']->getEditableAtLevel() == 'STYLE' ? true : false,
                                'value' => (isset($formElement['defaultValue']) && !empty($formElement['defaultValue']) ) ? $formElement['defaultValue'] : '',
                            ),
                            'options' => array(
                                'label' => $label,
                                'label_attributes' => array(
                                    'for' => $elementName
                                ),
                                'value_options' => $formElement['values'],
                            ),
                        ));
                    }
                    break;
                case 'radio':
                    if (count($formElement['values']) > 0) {
                        $this->add(array(
                            'type' => 'Zend\Form\Element\Radio',
                            'name' => $elementName,
                            'required' => false,
                            'allow_empty' => true,
                            'attributes' => array(
                                'class' => 'radio-inline',
                                'data-chain' => json_encode($chain),
                                'required' => false,
                                'disabled' => !empty($this->option) && $formElement['inputFieldId']->getEditableAtLevel() == 'STYLE' ? true : false
                            ),
                            'options' => array(
                                'label' => $label,
                                'value_options' => $formElement['values'],
                            )
                        ));
                    }
                    break;
                case 'product':
                    $this->add(array(
                        'type' => 'Zend\Form\Element\Select',
                        'name' => $elementName,
                        'required' => false,
                        'allow_empty' => true,
                        'attributes' => array(
                            'class' => 'chain-selector product-search-input',
                            'id' => $elementName,
                            'data-chain' => json_encode($chain),
                            'required' => false,
                            'multiple' => 'multiple',
                            'disabled' => !empty($this->option) && $formElement['inputFieldId']->getEditableAtLevel() == 'STYLE' ? true : false
                        ),
                        'options' => array(
                            'label' => $label .  "\n(Enter first 3 SKU chars)",
                            'label_attributes' => array(
                                'for' => $elementName
                            ),
                            'value_options' => array(),
                        ),
                    ));
                    break;
                case 'hidden':
                    $this->add(array(
                        'type' => 'Zend\Form\Element\Hidden',
                        'name' => $elementName,
                        'required' => false,
                        'allow_empty' => true,
                        'attributes' => array(
                            'value' => 0
                        )
                    ));
                    break;
                default:
                    break;
            }
        }

    }

    /**
     * Set the chained values for pop up window when option values are not same
     * @return $this
     */
    public function setAllowedChainData()
    {
        $elements = $this->getElements();
        foreach ($elements as $elementKey => $elementValue){
             if($elementValue instanceof  \Zend\Form\Element\Button){
                $attributes = $elementValue->getAttributes();
                if(array_key_exists('data-chain-allowed-values', $attributes)){
                    //Set value of all parent allowed values
                    $this->getAllowedChainData($attributes['data-input-field']);
                    $allowedValues = $this->getAllowedChainData($attributes['data-input-field']);
                    $elementValue->setAttribute('data-chain-allowed-values', $allowedValues );
                }
            }
       }
        return $this;
    }

    /**
     * Get chaind values from parent
     * @param int $dataInputField
     * @return string
     */
    public function getAllowedChainData($dataInputField)
    {
        $allowedValue = [];
        $elements = $this->getElements();
        foreach ($elements as $elementKey => $elementValue){
            if($elementValue instanceof  \Zend\Form\Element\Select){
                $attributes = $elementValue->getAttributes();
                if(array_key_exists('data-chain', $attributes)){
                    //Set value of all parent allowed values
                    $allValues = json_decode($attributes['data-chain'], true);
                    if(!empty($allValues)){
                        $setValue = $elementValue->getValue(); // value set as selected value in form
                        $allowedValues = isset($allValues[$setValue][$dataInputField]) ? $allValues[$setValue][$dataInputField] : null;
                        if(!empty($allowedValues))
                        {
                            foreach ($allowedValues as $value){
                                $allowedValue[] = $value['valueAvailable'];
                            }
                            return json_encode($allowedValue);
                        }
                     }
                }
            }
            // reset chain value
            $allowedValue = [];
        }
    }


    /**
     * @param $submitedFormData
     * @return bool
     */
    public function hydrateData($submitedFormData)
    {
        try {
            if (!empty($submitedFormData['inputfield'])) {
                foreach ($submitedFormData['inputfield'] as $inputFieldId => $inputFieldValue) {
                    $this->get('inputfield['.$inputFieldId.']')->setValue($inputFieldValue);
                }
            }
            return true;
        } catch (\Exception $ex) {
            //var_dump($ex->getMessage());
            return false;
        }
    }

    /**
     * @param $dynamicField
     */
    private function addInputFieldSpecification($dynamicField)
    {
        if (
            isset($dynamicField['validations']) &&
            isset($dynamicField['validations']['type']) &&
            strpos($dynamicField['name'], 'button-') === false
        ) {

            switch($dynamicField['validations']['type']) {
                case 'Int':
                    $validationArray = array(
                        'name' => $dynamicField['name'],
                        'required' => isset($dynamicField['validations']['required']) && empty($this->option) ? $dynamicField['validations']['required'] : false ,
                        'allow_empty' => isset($dynamicField['validations']['required']) && empty($this->option) ? !$dynamicField['validations']['required'] : true ,
                        'filters' => array(
                            array('name' => 'StringTrim'),
                        )
                    );
                    $validationArray['validators'][] = array(
                        'name' => 'Int'
                    );

                    if (
                        !empty($dynamicField['validations']['max']) ||
                        (!empty($dynamicField['validations']['max']) && !empty($dynamicField['validations']['min']))
                    ) {
                        $validationArray['validators'][] = array(
                            'name' => 'Zend\Validator\Between',
                            'options' => array(
                                'min' => !empty($dynamicField['validations']['min']) ? $dynamicField['validations']['min'] : 0,
                                'max' => !empty($dynamicField['validations']['max']) ? $dynamicField['validations']['max'] : false,
                            ),
                        );
                    }

                    break;
                case 'Decimal 5.1':
                    $validationArray = array(
                        'name' => $dynamicField['name'],
                        'required' => isset($dynamicField['validations']['required']) && empty($this->option) ? $dynamicField['validations']['required'] : false ,
                        'allow_empty' => isset($dynamicField['validations']['required']) && empty($this->option) ? !$dynamicField['validations']['required'] : true ,
                        'filters' => array(
                            array('name' => 'StringTrim'),
                        ),
                    );

                    $validationArray['validators'][] = array(
                        'name'    => 'Float',
                        'options' =>  array(
                            'locale' => 'en_GB'
                        )
                    );

                    if (
                        !empty($dynamicField['validations']['max']) ||
                        (!empty($dynamicField['validations']['max']) && !empty($dynamicField['validations']['min']))
                    ) {
                        $validationArray['validators'][] =  array(
                            'name'    => 'Zend\Validator\Between',
                            'options' =>  array(
                                'min' => !empty($dynamicField['validations']['min']) ? $dynamicField['validations']['min'] : 0,
                                'max' => !empty($dynamicField['validations']['max']) ? $dynamicField['validations']['max'] : false,
                            )
                        );
                    }
                    break;
                case 'Varchar':
                    $validationArray = array(
                        'name' => $dynamicField['name'],
                        'required' => isset($dynamicField['validations']['required']) && empty($this->option) ? $dynamicField['validations']['required'] : false ,
                        'allow_empty' => isset($dynamicField['validations']['required']) && empty($this->option) ? !$dynamicField['validations']['required'] : true ,
                        'filters' => array(
                            array('name' => 'StringTrim'),
                        ),
                    );

                    if (
                        !empty($dynamicField['validations']['max']) ||
                        (!empty($dynamicField['validations']['max']) && !empty($dynamicField['validations']['min']))
                    ) {
                        $validationArray['validators'] = array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => !empty($dynamicField['validations']['min']) ? $dynamicField['validations']['min'] : 0,
                                    'max' => !empty($dynamicField['validations']['max']) ? $dynamicField['validations']['max'] : 0,
                                ),
                            ),
                        );
                    }
                    break;
                case 'Regex':
                    $validationArray = array(
                        'name' => $dynamicField['name'],
                        'required' => isset($dynamicField['validations']['required']) && empty($this->option) ? $dynamicField['validations']['required'] : false ,
                        'allow_empty' => isset($dynamicField['validations']['required']) && empty($this->option) ? !$dynamicField['validations']['required'] : true ,
                        'filters' => array(
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(),
                    );
                    break;
                default:
                    $validationArray = array(
                        'name' => $dynamicField['name'],
                        'required' => isset($dynamicField['validations']['required']) && empty($this->option) ? $dynamicField['validations']['required'] : false ,
                        'allow_empty' => isset($dynamicField['validations']['required']) && empty($this->option) ? !$dynamicField['validations']['required'] : true ,
                        'filters' => array(
                            array('name' => 'StringTrim'),
                        ),
                    );
                    break;
            }
        } else {
            $validationArray = array(
                'name' => $dynamicField['name'],
                'required' => false ,
                'allow_empty' => true ,
                'filters' => array(
                    array('name' => 'StringTrim'),
                ),
            );
        }

        if (!empty($validationArray)) {
            $this->inputFilterSpecification[] = $validationArray;
        }
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return $this->inputFilterSpecification;
    }

    /**
     * Set a single option for an element
     *
     * @param  string $key
     * @param  mixed $value
     * @return self
     */
    public function setOption($key, $value)
    {
        // TODO: Implement setOption() method.
    }

    protected function getOptionsData($style)
    {
        if(is_array($this->cachedOptionsData) && !empty($this->cachedOptionsData)){
            return $this->cachedOptionsData;
        }

        return $this->_productsMapper->getAllOptionsForStyle($style, true);
    }
}