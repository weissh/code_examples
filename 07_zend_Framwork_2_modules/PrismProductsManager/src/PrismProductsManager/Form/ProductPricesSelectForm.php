<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;

class ProductPricesSelectForm extends Form implements EventManagerAwareInterface, InputFilterProviderInterface
{

    /**
     * @var EventManagerInterface
     */
    protected $events;

    /**
     * @var
     */
    protected $_dbAdapter;

    /**
     * Declare the service locator
     * @var type 
     */
    protected $_serviceLocator;
    protected $_existingVariantPrices;
    protected $_productId;
    protected $_variantId;

    /**
     * Initialize the form
     */
    public function init()
    {
        
    }

    /**
     * Pass the service locator and product mapper to the constructor.
     * 
     * @param type $serviceLocator
     * @param type $existingVariantPrices
     * @param type $productId
     * @param type $variantId
     */
    public function __construct($serviceLocator, $existingVariantPrices, $productId, $variantId)
    {
        $this->_existingVariantPrices = $existingVariantPrices;
        $this->_serviceLocator = $serviceLocator;
        $this->_productId = $productId;
        $this->_variantId = $variantId;

        $this->_dbAdapter = $serviceLocator->get('db-pms');

        $config = $serviceLocator->get('config');
        $this->_languageId = $config['siteConfig']['languages']['current-language'];

        parent::__construct('variant-prices-select-form');
        $this->setAttributes(array(
            'name' => 'variant-prices-select-form',
            'id' => 'variant-prices-select-form',
            'role' => 'form',
            'action' => '/products/get-variant-attributes/',
        ));
        $this->setHydrator(new ClassMethods());
        $this->prepareElements();
    }

    /**
     * Prepare the form to be built.
     */
    public function prepareElements()
    {

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'productId',
            'attributes' => array(
                'value' => $this->_productId
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'variantId',
            'attributes' => array(
                'value' => 0
            )
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));

        foreach ($this->_existingVariantPrices as $inputName => $inputValue) {
            $id = str_replace(' ', '-', strtolower($inputName)).'-'.$this->_variantId;
            $this->add(array(
                'type' => 'Zend\Form\Element\Text',
                'name' => $inputName,
                'attributes' => array(
                    'class' => 'form-control product-price',
                    'id' => $id,
                    'value' => $inputValue
                ),
            ));
        }

        $this->add(array(
            'name' => 'createVariantAddPrices',
            'attributes' => array(
                'id' => 'create-variant-add-prices',
                'type' => 'submit',
                'value' => 'Update Prices',
                'class' => 'btn btn-success margin-left pull-right',
            ),
        ));

        $this->add(array(
            'name' => 'closeModal',
            'attributes' => array(
                'id' => 'close-modal',
                'type' => 'button',
                'value' => 'Close',
                'data-dismiss' => 'modal',
                'class' => 'btn btn-default margin-left pull-right',
            ),
        ));
    }

    /**
     * Input filter for validation
     * 
     * @return type
     */
    public function getInputFilterSpecification()
    {
        return array(
            array(
                'name' => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
            array(
                'name' => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            )
        );
    }

    /**
     * Set the event manager instance used by this context
     *
     * @param EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;

        return $this;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }

}
