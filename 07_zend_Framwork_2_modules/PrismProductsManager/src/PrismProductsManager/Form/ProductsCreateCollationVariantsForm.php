<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;

class ProductsCreateCollationVariantsForm extends Form implements InputFilterProviderInterface
{
    /**
      * @var EventManagerInterface
    */
    protected $events;

    public function __construct()
    {

        parent::__construct('create-collation-variants');

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'createVariantsNumber',
            'attributes' => array(
                'placeholder' => 'Number of Variants',
                'class' => 'form-control create-collation-variants-number',
                'value' => 0
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'productId',
        ));

    }

    public function init()
    {
        //already used in __construct because eventManager usage
    }

    public function getInputFilterSpecification()
    {
        return array(
            array(
                'name' => 'createVariantsNumber',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities')
                ),
                'validators' => array(
                    array(
                        'name' => 'Int'
                    ),
                )
            ),
            array(
                'name' => 'productId',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities')
                ),
                'validators' => array(
                    array(
                        'name' => 'Int'
                    ),
                )
            ),
        );
    }
}
