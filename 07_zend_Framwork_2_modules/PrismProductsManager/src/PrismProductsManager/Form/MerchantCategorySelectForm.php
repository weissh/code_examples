<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;

class MerchantCategorySelectForm extends Form implements EventManagerAwareInterface, InputFilterProviderInterface
{

    /**
     * @var EventManagerInterface
     */
    protected $events;
    
    /**
     * Declare the products mapper
     * @var type 
     */
    protected $_productsMapper;
    
    /**
     * Declare the products mapper
     * @var type 
     */
    protected $_merchantCategoriesMapper;
    
    /**
     *  Declare the parent categories
     * @var type 
     */
    protected $_parentCategories;
    
    /**
     * Declare the service locator
     * @var type 
     */
    protected $_serviceLocator;

    /**
     * Does a category has children
     * @var type 
     */
    protected $_childrenCount = 0;


    /**
     * Initialize the form
     */
    public function init()
    {
        
    }

    /**
     * Pass the service locator and product mapper to the constructor.
     *
     * @param type $serviceLocator
     * @param array $merchantCategoriesMapper
     * @internal param \PrismProductsManager\Form\type $productMapper
     */
    public function __construct($serviceLocator, $merchantCategoriesMapper)
    {
        $this->_merchantCategoriesMapper = $merchantCategoriesMapper;
        $this->_serviceLocator = $serviceLocator;
        
        $parentId = 0;
        
        $parentCategories = $this->_merchantCategoriesMapper->fetchMerchantCategories($parentId);
        if ($parentCategories['dataCount'] > 0) {
            //$this->_parentCategories = $this->_parentCategories + $parentCategories['data'];
            $this->_parentCategories = $parentCategories['data'];
        }
        $this->_childrenCount = $parentCategories['childrenCount'];
        
        parent::__construct('select-merchant-category-form');
        $this->setAttributes(array(
            'name' => 'selectMerchantCategoryForm',
            'role' => 'form',
            'action' => '/products/add',
        ));
        
        $this->setHydrator(new ClassMethods());
        $this->prepareElements();
    }

    /**
     * Prepare the form to be built.
     */        
    public function prepareElements()
    {

         $this->add(array(
             'type' => 'Zend\Form\Element\Hidden',
             'name' => 'fetchSkuBuildingRules',
             'attributes' => array(
                     'value' => 0
             )
         ));
         
         $this->add(array(
             'type' => 'Zend\Form\Element\Hidden',
             'name' => 'productMerchantCategoryId',
             'attributes' => array(
                     'value' => 0
             )
         ));

         $this->add(array(
             'type' => 'Zend\Form\Element\Hidden',
             'name' => 'productId',
             'attributes' => array(
                     'value' => 0
             )
         ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            //'name' => 'merchantCategoryParentId',
            'name' => 'merchantCategory1',
            'attributes' => array(
                'class' => 'selectize selectized parent-category merchant-category',
                'id' => 'merchant-category-1',
                'required' => 'required',
                'data-children-count' => $this->_childrenCount,
                
            ),
            'options' => array(
                'label' => "Parent Category *",
                'label_attributes' => array(
                    'for' => 'group-parent'
                ),
                // this date will come from DB
            'value_options' => $this->_parentCategories,
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
                
        $this->add(array(
            'name' => 'submitMerchantCategoryForm',
            'attributes' => array(
                'id' => 'select-merchant-btn',
                'type' => 'submit',
                'value' => 'Select and continue',
                'class' => 'btn btn-success pull-right hide',
            ),
        ));
        
    }

    public function getInputFilterSpecification()
    {
        return array(
            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );
    }

    /**
     * Set the event manager instance used by this context
     *
     * @param EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;

        return $this;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }

}
