<?php
namespace PrismProductsManager\Form\View\Helper;

use Zend\Form\View\Helper\FormElementErrors as OriginalFormElementErrors;

class FormElementErrors extends OriginalFormElementErrors
{
    protected $messageCloseString     = '</li></ul>';
    protected $messageOpenFormat      = '<ul role="alert" class="list-unstyled alert alert-danger"%s><li><i class="icon-error"></i>';
    protected $messageSeparatorString = '</li><li class="alert alert-danger" role="alert">';

}