<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;

class ColoursForm extends Form implements EventManagerAwareInterface, InputFilterProviderInterface
{
    /**
      * @var EventManagerInterface
    */
    protected $events;

    /**
     * @var DB adapter
     */
    protected  $_dbAdapter;

    /**
     * Form submitted data
     */
    protected $submittedData;

    public function __construct($dbAdapter)
    {
        parent::__construct('colours-form');
        $this->add(array(
            'name' => 'colourId',
            'type' => 'hidden'
        ));
        $this->add(array(
            'name' => 'oldColourCode',
            'type' => 'hidden'
        ));
        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'colourName',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Colour Name'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'colourHex',
            'attributes' => array(
                'class' => 'form-control pull-left',
                'placeholder' => 'Colour Hex',
                'style' => 'width:75%',
                'id' => 'colourHex'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'colourCode',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Colour Code'
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'button',
            'attributes' => array (
                'class' => 'btn btn-primary save-attribute'
            )
        ));

        $this->_dbAdapter = $dbAdapter;
    }

    public function init()
    {
        //already used in __construct because eventManager usage
    }

    public function getInputFilterSpecification()
    {
        return array(
            array(
                'name' => 'colourId',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities')
                ),
                'validators' => array(
                    array(
                        'name' => 'Int'
                    ),
                )
            ),
            array(
                'name' => 'colourName',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3
                        )
                    ),
                )
            ),
            array(
                'name' => 'colourHex',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 7,
                            'max' => 7
                        )
                    ),
                    array(
                        'name' => 'Regex',
                        'options' => array(
                            'pattern' => '/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/',
                            'messages' => array(
                                'regexNotMatch'=>'Please use valid HTML colour code'
                            ),
                        ),
                    ),
                )
            ),
            $this->colourCodeValidation(),
            array(
                'name' => 'oldColourCode',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 5,
                        )
                    ),
                )
            )

        );
    }

    protected function colourCodeValidation()
    {
        if (!empty($this->submittedData['colourId']) &&  $this->submittedData['oldColourCode'] === $this->submittedData['colourCode']) {
            return array(
                'name' => 'colourCode',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 5,
                        )
                    )
                )
            );

        }else {
            return array(
                'name' => 'colourCode',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 5,
                        )
                    ),
                    array(
                        'name'    => 'Zend\Validator\Db\NoRecordExists',
                        'options' => array(
                            'table' => 'COLOURS',
                            'field' => 'colourCode',
                            'adapter' => $this->_dbAdapter,
                            'messages' => array(
                                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'The colour code already exists in database'
                            ),
                        ),
                    )
                )
            );
        }
    }

    /**
     * @param mixed $submitedData
     */
    public function setSubmittedData($submittedData)
    {
        $this->submittedData = $submittedData;
    }

    /**
     * Set the event manager instance used by this context
     *
     * @param EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;

        return $this;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }

}
