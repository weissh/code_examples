<?php
namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class AddVariantToCategoryForm
 * @package PrismProductsManager\Form
 */
class AddVariantToCategoryForm extends Form implements InputFilterProviderInterface
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct('add-category');
        $this->setAttributes(array(
            'name' => 'addVariantTocategory',
            'role' => 'form',
            'action' => '/marketing-categories',
        ));
        $this->prepareElements();
    }
    

    /**
     * Build add / edit category form
     */
    public function prepareElements()
    {
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'marketingCategoryId',
            'attributes' => array(
                'value' => 0
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'marketingCategoryName',
            'attributes' => array(
                'value' => ''
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'showAddVariantPopup',
            'attributes' => array(
                'value' => 0
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'showRemoveVariantPopup',
            'attributes' => array(
                'value' => 0
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'showManageProductsGroup',
            'attributes' => array(
                'value' => 0
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'productCodes',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'product-codes',
                'rows' => 10
            ),
            'options' => array(
                'label' => 'Product Codes (One per line)',
                'label_attributes' => array(
                    'for' => 'product-codes',
                    'class' => 'moreInfo',
                    'data-container' => 'body',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => '<p>Product codes can be at</p><p><ul><li>Style level</li></ul></p>',
                    'data-trigger' => 'hover',
                    'data-html' => 'true'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));

        $this->add(array(
            'name' => 'saveProductCodes',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit',
                'id' => 'save-product-codes-for-marketing-category',
                'class' => 'btn btn-primary pull-right',
            ),
        ));

        $this->add(array(
            'name' => 'removeProductCodes',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit',
                'id' => 'remove-product-codes-for-marketing-category',
                'class' => 'btn btn-primary pull-right',
            ),
        ));

        $this->add(array(
            'name' => 'backToCategoryScreen',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Back to marketing category',
                'class' => 'btn btn-primary',
            ),
        ));
    }
    
    /**
     * Validate the form input
     * @see \Zend\InputFilter\InputFilterProviderInterface::getInputFilterSpecification()
     */
    public function getInputFilterSpecification()
    {
        $result = array(            
            array(
                'name' => 'productCodes',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 5,
                        )
                    ),
                )
            ),
            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );
        
        return $result;
    }
    
}
