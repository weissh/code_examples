<?php

namespace PrismProductsManager\Form;

use PrismProductsManager\Model\AttributesGroupMapper;
use PrismProductsManager\Service\AssignFieldToTabsService;
use PrismProductsManager\Service\CommonAttributesService;
use Zend\Form\Form;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Class AssignInputFieldsForm
 * @package PrismProductsManager\Form
 */
class InputFieldValidationForm extends Form implements InputFilterProviderInterface
{
    /**
     * @var AssignFieldToTabsService
     */
    protected $assignFieldToTabsService;

    /**
     * @param AssignFieldToTabsService $assignFieldToTabsService
     */
    public function __construct(
        AssignFieldToTabsService $assignFieldToTabsService
    )
    {
        /** Services */
        $this->assignFieldToTabsService = $assignFieldToTabsService;
        /** Set form params */
        parent::__construct('input-field-validation');
        $this->setAttributes(array(
            'name' => 'input-field-validation',
            'id' => 'input-field-validation',
            'role' => 'form',
            'action' => '/assign-fields-to-tabs/save-validation'
        ));

        /** Generate form elements */
        $this->prepareElements();
    }

    /**
     * Prepare the form to be built.
     */        
    public function prepareElements()
    {
        $this->add(array(
            'name' => 'inputValidationId',
            'type' => 'hidden'
        ));
        $this->add(array(
            'name' => 'inputFieldId',
            'type' => 'hidden'
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'inputValidationType',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'inputValidationType',
                'required' => 'required',
            ),
            'options' => array(
                'label' => "Validation Types *",
                'label_attributes' => array(
                    'for' => 'inputValidationType'
                ),
                'value_options' => $this->assignFieldToTabsService->getInputValidationTypeForForm(),
            ),
        ));

        $this->add(array(
            'name' => 'minValue',
            'type' => 'Zend\Form\Element\Number',
            'required' => false,
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Min Value',
            ),
            'options' => array(
                'label' => "Min Value",
                'label_attributes' => array(
                    'for' => 'minValue'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'maxValue',
            'type' => 'Zend\Form\Element\Number',
            'required' => false,
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Max Value',
            ),
            'options' => array(
                'label' => "Max Value",
                'label_attributes' => array(
                    'for' => 'maxValue'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'pattern',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Pattern',
            ),
            'options' => array(
                'label' => "Pattern ( needed for regex expression )",
                'label_attributes' => array(
                    'for' => 'pattern'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
        $this->add(array(
            'name' => 'save-validation',
            'attributes' => array(
                'id' => 'edit-field',
                'type' => 'submit',
                'value' => 'Save',
                'class' => 'btn btn-success pull-right',
            ),
        ));
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            array(
                'name' => 'inputValidationType',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            array(
                'name' => 'minValue',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            array(
                'name' => 'maxValue',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            array(
                'name' => 'pattern',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            ),
            array(
                'name' => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            )
        );
    }
}
