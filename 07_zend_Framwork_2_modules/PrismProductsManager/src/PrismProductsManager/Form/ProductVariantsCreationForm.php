<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;

class ProductVariantsCreationForm extends Form implements EventManagerAwareInterface, InputFilterProviderInterface
{

    /**
     * @var EventManagerInterface
     */
    protected $events;

    /**
     * Initialize the form
     */
    public function init()
    {
        
    }

    /**
     * Pass the service locator and product mapper to the constructor.
     * 
     * @param type $serviceLocator
     * @param type $productMapper
     */
    public function __construct($serviceLocator, $productMapper)
    {
        $this->_productsMapper = $productMapper;
        $this->_serviceLocator = $serviceLocator;

        parent::__construct('product-variants-creation-form');
        $this->setAttributes(array(
            'name' => 'productVariantsCreationForm',
            'id' => 'product-variants-creation-form',
            'role' => 'form',
            'action' => '/products/add',
        ));
        $this->setHydrator(new ClassMethods());
        $this->prepareElements();
    }

    /**
     * Prepare the form to be built.
     */
    public function prepareElements()
    {

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'productId',
            'attributes' => array(
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'firstPartOfSku',
            'attributes' => array(
                'value' => ''
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'showcategorypopup',
            'attributes' => array(
                'value' => 0
            )
        ));
         
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'variantId',
            'attributes' => array(
                 'value' => 0
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'productVariantCount',
            'attributes' => array(
                'value' => 0
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'useSkuAsBarcode',
            'options' => array(
                'label' => 'Use SKU as barcode',
                //'use_hidden_element' => true,
                'checked_value' => 1,
                'unchecked_value' => 0
            )
        ));
                                
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'getVariants',
            'attributes' => array(
                'value' => ''
            )
        ));

        //Button to add selected product variants to a list of variants.
        $this->add(array(
            'name' => 'addNewColour',
            'attributes' => array(
                'id' => 'add-new-colour',
                'type' => 'button',
                'value' => 'Add new colour',
                'class' => 'btn btn-primary pull-right btn-xs hide',
            ),
        ));


        //Button to add selected product variants to a list of variants.
        $this->add(array(
            'name' => 'addProductvariants',
            'attributes' => array(
                'id' => 'add-product-variants',
                'type' => 'button',
                'value' => 'Add variants',
                'class' => 'btn btn-success pull-right btn-xs',
            ),
        ));

        //Button to save the product variant
        $this->add(array(
            'name' => 'saveVariants',
            'attributes' => array(
                'id' => 'save-variants',
                'type' => 'submit',
                'value' => ' Save ',
                'class' => 'btn btn-success pull-right',
            ),
        ));

        //Button to manual sync the variants to channels
        $this->add(array(
            'name' => 'manualSyncButton',
            'attributes' => array(
                'id' => 'sync-variants',
                'type' => 'submit',
                'value' => ' Sync ',
                'class' => 'btn btn-success pull-right',
            ),
        ));

        //Button to save the product variant
        $this->add(array(
            'name' => 'deleteSelectedVariants',
            'attributes' => array(
                'id' => 'delete-selected-variants',
                'type' => 'button',
                'value' => ' Delete selected ',
                'class' => 'btn btn-danger btn-sm pull-right',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 1800
                )
            )
        ));
    }

    /**
     * Input filter for validation
     * 
     * @return type
     */
    public function getInputFilterSpecification()
    {
        return array(
            array(
                'name' => 'productId',
                'required' => true,
                'filters' => array(
                    array ('name' => 'Int')
                ),
                'validators' => array(
                    array(
                        'name' => 'GreaterThan',
                        'options' => array(
                            'min' => 0,
                            'inclusive' => true
                        ),
                    ),
                ),
            ),
            array(
                'name' => 'barcode',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'HtmlEntities')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 75,
                        ),
                    ),
                ),
            ),
            array(
                'name' => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );
    }

    /**
     * Set the event manager instance used by this context
     *
     * @param EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;

        return $this;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }

}
