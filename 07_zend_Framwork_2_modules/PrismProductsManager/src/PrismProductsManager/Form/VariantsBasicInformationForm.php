<?php
namespace PrismProductsManager\Form;

use Zend\Form\ElementInterface;
use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class AddCategoryForm
 * @package PrismProductsManager\Form
 */
class VariantsBasicInformationForm extends Form implements InputFilterProviderInterface
{


    public function __construct()
    {
        parent::__construct('VariantsBasicInformationForm');
        $this->setAttributes(array(
            'name' => 'VariantsBasicInformationForm',
            'role' => 'form'
        ));
        $this->prepareElements();
    }

    /**
     * Build add / edit category form
     */
    public function prepareElements()
    {
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'variantIds',
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'productId',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'shortProductName',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'short-product-name',
                'placeholder' => 'Short Product Name',
            ),
            'options' => array(
                'label' => 'Short Product Name',
                'label_attributes' => array(
                    'for' => 'short-product-name'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'productName',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'product-name',
                'placeholder' => 'Short Product Name',
            ),
            'options' => array(
                'label' => 'Product Name',
                'label_attributes' => array(
                    'for' => 'product-name'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'shortDescription',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'short-description',
                'placeholder' => 'Short Description',
            ),
            'options' => array(
                'label' => 'Short Description',
                'label_attributes' => array(
                    'for' => 'short-description'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'longDescription',
            'attributes' => array(
                'class' => 'form-control tiny-mce',
                'id' => 'long-description',
                'rows' => 4,
                'name' => 'longDescription'
            ),
            'options' => array(
                'label' => 'Long Description',
                'label_attributes' => array(
                    'for' => 'long-description'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'metaTitle',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'meta-title',
                'placeholder' => 'Long Description',
            ),
            'options' => array(
                'label' => 'Meta Title',
                'label_attributes' => array(
                    'for' => 'meta-title'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'metaDescription',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'meta-description',
                'placeholder' => 'Meta Description',
            ),
            'options' => array(
                'label' => 'Meta Description',
                'label_attributes' => array(
                    'for' => 'meta-description'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'metaKeyword',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'meta-keyword',
                'placeholder' => 'Meta Keyword',
            ),
            'options' => array(
                'label' => 'Meta Keyword',
                'label_attributes' => array(
                    'for' => 'meta-keyword'
                ),
            ),
        ));
        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));

        $this->add(array(
            'name' => 'submitChangeDetails',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'class' => 'btn btn-success pull-right margin-left',
            ),
        ));

        $this->add(array(
            'name' => 'cancelChangeDetails',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Cancel',
                'data-dismiss' => 'modal',
                'class' => 'btn btn-default pull-right',
            ),
        ));
    }

    /**
     *
     */
    public function init()
    {
        //already used in __construct because eventManager usage
    }

    /**
     * Validate the form input
     * @see \Zend\InputFilter\InputFilterProviderInterface::getInputFilterSpecification()
     */
    public function getInputFilterSpecification()
    {
        $result = array(
            array(
                'name' => 'variantIds',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities')
                ),
            ),
            array(
                'name' => 'productId',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities')
                ),
            ),
            array(
                'name' => 'shortProductName',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
            ),
            array(
                'name' => 'productName',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 180,
                        )
                    ),
                )
            ),
            array(
                'name' => 'metaKeyword',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 75,
                        )
                    ),
                )
            ),
            array(
                'name' => 'metaDescription',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 160,
                        )
                    ),
                )
            ),
            array(
                'name' => 'shortDescription',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 150,
                        )
                    ),
                )
            ),
            array(
                'name' => 'longDescription',
                'required' => false,
                'filters' => array(
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 2500,
                        )
                    ),
                )
            ),


            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );

        return $result;
    }
}
