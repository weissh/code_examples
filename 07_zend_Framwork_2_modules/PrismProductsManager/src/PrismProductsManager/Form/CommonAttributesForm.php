<?php

namespace PrismProductsManager\Form;

use PrismProductsManager\Service\CommonAttributesService;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 * Class CommonAttributesForm
 * @package PrismProductsManager\Form
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class CommonAttributesForm extends Form implements  InputFilterProviderInterface
{
    /**
     * @var EventManagerInterface
     */
    protected $events;

    /**
     * @var CommonAttributesService
     */
    protected $commonAttributesService;

    /**
     * @param CommonAttributesService $commonAttributesService
     */
    public function __construct(CommonAttributesService $commonAttributesService)
    {
        $this->commonAttributesService = $commonAttributesService;

        parent::__construct('common-attributes-form');

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'commonAttributeId',
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'isEditable',
        ));
        $this->add (array(
            'name' => 'description',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Common Attribute description *'
            ),
            'options' => array(
                'label' => 'Common Attribute description',
                'label_attributes' => array(
                    'for' => 'description',
                    'class' => 'control-label'
                )
            )
        ));
        $this->add (array(
            'name' => 'mapping',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Common Attribute mapping'
            ),
            'options' => array(
                'label' => 'Common Attribute mapping',
                'label_attributes' => array(
                    'for' => 'description',
                    'class' => 'control-label'
                )
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'commonAttributeType',
            'options' => array(
                'value_options' => $this->commonAttributesService->getCommonAttributesDisplayType(),
                'label' => 'Display type *',
                'label_attributes' => array(
                    'for' => 'commonAttributeType',
                    'class' => 'control-label'
                ),
            ),
            'attributes' => array (
                'class' => 'selectize selectized',
                'tabindex' => '-1',
                'style' => 'display:none'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'isTicketByDefault',
            'attributes' => array (
                'class' => '',
            ),
            'options' => array(
                'label' => 'Ticked by default',
                'label_attributes' => array(
                    'for' => 'isTicketByDefault',
                    'class' => 'control-label'
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'usedForFiltering',
            'attributes' => array (
                'class' => '',
            ),
            'options' => array(
                'label' => 'Used for filtering',
                'label_attributes' => array(
                    'for' => 'usedForFiltering',
                    'class' => 'control-label'
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'usedForMerchantCategories',
            'attributes' => array (
                'class' => ''
            ),
            'options' => array(
                'label' => 'Used for SKU building',
                'label_attributes' => array(
                    'for' => 'usedForMerchantCategories',
                    'class' => 'control-label '
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'isSizeProperty',
            'attributes' => array (
                'class' => '',
                'label' => 'Used as Size Property'
            ),
            'options' => array(
                'label' => 'Used as Size Property',
                'label_attributes' => array(
                    'for' => 'isSizeProperty',
                    'class' => 'control-label'
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'isSize',
            'attributes' => array (
                'class' => '',
                'label' => 'Used for sizing'
            ),
            'options' => array(
                'label' => 'Used for sizing',
                'label_attributes' => array(
                    'for' => 'isSize',
                    'class' => 'control-label'
                ),
            )
        ));

        // list all common attributes with isSizeProperty is true
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'commonAttributeSizeProperty',
            'required' => false,
            'allow_empty' => true,
            'attributes' => array (
                'class' => 'form-control multiselect-selector',
                'id' => 'commonAttributeSizeProperty',
                'required' => false,
                'multiple' => 'multiple',
                //'style' => 'display:none'
            ),
            'options' => array(
                //'empty_option' => '-- Please choose size property --',
                'value_options' => $this->commonAttributesService->getAllSizePropertyCommonAttributes(),
                'label' => 'Size Property',
                'label_attributes' => array(
                    'for' => 'commonAttributeSizeProperty',
                    'class' => 'control-label'
                ),
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'chained',
            'attributes' => array (
                'class' => '',
            ),
            'options' => array(
                'label' => 'Chained attribute',
                'label_attributes' => array(
                    'for' => 'chained',
                    'class' => 'control-label'
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'period',
            'attributes' => array (
                'class' => '',
            ),
            'options' => array(
                'label' => 'Time period attribute',
                'label_attributes' => array(
                    'for' => 'period',
                    'class' => 'control-label'
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'keepHistory',
            'attributes' => array (
                'class' => '',
            ),
            'options' => array(
                'label' => 'Record history',
                'label_attributes' => array(
                    'for' => 'keepHistory',
                    'class' => 'control-label'
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'autoFromDate',
            'attributes' => array (
                'class' => '',
            ),
            'options' => array(
                'label' => 'Auto from date',
                'label_attributes' => array(
                    'for' => 'autoFromDate',
                    'class' => 'control-label'
                ),
            )
        ));

        $this->add (array(
            'name' => 'commonAttributeDataElement',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Javascript Size Group Data',
                'readonly' => true
            ),
            'options' => array(
                'label' => 'Javascript Size Group Data',
                'label_attributes' => array(
                    'for' => 'commonAttributeDataElement',
                    'class' => 'control-label'
                ),
            )
        ));
        
        $this->add (array(
            'name' => 'EditableOnLevel',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Editable on Level',
                'value_options' => array (
                    'ALL' => 'ALL',
                    'STYLE' => 'STYLE',
                    'OPTION' => 'OPTION',
                ),
                'label_attributes' => array(
                    'for' => 'EditableOnLevelElement',
                    'class' => 'control-label'
                ),
            ),
            'attributes' => array (
                'class' => 'selectize selectized',
                'tabindex' => '-1',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'validators' => array(
                array(
                    'name' => 'NotSame',
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\Csrf::NOT_SAME => 'Form has timeout out. Please submit the form again.',
                        ),
                    ),
                ),
            ),
        ));
        $this->add (array(
            'name' => 'submit',
            'type' => 'button',
            'attributes' => array (
                'type' => 'submit',
                'value' => 'Save',
                'class' => 'btn btn-primary save-common-attribute'
            )
        ));
    }

    /**
     *
     */
    public function init()
    {
        //already used in __construct because eventManager usage
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $result  =  array(
                        array(
                            'name' => 'commonAttributeId',
                            'required' => false,
                            'filters' => array(
                                array('name' => 'StripTags'),
                                array('name' => 'StringTrim'),
                                array ('name' => 'HtmlEntities')
                            ),
                            'validators' => array(
                                array(
                                    'name' => 'Int'
                                ),
                            )
                        ),
                        array(
                            'name' => 'isEditable',
                            'required' => false,
                            'filters' => array(
                                array('name' => 'StripTags'),
                                array('name' => 'StringTrim'),
                                array ('name' => 'HtmlEntities')
                            ),
                            'validators' => array(
                                array(
                                    'name' => 'Int'
                                ),
                            )
                        ),
                        array(
                            'name' => 'description',
                            'required' => true,
                            'filters' => array(
                                array ('name' => 'StripTags'),
                                array ('name' => 'StringTrim'),
                            ),
                            'validators' => array(
                                array (
                                    'name' => 'StringLength',
                                    'options' => array(
                                        'min' => 3,
                                        'max' => 60,
                                    ),
                                ),
                            ),
                        ),
                        array(
                            'name' => 'commonAttributeDataElement',
                            'required' => false,
                            'filters' => array(
                                array ('name' => 'StripTags'),
                                array ('name' => 'StringTrim')
                            ),
                            'validators' => array(
                                array (
                                    'name' => 'StringLength',
                                    'options' => array(
                                        'encoding' => 'UTF-8'
                                    ),
                                ),
                            ),
                        ),
                        array(
                            'name' => 'commonAttributeType',
                            'required' => false,
                        ),
                        array(
                            'name' => 'commonAttributeSizeProperty',
                            'required' => false,
                        ),
                        array(
                            'name' => 'EditableOnLevel',
                            'required' => false,
                        ),
                        array(
                            'name'     => 'csrf',
                            'required' => true,
                            'validators' => array(
                                array(
                                    'name' => 'csrf',
                                    'options' => array(
                                        'messages' => array(
                                            \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    );
        return $result;
    }

    /**
     * Set a single option for an element
     *
     * @param  string $key
     * @param  mixed $value
     * @return self
     */
    public function setOption($key, $value)
    {
        // TODO: Implement setOption() method.
    }
}