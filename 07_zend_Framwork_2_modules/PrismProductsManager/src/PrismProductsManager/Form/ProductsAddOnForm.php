<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;

class ProductsAddOnForm extends Form implements InputFilterProviderInterface
{
    /**
      * @var EventManagerInterface
    */
    protected $events;

    public function __construct()
    {

        parent::__construct('product-add-on-form');
        $this->setAttributes(array(
            'name' => 'product-add-on-form',
            'role' => 'form',
            'action' => '/products/addEdit/',
            'onsubmit' => 'return false;'
        ));

        $this->add(array(
            'name' => 'productId',
            'type' => 'hidden'
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'product-add-on-search',
            'options' => array(
            )
        ));



        $this->add(array(
            'name' => 'saveProductAddOn',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'id' => 'submit-button',
                'class' => 'btn btn-success ',
                'value' => 'Save'
            ),

        ));

        
    }

    public function init()
    {

    }

    public function getInputFilterSpecification()
    {
        return array(

            'product-add-on-form' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'Zend\Filter\StringTrim'),
                ),
            ),

            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),

        );
    }

    /**
     * Set the event manager instance used by this context
     *
     * @param EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;

        return $this;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }

}
