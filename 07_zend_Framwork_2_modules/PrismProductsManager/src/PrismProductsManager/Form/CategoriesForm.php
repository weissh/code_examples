<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;

class CategoriesForm extends Form implements EventManagerAwareInterface, InputFilterProviderInterface
{
    /**
      * @var EventManagerInterface
    */
    protected $events;

    public function __construct()
    {

        parent::__construct('categories-form');

        $this->add(array(
            'name' => 'id',
            'type' => 'hidden'
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'parent-group',
            'attributes' => array(
                'id' => 'parent-group',
            ),
            'options' => array(
                'label' => 'Parent',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'product-group-name',
            'options' => array(
                'label' => 'Subject',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'product-group-name',
            'options' => array(
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'product-group-description',
            'options' => array(
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'meta-title',
            'options' => array(
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'meta-keywords',
            'options' => array(
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'meta-description',
            'options' => array(
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'enable',
            'options' => array(
                'label' => 'Enable '
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'hide',
            'options' => array(
                'label' => 'Hide '
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'lock',
            'options' => array(
                'label' => 'Lock '
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'id' => 'submit-button',
                'class' => 'btn btn-primary pull-right'
            ),
            'options' => array(
                'label' => 'Create',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Button',
            'name' => 'cancel',
            'attributes' => array(
                'id' => 'cancel-button',
                'class' => 'btn btn-default pull-right'
            ),
            'options' => array(
                'label' => 'Cancel',
            )
        ));
        
    }

    public function init()
    {
        //already used in __construct because eventManager usage
    }

    public function getInputFilterSpecification()
    {
        return array(
//            array(
//                'name'     => 'email',
//                'filters'  => array(
//                    array('name' => 'StripTags'),
//                    array('name' => 'StringTrim'),
//                ),
//                'validators' => array(
//                    array(
//                        'name'    => 'EmailAddress',
//                    ),
//                ),
//            ),
//            array(
//                'name' => 'product-group-name',
//                'required' => false,
//                'filters' => array(
//                    array('name' => 'StripTags'),
//                    array('name' => 'StringTrim'),
//                ),
            'product-group-name' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'Zend\Filter\StringTrim'),
                ),
            ),

            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
//            ),
//            array(
//              'name'     => 'csrf',
//                'required' => false,
//                'validators' => array(
//                    array(
//                        'name'    => 'Csrf',
//                    ),
//                ),
//            ),
        );
    }

    /**
     * Set the event manager instance used by this context
     *
     * @param EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;

        return $this;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }

}
