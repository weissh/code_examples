<?php
namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class EditCoreAttributeGroupForm
 * @package PrismProductsManager\Form
 */
class EditCoreAttributeGroupForm extends Form implements InputFilterProviderInterface
{

    public function __construct()
    {
        parent::__construct('edit-core-attribute-group');
        $this->setAttributes(array(
            'name' => 'editCoreAttributeGroup',
            'role' => 'form',
            'action' => '/core-attributes',
        ));
        $this->prepareElements();
    }

    /**
     * Edit core attribute form
     */
    public function prepareElements()
    {
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'attributeGroupId',
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'publicName',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'core-attribute-name',
                'placeholder' => 'Core Group Attribute Public Name',
                'required' => 'required',
            ),
            'options' => array(
                'label' => 'Core Group Attribute Public Name',
                'label_attributes' => array(
                    'for' => 'core-attribute-name'
                ),
            ),
        ));
        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
        $this->add(array(
            'name' => 'submitEditCoreAttributeGroup',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'class' => 'btn btn-primary pull-right',
            ),
        ));
        $this->add(array(
            'name' => 'cancelEditCoreAttributeGroup',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Cancel',
                'data-dismiss' => 'modal',
                'class' => 'btn btn-default pull-right',
            ),
        ));
    }

    /**
     *
     */
    public function init()
    {
        //already used in __construct because eventManager usage
    }

    /**
     * Validate the form input
     * @see \Zend\InputFilter\InputFilterProviderInterface::getInputFilterSpecification()
     */
    public function getInputFilterSpecification()
    {
        $result = array(
            array(
                'name' => 'attributeGroupId',
                'required' => true,
                'filters' => array(
                    array('name' => 'Int'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Between',
                        'options' => array(
                            'min' => 1,
                        ),
                    ),
                ),
            ),
            array(
                'name' => 'publicName',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 30,
                        )
                    ),
                )
            ),
            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );

        return $result;
    }
}
