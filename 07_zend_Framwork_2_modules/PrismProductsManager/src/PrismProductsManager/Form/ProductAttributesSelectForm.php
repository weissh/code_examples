<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;

class ProductAttributesSelectForm extends Form implements EventManagerAwareInterface, InputFilterProviderInterface
{
    /**
     * @var EventManagerInterface
     */
    protected $events;
    protected $_parentCategories;
    protected $_serviceLocator;
    protected $_productAttributes = array();
    protected $_attributeElements = array();
    protected $_dynamicallyGeneratedAttributes = array();

    /**
     * Initialize the form
     */
    public function init()
    {
        
    }

    /**
     * Pass the service locator and the processed attributes
     *
     * @param $serviceLocator
     * @param $dynamicallyGeneratedAttributes
     * @param $isVariant
     */
    public function __construct($serviceLocator, $dynamicallyGeneratedAttributes, $isVariant)
    {
        parent::__construct('product-attributes-select-form');
        $this->_serviceLocator = $serviceLocator;
        $this->_dynamicallyGeneratedAttributes = $dynamicallyGeneratedAttributes;

        if ($isVariant === 0) {
            $this->setAttributes(array(
                'name' => 'productAttributesSelectForm',
                'id' => 'product-attributes-select-form',
                'role' => 'form',
                'action' => '/products/edit',
            ));
        } else {
            $this->setAttributes(array(
                'name' => 'variantAttributesSelectForm',
                'id' => 'variant-attributes-select-form',
                'role' => 'form',
                'action' => '/products/get-variant-attributes/',
            ));
        }
        
        $this->setHydrator(new ClassMethods());
        $this->prepareElements($isVariant);
    }

    public function getDynamicallyGeneratedAttributes()
    {
        return $this->_dynamicallyGeneratedAttributes;
    }

    public function setDynamicallyGeneratedAttributes($_dynamicallyGeneratedAttributes)
    {
        $this->_dynamicallyGeneratedAttributes = $_dynamicallyGeneratedAttributes;
    }

    public function getAttributeElements()
    {
        return $this->_attributeElements;
    }

    public function setAttributeElements($_attributeElements)
    {
        $this->_attributeElements = $_attributeElements;
    }

    /**
     * Prepare the form to be built.
     */
    public function prepareElements($isVariant)
    {
        $this->add(array(
            'name' => 'id',
            'type' => 'hidden'
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'productId',
            'attributes' => array(
                'value' => 0
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'variantId',
            'attributes' => array(
                'value' => 0,
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'submitButtonName',
            'attributes' => array(
                'value' => ''
            )
        ));
        if ($isVariant === 0) {
            $this->add(array(
                'name' => 'createProductAddAttributes',
                'attributes' => array(
                    'id' => 'create-product-add-attributes',
                    'type' => 'submit',
                    'value' => 'Save and continue',
                    'class' => 'btn btn-success pull-right',
                ),
            ));
        } else {
            $this->add(array(
                'name' => 'createProductAddAttributes',
                'attributes' => array(
                    'id' => 'create-variant-add-attributes',
                    'type' => 'submit',
                    'value' => 'Save and continue',
                    'class' => 'btn btn-success pull-right',
                ),
            ));
        }

        if (count($this->_dynamicallyGeneratedAttributes) > 0) {


            foreach ($this->_dynamicallyGeneratedAttributes as $attributeType => $attributesArray) {

                if ($attributeType == 'new') {
                    foreach ($attributesArray as $realAttributesArray) {
                        foreach ($realAttributesArray as $attributesArrayValue) {
                            //Extract the form element attribute
                            $this->_addElementBasedOnType($attributesArrayValue, $isVariant);
                            $newHiddenAttributeProperties = array(
                                'name' => 'not_use_'.$attributesArrayValue['name'],
                                'viewAs' => 'hidden'
                            );
                            $this->_addElementBasedOnType($newHiddenAttributeProperties, $isVariant);
                        }
                    }
                } else {
                    $count = 0;
                    foreach ($attributesArray as $attributesArrayKey => $attributesArrayValue) {
                        //echo ++$count;
                        //Extract the form element attribute
                        $this->_addElementBasedOnType($attributesArrayValue, $isVariant);
                    }
                }
            }
        }
        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
    }

    private function _addElementBasedOnType($attributesArrayValue, $isVariant)
    {
        $elementName = $attributesArrayValue['name'];
        $label = isset($attributesArrayValue['publicName']) ? $attributesArrayValue['publicName'] : '';
        $viewAs = strtolower($attributesArrayValue['viewAs']);
        $fieldIdPrefix = ($isVariant === 0) ? '' : 'variant_';



        switch ($viewAs) {
            case 'checkbox':
                $this->add(array(
                    'type' => 'Zend\Form\Element\Checkbox',
                    'name' => $elementName,
                    'options' => array(
                        'label' => $label,
                        'use_hidden_element' => true,
                        'checked_value' => 1,
                        'unchecked_value' => 0
                    )
                ));
                break;
            case 'text':
                $this->add(array(
                    'type' => 'Zend\Form\Element\Text',
                    'name' => $elementName,
                    'attributes' => array(
                        'class' => 'form-control ' ,
                        'id' => $fieldIdPrefix.$elementName,
                        'placeholder' => $label,
                    ),
                    'options' => array(
                        'label' => $label,
                        'label_attributes' => array(
                            'for' => $fieldIdPrefix.$elementName
                        ),
                    ),
                ));
                break;
            case 'select':
                if (count($attributesArrayValue['values']) > 0) {
                    $this->add(array(
                        'type' => 'Zend\Form\Element\Select',
                        'name' => $elementName,
                        'attributes' => array(
                            'class' => 'selectize selectized',
                            'id' => $fieldIdPrefix.$elementName,
                        ),
                        'options' => array(
                            'label' => $label,
                            'label_attributes' => array(
                                'for' => $fieldIdPrefix.$elementName
                            ),
                            'value_options' => $attributesArrayValue['values'],
                        ),
                    ));
                }
                break;
            case 'radio':
                if (count($attributesArrayValue['values']) > 0) {
                    $this->add(array(
                        'type' => 'Zend\Form\Element\Radio',
                        'name' => $elementName,
                        'attributes' => array(
                            'class' => 'radio-inline',
                        ),
                        'options' => array(
                            'label' => $label,
                            'value_options' => $attributesArrayValue['values'],
                        )
                    ));
                }
                break;
            case 'hidden':
                $this->add(array(
                    'type' => 'Zend\Form\Element\Hidden',
                    'name' => $elementName,
                    'attributes' => array(
                        'value' => 0
                    )
                ));
                break;
            default:
                break;
        }
    }

    /**
     * Input filter for validation
     * 
     * @return type
     */
    public function getInputFilterSpecification()
    {
        $validationArray = array();
        $validationArray[] = array(
            'name' => 'csrf',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'csrf',
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                        ),
                    ),
                ),
            ),
        );

        if (count($this->_dynamicallyGeneratedAttributes) > 0) {
            foreach ($this->_dynamicallyGeneratedAttributes as $attributeType => $attributesArray) {
                if ($attributeType == 'new') {
                    foreach ($attributesArray as $realAttributesArray) {
                        foreach ($realAttributesArray as $attributesArrayValue) {
                            //Extract the form element attribute
                            $required = ($attributesArrayValue['viewAs'] == 'TEXT') ? false : true;
                            $elementName = $attributesArrayValue['name'];
                            $validationArray[] = array(
                                'name' => $elementName,
                                'required' => $required,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                    array('name' => 'HtmlEntities'),
                                )
                            );
                        }
                    }
                } else {
                    foreach ($attributesArray as $attributesArrayValue) {
                        //Extract the form element attribute
                        $required = ($attributesArrayValue['viewAs'] == 'TEXT') ? false : true;
                        $elementName = $attributesArrayValue['name'];
                        $validationArray[] = array(
                            'name' => $elementName,
                            'required' => $required,
                            'filters' => array(
                                array('name' => 'StripTags'),
                                array('name' => 'StringTrim'),
                                array('name' => 'HtmlEntities'),
                            )
                        );
                    }
                }
            }
        }
        return $validationArray;
    }

    /**
     * Set the event manager instance used by this context
     *
     * @param EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;

        return $this;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }
}
