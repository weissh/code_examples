<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class AddAttributesMerchantGroupForm
 * @package PrismProductsManager\Form
 */
class AddAttributesMerchantGroupForm extends Form implements  InputFilterProviderInterface
{
    /**
     * @var EventManagerInterface
     */
    protected $events;

    /**
     * @var
     */
    public $formData;

    /**
     *
     */
    protected $_dbAdapter;
    /**
     *
     */
    public function __construct()
    {
        parent::__construct('attributes-merchant-group-add');

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'attributeGroupId',
        ));
        $this->add (array(
            'name' => 'attributeMerchantGroupName',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Merchant Attribute Group Name'
            ),
        ));
        $this->add (array(
            'name' => 'attributeMerchantGroupPublicName',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Merchant Attribute Group Public Name'
            ),
        ));
        $this->add (array(
            'name' => 'attributeMerchantGroupCode',
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Merchant Attribute Group Code',
                'maxlength' => 6
            ),

        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'attributeMerchantType',
            'options' => array(
                'value_options' => array (
                    'COLOURS_Groups' => 'Colours Group',
                    'SIZES_Shoes' => 'Shoe Sizes Group',
                    'SIZES_Clothes' => 'Clothes Sizes Group',
                    'SIZES_Shirts' => 'Shirt Sizes Group',
                    'SIZES_Accessories' => 'Accesories Sizes',
                    'SIZES_Belts' => 'Belts Sizes',
                    'SIZES_Jackets' => 'Jackets Sizes',
                    'SIZES_Trousers' => 'Trausers Sizes',
                    'SIZES_SwimShorts' => 'Swim Shorts Sizes',
                )
            ),
            'attributes' => array (
                'class' => 'selectize selectized',
                'tabindex' => '-1',
                'style' => 'display:none'
            )
        ));
        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));

        $this->add( array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'validators' => array(
                array(
                    'name' => 'NotSame',
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\Csrf::NOT_SAME => 'Please enter a value for "foo".',
                        ),
                    ),
                ),
            ),
        ) );
        $this->add (array(
            'name' => 'submit',
            'type' => 'button',
            'attributes' => array (
                'class' => 'btn btn-primary save-merchant-group'
            )
        ));

    }


    /**
     *
     */
    public function init()
    {
        //already used in __construct because eventManager usage
    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $result  =  array(
                        array(
                            'name' => 'attributeMerchantGroupName',
                            'required' => true,
                            'filters' => array(
                                array ('name' => 'StripTags'),
                                array ('name' => 'StringTrim'),
                                array ('name' => 'HtmlEntities'),
                            ),
                            'validators' => array(
                                array (
                                    'name' => 'StringLength',
                                    'options' => array(
                                        'encoding' => 'UTF-8',
                                        'min' => 5,
                                        'max' => 60,
                                    ),
                                ),
                            ),
                        ),
                        array(
                            'name' => 'attributeMerchantGroupPublicName',
                            'required' => true,
                            'filters' => array(
                                array ('name' => 'StripTags'),
                                array ('name' => 'StringTrim'),
                                array ('name' => 'HtmlEntities'),
                            ),
                            'validators' => array(
                                array (
                                    'name' => 'StringLength',
                                    'options' => array(
                                        'encoding' => 'UTF-8',
                                        'min' => 5,
                                        'max' => 60,
                                    ),
                                ),

                            ),
                        ),
                        array(
                            'name'     => 'csrf',
                            'required' => true,
                            'validators' => array(
                                array(
                                    'name' => 'csrf',
                                    'options' => array(
                                        'messages' => array(
                                            \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    );
        if (isset($this->formData['attributeGroupId']) && empty($this->formData['attributeGroupId'])) {
            $result[] = array(
                'name' => 'attributeMerchantGroupCode',
                'required' => true,
                'filters' => array(
                    array ('name' => 'StripTags'),
                    array ('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array (
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 6,
                        ),

                    ),
                    array(
                        'name'    => 'Zend\Validator\Db\NoRecordExists',
                        'options' => array(
                            'table' => 'ATTRIBUTES_Groups',
                            'field' => 'groupCode',
                            'adapter' => $this->_dbAdapter,
                            'exclude' => array(
                                'field' => 'status',
                                'value' => 'DELETED'
                            ),
                            'messages' => array(
                                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'The Group Code is already is used.'
                            ),
                        ),
                    ),
                ),
            );
        }
        return $result;
    }

    /**
     * @param mixed $dbAdapter
     */
    public function setDbAdapter($dbAdapter)
    {
        $this->_dbAdapter = $dbAdapter;
    }
}
