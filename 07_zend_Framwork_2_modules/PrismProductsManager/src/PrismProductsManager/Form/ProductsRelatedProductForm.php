<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;

class ProductsRelatedProductForm extends Form implements EventManagerAwareInterface, InputFilterProviderInterface
{
    
    /**
     * @var EventManagerInterface
     */
    protected $events;
    
    /**
     * Declare the service locator
     * @var type 
     */
    protected $_serviceLocator;
    
    protected $_productTypes;

    protected $_status = array();


    /**
     * Initialize the form
     */
    public function init()
    {
        
    }

    /**
     * Pass the service locator and product mapper to the constructor.
     * 
     * @param type $serviceLocator
     * @param type $productMapper
     */
    public function __construct($serviceLocator)
    {
        $this->_serviceLocator = $serviceLocator;
        
        $edit = false;
                
        parent::__construct();
        $this->setAttributes(array(
                'name' => 'productRelatedProductForm',
                'id' => 'product-related-product-form',
                'role' => 'form',
                'action' => '/products/add',
            ));
        $this->setHydrator(new ClassMethods());
        $this->prepareElements();
    }
    
    /**
     * Prepare the form to be built.
     */        
    public function prepareElements()
    {
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'relatedProductId',
            'attributes' => array(
                'value' => 0,
                'id' => 'related-product-id'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'productId',
            'attributes' => array(
                'value' => 0,
                'id' => 'product-id'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'action',
            'attributes' => array(
                'value' => '',
                'id' => 'action'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'isVariant',
            'attributes' => array(
                'value' => 0,
                'id' => 'is-variant'
            )
        ));     
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'productSearch',
            'attributes' => array(
                'class' => 'form-control product-search',
                'id' => 'product-search-box',
                'placeholder' => 'Enter product name...'
            ),
            'options' => array(
                'label' => 'Product Name',
                'label_attributes' => array(
                    'for' => 'product-search'
                ),
            ),
        ));
        
        $this->add(array(
            'name' => 'saveRelatedProduct',
            'attributes' => array(
                'id' => 'save-related-product',
                'type' => 'submit',
                'value' => ' Save ',
                'class' => 'btn btn-success pull-right',
            ),
        ));
        
        $this->add(array(
            'name' => 'finishAndClose',
            'attributes' => array(
                'id' => 'finish-and-close',
                'type' => 'submit',
                'value' => ' Finish and Close ',
                'class' => 'btn btn-success pull-right margin-left',
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
        
    }
    
    /**
     * Input filter for validation
     * 
     * @return type
     */
    public function getInputFilterSpecification()
    {
        return array(
            array(
                'name' => 'relatedProductId',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities')
                ),
            ),
            array(
                'name' => 'productId',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities')
                ),
            ),
            array(
                'name' => 'isVariant',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities')
                ),
            ),
            array(
                'name' => 'productSearch',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
            ),
            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );
    }
    
    /**
     * Set the event manager instance used by this context
     *
     * @param EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;

        return $this;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }
    
}
