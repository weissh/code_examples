<?php

namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;

class ProductsActivationForm extends Form implements EventManagerAwareInterface, InputFilterProviderInterface
{
    
    /**
     * @var EventManagerInterface
     */
    protected $events;
    
    /**
     * Declare the products mapper
     * @var type 
     */
    protected $_productsMapper;
    
    /**
     * Declare the service locator
     * @var type 
     */
    protected $_serviceLocator;
    
    protected $_productTypes;

    protected $_status = array();



    /**
     * Initialize the form
     */
    public function init()
    {
        
    }

    /**
     * Pass the service locator and product mapper to the constructor.
     * 
     * @param type $serviceLocator
     * @param type $productMapper
     */
    public function __construct($serviceLocator, $productMapper)
    {
        $this->_productsMapper = $productMapper;
        $this->_serviceLocator = $serviceLocator;
        
        parent::__construct('product-activation-form');
        $this->setAttributes(array(
            'name' => 'productActivationForm',
            'id' => 'product-activation-form',
            'role' => 'form',
            'action' => '/products/add',
        ));
        $this->setHydrator(new ClassMethods());
        $this->prepareElements();
    }
    
    /**
     * Prepare the form to be built.
     */        
    public function prepareElements()
    {

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'productId',
            'attributes' => array(
                'value' => 0
            )
        ));       
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'versionId',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'version-id',
                'value' => '1',
                'readonly' => 'readonly'
            ),
            'options' => array(
                'label' => 'Version',
                'label_attributes' => array(
                    'for' => 'version-id'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'versionDescription',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'version-description',
                'rows' => 3,
            ),
            'options' => array(
                'label' => 'Version Description',
                'label_attributes' => array(
                    'for' => 'version-description'
                ),
            ),
        ));
            
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'status',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'status',
                'required' => 'required',
            ),
            'options' => array(
                'label' => "Status *",
                'label_attributes' => array(
                    'for' => 'status'
                ),
            'value_options' => array('ACTIVE'),
            ),
        ));
        
        $this->add(array(
            'name' => 'activateProduct',
            'attributes' => array(
                'id' => 'activate-product',
                'type' => 'submit',
                'value' => 'Activate Product',
                'class' => 'btn btn-success pull-right',
            ),
        ));
        
        $this->add(array(
            'name' => 'deactivateProduct',
            'attributes' => array(
                'id' => 'deactivate-product',
                'type' => 'submit',
                'value' => 'Deactivate Product',
                'class' => 'btn btn-warning pull-right',
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));
        
    }
    
    /**
     * Input filter for validation
     * 
     * @return type
     */
    public function getInputFilterSpecification()
    {
        return array(
            array(
                'name' => 'versionId',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'HtmlEntities'),
                )
            ),
            array(
                'name' => 'versionDescription',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'HtmlEntities'),
                )
            ),
            array(
                'name' => 'status',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'HtmlEntities'),
                )
            ),
            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );
    }

    /**
     * Set the event manager instance used by this context
     *
     * @param EventManagerInterface $events
     * @return mixed
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;

        return $this;
    }

    public function getEventManager()
    {
        if (!$this->events) {
            $this->setEventManager(new EventManager());
        }

        return $this->events;
    }
}
