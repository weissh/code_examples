<?php
namespace PrismProductsManager\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class AddCategoryForm
 * @package PrismProductsManager\Form
 */
class AddCategoryForm extends Form implements InputFilterProviderInterface
{

    /**
     * @var
     */
    protected $_dbAdapter;
    /**
     * Form submitted data
     */
    protected $_submittedData;
    /**
     * Flag used to validate on edit method
     */
    protected $_duplicateFlag = true;

    protected $_duplicateUrlFlag = false;

    protected $_validateCategoryNameFlag = true;

    /**
     *
     */
    public function __construct()
    {
        parent::__construct('add-category');
        $this->setAttributes(array(
            'name' => 'newCategoryForm',
            'role' => 'form',
            'action' => '/marketing-categories',
        ));
        $this->prepareElements();


    }

    /**
     * Build add / edit category form
     */
    public function prepareElements()
    {
        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'categoryId',
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'categoryParentId',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'group-parent',
                'required' => 'required',
            ),
            'options' => array(
                'label' => "Parent *",
                'label_attributes' => array(
                    'for' => 'group-parent'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'categoryName',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'category-name',
                'placeholder' => 'Product group name',
            //'required' => 'required',
            ),
            'options' => array(
                'label' => 'Product Category Name',
                'label_attributes' => array(
                    'for' => 'category-name'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'urlName',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'category-url-name',
                'placeholder' => 'Category Url Name',
            //'required' => 'required',
            ),
            'options' => array(
                'label' => 'Category Url Name',
                'label_attributes' => array(
                    'for' => 'category-url-name'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'tags',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'tags',
                'placeholder' => 'Tags',
            //'required' => 'required',
            ),
            'options' => array(
                'label' => 'Tags',
                'label_attributes' => array(
                    'for' => 'tags'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'status',
            'attributes' => array(
                'class' => 'selectize selectized',
                'id' => 'status',
            ),
            'options' => array(
                'label' => 'Status',
                'label_attributes' => array(
                    'for' => 'status'
                ),
                // this date will come from DB
                'value_options' => array(
                    'INACTIVE' => 'INACTIVE',
                    'ACTIVE' => 'ACTIVE',
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'version',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'version',
                'placeholder' => 'Version',
            //'required' => 'required',
            ),
            'options' => array(
                'label' => 'Version',
                'label_attributes' => array(
                    'for' => 'version'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'shortDescription',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'short-description',
                'rows' => 3
            ),
            'options' => array(
                'label' => 'Short Description',
                'label_attributes' => array(
                    'for' => 'short-description'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'longDescription',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'long-description',
                'rows' => 3
            ),
            'options' => array(
                'label' => 'Long Description',
                'label_attributes' => array(
                    'for' => 'long-description'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'versionDescription',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'version-description',
                'rows' => 3
            ),
            'options' => array(
                'label' => 'Version Description',
                'label_attributes' => array(
                    'for' => 'version-description'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'metaTitle',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'meta-title',
                'placeholder' => 'meta title',
            //'required' => 'required',
            ),
            'options' => array(
                'label' => 'Meta Title',
                'label_attributes' => array(
                    'for' => 'meta-title'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'metaKeyword',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'meta-keywords',
                'placeholder' => 'Meta keywords',
            //'required' => 'required',
            ),
            'options' => array(
                'label' => 'Meta Keywords',
                'label_attributes' => array(
                    'for' => 'meta-keywords'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'metaDescription',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'meta-description',
                'rows' => 3
            ),
            'options' => array(
                'label' => 'Meta Description',
                'label_attributes' => array(
                    'for' => 'meta-description'
                ),
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'websites',
            'attributes' => array(
                'multiple' => 'multiple',
                'class' => 'form-control',
                'placeholder' => 'Websites'
            ),
            'options' => array(
                'label' => 'Active on websites',
            ),
        ));

        $this->add(array(
            'name' => 'csrf',
            'type' => 'Zend\Form\Element\Csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 2700
                )
            )
        ));

        $this->add(array(
            'name' => 'submitAddCategoryForm',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'class' => 'btn btn-primary pull-right',
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'categoryBannerImage',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'categoryBannerImage',
                'onclick' => "javascript:openImageFinder([['inputRefId', 'categoryBannerImage'],['imgId', 'image-change'] ] );",
            ),
            'options' => array(
                'label' => 'Image',
                'label_attributes' => array(
                    'for' => 'categoryBannerImage'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'cancelAddCategoryForm',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Cancel',
                'data-dismiss' => 'modal',
                'class' => 'btn btn-default pull-right',
            ),
        ));
    }

    /**
     *
     */
    public function init()
    {
        //already used in __construct because eventManager usage
    }

    /**
     * Validate the form input
     * @see \Zend\InputFilter\InputFilterProviderInterface::getInputFilterSpecification()
     */
    public function getInputFilterSpecification()
    {
        $result = array(
            array(
                'name' => 'categoryParentId',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities')
                ),
            ),
            array(
                'name' => 'status',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
            ),
            array(
                'name' => 'metaTitle',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 75,
                        )
                    ),
                )
            ),
            array(
                'name' => 'metaKeyword',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 75,
                        )
                    ),
                )
            ),
            array(
                'name' => 'metaDescription',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 160,
                        )
                    ),
                )
            ),
            array(
                'name' => 'shortDescription',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 30,
                        )
                    ),
                )
            ),
            array(
                'name' => 'longDescription',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 500,
                        )
                    ),
                )
            ),

            array(
                'name' => 'websites',
                'required' => true,
            ),
            array(
                'name' => 'tagCategories',
                'required' => false,
            ),
            array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\Csrf::NOT_SAME => 'The form timed out. Please try to resubmit the form.',
                            ),
                        ),
                    ),
                ),
            ),
        );

        if ((!empty($this->_submittedData['categoryId']) && $this->_duplicateUrlFlag) || empty($this->_submittedData['categoryId'])) {
            $result[] = array(
                'name' => 'urlName',
                'required' => true,
                'filters' => array(
                    array ('name' => 'StripTags'),
                    array ('name' => 'StringTrim')
                ),
                'validators' => array(
                    array (
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 40,
                        ),
                    ),
                    array(
                        'name'    => 'Zend\Validator\Db\NoRecordExists',
                        'options' => array(
                            'table' => 'CATEGORIES_Definitions',
                            'field' => 'urlName',
                            'adapter' => $this->_dbAdapter,
                            'exclude' => array(
                                'field' => 'status',
                                'value' => 'DELETED'
                            ),
                            'messages' => array(
                                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'The url name is already is used.'
                            ),
                        ),
                    ),
                ),
            );
        } else {
            $result[] = array(
                'name' => 'urlName',
                'required' => true,
                'filters' => array(
                    array ('name' => 'StripTags'),
                    array ('name' => 'StringTrim')
                ),
                'validators' => array(
                    array (
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 3,
                            'max' => 40,
                        ),
                    ),
                ),
            );
        }
        // Check if method is edit or add
        // Disable unique category name if method is edit and new category name is the same as saved category name
        if ((!empty($this->_submittedData['categoryId']) && $this->_duplicateFlag) || (empty($this->_submittedData['categoryId']) && $this->_validateCategoryNameFlag === true)) {

            $result[] = array(
                'name' => 'categoryName',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 75,
                        )
                    ),
                    array(
                        'name'    => 'Zend\Validator\Db\NoRecordExists',
                        'options' => array(
                            'table' => 'CATEGORIES_Definitions',
                            'field' => 'categoryName',
                            'adapter' => $this->_dbAdapter,
                            'exclude' => array(
                                'field' => 'status',
                                'value' => 'DELETED'
                            ),
                            'messages' => array(
                                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'The category name already exists in database'
                            ),
                        ),
                    ),
                )
            );
        } else {
            $result[] = array(
                'name' => 'categoryName',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array ('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 75,
                        )
                    ),
                )
            );
        }
        return $result;
    }

    /**
     * @return mixed
     */
    public function getDBAdapter()
    {
        return $this->_dbAdapter;
    }

    /**
     * @param $_dbAdapter
     */
    public function setDBAdapter($_dbAdapter)
    {
        $this->_dbAdapter = $_dbAdapter;
    }
    
    /**
     * 
     * @param type $_validateCategoryNameFlag
     */
    public function setValidateCategoryNameFlag($_validateCategoryNameFlag) 
    {
        $this->_validateCategoryNameFlag = $_validateCategoryNameFlag;
    }

    /**
     * @param $_duplicateFlag
     */
    public function setDuplicateFlag($_duplicateFlag)
    {
        $this->_duplicateFlag = $_duplicateFlag;
    }

    /**
     * @param $_duplicateFlag
     */
    public function setDuplicateUrlFlag($_duplicateUrlFlag)
    {
        $this->_duplicateUrlFlag = $_duplicateUrlFlag;
    }

    /**
     * @param $_submittedData
     * @param $_duplicateFlag
     */
    public function setSubmittedData($_submittedData, $_duplicateFlag)
    {
        $this->_submittedData = $_submittedData;
        $this->_duplicateFlag = $_duplicateFlag;
    }
}
