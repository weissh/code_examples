<?php

namespace PrismProductsManager\Factory\Controller;

use PrismProductsManager\Controller\ScheduleController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ScheduleControllerFactory
 * @package PrismProductsManager\Factory\Controller
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class ScheduleControllerFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ScheduleController
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** Get the service locator */
        $serviceLocator = $serviceLocator->getServiceLocator();
        /** @var \PrismProductsManager\Service\ScheduleService */
        $scheduleService = $serviceLocator->get('PrismProductsManager\Service\ScheduleService');

        $viewRenderer = $serviceLocator->get('viewrenderer');
        /** @var \PrismProductsManager\Service\InputService $inputService */
        $inputService = $serviceLocator->get('PrismProductsManager\Service\InputService');
        $bulkActionMapper = $serviceLocator->get('PrismProductsManager\Model\BulkActionsMapper');


        return new ScheduleController(
            $scheduleService,
            $viewRenderer,
            $inputService,
            $bulkActionMapper
        );
    }
}
