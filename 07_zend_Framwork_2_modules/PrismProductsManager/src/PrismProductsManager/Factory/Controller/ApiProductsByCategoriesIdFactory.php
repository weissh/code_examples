<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\ApiProductsByCategoriesIdController;

/**
 * Api Products By Category Id Service
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiProductsByCategoriesIdFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        // inject the form to controller
        $services     = $serviceLocator->getServiceLocator();

        $apiProductsMapper =  $services->get('PrismProductsManager\Model\ApiProductsMapper');

        $controller = new ApiProductsByCategoriesIdController($apiProductsMapper);

        return $controller;
    }
}
