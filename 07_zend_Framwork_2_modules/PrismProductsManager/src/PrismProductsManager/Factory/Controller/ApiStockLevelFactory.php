<?php

namespace PrismProductsManager\Factory\Controller;

use PrismProductsManager\Controller\ApiStockLevelController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\ApiProductsByCategoriesIdController;

/**
 * Api Cache stock level from spectrum
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiStockLevelFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        // inject the form to controller
        $services     = $serviceLocator->getServiceLocator();

        $config =  $services->get('Config');

        $controller = new ApiStockLevelController($config);

        return $controller;
    }
}
