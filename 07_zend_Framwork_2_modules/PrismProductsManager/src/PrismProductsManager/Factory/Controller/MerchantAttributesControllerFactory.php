<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\MerchantAttributesController;
use PrismElasticSearch\Enum\ElasticSearchTypes;

/**
 * Class AttributesControllerFactory
 * @package PrismProductsManager\Factory\Controller
 */
class MerchantAttributesControllerFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return AttributesController
     */
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();
        $dbAdapter = $services->get('db-pms');

        $formElementManager = $services->get('FormElementManager');

        $addAttributesMerchantGroupForm = $formElementManager->get('PrismProductsManager\Form\AddAttributesMerchantGroupForm');
        $addAttributesMerchantGroupForm->setDbAdapter($dbAdapter);

        $downloadBulkTemplateForm = $formElementManager->get('PrismProductsManager\Form\DownloadBulkForm');
        $downloadUploadTemplateForm = $formElementManager->get('PrismProductsManager\Form\UploadBulkForm');

        $addAttributesColourGroupsForm = $formElementManager->get('PrismProductsManager\Form\AddAttributesColourGroupsForm');
        $addAttributesColourGroupsForm->setDbAdapter($dbAdapter);
        $addAttributesExistingColoursGroupForm = $formElementManager->get('PrismProductsManager\Form\AddAttributesExistingColoursGroupForm');
        $addAttributesExistingColoursGroupForm->setDbAdapter($dbAdapter);

        $simplePagination = $services->get('SimplePagination');
        $searchForm = $formElementManager->get('PrismProductsManager\Form\SearchForm');

        $attributesGroupMapper = $services->get('PrismProductsManager\Model\AttributesGroupMapper');
        $coloursMapper = $services->get('PrismProductsManager\Model\ColoursMapper');
        $productsMapper = $services->get('PrismProductsManager\Model\ProductsMapper');

        $queueService = $services->get('QueueService');

        $controller = new MerchantAttributesController(
            $downloadBulkTemplateForm,
            $downloadUploadTemplateForm,
            $addAttributesMerchantGroupForm,
            $addAttributesColourGroupsForm,
            $addAttributesExistingColoursGroupForm,
            $attributesGroupMapper,
            $coloursMapper,
            $searchForm,
            $simplePagination,
            $queueService,
            $productsMapper
        );
        return $controller;
    }
}
