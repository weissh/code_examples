<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\ApiProductsByCategoriesNameController;

/**
 * Api Products By Category Name Service
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiProductsByCategoriesNameFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        // inject the form to controller
        $services     = $serviceLocator->getServiceLocator();
        $apiProductsMapper =  $services->get('PrismProductsManager\Model\ApiProductsMapper');
        $categoriesMapper =  $services->get('PrismProductsManager\Model\CategoriesMapper');
        $pagesMapper =  $services->get('PrismContentManager\Model\PagesMapper');
        $controller = new ApiProductsByCategoriesNameController($apiProductsMapper, $categoriesMapper, $pagesMapper);
        return $controller;
    }
}
