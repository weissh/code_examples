<?php

namespace PrismProductsManager\Factory\Controller\DataTables;

use PrismProductsManager\Controller\DataTables\DataTablesController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ScheduleControllerFactory
 * @package PrismProductsManager\Factory\Controller
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class DataTablesControllerFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return DataTablesController
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** Get the service locator */
        $serviceLocator = $serviceLocator->getServiceLocator();

        $allConfig = $serviceLocator->get('Config');
        $dbPmsConfig = $allConfig['db']['adapters']['db-pms'];
        $productsMapper = $serviceLocator->get('PrismProductsManager\Model\ProductsMapper');

        return new DataTablesController(
            $dbPmsConfig,
            $productsMapper,
            $allConfig
        );
    }
}
