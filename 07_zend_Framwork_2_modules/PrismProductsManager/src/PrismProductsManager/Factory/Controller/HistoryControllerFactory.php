<?php

namespace PrismProductsManager\Factory\Controller;

use PrismProductsManager\Controller\HistoryController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class HistoryControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();
        $historicalService = $services->get('PrismProductsManager\Service\HistoricalService');

        $allConfig = $services->get('Config');
        $dbPmsConfig = $allConfig['db']['adapters']['db-pms'];
        $viewRenderer = $services->get('viewrenderer');

        return new HistoryController($historicalService, $dbPmsConfig, $viewRenderer);
    }
}
