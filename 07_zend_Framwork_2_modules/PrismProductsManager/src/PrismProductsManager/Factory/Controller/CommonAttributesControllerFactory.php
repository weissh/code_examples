<?php

namespace PrismProductsManager\Factory\Controller;

use PrismProductsManager\Controller\CommonAttributesController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class CommonAttributesControllerFactory
 * @package PrismProductsManager\Factory\Controller
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class CommonAttributesControllerFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return CommonAttributesController
     */
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();

        $simplePagination = $services->get('SimplePagination');
        $config = $services->get('config');
        $dbAdapter = $services->get('db-pms');
        /** Get Mappers */
        $commonAttributesMapper = $services->get('PrismProductsManager\Model\CommonAttributesMapper');
        $productsMapper = $services->get('PrismProductsManager\Model\ProductsMapper');
        /** Get Forms */
        $searchForm = $services->get('FormElementManager')->get('PrismProductsManager\Form\SearchForm');
        $commonAttributesForm = $services->get('PrismProductsManager\Form\CommonAttributesForm');
        $commonAttributesValuesForm = $services->get('PrismProductsManager\Form\CommonAttributesValuesForm');
        $territoryOverrideForm = $services->get('PrismProductsManager\Form\TerritoryOverrideForm');
        /** Get Services */
        $commonAttributesService = $services->get('PrismProductsManager\Service\CommonAttributesService');
        $viewRenderer =  $services->get('viewrenderer');
        $assignFieldsToTabsService = $services->get('PrismProductsManager\Service\AssignFieldToTabsService');


        return new CommonAttributesController(
            $simplePagination,
            $searchForm,
            $config,
            $commonAttributesMapper,
            $commonAttributesForm,
            $dbAdapter,
            $commonAttributesValuesForm,
            $viewRenderer,
            $commonAttributesService,
            $productsMapper,
            $assignFieldsToTabsService,
            $territoryOverrideForm
        );
    }
}