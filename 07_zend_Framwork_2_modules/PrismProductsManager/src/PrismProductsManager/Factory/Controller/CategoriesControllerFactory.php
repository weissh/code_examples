<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\CategoriesController;
use PrismElasticSearch\Enum\ElasticSearchTypes;

class CategoriesControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // inject the form to controller
        $services = $serviceLocator->getServiceLocator();
        $formElementManager = $services->get('FormElementManager');

        $addCategoryForm = $formElementManager->get('PrismProductsManager\Form\AddCategoryForm');
        $categoriesMapper = $services->get('PrismProductsManager\Model\CategoriesMapper');
        $simplePagination = $services->get('SimplePagination');
        $dbAdapter = $services->get('db-pms');
        $searchForm = $formElementManager->get('PrismProductsManager\Form\SearchForm');
        $addVariantToCategoryForm = $formElementManager->get('PrismProductsManager\Form\AddVariantToCategoryForm');
        $productsMapper = $services->get('PrismProductsManager\Model\ProductsMapper');
        $updateNavigationService = $services->get('PrismContentManager\Service\UpdateNavigation');
        $queueService = $services->get('QueueService');

        $config =  $services->get('CommonStorage');
        $siteConfig['siteConfig'] = $config->read('siteConfig');
        $clientConfig  = $config->read('clientConfig');
        $languageId = $siteConfig['siteConfig']['languages']['current-language'];
        $websiteId = $siteConfig['siteConfig']['websites']['current-website-id'];

        $siteurl = $siteConfig['siteConfig']['websites']['website-url'][1];

        $controller = new CategoriesController(
            $addCategoryForm,
            $categoriesMapper,
            $dbAdapter,
            $searchForm,
            $services,
            $simplePagination,
            $queueService,
            $productsMapper,
            $addVariantToCategoryForm,
            $updateNavigationService,
            $siteurl,
            $languageId,
            $websiteId,
            $siteConfig,
            $clientConfig
        );
        return $controller;
    }
}
