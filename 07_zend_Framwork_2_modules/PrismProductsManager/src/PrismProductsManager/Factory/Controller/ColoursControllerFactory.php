<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\ColoursController;

class ColoursControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $services      = $serviceLocator->getServiceLocator();

        $coloursMapper = $services->get('PrismProductsManager\Model\ColoursMapper');
        $coloursForm   = $services->get('FormElementManager')->get('PrismProductsManager\Form\ColoursForm');

        $controller    = new ColoursController($coloursMapper, $coloursForm);

        return $controller;
    }
}
