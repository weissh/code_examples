<?php

namespace PrismProductsManager\Factory\Controller;

use PrismProductsManager\Controller\AssignFieldsToProductTabsController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class AssignFieldsToProductTabsControllerFactory
 * @package PrismProductsManager\Factory\Controller
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class AssignFieldsToProductTabsControllerFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return AssignFieldsToProductTabsController
     */
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();

        $config = $services->get('config');
        /** Get Services */
        $assignFieldsToTabsService = $services->get('PrismProductsManager\Service\AssignFieldToTabsService');
        $viewRenderer =  $services->get('viewrenderer');

        /** Get Forms */
        $searchForm = $services->get('FormElementManager')->get('PrismProductsManager\Form\SearchForm');
        $assignInputFieldsForm = $services->get('FormElementManager')->get('PrismProductsManager\Form\AssignInputFieldsForm');
        $inputFieldValidationForm = $services->get('FormElementManager')->get('PrismProductsManager\Form\InputFieldValidationForm');

        return new AssignFieldsToProductTabsController(
            $config,
            $assignFieldsToTabsService,
            $viewRenderer,
            $searchForm,
            $assignInputFieldsForm,
            $inputFieldValidationForm
        );
    }
}