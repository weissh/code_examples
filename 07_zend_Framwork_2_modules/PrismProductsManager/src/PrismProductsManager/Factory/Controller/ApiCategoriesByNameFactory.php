<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\ApiCategoriesByNameController;

/**
 * Api Categories By Name Service
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiCategoriesByNameFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // inject the form to controller
        $services     = $serviceLocator->getServiceLocator();
        $apiCategoriesMapper =  $services->get('PrismProductsManager\Model\ApiCategoriesMapper');

        $pagesMapper =  $services->get('PrismContentManager\Model\PagesMapper');
        $controller = new ApiCategoriesByNameController($apiCategoriesMapper, $pagesMapper);
        return $controller;
    }
}
