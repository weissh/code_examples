<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\ApiCategoriesByIdController;

/**
 * Api Categories By Id Service
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiCategoriesByIdFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        // inject the form to controller
        $services     = $serviceLocator->getServiceLocator();

        $apiCategoriesMapper =  $services->get('PrismProductsManager\Model\ApiCategoriesMapper');

        $controller = new ApiCategoriesByIdController($apiCategoriesMapper);

        return $controller;
    }
}
