<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Form\AddCategoryFormFilter;
use PrismProductsManager\Controller\TaxController;

class TaxControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // inject the form to controller
        $services = $serviceLocator->getServiceLocator();
        $taxMapper =  $services->get('PrismProductsManager\Model\TaxMapper');
        $addNewTaxForm = $services->get('FormElementManager')->get('PrismProductsManager\Form\AddNewTaxForm');

        $config =  $services->get('CommonStorage');
        $siteConfig['siteConfig'] = $config->read('siteConfig');
        $languagesConfig = $siteConfig['siteConfig']['languages'];

        $controller = new TaxController(
            $taxMapper,
            $addNewTaxForm,
            $languagesConfig
        );

        return $controller;
    }
}
