<?php

namespace PrismProductsManager\Factory\Controller\RestfulAPI;

use PrismProductsManager\Controller\RestfulAPI\ProductDataAPIController;
use PrismProductsManager\Controller\RestfulAPI\StandardProductDataAPIController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
/**
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class StandardProductDataAPIControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // inject the form to controller
        $services     = $serviceLocator->getServiceLocator();
        $productMapper = $services->get('PrismProductsManager\Model\ProductsMapper');
        $scheduleService = $services->get('PrismProductsManager\Service\ScheduleService');
        $config = $services->get('Config');
        $commonAttributesService = $services->get('PrismProductsManager\Service\CommonAttributesService');

        return new StandardProductDataAPIController(
            $config,
            $productMapper,
            $scheduleService,
            $commonAttributesService
        );
    }
}
