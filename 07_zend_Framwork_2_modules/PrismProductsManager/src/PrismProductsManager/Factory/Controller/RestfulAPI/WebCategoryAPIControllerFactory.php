<?php

namespace PrismProductsManager\Factory\Controller\RestfulAPI;

use PrismProductsManager\Controller\RestfulAPI\WebCategoryAPIController;
use PrismProductsManager\Service\CommonAttributesService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class WebCategoryAPIControllerFactory
 *
 * @package PrismProductsManager\Factory\Controller\RestfulAPI
 */
class WebCategoryAPIControllerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return \PrismProductsManager\Controller\RestfulAPI\WebCategoryAPIController
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Zend\Mvc\Controller\ControllerManager $serviceLocator */
        /** @var \Zend\ServiceManager\ServiceManager $serviceManager */
        $serviceManager = $serviceLocator->getServiceLocator();

        $config = $serviceManager->get('Config');

        $sharedSecret = $config['sipAPI']['sharedSecret'];
        $webCategoryField = $config['clientConfig']['web-category-field'];
        $commonAttributeService = $serviceManager->get(CommonAttributesService::class);

        return new WebCategoryAPIController($sharedSecret, $webCategoryField, $commonAttributeService);
    }
}
