<?php

namespace PrismProductsManager\Factory\Controller\RestfulAPI;

use PrismProductsManager\Controller\RestfulAPI\ProductApiController;
use PrismProductsManager\Model\ProductsMapper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ProductApiControllerFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Zend\Mvc\Controller\ControllerManager $serviceLocator */
        /** @var \Zend\ServiceManager\ServiceManager $serviceManager */
        $serviceManager = $serviceLocator->getServiceLocator();
        $config = $serviceManager->get('Config');

        $sharedSecret = $config['sipAPI']['sharedSecret'];
        /** @var \PrismProductsManager\Model\ProductsMapper $productsMapper */
        $productsMapper = $serviceManager->get(ProductsMapper::class);
        $controller = new ProductApiController($sharedSecret, $productsMapper);

        return $controller;
    }
}
