<?php

namespace PrismProductsManager\Factory\Controller\RestfulAPI;

use PrismProductsManager\Controller\RestfulAPI\ProductDataAPIController;
use PrismProductsManager\Controller\RestfulAPI\ProductPriceAPIController;
use PrismProductsManager\Controller\RestfulAPI\StandardProductPriceAPIController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
/**
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class StandardProductPriceAPIControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // inject the form to controller
        $services     = $serviceLocator->getServiceLocator();
        $productMapper = $services->get('PrismProductsManager\Model\ProductsMapper');
        $scheduleService = $services->get('PrismProductsManager\Service\ScheduleService');
        $config = $services->get('Config');

        return new StandardProductPriceAPIController(
            $config,
            $productMapper,
            $scheduleService
        );
    }
}
