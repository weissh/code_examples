<?php

namespace PrismProductsManager\Factory\Controller\RestfulAPI;

use PrismProductsManager\Controller\RestfulAPI\ProductDataAPIController;
use PrismProductsManager\Controller\RestfulAPI\StandardProductDataAPIController;
use PrismProductsManager\Controller\RestfulAPI\UpdateProductAttributesController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
/**
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class UpdateProductAttributesControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // inject the form to controller
        $services     = $serviceLocator->getServiceLocator();
        $productMapper = $services->get('PrismProductsManager\Model\ProductsMapper');
        $config = $services->get('Config');
        $commonAttributesService = $services->get('PrismProductsManager\Service\CommonAttributesService');

        return new UpdateProductAttributesController(
            $config,
            $productMapper,
            $commonAttributesService
        );
    }
}
