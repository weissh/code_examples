<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\CoreAttributesController;


/**
 * Class AttributesControllerFactory
 * @package PrismProductsManager\Factory\Controller
 */
class CoreAttributesControllerFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return AttributesController
     */
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $services           = $serviceLocator->getServiceLocator();

        $attributesMapper       = $services->get('PrismProductsManager\Model\AttributesGroupMapper');
        $formElementManager     = $services->get('FormElementManager');
        $editCoreAttributesForm = $formElementManager->get('PrismProductsManager\Form\EditCoreAttributeGroupForm');
        $searchForm             = $formElementManager->get('PrismProductsManager\Form\SearchForm');
        $simplePagination       = $services->get('SimplePagination');

        $controller = new CoreAttributesController(
            $attributesMapper,
            $editCoreAttributesForm,
            $searchForm,
            $simplePagination
        );

        return $controller;
    }
}
