<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\MerchantCategoriesController;

class MerchantCategoriesControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();
        $dbAdapter = $services->get('db-pms');
        $formElementManager = $services->get('FormElementManager');

        $addMerchantCategoryForm = $formElementManager->get('PrismProductsManager\Form\AddMerchantCategoryForm');
        $merchantCategoriesMapper =  $services->get('PrismProductsManager\Model\MerchantCategoriesMapper');
        $searchForm = $formElementManager->get('PrismProductsManager\Form\SearchForm');
        $simplePagination = $services->get('SimplePagination');

        $controller = new MerchantCategoriesController(
            $dbAdapter,
            $merchantCategoriesMapper,
            $addMerchantCategoryForm,
            $searchForm,
            $simplePagination
        );

        return $controller;
    }
}
