<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\ApiVoucherProductBySpectrumId;

/**
 * Api Voucher Product by Spectrum Id Factory
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiVoucherProductBySpectrumIdFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // inject the form to controller
        $services     = $serviceLocator->getServiceLocator();
        $apiProductsMapper =  $services->get('PrismProductsManager\Model\ApiProductsMapper');
        $productFolderMapper =  $services->get('PrismMediaManager\Model\ProductFolderMapper');
        $controller = new ApiVoucherProductBySpectrumId($apiProductsMapper, $productFolderMapper);

        return $controller;
    }
}
