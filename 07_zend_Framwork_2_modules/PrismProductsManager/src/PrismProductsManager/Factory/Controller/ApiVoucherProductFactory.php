<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\ApiVoucherProduct;

/**
 * Api Voucher Product Factory
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiVoucherProductFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // inject the form to controller
        $services     = $serviceLocator->getServiceLocator();
        $apiProductsMapper =  $services->get('PrismProductsManager\Model\ApiProductsMapper');
        $productFolderMapper =  $services->get('PrismMediaManager\Model\ProductFolderMapper');
        $controller = new ApiVoucherProduct($apiProductsMapper, $productFolderMapper);

        return $controller;
    }
}
