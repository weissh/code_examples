<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\ApiCategoriesIdController;

class ApiCategoriesIdFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        // inject the form to controller
        $services     = $serviceLocator->getServiceLocator();
        $apiCategoriesMapper =  $services->get('PrismProductsManager\Model\ApiCategoriesMapper');
        $controller = new ApiCategoriesIdController($apiCategoriesMapper);
        return $controller;
    }
}
