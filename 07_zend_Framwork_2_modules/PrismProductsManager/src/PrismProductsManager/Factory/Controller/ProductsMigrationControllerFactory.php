<?php

namespace PrismProductsManager\Factory\Controller;

use PrismProductsManager\Controller\ProductsMigrationController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ProductsMigrationControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // inject the form to controller
        $services = $serviceLocator->getServiceLocator();
        $productMigrationMapper =  $services->get('ProductsMigrationMapper');

        $controller = new ProductsMigrationController(
            $productMigrationMapper

        );
        return $controller;
    }
}

