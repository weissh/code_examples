<?php

namespace PrismProductsManager\Factory\Controller;

use PrismProductsManager\Entity\BulkProcessing;
use PrismProductsManager\Controller\BulkProcessesController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class BulkProcessesControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();
        
        $em =  $services->get('Doctrine\ORM\EntityManager');
        $bulkProcessingRepository = $em->getRepository(BulkProcessing::class);
        $usersClientMapper = $services->get('UsersClientMapper');
        $controller = new BulkProcessesController($bulkProcessingRepository, $usersClientMapper);
        
        return $controller;
    }
}
