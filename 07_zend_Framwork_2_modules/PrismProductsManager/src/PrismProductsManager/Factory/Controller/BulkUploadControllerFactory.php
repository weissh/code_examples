<?php

namespace PrismProductsManager\Factory\Controller;

use PrismProductsManager\Controller\BulkUploadController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\BulkActionsController;
use PrismElasticSearch\Enum\ElasticSearchTypes;

/**
 * Class BulkUploadControllerFactory
 * @package PrismProductsManager\Factory\Controller
 */
class BulkUploadControllerFactory implements FactoryInterface
{

    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();
        $formElementManager = $services->get('FormElementManager');
        $uploadTemplateForm = $formElementManager->get('PrismProductsManager\Form\BulkUploadForm');

        $bulkActionsMapper = $services->get('PrismProductsManager\Model\BulkActionsMapper');

        return new BulkUploadController(
            $uploadTemplateForm,
            $bulkActionsMapper
        );
    }
}