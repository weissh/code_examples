<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\MarketingAttributesController;

/**
 * Class MarketingAttributesControllerFactory
 * @package PrismProductsManager\Factory\Controller
 */
class MarketingAttributesControllerFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return MarketingAttributesController
     */
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();
        $formElementManager = $services->get('FormElementManager');

        $addAttributesMarketingGroupForm = $formElementManager->get('PrismProductsManager\Form\AddAttributesMarketingGroupForm');
        $addAttributesMarketingForm = $formElementManager->get('PrismProductsManager\Form\AddAttributesMarketingForm');
        $simplePagination = $services->get('SimplePagination');
        $attributesMapper = $services->get('PrismProductsManager\Model\AttributesGroupMapper');
        $searchForm = $formElementManager->get('PrismProductsManager\Form\SearchForm');
        $productsMapper = $services->get('PrismProductsManager\Model\ProductsMapper');
        $queueService = $services->get('QueueService');

        $controller = new MarketingAttributesController(
            $addAttributesMarketingGroupForm,
            $addAttributesMarketingForm,
            $attributesMapper,
            $searchForm,
            $simplePagination,
            $queueService,
            $productsMapper
        );

        return $controller;
    }
}
