<?php

namespace PrismProductsManager\Factory\Controller;

use PrismProductsManager\Controller\NauticaliaProductsMigrationController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class NauticaliaProductsMigrationControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // inject the form to controller
        $services = $serviceLocator->getServiceLocator();
        $nauticaliaProductMigrationMapper =  $services->get('NauticaliaProductsMigrationMapper');

        $controller = new NauticaliaProductsMigrationController(
            $nauticaliaProductMigrationMapper
        );
        return $controller;
    }
}