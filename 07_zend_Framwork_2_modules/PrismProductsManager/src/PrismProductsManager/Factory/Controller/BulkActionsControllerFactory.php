<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\BulkActionsController;
use PrismElasticSearch\Enum\ElasticSearchTypes;

/**
 * Class AttributesControllerFactory
 * @package PrismProductsManager\Factory\Controller
 */
class BulkActionsControllerFactory implements FactoryInterface
{

    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();
        $dbAdapter = $services->get('db-pms');

        $config =  $services->get('CommonStorage');
        $siteConfig['siteConfig'] = $config->read('siteConfig');

        $formElementManager = $services->get('FormElementManager');
        $downloadBulkTemplateForm = $formElementManager->get('PrismProductsManager\Form\BulkDownloadForm');
        $uploadTemplateForm = $formElementManager->get('PrismProductsManager\Form\BulkUploadForm');

        // Get Mappers
        $bulkActionsMapper = $services->get('PrismProductsManager\Model\BulkActionsMapper');
        $productsMapper = $services->get('PrismProductsManager\Model\ProductsMapper');
        $queueService = $services->get('QueueService');


        $controller = new BulkActionsController(
            $downloadBulkTemplateForm,
            $uploadTemplateForm,
            $queueService,
            $bulkActionsMapper,
            $productsMapper,
            $siteConfig
        );
        return $controller;
    }





}
