<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\ApiCategoriesLeftSidebarController;

/**
 * Api Categories By Id Service
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiCategoriesLeftSidebarFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        // inject the form to controller
        $services     = $serviceLocator->getServiceLocator();

        $apiCategoriesMapper =  $services->get('PrismProductsManager\Model\ApiCategoriesMapper');
        $categoriesMapper =  $services->get('PrismProductsManager\Model\CategoriesMapper');

        $controller = new ApiCategoriesLeftSidebarController($apiCategoriesMapper, $categoriesMapper);

        return $controller;
    }
}
