<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\ProductsController;

class ProductsControllerFactory implements FactoryInterface
{
    /**
     * @var CacheAdapter
     */
    protected $cacheAdapter;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocator = $serviceLocator->getServiceLocator();
        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig['siteConfig'] = $config->read('siteConfig');
        $languagesConfig = $siteConfig['siteConfig']['languages'];
        $websitesConfig = $siteConfig['siteConfig']['websites'];
        $siteUrl = $siteConfig['siteConfig']['websites']['website-url'][1];
        $clientConfig = $config->read('clientConfig');

        $cacheAdapter = $serviceLocator->get('PrismCommon\Adapter\CacheAdapter');

        $currenciesMapper = $serviceLocator->get('PrismProductsManager\Model\CurrencyMapper');
        $currencyConfig = $this->getCurrencyConfig($currenciesMapper, $websitesConfig, $cacheAdapter);

        $productsMapper = $serviceLocator->get('PrismProductsManager\Model\ProductsMapper');
        $queueService = $serviceLocator->get('QueueService');
        $commonAttributesValuesDefinitionsTable = $serviceLocator->get('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesDefinitionsTable');
        $commonAttributesValuesTable = $serviceLocator->get('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable');
        $inputService = $serviceLocator->get('PrismProductsManager\Service\InputService');
        $commonAttributesService = $serviceLocator->get('PrismProductsManager\Service\CommonAttributesService');
        $workflowService = $serviceLocator->get('PrismProductsManager\Service\WorkflowService');
        $inputFieldsForm = $serviceLocator->get('FormElementManager')->get('PrismProductsManager\Form\InputFieldsForm');
        $coloursMapper = $serviceLocator->get('PrismProductsManager\Model\ColoursMapper');

        //Inject the mapper and forms into the controller
        return new ProductsController(
            $productsMapper,
            $languagesConfig,
            $websitesConfig,
            $currencyConfig,
            $siteUrl,
            $queueService,
            $siteConfig,
            $clientConfig,
            $commonAttributesValuesDefinitionsTable,
            $commonAttributesValuesTable,
            $inputService,
            $inputFieldsForm,
            $commonAttributesService,
            $coloursMapper,
            $workflowService,
            $cacheAdapter
        );

    }

    protected function getCurrencyConfig($currenciesMapper, $websitesConfig, $cacheAdapter)
    {
        $currencyConfig = $cacheAdapter->getCache('currencyConfig', true);
        if(null !== $currencyConfig){
            return $currencyConfig;
        }

        $currencyConfig = array();
        $currenciesArray = array();

        $currencies = $currenciesMapper->getAllCurrencies();

        foreach ($currencies as $currency) {
            $currenciesArray[$currency['currencyId']] = $currency['currency'];
        }

        foreach ($websitesConfig['websites-currencies'] as $websiteId => $currencyId) {
            $currencyConfig[$websiteId] = $currenciesArray[$currencyId];
        }

        $cacheAdapter->setCache('currencyConfig', $currencyConfig);

        return $currencyConfig;
    }
}
