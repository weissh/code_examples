<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\BarcodeUploadController;

class BarcodeUploadControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // inject the form to controller
        $services = $serviceLocator->getServiceLocator();
        $productsMapper = $services->get('PrismProductsManager\Model\ProductsMapper');
        $uploadBarcodesForm = $services->get('FormElementManager')->get('PrismProductsManager\Form\UploadBarcodesForm');
        $controller = new BarcodeUploadController($uploadBarcodesForm, $productsMapper);
        return $controller;
    }
}
