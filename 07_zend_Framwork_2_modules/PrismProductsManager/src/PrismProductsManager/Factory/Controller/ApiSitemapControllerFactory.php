<?php

namespace PrismProductsManager\Factory\Controller;

use PrismProductsManager\Controller\ApiSitemapController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ApiSitemapControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        // inject the form to controller
        $services = $serviceLocator->getServiceLocator();
        $categoryMapper = $services->get('PrismProductsManager\Model\CategoriesMapper');
        $pagesMapper =  $services->get('PrismContentManager\Model\PagesMapper');
        $config = $services->get('Config');

        $controller = new ApiSitemapController($categoryMapper, $pagesMapper, $config);
        return $controller;
    }
}
