<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\ApiCategoriesController;

/**
 * Api Categories Service
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiCategoriesFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        // inject to controller
        $services     = $serviceLocator->getServiceLocator();
        $apiCategoriesMapper =  $services->get('PrismProductsManager\Model\ApiCategoriesMapper');
        $categoriesMapper =  $services->get('PrismProductsManager\Model\CategoriesMapper');
        $controller = new ApiCategoriesController($apiCategoriesMapper, $categoriesMapper);
        return $controller;
    }
}
