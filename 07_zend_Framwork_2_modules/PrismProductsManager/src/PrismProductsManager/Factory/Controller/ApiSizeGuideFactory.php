<?php

namespace PrismProductsManager\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\ApiSizeGuideController;

class ApiSizeGuideFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        // inject the form to controller
        $services     = $serviceLocator->getServiceLocator();
        $attributesGroupMapper =  $services->get('PrismProductsManager\Model\AttributesGroupMapper');
        $controller = new ApiSizeGuideController($attributesGroupMapper);
        return $controller;
    }
}
