<?php

namespace PrismProductsManager\Factory\Controller;

use PrismElasticSearch\Enum\ElasticSearchTypes;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Controller\ApiProductsController;

class ApiProductsControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        // inject the form to controller
        $services = $serviceLocator->getServiceLocator();
        $productsMapper = $services->get('PrismProductsManager\Model\ProductsMapper');
        $queueService = $services->get('QueueService');

        $controller = new ApiProductsController($productsMapper, $queueService);
        return $controller;
    }
}
