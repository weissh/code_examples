<?php
namespace PrismProductsManager\Factory\Model;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Model\SizesShoesMapper;


class SizesShoesMapperFactory implements FactoryInterface
{
    /**
     * Create a AttributesGroupMapper class instance which will be injected with Table Model
     * PrismProductsManager\Model\AttributesGroupMapper
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \PrismProductsManager\Factory\Model\AttributesGroupMapperFactory
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('db-pms');

        $sizesShoesMapper = new SizesShoesMapper($dbAdapter);

        return $sizesShoesMapper;
    }
}