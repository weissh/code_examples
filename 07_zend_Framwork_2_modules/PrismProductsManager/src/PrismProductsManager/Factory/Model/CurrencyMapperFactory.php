<?php
namespace PrismProductsManager\Factory\Model;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Model\CurrencyMapper;


class CurrencyMapperFactory implements FactoryInterface
{
    /**
     * Create a CurrencyMapper class instance which will be injected with Table Model
     * PrismProductsManager\Model\CurrencyMapper
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \PrismProductsManager\Factory\Model\AttributesGroupMapperFactory
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('db-pms');

        $queryPaginator = $serviceLocator->get('QueryPagination');
        $currencyMapper = new CurrencyMapper($dbAdapter,$queryPaginator);

        return $currencyMapper;
    }
}