<?php
namespace PrismProductsManager\Factory\Model;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use PrismProductsManager\Model\CategoriesTable;
use PrismProductsManager\Model\Categories;
use Zend\Stdlib\Hydrator\ObjectProperty;
use Zend\Db\ResultSet\HydratingResultSet;

class CategoriesTableFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $db = $serviceLocator->get('db-pms');
        
        $resultSetPrototype = new HydratingResultSet();
        $resultSetPrototype->setHydrator(new ObjectProperty());
        $resultSetPrototype->setObjectPrototype(new Categories());
        
        $tableGateway = new TableGateway('CATEGORIES', $db, null, $resultSetPrototype);
        $table = new CategoriesTable($tableGateway);
        
        return $table;
    }
}
