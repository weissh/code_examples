<?php
namespace PrismProductsManager\Factory\Model;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Model\CategoriesMapper;
use PrismProductsManager\Model\CategoriesDefinitionsEntity;
use PrismProductsManager\Model\CategoriesEntity;
use PrismProductsManager\Model\CategoriesTagsEntity;
use PrismProductsManager\Model\CategoriesRClientsWebsitesEntity;
use Zend\Stdlib\Hydrator\ClassMethods;

class CategoriesMapperFactory implements FactoryInterface
{
    /**
     * Create a CategoriesMapper class instance which will be injected with Table Model
     * PrismProductsManager\Model\CategoriesTable
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator            
     * @return \PrismProductsManager\Factory\Model\ProductCategoriesMapper
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Retrieve the database adapters
        $dbAdapter = $serviceLocator->get('db-pms');
        
        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig = $config->read('siteConfig');
        $clientConfig = $config->read('clientConfig');

        $categoriesDefinitionsEntity = new CategoriesDefinitionsEntity();
        $categoriesEntity = new CategoriesEntity();
        $categoriesTagsEntity = new CategoriesTagsEntity();
        $categoriesWebsitesEntity = new CategoriesRClientsWebsitesEntity();

        $queryPaginator = $serviceLocator->get('QueryPagination');
        $updateNavigationService = $serviceLocator->get('PrismContentManager\Service\UpdateNavigation');
        $queueService = $serviceLocator->get('QueueService');

        $hydrator = new ClassMethods(false);
        $categoriesMapper = new CategoriesMapper(
            $dbAdapter, $siteConfig, $categoriesDefinitionsEntity, $categoriesEntity,
            $categoriesTagsEntity, $categoriesWebsitesEntity, $hydrator, $queryPaginator,
            $updateNavigationService, $queueService,$clientConfig);
        return $categoriesMapper;
    }
}
