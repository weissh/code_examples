<?php
namespace PrismProductsManager\Factory\Model;
use PrismProductsManager\Model\ApiCategoriesMapper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;



/**
 * Class ApiCategoriesMapperFactory
 * @package PrismProductsManager\Factory\Model
 */
class ApiCategoriesMapperFactory implements FactoryInterface
{
    /**
     * Create a AttributesGroupMapper class instance which will be injected with Table Model
     * PrismProductsManager\Model\AttributesGroupMapper
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \PrismProductsManager\Factory\Model\AttributesGroupMapperFactory
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('db-pms');
        $imageMapper = $serviceLocator->get('PrismMediaManager\Model\ImageMapper');

        $apiCategoriesMapper = new ApiCategoriesMapper($dbAdapter,$imageMapper);

        return $apiCategoriesMapper;
    }
}