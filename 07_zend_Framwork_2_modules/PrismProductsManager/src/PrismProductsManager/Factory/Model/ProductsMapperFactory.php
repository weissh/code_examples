<?php
namespace PrismProductsManager\Factory\Model;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Model\ProductsMapper;
use Zend\Session\Container;


class ProductsMapperFactory implements FactoryInterface
{
    /**
     * Create a ProductsMapper class instance which will be injected with Table Model
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \PrismProductsManager\Factory\Model\ProductsMapper
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Retrieve the database adapters
        $dbAdapter = $serviceLocator->get('db-pms');

        //Retrieve the languages
        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig = $config->read('siteConfig');
        $clientConfig = $config->read('clientConfig');

        $client = $config->read('spectrumAPI');
        $clientCode = $client['client'];

        //Loged in user
        //$logedInUserDetails = $config->read('storage');
        //var_dump($logedInUserDetails); die;
        $session = new Container('prism_cms');
        $logedInuserDetails = json_decode($session->offsetGet('storage'), true);
        $logedInUserId = $logedInuserDetails['userId'];


        //Get the attribute group mapper
        $attributesGroupMapper = $serviceLocator->get('PrismProductsManager\Model\AttributesGroupMapper');

        //Get the attribute group mapper
        $categoriesMapper = $serviceLocator->get('PrismProductsManager\Model\CategoriesMapper');

        //Get the attribute group mapper
        $actionLogger = $serviceLocator->get('PrismCommon\Factory\Service\ActionLoggerFactory');

        //Get the merchant group mapper
        $merchantCategoriesMapper = $serviceLocator->get('PrismProductsManager\Model\MerchantCategoriesMapper');
        $queryPaginator = $serviceLocator->get('QueryPagination');
        $taxMapper = $serviceLocator->get('PrismProductsManager\Model\TaxMapper');
        $productFolderMapper = $serviceLocator->get('PrismMediaManager\Model\ProductFolderMapper');
        $mediaServer = $serviceLocator->get('PrismMediaManager\Service\RSMapper');

        $commonAttributesValuesTable = $serviceLocator->get('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable');
        $inputService = $serviceLocator->get('PrismProductsManager\Service\InputService');
        $coloursMapper = $serviceLocator->get('PrismProductsManager\Model\ColoursMapper');
        $commonAttributesService = $serviceLocator->get('PrismProductsManager\Service\CommonAttributesService');
        $queueService = $serviceLocator->get('QueueService');
        $mailSender = $serviceLocator->get('MailSender');

        $config     = $serviceLocator->get('config');

        $historicalService = $serviceLocator->get('PrismProductsManager\Service\HistoricalService');
        $loggingService = $serviceLocator->get('LoggingService');


        //Get the categories mapper
        $productsMapper = new ProductsMapper(
            $dbAdapter,
            $siteConfig,
            $attributesGroupMapper,
            $categoriesMapper,
            $merchantCategoriesMapper,
            $actionLogger,
            $queryPaginator,
            $taxMapper,
            $productFolderMapper,
            $mediaServer,
            $logedInUserId,
            $commonAttributesValuesTable,
            $inputService,
            $coloursMapper,
            $commonAttributesService,
            $clientCode,
            $queueService,
            $clientConfig,
            $mailSender,
            $config,
            $historicalService,
            $loggingService
        );
        return $productsMapper;
    }
}
