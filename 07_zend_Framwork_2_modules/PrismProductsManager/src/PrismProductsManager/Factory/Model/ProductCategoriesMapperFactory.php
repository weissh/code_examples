<?php
namespace PrismProductsManager\Factory\Model;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Model\ProductCategoriesMapper;

class ProductCategoriesMapperFactory implements FactoryInterface
{
    /**
     * Create a ProductCategoriesMapper class instance which will be injected with Table Model
     * PrismProductsManager\Model\CategoriesTable
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator            
     * @return \PrismProductsManager\Factory\Model\ProductCategoriesMapper
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $categories = $serviceLocator->get('PrismProductsManager\Model\CategoriesTable');

        return new ProductCategoriesMapper($categories);
    }
}
