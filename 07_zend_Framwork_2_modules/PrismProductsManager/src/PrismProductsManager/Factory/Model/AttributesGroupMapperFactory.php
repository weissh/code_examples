<?php
namespace PrismProductsManager\Factory\Model;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Model\AttributesGroupMapper;
use PrismProductsManager\Model\AttributesGroupEntity;
use PrismProductsManager\Model\AttributesGroupDefinitionsEntity;
use PrismProductsManager\Model\AttributesMarketingEntity;
use PrismProductsManager\Model\AttributesMarketingDefinitionsEntity;
use PrismProductsManager\Model\AttributesMerchantsEntity;
use PHPExcel;


/**
 * Class AttributesGroupMapperFactory
 * @package PrismProductsManager\Factory\Model
 */
class AttributesGroupMapperFactory implements FactoryInterface
{
    /**
     * Create a AttributesGroupMapper class instance which will be injected with Table Model
     * PrismProductsManager\Model\AttributesGroupMapper
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \PrismProductsManager\Factory\Model\AttributesGroupMapperFactory
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('db-pms');

        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig['siteConfig'] = $config->read('siteConfig');
        $language = $siteConfig['siteConfig']['languages'];

        $attributeGroupEntity = new AttributesGroupEntity();
        $attributesGroupDefinitionsEntity = new AttributesGroupDefinitionsEntity();
        $attributesMarketingEntity = new AttributesMarketingEntity();
        $attributesMarketingDefinitionsEntity = new AttributesMarketingDefinitionsEntity();
        $attributesMerchantsEntity = new AttributesMerchantsEntity();

        $excelReader = new \PHPExcel();

        $rsMapper = $serviceLocator->get('PrismMediaManager\Service\RSMapper');

        $attributesGroupMapper = new AttributesGroupMapper(
            $attributeGroupEntity, $attributesGroupDefinitionsEntity,
            $attributesMarketingEntity, $attributesMarketingDefinitionsEntity,
            $attributesMerchantsEntity, $dbAdapter, $language, $excelReader, $rsMapper);

        return $attributesGroupMapper;
    }
}