<?php
namespace PrismProductsManager\Factory\Model;
use PrismProductsManager\Model\ProductsMapperPrices;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Model\BulkActionsMapper;
use Zend\Db\Sql\Sql;
use Zend\Stdlib\Hydrator\ClassMethods;
use PHPExcel;


/**
 * Class AttributesGroupMapperFactory
 * @package PrismProductsManager\Factory\Model
 */
class BulkActionsMapperFactory implements FactoryInterface
{
    /**
     * Create a AttributesGroupMapper class instance which will be injected with Table Model
     * PrismProductsManager\Model\AttributesGroupMapper
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \PrismProductsManager\Factory\Model\AttributesGroupMapperFactory
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        $dbAdapter = $serviceLocator->get('db-pms');
        $sql = new Sql($dbAdapter);
        $hydrator = new ClassMethods();

        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig['siteConfig'] = $config->read('siteConfig');
        $language = $siteConfig['siteConfig']['languages'];

        $queueService = $serviceLocator->get('QueueService');

        $productPriceMapper = new ProductsMapperPrices($sql,$hydrator,$dbAdapter);


        $excelReader = new \PHPExcel();
        $commonAttributesService = $serviceLocator->get('PrismProductsManager\Service\CommonAttributesService');
        $productsMapper = $serviceLocator->get('PrismProductsManager\Model\ProductsMapper');

        $scheduleService = $serviceLocator->get('PrismProductsManager\Service\ScheduleService');

        $bulkActionsMapper = new BulkActionsMapper(
            $dbAdapter,
            $language,
            $excelReader,
            $siteConfig,
            $productPriceMapper,
            $queueService,
            $commonAttributesService,
            $productsMapper,
            $scheduleService
            );

        return $bulkActionsMapper;
    }
}