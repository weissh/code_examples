<?php
namespace PrismProductsManager\Factory\Model;

use PrismProductsManager\Model\NauticaliaProductsMigrationMapper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class NauticaliaProductsMigrationMapperFactory implements FactoryInterface
{
    /**
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \PrismProductsManager\Factory\Model\ProductsMapper
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Retrieve the database adapters
        $config = $serviceLocator->get('Config');

        $dbAdapter = $serviceLocator->get('db-pms');
        $oldPMSAdapter = $serviceLocator->get('old-pms');
        $adminAdapter = $serviceLocator->get('db-admin');
        $spectrumAdapter = $serviceLocator->get('db-spectrum');
        $productFolderMapper = $serviceLocator->get('PrismMediaManager\Model\ProductFolderMapper');
        $customerService = $serviceLocator->get('PrismSpectrumApiClient\Service\Customer');
        $productsMapper = new NauticaliaProductsMigrationMapper(
            $config,
            $dbAdapter,
            $oldPMSAdapter,
            $adminAdapter,
            $spectrumAdapter,
            $productFolderMapper,
            $customerService
        );
        return $productsMapper;
    }
}