<?php
namespace PrismProductsManager\Factory\Model;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Model\MerchantCategoriesMapper;
use PrismProductsManager\Model\MerchantCategoriesEntity;
use PrismProductsManager\Model\MerchantCategoriesDefinitionsEntity;
use PrismProductsManager\Model\SkuRulesEntity;
use PrismProductsManager\Model\CategoriesEntity;

class MerchantCategoriesMapperFactory implements FactoryInterface
{
    /**
     * Create a MerchantCategoriesMapper class instance which will be injected with Table Model
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator            
     * @return \PrismProductsManager\Factory\Model\ProductCategoriesMapper
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Retrieve the database adapters
        $dbAdapter = $serviceLocator->get('db-pms');
        
        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig = $config->read('siteConfig');

        //Get the attribute group mapper
        $attributesGroupMapper = $serviceLocator->get('PrismProductsManager\Model\AttributesGroupMapper');
        $commonAttributesTable = $serviceLocator->get('PrismProductsManager\Model\CommonAttributes\CommonAttributesTable');
        $commonAttributesValuesTable = $serviceLocator->get('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable');
        $commonAttributesValuesDefinitionsTable = $serviceLocator->get('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesDefinitionsTable');

        $merchantCategoriesDefinitionsEntity = new MerchantCategoriesDefinitionsEntity();

        $merchantCategoriesEntity = new MerchantCategoriesEntity();

        $skuRulesEntity = new SkuRulesEntity();

        $merchantCategoriesMapper = new MerchantCategoriesMapper(
            $dbAdapter, $siteConfig, $attributesGroupMapper,
            $merchantCategoriesDefinitionsEntity, $merchantCategoriesEntity,
            $skuRulesEntity, $commonAttributesTable, $commonAttributesValuesTable, $commonAttributesValuesDefinitionsTable);

        return $merchantCategoriesMapper;
    }
}
