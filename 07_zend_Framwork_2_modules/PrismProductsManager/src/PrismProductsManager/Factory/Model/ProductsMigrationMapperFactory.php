<?php
namespace PrismProductsManager\Factory\Model;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Model\ProductsMigrationMapper;

class ProductsMigrationMapperFactory implements FactoryInterface
{
    /**
     * Create a ProductsMapper class instance which will be injected with Table Model
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator            
     * @return \PrismProductsManager\Factory\Model\ProductsMapper
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Retrieve the database adapters
        $config = $serviceLocator->get('Config');

        $dbAdapter = $serviceLocator->get('db-pms');
        $oldPMSAdapter = $serviceLocator->get('old-pms');
        $adminAdapter = $serviceLocator->get('db-admin');
        $spectrumAdapter = $serviceLocator->get('db-spectrum');
        $productFolderMapper = $serviceLocator->get('PrismMediaManager\Model\ProductFolderMapper');
        $customerService = $serviceLocator->get('PrismSpectrumApiClient\Service\Customer');
        $productsMapper = $serviceLocator->get('PrismProductsManager\Model\ProductsMapper');
        $productsMigrationMapper = new ProductsMigrationMapper(
            $config,
            $dbAdapter,
            $oldPMSAdapter,
            $adminAdapter,
            $spectrumAdapter,
            $productFolderMapper,
            $customerService,
            $productsMapper
        );
        return $productsMigrationMapper;
    }
}
