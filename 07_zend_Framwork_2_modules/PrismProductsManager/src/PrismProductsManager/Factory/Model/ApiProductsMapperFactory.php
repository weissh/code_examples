<?php
namespace PrismProductsManager\Factory\Model;
use PrismProductsManager\Model\ApiProductsMapper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;



/**
 * Class ApiCategoriesMapperFactory
 * @package PrismProductsManager\Factory\Model
 */
class ApiProductsMapperFactory implements FactoryInterface
{
    /**
     * Create a AttributesGroupMapper class instance which will be injected with Table Model
     * PrismProductsManager\Model\AttributesGroupMapper
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \PrismProductsManager\Factory\Model\AttributesGroupMapperFactory
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('db-pms');

        //Get the attribute group mapper
        $productsMapper = $serviceLocator->get('PrismProductsManager\Model\ProductsMapper');

        $apiProductsMapper = new ApiProductsMapper($dbAdapter, $productsMapper);

        return $apiProductsMapper;
    }
}