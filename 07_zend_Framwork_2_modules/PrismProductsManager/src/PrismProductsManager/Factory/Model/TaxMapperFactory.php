<?php
namespace PrismProductsManager\Factory\Model;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Model\TaxMapper;

class TaxMapperFactory implements FactoryInterface
{
    /**
     * Create a TaxMapper class instance which will be injected with Table Model
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator            
     * @return \PrismProductsManager\Factory\Model\TaxMapper
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Retrieve the database adapters
        $dbAdapter = $serviceLocator->get('db-pms');

        //Retrieve the languages
        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig = $config->read('siteConfig');
        $languages = $siteConfig['languages']['site-languages'];

        //Get the categories mapper
        $taxMapper = new TaxMapper(
            $dbAdapter,
            $serviceLocator,
            $languages
        );
        return $taxMapper;
    }
}
