<?php
namespace PrismProductsManager\Factory\Model;

use PrismProductsManager\Model\CommonAttributesMapper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class CommonAttributesMapperFactory
 * @package PrismProductsManager\Factory\Model
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class CommonAttributesMapperFactory implements FactoryInterface
{

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return CommonAttributesMapper
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        $dbAdapter = $serviceLocator->get('db-pms');

        $config =  $serviceLocator->get('CommonStorage');
        $queueService = $serviceLocator->get('QueueService');

        $siteConfig['siteConfig'] = $config->read('siteConfig');
        $clientConfig = $config->read('clientConfig');
        $language = $siteConfig['siteConfig']['languages'];



        return new CommonAttributesMapper($dbAdapter, $clientConfig, $language, $queueService);
    }
}