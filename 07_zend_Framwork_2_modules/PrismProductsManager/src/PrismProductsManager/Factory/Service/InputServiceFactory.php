<?php
namespace PrismProductsManager\Factory\Service;

use Zend\Db\Sql\Sql;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Service\InputService;
use Zend\Session\Container;


class InputServiceFactory implements FactoryInterface
{
    /**
     * Create a InputServiceFactory class instance which will be injected with Table Model
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \PrismProductsManager\Factory\Model\InputService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Retrieve the database adapters
        $dbAdapter = $serviceLocator->get('db-pms');
        $sql = new Sql($dbAdapter);

        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig = $config->read('siteConfig');
        $doctrineEntity = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $commonAttributesMapper = $serviceLocator->get('PrismProductsManager\Model\CommonAttributesMapper');

        $attributesGroupMapper = $serviceLocator->get('PrismProductsManager\Model\AttributesGroupMapper');
        $cacheAdapter = $serviceLocator->get('PrismCommon\Adapter\CacheAdapter');

        return new InputService(
            $dbAdapter,
            $siteConfig,
            $doctrineEntity,
            $sql,
            $commonAttributesMapper,
            $attributesGroupMapper,
            $cacheAdapter
        );
    }
}
