<?php
namespace PrismProductsManager\Factory\Service;

use PrismProductsManager\Entity\ScheduleAttributes;
use PrismProductsManager\Entity\SchedulePrices;
use PrismProductsManager\Entity\Schedules;
use PrismProductsManager\Entity\ScheduleTrail;
use PrismProductsManager\Entity\ScheduleTypes;
use PrismProductsManager\Service\ScheduleService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ScheduleServiceFactory
 * @package PrismProductsManager\Factory\Service
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class ScheduleServiceFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ScheduleService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Doctrine\ORM\EntityManager $doctrineEntity */
        $doctrineEntity = $serviceLocator->get('doctrine.entitymanager.orm_default');
        /** @var \PrismProductsManager\Model\ProductsMapper $productsMapper  */
        $productsMapper = $serviceLocator->get('PrismProductsManager\Model\ProductsMapper');
        /** @var \PrismProductsManager\Model\AttributesGroupMapper $attributesMapper  */
        $attributesMapper = $serviceLocator->get('PrismProductsManager\Model\AttributesGroupMapper');

        /** @var \Doctrine\Common\Persistence\ObjectRepository $schedulesRepository */
        $schedulesRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\Schedules');
        /** @var \Doctrine\Common\Persistence\ObjectRepository $schedulePricesRepository */
        $schedulePricesRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\SchedulePrices');
        /** @var \Doctrine\Common\Persistence\ObjectRepository $scheduleTrailRepository */
        $scheduleTrailRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\ScheduleTrail');
        /** @var \Doctrine\Common\Persistence\ObjectRepository $scheduleTypesRepository */
        $scheduleTypesRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\ScheduleTypes');
        /** @var \Doctrine\Common\Persistence\ObjectRepository $scheduleTypesRepository */
        $territoriesRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\Territories');
        /** @var \Doctrine\Common\Persistence\ObjectRepository $scheduleTypesRepository */
        $currencyRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\Currency');

        /** @var \PrismProductsManager\Entity\Schedules $schedulesEntity */
        $schedulesEntity = new Schedules();
        /** @var \PrismProductsManager\Entity\SchedulePrices $schedulePricesEntity */
        $schedulePricesEntity = new SchedulePrices();
        /** @var \PrismProductsManager\Entity\ScheduleTrail $scheduleTrailEntity */
        $scheduleTrailEntity = new ScheduleTrail();
        /** @var \PrismProductsManager\Entity\ScheduleTypes $scheduleTypesEntity */
        $scheduleTypesEntity = new ScheduleTypes();
        /** @var \PrismProductsManager\Entity\ScheduleAttributes $scheduleAttributesEntity */
        $scheduleAttributesEntity = new ScheduleAttributes();
        /** @var \PrismProductsManager\Service\InputService $inputService */
        $inputService = $serviceLocator->get('PrismProductsManager\Service\InputService');
        /** @var \PrismProductsManager\Service\CommonAttributesService $commonAttributesService */
        $commonAttributesService = $serviceLocator->get('PrismProductsManager\Service\CommonAttributesService');
        /** @var \PrismQueueManager\Service\QueueService $queueService */
        $queueService = $serviceLocator->get('QueueService');
        /** @var \Zend\Db\Adapter\Adapter $dbAdapter */
        $dbAdapter = $serviceLocator->get('db-pms');

        return new ScheduleService(
            $doctrineEntity,
            $productsMapper,
            $attributesMapper,
            $schedulesRepository,
            $schedulePricesRepository,
            $scheduleTrailRepository,
            $scheduleTypesRepository,
            $territoriesRepository,
            $currencyRepository,
            $schedulesEntity,
            $schedulePricesEntity,
            $scheduleTrailEntity,
            $scheduleTypesEntity,
            $scheduleAttributesEntity,
            $inputService,
            $commonAttributesService,
            $queueService,
            $dbAdapter
        );
    }
}