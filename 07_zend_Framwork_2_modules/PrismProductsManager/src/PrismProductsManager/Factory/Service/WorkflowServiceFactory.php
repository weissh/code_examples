<?php
namespace PrismProductsManager\Factory\Service;

use PrismProductsManager\Service\WorkflowService;
use Zend\Db\Sql\Sql;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class WorkflowServiceFactory implements FactoryInterface
{
    /**
     * Create a WorkflowServiceFactory class
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \PrismProductsManager\Service\WorkflowService
     * @author  Hani Weiss <hani.weiss@whistl.co.uk>
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig = $config->read('siteConfig');
        $doctrineEntity = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $productsMapper = $serviceLocator->get('PrismProductsManager\Model\ProductsMapper');

        $assignFieldsToTabsService = $serviceLocator->get('PrismProductsManager\Service\AssignFieldToTabsService');
        $inputService = $serviceLocator->get('PrismProductsManager\Service\InputService');

        $dbAdapter = $serviceLocator->get('db-pms');
        $sql = new Sql($dbAdapter);

        //Get the categories mapper
        return new WorkflowService(
            $productsMapper,
            $assignFieldsToTabsService,
            $inputService,
            $doctrineEntity,
            $dbAdapter,
            $sql
        );
    }
}