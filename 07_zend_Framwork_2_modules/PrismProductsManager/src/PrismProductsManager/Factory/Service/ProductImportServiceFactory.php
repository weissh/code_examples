<?php

namespace PrismProductsManager\Factory\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use PrismProductsManager\Service\ProductImportService;

class ProductImportServiceFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ProductImportService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Zend\Db\Adapter\Adapter $pmsDbAdapter */
        $pmsDbAdapter = $serviceLocator->get('db-pms');
        /** @var \PrismProductsManager\Service\CommonAttributesService $commonAttributesService */
        $commonAttributesService = $serviceLocator->get('PrismProductsManager\Service\CommonAttributesService');
        /** @var \PrismProductsManager\Model\ColoursMapper $coloursMapper */
        $coloursMapper = $serviceLocator->get('PrismProductsManager\Model\ColoursMapper');

        return new ProductImportService(
            $pmsDbAdapter,
            $commonAttributesService,
            $coloursMapper
        );
    }
}