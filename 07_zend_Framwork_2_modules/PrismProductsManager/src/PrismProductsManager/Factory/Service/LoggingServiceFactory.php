<?php

namespace PrismProductsManager\Factory\Service;

use PrismProductsManager\Service\LoggingService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LoggingServiceFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return LoggingService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $doctrineEntity = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $loggedUpdateRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\LoggedUpdate');

        return new LoggingService(
            $doctrineEntity,
            $loggedUpdateRepository
        );
    }
}