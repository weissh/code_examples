<?php
namespace PrismProductsManager\Factory\Service;

use PrismProductsManager\Entity\InputFieldRAttributes;
use PrismProductsManager\Entity\InputFieldRPages;
use PrismProductsManager\Entity\InputFieldRValidations;
use PrismProductsManager\Entity\InputFields;
use PrismProductsManager\Entity\InputFieldsets;
use PrismProductsManager\Entity\InputFieldTypes;
use PrismProductsManager\Entity\InputPages;
use PrismProductsManager\Entity\InputValidations;
use PrismProductsManager\Entity\InputValidationTypes;
use PrismProductsManager\Service\AssignFieldToTabsService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AssignFieldToTabsServiceFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig = $config->read('siteConfig');
        $doctrineEntity = $serviceLocator->get('doctrine.entitymanager.orm_default');

        /** Get Services */
        $commonAttributesService = $serviceLocator->get('PrismProductsManager\Service\CommonAttributesService');
        /** Get Mappers */
        $attributesMapper = $serviceLocator->get('PrismProductsManager\Model\AttributesGroupMapper');

        /** Get Entity repositories */
        $inputFieldsRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\InputFields');
        $inputFieldTypesRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\InputFieldTypes');
        $inputPagesRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\InputPages');
        $inputFieldsetsRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\InputFieldsets');
        $inputFieldRAttributesRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\InputFieldRAttributes');
        $attributesGroupsRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\AttributesGroups');
        $inputFieldValidationsRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\InputValidations');
        $inputFieldValidationTypesRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\InputValidationTypes');
        $workflowsRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\Workflows');
        $inputFieldRPagesRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\InputFieldRPages');

        /** Get Entities */
        $inputFieldsEntity = new InputFields();
        $inputFieldTypesEntity = new InputFieldTypes();
        $inputPagesEntity = new InputPages();
        $inputFieldsetsEntity = new InputFieldsets();
        $inputFieldRAttributesEntity = new InputFieldRAttributes();
        $inputFieldRPagesEntity = new InputFieldRPages();
        $inputFieldValidationsEntity = new InputValidations();
        $inputFieldValidationTypesEntity = new InputValidationTypes();

        return new AssignFieldToTabsService(
            $siteConfig,
            $doctrineEntity,
            $inputFieldsRepository,
            $inputFieldTypesRepository,
            $inputPagesRepository,
            $inputFieldsetsRepository,
            $inputFieldRAttributesRepository,
            $attributesGroupsRepository,
            $inputFieldValidationsRepository,
            $inputFieldValidationTypesRepository,
            $workflowsRepository,
            $inputFieldRPagesRepository,
            $inputFieldsEntity,
            $inputFieldTypesEntity,
            $inputPagesEntity,
            $inputFieldsetsEntity,
            $inputFieldRAttributesEntity,
            $inputFieldRPagesEntity,
            $inputFieldValidationsEntity,
            $inputFieldValidationTypesEntity,
            $commonAttributesService,
            $attributesMapper
        );
    }
}
