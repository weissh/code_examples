<?php
namespace PrismProductsManager\Factory\Service;

use PrismProductsManager\Entity\CommonAttributes;
use PrismProductsManager\Entity\CommonAttributesDefinitions;
use PrismProductsManager\Entity\CommonAttributesGuiRepresentation;
use PrismProductsManager\Entity\CommonAttributesMapping;
use PrismProductsManager\Entity\CommonAttributesValues;
use PrismProductsManager\Entity\CommonAttributesValuesChain;
use PrismProductsManager\Entity\CommonAttributesValuesDefinitions;
use PrismProductsManager\Entity\CommonAttributesValuesEquivalence;
use PrismProductsManager\Entity\CommonAttributesValuesGuiRepresentation;
use PrismProductsManager\Service\CommonAttributesService;
use PrismProductsManager\Service\HistoricalService;
use Zend\Db\Sql\Sql;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class HistoricalServiceFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('db-pms');

        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig = $config->read('siteConfig');
        $entityManager = $serviceLocator->get('doctrine.entitymanager.orm_default');

        $commonAttributesService = $serviceLocator->get('PrismProductsManager\Service\CommonAttributesService');
        $authService = $serviceLocator->get('AuthService');

        $usersClientMapper = $serviceLocator->get('UsersClientMapper');

        //Get the categories mapper
        return new HistoricalService(
            $entityManager,
            $commonAttributesService,
            $authService,
            $dbAdapter,
            $usersClientMapper
        );
    }
}