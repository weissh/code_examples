<?php
namespace PrismProductsManager\Factory\Service;

use PrismProductsManager\Entity\CommonAttributes;
use PrismProductsManager\Entity\CommonAttributesDefinitions;
use PrismProductsManager\Entity\CommonAttributesGuiRepresentation;
use PrismProductsManager\Entity\CommonAttributesMapping;
use PrismProductsManager\Entity\CommonAttributesValues;
use PrismProductsManager\Entity\CommonAttributesValuesChain;
use PrismProductsManager\Entity\CommonAttributesValuesDefinitions;
use PrismProductsManager\Entity\CommonAttributesValuesEquivalence;
use PrismProductsManager\Entity\CommonAttributesValuesGuiRepresentation;
use PrismProductsManager\Service\CommonAttributesService;
use Zend\Db\Sql\Sql;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class CommonAttributesServiceFactory implements FactoryInterface
{
    /**
     * Create a CommonAttributesService class
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \PrismProductsManager\Service\CommonAttributesService
     * @author  Hani Weiss <hani.weiss@whistl.co.uk>
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig = $config->read('siteConfig');
        $doctrineEntity = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $commonAttributesMapper = $serviceLocator->get('PrismProductsManager\Model\CommonAttributesMapper');
        /** @var \Zend\Db\Adapter\AdapterInterface */
        $dbAdapter = $serviceLocator->get('db-pms');
        $sql = new Sql($dbAdapter);

        /** Get Entity repositories */
        $commonAttributesEntityRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\CommonAttributes');
        $commonAttributesViewTypeEntityRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\CommonAttributesViewType');
        $commonAttributesValuesRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\CommonAttributesValues');
        $commonAttributesValuesGuiRepresentationRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\CommonAttributesValuesGuiRepresentation');
        $commonAttributesValuesEquivalenceRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\CommonAttributesValuesEquivalence');
        $commonAttributesValuesChainRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\CommonAttributesValuesChain');
        $commonAttributesDefinitionsRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\CommonAttributesDefinitions');
        $territoriesRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\Territories');

        /** Get Entities */
        $commonAttributeEntity = new CommonAttributes();
        $commonAttributeDefinitionEntity = new CommonAttributesDefinitions();
        $commonAttributeGUIRepresentationEntity = new CommonAttributesGuiRepresentation();
        $commonAttributeValuesEntity = new CommonAttributesValues();
        $commonAttributeValuesDefinitionEntity = new CommonAttributesValuesDefinitions();
        $commonAttributeValuesEquivalenceEntity = new CommonAttributesValuesEquivalence();
        $commonAttributeValuesGuiRepresentationEntity = new CommonAttributesValuesGuiRepresentation();
        $commonAttributeValuesChain = new CommonAttributesValuesChain();
        $commonAttributeMapping = new CommonAttributesMapping();

        //Get the categories mapper
        return new CommonAttributesService(
            $siteConfig,
            $doctrineEntity,
            $commonAttributesMapper,
            $commonAttributesEntityRepository,
            $commonAttributesViewTypeEntityRepository,
            $commonAttributeEntity,
            $commonAttributeDefinitionEntity,
            $commonAttributeGUIRepresentationEntity,
            $commonAttributeValuesEntity,
            $commonAttributeValuesDefinitionEntity,
            $commonAttributeValuesEquivalenceEntity,
            $commonAttributeValuesGuiRepresentationEntity,
            $commonAttributeMapping,
            $commonAttributesValuesRepository,
            $commonAttributesValuesGuiRepresentationRepository,
            $commonAttributesValuesEquivalenceRepository,
            $commonAttributeValuesChain,
            $commonAttributesValuesChainRepository,
            $commonAttributesDefinitionsRepository,
            $territoriesRepository,
            $sql
        );
    }
}