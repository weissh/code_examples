<?php

namespace PrismProductsManager\Factory\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use PrismProductsManager\Service\ProductsUpdateService;
//use PrismElasticSearch\Enum\ElasticSearchTypes;

class ProductsUpdateServiceFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ProductsUpdateService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config =  $serviceLocator->get('CommonStorage');
        $originalConfig = $serviceLocator->get('config');
        $siteConfig['siteConfig'] = $config->read('siteConfig');

        //Get some mappers that will be injected into the controller
        $queueService = $serviceLocator->get('QueueService');

        /** @var $elasticIndexingService \PrismElasticSearch\Service\Manager\ElasticSearchIndexingManager */
//        $elasticSearchIndexingService = $serviceLocator->get('ElasticSearchIndexingManager');
//        $elasticSearchIndexingService->setupIndexCategory(ElasticSearchTypes::PRODUCTS);
        /** @var $elasticIndexingService \PrismElasticSearch\Service\Base\ElasticSearchBaseIndexer */


        return new ProductsUpdateService(
            $queueService, //,
            $siteConfig,
            $originalConfig
            // $elasticSearchIndexingService
        );

    }
}
