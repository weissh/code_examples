<?php
/**
 * Created by PhpStorm.
 * User: radug
 * Date: 02/03/16
 * Time: 16:35
 */

namespace PrismProductsManager\Factory\Command;

use PrismProductsManager\Command\ProductImportCommand;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ProductImportCommandFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // inject the form to controller
        $services = $serviceLocator->getServiceLocator();

        ;
        return new ProductImportCommand($services->get('ProductImport'));
    }
}