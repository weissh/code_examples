<?php

namespace PrismProductsManager\Factory\Command;

use PrismProductsManager\Model\BulkActionsMapper;
use PrismProductsManager\Command\BulkErrorReportCommand;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class BulkErrorReportCommandFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();
        
        $entityManager = $services->get('Doctrine\ORM\EntityManager');
        $mailSender = $services->get('MailSender');
        $config     = $services->get('config');
        
        return new BulkErrorReportCommand(
            $entityManager->getRepository('PrismProductsManager\Entity\BulkProcessing'),
            $mailSender,
            $config['bulk-error-report']
        );
    }
}