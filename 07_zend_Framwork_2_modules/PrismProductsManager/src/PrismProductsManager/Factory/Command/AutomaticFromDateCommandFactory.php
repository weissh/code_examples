<?php

namespace PrismProductsManager\Factory\Command;

use PrismProductsManager\Command\AutomaticFromDateCommand;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AutomaticFromDateCommandFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();
        $historicalService = $services->get('PrismProductsManager\Service\HistoricalService');

        return new AutomaticFromDateCommand($historicalService);
    }
}