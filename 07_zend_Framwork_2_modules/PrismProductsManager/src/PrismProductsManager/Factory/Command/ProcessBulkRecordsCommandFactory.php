<?php

namespace PrismProductsManager\Factory\Command;

use PrismProductsManager\Model\BulkActionsMapper;
use PrismProductsManager\Command\ProcessBulkRecordsCommand;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ProcessBulkRecordsCommandFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();
        return new ProcessBulkRecordsCommand(
            $services->get(BulkActionsMapper::class)
        );
    }
}