<?php

namespace PrismProductsManager\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Form\ProductAttributesSelectForm;

class ProductAttributesSelectFormFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Get the attribute group mapper
        $attributesGroupMapper = $serviceLocator->get('PrismProductsManager\Model\AttributesGroupMapper');

        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig = $config->read('siteConfig');
        $languageId = $siteConfig['languages']['current-language'];

        $routeMatch = $serviceLocator->get('Application')->getMvcEvent()->getRouteMatch();
        $productId = $routeMatch->getParam('id', false);
        $variantId = $routeMatch->getParam('variantId', false);
        $isVariant = 0;

        if ($productId === false && $variantId === false) {

            $existingAttributeGroupsIdsForProduct = false;

        } else {
            // Check if variant id exist

            if ($variantId !== false) {
                $isVariant = 1;
                $variantIds = explode('-', $variantId);

                // Fetch existing attributes for every variant ID
                foreach ($variantIds as $variantId) {
                    $existingAttributeGroupsIdsForProduct[] = $attributesGroupMapper->fetchExistingAttributeGroupsIdsForProduct($variantId,$isVariant);
                }
            } else {
                $existingAttributeGroupsIdsForProduct = $attributesGroupMapper->fetchExistingAttributeGroupsIdsForProduct($productId, $isVariant);
            }
        }
        //Get all attributes from database.
        $allEnabledAttributesGroups = $attributesGroupMapper->fetchAllAttributeGroups($languageId);
        $dynamicallyGeneratedAttributes = array();

        $fetchAvailableAttributeGroupIdsForProduct = $attributesGroupMapper->fetchAllAttributeGroupsForMarketingCategory($productId);

     //   var_dump($fetchAvailableAttributeGroupIdsForProduct);

        //Loop through and extract some attribute names we will need to create the html elements
        if (count($allEnabledAttributesGroups) > 0) {
            foreach ($allEnabledAttributesGroups as $attributeType => $attributesArray) {
                if (isset($attributeType) && trim($attributeType) != '' && is_array($attributesArray) && count($attributesArray) > 0) {
                    foreach ($attributesArray as $attributesArrayKey => $attributesArrayValue) {

                        $attributeType = strtolower($attributeType);
                        $toBeSavedArray = array();
                        $extractedAttribute = $attributesGroupMapper->extractAttributeIdAndName($attributesArrayValue);
                        $toBeSavedArray['attributeGroupId'] = $attributesArrayValue['attributeGroupId'];
                        $toBeSavedArray['publicName'] = $attributesArrayValue['publicName'];
                        $toBeSavedArray['label'] = $extractedAttribute['label'];
                        $toBeSavedArray['name'] = $attributeType. '_'. $attributesArrayValue['attributeGroupId'];
                        $toBeSavedArray['viewAs'] = $attributesArrayValue['viewAs'];
                        if (isset($attributesArrayValue['values']) && count($attributesArrayValue['values']) > 0) {
                            $toBeSavedArray['values'] = $attributesArrayValue['values'];
                        }
                  //      var_dump($attributesArrayValue['attributeGroupId']);
                  //      var_dump($fetchAvailableAttributeGroupIdsForProduct);
                        if (in_array($attributesArrayValue['attributeGroupId'], $fetchAvailableAttributeGroupIdsForProduct)) {
                            // this means this is an approved attribute for us
                            if ($existingAttributeGroupsIdsForProduct !== false && !in_array($attributesArrayValue['attributeGroupId'], $existingAttributeGroupsIdsForProduct)) {

                                // attribute is new
                                $toBeSavedArray['name'] = 'new_'.$toBeSavedArray['name'];
                                $dynamicallyGeneratedAttributes['new'][$attributeType][] = $toBeSavedArray;
                            } else {
                                $dynamicallyGeneratedAttributes[$attributeType][] = $toBeSavedArray;
                            }
                        } else {
                            // this means this is not an approved attribute for us
                            // aldo if already exists on the product allow it as an existing one

                            // Commmented out because the if statement is already declared on top of this,
                            // and this else is used if the firs condition fails.
                            // Because the fallback had the same condition as the first condition
                            // Attributes for variants broke.
                            if ($existingAttributeGroupsIdsForProduct !== false && in_array($attributesArrayValue['attributeGroupId'], $existingAttributeGroupsIdsForProduct)) {
                                // attribute is new
                                $dynamicallyGeneratedAttributes[$attributeType][] = $toBeSavedArray;
                            }
                        }
                    }
                }
            }
        }

        if (isset($dynamicallyGeneratedAttributes['new'])) {
            if (count($dynamicallyGeneratedAttributes) == 1) {
                // it is a new product without any attribute binded to it.
                // so all the attributes should be mandatory at this point
                $dynamicallyGeneratedAttributes = $dynamicallyGeneratedAttributes['new'];
                foreach ($dynamicallyGeneratedAttributes as $attributeType => $attributesArray) {
                    foreach ($attributesArray as $key => $attributesPropertiesArray) {
                        foreach ($attributesPropertiesArray as $attributeName => $attributeValue) {
                            if ($attributeName == 'name') {
                                $dynamicallyGeneratedAttributes[$attributeType][$key][$attributeName] = str_replace('new_', '', $dynamicallyGeneratedAttributes[$attributeType][$key][$attributeName]);
                            }
                        }
                    }
                }
            } else {

                // push the new attributes at the beginning of the form
                $newAttributesArray = $dynamicallyGeneratedAttributes['new'];
                unset($dynamicallyGeneratedAttributes['new']);
                $dynamicallyGeneratedAttributes = array_merge(array('new' => $newAttributesArray), $dynamicallyGeneratedAttributes);
            }
        }

        //Inject objects into the form.
        $formObject = new ProductAttributesSelectForm($serviceLocator, $dynamicallyGeneratedAttributes, $isVariant);
        return $formObject;
    }
}
