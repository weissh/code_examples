<?php

namespace PrismProductsManager\Factory\Form;

use PrismProductsManager\Form\ProductsAddOnForm;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ProductsAddOnFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        //Inject the service locator and mapper into the form
        $productsAddOnForm = new ProductsAddOnForm();

        return $productsAddOnForm;
    }
}
