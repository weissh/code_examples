<?php

namespace PrismProductsManager\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Form\AddAttributesMarketingForm;

class AddAttributesMarketingFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('db-pms');

        //Inject the service locator and mapper into the form
        $AddAttributesMarketingForm = new AddAttributesMarketingForm($dbAdapter);

        return $AddAttributesMarketingForm;
    }
}
