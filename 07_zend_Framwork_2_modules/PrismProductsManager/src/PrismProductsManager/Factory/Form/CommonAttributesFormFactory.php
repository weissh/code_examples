<?php

namespace PrismProductsManager\Factory\Form;

use PrismProductsManager\Form\CommonAttributesForm;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class CommonAttributesFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig = $config->read('siteConfig');

        //Retrieve the common attributes service
        $commonAttributesService = $serviceLocator->get('PrismProductsManager\Service\CommonAttributesService');

        //Inject the service locator and mapper into the form
        return new CommonAttributesForm($commonAttributesService);
    }
}