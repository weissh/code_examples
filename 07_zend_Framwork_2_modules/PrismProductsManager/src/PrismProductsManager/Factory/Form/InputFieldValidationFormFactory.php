<?php

namespace PrismProductsManager\Factory\Form;

use PrismProductsManager\Form\InputFieldValidationForm;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class InputFieldValidationFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** Retrieve Services */
        $assignFieldToTabsService = $serviceLocator->get('PrismProductsManager\Service\AssignFieldToTabsService');

        return new InputFieldValidationForm(
            $assignFieldToTabsService
        );
    }
}