<?php

namespace PrismProductsManager\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Form\ProductPricesSelectForm;

class ProductPricesSelectFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Get the attribute group mapper
        $productsMapper = $serviceLocator->get('PrismProductsManager\Model\ProductsMapper');

        $routeMatch = $serviceLocator->get('Application')->getMvcEvent()->getRouteMatch();
        $productId = $routeMatch->getParam('id', false);
        $variantId = $routeMatch->getParam('variantId', false);
        if ($productId === false || $variantId === false) {
            //throw new \Exception('You must specify a product and variant id!');
            return false;
        } else {
            $variantIds = explode('-', $variantId);
            $variantId = $variantIds[0];
            $existingVariantPrices = $productsMapper->getProductPrices($variantId, false, 1);
        }

        //Inject objects into the form.
        $formObject = new ProductPricesSelectForm($serviceLocator, $existingVariantPrices, $productId, $variantId);
        return $formObject;
    }
}
