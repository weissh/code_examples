<?php

namespace PrismProductsManager\Factory\Form;

use PrismProductsManager\Form\AssignInputFieldsForm;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Form\InputFieldsForm;

class AssignInputFieldsFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** Retrieve Services */
        $assignFieldToTabsService = $serviceLocator->get('PrismProductsManager\Service\AssignFieldToTabsService');
        $commonAttributesService = $serviceLocator->get('PrismProductsManager\Service\CommonAttributesService');
        /** Retrieve mappers */
        $attributesGroupMapper = $serviceLocator->get('PrismProductsManager\Model\AttributesGroupMapper');

        return new AssignInputFieldsForm(
            $assignFieldToTabsService,
            $commonAttributesService,
            $attributesGroupMapper
        );
    }
}