<?php

namespace PrismProductsManager\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Form\ProductSupplierPriceForm;

/**
 * Class ProductSupplierPriceFormFactory
 * @package PrismProductsManager\Factory\Form
 */
class ProductSupplierPriceFormFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ProductSupplierPriceForm
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Get the attribute group mapper
        $productsMapper = $serviceLocator->get('PrismProductsManager\Model\ProductsMapper');

        $formObject = new ProductSupplierPriceForm($productsMapper);
        return $formObject;
    }
}