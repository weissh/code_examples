<?php

namespace PrismProductsManager\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Form\AddMerchantCategoryForm;

class AddMerchantCategoryFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Retrieve the categories mapper
        $categoriesMapper = $serviceLocator->get('PrismProductsManager\Model\CategoriesMapper');
        $attributesMapper = $serviceLocator->get('PrismProductsManager\Model\AttributesGroupMapper');
        $commonAttributesTable = $serviceLocator->get('PrismProductsManager\Model\CommonAttributes\CommonAttributesTable');

        $dbAdapter = $serviceLocator->get('db-pms');

        //Inject the service locator and mapper into the form
        $AddMerchantCategoryForm = new AddMerchantCategoryForm($categoriesMapper, $attributesMapper, $dbAdapter, $commonAttributesTable);

        return $AddMerchantCategoryForm;
    }
}
