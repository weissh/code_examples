<?php

namespace PrismProductsManager\Factory\Form;

use PrismProductsManager\Form\TerritoryOverrideForm;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TerritoryOverrideFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        // Retrieve the inputService mapper
        $commonAttributesService = $serviceLocator->get('PrismProductsManager\Service\CommonAttributesService');

        //Inject the service locator and mapper into the form
        return new TerritoryOverrideForm($commonAttributesService);
    }
}
