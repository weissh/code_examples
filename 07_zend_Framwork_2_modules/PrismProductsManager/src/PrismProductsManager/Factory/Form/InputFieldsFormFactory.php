<?php

namespace PrismProductsManager\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Form\InputFieldsForm;

/**
 * Class InputFieldsFormFactory
 * @package PrismProductsManager\Factory\Form
 */
class InputFieldsFormFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return InputFieldsForm
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocator = $serviceLocator->getServiceLocator();

        // Retrieve the inputService mapper
        $inputService = $serviceLocator->get('PrismProductsManager\Service\InputService');

        // Retrieve the productsMapper mapper
        $productsMapper = $serviceLocator->get('PrismProductsManager\Model\ProductsMapper');

        $routeMatch = $serviceLocator->get('Application')->getMvcEvent()->getRouteMatch();

        $pageUrl = $routeMatch->getParam('displayPage', false);
        $style = $routeMatch->getParam('style', null);
        $option = $routeMatch->getParam('option', null);

        $cacheAdapter = $serviceLocator->get('PrismCommon\Adapter\CacheAdapter');

        $optionsData =  $this->getOptionsData($productsMapper, $style, $option, $cacheAdapter);
        $dynamicFields = $this->getDynamicFields($inputService, $pageUrl, $style, $option, $optionsData, $cacheAdapter);

        $doctrineEntity = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $inputPagesRepository = $doctrineEntity->getRepository('PrismProductsManager\Entity\InputPages');

        $pageEntity = $inputPagesRepository->findOneBy(array('pageurl' => $pageUrl));

        $config =  $serviceLocator->get('config');
        $clientConfig = $config['clientConfig'];

        //Inject the service locator and mapper into the form
        return new InputFieldsForm(
            $serviceLocator,
            $dynamicFields,
            $productsMapper,
            $inputPagesRepository,
            $pageEntity,
            $style,
            $option,
            $clientConfig,
            $optionsData
        );
    }

    /**
     * @param $productsMapper
     * @param $style
     * @param $option
     * @param $cacheAdapter
     * @return array
     */
    protected function getOptionsData($productsMapper, $style, $option, $cacheAdapter)
    {
        $optionsData = array();
        if (!empty($style) && empty($option)) {

            $cacheKey  = $style . '_options_list';

            $optionsData = $cacheAdapter->getCache($cacheKey, true);
            if(null !== $optionsData){
                return $optionsData;
            }

            $optionsData = $productsMapper->getAllOptionsForStyle($style, true);

            $cacheAdapter->setCache($cacheKey, $optionsData);

        }

        return $optionsData;
    }

    /**
     * @param $inputService
     * @param $pageUrl
     * @param $style
     * @param $option
     * @param $optionsData
     * @param $cacheAdapter
     * @return array
     */
    protected function getDynamicFields($inputService, $pageUrl, $style, $option, $optionsData, $cacheAdapter)
    {
        $dynamicFields = array();
        /** Retrieve all input fields for $pageUrl **/
        if ($pageUrl !== false) {

        //    $cacheKey  = $style . '_' . $pageUrl . '_' . $option . '_dynamic_fields' ;

        //    $dynamicFields = $cacheAdapter->doctrineGetCache($cacheKey);
        //    if(null !== $dynamicFields){
        //        return $dynamicFields;
        //    }

            $dynamicFields = $inputService->getPageInputFields($pageUrl, $style, $option, $optionsData);

        //    $cacheAdapter->doctrineUpsertCache($cacheKey, $dynamicFields);

        }

        return $dynamicFields;
    }
}
