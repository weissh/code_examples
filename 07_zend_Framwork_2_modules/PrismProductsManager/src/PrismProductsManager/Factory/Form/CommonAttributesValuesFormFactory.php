<?php

namespace PrismProductsManager\Factory\Form;

use PrismProductsManager\Form\CommonAttributesValuesForm;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CommonAttributesValuesFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Retrieve the categories mapper
        $dbAdapter = $serviceLocator->get('db-pms');

        //Retrieve the common attributes service
        $commonAttributesService = $serviceLocator->get('PrismProductsManager\Service\CommonAttributesService');

        //Get Common Attrubute Id to be sued in form
        $routeMatch = $serviceLocator->get('Application')->getMvcEvent()->getRouteMatch();
        $attributeId = $routeMatch->getParam('attributeId', null);

        //Inject the service locator and mapper into the form
        $AddMerchantCategoryForm = new CommonAttributesValuesForm($dbAdapter, $commonAttributesService, $attributeId);

        return $AddMerchantCategoryForm;
    }
}
