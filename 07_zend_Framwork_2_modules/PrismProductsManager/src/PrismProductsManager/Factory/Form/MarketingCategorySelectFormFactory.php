<?php

namespace PrismProductsManager\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Form\MarketingCategorySelectForm;

class MarketingCategorySelectFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Retrieve the categories mapper
        $categoriesMapper = $serviceLocator->get('PrismProductsManager\Model\CategoriesMapper');
        
        //Inject the service locator and mapper into the form
        $marketingCategorySelectForm = new MarketingCategorySelectForm($serviceLocator, $categoriesMapper);

        return $marketingCategorySelectForm;
    }
}
