<?php

namespace PrismProductsManager\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Form\ProductVariantsCreationForm;

class ProductVariantCreationFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Retrieve the products mapper
        $productsMapper = $serviceLocator->get('PrismProductsManager\Model\ProductsMapper');
        
        //Inject the service locator and mapper into the form
        $productsActivationForm = new ProductVariantsCreationForm($serviceLocator, $productsMapper);

        return $productsActivationForm;
    }
}
