<?php

namespace PrismProductsManager\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Form\MerchantCategorySelectForm;

class MerchantCategorySelectFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Retrieve merchant category mapper.
        $merchantCategoryMapper = $serviceLocator->get('PrismProductsManager\Model\MerchantCategoriesMapper');
        
        //Inject the service locator and mapper to the form.
        $merchantCategorySelectForm = new MerchantCategorySelectForm($serviceLocator, $merchantCategoryMapper);

        return $merchantCategorySelectForm;
    }
}
