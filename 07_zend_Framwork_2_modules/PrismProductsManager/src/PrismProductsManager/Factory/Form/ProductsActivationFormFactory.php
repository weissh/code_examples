<?php

namespace PrismProductsManager\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Form\ProductsActivationForm;

class ProductsActivationFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Retrieve the products mapper
        $productsMapper = $serviceLocator->get('PrismProductsManager\Model\ProductsMapper');
        
        //Inject the service locator and mapper into the form
        $productsActivationForm = new ProductsActivationForm($serviceLocator, $productsMapper);

        return $productsActivationForm;
    }
}
