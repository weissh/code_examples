<?php

namespace PrismProductsManager\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Form\AddNewTaxForm;

class AddNewTaxFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig = $config->read('siteConfig');

        //Retrieve the products mapper
        $taxMapper = $serviceLocator->get('PrismProductsManager\Model\TaxMapper');
        $countryService = $serviceLocator->get('PrismSpectrumApiClient\Service\Country');
        $countriesList = $countryService->getList();

        if (is_object($countriesList)) {
            $countriesList = (array) $countriesList->success;
        }
        $currentLanguage = $siteConfig['languages']['current-language'];

        //Inject the service locator and mapper into the form
        $productsAddNewTaxForm = new AddNewTaxForm($serviceLocator, $taxMapper, $countriesList, $currentLanguage);
        return $productsAddNewTaxForm;
    }
}
