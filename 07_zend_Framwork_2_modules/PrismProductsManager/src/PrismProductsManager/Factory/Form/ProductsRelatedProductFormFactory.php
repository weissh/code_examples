<?php

namespace PrismProductsManager\Factory\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismProductsManager\Form\ProductsRelatedProductForm;

class ProductsRelatedProductFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //Get the product group mapper
        //$productsMapper = $serviceLocator->get('PrismProductsManager\Model\ProductsMapper');
        
        //Inject objects into the form.
        $controller = new ProductsRelatedProductForm($serviceLocator);

        return $controller;
    }
}
