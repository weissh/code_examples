<?php

namespace PrismProductsManager\Factory\View;

use PrismProductsManager\View\Helper\ManualSyncViewHelper;
use PrismProductsManager\View\Helper\OutputTableHeaderHelper;
use PrismProductsManager\View\Helper\OutputTableHeaderViewHelper;
use Zend\Mvc\Router\Http\RouteMatch;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * View helper to get manual sync options
 *
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class OutputTableHeaderViewHelperFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();
        $config = $services->get('Config');
        return new OutputTableHeaderViewHelper($config);
    }
}
