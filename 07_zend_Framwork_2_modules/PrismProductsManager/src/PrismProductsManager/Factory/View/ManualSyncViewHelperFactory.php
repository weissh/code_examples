<?php

namespace PrismProductsManager\Factory\View;

use PrismProductsManager\View\Helper\ManualSyncViewHelper;
use Zend\Mvc\Router\Http\RouteMatch;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * View helper to get manual sync options
 *
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class ManualSyncViewHelperFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();
        $config = $services->get('Config');
        $manualSyncHelper = new ManualSyncViewHelper($config);

        return $manualSyncHelper;
    }
}
