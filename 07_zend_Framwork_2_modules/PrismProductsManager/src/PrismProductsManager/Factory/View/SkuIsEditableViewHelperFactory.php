<?php

namespace PrismProductsManager\Factory\View;

use PrismProductsManager\View\Helper\SkuIsEditableViewHelper;
use Zend\Mvc\Router\Http\RouteMatch;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * View helper to get manual sync options
 *
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class SkuIsEditableViewHelperFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();
        $config = $services->get('Config');
        $productsMapper = $services->get('PrismProductsManager\Model\ProductsMapper');

        $manualSyncHelper = new SkuIsEditableViewHelper($config, $productsMapper);

        return $manualSyncHelper;
    }
}
