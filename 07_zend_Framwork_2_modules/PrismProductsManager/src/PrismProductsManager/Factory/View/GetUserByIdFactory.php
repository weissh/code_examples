<?php

namespace PrismProductsManager\Factory\View;

use PrismProductsManager\View\Helper\GetUserById;
use Zend\Mvc\Router\Http\RouteMatch;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class GetUserByIdFactory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $services = $serviceLocator->getServiceLocator();
        $usersTable = $services->get('PrismUsersManager\Model\UsersTable');
        
        $helper = new GetUserById($usersTable);

        return $helper;
    }
}
