<?php

namespace PrismProductsManager\Validator;

use Respect\Validation\Rules\AllOf;
use Respect\Validation\Validator as v;

/**
 * Class ProductData
 *
 * @package PrismProductsManager\Validator
 */
class ProductData extends AllOf
{
    /**
     * ProductData constructor.
     */
    public function __construct()
    {
        $this->addRule(v::arrayType());
        $this->addRule(v::key('productName', v::stringType(), true));
        $this->addRule(v::key('productDescription', v::stringType(), true));
        $this->addRule(v::key('barcode', v::stringType(), true));
        $this->addRule(v::key('sku', v::stringType(), true));
        $this->addRule(v::key('style', v::stringType(), true));
        $this->addRule(v::key('active', v::oneOf(
            v::equals('1'),
            v::equals('0')
        ), true));
        $this->addRule(v::key('colour', v::allOf(
            v::key('colourCode', v::stringType(), true),
            v::key('colourName', v::stringType(), true)
        ), true));
        $this->addRule(v::key('size', v::allOf(
            v::key('sizeValue', v::stringType(), true),
            v::key('sizeCode', v::stringType(), true)
        ), true));
        $this->addRule(v::key('prices', v::allOf(
            v::arrayType(),
            v::each(v::allOf(
                v::key('nowPrice', v::stringType(), true),
                v::key('wasPrice', v::stringType(), true)
            ))
        ), true));
        $this->addRule(v::key('images', v::allOf(
            v::arrayType(),
            v::each(v::allOf(
                v::key('lowRes', v::stringType(), true),
                v::key('highRes', v::stringType(), true)
            ))
        ), true));
        $this->addRule(v::key('attributes', v::arrayType(), true));
        $this->addRule(v::key('styleGroup', v::arrayType(), true));
    }

    /**
     * @return string
     */
    public static function getNamespace()
    {
        return __NAMESPACE__;
    }
}
