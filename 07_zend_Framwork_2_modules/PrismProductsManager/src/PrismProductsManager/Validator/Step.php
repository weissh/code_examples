<?php

namespace PrismProductsManager\Validator;

use Zend\Validator\Step as StepValidator;

/**
 * Step
 *
 * Step validator.
 *
 * @package PrismProductsManager\Validator
 */
class Step extends StepValidator
{
    /**
     * isValid
     *
     * Check if the value is a valid number.
     *
     * @param  mixed  $value
     *
     * @return boolean
     */
    public function isValid($value)
    {
        //return parent::isValid($value);


        if (!is_numeric($value)) {
            $this->error(self::INVALID);
            return false;
        }

        $this->setValue($value);

        $stepPlaces = strlen(substr(strrchr($this->step, "."), 1));

        $valuePlaces = strlen(substr(strrchr($value, "."), 1));

        if ($stepPlaces < $valuePlaces) {
            $this->error(self::NOT_STEP);
            return false;
        }

        $testValue = str_replace('.', '', number_format($value,$stepPlaces,'.',''));
        $testBaseValue = str_replace('.', '', number_format($this->baseValue,$stepPlaces,'.',''));
        $testStep = str_replace('.', '', $this->step) + 0;

        $mod = ($testValue - $testBaseValue) % $testStep;
        //$fmod = $this->fmod($value - $this->baseValue, $this->step);

        if ($mod !== 0 && $mod !== $testStep) {
            $this->error(self::NOT_STEP);
            return false;
        }

        return true;
    }

}