<?php

namespace PrismProductsManager\Validator;

use Respect\Validation\Exceptions\NestedValidationException;

class ProductDataException extends NestedValidationException
{
}
