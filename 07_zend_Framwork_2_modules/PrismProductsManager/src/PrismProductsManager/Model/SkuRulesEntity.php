<?php

namespace PrismProductsManager\Model;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
/**
 * Class SkuRulesEntity
 * @package PrismProductsManager\Model
 */
class SkuRulesEntity
{

    /**
     * @var
     */
    protected $_skuRuleId;

    /**
     * @var
     */
    protected $_merchantcategoryId;

    /**
     * @var
     */
    protected $_merchantAttributeGroupId;

    /**
     * @var
     */
    protected $_position;

    /**
     * @var
     */
    protected $_isCommonAttribute = 0;

    /**
     * @return mixed
     */
    public function getIsCommonAttribute()
    {
        return $this->_isCommonAttribute;
    }

    /**
     * @param mixed $isCommonAttribute
     */
    public function setIsCommonAttribute($isCommonAttribute)
    {
        $this->_isCommonAttribute = $isCommonAttribute;
    }

    /**
     * @return mixed
     */
    public function getSkuRuleId()
    {
        return $this->_skuRuleId;
    }

    /**
     * @return mixed
     */
    public function getMerchantcategoryId()
    {
        return $this->_merchantcategoryId;
    }

    /**
     * @return mixed
     */
    public function getMerchantAttributeGroupId()
    {
        return $this->_merchantAttributeGroupId;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->_position;
    }

    /**
     * @param $_skuRuleId
     */
    public function setSkuRuleId($_skuRuleId)
    {
        $this->_skuRuleId = $_skuRuleId;
    }

    /**
     * @param $_merchantcategoryId
     */
    public function setMerchantcategoryId($_merchantcategoryId)
    {
        $this->_merchantcategoryId = $_merchantcategoryId;
    }

    /**
     * @param $_merchantAttributeGroupId
     */
    public function setMerchantAttributeGroupId($_merchantAttributeGroupId)
    {
        $this->_merchantAttributeGroupId = $_merchantAttributeGroupId;
    }

    /**
     * @param $_position
     */
    public function setPosition($_position)
    {
        $this->_position = $_position;
    }


}

