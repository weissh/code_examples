<?php
namespace PrismProductsManager\Model;


/**
 * Class AttributesMarketingDefinitionsEntity
 * @package PrismProductsManager\Model
 */
class AttributesMarketingDefinitionsEntity
{

    /**
     * @var
     */
    protected $_attributesDefinitionsId;

    /**
     * @var
     */
    protected $_languageId;

    /**
     * @var
     */
    protected $_attributeId;

    /**
     * @var
     */
    protected $_attributeValue;

    /**
     *
     */
    public function __construct()
    {

    }

    /**
     * @param mixed $attributeId
     */
    public function setAttributeId($attributeId)
    {
        $this->_attributeId = $attributeId;
    }

    /**
     * @return mixed
     */
    public function getAttributeId()
    {
        return $this->_attributeId;
    }

    /**
     * @param mixed $attributeValue
     */
    public function setAttributeValue($attributeValue)
    {
        $this->_attributeValue = $attributeValue;
    }

    /**
     * @return mixed
     */
    public function getAttributeValue()
    {
        return $this->_attributeValue;
    }

    /**
     * @param mixed $attributesDefinitionsId
     */
    public function setAttributesDefinitionsId($attributesDefinitionsId)
    {
        $this->_attributesDefinitionsId = $attributesDefinitionsId;
    }

    /**
     * @return mixed
     */
    public function getAttributesDefinitionsId()
    {
        return $this->_attributesDefinitionsId;
    }

    /**
     * @param mixed $languageId
     */
    public function setLanguageId($languageId)
    {
        $this->_languageId = $languageId;
    }

    /**
     * @return mixed
     */
    public function getLanguageId()
    {
        return $this->_languageId;
    }

}