<?php

namespace PrismProductsManager\Model;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class ProductsMisSpellsEntity
{
    protected $_productMisSpellId;
    
    protected $_productId;
            
    protected $_misSpell;
    
    protected $_created;
    
    protected $_modified;
    
    protected $_languageId;

    public function __construct()
    {
        $currentDatetime =  date("Y-m-d H:i:s");
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
    }

        public function getProductMisSpellId()
    {
        return $this->_productMisSpellId;
    }

    public function getProductId()
    {
        return $this->_productId;
    }

    public function getMisSpell()
    {
        return $this->_misSpell;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setProductMisSpellId($_productMispellId)
    {
        $this->_productMisSpellId = $_productMispellId;
    }

    public function setProductId($_productId)
    {
        $this->_productId = $_productId;
    }

    public function setMisSpell($_misSpell)
    {
        $this->_misSpell = $_misSpell;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }

    public function getLanguageId()
    {
        return $this->_languageId;
    }

    public function setLanguageId($_languageId)
    {
        $this->_languageId = $_languageId;
    }

}
