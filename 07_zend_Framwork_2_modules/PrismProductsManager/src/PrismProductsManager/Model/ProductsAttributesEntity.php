<?php

namespace PrismProductsManager\Model;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class ProductsAttributesEntity
{
    
    protected $_productAttributeId;
    protected $_productId;
    protected $_manufacturerId = 0;
    protected $_ean13 = '';
    protected $_ecoTax = 0.00;
    protected $_sku;
    protected $_supplierReference = '';
    protected $_status = 'INACTIVE';
    protected $_indexed = 0;
    protected $_isDefaultProduct = 0;
    protected $_created;
    protected $_modified;

    public function __construct()
    {
        $currentDatetime = new \Zend\Db\Sql\Expression("NOW()");
        
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
        
    }
    
    public function getProductAttributeId()
    {
        return $this->_productAttributeId;
    }

    public function getProductId()
    {
        return $this->_productId;
    }

    public function getManufacturerId()
    {
        return $this->_manufacturerId;
    }

    public function getEan13()
    {
        return $this->_ean13;
    }

    public function getEcoTax()
    {
        return $this->_ecoTax;
    }

    public function getSku()
    {
        return $this->_sku;
    }

    public function getSupplierReference()
    {
        return $this->_supplierReference;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getIndexed()
    {
        return $this->_indexed;
    }

    public function getIsDefaultProduct()
    {
        return $this->_isDefaultProduct;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setProductAttributeId($_productAttributeId)
    {
        $this->_productAttributeId = $_productAttributeId;
    }

    public function setProductId($_productId)
    {
        $this->_productId = $_productId;
    }

    public function setManufacturerId($_manufacturerId)
    {
        $this->_manufacturerId = $_manufacturerId;
    }

    public function setEan13($_ean13)
    {
        $this->_ean13 = $_ean13;
    }

    public function setEcoTax($_ecoTax)
    {
        $this->_ecoTax = $_ecoTax;
    }

    public function setSku($_sku)
    {
        $this->_sku = $_sku;
    }

    public function setSupplierReference($_supplierReference)
    {
        $this->_supplierReference = $_supplierReference;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setIndexed($_indexed)
    {
        $this->_indexed = $_indexed;
    }

    public function setIsDefaultProduct($_isDefaultProduct)
    {
        $this->_isDefaultProduct = $_isDefaultProduct;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }
    
}
