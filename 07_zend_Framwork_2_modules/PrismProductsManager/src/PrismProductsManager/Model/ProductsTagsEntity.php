<?php

namespace PrismProductsManager\Model;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class ProductsTagsEntity
{
    protected $_productTagId;
    
    protected $_productId;
            
    protected $_alias;

    protected $_status;
    
    protected $_created;
    
    protected $_modified;
    
    protected $_languageId;

    public function __construct()
    {
        $currentDatetime =  date("Y-m-d H:i:s");
        $this->_status = 'ACTIVE';
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
    }

        public function getProductTagId()
    {
        return $this->_productTagId;
    }

    public function getProductId()
    {
        return $this->_productId;
    }

    public function getAlias()
    {
        return $this->_alias;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setProductTagId($_productTagId)
    {
        $this->_productTagId = $_productTagId;
    }

    public function setProductId($_productId)
    {
        $this->_productId = $_productId;
    }

    public function setAlias($_alias)
    {
        $this->_alias = $_alias;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }
    
    public function getLanguageId()
    {
        return $this->_languageId;
    }

    public function setLanguageId($_languageId)
    {
        $this->_languageId = $_languageId;
    }


}
