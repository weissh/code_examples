<?php

namespace PrismProductsManager\Model;

/**
 * Class MerchantCategoriesDefinitionsEntity
 * @package PrismProductsManager\Model
 * @author cristian.popescu
 */
class MerchantCategoriesDefinitionsEntity
{
    /**
     * @var
     */
    protected $_productMerchantCategoryId;
    /**
     * @var
     */
    protected $_languageId;
    /**
     * @var
     */
    protected $_categoryName;
    /**
     * @var
     */
    protected $_shortDescription;
    /**
     * @var
     */
    protected $_fullDescription;
    /**
     * @var
     */
    protected $_created;
    /**
     * @var
     */
    protected $_modified;

    /**
     * @param mixed $categoryName
     */
    public function setCategoryName($categoryName)
    {
        $this->_categoryName = $categoryName;
    }

    /**
     * @return mixed
     */
    public function getCategoryName()
    {
        return $this->_categoryName;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->_created = $created;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->_created;
    }

    /**
     * @param mixed $fullDescription
     */
    public function setFullDescription($fullDescription)
    {
        $this->_fullDescription = $fullDescription;
    }

    /**
     * @return mixed
     */
    public function getFullDescription()
    {
        return $this->_fullDescription;
    }

    /**
     * @param mixed $languageId
     */
    public function setLanguageId($languageId)
    {
        $this->_languageId = $languageId;
    }

    /**
     * @return mixed
     */
    public function getLanguageId()
    {
        return $this->_languageId;
    }

    /**
     * @param mixed $modified
     */
    public function setModified($modified)
    {
        $this->_modified = $modified;
    }

    /**
     * @return mixed
     */
    public function getModified()
    {
        return $this->_modified;
    }

    /**
     * @param mixed $productMerchantCategoryId
     */
    public function setProductMerchantCategoryId($productMerchantCategoryId)
    {
        $this->_productMerchantCategoryId = $productMerchantCategoryId;
    }

    /**
     * @return mixed
     */
    public function getProductMerchantCategoryId()
    {
        return $this->_productMerchantCategoryId;
    }

    /**
     * @param mixed $shortDescription
     */
    public function setShortDescription($shortDescription)
    {
        $this->_shortDescription = $shortDescription;
    }

    /**
     * @return mixed
     */
    public function getShortDescription()
    {
        return $this->_shortDescription;
    }

}
    