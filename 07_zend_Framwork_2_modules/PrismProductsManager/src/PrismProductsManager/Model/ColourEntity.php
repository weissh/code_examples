<?php
namespace PrismProductsManager\Model;


class ColourEntity
{

    protected $_colourId = null;

    protected $_isDefault = null;

    protected $_code;

    protected $_hex;

    protected $_enabled;

    protected $_modified;

    public function __construct()
    {

    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->_code = $code;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * @param null $colourId
     */
    public function setColourId($colourId)
    {
        $this->_colourId = $colourId;
    }

    /**
     * @return null
     */
    public function getColourId()
    {
        return $this->_colourId;
    }

    /**
     * @param mixed $enabled
     */
    public function setEnabled($enabled)
    {
        $this->_enabled = $enabled;
    }

    /**
     * @return mixed
     */
    public function getEnabled()
    {
        return $this->_enabled;
    }

    /**
     * @param mixed $hex
     */
    public function setHex($hex)
    {
        $this->_hex = $hex;
    }

    /**
     * @return mixed
     */
    public function getHex()
    {
        return $this->_hex;
    }

    /**
     * @param mixed $modified
     */
    public function setModified($modified)
    {
        $this->_modified = $modified;
    }

    /**
     * @return mixed
     */
    public function getModified()
    {
        return $this->_modified;
    }


}