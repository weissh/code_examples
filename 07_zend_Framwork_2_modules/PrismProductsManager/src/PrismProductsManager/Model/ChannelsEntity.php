<?php

namespace PrismProductsManager\Model;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class ChannelsEntity
{

    protected $_channelId;
    protected $_status;
    protected $_created;
    protected $_modified;

    public function __construct()
    {
        $currentDatetime = new \Zend\Db\Sql\Expression("NOW()");

        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
        $this->_status = 'ACTIVE';
    }

    public function getChannelId()
    {
        return $this->_channelId;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setChannelId($_channelId)
    {
        $this->_channelId = $_channelId;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }

}
