<?php
namespace PrismProductsManager\Model;

use PrismElasticSearch\Enum\ElasticSearchTypes;
use PrismProductsManager\Service\CommonAttributesService;
use PrismProductsManager\Service\ScheduleService;
use PrismQueueManager\Service\QueueService;
use Zend\Db\Adapter\Adapter;
use Zend\Json\Json;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\EventManager\EventManagerInterface;
use Zend\Db\Sql\Select;
use PHPExcel_IOFactory;
use PHPExcel_Style_Fill;
use PHPExcel;
use PrismMediaManager\Service\RSMapper;
use Zend\Db\Sql\Expression;
use PrismProductsManager\Model\Exception;
use PHPExcel_Cell_DataType;
use PHPExcel_Reader_CSV;
use Respect\Validation\Rules\NoWhitespace;
use PrismPromotionsManager\Promotion\Rule\Variable\Variable;
use Zend\Filter\StringToLower;
use \Zend\I18n\Filter\Alnum;
use Zend\Filter\Word\CamelCaseToDash;
use Zend\Crypt\PublicKey\Rsa\PublicKey;
use Zend\Validator\Date;

/**
 * Class BulkActionsMapper
 *
 * @package PrismProductsManager\Model
 * @author Vilius Vaitiekunas <vilius.vaitiekunas@whistl.co.uk>
 */
class BulkActionsMapper
{

    /**
     *
     * @var array Submitted data from the forms
     */
    protected $_submittedData;

    /**
     *
     * @var array Languages from the config file
     */
    protected $_languages;

    /**
     *
     * @var object Zend 2 ClassMethods object
     */
    protected $hydrator;

    /**
     *
     * @var object Used to read xls files
     */
    protected $_excelReader;

    /**
     *
     * @var /SiteConfig
     */
    protected $_siteConfig;

    /**
     *
     * @var ProductsMapperPrices
     */
    protected $_productsMapperPrice;

    /**
     *
     * @var QueueService
     */
    protected $_queueService;

    /**
     *
     * @var ProductsMapper
     */
    protected $productsMapper;

    /**
     *
     * @var BULK_PROCESSING table name string
     */
    protected $_blukProcessingTable = 'BULK_PROCESSING';

    /**
     *
     * @var int
     */
    protected $bulkProcessingId;

    const EXCEL_TEMPLATE = ROOT_PATH . "/data/phpexcel_templates/download_base.xlsx";

    /**
     *
     * @param Adapter $dbAdapter
     * @param
     *            $languages
     * @param PHPExcel $excel
     * @param
     *            $siteConfig
     * @param ProductsMapperPrices $productPriceMapper
     * @param QueueService $queueService
     * @param CommonAttributesService $commonAttributesService
     * @param ProductsMapper $productsMapper
     */
    public function __construct(Adapter $dbAdapter, $languages, PHPExcel $excel, $siteConfig, ProductsMapperPrices $productPriceMapper, QueueService $queueService, CommonAttributesService $commonAttributesService, ProductsMapper $productsMapper, ScheduleService $scheduleService)
    {
        // Set variables from the factory inject
        $this->_excelReader = $excel;
        $this->_dbAdapter = $dbAdapter;
        $this->_languages = $languages;
        $this->_sql = new Sql($dbAdapter);
        $this->_siteConfig = $siteConfig;
        $this->_productsMapperPrice = $productPriceMapper;
        $this->_queueService = $queueService;
        $this->commonAttributesService = $commonAttributesService;
        $this->productsMapper = $productsMapper;
        $this->scheduleService = $scheduleService;
    }

    /**
     *
     * @param
     *            $data
     * @throws \Exception
     */
    public function bulk($data)
    {
        $this->_submittedData = $data;
        switch ($this->_submittedData['submit']) {
            case 'Download Template':
                $this->setTemplateType($this->_submittedData);
                return true;
                break;
            case 'Upload Template':
                $this->setTemplateType($this->_submittedData);
                break;
            default:
                throw new \Exception('Invalid case: ' . $this->_submittedData['submit']);
        }
    }

    /**
     *
     * @param
     *            $data
     * @throws \Exception
     */
    public function setTemplateType($data)
    {
        $this->_submittedData = $data;
        switch ($this->_submittedData['downloadType']) {
            case 'Price Template':
                $data = $this->getAllPrices($this->_submittedData);
                $title = $this->createPriceColumnTitle();
                break;
            default:
                throw new \Exception('Invalid case: ' . $this->_submittedData['submit']);
        }
        $this->generateFile($title, $data);
    }

    /**
     *
     * @param
     *            $sheetTitle
     * @param
     *            $data
     * @throws \Exception
     */
    public function generateFile($sheetTitle, $data)
    {
        $this->_excelReader->getProperties()->setCreator('Prism DM');
        $this->_excelReader->getProperties()->setTitle('Bulk Uploader');
        $this->_excelReader->createSheet();

        $i = 0;
        $this->_excelReader->createSheet($i);
        $this->_excelReader->getActiveSheetIndex($i);
        $this->_excelReader->setActiveSheetIndex($i);

        $columnLetter = 'A';
        $rowNumber = 1;

        // Set Column Titles

        foreach ($sheetTitle as $column) {
            $this->_excelReader->getActiveSheet()->setCellValue($columnLetter . $rowNumber, $column);
            ++ $columnLetter;
        }
        ++ $rowNumber;
        // Set Column Values
        foreach ($data as $rows) {
            $newColumnLetter = 'A';
            foreach ($rows as $colVal) {
                $this->_excelReader->getActiveSheet()->setCellValueExplicit($newColumnLetter . $rowNumber, $colVal, PHPExcel_Cell_DataType::TYPE_STRING);
                ++ $newColumnLetter;
            }
            ++ $rowNumber;
        }

        $this->_excelReader->setActiveSheetIndex(0);
        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="template.csv"');
        header('Cache-Control: max-age=0');

        // Add File download cookie to trigger loader stop event
        setcookie('fileDownload', '1', time() + 60, '/');

        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $objWriter = \PHPExcel_IOFactory::createWriter($this->_excelReader, 'CSV');
        $objWriter->save('php://output');
        exit();
    }

    /**
     *
     * @param null $data
     * @return array
     */
    public function getAllPrices($data = null)
    {
        $predifiendCurrencySQL = " ";
        $predifiendCurrencySQL2 = " ";

        if (! is_null($data)) {
            $currencyID = $data['currencyType'];
            if ($currencyID != 'All' && $currencyID !== null) {
                $predifiendCurrencySQL = "AND currencyId = $currencyID";
                $predifiendCurrencySQL2 = "AND c.currencyId = $currencyID";
            }
        }

        // Select 1, get all prices and skus which have PRODUCTS_R_ATTRIBUTES ID
        $select1 = $this->_sql->select()
            ->from(array(
                'pra' => 'PRODUCTS_R_ATTRIBUTES'
            ))
            ->columns(array(
                'sku',
                'productId'
            ))
            ->join(array(
                'pr' => 'PRODUCTS_Prices'
            ), new Expression("pr.productOrVariantId = pra.productAttributeId AND pr.productIdFrom = 'PRODUCTS_R_ATTRIBUTES' AND pr.status = 'ACTIVE' $predifiendCurrencySQL"), array(
                '*'
            ), Select::JOIN_RIGHT)
            ->where('pra.sku IS NOT NULL');

        // subSelect1 is used as nested select query for $slect2 function
        $subSelect1 = $this->_sql->select()
            ->from(array(
                'p' => 'PRODUCTS'
            ))
            ->columns((array()))
            ->join(array(
                'pra2' => 'PRODUCTS_R_ATTRIBUTES'
            ), new Expression("p.productId = pra2.productId"), array(
                '*'
            ), Select::JOIN_LEFT)
            ->where('pra2.productAttributeId IS NULL');

        // Select 2, get all prices and skus which have PRODUCTS ID, mainly e-vouchers
        $select2 = $this->_sql->select()
            ->from(array(
                'T1' => $subSelect1
            ))
            ->columns(array(
                'sku',
                'productId'
            ))
            ->join(array(
                'pr' => 'PRODUCTS_Prices'
            ), new Expression("pr.productOrVariantId = T1.productId AND pr.productIdFrom = 'PRODUCTS_R_ATTRIBUTES' AND pr.status = 'ACTIVE' $predifiendCurrencySQL"), array(
                '*'
            ), Select::JOIN_RIGHT)
            ->where('T1.sku IS NOT NULL');

        // Perform a union join
        $select1->combine($select2, 'UNION');

        // Add currency table to the sku products table
        $selectFinal = $this->_sql->select()
            ->from(array(
                'T2' => $select1
            ))
            ->columns(array(
                '*'
            ))
            ->join(array(
                'c' => 'CURRENCY'
            ), new Expression("c.currencyId = T2.currencyId $predifiendCurrencySQL2"), array(
                '*'
            ), Select::JOIN_LEFT)
            ->order('sku ASC');

        $statement = $this->_sql->prepareStatementForSqlObject($selectFinal);
        $result = $statement->execute();

        // Build the array for csv
        // initialize the result to an empty array

        // If data is not null, its a download template request which means we dont need associative array
        // We cant rely on users reading column titles as they probably will mess everything up
        $priceTemplateArray = array();
        if (! is_null($data)) {
            foreach ($result as $res) {
                $priceTemplateArray[] = array(
                    $res['productOrVariantId'],
                    $res['productPriceId'],
                    $res['sku'],
                    $res['isoCode'],
                    $res['nowPrice'],
                    $res['offerPrice'],
                    $res['wasPrice'],
                    $res['costPrice'],
                    $res['tradingCostPrice'],
                    $res['wholesalePrice'],
                    $res['freightPrice'],
                    $res['dutyPrice'],
                    $res['designPrice'],
                    $res['packagingPrice']
                );
            }
        } else {
            // Return associative array for price comparison and other information use
            foreach ($result as $res) {
                $priceTemplateArray[$res['productPriceId']] = array(
                    'productOrVariantId' => $res['productOrVariantId'],
                    'productIdFrom' => $res['productIdFrom'],
                    'productId' => $res['productId'],
                    'sku' => $res['sku'],
                    'isoCode' => $res['isoCode'],
                    'currencyId' => $res['currencyId'],
                    'price' => $res['nowPrice'],
                    'nowPrice' => $res['nowPrice'],
                    'offerPrice' => $res['offerPrice'],
                    'wasPrice' => $res['wasPrice'],
                    'costPrice' => $res['costPrice'],
                    'tradingCostPrice' => $res['tradingCostPrice'],
                    'wholesalePrice' => $res['wholesalePrice'],
                    'freightPrice' => $res['freightPrice'],
                    'dutyPrice' => $res['dutyPrice'],
                    'designPrice' => $res['designPrice'],
                    'packagingPrice' => $res['packagingPrice']
                );
            }
        }
        return $priceTemplateArray;
    }

    // Creates price column title,
    // TODO Change to switch case for generic implementation
    /**
     *
     * @return array
     */
    public function createPriceColumnTitle()
    {
        $priceTemplateArray = array(
            'Product ID',
            'Price ID',
            'SKU',
            'Currency',
            'Now Price',
            'Offer Price',
            'Was Price',
            'Cost Price',
            'Trading Price',
            'Whole Sale Price',
            'Freight Price',
            'Duty Price',
            'Design Price',
            'Packaging Price'
        );
        return $priceTemplateArray;
    }

    /**
     *
     * @param
     *            $data
     */
    public function bulkUpload($data)
    {
        if (! empty($data['uploadType'])) {
            $uploadData = $this->uploadFile($data);

            switch ($data['uploadType']) {
                case 'All_Common_Attributes':
                    $this->bulkUploadAllCommonAttributes($uploadData['rows']);
                    break;
                case 'Common_Attributes_Cascade':
                    $this->bulkUploadCascadeCommonAttributes($uploadData['rows']);
                    break;
                case 'prices_at_option':
                    $this->bulkUploadPrices($uploadData['rows']);
                    break;
            }

            /**
             * Remove uploaded file from the server
             */
            // $this->unlinkFile($uploadData['filePath']);
            exit();
        }
    }

    /**
     *
     * @param
     *            $rows
     */
    public function bulkUploadPrices($rows)
    {
        $errorArray = array();
        $pricesHeaders = $this->getPricesBasedOnHeaders($rows[0]);

        /**
         * Check if all header values matched and create new error entry
         */
        if (! empty($pricesHeaders['error'])) {
            $errorArray['header-not-matched'] = $pricesHeaders['error'];
        }

        /**
         * Check if the count for header values is higher than 1 to see
         * if any other value matched an price (1 value is from the option code)
         */
        if (count($pricesHeaders['header']) > 1) {
            foreach ($rows as $key => $row) {
                /**
                 * Skip the first row - it contains option
                 */
                if ($key !== 0 && $row) {
                    $row[0] = str_replace(" ", "", $row[0]);
                    /**
                     * Process each row and return any errors might occur
                     */
                    $errorArray['rows'][$key] = $this->processPriceRow($row, $pricesHeaders['header']);
                }
            }
        }

        /**
         * Generate result spreadsheet with errors
         */
        $this->generateErrorSpreadsheet($errorArray, $rows);
    }

    /**
     *
     * @param
     *            $row
     * @param
     *            $headerValues
     * @return array
     */
    private function processPriceRow($row, $headerValues)
    {
        $errorArray = array();
        /**
         * Get all SKUs for this option
         */
        $optionData = $this->productsMapper->getVariantsIdForOption($row[0]);
        /**
         * Check if option has been matched to some variants
         */
        if ($optionData->count() > 0) {
            /**
             * Locate the start date in the header
             */
            $startDate = new \DateTime('now');
            foreach ($headerValues as $headerKey => $header) {
                if ($header == 'Start Date') {
                    try {
                        if (trim($row[$headerKey]) > 10) {
                            $startDate = new \DateTime($row[$headerKey], new \DateTimeZone("Europe/London"));
                        } else {
                            $startDate = new \DateTime($row[$headerKey] . ' 00:00:00', new \DateTimeZone("Europe/London"));
                        }
                    } catch (\Exception $e) {
                        $errorArray['value-not-valid']['Start Date'] = $row[$headerKey] . ' is not valid format';
                        return $errorArray;
                    }
                }
            }

            if ($startDate->getTimestamp() <= time()) {
                /**
                 * If startDate is in the past|present save the price in the prices table
                 */
                $errorArray['value-not-valid'] = $this->savePriceRowDataAgainst('prices-table', $optionData, $headerValues, $row, $startDate);
            } else {
                /**
                 * Else save the prices to a schedule event
                 */
                $errorArray['value-not-valid'] = $this->savePriceRowDataAgainst('schedule-table', $optionData, $headerValues, $row, $startDate);
            }
        } else {
            $errorArray['option-not-matched'] = $row[0];
        }

        return $errorArray;
    }

    /**
     *
     * @param
     *            $saveType
     * @param
     *            $optionData
     * @param
     *            $headerValues
     * @param
     *            $row
     * @param
     *            $startDate
     * @return array
     */
    private function savePriceRowDataAgainst($saveType, $optionData, $headerValues, $row, $startDate)
    {
        $errorArray = array();
        /**
         * Loop all the variant data
         */
        foreach ($optionData as $variant) {
            /**
             * Loop all the header data
             */
            foreach ($headerValues as $key => $headerValue) {
                /**
                 * Skip the key if 0 - it means the header is the option code of the product
                 */
                if ($key !== 0) {

                    $this->_queueService->addSpectrumEntryToQueueForCommonAttributes($variant['productAttributeId']);

                    if (is_array($headerValue)) {
                        /**
                         * Validate value for float
                         */
                        $floatValidator = new \Zend\I18n\Validator\IsFloat();
                        $priceValue = $row[$key];
                        if (! $floatValidator->isValid($priceValue) && ! empty($priceValue)) {
                            $errorArray[$key] = $priceValue . ' is not a valid value for ' . $headerValue['headerName'];
                            continue;
                        }
                        /**
                         * If this is array then we need to save the value/values against the variant
                         */
                        switch ($saveType) {
                            case 'prices-table':
                                if (! isset($headerValue['currency'])) {
                                    foreach ($headerValue as $individualHeaderValue) {
                                        if (is_array($individualHeaderValue)) {
                                            $this->saveRowToVariantAgainstPrices($variant, $key, $row, $individualHeaderValue);
                                        }
                                    }
                                } else {
                                    $this->saveRowToVariantAgainstPrices($variant, $key, $row, $headerValue);
                                }
                                break;
                            case 'schedule-table':
                                if (! isset($headerValue['currency'])) {
                                    foreach ($headerValue as $individualHeaderValue) {
                                        $this->saveRowToVariantAgainstSchedules($variant, $key, $row, $individualHeaderValue, $startDate);
                                    }
                                } else {
                                    $this->saveRowToVariantAgainstSchedules($variant, $key, $row, $headerValue, $startDate);
                                }
                                break;
                        }
                    } else {
                        // headerValue is not array so check if "TC (Price|Currency)" and process upate
                        if ($headerValue == 'TC (Price|Currency)' && $saveType == 'prices-table') {
                            $tradingCostUpdate = $this->updateTradingCostOnOptionLevel($headerValue, $key, $row, $variant);
                            if ($tradingCostUpdate == false) {
                                // @todo add into error report
                            }
                        }

                        if ($headerValue == 'TC (Price|Currency)' && $saveType == 'schedule-table') {
                            $tradingCostSchedule = $this->scheduleTradingCostOnOptionLevel($headerValue, $key, $row, $variant);
                            if ($tradingCostSchedule == false) {
                                // @todo add into error report
                            }
                        }
                    }
                }
            }
        }
        $this->_queueService->addSipEntryToQueue($row[0], 6, $this->productsMapper, true);

        return $errorArray;
    }

    /**
     * Update the tading cost on option level
     * This used in bulk upload called from :
     * ->savePriceRowDataAgainst()
     *
     * @param string` $headerValue
     * @param int $key
     * @param array` $row
     * @param array $variant
     * @return boolean
     */
    public function updateTradingCostOnOptionLevel($headerValue, $key, $row, $variant)
    {
        $productId = $variant['productAttributeId'];
        $recordData = explode("|", $row[$key]);
        if (is_array($recordData) && count($recordData) == 2) {
            $this->productsMapper->upateProductSupplierPriceonOptionLevel($productId, $recordData['0'], $recordData['1']);
            return true;
        }
        return false;
    }

    public function scheduleTradingCostOnOptionLevel($headerValue, $key, $row, $variant)
    {
        return;
    }

    /**
     *
     * @param
     *            $variant
     * @param
     *            $key
     * @param
     *            $row
     * @param
     *            $headerValue
     */
    private function saveRowToVariantAgainstPrices($variant, $key, $row, $headerValue)
    {
        if (isset($headerValue['currency']) && isset($headerValue['territory']) && isset($headerValue['price'])) {
            /**
             * Get currency and territory id
             */
            $currencyId = $this->productsMapper->getCurrencyIdByString($headerValue['currency']);
            $territoryId = $this->productsMapper->getTerritoryId($headerValue['territory']);

            /**
             * Check if variant has already saved prices
             */
            $variantExistingPrice = $this->productsMapper->getVariantPriceBasedOnCurrencyAndTerritory($variant['productAttributeId'], $currencyId, $territoryId, $headerValue['price']);

            if ($variantExistingPrice) {
                /**
                 * Variant has already a price entry so we need to update it
                 */
                $this->productsMapper->updatePriceEntry($variantExistingPrice['productPriceId'], $headerValue['price'], $row[$key]);
            } else {
                /**
                 * Variant need a new price entry - it has none
                 */
                $this->productsMapper->insertNewPriceEntryToVariant($variant['productAttributeId'], $headerValue['price'], $row[$key], $currencyId, $territoryId);
            }
        }
    }

    /**
     *
     * @param
     *            $variant
     * @param
     *            $key
     * @param
     *            $row
     * @param
     *            $headerValue
     * @param
     *            $startDate
     * @return bool
     * @throws \Exception
     */
    private function saveRowToVariantAgainstSchedules($variant, $key, $row, $headerValue, $startDate)
    {
        if (isset($headerValue['currency']) && isset($headerValue['territory']) && isset($headerValue['price'])) {
            /**
             * Get currency and territory id
             */
            $currencyId = $this->productsMapper->getCurrencyIdByString($headerValue['currency']);
            $territoryId = $this->productsMapper->getTerritoryId($headerValue['territory']);

            return $this->scheduleService->insertNewPriceSchedule($variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES', $currencyId, $territoryId, 'PRODUCTS_Prices', $headerValue['price'], $row[$key], $startDate);
        }
        return false;
    }

    /**
     *
     * @param
     *            $errorArray
     * @param
     *            $rows
     */
    private function generatePricesErrorSpreadsheet($errorArray, $rows)
    {}

    /**
     *
     * @param
     *            $headers
     * @return array
     */
    private function getPricesBasedOnHeaders($headers)
    {
        $fixedHeaders = $this->getFixedHeaders();
        $errorArray = array();
        $headerArray = array();
        foreach ($headers as $key => $header) {
            if ($key !== 0) {
                if (! isset($fixedHeaders[$header])) {

                    $errorArray[$key] = $header;
                    continue;
                }
                if (is_array($fixedHeaders[$header])) {
                    $tmpHeader = array(
                        'headerName' => $header
                    );
                    $headerArray[$key] = array_merge($tmpHeader, $fixedHeaders[$header]);
                } else {
                    $headerArray[$key] = $fixedHeaders[$header];
                }
            } else {
                $headerArray[$key] = $header;
            }
        }
        return array(
            'header' => $headerArray,
            'error' => $errorArray
        );
    }

    /**
     *
     * @return array
     */
    private function getFixedHeaders()
    {
        return array(
            'TC (Price|Currency)' => 'TC (Price|Currency)',
            'GBP SP' => array(
                'currency' => 'GBP',
                'territory' => 'GB',
                'price' => 'nowPrice'
            ),
            'GBP WAS SP' => array(
                'currency' => 'GBP',
                'territory' => 'GB',
                'price' => 'wasPrice'
            ),
            'USD SP' => array(
                'currency' => 'USD',
                'territory' => 'US',
                'price' => 'nowPrice'
            ),
            'USD WAS SP' => array(
                'currency' => 'USD',
                'territory' => 'US',
                'price' => 'wasPrice'
            ),
            'CAD SP' => array(
                'currency' => 'CAD',
                'territory' => 'CA',
                'price' => 'nowPrice'
            ),
            'CAD WAS SP' => array(
                'currency' => 'CAD',
                'territory' => 'CA',
                'price' => 'wasPrice'
            ),
            'AUD SP' => array(
                'currency' => 'AUD',
                'territory' => 'GB',
                'price' => 'nowPrice'
            ),
            'AUD WAS SP' => array(
                'currency' => 'AUD',
                'territory' => 'GB',
                'price' => 'wasPrice'
            ),
            'EUR SP' => array(
                'currency' => 'EUR',
                'territory' => 'EU',
                'price' => 'nowPrice'
            ),
            'EUR WAS SP' => array(
                'currency' => 'EUR',
                'territory' => 'EU',
                'price' => 'wasPrice'
            ),
            'DE EUR SP' => array(
                'currency' => 'EUR',
                'territory' => 'DE',
                'price' => 'nowPrice'
            ),
            'DE EU WAS SP' => array(
                'currency' => 'EUR',
                'territory' => 'DE',
                'price' => 'wasPrice'
            ),
            'GBP CP' => array(
                array(
                    'currency' => 'GBP',
                    'territory' => 'GB',
                    'price' => 'costPrice'
                ),
                array(
                    'currency' => 'GBP',
                    'territory' => 'ROW',
                    'price' => 'costPrice'
                ),
                array(
                    'currency' => 'GBP',
                    'territory' => 'EU',
                    'price' => 'costPrice'
                )
            ),
            'USD CP' => array(
                'currency' => 'USD',
                'territory' => 'US',
                'price' => 'costPrice'
            ),
            'CAD CP' => array(
                'currency' => 'CAD',
                'territory' => 'CA',
                'price' => 'costPrice'
            ),
            'AUD CP' => array(
                'currency' => 'AUD',
                'territory' => 'GB',
                'price' => 'costPrice'
            ),
            'EUR CP' => array(
                'currency' => 'EUR',
                'territory' => 'DE',
                'price' => 'costPrice'
            ),
            'Start Date' => 'Start Date'
        );
    }

    /**
     *
     * @param
     *            $data
     * @return array
     */
    private function uploadFile($data, $newFileName = null, $changeName = false)
    {
        $httpAdapter = new \Zend\File\Transfer\Adapter\Http();

        $newFile = '';
        if ($httpAdapter->isValid()) {
            $uploadPath = ROOT_PATH . '/public/uploads/';
            $httpAdapter->setDestination($uploadPath);
            if ($httpAdapter->receive($data['feedAttachment']['name'])) {
                $newFile = $httpAdapter->getFileName();
                if ($changeName === true) {
                    if ($newFileName == null) {
                        $newFileName = $uploadPath . $this->generateUniqueFileName($data['feedAttachment']['name']);
                    } else {
                        $newFileName = $uploadPath . $newFileName;
                    }

                    @copy($uploadPath . $data['feedAttachment']['name'], $newFileName);
                    $newFile = $newFileName;
                }
            }
        }

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($newFile);

        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();
        $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn) - 1;

        for ($row = 1; $row <= $highestRow; ++ $row) {
            $file_data = array();
            for ($col = 0; $col <= $highestColumnIndex; ++ $col) {
                //$value = iconv("UTF-8", "ISO-8859-1//IGNORE", $objWorksheet->getCellByColumnAndRow($col, $row)->getValue());
                $value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                $file_data[$col] = trim($value);
            }
            $result[] = $file_data;
        }

        return array(
            'rows' => $result,
            'filePath' => $newFile
        );
    }

    /**
     *
     * @param
     *            $rows
     * @return array
     * @internal param $filePath
     */
    private function bulkUploadAllCommonAttributes($rows)
    {
        try {
            $errorArray = array();
            // var_dump(
            // $rows[0]
            // );
            $commonAttributes = $this->getCommonAttributesBasedOnHeader($rows[0]);

            // var_dump(
            // $commonAttributes['error']
            // );

            /**
             * Check if all header values matched and create new error entry
             */
            if (! empty($commonAttributes['error'])) {
                $errorArray['header-not-matched'] = $commonAttributes['error'];
            }

            /**
             * Check if the count for header values is higher than 1 to see
             * if any other value matched an attribute (1 value is from the option code)
             */
            if (count($commonAttributes['header']) > 1) {
                foreach ($rows as $key => $row) {
                    /**
                     * Skip the first row - it contains header values
                     */
                    if ($key !== 0 && $row) {

                        // var_dump(
                        // $row
                        // );

                        /**
                         * Process each row and return any errors might occur
                         */
                        $errorArray['rows'][$key] = $this->processCommonAttributeRow($row, $commonAttributes['header']);
                    }
                }
            }

            /**
             * Generate result spreadsheet with errors
             */
            $this->generateErrorSpreadsheet($errorArray, $rows);
        } catch (\Exception $ex) {
            return true;
        }
    }

    /**
     *
     * @param
     *            $rows
     * @return array
     */
    private function bulkUploadCascadeCommonAttributes($rows)
    {
        try {
            $errorArray = array();
            $commonAttributes = $this->getCommonAttributesBasedOnHeader($rows[0]);

            /**
             * Check if all header values matched and create new error entry
             */
            if (! empty($commonAttributes['error'])) {
                $errorArray['header-not-matched'] = $commonAttributes['error'];
            }

            /**
             * Check if the count for header values is higher than 1 to see
             * if any other value matched an attribute (1 value is from the option code)
             */
            if (count($commonAttributes['header']) > 1) {
                foreach ($rows as $key => $row) {
                    /**
                     * Skip the first row - it contains header values
                     */
                    if ($key !== 0 && $row) {

                        /**
                         * Process each row and return any errors might occur
                         */
                        $errorArray['rows'][$key] = $this->processCommonAttributeRow($row, $commonAttributes['header']);
                    }
                }
            }
            /**
             * Generate result spreadsheet with errors
             */
            $this->generateErrorSpreadsheet($errorArray, $rows);
        } catch (\Exception $ex) {
            return true;
        }
    }

    /**
     *
     * @param
     *            $filePath
     * @return bool
     */
    private function unlinkFile($filePath)
    {
        try {
            @unlink($filePath);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     *
     * @param
     *            $errorArray
     * @return bool
     */
    private function createSpreadsheetForHeaderErrors($errorArray)
    {
        try {
            $columnLetter = 'A';
            $rowNumber = 1;

            /**
             * Set up headers
             */
            $this->_excelReader->getActiveSheet()->setCellValue($columnLetter . $rowNumber, 'Headers not matched');
            $rowNumber ++;

            /**
             * Create cells for headers that did not matched
             */
            if (! empty($errorArray['header-not-matched'])) {
                foreach ($errorArray['header-not-matched'] as $headerName) {
                    $this->_excelReader->getActiveSheet()->setCellValueExplicit($columnLetter . $rowNumber, $headerName, PHPExcel_Cell_DataType::TYPE_STRING);
                    $rowNumber ++;
                }
            }

            /**
             * Add error note
             */
            $this->_excelReader->getActiveSheet()->setCellValueExplicit($columnLetter . $rowNumber, 'Note: Headers above could not be matched to any of the common attributes for all options !', PHPExcel_Cell_DataType::TYPE_STRING);

            return $rowNumber;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     *
     * @param array $array
     * @return bool
     */
    public static function is_assoc(array $array)
    {
        // Keys of the array
        $keys = array_keys($array);

        // If the array keys of the keys match the keys, then the array must
        // not be associative (e.g. the keys array looked like {0:0, 1:1...}).
        return array_keys($keys) !== $keys;
    }

    /**
     *
     * @param
     *            $errorArray
     * @param
     *            $rows
     * @return bool
     */
    private function createSpreadsheetForOtherErrors($errorArray, $rows, $rowNrLeft)
    {
        try {
            $columnLetter = 'A';
            $rowNumber = $rowNrLeft + 3;

            $this->_excelReader->getActiveSheet()->setCellValue($columnLetter . $rowNumber, 'Option Code');
            $columnLetter ++;
            $this->_excelReader->getActiveSheet()->setCellValue($columnLetter . $rowNumber, 'Errors');
            $rowNumber ++;

            foreach ($errorArray['rows'] as $keyRow => $errors) {
                /**
                 * Set up option code
                 */
                $columnLetter = 'A';
                $this->_excelReader->getActiveSheet()->setCellValue($columnLetter . $rowNumber, $rows[$keyRow][0]);
                $columnLetter ++;

                if ($this->is_assoc($errors) && isset($errors['option-not-matched'])) {
                    /**
                     * Option could not be found error
                     */
                    $this->_excelReader->getActiveSheet()->setCellValue($columnLetter . $rowNumber, 'Option could not be found');
                } else {
                    /**
                     * Other errors
                     */
                    foreach ($errors as $headerKey => $error) {
                        switch (true) {
                            case isset($error['value-not-found']):
                                if (is_array($error['value-not-found'])) {
                                    $optionCode = $rows[$keyRow][0];
                                    if (! empty($error['value-not-found'][$optionCode])) {
                                        foreach ($error['value-not-found'][$optionCode] as $errorMessage) {
                                            $this->_excelReader->getActiveSheet()->setCellValue($columnLetter . $rowNumber, $errorMessage);
                                            $columnLetter ++;
                                        }
                                    }
                                } else {
                                    $this->_excelReader->getActiveSheet()->setCellValue($columnLetter . $rowNumber, $error['value-not-found']);
                                    $columnLetter ++;
                                }
                                break;

                            case isset($error['checkbox']):
                                $this->_excelReader->getActiveSheet()->setCellValue($columnLetter . $rowNumber, $error['checkbox']);
                                $columnLetter ++;
                                break;
                            default:
                                if (is_array($error)) {
                                    foreach ($error as $er) {
                                        $this->_excelReader->getActiveSheet()->setCellValue($columnLetter . $rowNumber, $er);
                                        $columnLetter ++;
                                    }
                                }
                                break;
                        }
                    }
                }
                $rowNumber ++;
            }
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     *
     * @param
     *            $errorArray
     * @throws \Exception
     */
    private function generateErrorSpreadsheet($errorArray, $rows)
    {
        $this->_excelReader->getProperties()->setCreator('Prism DM');
        $this->_excelReader->getProperties()->setTitle('other errors');
        $this->_excelReader->createSheet();

        $this->_excelReader->createSheet(0);
        $this->_excelReader->getActiveSheetIndex(0);
        $this->_excelReader->setActiveSheetIndex(0);

        // die;
        /**
         * Create spreadsheet for header errors
         */
        $rowNrLeft = $this->createSpreadsheetForHeaderErrors($errorArray);
        /**
         * Create spreadsheet for other errors errors
         */
        $this->createSpreadsheetForOtherErrors($errorArray, $rows, $rowNrLeft);

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="errors.csv"');
        header('Cache-Control: max-age=0');

        // Add File download cookie to trigger loader stop event
        setcookie('fileDownload', '1', time() + 60, '/');

        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $objWriter = \PHPExcel_IOFactory::createWriter($this->_excelReader, 'CSV');
        $objWriter->save('php://output');
    }

    /**
     *
     * @param
     *            $row
     * @param
     *            $headerValues
     * @return array
     */
    private function processCommonAttributeRow($row, $headerValues)
    {
        $errorArray = array();
        // once option are same upate the value for on style level
        $updatedOnStyleLevel = array();
        $attributeValueForStyleLevelUpdate = null;

        /**
         * Get all SKUs for this option
         */
        $optionData = $this->productsMapper->getVariantsIdForOption($row[0]);

        /**
         * Check if option has been matched to some variants
         */
        if ($optionData->count() > 0) {
            /**
             * Loop all the variant data
             */
            foreach ($optionData as $variant) {
                /**
                 * Loop all the header data
                 */
                foreach ($headerValues as $key => $headerValueEntity) {
                    /**
                     * Skip the key if 0 - it means the header is the option code of the product
                     */
                    if ($key !== 0) {
                        if ($headerValueEntity != 'Style Name' && $headerValueEntity != 'Style Short Name') {
                            /**
                             * Get the attribute type in order to validate data against it
                             */
                            $attributeType = $headerValueEntity->getCommonAttributeId()
                                ->getCommonAttributeGuiRepresentation()
                                ->getCommonAttributesViewTypeId()
                                ->getCommonAttributeViewType();
                            $attributeValue = $row[$key];
                            $commonAttributeId = $headerValueEntity->getCommonAttributeId()->getCommonAttributeId();

                            switch ($attributeType) {
                                case 'CHECKBOX':
                                    /**
                                     * Attribute Value for checkbox should be 0 or 1
                                     */
                                    if ($attributeValue != '0' && $attributeValue != '1') {
                                        $errorArray[$key]['checkbox'] = $attributeValue . ' is not a valid value for the common attribute ' . $headerValueEntity->getDescription();
                                        break;
                                    }
                                    $this->productsMapper->deleteProductCommonAttribute($commonAttributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
                                    $this->productsMapper->saveCommonAttributeAgainstProduct($attributeValue, $commonAttributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
                                    $attributeValueForStyleLevelUpdate = $attributeValue;
                                    break;
                                case 'SELECT':
                                    $attributeValueEntity = $this->commonAttributesService->getCommonAttributeValueByName($attributeValue, $commonAttributeId);
                                    if (empty($attributeValueEntity)) {
                                        /**
                                         * Save as error - value was not found at the attribute values
                                         */
                                        $errorArray[$key]['value-not-found'] = $attributeValue . ' value was not found to the common attribute ' . $headerValueEntity->getDescription();
                                    } else {
                                        $attributeValueId = $attributeValueEntity->getCommonAttributeValueId();
                                        /**
                                         * Save the value against the product
                                         */
                                        $this->productsMapper->deleteProductCommonAttribute($commonAttributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
                                        $this->productsMapper->saveCommonAttributeAgainstProduct($attributeValueId, $commonAttributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
                                        $attributeValueForStyleLevelUpdate = $attributeValueId;
                                    }
                                    break;
                                case 'MULTISELECT':
                                    unset($attributeValueForStyleLevelUpdate); // incase exist we will replce it with array
                                    /**
                                     * Delete current attributes for common attribute id
                                     */
                                    $this->productsMapper->deleteProductCommonAttribute($commonAttributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
                                    /**
                                     * Explode values by comma and loop all the values, checking if each value exists
                                     */
                                    $attributeValues = explode(',', $attributeValue);
                                    foreach ($attributeValues as $attributeValue) {
                                        $attributeValueEntity = $this->commonAttributesService->getCommonAttributeValueByName($attributeValue, $commonAttributeId);
                                        if (empty($attributeValueEntity)) {
                                            /**
                                             * Save as error - value was not found at the attribute values
                                             */
                                            $errorArray[$key]['value-not-found'][$row[0]][$attributeValue] = $attributeValue . ' value was not found to the common attribute ' . $headerValueEntity->getDescription();
                                        } else {
                                            /**
                                             * Attribute value exists and and it is valid => save it against the product
                                             */
                                            $attributeValueId = $attributeValueEntity->getCommonAttributeValueId();
                                            $this->productsMapper->saveCommonAttributeAgainstProduct($attributeValueId, $commonAttributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
                                            $attributeValueForStyleLevelUpdate[] = $attributeValueId;
                                        }
                                    }
                                    break;
                                case 'TEXT':
                                case 'DATEPICKER':

                                    // echo $attributeValue; die;
                                    try {
                                        if (trim($attributeValue) > 10) {
                                            $attributeValue = new \DateTime($attributeValue, new \DateTimeZone("Europe/London"));
                                        } else {
                                            $attributeValue = new \DateTime($attributeValue . ' 00:00:00', new \DateTimeZone("Europe/London"));
                                        }
                                    } catch (\Exception $e) {
                                        $errorArray[$key]['value-not-valid'] = $attributeValue . ' is not valid format';
                                        return $errorArray;
                                    }
                                case 'TEXTAREA':
                                case 'DATEPICKER - DATE ONLY':
                                    $this->productsMapper->deleteProductCommonAttribute($commonAttributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
                                    $this->productsMapper->saveCommonAttributeAgainstProduct($attributeValue, $commonAttributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
                                    $attributeValueForStyleLevelUpdate = $attributeValue;
                                    break;
                            }
                        } else {
                            // process Style name and short style name
                            $result = $this->processBulkUploadStyleNames($variant, $headerValueEntity, $row, $key);
                            $product = $variant['productId'];
                            if (($headerValueEntity == 'Style Name' && ! isset($updatedOnStyleLevel[$product]['TableData']['Style Name'])) || ($headerValueEntity == 'Style Short Name' && ! isset($updatedOnStyleLevel[$product]['TableData']['Style Short Name']))) {
                                unset($commonAttributeId); // column is not common attribute therefore remove from previous loop!
                                $updatedOnStyleLevel[$product]['TableData'][$headerValueEntity] = $row[$key];
                            }
                            if ($result == false) {
                                // add to error
                            }
                        }
                        // Once the common attribute is saved, then add the variant into QUEUE_Manager for specturm update
                        $this->_queueService->addSpectrumEntryToQueueForCommonAttributes($variant['productAttributeId']);

                        // prepare data for style levle update
                        $product = $variant['productId'];
                        if (isset($commonAttributeId) && ! isset($updatedOnStyleLevel[$product]['commonAttribute'][$commonAttributeId])) {
                            $updatedOnStyleLevel[$product]['commonAttribute'][$commonAttributeId]['type'] = $attributeType;
                            $updatedOnStyleLevel[$product]['commonAttribute'][$commonAttributeId]['value'] = $attributeValueForStyleLevelUpdate;
                        }
                    }
                }
            }
            // once loop finished add to queue record ro SIP
            $this->_queueService->addSipEntryToQueue($row[0], 5, $this->productsMapper, true);

            $this->updateSkuValueOnOptionsEqual($updatedOnStyleLevel);
        } else {
            $errorArray['option-not-matched'] = $row[0];
        }

        return $errorArray;
    }

    /**
     * Update the value of common attribute on style level when option value is same
     *
     * @param array $updatedOnStyleLevel
     */
    public function updateSkuValueOnOptionsEqual($updatedOnStyleLevel)
    {
        // Get The style from option
        $style = $updatedOnStyleLevel; // getStyleFromOption($option);
        foreach ($updatedOnStyleLevel as $productId => $value) {
            $variants = $this->productsMapper->getVariants($productId);
            $variantsIds = $this->listVariantProductId($variants);

            // update standard common attribute
            if (isset($updatedOnStyleLevel[$productId]['commonAttribute'])) {
                foreach ($updatedOnStyleLevel[$productId]['commonAttribute'] as $commonAttributeId => $commonAttributeIdData) {
                    $this->updateStyleLevelCommonAttributeIdOnSamevalue($productId, $commonAttributeId, $variantsIds, $commonAttributeIdData['value'], $commonAttributeIdData['type']);
                }
            }
            // Update common attribute where value is saved in other table currently
            // Bluk upload support only Style Short Name, Style Short
            if (isset($updatedOnStyleLevel[$productId]['TableData'])) {
                foreach ($updatedOnStyleLevel[$productId]['TableData'] as $tableDataKey => $tableDataValue) {
                    $this->updateStyleLevelTableDataOnSamevalue($productId, $variantsIds, $tableDataKey, $tableDataValue);
                }
            }
        }
        return $style;
    }

    protected function updateStyleLevelTableDataOnSamevalue($productId, $variantsIds, $tableDataKey, $tableDataValue)
    {
        $columnName = "shortProductName";
        if ($tableDataKey == "Style Name") {
            $columnName = "productName";
        }
        $isSame = $this->productsMapper->isSameColummnValueForAllVariants($variantsIds, $columnName, $tableDataValue);

        if ($isSame) {
            $this->productsMapper->UpdateStyleLevelColummnValueOnsameVariantsValue($productId, $columnName, $tableDataValue);
        }
    }

    /**
     *
     * @param int $productId
     * @param int $commonAttributeId
     * @param string $variantsIds
     *            list of option this will be used in mysql query IN()
     * @param
     *            string | array $commonAttributeIdValue array when multislect
     * @param string $commonAttributeIdDataType
     */
    protected function updateStyleLevelCommonAttributeIdOnSamevalue($productId, $commonAttributeId, $variantsIds, $commonAttributeIdValue, $commonAttributeIdDataType)
    {
        $isSameValue = $this->productsMapper->isSameValueCrossOptions($commonAttributeId, $variantsIds, $commonAttributeIdValue, $commonAttributeIdDataType);
        if ($isSameValue == true) {
            $this->productsMapper->updateCommonAttbuteValueOnStyleLevel($productId, $commonAttributeId, $commonAttributeIdValue);
        }
    }

    /**
     * Return list of all variants id the function will return array / string / Json
     *
     * @param array $variants
     * @param string $returnType
     *            array|string|json
     * @param string $delimeter
     *            used with string type return
     */
    protected function listVariantProductId($variants, $returnType = 'string', $delimeter = ', ')
    {
        $totalIds = count($variants['data']);
        if ($returnType == 'string') {
            $result = '';
            for ($i = 0; $i < $totalIds; $i ++) {
                $variantDetails = $variants['data'][$i];
                if ($i == $totalIds - 1) {
                    $result .= $variantDetails['productAttributeId'];
                } else {
                    $result .= $variantDetails['productAttributeId'] . $delimeter;
                }
            }
            return $result;
        }

        // @TODO expand the fucntion to return array and json
    }

    /**
     *
     * @param array $variant
     * @param string $headerValueEntity
     * @param array $row
     * @param string $key
     */
    public function processBulkUploadStyleNames($variant, $headerValueEntity, $row, $key)
    {
        if ($headerValueEntity == 'Style Name') {
            return $this->productsMapper->updateProductNameByProductId($variant['productAttributeId'], $row[$key]);
        }

        if ($headerValueEntity == 'Style Short Name') {
            return $this->productsMapper->updateShortProductNameByProductId($variant['productAttributeId'], $row[$key]);
        }
    }

    /**
     *
     * @param
     *            $header
     * @return array
     */
    private function getCommonAttributesBasedOnHeader($header)
    {
        $headerArray = array();
        $errorArray = array();
        foreach ($header as $key => $commonAttributeValueDefinition) {
            if ($key !== 0) {
                if ($commonAttributeValueDefinition != 'Style Name' && $commonAttributeValueDefinition != 'Style Short Name') {
                    /**
                     * Get common attribute
                     */
                    $commonAttributeEntity = $this->commonAttributesService->getCommonAttributeByName($commonAttributeValueDefinition);
                    if (! empty($commonAttributeEntity)) {
                        /**
                         * Header value could be matched to common attribute value
                         */
                        $headerArray[$key] = $commonAttributeEntity;
                    } else {
                        /**
                         * Header value could not be matched to existing common attribute
                         */
                        $errorArray[$key] = $commonAttributeValueDefinition;
                    }
                } else {
                    // SKIP as this Style Name or Style Short Name
                    $headerArray[] = $commonAttributeValueDefinition;
                }
            } else {
                /**
                 * Skip the first column as this is the option code column
                 */
                $headerArray[] = $commonAttributeValueDefinition;
            }
        }
        return array(
            'header' => $headerArray,
            'error' => $errorArray
        );
    }

    /**
     *
     * @param
     *            $data
     * @return array
     * @throws \Exception
     */
    public function bulkUploadFile($data)
    {
        // Check the file and Save it physical to the server
        $httpAdapter = new \Zend\File\Transfer\Adapter\Http();
        $fileSize = new \Zend\Validator\File\Size(array(
            'max' => 50000000
        )); // 50MB
        $extension = new \Zend\Validator\File\Extension(array(
            'extension' => array(
                'csv',
                'csv'
            )
        ));
        $httpAdapter->setValidators(array(
            $fileSize,
            $extension
        ), $data['feedAttachment']['name']);

        $newFile = '';
        if ($httpAdapter->isValid()) {
            $uploadPath = ROOT_PATH . '/public/uploads/';
            $httpAdapter->setDestination($uploadPath);
            if ($httpAdapter->receive($data['feedAttachment']['name'])) {
                $newFile = $httpAdapter->getFileName();
            }
        }

        $rows = array();

        $objReader = \PHPExcel_IOFactory::createReader('CSV');
        $objPHPExcel = $objReader->load($newFile);

        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();
        $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn) - 1;

        for ($row = 1; $row <= $highestRow; ++ $row) {
            $file_data = array();
            for ($col = 0; $col <= $highestColumnIndex; ++ $col) {
                $value = iconv("UTF-8", "ISO-8859-1//IGNORE", $objWorksheet->getCellByColumnAndRow($col, $row)->getValue());
                $file_data[$col] = trim($value);
            }
            $rows[] = $file_data;
        }

        // Unset Header Titles
        unset($rows[0]);

        $currentPricesArray = $this->getAllPrices();

        // Hold Product Id's which have been added to spectrum & elastic search
        $productIdHolder = array();

        $countChanges = 0;

        // Begin SQL Transaction so if there are errors we can roll back
        $transactionErrorsArray = array();
        $this->_sql->getAdapter()->driver->getConnection()->beginTransaction();

        foreach ($rows as $row) {

            // Check if row is set by checking row ID (first excel column)
            if (isset($row[0])) {
                // Declare IDs with their types
                $productOrVariantId = (int) $row[0];
                $priceID = (string) $row[1];

                if (isset($currentPricesArray[$priceID])) {
                    /**
                     * Convert Excel row Headers to PRODUCTS_Price columns headers
                     * Change excel row column types to string as it returns float & sql returns string for all
                     * Compare excel row values with sql values for any changes
                     */
                    $newPriceArray = array(
                        'nowPrice' => number_format((float) $row['4'], 2, '.', ''),
                        'price' => number_format((float) $row['4'], 2, '.', ''),
                        'offerPrice' => number_format((float) $row['5'], 2, '.', ''),
                        'wasPrice' => number_format((float) $row['6'], 2, '.', ''),
                        'costPrice' => number_format((float) $row['7'], 2, '.', ''),
                        'tradingCostPrice' => number_format((float) $row['8'], 2, '.', ''),
                        'wholesalePrice' => number_format((float) $row['9'], 2, '.', ''),
                        'freightPrice' => number_format((float) $row['10'], 2, '.', ''),
                        'dutyPrice' => number_format((float) $row['11'], 2, '.', ''),
                        'designPrice' => number_format((float) $row['12'], 2, '.', ''),
                        'packagingPrice' => number_format((float) $row['13'], 2, '.', '')
                    );

                    if ($this->compareAssociativeArrays($newPriceArray, $currentPricesArray[$priceID]) == true) {
                        /**
                         * Record Needs Updating
                         * Deactivate Current Price for this record
                         */
                        $this->_productsMapperPrice->_deactivateProductPrices($productOrVariantId, $currentPricesArray[$priceID]['currencyId'], $currentPricesArray[$priceID]['productIdFrom']);

                        // If product ID is in product id holder array, spectrum & elastic has been notified about the update for that product ID
                        if (in_array($currentPricesArray[$priceID]['productId'], $productIdHolder)) {
                            // Update DB
                            $insertResult = $this->_productsMapperPrice->insertNewVariantPrices($currentPricesArray[$priceID]['productOrVariantId'], $currentPricesArray[$priceID]['currencyId'], $newPriceArray);
                        } else {
                            array_push($productIdHolder, $currentPricesArray[$priceID]['productId']);

                            $insertResult = $this->_productsMapperPrice->insertNewVariantPrices($currentPricesArray[$priceID]['productOrVariantId'], $currentPricesArray[$priceID]['currencyId'], $newPriceArray);
                            // Update Spectrum
                            $this->_queueService->addSpectrumEntryToQueue($currentPricesArray[$priceID]['productId']);
                            // update the product in Elastic Search
                            $this->_queueService->addIndexEntryToQueue($currentPricesArray[$priceID]['productId'] . '_0', ElasticSearchTypes::PRODUCTS);
                        }
                        if ($insertResult === 0) {
                            $transactionErrorsArray = array(
                                "$currentPricesArray[$priceID][sku]  has changes but has not been affected"
                            );
                        }
                        $countChanges = $countChanges + $insertResult;
                    }
                }
            }
        }

        if (count($transactionErrorsArray) === 0) {
            $this->_sql->getAdapter()->driver->getConnection()->commit();
            $response = array(
                'Result' => 'Success',
                'countChanges' => "$countChanges records have been updated"
            );
        } else {
            $this->_sql->getAdapter()->driver->getConnection()->rollback();
            $response = array(
                'Result' => 'Error',
                'Errors' => $transactionErrorsArray
            );
        }

        return $response;
    }

    /**
     *
     * @param array $array1
     * @param array $array2
     * @throws \Exception
     */
    public function compareAssociativeArrays($array1 = array(), $array2 = array())
    {
        $notMatch = false;
        if (! empty($array1) && ! empty($array2)) {
            foreach ($array1 as $key => $value) {
                if (isset($array2[$key])) {
                    if ($array2[$key] != $value) {
                        /**
                         * Does Not Match
                         * Set value to true
                         */
                        $notMatch = true;
                    }
                } else {
                    throw new \Exception("$key not found in Array 2. All keys from array 1 required in array 2.");
                }
            }
            // Return result
            return $notMatch;
        } else {
            throw new \Exception("Array 1 or Array 2 is empty.");
        }
    }

    public function downloadTemplate($downloadType)
    {
        $headersArray = array();
        switch ($downloadType) {
            case 'All_Common_Attributes':
                $commonAttributes = $this->commonAttributesService->getAllCommonAttributes();
                $headersArray[] = 'Option (Style + Color)';
                foreach ($commonAttributes as $header) {
                    if (! $header->getissize()) {
                        $headersArray[] = $header->getcommonAttributeDefinitions()->getdescription();
                    }
                }
                break;
            case 'Common_Attributes_Cascade':
                $commonAttributes = $this->commonAttributesService->getCascadeCommonAttributes();
                $headersArray[] = 'Style';
                $headersArray[] = 'Style Name';
                $headersArray[] = 'Style Short Name';
                foreach ($commonAttributes as $header) {
                    if (! $header->getissize()) {
                        $headersArray[] = $header->getcommonAttributeDefinitions()->getdescription();
                    }
                }
                break;
            case 'Price':
                $headersArray = array(
                    'Option (Style + Color)',
                    'TC (Trading Currency)',
                    'TCP (Trading Currency Price)',
                    'GBP SP',
                    'GBP WAS SP',
                    'USD SP',
                    'USD WAS SP',
                    'CAD SP',
                    'CAD WAS SP',
                    'AUD SP',
                    'AUD WAS SP',
                    'EUR SP',
                    'EUR WAS SP',
                    'DE EUR SP',
                    'DE EU WAS SP',
                    'GBP CP',
                    'USD CP',
                    'CAD CP',
                    // LTS request to hide AUD CP, EUR CP. Please do not delete only comment out
                    // 'AUD CP',
                    // 'EUR CP',
                );
                break;
            case 'Price_cascade':
                $headersArray = array(
                    'Style',
                    'TC (Trading Currency)',
                    'TCP (Trading Currency Price)',
                    'GBP SP',
                    'GBP WAS SP',
                    'USD SP',
                    'USD WAS SP',
                    'CAD SP',
                    'CAD WAS SP',
                    'AUD SP',
                    'AUD WAS SP',
                    'EUR SP',
                    'EUR WAS SP',
                    'DE EUR SP',
                    'DE EU WAS SP',
                    'GBP CP',
                    'USD CP',
                    'CAD CP',
                    // LTS request to hide AUD CP, EUR CP. Please do not delete only comment out
                    // 'AUD CP',
                    // 'EUR CP',
                );
                break;
        }

        $this->_excelReader = PHPExcel_IOFactory::load(self::EXCEL_TEMPLATE);

        $this->_excelReader->getProperties()->setCreator('Prism DM');
        $this->_excelReader->getProperties()->setTitle('Bulk Uploader');

        $i = 0;
        //$this->_excelReader->createSheet($i);
        $this->_excelReader->getActiveSheetIndex($i);
        $this->_excelReader->setActiveSheetIndex($i);

        $columnLetter = 'A';
        $rowNumber = 1;

        // Set Column Titles

        foreach ($headersArray as $header) {
            $this->_excelReader->getActiveSheet()->setCellValueExplicit($columnLetter . $rowNumber, $header, PHPExcel_Cell_DataType::TYPE_STRING);
            ++ $columnLetter;
        }

        $this->_excelReader->setActiveSheetIndex(0);
        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="' . $downloadType . '.xlsx"');
        header('Cache-Control: max-age=0');

        // Add File download cookie to trigger loader stop event
        setcookie('fileDownload', '1', time() + 60, '/');

        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $objWriter = \PHPExcel_IOFactory::createWriter($this->_excelReader, 'Excel2007');
        $objWriter->save('php://output');
        exit();
    }

    /**
     * Check if the common attribute "ready for web" is selected
     *
     * @param string $optionCode
     * @return boolean
     */
    protected function isReadyForWeb($optionCode)
    {
        // On LTS request bulk should always update SIP
        return true;

        // return $this->productsMapper->isReadyForWeb($optionCode);
    }

    /**
     * generate
     *
     * @param unknown $uploadPath
     * @param unknown $fileName
     * @return string
     */
    protected function generateUniqueFileName($fileName, $uploadPath = null)
    {
        $originalFileName = $fileName;

        $temp = explode('.', $fileName);
        $ext = array_pop($temp);
        $fileName = implode('.', $temp);
        $date = date('YmdHis');

        $filter = new StringToLower('UTF-8');
        $fileName = $filter->filter($fileName);

        $filter = new Alnum(true);
        $fileName = $filter->filter($fileName);

        $fileName = str_replace(' ', '_', $fileName) . "_" . $date . "." . $ext;

        if ($uploadPath !== null) {
            @copy($uploadPath . $originalFileName, $uploadPath . $fileName);
            return $uploadPath . $fileName;
        }

        return $fileName;
    }

    public function uplaodTemplate($userId, $data)
    {
        $response = [
            'error' => false,
            'message' => []
        ];

        $originalFileName = $data['feedAttachment']['name'];
        //var_dump(mb_detect_encoding($originalFileName));
        //die;
        $changedFileName = $this->generateUniqueFileName($data['feedAttachment']['name']);
        $uploadData = $this->uploadFile($data, $changedFileName, true);

        $fileHeading = $this->getFileHeading($uploadData);

        if ($data['uploadType'] == 'All_Common_Attributes' || $data['uploadType'] == 'Common_Attributes_Cascade') {
            $response = $this->validateCommonAttributesHeading($fileHeading, $data['uploadType']);
        }

        if ($data['uploadType'] == 'Price' || $data['uploadType'] == 'Price_cascade') {
            $response = $this->validatePricesHeading($fileHeading, $data['uploadType']);
        }

        if (! $response['error']) {
            $processType = $this->getPocessType($data['uploadType']);
            $submittedBy = $userId->userId;
            $response = $this->addBulkProcess($processType, $submittedBy, $originalFileName, $changedFileName, $fileHeading, $uploadData);

        }

        return $response;
    }

    protected function getFileHeading($uploadData)
    {
        $headings = [];
        array_walk($uploadData['rows'][0], function ($row) use (&$headings) {
            if ($row !== '') {
                $headings[] = $row;
            }
        });

        return $headings;
    }

    protected function getFileHeadingValues()
    {
        return;
    }

    /**
     * Return the process_type from BULK_PROCESSING table
     *
     * @param unknown $uploadType
     * @return string
     */
    public function getPocessType($uploadType)
    {
        $processType = [
            'All_Common_Attributes' => 'COMMON_ALL',
            'Common_Attributes_Cascade' => 'COMMON_CASCADE',
            'Price' => 'PRICE_ALL',
            'Price_cascade' => 'PRICE_CASCADE'
        ];
        return $processType[$uploadType];
    }

    /**
     * Validate uploaded files headers
     *
     * All Common Attributes file
     * - should contain only common attribute that are editable on all(style, option) or editable on option level only
     *
     * Common Attributes Cascades file
     * - should contain only common attribute that are editable at style level
     */
    public function validateCommonAttributesHeading($fileHeading, $uploadType)
    {
        $response = [
            'error' => false,
            'message' => []
        ];

        $errorMessage = [];

        if ($uploadType === 'All_Common_Attributes') {
            $commonAttributes = $this->commonAttributesService->getAllCommonAttributes();
            $headersArray[0] = 'Option (Style + Color)';
        } else {
            $commonAttributes = $this->commonAttributesService->getCascadeCommonAttributes();
            $headersArray[0] = 'Style';
            $headersArray[1] = 'Style Name';
            $headersArray[2] = 'Style Short Name';
        }

        foreach ($commonAttributes as $header) {
            if (! $header->getissize()) {
                $headersArray[] = $header->getcommonAttributeDefinitions()->getdescription();
            }
        }

        array_walk($fileHeading, function ($row) use (&$errorMessage, $headersArray) {
            if (! in_array($row, $headersArray) && $row !== '##SCHEDULE##') {
                $errorMessage[] = 'Header ' . $row . ' is not match';
            }
        });

        if (! empty($errorMessage)) {
            return [
                'error' => true,
                'message' => $errorMessage
            ];
        }

        return $response;
    }

    public function validatePricesHeading($fileHeading, $uploadType)
    {
        $response = [
            'error' => false,
            'message' => []
        ];

        $errorMessage = [];

        if ($uploadType === 'Price') {
            $headersArray = array(
                'Option (Style + Color)',
                'TC (Trading Currency)',
                'TCP (Trading Currency Price)',
                'GBP SP',
                'GBP WAS SP',
                'USD SP',
                'USD WAS SP',
                'CAD SP',
                'CAD WAS SP',
                'AUD SP',
                'AUD WAS SP',
                'EUR SP',
                'EUR WAS SP',
                'DE EUR SP',
                'DE EU WAS SP',
                'GBP CP',
                'USD CP',
                'CAD CP',
                // LTS request to hide AUD CP, EUR CP. Please do not delete only comment out
                // 'AUD CP',
                // 'EUR CP',
                'Start Date'
            );
        } else {
            $headersArray = array(
                'Style',
                'TC (Trading Currency)',
                'TCP (Trading Currency Price)',
                'GBP SP',
                'GBP WAS SP',
                'USD SP',
                'USD WAS SP',
                'CAD SP',
                'CAD WAS SP',
                'AUD SP',
                'AUD WAS SP',
                'EUR SP',
                'EUR WAS SP',
                'DE EUR SP',
                'DE EU WAS SP',
                'GBP CP',
                'USD CP',
                'CAD CP',
                // LTS request to hide AUD CP, EUR CP. Please do not delete only comment out
                // 'AUD CP',
                // 'EUR CP',
                'Start Date'
            );
        }

        array_walk($fileHeading, function ($row) use (&$errorMessage, $headersArray) {
            if (! in_array($row, $headersArray) && $row !== '##SCHEDULE##') {
                $errorMessage[] = 'Header ' . $row . ' is not match';
            }
        });

        if (! empty($errorMessage)) {
            return [
                'error' => true,
                'message' => $errorMessage
            ];
        }

        return $response;
    }

    /**
     * Read all rows from uploaded file and add them into BULK_PROCESSING
     * @param string $processType
     * @param string $submittedBy
     * @param string $originalFileName
     * @param string $schedule
     * @param string $processKey
     * @param string $processValue
     * @return int
     */
    public function addBulkProcess($processType, $submittedBy, $originalFileName, $changedFileName, $fileHeading, $uploadData)
    {
        $numRecords = count($uploadData['rows']) - 1;
        $countHeading = count($fileHeading) - 1;
        $params = [];
        $pricesHeaderArray = array(
            'TCP (Trading Currency Price)',
            'GBP SP',
            'GBP WAS SP',
            'USD SP',
            'USD WAS SP',
            'CAD SP',
            'CAD WAS SP',
            'AUD SP',
            'AUD WAS SP',
            'EUR SP',
            'EUR WAS SP',
            'DE EUR SP',
            'DE EU WAS SP',
            'GBP CP',
            'USD CP',
            'CAD CP',
        );

        if($numRecords < 1 )
        {
            return [
                'error' => true,
                'message' => ['The file is empty']
            ];
        }
        $queryTextPart1 = "INSERT INTO `lts_prism_pms`.`BULK_PROCESSING` 
                          (`process_type`, `orginal_file_name`, `changed_file_name`, `styleOrOption`, `schedule`, 
                          `process_key`, `process_value`, `submitted_by`, `created`, `processing`, `processed`, 
                          `processed_response`) VALUES ";
        $queryTextPart2 = '';

        for ($i = 0; $i < $numRecords; $i ++) {
            for ($n = 0; $n < $countHeading; $n ++) {

                $processing = '0';
                $processed = '0';
                $processed_response = '';

                $column = $uploadData['rows'][$i + 1];
                $heading = $fileHeading[$n + 1];
                $headingValue = $this->convertHeadingValueToSpecialKeywords($column[$n + 1]);//(! empty($column[$n + 1])) ? $column[$n + 1] : '##SKIP##';
                if (in_array($heading, $pricesHeaderArray)) {
                    $headingValue = number_format($headingValue, 2);
                }
                if ($heading === 'Start Date') {
                    $headingValue = date('Y-m-d H:i:s', \PHPExcel_Shared_Date::ExcelToPHP($headingValue));
                }

                $optionOrStyle = $column[0];

                //if $optionOrStyle not set ignore the records. this been added to avoid empty rows from excel sheet
                if(!isset($optionOrStyle) || $optionOrStyle =='')
                {
                    continue;
                }
                $created = date('Y-m-d H:i:s');

                //if the file column is ##SCHEDULE## then the values in this column will used query
                if ($heading !== '##SCHEDULE##') {
                    $schedule = (! empty($fileHeading[$n + 2]) && ! empty($column[$n + 2])) ? $this->setSchedule($fileHeading[$n + 2], $column[$n + 2]) : $this->setSchedule();

                    if($schedule == '0000-00-00 00:00:00'){
                        $processing = '1';
                        $processed = '1';
                        $processed_response = 'ERROR:  Incorrect date format';
                    }

                    //$queryTextPart2 .= "('$processType','$originalFileName', '$changedFileName','$optionOrStyle','$schedule','$heading','$headingValue', '$submittedBy', '$created', '$processing', '$processed', '$processed_response')," . PHP_EOL;
                    $queryTextPart2 .= "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)," . PHP_EOL;
                    $params[] = $processType;
                    $params[] = $originalFileName;
                    $params[] = $changedFileName;
                    $params[] = strtoupper($optionOrStyle);
                    $params[] = $schedule;
                    $params[] = $heading;
                    $params[] = $headingValue;
                    $params[] = $submittedBy;
                    $params[] = $created;
                    $params[] = $processing;
                    $params[] = $processed;
                    $params[] = $processed_response;

                }
            }
            $queryTextPart2 = trim($queryTextPart2);

            if ($i == $numRecords - 1) {

                $queryTextPart3 = ';';

            }
        }
        $fullQuery = $queryTextPart1 . rtrim($queryTextPart2, ",") . $queryTextPart3;
        $numOfChangesAddToDB = $this->executeInsertQuery($fullQuery, $params);

        //return $numOfChangesAddToDB;
        return [
            'error' => false,
            'message' => [ $numOfChangesAddToDB  . ' changes was add to bulk upload processes']
        ];
    }
    /**
     * if the value empty then convert it to ##SKIP##
     * @param string $value
     * @return string
     */
    protected function convertHeadingValueToSpecialKeywords($value)
    {
        if($value == '0' )
        {
            return '0';
        }

        if($value == '' ||  $value == null)
        {
            return '##SKIP##';
        }
        return $value;
    }

    /**
     * Execute insert mysql queries
     *
     * @param string $query
     * @throws \Exception
     * @return int Affected Rows
     */
    protected function executeInsertQuery($query, $params)
    {
        try {
            $result = $this->_dbAdapter->query($query, $params);    //->execute();
            return $result->getAffectedRows();
        } catch (\Exception $e) {
            throw new \Exception($e);
        }
    }

    /**
     * Convert date from excel sheet into mysql datetime
     *
     * @param string $columnName
     * @param string $columnValue
     * @param boolean $strictFormat
     * @return string
     */
    protected function setSchedule($columnName = null, $columnValue = null, $strictFormat = true)
    {
        $default  = '0000-00-00 00:00:00';
        if ($columnName !== '##SCHEDULE##') {
            return date('Y-m-d H:i:s');
        }

        if (empty($columnValue) || $columnValue == '##NOW##') {
            return date('Y-m-d H:i:s');
        }

        //if ($strictFormat == false && is_numeric($columnValue)){
        if (is_numeric($columnValue)) {
            $UNIX_DATE = ($columnValue - 25569) * 86400;
            //return date("Y-m-d H:i:s", $UNIX_DATE);
            $columnValue = date("Y-m-d H:i:s", $UNIX_DATE);
        }

        if($strictFormat){

            $validDate = new Date(array('format' => 'Y-m-d'));
            $validDateTime = new Date(array('format' => 'Y-m-d H:i'));
            $validDateTimeSecond = new Date(array('format' => 'Y-m-d H:i:s'));

            if($validDate->isValid($columnValue) ||
                $validDateTime->isValid($columnValue) ||
                $validDateTimeSecond->isValid($columnValue)){

                return date('Y-m-d H:i:s', strtotime($columnValue));
            }
        }

        return $default;

    }

    /**
     *
     * @param int | array $bulkProcessingId
     * @param array $data
     */
    protected function updateBulkProcess($bulkProcessingId, $data)
    {
        $update = $this->_sql->update();
        $update->table($this->_blukProcessingTable);
        $update->set($data);

        if(is_array($bulkProcessingId))
        {
            $update->where->in('bulk_processing_id', $bulkProcessingId);
        }else{
            $update->where('bulk_processing_id = ' . $bulkProcessingId);
        }

        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     * Tobe run by cron to process all unprocessed records from BULK_PROCESSING
     * @throws \Exception
     */
    public function processBulkRecords()
    {
        try {
            $records = $this->getUnprocessedBulkProcess();
            if (! empty($records) && $this->setRecordsToProcessing($records) !== false) {
                $this->updateCommonAttrubuteOrPrice($records);
            }
        } catch (\Exception $e) {
            throw new \Exception($e);
        }
    }
    /**
     * send product update to sip and spectrum
     * @throws \Exception
     */
    public function notifySipAndSpectrum()
    {
        $processedRecord = [];
        try {
            $bulkIds = [];
            $records = $this->getBulkProcessesForSipAndSpectrum();

            //set records to processed so other cron will not pic the rocord
            foreach ($records as $record) {
                $bulkIds[] = $record['bulk_processing_id'];
            }

            if($bulkIds){
                $data = [
                    'sip_spectrum_notification' => '1'
                ];
                $this->updateBulkProcess($bulkIds, $data);
            }


            if (! empty($records)) {
                foreach ($records as $record) {

                    if($record['process_type'] == 'COMMON_CASCADE' || $record['process_type'] == 'COMMON_ALL')
                    {
                        $entrytype = '5';
                    }

                    if($record['process_type'] == 'PRICE_CASCADE' || $record['process_type'] == 'PRICE_ALL')
                    {
                        $entrytype = '6';
                    }

                    $recordToProcess = $record['styleOrOption'] . '_SIP_' . $entrytype;

                    //if record was already processed in this loop do not add into queue manager
                    if(in_array($recordToProcess, $processedRecord)){
                        continue;
                    }

                    $styleOrOption = $this->getTypeOfSavedProductByStyleOrOption($record['styleOrOption']);

                    // check if working with style or option
                    if($styleOrOption == 'OPTION'){
                        $productFrom = 'PRODUCTS_R_ATTRIBUTES';
                        $options = [$record['styleOrOption']];
                    }

                    if($styleOrOption == 'STYLE'){
                        $productFrom = 'PRODUCTS';
                        // Get all options
                        $options = [$record['styleOrOption']];
                    }

                    if (isset($productFrom)) {

                        if($record['process_type'] == 'COMMON_CASCADE' || $record['process_type'] == 'COMMON_ALL')
                        {
                            // to apply rules ReadyForWeb
                            $entrytype = '5';
                            $checkReadyForWeb = true;
                            $forceInserstion = false;
                        }

                        if($record['process_type'] == 'PRICE_CASCADE' || $record['process_type'] == 'PRICE_ALL')
                        {
                            $entrytype = '6';
                            $checkReadyForWeb = false;
                            $forceInserstion = true;
                        }

                        foreach ($options as $option) {
                            $this->notfySip($option, $entrytype, $checkReadyForWeb, $forceInserstion);
                        }

                        // get all style variants
                        $styleOrOptionIds = $this->getStyleOrOptionIds($record['styleOrOption'], 'PRODUCTS_R_ATTRIBUTES');
                        $this->notfySpectrum($styleOrOptionIds);

                        $processedRecord[] = $recordToProcess;
                    }

                }
            }
        } catch (\Exception $e) {
            throw new \Exception($e);
        }
    }

    /**
     * update data in sip
     * @param string $styleOrOption
     * @param int $entryType
     * @return boolean
     */
    protected function notfySip($styleOrOption, $entryType, $checkIsReadyForWeb = false, $forceInsertion = false)
    {
        return $this->_queueService->addSipEntryToQueue($styleOrOption, $entryType, $this->productsMapper, $checkIsReadyForWeb, $forceInsertion);
    }

    /**
     * update data in spectrum
     * @param array $ids
     */
    protected function notfySpectrum($ids)
    {
        for ($i = 0; $i < count($ids); $i++) {
            $this->_queueService->addSpectrumEntryToQueueForCommonAttributes($ids[$i]);
        }

    }
    /**
     * get all successful update processes and sent them to sip and spectrum
     * @return unknown[]
     */
    public function getBulkProcessesForSipAndSpectrum()
    {
        $processes = [];

        try {
            $sql = new Sql($this->_dbAdapter);

            $select = $sql->select();
            $select->from($this->_blukProcessingTable);
            $select->where->equalTo('sip_spectrum_notification', 0);
            $select->where->equalTo('processed', 1);
            $select->where->equalTo('processed_response', 'SUCCESS');
            // we should add limit in case the script slow
            //$select->group('styleOrOption');

            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();

            foreach ($result as $row) {
                $processes[] = $row;
            }
        } catch (\Exception $e) {
            return [];
        }

        return $processes;
    }

    /**
     * Based on process_type select the correct strategy to update common attributes
     * @param array $records
     */
    protected function updateCommonAttrubuteOrPrice($records)
    {
        foreach ($records as $record) {
            if ($record['process_type'] == 'COMMON_CASCADE') {
                $this->updateCommonAttributeCascade($record);
            }

            if ($record['process_type'] == 'COMMON_ALL') {
                $this->updateCommonAttributeAll($record);
            }

            if ($record['process_type'] == 'PRICE_CASCADE') {
                $this->updatePriceCascade($record);
            }

            if ($record['process_type'] == 'PRICE_ALL') {
                $this->updatePriceAll($record);
            }
        }
    }

    /**
     * Check if the updated common attributes are updated on style or option level
     * @param unknown $styleOrOption
     * @return string|NULL|string
     */
    protected function getTypeOfSavedProductByStyleOrOption($styleOrOption)
    {
        $type = null;

        // Check if $styleOrOption is style
        if ($this->isStyle($styleOrOption)) {
            $type = "STYLE";
            return $type;
        }

        // Check if $styleOrOption is option
        if ($this->isOption($styleOrOption)) {
            $type = "OPTION";
            return $type;
        }

        return $type;
    }
    /**
     * Check if the updated common attributes are updated on style level
     * @param string $styleOrOption
     * @return boolean
     */
    protected function isStyle($styleOrOption)
    {
        try {
            $sql = new Sql($this->_dbAdapter);

            $select = $sql->select();
            $select->from('PRODUCTS');
            $select->where->equalTo('Style', $styleOrOption);

            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();

            $affectedRows = $result->getAffectedRows();

            if ($affectedRows == 1) {
                return true;
            }
        } catch (\Exception $e) {
            return false;
        }
        return false;
    }
    /**
     * Check if the updated common attributes are updated on option level
     * @param string $styleOrOption
     * @return boolean
     */
    protected function isOption($styleOrOption)
    {
        try {
            $sql = new Sql($this->_dbAdapter);

            $select = $sql->select();
            $select->from('PRODUCTS_R_ATTRIBUTES');
            $select->where->like('SKU', $styleOrOption . "%");

            // echo $sql->getSqlstringForSqlObject($select); die ;
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();

            $affectedRows = $result->getAffectedRows();

            if ($affectedRows > 0) {
                return true;
            }
        } catch (\Exception $e) {
            return false;
        }
        return false;
    }

    /**
     * get the table name where product id is stored
     *
     * @param string $isValidStyleOrVariant
     * @return string
     */
    protected function setProductFrom($isValidStyleOrVariant)
    {
        if ($isValidStyleOrVariant == 'STYLE') {
            return "PRODUCTS";
        }

        if ($isValidStyleOrVariant == 'OPTION') {
            return "PRODUCTS_R_ATTRIBUTES";
        }
    }

    /**
     * Update the common attribute from file type COMMON_ALL
     * @param array $record
     */
    protected function updateCommonAttributeAll($record)
    {
        $this->setBulkProcessingId($record['bulk_processing_id']);

        // Check if styleOrVariant value type and exist
        $isValidStyleOrVariant = $this->getTypeOfSavedProductByStyleOrOption($record['styleOrOption']);

        if ($isValidStyleOrVariant === null) {
            // update processed_response with error message product could not be found
            $data = [
                'processed_response' => 'ERROR:  Style or Option not found',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
            return;
        }

        $productIdFrom = $this->setProductFrom($isValidStyleOrVariant);

        // check if update value is not a special keyword
        if (! $this->isRecordWithSpecialKeyword($record['process_value'])) {

            // add code to edit 'Style Name' and 'Style Short Name' these are not standard CA
            if ($record['process_key'] == 'Style Name' || $record['process_key'] == 'Style Short Name') {
                $this->updateStyleNameAndStyleShortName($record['styleOrOption'], $productIdFrom, $record['process_value'], $record['process_key']);
                return;
            }

            $attributeType = $this->getAttributeType($record['process_key']);
            if ($attributeType) {
                $commonAttributeId = $this->getCommonAttributeId($record['process_key']);
                $styleOrOptionIds = $this->getstyleOrOptionIds($record['styleOrOption'], $productIdFrom);
                $saveOrUpdateCommonAttribute = $this->saveOrUpdateCommonAttribute(
                    $attributeType,
                    $record['styleOrOption'],
                    $commonAttributeId,
                    $record['process_value'],
                    $productIdFrom,
                    $styleOrOptionIds,
                    $record['submitted_by']
                );
            } else {
                // update processed_response common attribute was not found
                $data = [
                    'processed_response' => 'ERROR: The common Attribute type not found',
                    'processed' => '1'
                ];
                $this->updateBulkProcess($this->bulkProcessingId, $data);
            }
        } else {
            $this->updateCommonAttributeWithSepcialKeyword($record['process_value'], $record);
        }
    }

    /**
     * Update the common attribute from file type COMMON_CASCADE
     * @param array $record
     */
    protected function updateCommonAttributeCascade($record)
    {
        $this->setBulkProcessingId($record['bulk_processing_id']);

        if($this->isStyle($record['styleOrOption']) === true){
            if (! $this->isRecordWithSpecialKeyword($record['process_value'])) {

                $styleIdFrom = 'PRODUCTS';
                $styleId = $this->getStyleOrOptionIds($record['styleOrOption'], $styleIdFrom);

                // get all style variants
                $variantIdFrom = 'PRODUCTS_R_ATTRIBUTES';
                $optionIds = $this->getStyleOrOptionIds($record['styleOrOption'], $variantIdFrom);

                // add code to edit 'Style Name' and 'Style Short Name' these are not standard CA
                if ($record['process_key'] == 'Style Name' || $record['process_key'] == 'Style Short Name') {
                    // update style
                    $this->updateStyleNameAndStyleShortName($record['styleOrOption'], $styleIdFrom, $record['process_value'], $record['process_key'], $styleId);
                    // update all variants (options)
                    $this->updateStyleNameAndStyleShortName($record['styleOrOption'], $variantIdFrom, $record['process_value'], $record['process_key'], $optionIds);
                    return;
                }

                $attributeType = $this->getAttributeType($record['process_key']);
                if ($attributeType) {
                    $commonAttributeId = $this->getCommonAttributeId($record['process_key']);
                    //update style

                    $saveOrUpdateCommonAttribute = $this->saveOrUpdateCommonAttribute(
                        $attributeType,
                        $record['styleOrOption'],
                        $commonAttributeId,
                        $record['process_value'],
                        $styleIdFrom,
                        $styleId,
                        $record['submitted_by']
                    );
                    // update all variants (options)
                    $saveOrUpdateCommonAttribute = $this->saveOrUpdateCommonAttribute(
                        $attributeType,
                        $record['styleOrOption'],
                        $commonAttributeId,
                        $record['process_value'],
                        $variantIdFrom,
                        $optionIds,
                        $record['submitted_by']
                        );
                } else {
                    // update processed_response common attribute was not found
                    $data = [
                        'processed_response' => 'ERROR: The common Attribute type not found',
                        'processed' => '1'
                    ];
                    $this->updateBulkProcess($this->bulkProcessingId, $data);
                }

            }else{
                $this->updateCommonAttributeWithSepcialKeyword($record['process_value'], $record);
            }
        }else{
            $data = [
                'processed_response' => 'ERROR: Style not found',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
        }
    }

    public function  updatePriceCascade($record)
    {
        $this->setBulkProcessingId($record['bulk_processing_id']);

        // Check if styleOrVariant value type and exists
        $isValidStyle = $this->isStyle($record['styleOrOption']);

        if ($isValidStyle === false) {
            // update processed_response with error message product could not be found
            $data = [
                'processed_response' => 'ERROR:  Not a valid Style',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
            return;
        }

        //validate price is numeric value
        if($record['process_key'] != 'TC (Trading Currency)' && !is_numeric($record['process_value']) || $record['process_value'] < 0){
            $data = [
                'processed_response' => 'ERROR:  The price value must be a positive number',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
            return;
        }

        $styleIdFrom = 'PRODUCTS';
        $styleId = $this->getStyleOrOptionIds($record['styleOrOption'], $styleIdFrom);

        // get all style variants
        $variantIdFrom = 'PRODUCTS_R_ATTRIBUTES';
        $optionIds = $this->getStyleOrOptionIds($record['styleOrOption'], $variantIdFrom);

        // check if update value is not a special keyword
        if (! $this->isRecordWithSpecialKeyword($record['process_value'])) {

            if ($record['process_key'] == 'TC (Trading Currency)' || $record['process_key'] == 'TCP (Trading Currency Price)')
            {
                // update trading currency and price for style
                $this->updateTradingCurrencyPrice($record['process_key'], $record['process_value'], $styleIdFrom, $styleId, $record['submitted_by']);
                // update trading currency and price for option
                $this->updateTradingCurrencyPrice($record['process_key'], $record['process_value'], $variantIdFrom, $optionIds, $record['submitted_by']);
                return;
            }

            $territoryCurrencyValue = $this->mapingBulkPriceHeading($record['process_key']);

            $updateStylePrice = $this->productsMapper->bulkUpdatePrice($record['process_value'], $styleIdFrom, $styleId, $territoryCurrencyValue, $record['submitted_by']);
            $updateOptionPrice = $this->productsMapper->bulkUpdatePrice($record['process_value'], $variantIdFrom, $optionIds, $territoryCurrencyValue, $record['submitted_by']);

            if($updateStylePrice == true && $updateOptionPrice == true){
                $data = [
                    'processed_response' => 'SUCCESS',
                    'processed' => '1'
                ];
            }else{
                $data = [
                    'processed_response' => 'ERROR: the price could not be updated please check territory and currency settings',
                    'processed' => '1'
                ];
            }

            $this->updateBulkProcess($this->bulkProcessingId, $data);

        } else {
            $this->updateCommonAttributeWithSepcialKeyword($record['process_value'], $record);
        }
    }
    /**
     * Update Prices on option as style level
     * @param array $record
     */
    public function updatePriceAll($record)
    {
        $this->setBulkProcessingId($record['bulk_processing_id']);

        // Check if styleOrVariant value type and exists
        $isValidStyleOrVariant = $this->getTypeOfSavedProductByStyleOrOption($record['styleOrOption']);
        if ($isValidStyleOrVariant === null) {
            // update processed_response with error message product could not be found
            $data = [
                'processed_response' => 'ERROR:  Style or Option not found',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
            return;
        }

        //validate price is numeric value
        if($record['process_key'] != 'TC (Trading Currency)' && !is_numeric($record['process_value']) || $record['process_value'] < 0){
            $data = [
                'processed_response' => 'ERROR:  The price value must be a positive number',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
            return;
        }

        $productIdFrom = $this->setProductFrom($isValidStyleOrVariant);
        $styleOrOptionIds = $this->getStyleOrOptionIds($record['styleOrOption'], $productIdFrom);

        // check if update value is not a special keyword
        if (! $this->isRecordWithSpecialKeyword($record['process_value'])) {
            // update trading currency and price
            if ($record['process_key'] == 'TC (Trading Currency)' || $record['process_key'] == 'TCP (Trading Currency Price)')
            {
                $this->updateTradingCurrencyPrice($record['process_key'], $record['process_value'], $productIdFrom, $styleOrOptionIds, $record['submitted_by']);
                return;
            }
            $territoryCurrencyValue = $this->mapingBulkPriceHeading($record['process_key']);
            $updatePrice = $this->productsMapper->bulkUpdatePrice($record['process_value'], $productIdFrom, $styleOrOptionIds, $territoryCurrencyValue, $record['submitted_by']);
            if($updatePrice == true){
                $data = [
                    'processed_response' => 'SUCCESS',
                    'processed' => '1'
                ];
            }else{
                $data = [
                    'processed_response' => 'ERROR: the price could not be updated please check territory and currency settings',
                    'processed' => '1'
                ];
            }

            $this->updateBulkProcess($this->bulkProcessingId, $data);

        } else {
            $this->updateCommonAttributeWithSepcialKeyword($record['process_value'], $record);
        }
    }

    protected function updateTradingCurrencyPrice(
        $processKey,
        $processValue,
        $productIdFrom,
        $styleOrOptionIds,
        $subbmitedUserId = null)
    {
        if ($processKey == 'TC (Trading Currency)')
        {
            $validCurrencies = $this->productsMapper->getActiveCurrencies();

            if(false == in_array($processValue, $validCurrencies)){

                $data = [
                    'processed_response' => 'ERROR:  Invalid Trading Currency.',
                    'processed' => '1'
                ];

                $this->updateBulkProcess($this->bulkProcessingId, $data);
                return;
            }

            $data = [
                'currencyCode' => $processValue
            ];
        }

        if ($processKey == 'TCP (Trading Currency Price)')
        {
            if(!is_numeric($processValue) || $processValue < 0){

                $data = [
                    'processed_response' => 'ERROR:  The price value must be a positive number',
                    'processed' => '1'
                ];

                $this->updateBulkProcess($this->bulkProcessingId, $data);
                return;
            }

            $data = [
                'price' => $processValue
            ];
        }

        $result = $this->productsMapper->blukUpdateProductTradingCurrencyOrPrice($data, $styleOrOptionIds, $productIdFrom, $subbmitedUserId);
        if($result)
        {
            $data = [
                'processed_response' => 'SUCCESS',
                'processed' => '1'
            ];
        }else{
            $data = [
                'processed_response' => 'ERROR:  Could not update ' . $processKey . '. Please check if record exist for: ' . $styleOrOptionIds . ' from ' . $productIdFrom,
                'processed' => '1'
            ];
        }
        $this->updateBulkProcess($this->bulkProcessingId, $data);
        return;

    }

    protected function updateStyleNameAndStyleShortName($styleOrOption, $productIdFrom, $newValue, $commonAttributeKey, $styleOrOptionIds = null)
    {
        try {

            if($styleOrOptionIds == null){
                $styleOrOptionIds = $this->getStyleOrOptionIds($styleOrOption, $productIdFrom);
            }

            for ($i = 0; $i < count($styleOrOptionIds); $i++) {
                if ($commonAttributeKey == 'Style Name') {
                    $isupdated = $this->productsMapper->updateProductNameByProductId($styleOrOptionIds[$i], $newValue, $productIdFrom);
                }

                if ($commonAttributeKey == 'Style Short Name') {
                    $isupdated = $this->productsMapper->updateShortProductNameByProductId($styleOrOptionIds[$i], $newValue, $productIdFrom);
                }
            }
            $data = [
                'processed_response' => 'SUCCESS',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);

        } catch (\Exception $e) {
            $data = [
                'processed_response' => 'ERROR Could not process multi-select',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
        }
    }

    protected function setBulkProcessingId($bulkProcessingId)
    {
        $this->bulkProcessingId = $bulkProcessingId;
    }

    /**
     * Select the strategy to update common attribute based on $attributeType
     * @param string $attributeType
     * @param string $styleOrOption
     * @param int $commonAttributeId
     * @param string $processValue
     * @param string $productIdFrom
     * @param array $styleOrOptionIds
     * @param int|null $submittedByUser
     */
    public function saveOrUpdateCommonAttribute(
        $attributeType,
        $styleOrOption,
        $commonAttributeId,
        $processValue,
        $productIdFrom,
        $styleOrOptionIds,
        $submittedByUser = null
    )
    {
        if ($attributeType == 'CHECKBOX') {
            $result = $this->processCommonAttributeTypeCheckbox(
                $commonAttributeId,
                $styleOrOption,
                $processValue,
                $productIdFrom,
                $styleOrOptionIds,
                $submittedByUser
            );
        }

        if ($attributeType == 'SELECT') {
            // if the value empty string then delete record
            if($processValue === ''){
                $result = $this->deleteCommonAttributeTypeSelect(
                    $commonAttributeId,
                    $styleOrOption,
                    $processValue,
                    $productIdFrom,
                    $styleOrOptionIds,
                    $submittedByUser
                );
            }else{
                $result = $this->processCommonAttributeTypeSelect(
                    $commonAttributeId,
                    $styleOrOption,
                    $processValue,
                    $productIdFrom,
                    $styleOrOptionIds,
                    $submittedByUser
                );
            }
        }

        if ($attributeType == 'MULTISELECT') {
            $result = $this->processCommonAttributeTypeMultiselect(
                $commonAttributeId,
                $styleOrOption,
                $processValue,
                $productIdFrom,
                $styleOrOptionIds,
                $submittedByUser
            );
        }

        if ($attributeType == 'TEXT') {
            $result = $this->processCommonAttributeTypeText(
                $commonAttributeId,
                $styleOrOption,
                $processValue,
                $productIdFrom,
                $styleOrOptionIds,
                $submittedByUser
            );
        }

        if ($attributeType == 'DATEPICKER') {
            $result = $this->processCommonAttributeTypeDatepickerDatetime(
                $commonAttributeId,
                $styleOrOption,
                $processValue,
                $productIdFrom,
                $styleOrOptionIds,
                $submittedByUser
            );
        }

        if ($attributeType == 'TEXTAREA') {
            $result = $this->processCommonAttributeTypeTextare(
                $commonAttributeId,
                $styleOrOption,
                $processValue,
                $productIdFrom,
                $styleOrOptionIds,
                $submittedByUser);
        }

        if ($attributeType == 'DATEPICKER - DATE ONLY') {
            $result = $this->processCommonAttributeTypeDatepickerDateOnly(
                $commonAttributeId,
                $styleOrOption,
                $processValue,
                $productIdFrom,
                $styleOrOptionIds,
                $submittedByUser);
        }
    }

    /**
     * Get common attribute Id by common attribute value
     *
     * @param sting $processKey
     * @return int
     */
    protected function getCommonAttributeId($processKey)
    {
        $commonAttributeId = $this->commonAttributesService->getCommonAttributeIdByProcessKey($processKey);
        return $commonAttributeId;
    }

    /**
     * Get the common attribute type by common attribute value
     *
     * @param string $processKey
     * @return string
     */
    protected function getAttributeType($processKey)
    {
        $attributeType = $this->commonAttributesService->getAttributeTypeByProcessKey($processKey);
        return $attributeType;
    }
    /**
     * Check if the value of common attribute is one of the special keywords
     * @param string $processedValue
     * @return boolean
     */
    protected function isRecordWithSpecialKeyword($processedValue)
    {
        $isSpecial = false;
        if ($processedValue === "##SKIP##" || // skip processing this value
            $processedValue === "##SCHEDULE##" || // is record is set for schedule
            $processedValue === "##NOW##" || // set current time
            $processedValue === "##NULL##" || // set value to null
            $processedValue === "##EMPTY##" || // set value empty string
            $processedValue === "##CLEAR##" || // set value empty string
            $processedValue === "##DELETE##") // to delete full record

        {
            $isSpecial = true;
        }

        return $isSpecial;
    }
    /**
     * Process the common attribute based on function of the special keyword
     * @param unknown $processedValue
     * @param unknown $record
     */
    protected function updateCommonAttributeWithSepcialKeyword($processedValue, $record)
    {
        // ##SKIP## do not do any action
        if ($processedValue == "##SKIP##") {
            $data = [
                'processed_response' => 'SUCCESS',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
            return;
        }

        if ($processedValue == "##CLEAR##" || $processedValue == "##EMPTY##") {
            //Set the value to empty '' and re-run the method
            $record['process_value'] = '';

            if ($record['process_type'] == 'COMMON_CASCADE') {
                $this->updateCommonAttributeCascade($record);
            }

            if ($record['process_type'] == 'COMMON_ALL') {
                $this->updateCommonAttributeAll($record);
            }

        }
        return;
    }

    /**
     * Get all records waiting to be processed
     *
     * @return $processes[]
     */
    public function getUnprocessedBulkProcess()
    {
        $processes = [];

        try {
            $sql = new Sql($this->_dbAdapter);

            $select = $sql->select();
            $select->from($this->_blukProcessingTable);
            $select->where->equalTo('processing', 0);
            $select->where->lessThanOrEqualTo('schedule', date("Y-m-d H:i:s"));

            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();

            foreach ($result as $row) {
                $processes[] = $row;
            }
        } catch (\Exception $e) {
            return $processes;
        }

        return $processes;
    }

    /**
     *
     * @param unknown $data
     * @throws \Exception
     * @return boolean
     */
    public function setRecordsToProcessing($data)
    {
        if (empty($data)) {
            return false;
        }

        foreach ($data as $record) {
            $ids[] = $record['bulk_processing_id'];
        }

        try {
            $sql = new Sql($this->_dbAdapter);
            $update = $sql->update();
            $update->table($this->_blukProcessingTable);
            $update->set([
                'processing' => 1
            ]);
            $update->where->in('bulk_processing_id', $ids);

            // echo $sql->getSqlstringForSqlObject($update); die ;
            $statement = $sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Update style/option common attribute type CHECKBOX
     * @param int $commonAttributeId
     * @param string $styleOrOption
     * @param string $processValue
     * @param string $productIdFrom
     * @param array $styleOrOptionIds
     * @param null | int $submittedByUser
     */
    public function processCommonAttributeTypeCheckbox(
        $commonAttributeId,
        $styleOrOption,
        $processValue,
        $productIdFrom,
        $styleOrOptionIds,
        $submittedByUser = null
    )
    {
        if ($processValue == '0' || $processValue == '1') {

            $historicalEntries = $this->productsMapper->createHistoricalEntriesForCommonAttribute(
                $commonAttributeId,
                [$processValue],
                $productIdFrom,
                $styleOrOptionIds,
                $submittedByUser
            );

            /**
             * Save the value against the product
             */
            $this->productsMapper->blukDeleteProductCommonAttribute($commonAttributeId, $styleOrOptionIds, $productIdFrom);
            $this->productsMapper->blukSaveCommonAttributeAgainstProduct($processValue, $commonAttributeId, $styleOrOptionIds, $productIdFrom);

            $this->productsMapper->recordHistoricalChanges($historicalEntries);
            $data = [
                'processed_response' => 'SUCCESS',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);

        } else {
            $data = [
                'processed_response' => 'ERROR: CHECKBOX Value MUST be 0 or 1.',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
        }

    }

    /**
     * Update style/option common attribute type SELECT
     * @param int $commonAttributeId
     * @param string $styleOrOption
     * @param string $processValue
     * @param string $productIdFrom
     * @param array $styleOrOptionIds
     * @param null|int $submittedByUser
     */
    public function processCommonAttributeTypeSelect(
        $commonAttributeId,
        $styleOrOption,
        $processValue,
        $productIdFrom,
        $styleOrOptionIds,
        $submittedByUser = null)
    {
        $attributeValueEntity = $this->commonAttributesService->getCommonAttributeValueByName($processValue, $commonAttributeId);
        if (empty($attributeValueEntity)) {
            /**
             * Save as error - value was not found at the attribute values
             */
            $data = [
                'processed_response' => 'ERROR: Value was not found',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);

        } else {
            $attributeValueId = $attributeValueEntity->getCommonAttributeValueId();
            /**
             * Save the value against the product
             */
            $historicalEntries = $this->productsMapper->createHistoricalEntriesForCommonAttribute(
                $commonAttributeId,
                [$attributeValueId],
                $productIdFrom,
                $styleOrOptionIds,
                $submittedByUser
            );

            $this->productsMapper->blukDeleteProductCommonAttribute($commonAttributeId, $styleOrOptionIds, $productIdFrom);
            $this->productsMapper->blukSaveCommonAttributeAgainstProduct($attributeValueId, $commonAttributeId, $styleOrOptionIds, $productIdFrom);

            $this->productsMapper->recordHistoricalChanges($historicalEntries);

            $data = [
                'processed_response' => 'SUCCESS',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
        }
    }

    /**
     * Delete common attribute record
     * @param $commonAttributeId
     * @param $productIdFrom
     * @param $styleOrOptionIds
     * @param $submittedByUser
     */
    public function deleteCommonAttributeTypeSelect(
        $commonAttributeId,
        $productIdFrom,
        $styleOrOptionIds,
        $submittedByUser
    )
    {
        try {
            $attributeValueEntity = $this->commonAttributesService->getCommonAttributeValueByName('', $commonAttributeId);
            $attributeValueId = $attributeValueEntity->getCommonAttributeValueId();
            /**
             * Save the value against the product
             */
            $historicalEntries = $this->productsMapper->createHistoricalEntriesForCommonAttribute(
                $commonAttributeId,
                [$attributeValueId],
                $productIdFrom,
                $styleOrOptionIds,
                $submittedByUser
            );

            $this->productsMapper->blukDeleteProductCommonAttribute($commonAttributeId, $styleOrOptionIds, $productIdFrom);
            $this->productsMapper->blukSaveCommonAttributeAgainstProduct($attributeValueId, $commonAttributeId, $styleOrOptionIds, $productIdFrom);

            $this->productsMapper->recordHistoricalChanges($historicalEntries);

            $data = [
                'processed_response' => 'SUCCESS',
                'processed' => '1'
            ];

        } catch (\Exception $e) {
            $data = [
                'processed_response' => 'ERROR: Failed to delete common attribute value',
                'processed' => '1'
            ];
        }

        $this->updateBulkProcess($this->bulkProcessingId, $data);
    }


    /**
     * Update style/option common attribute type MULTISELECT
     * @param int $commonAttributeId
     * @param string $styleOrOption
     * @param string $processValue
     * @param string $productIdFrom
     * @param array $styleOrOptionIds
     * @param null|int $submittedByUser
     */
    public function processCommonAttributeTypeMultiselect(
        $commonAttributeId,
        $styleOrOption,
        $processValue,
        $productIdFrom,
        $styleOrOptionIds,
        $submittedByUser = null)
    {
        try {
            $this->productsMapper->blukDeleteProductCommonAttribute($commonAttributeId, $styleOrOptionIds, $productIdFrom);

            $attributeValues = explode('|', $processValue);
            foreach ($attributeValues as $attributeValue)
            {
                $attributeValueEntity = $this->commonAttributesService->getCommonAttributeValueByName($attributeValue, $commonAttributeId);

                if($attributeValueEntity != false){
                    $attributeValueId = $attributeValueEntity->getCommonAttributeValueId();

                    $historicalEntries = $this->productsMapper->createHistoricalEntriesForCommonAttribute(
                        $commonAttributeId,
                        [$attributeValueId],
                        $productIdFrom,
                        $styleOrOptionIds,
                        $submittedByUser
                    );

                    $this->productsMapper->blukSaveCommonAttributeAgainstProduct($attributeValueId, $commonAttributeId, $styleOrOptionIds, $productIdFrom);

                    $this->productsMapper->recordHistoricalChanges($historicalEntries);

                    $data = [
                        'processed_response' => 'SUCCESS',
                        'processed' => '1'
                    ];
                }else{
                    $data = [
                        'processed_response' => 'ERROR: Failed to find the value: ' . $attributeValue ,
                        'processed' => '1'
                    ];
                }
            }

            $data = [
                'processed_response' => 'SUCCESS',
                'processed' => '1'
            ];
        } catch (\Exception $e) {
            $data = [
                'processed_response' => 'ERROR: Failed to update multi-select',
                'processed' => '1'
            ];
        }
        $this->updateBulkProcess($this->bulkProcessingId, $data);
    }
    /**
     * Update style/option common attribute type TEXT
     * @param int $commonAttributeId
     * @param string $styleOrOption
     * @param string $processValue
     * @param string $productIdFrom
     * @param null|int $submittedByUser
     * @param array $styleOrOptionIds
     */
    public function processCommonAttributeTypeText(
        $commonAttributeId,
        $styleOrOption,
        $processValue,
        $productIdFrom,
        $styleOrOptionIds,
        $submittedByUser = null)
    {
        try {
            $historicalEntries = $this->productsMapper->createHistoricalEntriesForCommonAttribute(
                $commonAttributeId,
                [$processValue],
                $productIdFrom,
                $styleOrOptionIds,
                $submittedByUser
            );

            $this->productsMapper->blukDeleteProductCommonAttribute($commonAttributeId, $styleOrOptionIds, $productIdFrom);
            $this->productsMapper->blukSaveCommonAttributeAgainstProduct($processValue, $commonAttributeId, $styleOrOptionIds, $productIdFrom);

            $this->productsMapper->recordHistoricalChanges($historicalEntries);

            $data = [
                'processed_response' => 'SUCCESS',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);

        } catch (\Exception $e) {
            $data = [
                'processed_response' => 'ERROR: Could not save the value into DB please contact Prism.',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
        }

    }
    /**
     * Update style/option common attribute type DATEPICKER
     * @param int $commonAttributeId
     * @param string $styleOrOption
     * @param string $processValue
     * @param string $productIdFrom
     * @param array $styleOrOptionIds
     * @param null|int $submittedByUser
     */
    public function processCommonAttributeTypeDatepickerDatetime(
        $commonAttributeId,
        $styleOrOption,
        $processValue,
        $productIdFrom,
        $styleOrOptionIds,
        $submittedByUser = null)
    {
        try {
            if (trim($processValue) > 10) {
                $processValue = new \DateTime($processValue, new \DateTimeZone("Europe/London"));
            } else {
                $processValue = new \DateTime($processValue . ' 00:00:00', new \DateTimeZone("Europe/London"));
            }
        } catch (\Exception $e) {
            $data = [
                'processed_response' => 'ERROR: Could not format date time.',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
            return;
        }

        try {
            $historicalEntries = $this->productsMapper->createHistoricalEntriesForCommonAttribute(
                $commonAttributeId,
                [$processValue],
                $productIdFrom,
                $styleOrOptionIds,
                $submittedByUser
            );

            $this->productsMapper->blukDeleteProductCommonAttribute($commonAttributeId, $styleOrOptionIds, $productIdFrom);
            $this->productsMapper->blukSaveCommonAttributeAgainstProduct($processValue, $commonAttributeId, $styleOrOptionIds, $productIdFrom);

            $this->productsMapper->recordHistoricalChanges($historicalEntries);

            $data = [
                'processed_response' => 'SUCCESS',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);

        } catch (\Exception $e) {
            $data = [
                'processed_response' => 'ERROR: Could not save the value into DB please contact Prism.',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
        }

    }
    /**
     * Update style/option common attribute type TEXTARE
     * @param int $commonAttributeId
     * @param string $styleOrOption
     * @param string $processValue
     * @param string $productIdFrom
     * @param array $styleOrOptionIds
     * @param null|int $submittedByUser
     */
    public function processCommonAttributeTypeTextare(
        $commonAttributeId,
        $styleOrOption,
        $processValue,
        $productIdFrom,
        $styleOrOptionIds,
        $submittedByUser = null)
    {
        try {
            $historicalEntries = $this->productsMapper->createHistoricalEntriesForCommonAttribute(
                $commonAttributeId,
                [$processValue],
                $productIdFrom,
                $styleOrOptionIds,
                $submittedByUser
            );

            $this->productsMapper->blukDeleteProductCommonAttribute($commonAttributeId, $styleOrOptionIds, $productIdFrom);
            $this->productsMapper->blukSaveCommonAttributeAgainstProduct($processValue, $commonAttributeId, $styleOrOptionIds, $productIdFrom);
            $this->productsMapper->recordHistoricalChanges($historicalEntries);
            $data = [
                'processed_response' => 'SUCCESS',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);

        } catch (\Exception $e) {
            $data = [
                'processed_response' => 'ERROR: Could not save the value into DB please contact Prism.',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
        }
    }
    /**
     * Update style/option common attribute type DATEPICKER - DATE ONLY
     * @param int $commonAttributeId
     * @param string $styleOrOption
     * @param string $processValue
     * @param string $productIdFrom
     * @param array $styleOrOptionIds
     * @param null | int $submittedByUser
     */
    public function processCommonAttributeTypeDatepickerDateOnly(
        $commonAttributeId,
        $styleOrOption,
        $processValue,
        $productIdFrom,
        $styleOrOptionIds,
        $submittedByUser = null)
    {
        try {
            if (trim($processValue) > 10) {
                $processValue = new \DateTime($processValue, new \DateTimeZone("Europe/London"));
            } else {
                $processValue = new \DateTime($processValue . ' 00:00:00', new \DateTimeZone("Europe/London"));
            }
        } catch (\Exception $e) {
            $data = [
                'processed_response' => 'ERROR: Could not format date',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
            return;
        }

        try {
            $historicalEntries = $this->productsMapper->createHistoricalEntriesForCommonAttribute(
                $commonAttributeId,
                [$processValue],
                $productIdFrom,
                $styleOrOptionIds,
                $submittedByUser
            );

            $this->productsMapper->blukDeleteProductCommonAttribute($commonAttributeId, $styleOrOptionIds, $productIdFrom);
            $this->productsMapper->blukSaveCommonAttributeAgainstProduct($processValue, $commonAttributeId, $styleOrOptionIds, $productIdFrom);

            $this->productsMapper->recordHistoricalChanges($historicalEntries);

            $data = [
                'processed_response' => 'SUCCESS',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);

        } catch (\Exception $e) {
            $data = [
                'processed_response' => 'ERROR: Could not save the value into DB please contact Prism.',
                'processed' => '1'
            ];
            $this->updateBulkProcess($this->bulkProcessingId, $data);
        }

    }

    /**
     * get all ids of style / option (variants of the option) to be updated
     * @param string $styleOrOption
     * @param string $productIdFrom
     * @return array|boolean
     */
    public function getStyleOrOptionIds($styleOrOption, $productIdFrom)
    {
        try {

            if ($productIdFrom == 'PRODUCTS_R_ATTRIBUTES') {
                $column = [
                    'ids' => 'productAttributeId'
                ];
            } else {
                $column = [
                    'ids' => 'productId'
                ];
            }

            $sql = new Sql($this->_dbAdapter);

            $select = $sql->select();
            $select->columns($column);
            $select->from($productIdFrom);

            if ($productIdFrom == 'PRODUCTS_R_ATTRIBUTES') {
                $select->where->like('SKU', $styleOrOption . "%");
            } else {
                $select->where->equalTo('style', $styleOrOption);
            }

            // echo $sql->getSqlstringForSqlObject($select); //die ;
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();

            $affectedRows = $result->getAffectedRows();

            if ($result->count() > 0) {
                foreach ($result as $row) {
                    $ids[] = $row['ids'];
                }
            }

            return $ids;
        } catch (\Exception $e) {
            return false;
        }
        return false;
    }

    protected function mapingBulkPriceHeading($key)
    {
        $map = $this->priceColumnMapper();
        return $map[$key];
    }

    protected function priceColumnMapper()
    {
        return array(
            'GBP SP' => array(
                array(
                    'currency' => 'GBP',
                    'territory' => 'GB',
                    'column' => 'nowPrice'
                ),
            ),
            'GBP WAS SP' => array(
                array(
                    'currency' => 'GBP',
                    'territory' => 'GB',
                    'column' => 'wasPrice'
                ),
            ),
            'USD SP' => array(
                array(
                    'currency' => 'USD',
                    'territory' => 'US',
                    'column' => 'nowPrice'
                ),
            ),
            'USD WAS SP' => array(
                array(
                    'currency' => 'USD',
                    'territory' => 'US',
                    'column' => 'wasPrice'
                ),
            ),
            'CAD SP' => array(
                array(
                    'currency' => 'CAD',
                    'territory' => 'CA',
                    'column' => 'nowPrice'
                ),
            ),
            'CAD WAS SP' => array(
                array(
                    'currency' => 'CAD',
                    'territory' => 'CA',
                    'column' => 'wasPrice'
                ),
            ),
            'AUD SP' => array(
                array(
                    'currency' => 'AUD',
                    'territory' => 'GB',
                    'column' => 'nowPrice'
                ),
            ),
            'AUD WAS SP' => array(
                array(
                    'currency' => 'AUD',
                    'territory' => 'GB',
                    'column' => 'wasPrice'),
            ),
            'EUR SP' => array(
                array(
                    'currency' => 'EUR',
                    'territory' => 'EU',
                    'column' => 'nowPrice'),
            ),
            'EUR WAS SP' => array(
                array(
                    'currency' => 'EUR',
                    'territory' => 'EU',
                    'column' => 'wasPrice'
                ),
            ),
            'DE EUR SP' => array(
                array(
                    'currency' => 'EUR',
                    'territory' => 'DE',
                    'column' => 'nowPrice'
                ),
            ),
            'DE EU WAS SP' => array(
                array(
                    'currency' => 'EUR',
                    'territory' => 'DE',
                    'column' => 'wasPrice'
                ),
            ),
            'GBP CP' => array(
                array(
                    'currency' => 'GBP',
                    'territory' => 'GB',
                    'column' => 'costPrice'
                ),
                array(
                    'currency' => 'GBP',
                    'territory' => 'ROW',
                    'column' => 'costPrice'
                ),
                array(
                    'currency' => 'GBP',
                    'territory' => 'EU',
                    'column' => 'costPrice'
                )
            ),
            'USD CP' => array(
                array(
                    'currency' => 'USD',
                    'territory' => 'US',
                    'column' => 'costPrice'
                ),
            ),
            'CAD CP' => array(
                array(
                    'currency' => 'CAD',
                    'territory' => 'CA',
                    'column' => 'costPrice'
                ),
            ),
            'AUD CP' => array(
                array(
                    'currency' => 'AUD',
                    'territory' => 'GB',
                    'column' => 'costPrice'
                ),
            ),
            'EUR CP' => array(
                array(
                    'currency' => 'EUR',
                    'territory' => 'DE',
                    'column' => 'costPrice'
                ),
            )
        );
    }
    /**
     * Get the key based on keys array values
     * @param srting $currency
     * @param string $territory
     * @param string $column
     * @return string[][]|boolean
     */
    protected function getPriceKey($currency, $territory, $column)
    {
        $map = $this->priceColumnMapper();
        foreach($map as $key => $value)
        {
            if ( $map[$key][0]['currency'] === $currency && $map[$key][0]['territory'] === $territory && $map[$key][0]['column'] === $column )
                return $key;
        }
        return false;
    }

    /**
     * Get list of all schedules for an option or style
     * @param array $params
     * @return array
     */
    public function getScheduledPrices($params)
    {
        $result = [];

        if($params['priceTable'] == 'PRODUCTS_Supplier_Price'){
            if($params['priceColumn'] == 'currencyCode'){
                $key = 'TC (Trading Currency)';
            }elseif($params['priceColumn'] == 'price'){
                $key = 'TCP (Trading Currency Price)';
            }else{
                $key = false;
            }
        }else{
            $key = $this->getPriceKey($params['currency'], $params['territory'], $params['priceColumn']);
        }
        if($key == false)
        {
            return $result;
        }

        $result = $this->getScheduledBulkProcessesByKey($key, $params['code']);
        return $result;
    }

    Public function getScheduledBulkProcessesByKey($key, $styleOrOption)
    {
        $result = [];

        try {
            $sql = new Sql($this->_dbAdapter);

            $select = $sql->select();
            $select->from($this->_blukProcessingTable);
            $select->where->equalTo('processing', 0);
            $select->where->equalTo('processed', 0);
            $select->where->equalTo('styleOrOption', $styleOrOption);
            $select->where->equalTo('process_key', $key);
            $select->where->equalTo('status', '1');
            $select->where->greaterThan('schedule', date("Y-m-d H:i:s"));

            //echo $sql->getSqlstringForSqlObject($select);
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            foreach ($results as $row) {
                $result[] = $row;
            }
        } catch (\Exception $e) {
            return $result;
        }
        return $result;
    }

    /**
     * set bulk process deleted  as deleted 0
     * @param int $bulkProcessingId
     */
    public function deleteBulkProcessById($bulkProcessingId)
    {
        $data = [
            'status' => '0',
            'processing' => '1',
            'processed' => '1'
        ];

        try {
            $this->updateBulkProcess($bulkProcessingId, $data);
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }
}