<?php
namespace PrismProductsManager\Model;

/**
 *
 * @author hani.weiss
 *        
 */
class CategoriesEntity
{

    protected $_categoryId;

    protected $_categoryParentId;

    protected $_treeDepth = 1;

    protected $_position = 1;

    protected $_status = 'INACTIVE';

    protected $_created;

    protected $_modified;

    /**
     * @param mixed $modified
     */
    public function setModified($modified)
    {
        $this->_modified = $modified;
    }

    /**
     * @return mixed
     */
    public function getModified()
    {
        return $this->_modified;
    }

    public function __construct()
    {
        $this->_created = date("Y-m-d H:i:s");
        $this->_categoryParentId = 1;
    }

    /**
     *
     * @return the $_categoryId
     */
    public function getCategoryId()
    {
        return $this->_categoryId;
    }

    /**
     *
     * @return the $_parentId
     */
    public function getCategoryParentId()
    {
        return $this->_categoryParentId;
    }

    /**
     *
     * @return the $_treeDepth
     */
    public function getTreeDepth()
    {
        return $this->_treeDepth;
    }

    /**
     *
     * @return the $_position
     */
    public function getPosition()
    {
        return $this->_position;
    }

    /**
     *
     * @return the $_status
     */
    public function getStatus()
    {
        return $this->_status;
    }

    /**
     *
     * @return the $_created
     */
    public function getCreated()
    {
        return $this->_created;
    }

    /**
     *
     * @param field_type $_categoryId            
     */
    public function setCategoryId($categoryId)
    {
        $this->_categoryId = $categoryId;
    }

    /**
     *
     * @param field_type $_parentId            
     */
    public function setCategoryParentId($categoryParentId)
    {
        $this->_categoryParentId = $categoryParentId;
    }

    /**
     *
     * @param field_type $_treeDepth            
     */
    public function setTreeDepth($treeDepth)
    {
        $this->_treeDepth = $treeDepth;
    }

    /**
     *
     * @param field_type $_position            
     */
    public function setPosition($position)
    {
        $this->_position = $position;
    }

    /**
     *
     * @param field_type $_status            
     */
    public function setStatus($status)
    {
        $this->_status = $status;
    }

    /**
     *
     * @param string $_created            
     */
    public function setCreated($created)
    {
        $this->_created = $created;
    }
}