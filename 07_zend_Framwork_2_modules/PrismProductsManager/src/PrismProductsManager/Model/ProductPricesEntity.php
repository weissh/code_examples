<?php
namespace PrismProductsManager\Model;


/**
 * Class ProductPricesEntity
 * @package PrismProductsManager\Model
 */
class ProductPricesEntity
{

   /**
     * @var integer
     */
    private $productPriceId;

    /**
     * @var string
     */
    private $productIdFrom;

    /**
     * @var integer
     */
    private $productId;

    /**
     * @var integer
     */
    private $regionId;

    /**
     * @var integer
     */
    private $currencyId;

    /**
     * @var string
     */
    private $price;

    /**
     * @var string
     */
    private $offerPrice;

    /**
     * @var string
     */
    private $nowPrice;

    /**
     * @var string
     */
    private $wasPrice;

    /**
     * @var string
     */
    private $costPrice;

    /**
     * @var string
     */
    private $tradingCostPrice;

    /**
     * @var string
     */
    private $wholesalePrice;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var timestamp
     */
    private $modified;

    /**
     * @var \DateTime
     */
    private $created;


    /**
     * Get productPriceId
     *
     * @return integer 
     */
    public function getProductPriceId()
    {
        return $this->productPriceId;
    }

    /**
     * Set productIdFrom
     *
     * @param string $productIdFrom
     * @return Test
     */
    public function setProductIdFrom($productIdFrom)
    {
        $this->productIdFrom = $productIdFrom;

        return $this;
    }

    /**
     * Get productIdFrom
     *
     * @return string 
     */
    public function getProductIdFrom()
    {
        return $this->productIdFrom;
    }

    /**
     * @return string
     */
    public function getOfferPrice()
    {
        return $this->offerPrice;
    }

    /**
     * @param string $offerPrice
     */
    public function setOfferPrice($offerPrice)
    {
        $this->offerPrice = $offerPrice;
    }
    /**
     * Set productId
     *
     * @param integer $productId
     * @return Test
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set regionId
     *
     * @param integer $regionId
     * @return Test
     */
    public function setRegionId($regionId)
    {
        $this->regionId = $regionId;

        return $this;
    }

    /**
     * Get regionId
     *
     * @return integer 
     */
    public function getRegionId()
    {
        return $this->regionId;
    }

    /**
     * Set currencyId
     *
     * @param integer $currencyId
     * @return Test
     */
    public function setCurrencyId($currencyId)
    {
        $this->currencyId = $currencyId;

        return $this;
    }

    /**
     * Get currencyId
     *
     * @return integer 
     */
    public function getCurrencyId()
    {
        return $this->currencyId;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Test
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set nowPrice
     *
     * @param string $nowPrice
     * @return Test
     */
    public function setNowPrice($nowPrice)
    {
        $this->nowPrice = $nowPrice;

        return $this;
    }

    /**
     * Get nowPrice
     *
     * @return string 
     */
    public function getNowPrice()
    {
        return $this->nowPrice;
    }

    /**
     * Set wasPrice
     *
     * @param string $wasPrice
     * @return Test
     */
    public function setWasPrice($wasPrice)
    {
        $this->wasPrice = $wasPrice;

        return $this;
    }

    /**
     * Get wasPrice
     *
     * @return string 
     */
    public function getWasPrice()
    {
        return $this->wasPrice;
    }

    /**
     * Set costPrice
     *
     * @param string $costPrice
     * @return Test
     */
    public function setCostPrice($costPrice)
    {
        $this->costPrice = $costPrice;

        return $this;
    }

    /**
     * Get costPrice
     *
     * @return string 
     */
    public function getCostPrice()
    {
        return $this->costPrice;
    }

    /**
     * Set tradingCostPrice
     *
     * @param string $tradingCostPrice
     * @return Test
     */
    public function setTradingCostPrice($tradingCostPrice)
    {
        $this->tradingCostPrice = $tradingCostPrice;

        return $this;
    }

    /**
     * Get tradingCostPrice
     *
     * @return string 
     */
    public function getTradingCostPrice()
    {
        return $this->tradingCostPrice;
    }

    /**
     * Set wholesalePrice
     *
     * @param string $wholesalePrice
     * @return Test
     */
    public function setWholesalePrice($wholesalePrice)
    {
        $this->wholesalePrice = $wholesalePrice;

        return $this;
    }

    /**
     * Get wholesalePrice
     *
     * @return string 
     */
    public function getWholesalePrice()
    {
        return $this->wholesalePrice;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Test
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set modified
     *
     * @param \timestamp $modified
     * @return Test
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \timestamp 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Test
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->productPriceId      = (isset($data['productPriceId'])) ? $data['productPriceId']   : null;
        $this->productIdFrom = (isset($data['productIdFrom']))   ? $data['productIdFrom']     : null;
        $this->productId = (isset($data['productId']))   ? $data['productId']     : null;
        $this->regionId = (isset($data['regionId']))   ? $data['regionId']     : null;
        $this->currencyId = (isset($data['currencyId']))   ? $data['currencyId']     : null;
        $this->price = (isset($data['price']))   ? $data['price']     : null;
        $this->nowPrice = (isset($data['nowPrice']))   ? $data['nowPrice']     : null;
        $this->wasPrice = (isset($data['wasPrice']))   ? $data['wasPrice']     : null;
        $this->costPrice = (isset($data['costPrice']))   ? $data['costPrice']     : null;
        $this->tradingCostPrice = (isset($data['tradingCostPrice']))   ? $data['tradingCostPrice']     : null;
        $this->wholesalePrice = (isset($data['wholesalePrice']))   ? $data['wholesalePrice']     : null;
        $this->status             = (isset($data['status']))   ? $data['status']     : 'ACTIVE';
        $this->modified         = (isset($data['modified']))   ? $data['modified']     : null;
        $this->created           = (isset($data['created']))     ? $data['created']       : date('Y-m-d H:i:s');
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);

    }


}