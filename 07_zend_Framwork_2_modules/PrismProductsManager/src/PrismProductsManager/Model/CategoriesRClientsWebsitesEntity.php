<?php

namespace PrismProductsManager\Model;


/**
 * Class CategoriesRClientsWebsitesEntity
 * @package PrismProductsManager\Model
 */
class CategoriesRClientsWebsitesEntity
{
    /**
     * @var
     */
    protected $_categoryWebsiteId = '';
    /**
     * @var
     */
    protected $_categoryId;
    /**
     * @var
     */
    protected $_websiteId;

    /**
     * @param mixed $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->_categoryId = $categoryId;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->_categoryId;
    }

    /**
     * @param mixed $categoryWebsiteId
     */
    public function setCategoryWebsiteId($categoryWebsiteId)
    {
        $this->_categoryWebsiteId = $categoryWebsiteId;
    }

    /**
     * @return mixed
     */
    public function getCategoryWebsiteId()
    {
        return $this->_categoryWebsiteId;
    }

    /**
     * @param mixed $websiteId
     */
    public function setWebsiteId($websiteId)
    {
        $this->_websiteId = $websiteId;
    }

    /**
     * @return mixed
     */
    public function getWebsiteId()
    {
        return $this->_websiteId;
    }


}
