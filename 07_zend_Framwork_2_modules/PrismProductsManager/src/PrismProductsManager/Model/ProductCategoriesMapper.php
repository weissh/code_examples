<?php


namespace PrismProductsManager\Model;

use PrismProductsManager\Model\CategoriesTable;
use PrismProductsManager\Model\CategoryDefinitionsTable;

class ProductCategoriesMapper
{
    protected $categoriesTable;

    public function __construct(CategoriesTable $categoriesTable)
    {
        $this->categoriesTable = $categoriesTable;
    }

    /*public function findClientByUserId($id)
    {
        $select = $this->permissionemployee->getTableGateway()->getSql()->select();
        $select->join('CLIENTS_Websites', 'CLIENTS_Websites.websiteId = PERMISSIONS_Prism_Employee.websiteId');
        $select->join('CLIENTS', 'CLIENTS_Websites.clientId = CLIENTS.clientId');
        $select->where(array('userId' => $id));

        $select->group(array('CLIENTS.clientId'));

        $employeeclientpermissions = $this->permissionemployee->getTableGateway()
                            ->selectWith($select);

        return $employeeclientpermissions;
    }*/


    public function getCategories()
    {
//        echo '<pre>';
//        print_r($this->categoriesTable); exit;
        $select = $this->categoriesTable->getTableGateway()->getSql()->select();
        $select->where(array('enabled' => 1));
        $rowset = $this->categoriesTable->getTableGateway()->selectWith($select);

        return $rowset;
    }


    public function saveCategory($data) {
        $result = FALSE;
        $currentDateTime = date('Y-m-d H:i:s');
        $data['created'] = (!isset($data['created'])) ? $currentDateTime : $data['created'];
        $data['modified'] = (!isset($data['modified'])) ? $currentDateTime : $data['modified'];
        if ($data){
            if (! $this->categoriesTable->getTableGateway()->insert($data)) {
                $result = TRUE;  //handle/log errors here...
            }
        }
       // $lastInsertedId = $this->getLastInsertValue();

        return $result;
    }

    /**
     * Update existing category by $id
     *
     * @param int $id
     * @param array $data
     */
    public function updateExistingCategory($id, $data) {
        if ($id > 0) {
            if (! $this->categoriesTable->getTableGateway()->update($data, array('id' => $id))) {
                return false;
            }
            return $id;
        }
    }
}
