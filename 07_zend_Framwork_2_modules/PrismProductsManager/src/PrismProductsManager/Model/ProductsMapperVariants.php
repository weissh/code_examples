<?php

namespace PrismProductsManager\Model;

use PrismMediaManager\Model\ProductFolderMapper;
use PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable;
use Zend\Db\Adapter\Adapter;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
//use Zend\Db\Sql\Select;
//use Zend\Db\ResultSet\HydratingResultSet;
use PrismProductsManager\Model\ProductsMapper;
use Zend\Filter\File\UpperCase;

/**
 * This class is a sub class of ProductsMapper class.
 * It was created to reduce the length of the parent calss.
 *
 * It holds business logic for Product variants.
 */

class ProductsMapperVariants extends ProductsMapper
{

    /**
     * @var Sql
     */
    protected $_sql;
    /**
     * @var
     */
    protected $_hydrator;
    /**
     * @var Adapter
     */
    protected $_dbAdapter;
    /**
     * @var MerchantCategoriesMapper
     */
    protected $_merchantCategoriesMapper;
    /**
     * @var ProductsMapperAttributes
     */
    protected $_productsMapperAttributes;
    /**
     * @var ProductsMapperPrices
     */
    protected $_productsMapperPrices;
    /**
     * @var ProductFolderMapper
     */
    protected $_productFolderMapper;
    /**
     * @var string
     */
    protected $_sizeSkuConverer = 'SIZE_SKU_Converter';

    /**
     * @param Sql $sql
     * @param $hydrator
     * @param Adapter $dbAdapter
     * @param MerchantCategoriesMapper $merchantCategoriesMapper
     * @param ProductsMapperAttributes $productsMapperAttributes
     * @param ProductsMapperPrices $productsMapperPrices
     * @param TaxMapper $taxMapper
     * @param ProductFolderMapper $productFolderMapper
     */
    public function __construct(
        Sql $sql, $hydrator, Adapter $dbAdapter, MerchantCategoriesMapper $merchantCategoriesMapper,
        ProductsMapperAttributes $productsMapperAttributes, ProductsMapperPrices $productsMapperPrices,
        TaxMapper $taxMapper, ProductFolderMapper $productFolderMapper, CommonAttributesValuesTable $commonAttributesValuesTable
    )
    {
        $this->_sql = $sql;
        $this->_hydrator = $hydrator;
        $this->_dbAdapter = $dbAdapter;
        $this->_merchantCategoriesMapper = $merchantCategoriesMapper;
        $this->_productsMapperAttributes = $productsMapperAttributes;
        $this->_productsMapperPrices = $productsMapperPrices;
        $this->_taxMapper = $taxMapper;
        $this->_productFolderMapper = $productFolderMapper;
        $this->commonAttributesValuesTable = $commonAttributesValuesTable;
    }

    /**
     * Function to get all variants that have been saved created for a product
     *
     * @param $productId
     * @param $notDeleted
     * @return mixed
     */
    public function getProductVariantsByProductID($productId, $notDeleted = true, $activeOnly = false)
    {
        $returnArray = array();
        $this->_sql->setTable(array('pvr' => $this->_productsVariantsRelationshipTable));
        $select = $this->_sql->select();
        $select->columns(
            array(
                'productAttributeId',
                'manufacturerId',
                'ean13',
                'ecotax',
                'sku',
                'supplierReference',
                'status',
                'indexed',
                'created',
                'modified',
            )
        );
        $select->join(
            array('p' => $this->_tableName), //Set join table and alias
            "p.productId = pvr.productId",
            array('productMerchantCategoryId')
        );
        $select->where(array('pvr.productId' => $productId));
        if($notDeleted) {
            $select->where->notEqualTo('pvr.status', 'DELETED');
        }

        if($activeOnly == true) {
            $select->where->notEqualTo('pvr.status', 'INACTIVE');
        }

        $select->order('pvr.productAttributeId DESC');

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $value) {
            $skuRules = $this->_merchantCategoriesMapper->getAttributeValue($value['productAttributeId'], $this->_productsAttributesSkuRuleRelationsTable);
            if (count($skuRules) > 0) {
                $position = 1;
                foreach ($skuRules as $key => $skuRuleArray) {
                    $value['skuRule' . $position] = array($key => strtoupper(current($skuRuleArray)));
                    $position++;
                }
            }
            $returnArray[] = $value;
        }
        return $returnArray;
    }

    /**
     * Check if a sku variant exist.
     *
     * @param $sku
     * @param $productId
     * @return mixed
     */
    protected function checkIfSkuExist($sku, $productId, $getAllData = false)
    {
        $returnValue = 0;
        $this->_sql->setTable(array('pvr' => $this->_productsVariantsRelationshipTable));
        $select = $this->_sql->select();
        $select->columns(array('productAttributeId', 'status'));
        $select->where(array('pvr.sku' => $sku, 'pvr.productId' => $productId));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            if ($getAllData) {
                $returnValue = $value;
            } else {
                $returnValue = $value['productAttributeId'];
            }
        }

        return $returnValue;
    }

    /**
     * Retrieve details about sku rule code
     *
     * @param $paramString
     * @return mixed
     */
    protected function _getSkuRuleCodeDetails($paramString)
    {
        $returnArray = array();
        if (trim($paramString) != '') {
            //the first sku rule the user selected
            $firstSkuRuleExploded = explode('@', $paramString);
            $firstSkuRuleId = $firstSkuRuleExploded[0];
            $tableName = $firstSkuRuleExploded[1];

            $skuRuleArray = array();
            if ($tableName == 'COLOURS_Groups') {
                $skuRuleArray = $this->_merchantCategoriesMapper->getColourNameByColourId($tableName, $firstSkuRuleId);
            } else if (strtolower(substr($tableName, 0, 4)) == 'size') {
                $skuRuleArray = $this->_merchantCategoriesMapper->getSizeNameBySizeId($tableName, $firstSkuRuleId);
            } else if ($tableName == 'COMMON_ATTRIBUTES_VALUES') {
                $skuRuleArray = $this->commonAttributesValuesTable->getTableGateway()->select('commonAttributeValueId = ' . $firstSkuRuleId);
                $skuRuleArray = $this->commonAttributesValuesTable->listResultsWithKeyAs($skuRuleArray, '', 'commonAttributeValueSkuCode', 'commonAttributeValueSkuCode');


            }
            $skuRuleCode = array_keys($skuRuleArray);
            $returnArray['tableName'] = $tableName;
            $returnArray['attributeId'] = $firstSkuRuleId;
            $returnArray['skuRuleCode'] = $skuRuleCode[0];
        }
        return $returnArray;
    }

        /**
     * Save product variants
     *
     * @param $params
     * @param $productId
     * @return int
     */
    protected function _saveVariants($params, $productId)
    {
        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
        // Get the first part of the sku. Usually set in the config
        $firstPartOfSku = $params['firstPartOfSku'];
        $useSkuAsBarcode = $params['useSkuAsBarcode'];

        // the first sku rule the user selected
        $secondPartOfSkuKey = $params['skuRules'][0];
        $skuRuleCode = $this->_getSkuRuleCodeDetails($secondPartOfSkuKey);
        $firstSkuRule = $skuRuleCode['skuRuleCode'];

        unset($params['skuRules'][0]);
        $subSkuRulesCount = count($params['skuRules']);
        if ($subSkuRulesCount == 0) {
            return array('error' => 'Could not create variants! Please contact support if the problem persists !');
        }
        $sku = array();
        $variantsSavedCount = 0;
        // Build the sku
        if ($subSkuRulesCount > 0) {
            foreach ($params['skuRules'] as $skuDetails) {
                if (is_array($skuDetails) && count($skuDetails)) {
                    foreach ($skuDetails as $skuValues) {
                        $skuValue = $this->_extractPartOfSku($skuValues) ;
                        // The
                        $sku[$skuValues] = strtoupper($firstPartOfSku . $firstSkuRule . $skuValue);
                    }
                }
            }
        }
        // Get all the Product_Attributes_Combinations for the parent Product
        $mainProductAttributesCombinations = $this->_productsMapperAttributes->getAllCombinationsForProduct($productId);
        $mainProductPrices = $this->getProductPrices($productId, true);
        $mainProductTaxRules = $this->_taxMapper->getTaxForProduct($productId, 0);

        $mainProductDetails = $this->getMinimumProductsDetails($productId);
        if (count($sku) > 0) {
            $productsVariantsEntity = new ProductsAttributesEntity();
            $productData = array();
            foreach ($sku as $skuRuleKey => $skuName) {
                // we must replace all the spaces in the skuName
                $skuName = str_replace(' ', '', $skuName);
                if (trim($skuName) != '') {
                    // Check if the sku exist so we skip saving it again
                    $skuData = $this->checkIfSkuExist($skuName, $productId, true);
                    if ($skuData['productAttributeId'] > 0 && $skuData['status'] != 'DELETED') {
                        // Variant already
                        continue;
                    }

                    if ($skuData['productAttributeId'] > 0 && $skuData['status'] == 'DELETED') {
                        $update = $this->_sql->update();
                        $update->table($this->_productsVariantsRelationshipTable);
                        $update->set(array('status' => 'INACTIVE'));
                        $update->where(array(
                            'productAttributeId' => $skuData['productAttributeId'],
                        ));
                        $statement = $this->_sql->prepareStatementForSqlObject($update);
                        $statement->execute();
                        $savedVariantId = $skuData['productAttributeId'];
                    } else {
                        $productData['sku'] = $skuName;
                        $productData['productId'] = $productId;
                        if ($useSkuAsBarcode > 0) {
                            $skuName = $this->checkIfSkuCanBeUsedAsBarcode($skuName);
                            if (empty($skuName)) {
                                $this->_dbAdapter->getDriver()->getConnection()->rollback();
                                return array('error' => 'Sku '.$skuName.' cannot be used as a barcode!');
                            }
                            $productData['ean13'] = $skuName;
                        } else {
                            $productVariants = $this->getProductVariantsByProductID($productId);
                            if (count($productVariants) == 0) {
                                // assign the product barcode tot his variant
                                $productData['ean13'] = $mainProductDetails['ean13'];
                            } else {
                                $nextBarcode = $this->getNextAvailableBarcodeForProduct();
                                if (empty($nextBarcode)) {
                                    $this->_dbAdapter->getDriver()->getConnection()->rollback();
                                    return array('error' => 'The variants could not be created since we don\'t have enough available barcodes on the system!');
                                } else {
                                    // assign this as a barcode for this variant
                                    $productData['ean13'] = $nextBarcode;
                                }
                            }
                        }
//                        $productData['status'] = $this->getProductStatusByProductId($productId);
                        $productData['status'] = 'INACTIVE';

                        $hydratedData = $this->_hydrator->hydrate($productData, $productsVariantsEntity);
                        $data = $this->_hydrator->extract($hydratedData);
                        $savedVariantId = $this->_save($data, $this->_productsVariantsRelationshipTable);
                    }


                    if ($savedVariantId > 0) {
                        //Save the relationship between variants and sku building rules.
                        $variantSkuRuleParams = array();
                        $skuRuleKeyExploded1 = explode('@', $secondPartOfSkuKey);
                        $variantSkuRuleParams[0]['productAttributeId'] = $savedVariantId;
                        $variantSkuRuleParams[0]['skuRuleId'] = $skuRuleKeyExploded1[0];
                        $variantSkuRuleParams[0]['skuRuleTable'] = $skuRuleKeyExploded1[1];

                        $skuRuleKeyExploded2 = explode('@', $skuRuleKey);
                        $variantSkuRuleParams[1]['productAttributeId'] = $savedVariantId;
                        $variantSkuRuleParams[1]['skuRuleId'] = $skuRuleKeyExploded2[0];
                        $variantSkuRuleParams[1]['skuRuleTable'] = $skuRuleKeyExploded2[1];
                        $this->_saveVariantSkuRuleRelationship($variantSkuRuleParams);
                        // Create the variant attributes in the Product_Attributes_Combination
                        $this->createVariantsAttributes($mainProductAttributesCombinations, $savedVariantId);
                        // Create the variant prices in the Product_Prices
                        $this->createVariantsPrices($mainProductPrices, $savedVariantId);
                        // Copy over the definitions from the product
                        $this->copyProductDefinitionsToVariants($productId, $savedVariantId);
                        // Copy the marketing categories from the main product to it's variants
                        $this->copyMarketingCategoriesFromProductToVariants($productId, $savedVariantId);
                        // Save the variants tax information
                        $this->_taxMapper->addTaxForProduct($mainProductTaxRules, $savedVariantId, 1);
                        // Save variant images from the main product
                        $this->_productFolderMapper->createProductFolderByProductAndVariant($productId, $savedVariantId);
                        $this->_saveProductSpectrum(
                            array('style' => $mainProductDetails['style']),
                            $savedVariantId,
                            $this->_productsVariantsRelationshipTable,
                            $productId
                        );
                        $variantsSavedCount++;
                    }
                }
            }
        }
        $this->_dbAdapter->getDriver()->getConnection()->commit();
        return $variantsSavedCount;
    }

    /**
     *  Copy marketing categories from a main product to it's variants
     *
     * @param type $productId
     * @param type $savedVariantId
     */
    public function copyMarketingCategoriesFromProductToVariants($productId, $savedVariantId)
    {
        $mainProductCategories = $this->_getMainProductCategories($productId);

        $productCategoriesEntity = new ProductsCategoriesEntity();
        if (count($mainProductCategories) > 0) {
            foreach ($mainProductCategories as $mainProductCategoryArray) {

                $mainProductCategoryId = isset($mainProductCategoryArray['categoryId']) ? $mainProductCategoryArray['categoryId'] : 0;
                $isDefault = isset($mainProductCategoryArray['isDefault']) ? $mainProductCategoryArray['isDefault'] : 0;
                if (isset($mainProductCategoryId) && $mainProductCategoryId > 0) {
                    //Check if this variant has previously been assigned to this category
                    $variantAlreadyInCategory = $this->checkIfVariantAlreadyAssignedToMarketingCategory($mainProductCategoryId, array($savedVariantId));
                    if (count($variantAlreadyInCategory) > 0) {
                        foreach ($variantAlreadyInCategory as $variantId) {
                            $productCategoryArray['productId'] = $variantId;
                            $productCategoryArray['isDefault'] = $isDefault;
                            $productCategoryArray['categoryId'] = $mainProductCategoryId;
                            $productCategoryArray['productIdFrom'] = $this->_productsVariantsRelationshipTable;
                            $hydratedData = $this->_hydrator->hydrate($productCategoryArray, $productCategoriesEntity);
                            $data = $this->_hydrator->extract($hydratedData);
                            $this->_save($data, $this->_productsCategoriesRelationshipTable);
                        }
                    }
                }
            }
        }
    }

    /**
     * Get the marketing categories of
     *
     * @param type $productId
     * @return type
     */
    protected function _getMainProductCategories($productId)
    {
        $this->_sql->setTable($this->_productsCategoriesRelationshipTable);
        $select = $this->_sql->select();
        $select->columns(array('categoryId', 'isDefault'));
        //$select->where->notEqualTo('status', 'DELETED');
        $select->where(array('productId' => $productId, 'productIdFrom' => $this->_tableName, 'status' => 'ACTIVE'));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $categoriesArray = array();
        foreach ($results as $key => $value) {
            if (isset($value['categoryId']) && $value['categoryId'] !== null) {
                $categoriesArray[] = array('categoryId' => $value['categoryId'], 'isDefault' => $value['isDefault']);
            }
        }

        return $categoriesArray;
    }

    /**
     * Check if a variant has been assigned to a marketing category
     * This is a copy of the same function in CategoriesMapper - During refactoring they should be merged
     *
     * @param type $categoryId
     * @param type $variantIdArray
     * @return type
     */
    public function checkIfVariantAlreadyAssignedToMarketingCategory($categoryId, $variantIdArray)
    {
        $returnArray = array();
        $this->_sql->setTable(array('prc' => $this->_productsCategoriesRelationshipTable));
        $select = $this->_sql->select();
        $select->columns(array('productCategoryId', 'categoryId', 'productId'));
        $select->where->notEqualTo('prc.status', 'DELETED');
        $select->where->in('prc.productId', $variantIdArray);
        $select->where(array('prc.categoryId'=> $categoryId));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $productCategoryId = array();
        foreach ($results as $value) {
            if ($value['productCategoryId'] !== null) {
                $productCategoryId[] = $value['productId'];
                $key = array_search($value['productId'], $variantIdArray);
                if ($key !== false) {
                    unset($variantIdArray[$key]);
                }
            }
        }

        return $variantIdArray;

    }

    /**
     * Save the relationship between variants and sku building rules.
     *
     * @param $variantSkuRuleParams
     * @return mixed
     */
    protected function _saveVariantSkuRuleRelationship($variantSkuRuleParams)
    {
        if (is_array($variantSkuRuleParams) && count($variantSkuRuleParams) > 0) {
            $productAttributesSkuRulesEntity = new ProductsAttributesSkuRulesEntity();
            foreach ($variantSkuRuleParams as $key => $skuRuleArray) {
                $hydratedData = $this->_hydrator->hydrate($skuRuleArray, $productAttributesSkuRulesEntity);
                $data = $this->_hydrator->extract($hydratedData);
                $this->_save($data, $this->_productsAttributesSkuRuleRelationsTable);
            }
        }
    }

    /**
     * Funtion to extract part of sku
     *
     * @param type $skuValues
     * @return type
     */
    protected function _extractPartOfSku($skuValues)
    {
        $skuRuleCode = $this->_getSkuRuleCodeDetails($skuValues);
        $tableName = strtolower(substr($skuRuleCode['tableName'], 0, 4));
        $code = $skuRuleCode['skuRuleCode'];

        $skuValue = '';
        if (is_numeric($code)) {
            switch ($tableName) {
                case 'size' :
                    $skuValue = $this->_changeSizeFormat($code);
                    break;
                default :
            }
        } else {
            switch ($tableName) {
                case 'size' :
                    $skuValue = $this->_convertSizeToNumeric($code);
                    break;
                default :
                    $skuValue = $code;
            }

        }
        return $skuValue;
    }

    /**
     * Convert size format e.g from 9.5 to 095
     * ReverseFormat converts the reverse way e.g from 095 to 9.5
     *
     * @param $sizeToFormat
     * @param $reverseFormat
     * @return mixed
     */
    protected function _changeSizeFormat($sizeToFormat, $reverseFormat = 0)
    {
        $skuValue = '';
        if ($reverseFormat < 1) {
            $skuValue = $sizeToFormat * 10;
            $skuValue = (strlen($skuValue) < 3) ? str_pad($skuValue, 3, 0, STR_PAD_LEFT) : $skuValue;
        }
        return $skuValue;
    }

    /**
     * The SKU must have numeric size
     * @param unknown $code
     * @return Ambigous <string, unknown>
     */
    protected function _convertSizeToNumeric($code)
    {
        $code = strtoupper($code);
        $skuValue = '';

        $this->_sql->setTable(array('skc' => $this->_sizeSkuConverer));
        $select = $this->_sql->select();
        $select->columns(array('skuCode'));
        $select->where(array('skc.sizeAlphabetic' => $code));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $skuValue = $statement->execute()->next();

        if (empty($skuValue))  {
            $skuValue = $code;
        } else {
            $skuValue = $skuValue['skuCode'];
        }

   /**
        switch ($code) {
            case ($code) == 'ONE SIZE':
            $skuValue = '001';
            break;

            case ($code) == 'S':
                $skuValue = '002';
                break;

            case ($code) == 'M':
                $skuValue = '003';
                break;

            case ($code) == 'L':
                $skuValue = '004';
                break;

            case ($code) == 'XL':
                $skuValue = '005';
                break;

            case ($code) == 'XXL':
                $skuValue = '006';
                break;

            case ($code) == 'XXXL':
                $skuValue = '007';
                break;

            case ($code) == 'S/M':
                $skuValue = '012';
                break;

            case ($code) == 'M/L':
                $skuValue = '023';
                break;

            case ($code) == 'L/XL':
                $skuValue = '034';
                break;


            default:
                $skuValue = $code;
            break;
        }
*/
        return $skuValue;
    }

    /**
     * @param $productId
     * @param $variantNumber
     * @return int
     */
    public function saveVariantsForCollation($productId, $variantNumber)
    {
        $variantsSavedCount = 0;
        try {
            $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
            /**
             * Get the main details from the product level
             * in order to create the variants with the same details
             */
            $mainProductAttributesCombinations = $this->_productsMapperAttributes->getAllCombinationsForProduct($productId);
            $mainProductPrices = $this->getProductPrices($productId, true);
            $mainProductTaxRules = $this->_taxMapper->getTaxForProduct($productId, 0);
            $mainProductDetails = $this->getMinimumProductsDetails($productId);
            $variantsSavedCount = 0;
            /**
             * Loop the number of variants that will need to be created
             */
            for ($i=1; $i<=$variantNumber; $i++) {
                $productsVariantsEntity = new ProductsAttributesEntity();
                $productData = array();
                $productData['productId'] = $productId;
                /**
                 * Get the barcode used in the main product holder if the product does not have any other variants
                 * Or get the next available barcode from the system ( or trigger exception )
                 */
                $productVariants = $this->getProductVariantsByProductID($productId);
                if (count($productVariants) == 0) {
                    // assign the product barcode tot his variant
                    $productData['ean13'] = $mainProductDetails['ean13'];
                } else {
                    $nextBarcode = $this->getNextAvailableBarcodeForProduct();
                    if (empty($nextBarcode)) {
                        throw new \Exception('No available barcodes in the system!');
                    } else {
                        // assign this as a barcode for this variant
                        $productData['ean13'] = $nextBarcode;
                    }
                }
                $productData['status'] = 'INACTIVE';
                /**
                 * Use the zf2 Hydrator Class to pass the data in the variant entity and extract it
                 */
                $hydratedData = $this->_hydrator->hydrate($productData, $productsVariantsEntity);
                $data = $this->_hydrator->extract($hydratedData);
                /**
                 * Save the variant data and return the new variant id
                 */
                $savedVariantId = $this->_save($data, $this->_productsVariantsRelationshipTable);
                /**
                 * After saving the variant we need to modify the sku of the variant using a predefined productId+variantId
                 * and update the sku for that variant in the database
                 */
                $productData['sku'] = (string)$productId . (string)$savedVariantId;
                $this->_update($productData, $this->_productsVariantsRelationshipTable, array('productAttributeId' => $savedVariantId));
                if ($savedVariantId > 0) {
                    /**
                     * Create the variant attributes in the Products_Attributes_Combination
                     */
                    $this->createVariantsAttributes($mainProductAttributesCombinations, $savedVariantId);
                    /**
                     * Create the variant prices in the Product_prices
                     */
                    $this->createVariantsPrices($mainProductPrices, $savedVariantId);
                    /**
                     * Copy over the definitions from the product
                     */
                    $this->copyProductDefinitionsToVariants($productId, $savedVariantId);
                    /**
                     * Copy the marketing categories from the main product to it's variants
                     */
                    $this->copyMarketingCategoriesFromProductToVariants($productId, $savedVariantId);
                    /**
                     * Save the variants tax information
                     */
                    $this->_taxMapper->addTaxForProduct($mainProductTaxRules, $savedVariantId, 1);
                    /**
                     * Save variant images from the main product
                     */
                    $this->_productFolderMapper->createProductFolderByProductAndVariant($productId, $savedVariantId);

                    $this->_saveProductSpectrum(
                        array('style' => $mainProductDetails['style']),
                        $savedVariantId,
                        $this->_productsVariantsRelationshipTable,
                        $productId
                    );
                    $variantsSavedCount++;
                }
            }
            $this->_dbAdapter->getDriver()->getConnection()->commit();
        } catch (\Exception $ex) {
            $this->_dbAdapter->getDriver()->getConnection()->rollback();
        }
        return $variantsSavedCount;
    }

    /**
     * @param $productId
     * @param $savedVariantId
     */
    protected function copyProductDefinitionsToVariants($productId, $savedVariantId)
    {
        $productDefinitions = $this->_getProductDefinitions($productId, false, true);
        foreach ($productDefinitions as $definitionArray) {
            $this->_sql->setTable($this->_productsDefinitionsTable);
            $action = $this->_sql->insert();
            $action->values(array(
                'productId' => $savedVariantId,
                'productIdFrom' => $this->_productsVariantsRelationshipTable,
                'versionId' => $definitionArray['versionId'],
                'versionDescription' => $definitionArray['versionDescription'],
                'shortProductName' => $definitionArray['shortProductName'],
                'productName' => $definitionArray['productName'],
                'urlName' => $definitionArray['urlName'],
                'metaTitle' => $definitionArray['metaTitle'],
                'metaKeyword' => $definitionArray['metaKeyword'],
                'metaDescription' => $definitionArray['metaDescription'],
                'shortDescription' => $definitionArray['shortDescription'],
                'longDescription' => $definitionArray['longDescription'],
                'makeLive' => $definitionArray['makeLive'],
                'status' => 'INACTIVE',
                'languageId' => $definitionArray['languageId']
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        }
    }

    /**
     * @param $mainProductPrices
     * @param $savedVariantId
     */
    protected function createVariantsPrices($mainProductPrices, $savedVariantId)
    {
        foreach ($mainProductPrices as $currencyId => $pricesArray) {
            $pricesArray['price'] = $pricesArray['nowPrice'];
            $this->_productsMapperPrices->insertNewVariantPrices(
                $savedVariantId,
                $currencyId,
                $pricesArray
            );
        }
    }

    /**
     * @param $mainProductAttributesCombinations
     * @param $savedVariantId
     */
    protected function createVariantsAttributes($mainProductAttributesCombinations, $savedVariantId)
    {
        foreach ($mainProductAttributesCombinations as $mainCombinationRow) {
            if (!empty($mainCombinationRow['translations'])) {
                foreach ($mainCombinationRow['translations'] as $languageId => $attributeValue) {
                    $this->_productsMapperAttributes->insertNewProductAttribute(
                        $savedVariantId,
                        $mainCombinationRow['attributeGroupId'],
                        1,
                        $attributeValue,
                        $languageId
                    );
                }
            } else {
                $this->_productsMapperAttributes->insertNewProductAttribute(
                    $savedVariantId,
                    $mainCombinationRow['attributeGroupId'],
                    1,
                    $mainCombinationRow['attributeValue']
                );
            }
        }
    }
}