<?php

namespace PrismProductsManager\Model;

use PrismContentManager\Service\UpdateNavigation;
use PrismQueueManager\Service\QueueService;
use Zend\Db\Adapter\Adapter;
use PrismProductsManager\Model\CategoriesEntity;
use PrismProductsManager\Model\CategoriesDefinitionsEntity;
use PrismProductsManager\Model\CategoriesTagsEntity;
use PrismProductsManager\Model\CategoriesRClientsWebsitesEntity;
use Zend\Db\Sql\Select;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use PrismElasticSearch\Enum\ElasticSearchTypes;
use Zend\Http\Client;
use Zend\Http\Request;

/**
 * Class CategoriesMapper
 * @package PrismProductsManager\Model
 */
class CategoriesMapper
{
    /**
     * @var string
     */
    protected $_tableName = 'CATEGORIES';
    /**
     * @var string
     */
    protected $_categoriesDefinitionsTable = 'CATEGORIES_Definitions';
    /**
     * @var string
     */
    protected $_categoriesTagsTable = 'CATEGORIES_Tags';
    /**
     * @var string
     */
    protected $_categoriesWebsitesTable = 'CATEGORIES_R_CLIENTS_Websites';
    /**
     * @var string
     */
    protected $_categoriesRAttributesTable = 'CATEGORIES_R_ATTRIBUTES';
    /**
     * @var string
     */
    protected $_categoriesMedia = 'CATEGORIES_Media';
    /**
     * @var string
     */
    protected $_productsRCategories = 'PRODUCTS_R_CATEGORIES';
    /**
     * @var string
     */
    protected $_merchantCategoriesTable = 'PRODUCTS_Merchant_Categories';
    /**
     * @var string
     */
    protected $_merchantCategoriesDefinitionsTable = 'PRODUCTS_Merchant_Categories_Definitions';
    /**
     * @var string
     */
    protected $_attributesGroupsTable = 'ATTRIBUTES_Groups';
    /**
     * @var string
     */
    protected $_attributesGroupDefinitionsTable = 'ATTRIBUTES_Group_Definitions';
    /**
     * @var string
     * Default Products tables
     */
    protected $_productsRCategoriesTable = 'PRODUCTS_R_CATEGORIES';
    /**
     * @var string
     */
    protected $_productsTable = 'PRODUCTS';
    /**
     * @var string
     */
    protected $_productsVariantRelationshipTable = 'PRODUCTS_R_ATTRIBUTES';
    /**
     * @var string
     */
    protected $_productsDefinitionsTable = 'PRODUCTS_Definitions';
    /**
     * @var string
     */
    protected $_productRCategories = 'PRODUCTS_R_CATEGORIES';

    /**
     * @var string
     */
    protected $_colourGroupTable = 'COLOURS_Groups';
    /**
     * @var string
     */
    protected $_colourGroupsDefinitionTable = 'COLOURS_Groups_Definitions';

    /**
     * @var string
     */
    protected $_productRAttributesRSkuRulesTable = 'PRODUCTS_R_ATTRIBUTES_R_SKU_Rules';
    /**
     * @var string
     */
    protected $_skuRules = 'SKU_Rules';
    /**
     * @var \Zend\Db\Adapter\Adapter
     */
    protected $_dbAdapter;
    /**
     * @var \Zend\Db\Sql\Sql
     */
    protected $_sql;
    /**
     * @var ClassMethods
     */
    protected $_hydrator;
    /**
     * @var array
     * @stores all site config
     */
    protected $_siteConfig;
    /**
     * @var array
     * @stores config
     */
    protected $config;
    /**
     * @var object
     */
    protected $_categoriesDefinitionsEntity;
    /**
     * @var object
     */
    protected $_categoriesTagsEntity;
    /**
     * @var object
     */
    protected $_categoriesWebsitesEntity;
    /**
     * @var object
     * @stores all site languages
     */
    protected $_languages;
    /**
     *
     * @var object
     * @stores default language
     */
    protected $_defaultLanguage;
    /**
     * @var object
     * @stores all websites
     */
    protected $_websites;
    /**
     * @var object
     * @stores default website
     */
    protected $_defaultWebsite;
    /**
     * @var array
     * @stores form data
     */
    protected $_submittedData;
    /**
     * record per page when using paginator
     * @var int
     */
    protected $_recordPerPage;
    /**
     * The paginator object
     * @var object
     * Query paginator service object
     */
    protected $_queryPaginator;
    /**
     * Webiste Id from session
     * @var int
     */
    protected $_currentWebsiteId;
    /**
     * current set language from session
     * @var int
     */
    protected $_currentLanguage;
    /**
     * Used to separate concatenated categories in a string
     * @var type
     */
    protected $_categoriesSeparator = ' --> ';

    /**
     * @var UpdateNavigation
     */
    protected $_updateNavigationService;

    /**
     * @var int
     */
    protected $_defaultSkuLength;

    /**
     * @var array
     */
    protected $_categoriesForElasticSearch = array();

    /**
     * @var QueueService
     */
    protected $_queueService;

    /**
     * @var clienConfig
     */
    protected $clientConfig;


    /**
     * @param Adapter $dbAdapter
     * @param array $siteConfig
     * @param CategoriesDefinitionsEntity $categoriesDefinitionsEntity
     * @param CategoriesEntity $categoriesEntity
     * @param CategoriesTagsEntity $categoriesTagsEntity
     * @param CategoriesRClientsWebsitesEntity $categoriesWebsitesEntity
     * @param \Zend\Stdlib\Hydrator\ClassMethods $hydrator
     * @param $queryPaginator
     * @param UpdateNavigation $updateNavigationService
     * @param QueueService $queueService
     * @param QueueService $queueService
     */
    public function __construct(Adapter $dbAdapter, $siteConfig = array(), CategoriesDefinitionsEntity $categoriesDefinitionsEntity,
                                CategoriesEntity $categoriesEntity, CategoriesTagsEntity $categoriesTagsEntity,
                                CategoriesRClientsWebsitesEntity $categoriesWebsitesEntity, ClassMethods $hydrator,
                                $queryPaginator, UpdateNavigation $updateNavigationService, QueueService $queueService,$clientConfig)
    {
        $this->_siteConfig = $siteConfig;
        $this->_categoriesDefinitionsEntity = $categoriesDefinitionsEntity;
        $this->_categoriesEntity = $categoriesEntity;
        $this->_categoriesTagsEntity = $categoriesTagsEntity;
        $this->_categoriesWebsitesEntity = $categoriesWebsitesEntity;
        $this->_hydrator = $hydrator;
        $this->_languages = $siteConfig['languages']['site-languages'];
        $this->_defaultLanguage = $siteConfig['languages']['default-language'];
        $this->_currentLanguage = $siteConfig['languages']['current-language'];
        $this->_websites = $siteConfig['websites']['websites'];
        $this->_defaultWebsite = $siteConfig['websites']['default-website'];
        $this->_recordPerPage = $siteConfig['pagination']['records-per-page'];
        $this->_currentWebsiteId = $siteConfig['websites']['current-website-id'];
        $this->_currentLanguage = $siteConfig['languages']['current-language'];
        $this->_dbAdapter = $dbAdapter;
        $this->_sql = new Sql($dbAdapter);
        $this->_sql->setTable($this->_tableName);
        $this->_queryPaginator = $queryPaginator;
        $this->_updateNavigationService = $updateNavigationService;
        $this->_defaultSkuLength = 12; //This should be moved to a config file
        $this->_queueService = $queueService;
        $this->clientConfig = $clientConfig;
    }

    /**
     * @return type
     */
    public function getCategoriesSeparator()
    {
        return $this->_categoriesSeparator;
    }

    /**
     * @param $_categoriesSeparator
     */
    public function setCategoriesSeparator($_categoriesSeparator)
    {
        $this->_categoriesSeparator = $_categoriesSeparator;
    }

    /**
     *  Fetch all marketing categories
     * @param string $type
     * @param null $search
     * @param $root
     * @returns array
     */
    public function fetchAllMarketingCategories($type = '', $search = null, $root = true, $onlyActive = false)
    {
        $action = $this->_sql->select()
            ->join(
                array('cd' => 'CATEGORIES_Definitions'),
                'CATEGORIES.categoryId = cd.categoryId'
            )
            ->where('cd.languageId = '. $this->_currentLanguage)
            ->where('cd.status != "DELETED"');


        if ($onlyActive) {
            $action->where('cd.status = "ACTIVE"');
        }

        // Search
        if ($search !== null) {
            $action->where
                ->nest
                ->like('cd.categoryName', '%' . $search . '%')
                ->or
                ->like('cd.shortDescription', '%' . $search . '%')
                ->or
                ->like('cd.longDescription', '%' . $search . '%')
                ->unnest;
        }
        if ($type == 'list') {
            $action->order('cd.categoryName ASC');
        } else {
            $action->order('CATEGORIES.position ASC');
        }

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        $categories = array();
        foreach ($result as $res) {
            if ($type == 'list') {
                if ($res['categoryId'] == '1' ) {
                    if ($root) {
                        $categories[$res['categoryId']] = 'ROOT';
                    }
                } else {
                    //$categories[$res['categoryId']] = $res['categoryName'];
                    $categories[$res['categoryId']] = ucfirst(html_entity_decode($res['categoryName'], ENT_QUOTES));
                }
            } else {
                $categories[] = $res;
            }
        }

        return $categories;
    }

    /**
     * This returns an array like:
     *      array(
     *          name => 'Our specified category',
     *          mis_spells => array('mis_spell1','mis_spell2'),
     *          url => category_url_N
     *      )
     *
     * for a specific category and language
     * @param $childCategoryId
     * @param $languageId
     * @returns array
     */
    public function getAllParentCategoriesForChildCategoryId($childCategoryId, $languageId)
    {
        $this->_sql->setTable(array('C' => $this->_tableName));
        $select = $this->_sql->select();
        $select->columns(
            array(
                'categoryId',
                'categoryParentId'
            )
        );
        $select->join(
            array(
                'CD' => $this->_categoriesDefinitionsTable
            ),
            'C.categoryId = CD.categoryId',
            array(
                'categoryName',
                'url' => 'urlName'
            )
        );
        $select->join(
            array(
                'CT' => $this->_categoriesTagsTable
            ),
            'CD.categoryDefinitionsId = CT.categoriesDefinitionsId',
            array(
                'tag' => 'alias'
            ),
            Select::JOIN_LEFT
        );
        $select->where(
            array(
                'C.categoryId' => $childCategoryId,
                'CD.languageId' => $this->_currentLanguage
            )
        );

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        // echo $this->_sql->getSqlStringForSqlObject($select);die;
        $results = $statement->execute();

        $finalArray = array();
        $finalArray['mis_spells'] = array();
        foreach($results as $result) {
            $finalArray['name'] = $result['categoryName'];
            $finalArray['url'] = $result['url'];
            $finalArray['parentId'] = $result['categoryParentId'];
            $finalArray['id'] = $result['categoryId'];
            $finalArray['filter_name'] = $result['categoryId'].'@'.$result['categoryName'];
            if (!empty($result['tag'])) {
                $finalArray['mis_spells'][] = $result['tag'];
            }
        }

        return $finalArray;
    }

    /**
     *  Fetch all merchant categories
     * @param string $type
     * @internal param $type
     * @returns array
     */
    public function fetchAllMerchantCategories($type = '')
    {
        $this->_sql->setTable($this->_merchantCategoriesTable);
        $action = $this->_sql->select()
            ->join(
                array('cd' => $this->_merchantCategoriesDefinitionsTable),
                $this->_merchantCategoriesTable . '.productMerchantCategoryId = cd.productMerchantCategoryId'
            )
            ->where('cd.languageId = '. $this->_currentLanguage)
            ->where($this->_merchantCategoriesTable . '.status != "DELETED"');
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        $categories = array(0 => 'ROOT');
        foreach ($result as $res) {
            if ($type == 'list') {
                $categories[$res['productMerchantCategoryId']] = $res['categoryName'];
            } else {
                $categories[] = $res;
            }
        }
        return $categories;
    }

    /**
     * Save marketing category in the database
     * @throw \Exception
     */
    public function saveMarketingCategory()
    {
        // Insert category in category table
        $categoryId = $this->_insertCategoryTable();
        // Insert category in category definition table
        $categoryDefinitionsIds = $this->_insertCategoryDefinitionTable($categoryId);
        // Insert the tags for the category
        $this->_insertCategoryTags($categoryDefinitionsIds);
        // Save the websites for which the category will be available
        $this->_insertCategoryWebsites($categoryId);

        // If image id present, save image
        if ( !empty( $this->_submittedData['categoryBannerImage'] ) && $this->clientConfig['features']['category_image_banner']) {
            $this->_insertCategoryImageBanner($categoryId);
        }


    }

    /**
     * Insert a new category in the category table
     * @return int
     */
    protected function _insertCategoryTable()
    {
        // Set the current tree depth level for the new category
        $this->_sql->setTable($this->_tableName);
        $action = $this->_sql->select()
            ->where('CATEGORIES.categoryId = '. $this->_submittedData['categoryParentId']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute()->next();
        $this->_categoriesEntity->setTreeDepth((int)$result['treeDepth']+1);
        // Set the current position for the new category ( when added it will pe added at the bottom )
        $action = $this->_sql->select()
            ->where('CATEGORIES.categoryParentId = '. $this->_submittedData['categoryParentId']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $position = 0;
        foreach ($result as $res) {
            if ((int)$res['position'] > $position) {
                $position = (int)$res['position'];
            }
        }
        $this->_categoriesEntity->setPosition(++$position);
        //Set the status of the new category
        $this->_categoriesEntity->setStatus($this->_submittedData['status']);
        //Set the category Parent Id
        $this->_categoriesEntity->setCategoryParentId($this->_submittedData['categoryParentId']);
        //Set the created and modified
        $this->_categoriesEntity->setModified(date("Y-m-d H:i:s"));
        $this->_categoriesEntity->setCreated(date("Y-m-d H:i:s"));

        // Insert the data in CATEGORIES TABLE
        $data = $this->_hydrator->extract($this->_categoriesEntity);
        $action = $this->_sql->insert();
        $action->values($data);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
        return $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
    }
    /**
     *
     */
    protected function _insertCategoryDefinitionTable($categoryId)
    {
        $this->_sql->setTable($this->_categoriesDefinitionsTable);
        // Set the category id
        $this->_categoriesDefinitionsEntity->setCategoryId($categoryId);
        // Set the category name
        $this->_categoriesDefinitionsEntity->setCategoryName($this->_submittedData['categoryName']);
        // Set Meta Title
        $this->_categoriesDefinitionsEntity->setMetaTitle($this->_submittedData['metaTitle']);
        // Set Meta Description
        $this->_categoriesDefinitionsEntity->setMetaDescription($this->_submittedData['metaDescription']);
        // Set Meta Keyword
        $this->_categoriesDefinitionsEntity->setMetaKeyword($this->_submittedData['metaKeyword']);
        // Set Short Description
        $this->_categoriesDefinitionsEntity->setShortDescription($this->_submittedData['shortDescription']);
        // Set Long Description
        $this->_categoriesDefinitionsEntity->setLongDescription($this->_submittedData['longDescription']);
        // Set status
        $this->_categoriesDefinitionsEntity->setStatus($this->_submittedData['status']);
        // Set urlName
        $this->_categoriesDefinitionsEntity->setUrlName($this->_submittedData['urlName']);
        //Set the created and modified
        $this->_categoriesDefinitionsEntity->setModified(date("Y-m-d H:i:s"));
        $this->_categoriesDefinitionsEntity->setCreated(date("Y-m-d H:i:s"));

        // Set languages and save
        $categoryDefinitionsIds = array();
        foreach ($this->_languages as $language) {
            $this->_categoriesDefinitionsEntity->setLanguageId($language);
            // Extract data from the entity
            $data = $this->_hydrator->extract($this->_categoriesDefinitionsEntity);
            // Save the data
            $action = $this->_sql->insert();
            $action->values($data);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
            $categoryDefinitionsIds[] = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
        }
        return $categoryDefinitionsIds;
    }
    /**
     *  Insert tags into categories
     * @throw \Exception
     */
    protected  function _insertCategoryTags($categoryDefinitionsIds)
    {
        $this->_sql->setTable($this->_categoriesTagsTable);
        // Set Tag alias
        $this->_categoriesTagsEntity->setStatus($this->_submittedData['status']);

        // Set Created and Modified
        $this->_categoriesTagsEntity->setModified(date("Y-m-d H:i:s"));
        $this->_categoriesTagsEntity->setCreated(date("Y-m-d H:i:s"));

        // Foreach Category definitions and foreach tag save in database
        foreach ($categoryDefinitionsIds as $categoryDefinitionsId) {
            if (!empty($this->_submittedData['tagCategories'])) {
                foreach ($this->_submittedData['tagCategories'] as $tag) {
                    // Insert the data into the Categories Tag Entity
                    $this->_categoriesTagsEntity->setCategoriesDefinitionsId($categoryDefinitionsId);
                    $this->_categoriesTagsEntity->setAlias($tag);

                    // Extract the data and insert in database
                    $data = $this->_hydrator->extract($this->_categoriesTagsEntity);
                    $action = $this->_sql->insert();
                    $action->values($data);
                    $statement = $this->_sql->prepareStatementForSqlObject($action);
                    $statement->execute();
                }
            }
        }
    }
    /**
     * Saves in the relation table for which website the category will be visible for
     */
    protected function _insertCategoryWebsites($categoryId)
    {
        $this->_sql->setTable($this->_categoriesWebsitesTable);
        // Set the category id
        $this->_categoriesWebsitesEntity->setCategoryId($categoryId);

        // Iterate all websites selected and save them into the database
        foreach ($this->_submittedData['websites'] as $website) {
            $this->_categoriesWebsitesEntity->setWebsiteId($website);
            $data = $this->_hydrator->extract($this->_categoriesWebsitesEntity);
            $action = $this->_sql->insert();
            $action->values($data);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        }
    }


    /**
     * Save image id  for category page banner
     */
    protected function _insertCategoryImageBanner($categoryId)
    {
        // Insert values
        $data = array('categoryId' => $categoryId,
                    'imageId' => $this->_submittedData['categoryBannerImage'],
                    'mediaPriority' => 1,
                    'created' => date("Y-m-d H:i:s"),
                    'modified' => date("Y-m-d H:i:s")
        );

        $this->_sql->setTable($this->_categoriesMedia);
        $action = $this->_sql->insert();
        $action->values($data);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        if($statement->execute()){
            $this->_clearCategoryCache($categoryId);
        }
    }


    public function _updateCategoryImageBanner($categoryId)
    {

        // Check if current row exist
        // If so, update row, else insert new one
        if (count($this->getCategoryBannerMedia($categoryId)) > 0) {
            // Insert values
            $data = array('categoryId' => $categoryId,
                'imageId' => $this->_submittedData['categoryBannerImage'],
                'mediaPriority' => 1,
                'created' => date("Y-m-d H:i:s"),
                'modified' => date("Y-m-d H:i:s")
            );

            $update = $this->_sql->update();
            $update->table($this->_categoriesMedia);
            $update->set($data);
            $update->where(array(
                'categoryId' => $categoryId,
            ));

            $statement = $this->_sql->prepareStatementForSqlObject($update);
            if($statement->execute()){
                $this->_clearCategoryCache($categoryId);
            }
        } else {
            $this->_insertCategoryImageBanner($categoryId);
        }


    }

    public function getCategoryBannerMedia($categoryId)
    {
        // Get the category id from the category name
        $response = array();
        $this->_sql->setTable($this->_categoriesMedia);
        $action = $this->_sql->select()
            ->where('categoryId = '.$categoryId.' ');

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        foreach ($result as $res) {
            if (isset($res['categoryId'])) {
                $response[] = $res;
            }
        }

        return $response;
    }


    /**
     *  Used to parse all categories and return the data need to build the hierarchy
     * @param $allCategories
     * @param $search
     * @return array
     */
    public function processGroupCategories($allCategories, $search = '')
    {
        $allCategories = $this->_getAdditionalInformationForCategories($allCategories);

        $groups = array();
        foreach ($allCategories as $group) {
            if ($group['categoryId'] != '1') {
                $groups[$group['categoryId']] = $group;
                if (!empty($search)) {
                    $groups[$group['categoryId']]['children'] = null;
                }
            }

        }
        if (!empty($groups) && empty($search)) {
            $finalArray = $this->_processGroupChildren($groups, 1);
        } else {
            $finalArray = $groups;
        }
        return $finalArray;
    }

    /**
     * @param $groups
     * @param int $parentKey
     * @return array|null
     */
    protected function _processGroupChildren($groups, $parentKey = 1)
    {
        $return = array();
        // Traverse the tree and search for direct children of the root

        foreach($groups as $categoryGroupId => $group) {
            // A direct child is found
            if($group['categoryParentId'] == $parentKey) {
                // Remove item from tree (we don't need to traverse this again)
                $newArrayElement = $group;
                unset($groups[$categoryGroupId]);
                // Append the child into result array and parse its children
                $newArrayElement['children'] = $this->_processGroupChildren($groups, $newArrayElement['categoryId']);
                $return[] = $newArrayElement;
            }
        }
        return empty($return) ? null : $return;
    }

    /**
     *  Parse the categories and get additional information ( Tags and Websites )
     * @param $allCategories
     * @return array
     */
    protected function _getAdditionalInformationForCategories($allCategories)
    {
        $newCategories = array();
        foreach ($allCategories as $category) {
            // Get the website for the $category
            $category['websites'] = array();
            $this->_sql->setTable(array('cw' => $this->_categoriesWebsitesTable));
            $action = $this->_sql->select();

            $action->join(
                array('cm' => $this->_categoriesMedia), //Set join table and alias
                new Expression('cm.categoryId = cw.categoryId'),
                '*',
                Select::JOIN_LEFT
            );

            $action->where('cw.categoryId = "'.$category['categoryId'].'"');

            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();
            foreach ($result as $res) {
                $category['websites'][] = $res;
            }
            // Get the Tags for the category
            $category['tags'] = array();
            $this->_sql->setTable($this->_categoriesTagsTable);
            $action = $this->_sql->select()
                ->where('CATEGORIES_Tags.categoriesDefinitionsId = "'.$category['categoryDefinitionsId'].'"');
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();

            foreach ($result as $res) {
                $category['tags'][] = $res;
            }
            $newCategories[] = $category;

        }
        return $newCategories;
    }

    /**
     * Returns all websites
     * @return object
     */
    public function fetchAllWebsites()
    {
        return $this->_websites;
    }

    /**
     * Returns default website
     * @return int
     */
    public function fetchDefaultWebsite()
    {
        return key($this->_defaultWebsite);
    }

    /**
     * @param mixed $submittedData
     */
    public function setSubmittedData($submittedData)
    {
        $this->_submittedData = $submittedData;
    }

    /**
     * Retrieve marketing categories.
     *
     * @param int|\PrismProductsManager\Model\type $parentId
     * @param int $treeDepth
     * @param int $position
     * @return type
     */
    public function fetchMarketingCategories($parentId = 1, $treeDepth = 1, $position = 1, $excludeRootCategory = false, $includeTreeDepth = true)
    {
        $returnArray = array('data' =>array());
        $this->_sql->setTable($this->_tableName);
        $select = $this->_sql->select();
            $select->columns(array('categoryId', 'treeDepth', 'position'));
        $select->where(array(
            'categoryParentId' => $parentId,
            'cdt.status' => 'ACTIVE',
            'cdt.languageId' => $this->_currentLanguage,
        ));
        if ($includeTreeDepth === true) {
            $select->where(array('treeDepth' => $treeDepth));
        }
        $select->join(
            array('cdt' => $this->_categoriesDefinitionsTable), //Set join table and alias
            "{$this->_tableName}.categoryId = cdt.categoryId",
            array("categoryName")
        );
        $select->order(array("cdt.categoryName ASC"));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($parentId == 1) {
            $returnArray['data'][0] = 'Choose category';
        }
        foreach ($results as $key => $value) {
            if ($excludeRootCategory === true) { //Remove the root category
                if ($value['categoryId'] == 1) {
                    continue;
                }
            }
            $returnArray['data'][$value['categoryId']]['categoryName'] = ucfirst(html_entity_decode($value['categoryName'], ENT_QUOTES));
            $returnArray['data'][$value['categoryId']]['treeDepth'] = $value['treeDepth'];
            $returnArray['data'][$value['categoryId']]['position'] = $value['position'];
            $returnArray['data'][$value['categoryId']]['childrenCount'] = $this->categoryChildrenCount($value['categoryId']);
        }
        $returnArray['dataCount'] = $results->count();
        $returnArray['childrenCount'] = $this->categoryChildrenCount($parentId);

        return $returnArray;
    }

    /**
     * Check if children exist so we know if we need to display another dropdown
     *
     * @param type $parentId
     * @return type
     */
    public function categoryChildrenCount($parentId)
    {
        $this->_sql->setTable($this->_tableName);
        $select = $this->_sql->select();
        $select->where(array('categoryParentId' => $parentId, 'status' => 'ACTIVE'));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results->count();
    }

    /**
     * Method used to enable Marketing categories
     * @param $params
     */
    public function enableCategory($params)
    {
        $navigationDataUpdate = array(
            'source' => 'category',
            'title' => '',
            'href' => '',
            'id' => $params['id'],
        );

        if ($params['checked'] == '0') {
            $status = 'INACTIVE';
            $navigationDataUpdate['action'] = 'suspend';
        } else {
            $status = 'ACTIVE';
            $navigationDataUpdate['action'] = 'activate';
        }

        $data = array(
            'status' => $status
        );
        $update = $this->_sql->update();
        $update->table($this->_categoriesDefinitionsTable);
        $update->set($data);
        $update->where(array(
            'categoryId' => $params['id'],
            'languageId' => $this->_currentLanguage
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();

        $update = $this->_sql->update();
        $update->table($this->_tableName);
        $update->set($data);
        $update->where(array(
            'categoryId' => $params['id'],
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();

        if ($params['checked'] == '1') {
            // Update the set filter flag for all parent of the group
            $this->_updateFilterFlagForParent($params['id'], $data);
        }

        // Update the set filter flag for all children of the group
        $this->_updateFilterFlagForChildren($params['id'], $data);

        $this->_updateNavigationService->synchronizeNavigation($navigationDataUpdate);
    }

    /**
     * Method used to lock / unlock Marketing categories
     * @param $params
     * @return int
     */
    public function changeLockCategory($params)
    {
        $data = array();
        if ($params['locked'] == '1') {
            $data['locked'] = 0;
        } else {
            $data['locked'] = 1;
        }
        $update = $this->_sql->update();
        $update->table($this->_categoriesDefinitionsTable);
        $update->set($data);
        $update->where(array(
            'categoryId' => $params['id'],
            'languageId' => $this->_currentLanguage
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        try {
            $result = $statement->execute();
        } catch (\Exception $e) {
            die('Error: ' . $e->getMessage());
        }
        return $result->getAffectedRows();
    }

    /**
     * $validateSameParent flag is used to validate if the new category being created
     * has the same parent as an existing category
     *
     * @param $submitedFormData
     * @return bool
     */
    public function fetchMarketingCategory($submitedFormData, $validateSameParent = false)
    {
        $this->_sql->setTable($this->_tableName);
        $action = $this->_sql->select()
            ->columns(array('*'))
            ->join(
                array('cd' => 'CATEGORIES_Definitions'),
                'CATEGORIES.categoryId = cd.categoryId'
            )
            ->where('cd.languageId = '. $this->_currentLanguage);
            if ($validateSameParent === true) {
                $action->where
                    ->nest
                    ->equalTo('CATEGORIES.categoryId', $submitedFormData['categoryId'])
                            ->or
                            ->equalTo('cd.categoryName', $submitedFormData['categoryName'])
                    ->unnest;
            } else {
                $action->where
                    ->nest
                    ->equalTo('CATEGORIES.categoryId', $submitedFormData['categoryId'])
                            ->or
                            ->equalTo('cd.urlName', $submitedFormData['urlName'])
                    ->unnest;
            }
        $action->order('CATEGORIES.position ASC');
        $statement = $this->_sql->prepareStatementForSqlObject($action);

        $result = $statement->execute();

        foreach ($result as $value) {
            if ($validateSameParent === true) {
                if (isset($value['categoryParentId']) && isset($submitedFormData['categoryParentId'])) {
                    //If the category id of the data submitted is equal to an esisting parent id
                    if ($value['categoryParentId'] == $submitedFormData['categoryParentId']) {
                        return true;
                    }
                }
            } else {
                if (($submitedFormData['categoryName'] == $value['categoryName']) || ($submitedFormData['urlName'] == $value['urlName'])){
                    return true;
                }
            }
        }

        return false;
    }

    /**
     *  Method used to update Marketing Products Categories
     */
    public function updateMarketingCategory()
    {
        // Update CATEGORIES Table
        $this->_updateCategoriesTable();
        // Update CATEGORIES Definitions Table
        $catDefId =  $this->_updateCategoriesDefinitionsTable();
        // Update CATEGORIES Tags Table
        $this->_updateCategoriesTagsTable($catDefId);$this->_insertCategoryTags(array($catDefId));
        // Update CATEGORIES R CLIENTS Websites
        $this->_updateCategoriesRClientsWebsites();
        $this->_insertCategoryWebsites($this->_submittedData['categoryId']);

        if ($this->clientConfig['features']['category_image_banner']) {
            $this->_updateCategoryImageBanner($this->_submittedData['categoryId']);
        }
    }

    /**
     * Method to update the categories table
     */
    protected function _updateCategoriesTable()
    {
        $data = array(
            'categoryParentId' => $this->_submittedData['categoryParentId'],
            'modified' => date("Y-m-d H:i:s")
        );
        $update = $this->_sql->update();
        $update->table($this->_tableName);
        $update->set($data);
        $update->where(array(
            'categoryId' => $this->_submittedData['categoryId']
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     * @param $categoryId
     * @return array
     */
    public function getProductsIdsFromCategory($categoryId)
    {
        $products = $this->getProductsForMarketingCategory($categoryId);

        $finalProductsIdsArray = array();
        foreach ($products as $product) {
            $finalProductsIdsArray[] = $product['productId'];
        }
        $finalProductsIdsArray = array_unique($finalProductsIdsArray);

        return $finalProductsIdsArray;
    }

    /**
     * @param $submittedData
     * @return bool
     */
    public function checkIfCategoryHasChangedUrl($submittedData)
    {
        $this->_sql->setTable($this->_categoriesDefinitionsTable);
        $action = $this->_sql->select()
            ->where('languageId = ' . $this->_currentLanguage)
            ->where('categoryId = "' . $submittedData['categoryId'] . '"');

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute()->next();

        if ($result['urlName'] == $submittedData['urlName']) {
            return false;
        } else {
            return true;
        }
    }
    /**
     * Method to update the categories definitions table
     */
    protected function _updateCategoriesDefinitionsTable()
    {
        $this->_sql->setTable($this->_categoriesDefinitionsTable);
        $data = array(
            'categoryName' => $this->_submittedData['categoryName'],
            'urlName' => $this->_submittedData['urlName'],
            'metaTitle' => $this->_submittedData['metaTitle'],
            'metaDescription' => $this->_submittedData['metaDescription'],
            'metaKeyword' => $this->_submittedData['metaKeyword'],
            'shortDescription' => $this->_submittedData['shortDescription'],
            'longDescription' => $this->_submittedData['longDescription'],
            'modified' => date("Y-m-d H:i:s")
        );
        $update = $this->_sql->update();
        $update->table($this->_categoriesDefinitionsTable);
        $update->set($data);
        $update->where(array(
            'categoryId' => $this->_submittedData['categoryId'],
            'languageId' => $this->_currentLanguage
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
        $action = $this->_sql->select()
            ->where('CATEGORIES_Definitions.languageId = '. $this->_currentLanguage)
            ->where('CATEGORIES_Definitions.categoryId = '.$this->_submittedData['categoryId']);

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        return $statement->execute()->next()['categoryDefinitionsId'];

    }

    /**
     * Method used to delete existing Categories Tags
     */
    protected function _updateCategoriesTagsTable($catDefId)
    {
        // Delete the current tags
        $this->_sql->setTable($this->_categoriesTagsTable);
        $action = $this->_sql->delete()
            ->where('CATEGORIES_Tags.categoriesDefinitionsId = ' . $catDefId);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     *  method used to delete existing Categories Websites
     */
    protected function _updateCategoriesRClientsWebsites()
    {
        // Delete the current websites
        $this->_sql->setTable($this->_categoriesWebsitesTable);
        $action = $this->_sql->delete()
            ->where('CATEGORIES_R_CLIENTS_Websites.categoryId = ' . $this->_submittedData['categoryId']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     * Method used to check if category has been used
     * @param $id
     * @return bool
     */
    public function checkMarketingCategoryOnProducts($id)
    {
        $this->_sql->setTable($this->_productsRCategories);
        $action = $this->_sql->select()
            ->where($this->_productsRCategories.'.categoryId = ' . $id)
            ->where($this->_productsRCategories.'.status = "ACTIVE"' );
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        if ( $statement->execute()->count() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if a marketing category is set as default for any product
     * @param type $id
     * @return type
     */
    public function checkIfMarketingCategoryIsDefaultForAnyProduct($id)
    {
        $this->_sql->setTable($this->_productsRCategories);
        $action = $this->_sql->select()
            ->columns(array('productId', 'productIdFrom'))
            ->where($this->_productsRCategories.'.categoryId = ' . $id)
            ->where($this->_productsRCategories.'.isDefault = 1 ')
            ->where($this->_productsRCategories.'.status != "DELETED"' );

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();
        $returnData = array();
        if ($statement->execute()->count() > 0) {
            foreach ($results as $key => $value) {
                $returnData[] = array('productId' => $value['productId'], 'productIdFrom' => $value['productIdFrom']);
            }
        }

        return $returnData;
    }

    public function checkIfMarketingCategoryIsDefaultForProduct($submittedData, $category)
    {
        $this->_sql->setTable($this->_productsRCategories);
        $action = $this->_sql->select()
            ->columns(array('productId', 'productIdFrom'))
            ->where($this->_productsRCategories.'.categoryId = ' . $category)
            ->where($this->_productsRCategories.'.isDefault = 1 ')
            ->where($this->_productsRCategories.'.status != "DELETED"' );

        if (empty($submittedData['variantId'])) {
            $action->where($this->_productsRCategories.'.productIdFrom = "PRODUCTS"' );
            $action->where($this->_productsRCategories.'.productId = ' . $submittedData['productId']  );
        } else {
            $action->where($this->_productsRCategories.'.productIdFrom = "PRODUCTS_R_ATTRIBUTES"' );
            $action->where($this->_productsRCategories.'.productId = ' . $submittedData['variantId']  );
        }

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();
        $returnData = array();
        if ($statement->execute()->count() > 0) {
            foreach ($results as $key => $value) {
                $returnData[] = array('productId' => $value['productId'], 'productIdFrom' => $value['productIdFrom']);
            }
        }

        return $returnData;
    }

    /**
     * Set status to delete for the Marketing Category Products
     * @param $id
     * @throw \Exception
     */
    public function deleteMarketingCategory($id)
    {
        // Change the children position in the hierarchy of the category that will be deleted
        $this->_checkAndChangeChildrenHierarchy($id);

        $data = array('status' => 'DELETED');
        // Set delete status to Categories Definitions Table
        $update = $this->_sql->update();
        $update->table($this->_categoriesDefinitionsTable);
        $update->set($data);
        $update->where(array('categoryId' => $id));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();

        // Set delete status to Categories Table
        $update->table($this->_tableName);
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     * Check and modify the children of the category in the hierarchy
     * If the category has children they will jump one level up in the hierarchy
     * @param $id
     * @throw \Exception
     */
    protected function _checkAndChangeChildrenHierarchy($id)
    {
        // Select the category that will be deleted
        $this->_sql->setTable($this->_tableName);
        $action = $this->_sql->select()
            ->where($this->_tableName.'.categoryId = ' . $id);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $parentId = $statement->execute()->next()['categoryParentId'];

        // Select the category childs of the main category
        $action = $this->_sql->select()
            ->where($this->_tableName.'.categoryParentId =' . $id);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $data = array('categoryParentId' => $parentId);
        if ($parentId == 1) {
            $data['treeDepth'] = 1;
        }
        // Foreach child category update parent to $parentId which
        // is the Parent of the category that will be deleted
        foreach ($result as $res) {
            $update = $this->_sql->update();
            $update->table($this->_tableName);
            $update->set($data);
            $update->where(array(
                'categoryId' => $res['categoryId'],
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            $statement->execute();
        }
    }

    /**
     * Check if the marketing category is locked or not
     * @param $id
     * @return bool
     */
    public function checkMarketingCategoryLocked($id)
    {
        $this->_sql->setTable($this->_categoriesDefinitionsTable);
        $action = $this->_sql->select()
            ->where($this->_categoriesDefinitionsTable.'.categoryId = ' . $id)
            ->where($this->_categoriesDefinitionsTable.'.locked = 1' );
        $statement = $this->_sql->prepareStatementForSqlObject($action);

        if ($statement->execute()->count() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Function to get the ids of all variants of a product
     *
     * @param type $productId
     * @return type
     */
    protected function getVariantIdsByProductId($productId)
    {
        $this->_sql->setTable(array('pvr' => $this->_productsVariantRelationshipTable));
        $select = $this->_sql->select();
        $select->columns(array('productAttributeId'));
        $select->where->notEqualTo('status', 'DELETED');
        $select->where('pvr.productId = ' . $productId);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $variantIds = array();
        foreach ($results as $key => $value) {
            if (isset($value['productAttributeId']) && $value['productAttributeId'] > 0)
            {
                $variantIds[] = $value['productAttributeId'];
            }
        }

        return array_unique($variantIds);
    }

        /**
     * Function to retrieve all categories related to a product
     *
     * @param $productId
     * @param $languageId
     * @param $isVariant
     * @return mixed
     */
    public function getProductCategoriesRelationByProductId($productId, $languageId = 1, $isVariant = 0)
    {
        $languageId = $this->_currentLanguage;
        //Get possible variants for the product
        //If there are variants, the where clause will change
        $variants = array();
        if ($isVariant == 0) {
            $variants = $this->getVariantIdsByProductId($productId);
        }

        $tableName = ($isVariant == 0) ? $this->_productsTable : $this->_productsVariantRelationshipTable;
        $returnArray = array();
        $this->_sql->setTable(array('pcr' => $this->_productsRCategories));
        $select = $this->_sql->select();
        $select->columns(array(
            'categoryId',
            'isDefault',
        ));
        if ($isVariant < 1 || count($variants) < 1) {
            $select->where(array(
                'pcr.productId' => $productId,
                'pcr.productIdFrom' => $tableName,
                'cdef.languageId' => $languageId,
                'pcr.status' => 'ACTIVE'
            ));
        } else {
            $variantsImploded = implode(',', $variants);
            $whereString = '((pcr.productId = ' . $productId .
                    ' AND pcr.productIdFrom = "' . $this->_productsTable .
                    '") OR (pcr.productId IN (' . $variantsImploded .
                    ') AND pcr.productIdFrom = "' . $this->_productsVariantRelationshipTable .
                    '")) AND pcr.status <> "DELETED"';
            $select->where($whereString);
        }
        $select->join(
            array('cdef' => $this->_categoriesDefinitionsTable),
            'pcr.categoryId = cdef.categoryId',
            array('categoryName', 'urlName')
        );
        $select->join(
            array('ct' => $this->_tableName),
            'pcr.categoryId = ct.categoryId',
            array(
                'categoryParentId',
                'treeDepth',
            )
        );
        $select->order('ct.treeDepth ASC');
        $select->group(array('pcr.categoryId', 'cdef.categoryName', 'cdef.urlName'));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $position = 0;
        $defaultCategoryId = 0;
        foreach ($results as $key => $value) {
            //Tree depth of what level a category is on the tree structure
            //If tree depth is greater than 1, we need to get the parent information
            if ($value['treeDepth'] > 1) {
                $categoryParentId = $value['categoryParentId'];
                //$returnArray[$productId][$position]['parent'] = $this->_getMarketingCategoryParentDetailsById($categoryId, $languageId);
                $value['parent'] = $this->_getMarketingCategoryParentDetailsById($categoryParentId, $languageId);
            }
            if ($value['isDefault'] > 0) {
                $defaultCategoryId = $value['categoryId'];
            }
            $returnArray[$productId][$position] = $value;
            $position++;
        }
        if (count($returnArray) > 0) {
            $returnArray['categoryList'] = $this->_buildMarketingCategoriesList($returnArray);
            $returnArray['defaultCategory'] = $defaultCategoryId;
        }
        return $returnArray;
    }

    /**
     * @param array $marketingCategoryArray
     * @return array
     */
    protected function _buildMarketingCategoriesList($marketingCategoryArray = array())
    {
        $returnArray = array();
        if (count($marketingCategoryArray) > 0) {
            $categorySeparator = $this->_categoriesSeparator;
            foreach ($marketingCategoryArray as $productId => $productCategoriesDetails) {
                if (is_array($productCategoriesDetails) && count($productCategoriesDetails) > 0) {
                    $categoryNameListArray = array();
                    $alreadySelected = array();
                    foreach ($productCategoriesDetails as $position => $categoriesDetails) {
                        $categoryNameList = '';
                        $treeDepth = $categoriesDetails['treeDepth'];
                        $alreadySelected[$categoriesDetails['categoryId']] = $categoriesDetails['categoryName'];
                        if ($treeDepth > 1) {
                            $parentCategoriesCount = count($categoriesDetails['parent']);
                            if (isset($categoriesDetails['parent']) && is_array($categoriesDetails['parent']) && $parentCategoriesCount > 0) {
                                foreach ($categoriesDetails['parent'] as $parentCategoryPosition => $parentCategoryDetails) {
                                    if (trim($parentCategoryDetails['categoryName']) != '') {
                                        $categoryNameList .= trim($parentCategoryDetails['categoryName']) . $categorySeparator;
                                    }
                                }
                            }
                        }
                        $categoryNameListArray[$categoriesDetails['categoryId']] = $categoryNameList; // . $lastCategoryName;
                    }
                }
            }

            //Merge the arrays using the categoryId as key.
            if (count($alreadySelected) > 0 && count($categoryNameListArray) > 0) {
                foreach ($categoryNameListArray as $categoryId => $categoryName) {
                    if (isset($alreadySelected[$categoryId])) {
                        $returnArray[$categoryId] = $categoryNameListArray[$categoryId] . $alreadySelected[$categoryId];
                    }
                }
            }
        }
        return $returnArray;
    }


    /**
     * @param $categoryParentId
     * @param $languageId
     * @return array
     */
    protected function _getMarketingCategoryParentDetailsById($categoryParentId, $languageId)
    {
        $returnArray = array();
        do {
            $this->_sql->setTable(array('c' => $this->_tableName));
            $select = $this->_sql->select();
            $select->columns(array(
                'categoryId',
                'categoryParentId',
                'treeDepth'
            ));
            $select->join(
                array('cdef' => $this->_categoriesDefinitionsTable),
                'c.categoryId = cdef.categoryId',
                array('categoryName')
            );
            $select->where(array(
                'c.categoryId' => $categoryParentId,
                'cdef.languageId' => $this->_currentLanguage,
            ));
            $select->where->greaterThan('c.categoryId', 1);
            $select->limit(1);
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            $count = 0;
            foreach ($results as $key => $value) {
                $returnArray[] = $value;
                //$returnArray = $value + $returnArray;
                $categoryParentId = $value['categoryParentId'];
                if (!empty($categoryParentId)) {
                    $count = 1;
                }
            }
            // this is so if we don;t have for some reason translation for a language it will exit the infinite loop
            if($count == 0) {
                break;
            }
        } while ($categoryParentId > 1);

        // this is so we account for the fact that the array might be empty
        if (!empty($returnArray)) {
            //Sort the array in order of the key.
            usort($returnArray, function($a, $b) {
                return $a['treeDepth'] - $b['treeDepth'];
            });
        }
        return $returnArray;
    }

    /**
     * Note : Select the first parent , update the flag, set the param id for the current group, Recursive
     * @param $id
     * @param $data
     * @return bool
     * @throw \Exception
     */
    private function _updateFilterFlagForParent($id, $data)
    {
        $this->_sql->setTable($this->_tableName);
        // Get the current group
        $action = $this->_sql->select()
            ->where($this->_tableName . '.categoryId = ' . $id);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute()->next();
        // Update parent group with flag
        $update = $this->_sql->update();
        $update->table($this->_categoriesDefinitionsTable);
        $update->set($data);
        $update->where(array(
            'categoryId' => $result['categoryParentId'],
        ));
        $update->where(array('status != "DELETED"'));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();

        $update = $this->_sql->update();
        $update->table($this->_tableName);
        $update->set($data);
        $update->where(array(
            'categoryId' => $result['categoryParentId'],
        ));
        $update->where(array('status != "DELETED"'));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();

        $navigationDataUpdate = array(
            'source' => 'category',
            'title' => '',
            'href' => '',
            'id' => $id,
            'action' => 'activate'
        );
        $this->_updateNavigationService->synchronizeNavigation($navigationDataUpdate);

        // If parent of group is top level stop recursive function
        if ($result['categoryParentId'] == $result['categoryId']) {
            return false;
        }
        // Recursive function call
        $this->_updateFilterFlagForParent($result['categoryParentId'], $data);
    }

    /**
     * Note : Select the first children , update the flag, set the param id for the current group, Recursive
     * @param $id
     * @param $data
     * @return bool
     * @throw \Exception
     */
    private function _updateFilterFlagForChildren($id, $data)
    {
        $this->_sql->setTable($this->_tableName);

        // Get the children group and check if the group has children
        $action = $this->_sql->select()
            ->where($this->_tableName . '.categoryParentId = ' . $id);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();

        // If the group dosen't have children kill it
        if (empty($result)) {
            return false;
        }
        foreach ($result as $res) {
            // Recursive function to update all children of $res
            $this->_updateChildrenFlagRecursive($res, $data);
            // Update children group with flag
            $update = $this->_sql->update();
            $update->table($this->_categoriesDefinitionsTable);
            $update->set($data);

            $update->where(array(
                'categoryId' => $res['categoryId'],
            ));
            $update->where('status != "DELETED"');
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            $statement->execute();


            $update = $this->_sql->update();
            $update->table($this->_tableName);
            $update->set($data);

            $update->where(array(
                'categoryId' => $res['categoryId'],
            ));
            $update->where('status != "DELETED"');
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            $statement->execute();

            $navigationDataUpdate = array(
                'source' => 'category',
                'title' => '',
                'href' => '',
                'id' => $res['categoryId'],
            );

            if ($data['status'] == 'INACTIVE') {
                $navigationDataUpdate['action'] = 'suspend';
            } else {
                $navigationDataUpdate['action'] = 'activate';
            }

            $this->_updateNavigationService->synchronizeNavigation($navigationDataUpdate);
        }
    }

    /**
     * Note : Selects all of the children groups
     * Iterate the groups and foreach group set the filter flag and recursive access the function
     * @param $group
     * @param $data
     * @return bool
     * @throw \Exception
     */
    private function  _updateChildrenFlagRecursive($group, $data)
    {
        $this->_sql->setTable($this->_tableName);
        // Get all of group childrens
        $action = $this->_sql->select()
            ->where($this->_tableName . '.categoryParentId = ' . $group['categoryId']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        if (empty($result)) {
            return false;
        }
        // Foreach children of $group loops again in recursive function _updateChildrenFlagRecursive
        foreach ($result as $res) {
            $update = $this->_sql->update();
            $update->table($this->_categoriesDefinitionsTable);
            $update->set($data);
            $update->where(array(
                'categoryId' => $res['categoryId'],
            ));
            $update->where('status != "DELETED"');
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            $statement->execute();

            $update = $this->_sql->update();
            $update->table($this->_tableName);
            $update->set($data);
            $update->where(array(
                'categoryId' => $res['categoryId'],
            ));
            $update->where('status != "DELETED"');
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            $statement->execute();

            $navigationDataUpdate = array(
                'source' => 'category',
                'title' => '',
                'href' => '',
                'id' => $res['categoryId'],
            );

            if ($data['status'] == 'INACTIVE') {
                $navigationDataUpdate['action'] = 'suspend';
            } else {
                $navigationDataUpdate['action'] = 'activate';
            }

            $this->_updateNavigationService->synchronizeNavigation($navigationDataUpdate);

            // Recursiv function call
            $this->_updateChildrenFlagRecursive($res, $data);
        }
    }


    /**
     * @param $categoryId
     * @return array
     */
    public function getProductsForMarketingCategory($categoryId)
    {

        $this->_sql->setTable($this->_productsRCategoriesTable);

        $action = $this->_sql->select()

            ->where($this->_productsRCategoriesTable . '.status != "DELETED"')
            ->where($this->_productsRCategoriesTable . '.categoryId = ' . $categoryId);

        $statement = $this->_sql->prepareStatementForSqlObject($action);

        $result = $statement->execute();

        $products = array();
        foreach ($result as $res) {
            if ($res['productIdFrom'] == 'PRODUCTS_R_ATTRIBUTES') {
                $this->_sql->setTable($res['productIdFrom']);
                $action = $this->_sql->select()
                    ->where('productAttributeId = ' . $res['productId']);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $mainProduct = $statement->execute()->next();

                $products[] = $mainProduct;
            } else {
                $products[] = $res;
            }

        }
        return $products;
    }

    /**
     * @param $categoryId
     * @param $styleColourLength
     * @param string $sku
     * @return array
     */
    public function getProductsInCategory($categoryId, $styleColourLength, $sku = '')
    {
        $this->_sql->setTable(array('prc' => $this->_productsRCategoriesTable));
        $action = $this->_sql->select();
        $action->columns(array('productCategoryId', 'categoryId', 'isDefault', 'priority'));
        $action->join(
            array('p' => $this->_productsTable),
            'p.productId = prc.productId',
            array('style', 'skuMainProduct' => 'sku'),
            Select::JOIN_INNER
        );
        $action->join(
            array('pd' => $this->_productsDefinitionsTable),
            new Expression('p.productId = pd.productId AND pd.productIdFrom = "'.$this->_productsTable.'"'),
            array('shortDescription', 'productName'),
            Select::JOIN_INNER
        );
        $action->join(
            array('pvr' => $this->_productsVariantRelationshipTable),
            'pvr.productId = p.productId',
            array('variantsCount' => new Expression('COUNT(productAttributeId)')),
            Select::JOIN_LEFT
            );
        $action->where(array('prc.categoryId' => $categoryId, 'pd.languageId' => $this->_currentLanguage));
        $action->where->notEqualTo('prc.status', 'DELETED');
        $action->where->notEqualTo('pd.status', 'DELETED');
        $action->where->notEqualTo('p.status', 'DELETED');
        if (trim($sku) != '') {
            $action->where->like('p.sku', "%{$sku}%");
        }
        $action->order(array('prc.priority', 'pd.productName'));
        $action->group(new Expression('LEFT(p.sku, ' . $styleColourLength . ')'));

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $products = array();
        if ($result) {
            foreach ($result as $res) {
                $res['isMainProduct'] = true;
                $this->_sql->setTable(array('pra' => $this->_productsVariantRelationshipTable));
                $select = $this->_sql->select()
                    ->join(
                        array('prc' => $this->_productsRCategoriesTable),
                        new Expression('pra.productAttributeId = prc.productId AND prc.productIdFrom = "PRODUCTS_R_ATTRIBUTES"'),
                        array('*'),
                        Select::JOIN_LEFT
                    );
                $select->where('prc.categoryId = ' . $categoryId);
                $select->where('prc.status = "ACTIVE"');
                $statement = $this->_sql->prepareStatementForSqlObject($select);
                $resCat = $statement->execute();
                if ($resCat->count() > 0) {
                    continue;
                }
                $products[] = $res;
            }
        }

        return $products;
    }


    /**
     * @param $categoryId
     * @param $styleColourLength
     * @param string $sku
     * @return array
     */
    public function getVariantsInCategory($categoryId, $styleColourLength, $sku = '')
    {
        $this->_sql->setTable(array('prc' => $this->_productsRCategoriesTable));
        $action = $this->_sql->select();
        $action->columns(array('productCategoryId', 'categoryId', 'isDefault', 'priority'));
        $action->join(
            array('pvr' => $this->_productsVariantRelationshipTable),
            'pvr.productAttributeId = prc.productId',
            array('productId', 'sku'),
            Select::JOIN_INNER
        );
        $action->join(
            array('p' => $this->_productsTable),
            'p.productId = pvr.productId',
            array('style', 'skuMainProduct' => 'sku'),
            Select::JOIN_INNER
        );
        $action->join(
            array('pd' => $this->_productsDefinitionsTable),
            new Expression('pvr.productId = pd.productId AND pd.productIdFrom = "'.$this->_productsTable.'"'),
            array('shortDescription', 'productName'),
            Select::JOIN_INNER
        );
        $action->where(array('prc.categoryId' => $categoryId, 'pd.languageId' => $this->_currentLanguage, 'prc.productIdFrom' => 'PRODUCTS_R_ATTRIBUTES'));
        $action->where->notEqualTo('prc.status', 'DELETED');
        $action->where->notEqualTo('pd.status', 'DELETED');
        $action->where->notEqualTo('p.status', 'DELETED');
        $action->where->notEqualTo('pvr.status', 'DELETED');
        if (trim($sku) != '') {
            $action->where->like('pvr.sku', "%{$sku}%");
        }
        $action->order(array('prc.priority', 'pd.productName'));
        $action->group(new Expression('IF (pvr.sku IS NOT NULL, LEFT(pvr.sku, ' . $styleColourLength . '), LEFT(p.sku, ' . $styleColourLength . '))'));

//echo $action->getSqlString($this->_dbAdapter->getPlatform()); exit;

        $statement = $this->_sql->prepareStatementForSqlObject($action);

        $result = $statement->execute();

        $products = array();
        if ($result) {
            foreach ($result as $res) {
                $res['isMainProduct'] = false;
                $products[] = $res;
            }
        }

        return $products;
    }

    /**
     * @param $sortArray
     * @param $sortKey
     * @param string $direction
     * @return mixed
     */
    protected function sortMultidimensionalArray($sortArray, $sortKey, $direction = 'ASC')
    {
        $sortKeyArray = array();
        foreach ($sortArray as $key => $row) {
            $sortKeyArray[$key]  = $row[$sortKey];
        }
        if ($direction == 'ASC') {
            array_multisort($sortKeyArray, SORT_ASC, $sortArray);
        } else {
            array_multisort($sortKeyArray, SORT_DESC, $sortArray);
        }

        return $sortArray;
    }

    /**
     * Get products with colours
     *
     * @param type $categoryId
     * @return type
     */
    public function getProductsWithColourForMarketingCategory($categoryId, $styleColourLength = 9, $sku = '', $orderBy = '', $orderByOption = '')
    {
        $consolidatedArray = array();
        //Get the main products in this category
        $mainProducts = $this->getProductsInCategory($categoryId, $styleColourLength, $sku);
        //Get the variants in this category
        $variantProducts = $this->getVariantsInCategory($categoryId, $styleColourLength, $sku);

        //Try to merget the main product and variants in this category
        if (count($mainProducts) > 0) {
            $consolidatedArray = array_merge($consolidatedArray, $mainProducts);
        }
        if (count($variantProducts) > 0) {
            $consolidatedArray = array_merge($consolidatedArray, $variantProducts);
        }

        $products = array();
        if (count($consolidatedArray) > 0) {
            //Get the colour definitions
            $activeColours = $this->getColorDefinitions();
            $variantCounter = 0;
            $styleArray = array();
            foreach ($consolidatedArray as $res) {
                //If the product doesn't have variant, get the sku from the main product
                $res['sku'] = (isset($res['sku']) && $res['sku'] !== null) ? $res['sku'] : $res['skuMainProduct'];
                $styleArray[$res['style']] = $res['style'];
                //Get the variant colour information (Name and colour code)
                $variantColourDetails = $this->getColourNameFromSku($res['sku'], $activeColours);

                $res['variantColour'] = $variantColourDetails['colourName'];
                $res['variantColourCode'] = $variantColourDetails['colourCode'];
                $res['status'] = $this->getAllVariantStatusByStyleColour($res['style'] . $res['variantColourCode']);
                //We want to display only variants and products without variants
                if (isset($res['variantColourCode'])  && (
                                ($res['variantColourCode'] !== null && trim($res['variantColourCode']) != '') ||
                                (trim($res['variantColourCode']) == ''
                                    && isset($res['variantsCount'])
                                    && $res['variantsCount'] < 1
                                    && isset($res['isMainProduct'])
                                    && $res['isMainProduct'] > 0))
                ) {
                   $products[trim($res['style']) . '-' . trim($res['variantColourCode'])] = $res;
                }

            }

            //If no sorting is selected, set a default sort option
            if (trim($orderBy) == '' && trim($orderByOption) == '') {
                $orderBy = 'priority';
                $orderByOption = 'ASC';
            }

            $direction = strtoupper(trim($orderByOption));
            $products = $this->sortMultidimensionalArray($products, $orderBy, $direction);

            $variantCount = count($products);
            $products['variantCount'] = $variantCount;
            $products['productCount'] = count($styleArray);
            //Lets get the default priority from the config file
            $products['defaultPriority'] = isset($this->_siteConfig['global-product-category-settings']['default-product-priority'])
                                        ? $this->_siteConfig['global-product-category-settings']['default-product-priority'] : 1;
        } else {
            $products['variantCount'] = 0;
            $products['productCount'] = 0;
        }

        return $products;
    }

    /**
     * This function checks if at least one variant is active, returns the status as active else returns inactive
     *
     * @param type $styleColour
     * @return type
     */
    public function getAllVariantStatusByStyleColour($styleColour = '')
    {
        if (trim($styleColour) != '') {
            $this->_sql->setTable(array('pvr' => $this->_productsVariantRelationshipTable));
            $action = $this->_sql->select();
            $action->columns(array('status'));
            $action->where->like('pvr.sku', "{$styleColour}%");
            $action->where->notEqualTo('pvr.status', 'DELETED');

            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();
            $variantStatus = array();
            foreach ($result as $value) {
                $variantStatus[] = $value['status'];
            }

            return in_array('ACTIVE', $variantStatus) ? 'ACTIVE' : 'INACTIVE';
        }

        //No style-colour was supplied so return an empty string
        return '';
    }

    /**
     * Retrieve all colours for a product
     *
     * @param type $productId
     * @param type $activeColours
     * @return type
     */
    public function getAllVariantColoursInAProduct($productId, $activeColours)
    {
        $this->_sql->setTable(array('p' => $this->_productsVariantRelationshipTable));
        $action = $this->_sql->select();
        $action->columns(array('sku'));
        $action->where(array('p.productId' => $productId));
        $action->where->notEqualTo('p.status', 'DELETED');

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $variants = array();
        foreach ($result as $res) {
            $variantColourDetails = $this->getColourNameFromSku($res['sku'], $activeColours);
            $variantColour = $variantColourDetails['colourName'];
            if (trim($variantColour) != '') {
                $variants[$res['sku']] = $variantColour;
            }
        }
        $variants = array_unique($variants);

        return $variants;
    }

    /**
     * Function to extract colour name from sku
     * This is based on STYLE+COLOUR+SIZE combination
     * in the order
     * STYLE = 6 characters
     * COLOUR = 3 characters
     * SIZE = 3 characters
     *
     * @todo This function needs to be more generic
     * - This can be achieved by looking at the sku building rule before splitting
     *
     * @param type $sku
     * @param type $colourArray
     * @return type
     */
    public function getColourNameFromSku($sku, $colourArray)
    {
        $skuLength = strlen(trim($sku));
        $defaultSkuLength = $this->_defaultSkuLength;
        $colourCode = '';
        $styleColour = substr($sku, 0, -3);
        $colourCode = substr($styleColour, -3);

        if ($skuLength == 9) {
            $colourCode = substr($sku, -3);
        }

        $colourName = '';
        if (trim($colourCode) != '') {
            if (isset($colourArray[$colourCode]) && trim($colourArray[$colourCode]) != '') {
                $colourName = $colourArray[$colourCode];
            }
        }

        return array('colourName' => $colourName, 'colourCode' => $colourCode);
    }

    /**
     * Get all active colours
     *
     * @return type
     */
    public function getColorDefinitions()
    {
        $this->_sql->setTable(array('cg' => $this->_colourGroupTable));
        $action = $this->_sql->select()
                ->columns(array('groupCode'))
            ->join(
                array('cgd' => $this->_colourGroupsDefinitionTable),
                'cgd.colourGroupId = cg.colourGroupId',
                array('colourName' => 'name'),
                Select::JOIN_INNER
            )
            ->where(array(
                    'cgd.languageId' => $this->_currentLanguage,
                    'cg.enabled' => 1
                )
            );

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $colours = array();
        foreach ($result as $res) {
            $colours[$res['groupCode']] = $res['colourName'];
        }
        return $colours;
    }

    /**
     * Function to remove variants from marketing categories
     *
     * @param type $productCodesArray
     * @param type $categoryId
     * @return type
     */
    public function removeVariantsFromMarketingCategory($productCodesArray = array(), $categoryId = 0)
    {
        $variantIds = array();
        $nonExistingProductsArray = array(); //An array of non-existing products
        $productCodesArrayCount = count($productCodesArray);
        $variantRemovedCount = 0;
        $productsNotFoundInThiscategory = array();
        $productsFoundInThiscategory = array();
        $defaultCategoryProducts = array();
        $elasticSearchQueueData = array();
        if ($productCodesArrayCount > 0 && $categoryId > 0) {
            foreach ($productCodesArray as $productData) {
                if (trim($productData) != '') {
                    // Set delete status to Categories Definitions Table
                    $productIdFrom = $this->_productsVariantRelationshipTable;

                    //Check if the category is set as default for each product
                    $isCategoryDefaultForVariant = $this->isCategoryDefaultForProduct($categoryId, $productIdFrom, $productData);
                    if ($isCategoryDefaultForVariant === true) {
                        $defaultCategoryProducts[] = $productData;
                        continue;
                    } else {
                        $isCategoryDefaultForProduct = $this->isCategoryDefaultForProduct($categoryId, $this->_productsTable, $productData);
                        if ($isCategoryDefaultForProduct === true) {
                            $defaultCategoryProducts[] = $productData;
                            continue;
                        }
                    }

                    //Check the variant table (PRODUCTS_R_ATTRIBUTES) for the supplied style, style-colour or sku
                    $variantIds = $this->getVariantIdsByNameFromVariantTable($productData);
                    $productId = $this->getProductIdFromStyle($productData);

                    //also check the main PRODUCTS table
                    $productIdFrom = $this->_productsTable;
//                    $variantIdsForMainProduct = $this->getVariantIdsByNameFromMainProductTable($productData);

                    $elasticSearchQueueArray = $this->extractProductIdsForIndexing($variantIds);
                    $elasticSearchQueueData = array_merge($elasticSearchQueueData, $elasticSearchQueueArray);

                    //Merge the id arrays retrieved from  varainta ansd main product
//                    $variantIds = array_merge($variantIds, $variantIdsForMainProduct);
                    if (!empty($productId)) {
                        $data = array('status' => 'DELETED');
                        $update = $this->_sql->update();
                        $update->table($this->_productsRCategoriesTable);
                        $update->set($data);
                        $update->where(array(
                            'categoryId' => $categoryId,
                            'productId' => $productId,
                            'productIdFrom' => 'PRODUCTS'
                        ));
                        $statement = $this->_sql->prepareStatementForSqlObject($update);
                        $statement->execute();
                        $productsFoundInThiscategory[] = $productData;
                        $variantRemovedCount++;
                    }
                    if (count($variantIds) > 0) {
                        foreach ($variantIds as $key => $variantId) {
                            $data = array('status' => 'DELETED');
                            $update = $this->_sql->update();
                            $update->table($this->_productsRCategoriesTable);
                            $update->set($data);
                            $update->where(array(
                                'categoryId' => $categoryId,
                                'productId' => $variantId,
                                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES'
                                ));
                            $statement = $this->_sql->prepareStatementForSqlObject($update);
                            $result = $statement->execute();
                            if ($result->count() > 0) {
                                $productsFoundInThiscategory[] = $productData;
                                $variantRemovedCount++;
                            } else {
                                $productsNotFoundInThiscategory[] = $productData;
                                unset($elasticSearchQueueData[$key]); //This product wasn't updated so we don't want to index it.
                            }
                        }

                        //There are instances where a product has been removed from the category but attempt is still made to remove it
                        if (count($productsFoundInThiscategory) > 0 && count($productsNotFoundInThiscategory) > 0) {
                            foreach ($productsNotFoundInThiscategory as  $key => $productsNotFound) {
                                if (in_array($productsNotFound, $productsFoundInThiscategory)) {
                                    if (is_int(array_search($productsNotFound, $productsFoundInThiscategory))) {
                                        unset($productsNotFoundInThiscategory[$key]);
                                    }

                                }
                            }
                        }
                    } else {
                        $nonExistingProductsArray[] = $productData;
                    }
                }
            }

            //Add the product to the queue manager to be indexed
            foreach ($elasticSearchQueueData as $elasticSearchKey => $productOrVariantId) {
                $this->_queueService->addIndexEntryToQueue($elasticSearchKey, ElasticSearchTypes::PRODUCTS);
            }

            return array(
                'productsOrVariantsRemovedCount' => $variantRemovedCount,
                'nonExistingProducts' => array_unique($nonExistingProductsArray),
                'productsNotFoundInThiscategory' => array_unique($productsNotFoundInThiscategory),
                'defaultCategoryProducts' => array_unique($defaultCategoryProducts)
                );
        }
    }
    public function getProductIdFromStyle($style)
    {
        $this->_sql->setTable($this->_productsTable);
        $select = $this->_sql->select()
            ->where('style = "' . $style .'"');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        return $statement->execute()->next()['productId'];
    }
    /**
     * If the variant id is in the queue, we don't want to add include the main productId
     *
     * @param type $variantIdArray
     * @param type $productIdArray
     * @return type
     */
    public function extractProductIdsForIndexing($variantIdArray = array(), $productIdArray = array())
    {
        $returnArray = $variantIdArray;
        if (count($productIdArray) > 0) {
            foreach ($productIdArray as $productId) {
                $match = false;
                $mainProductPattern = $productId . '_';
                if (is_array($variantIdArray) && count($variantIdArray) > 0) {
                    foreach ($variantIdArray as $productIdVariantId => $productOrVariantId) {
                        if (strpos($productIdVariantId, $mainProductPattern) !== false ) {
                            $match = true;
                            break;
                        }
                    }
                }
                if ($match === false) {
                    $returnArray[$mainProductPattern . '0'] = $productId;
                }
            }
        }

        return $returnArray;
    }

    /**
     * Checks if a category is default for a product
     *
     * @param type $categoryId
     * @param type $productIdFrom
     * @param type $sku
     * @return boolean
     */
    public function isCategoryDefaultForProduct($categoryId, $productIdFrom, $sku)
    {
        $this->_sql->setTable(array('prc' => $this->_productsRCategories));
        $action = $this->_sql->select()
            ->columns(array('isDefault', 'productId'));
            if ($productIdFrom == $this->_productsTable) {
                $action->join(
                    array('pif' => $productIdFrom),
                    new Expression('pif.productId = prc.productId'),
                    array('sku'),
                    Select::JOIN_LEFT
                );
            } else {
                $action->join(
                    array('pif' => $productIdFrom),
                    new Expression('prc.productId = pif.productAttributeId'),
                    array('sku'),
                    Select::JOIN_LEFT
                );
            }
            $action->where('prc.categoryId = ' . $categoryId)
            ->where('prc.isDefault = 1 ')
            ->where("prc.productIdFrom ='{$productIdFrom}'")
            ->where('prc.status = "ACTIVE"' )
            ->limit(1);
            $action->where->like('pif.sku', "$sku%");

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();
        $defaultProductCategoriesArray = array();
        if ($statement->execute()->count() > 0) {
            foreach ($results as $key => $value) {
                if (isset($value['isDefault']) && $value['isDefault'] > 0) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param $variantProritiesArray
     * @param $categoryId
     * @return int
     */
    public function updateVariantProductPrioritiesReloaded($variantProritiesArray, $categoryId, $resursiveFlag = true)
    {
        if (is_array($variantProritiesArray) && count($variantProritiesArray) > 0 && $categoryId > 0) {
            $returnArray = array();
            $key = 0;

            foreach ($variantProritiesArray as $categoryId_styleColour => $priority) {
                $categoryId_styleColourExploded = explode('_', $categoryId_styleColour);
                $styleColour = $categoryId_styleColourExploded[1];
                $categoryId = $categoryId_styleColourExploded[0];
                $isVariant = $categoryId_styleColourExploded[2];
                $productOrVariantIds = $this->_getProductOrVariantIdBasedOnStyleColour($styleColour, $isVariant);

                $oldPriority = array();
                if (isset($productOrVariantIds['id'][0]) && !empty($productOrVariantIds['id'][0])) {
                    $oldPriority = $this->_getOldPriorityForVariant($productOrVariantIds['id'][0], $isVariant, $categoryId);
                }
                $returnArray[$key]['old'] = isset($oldPriority['priority']) ? $oldPriority['priority'] : 1;
                $returnArray[$key]['new'] = $priority;
                $returnArray[$key]['product'] = $productOrVariantIds['id'];
                $returnArray[$key]['product-sku'] = $productOrVariantIds['sku'][0];
                if ($oldPriority['priority'] == $priority) {
                    $returnArray[$key]['changed'] = 0;
                } else {
                    $returnArray[$key]['changed'] = 1;
                }
                $returnArray[$key]['isVariant'] = $isVariant;
                $key++;

            }
        }

        $changedOnes = $this->_processTheNewChangedPriorities($returnArray);

        if (empty($changedOnes) && $resursiveFlag) {
            $resursiveFlag = false;
        }

        $unchangedOnes = $this->_processTheOldPriorities($returnArray, $changedOnes);
        $newArray = $changedOnes + $unchangedOnes;

        $return = $this->_updatePriorityWithNewProccessedValues($newArray, $categoryId);

        if ($resursiveFlag) {
            $styleArray = $this->getProductsWithColourForMarketingCategory($categoryId, $styleColourLength = 9, $sku = '');
            $newArray = arraY();
            foreach ($styleArray as $key => $array) {
                $code = str_replace('-','', $key);
                if ($code != 'variantCount' && $code != 'productCount' && $code != 'defaultPriority') {
                    $variant = 1;
                    if (isset($array['variantsCount']) && $array['variantsCount'] == 0) {
                        $variant = 0;
                    }
                    $string = $categoryId.'_'. str_replace('-','', $key) . '_' . $variant;
                    $newArray[$string] = $array['priority'];
                }
            }
            return $this->updateVariantProductPrioritiesReloaded($newArray, $categoryId, false);
        }

        return $return;
    }
    public function _removeChangedOnes($changedOnes, $returnArray)
    {
        foreach ($returnArray as $key => $normalArray) {
            foreach ($changedOnes as $changed) {
                if ($changed['product-sku'] == $normalArray['product-sku']) {
                    // sku has changed
                    unset($returnArray[$key]);
                }
            }
        }

        return $returnArray;
    }
    public function _processTheOldPriorities($returnArray, $changedOnes)
    {
        $return = array();
        foreach ($returnArray as $key => $priorityArray) {

            if ($priorityArray['old'] == $priorityArray['new']) {
                $priorityArray['new'] = $key;
                if ($key == 0) {
                    $priorityArray['new'] = $key+1;
                }
                $return = $this->getNewArrayOld($priorityArray, $return, $changedOnes);
            }
        }
        return $return;
    }

    public function getNewArrayOld($priorityArray, $return, $changedOnes, $recursive = false)
    {

        if (array_key_exists($priorityArray['new'], $return) || array_key_exists($priorityArray['new'], $changedOnes)) {
            $priorityArray['new']++;
            if ($recursive) {
                $return = $this->getNewArrayOld($priorityArray, $return, $changedOnes, true);
            } else {
                $result = $this->getNewArrayOld($priorityArray, $return, $changedOnes, true);
                $return[$result['new']] = $result;
            }
        } else {
            if ($recursive) {
                $return = $priorityArray;
            } else {
                $return[$priorityArray['new']] = $priorityArray;
            }

        }
        return $return;
    }


    public function _processTheNewChangedPriorities($returnArray)
    {
        $return = array();
        foreach ($returnArray as $priorityArray) {
            if ($priorityArray['old'] != $priorityArray['new']) {
                $return = $this->getNewArray($priorityArray, $return);
            }
        }

        return $return;
    }


    public function getNewArray($priorityArray, $return, $recursive = false)
    {

        if (array_key_exists($priorityArray['new'], $return)) {
            $priorityArray['new']++;
                if ($recursive) {
                    $return = $this->getNewArray($priorityArray, $return, true);
                } else {
                    $result = $this->getNewArray($priorityArray, $return, true);
                    $return[$result['new']] = $result;
                }
        } else {
            if ($recursive) {
                $return = $priorityArray;
            } else {
                $return[$priorityArray['new']] = $priorityArray;
            }

        }
        return $return;
    }

    /**
     * @param $returnArray
     * @return mixed
     */
    private function _processTheChangedPriorities($returnArray)
    {
        $flag = false;
        foreach ($returnArray as $key => $priority) {
            if (isset($priority['changed']) && $priority['changed'] != 2) {
                $priority['changed'] = 2;
                $buildNewArray[$priority['new']-1] = $priority;

                unset($returnArray[$key]);
                array_splice($returnArray, $priority['new']-1, 0, $buildNewArray);

                $flag = true;
                break;
            }
        }
        if ($flag) {
            $returnArray = $this->_processTheChangedPriorities($returnArray);
        }

        return $returnArray;
    }

    /**
     * @param $returnArray
     * @return mixed
     */
    private function _proccessAndIncrementPriority($returnArray)
    {
        // Processing and incrementing the priority based on the order that has been applied
        $highestValue = 0;
        foreach ($returnArray as $currentKey => $priority) {
            if (isset($returnArray[$currentKey - 1])) {
                if ($priority['new'] <= $returnArray[$currentKey - 1]['proccessed']) {
                    $returnArray[$currentKey]['proccessed'] = $highestValue + 1;
                    $flag = true;
                } else {
                    $returnArray[$currentKey]['proccessed'] = $priority['new'];
                }
            } else {
                $returnArray[$currentKey]['proccessed'] = $priority['new'];
            }
            $valuesPassed[] = $returnArray[$currentKey]['proccessed'];
            if ($highestValue <= $returnArray[$currentKey]['proccessed']) {
                $highestValue = $returnArray[$currentKey]['proccessed'];
            }
        }


        return $returnArray;
    }

    /**
     * @param $data
     * @param $categoryId
     * @return int
     */
    private function _updatePriorityWithNewProccessedValues($data, $categoryId)
    {
        $updatedPrioritiesCount = 0;
        // Iterate on priorities and save priority for the respective variants or products
        foreach ($data as $priorityArray) {
            $this->_insertNewEntryToTable($priorityArray, $categoryId);
            $update = $this->_sql->update();
            $update->table($this->_productsRCategoriesTable);
            $update->set(array('priority' => $priorityArray['new']));
            $update->where->notEqualTo('status', 'DELETED');
            $update->where(array(
                'categoryId' => $categoryId,
            ));
            if ($priorityArray['isVariant'] == '0') {
                $fromTable = 'PRODUCTS';
            } else {
                $fromTable = 'PRODUCTS_R_ATTRIBUTES';
            }
            $update->where('productIdFrom = "' . $fromTable . '"');
            $update->where->in('productId', $priorityArray['product']);
            $statement = $this->_sql->prepareStatementForSqlObject($update);

            $statement->execute();
            $updatedPrioritiesCount++;
            $this->_updateQueueManagerWithNewPriorities($priorityArray);
        }
        return $updatedPrioritiesCount;
    }

    private function _insertNewEntryToTable($priorityArray, $categoryId)
    {
        if ($priorityArray['isVariant'] == '0') {
            $fromTable = 'PRODUCTS';
        } else {
            $fromTable = 'PRODUCTS_R_ATTRIBUTES';
        }

        $this->_sql->setTable($this->_productsRCategoriesTable);
        foreach ($priorityArray['product'] as $productOrVariant) {
            $select = $this->_sql->select()
                ->where('productId = ' . $productOrVariant)
                ->where('categoryId = ' . $categoryId)
                ->where('status = "ACTIVE"')
                ->where('productIdFrom = "'.$fromTable.'"');
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            if ($result->count() == 0) {
                // Insert new row
                $data = array(
                    'productId' => $productOrVariant,
                    'categoryId' => $categoryId,
                    'isDefault' => 0,
                    'status' => 'ACTIVE',
                    'productIdFrom' => $fromTable,
                    'priority' => 1,
                    'created' => new Expression("NOW()"),
                );

                $this->_sql->setTable($this->_productsRCategoriesTable);
                $action = $this->_sql->insert();
                $action->values($data);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
            }
        }
    }

    /**
     * @param $priorityArray
     * @return bool
     */
    private function _updateQueueManagerWithNewPriorities($priorityArray)
    {
        if ($priorityArray['old'] == $priorityArray['new']) {
            return false;
        }
        if ($priorityArray['isVariant'] == 0) {
            foreach ($priorityArray['product'] as $product) {
                $this->_queueService->addIndexEntryToQueue($product.'_0', ElasticSearchTypes::PRODUCTS);
            }
        } else {
            $productId = $this->getProductIdFromVariantId($priorityArray['product'][0]);
            foreach ($priorityArray['product'] as $product) {
                $this->_queueService->addIndexEntryToQueue($productId . '_' . $product, ElasticSearchTypes::PRODUCTS);
            }
        }
        return true;
    }

    /**
     * @param $variantId
     * @return mixed
     */
    public function getProductIdFromVariantId($variantId)
    {
        $this->_sql->setTable(array('prv' => 'PRODUCTS_R_ATTRIBUTES'));
        $select = $this->_sql->select();
        $select->columns(array('productId'));
        $select->where('productAttributeId = "'.$variantId.'"');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() == 1) {
            return $results->next()['productId'];
        } else {
            return false;
        }
    }
    /**
     * Update variant priorities in marketing categories
     *
     * @param type $variantProritiesArray
     * @param type $categoryId
     * @return int
     */
    public function updateVariantProductPriorities($variantProritiesArray, $categoryId)
    {
        $updatedPrioritiesCount = 0;
        if (is_array($variantProritiesArray) && count($variantProritiesArray) > 0 && $categoryId > 0) {
            foreach ($variantProritiesArray as $categoryId_styleColour => $priority) {
                $categoryId_styleColourExploded = explode('_', $categoryId_styleColour);
                $styleColour = $categoryId_styleColourExploded[1];
                $categoryId = $categoryId_styleColourExploded[0];
                //Get the product ids for this sku
                $productIds = $this->getVariantIdsByNameFromVariantTable($styleColour);

                if (count($productIds) > 0) {
                    foreach ($productIds as $productId) {
                        $data = array('priority' => $priority);
                        $update = $this->_sql->update();
                        $update->table($this->_productsRCategoriesTable);
                        $update->set($data);
                        $update->where->notEqualTo('status', 'DELETED');
                        $update->where(array(
                            'categoryId' => $categoryId,
                            'productId' => $productId,
                            'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES'
                        ));
                        $statement = $this->_sql->prepareStatementForSqlObject($update);
                        if($statement->execute()) {
                            $updatedPrioritiesCount++;
                        }
                    }
                } else {
                    $this->_sql->setTable($this->_productsTable);
                    $select = $this->_sql->select();
                    $select->where->notEqualTo($this->_productsTable .'.status', 'DELETED');
                    $select->where->like($this->_productsTable . '.sku', "$styleColour%");
                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $results = $statement->execute();
                    foreach ($results as $product) {
                        $data = array('priority' => $priority);
                        $update = $this->_sql->update();
                        $update->table($this->_productsRCategoriesTable);
                        $update->set($data);
                        $update->where->notEqualTo('status', 'DELETED');
                        $update->where(array(
                            'categoryId' => $categoryId,
                            'productId' => $product['productId'],
                            'productIdFrom' => 'PRODUCTS'
                        ));
                        $statement = $this->_sql->prepareStatementForSqlObject($update);
                        if($statement->execute()) {
                            $updatedPrioritiesCount++;
                        }
                    }

                }
            }
        }

        return $updatedPrioritiesCount;
    }

    /**
     * Save variants against marketing category.
     * The steps are:
     * 1. Get all variant ids for each style, style-colour or sku supplied
     * 2. For each variant id, check if it has been assigned to the marketing category already
     * 3. Save each variant id against the marketing category
     * 4. Keep an array of style, style-colour or sku that were not found in the system - These will be displyed to the user
     *
     * @param type $productCodesArray
     * @param type $categoryId
     * @return type
     */
    public function saveVariantsForMarketingCategory($productCodesArray = array(), $categoryId = 0)
    {
        $variantIds = array();
        $nonExistingProductsArray = array(); //An array of non-existing products
        $productCodesArrayCount = count($productCodesArray);
        $productOrVariantInsertedCount = 0;
        $productsAlreadyInTheCategory = array();
        $variantNotAlreadyAssigned = array(); //Holds variant that are not already in this category
        $productNotAlreadyAssigned = array(); //Holds products that are not already in this category
        $variantAlreadyAssigned = array(); //Holds variants already in the category
        $productAlreadyAssigned = array(); //Holds products already in the category
        $returnProductsAlreadyInCategory = true;
        $elasticSearchQueueData = array();
        if ($productCodesArrayCount > 0 && $categoryId > 0) {
            foreach ($productCodesArray as $productData) {
                if (trim($productData) != '') {
                    $productIdFrom = $this->_productsVariantRelationshipTable;
                    //Check the variant table (PRODUCTS_R_ATTRIBUTES) for the supplied style, style-colour or sku
                    $variantIds = $this->getVariantIdsByNameFromVariantTable($productData);
                    //Merge the main product and variants
//                    $mergedIds = count($mainProductIds) > 0 && count($variantIds) > 0
//                            ? array_merge($variantIds, $mainProductIds)
//                            : (count($mainProductIds) > 0 ? $mainProductIds : (count($variantIds) > 0 ? $variantIds : array()));

                    if (count($variantIds) > 0) {
                        //Check if the product has been assigned to this category previously
                        //$productNotAlreadyAssigned = $this->checkIfVariantAlreadyAssignedToMarketingCategory($categoryId, $variantIds);
                        $checkIfAlreadyAssigned = $this->checkIfVariantAlreadyAssignedToMarketingCategory($categoryId, $variantIds, $returnProductsAlreadyInCategory);

                        if ($returnProductsAlreadyInCategory === true) {
                           $variantNotAlreadyAssigned = $checkIfAlreadyAssigned['variantIdArray'];
                           $variantAlreadyAssigned = $checkIfAlreadyAssigned['productsAlreadyInTheCategory'];
                        } else {
                            $variantNotAlreadyAssigned = $checkIfAlreadyAssigned;
                        }

                        if (count($variantNotAlreadyAssigned) > 0) {
                            foreach ($variantNotAlreadyAssigned as $variantId) {
                                $productInserted = $this->addProductToMarketingCategory($variantId, $categoryId);
                                $productOrVariantInsertedCount++;
                            }
                        }

                        if ((count($variantAlreadyAssigned) > 0 || count($productAlreadyAssigned) > 0) && !in_array($productData, $productsAlreadyInTheCategory)) {
                            $productsAlreadyInTheCategory[] = $productData;
                        }
                    }

                    //Attempt to get the main product id to insert into the category
                    $productIdFrom = $this->_productsTable;
                    $mainProductIds = $this->getVariantIdsByNameFromMainProductTable($productData);
                    if (count($mainProductIds) > 0) {
                        //Check if the product has been assigned to this category previously
                        //$productNotAlreadyAssigned = $this->checkIfVariantAlreadyAssignedToMarketingCategory($categoryId, $mainProductIds, $returnProductsAlreadyInCategory);
                        $checkIfAlreadyAssigned = $this->checkIfProductAlreadyAssignedToMarketingCategory($categoryId, $mainProductIds, $returnProductsAlreadyInCategory);
                        if ($returnProductsAlreadyInCategory === true) {
                           $productNotAlreadyAssigned = $checkIfAlreadyAssigned['variantIdArray'];
                           $productAlreadyAssigned = $checkIfAlreadyAssigned['productsAlreadyInTheCategory'];
                        } else {
                            $productNotAlreadyAssigned = $checkIfAlreadyAssigned;
                        }

                        if (count($productNotAlreadyAssigned) > 0) {
                            foreach ($productNotAlreadyAssigned as $variantId) {
                                $productInserted = $this->addProductToMarketingCategory($variantId, $categoryId, $productIdFrom);
                                $productOrVariantInsertedCount++;
                            }
                        }

                        if ((count($variantAlreadyAssigned) > 0 || count($productAlreadyAssigned) > 0) && !in_array($productData, $productsAlreadyInTheCategory)) {
                            $productsAlreadyInTheCategory[] = $productData;
                        }
                    }

                    //If no variant id or product id is found, the product might be non-existent
                    if (count($variantIds) < 1 && count($mainProductIds) < 1) {
                        $nonExistingProductsArray[] = $productData;
                    }
                }
            }

            $elasticSearchQueueArray = $this->extractProductIdsForIndexing($variantNotAlreadyAssigned, $productNotAlreadyAssigned);
            $elasticSearchQueueData = array_merge($elasticSearchQueueData, $elasticSearchQueueArray);

            //Add the product to the queue manager to be indexed
            foreach ($elasticSearchQueueData as $elasticSearchKey => $productOrVariantId) {
                $this->_queueService->addIndexEntryToQueue($elasticSearchKey, ElasticSearchTypes::PRODUCTS);
            }

            return array(
                'productOrVariantInsertedCount' => $productOrVariantInsertedCount,
                'nonExistingProducts' => $nonExistingProductsArray,
                'productsAlreadyInTheCategory' => $productsAlreadyInTheCategory);

        }
    }

    /**
     * Check if a variant has been assigned to a marketing category
     * This is a copy of the same function in ProductsMapperVariants - During refactoring they should be merged
     *
     * @param type $categoryId
     * @param type $variantIdArray
     * @param type $returnProductsAlreadyInCategory
     * @return type
     */
    public function checkIfVariantAlreadyAssignedToMarketingCategory($categoryId, $variantIdArray, $returnProductsAlreadyInCategory = false)
    {
        $returnArray = array();
        $this->_sql->setTable(array('prc' => $this->_productsRCategoriesTable));
        $select = $this->_sql->select();
        $select->columns(array('productCategoryId', 'categoryId', 'productId'));
        $select->where->notEqualTo('prc.status', 'DELETED');
        $select->where->in('prc.productId', $variantIdArray);
        $select->where(array('prc.categoryId'=> $categoryId));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $productCategoryId = array();
        $productsAlreadyInTheCategory = array();
        foreach ($results as $value) {
            if ($value['productCategoryId'] !== null) {
                $productCategoryId[] = $value['productId'];
                $key = array_search($value['productId'], $variantIdArray);
                if ($key !== false) {
                    $productsAlreadyInTheCategory = $variantIdArray[$key];
                    unset($variantIdArray[$key]);
                }
            }
        }

        if ($returnProductsAlreadyInCategory === true) {
            return array(
                'productsAlreadyInTheCategory' => $productsAlreadyInTheCategory,
                'variantIdArray' => $variantIdArray
                );
        }
        return $variantIdArray;
    }

    /**
     * Check if a product has been assigned to a marketing category
     * This is a copy of the same function in ProductsMapperVariants - During refactoring they should be merged
     *
     * @param type $categoryId
     * @param type $productIdArray
     * @param type $returnProductsAlreadyInCategory
     * @return type
     */
    public function checkIfProductAlreadyAssignedToMarketingCategory($categoryId, $productIdArray, $returnProductsAlreadyInCategory = false)
    {
        $returnArray = array();
        $this->_sql->setTable(array('prc' => $this->_productsRCategoriesTable));
        $select = $this->_sql->select();
        $select->columns(array('productCategoryId', 'categoryId', 'productId'));
        $select->where->notEqualTo('prc.status', 'DELETED');
        $select->where->in('prc.productId', $productIdArray);
        $select->where->equalTo('productIdFrom', 'PRODUCTS');
        $select->where(array('prc.categoryId'=> $categoryId));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $productCategoryId = array();
        $productsAlreadyInTheCategory = array();
        foreach ($results as $value) {
            if ($value['productCategoryId'] !== null) {
                $productCategoryId[] = $value['productId'];
                $key = array_search($value['productId'], $productIdArray);
                if ($key !== false) {
                    $productsAlreadyInTheCategory = $productIdArray[$key];
                    unset($productIdArray[$key]);
                }
            }
        }

        if ($returnProductsAlreadyInCategory === true) {
            return array(
                'productsAlreadyInTheCategory' => $productsAlreadyInTheCategory,
                'variantIdArray' => $productIdArray
            );
        }
        return $productIdArray;
    }

    /**
     * Get variants ids by sku name
     *
     * @param type $name
     * @return type
     */
    public function getVariantIdsByNameFromVariantTable($name)
    {
        $this->_sql->setTable(array('prc' => $this->_productsVariantRelationshipTable));
        $select = $this->_sql->select();
        $select->columns(array('productAttributeId', 'productId'));
        $select->where->notEqualTo('prc.status', 'DELETED');
        $select->where->like('prc.sku', "$name%");
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $variantIds = array();
        foreach ($results as $value) {
            $variantIds[$value['productId'] . '_' . $value['productAttributeId']] = $value['productAttributeId'];
        }

        return $variantIds;
    }

    /**
     * @param $styleColour
     * @param $isVariant
     * @return array
     */
    private function _getProductOrVariantIdBasedOnStyleColour($styleColour, $isVariant)
    {
        if ($isVariant == 1) {
            $this->_sql->setTable(array('prc' => $this->_productsVariantRelationshipTable));
        } else {
            $this->_sql->setTable(array('prc' => $this->_productsTable));
        }
        $select = $this->_sql->select();

        if ($isVariant == 1) {
            $select->columns(array('productAttributeId', 'sku'));
        } else {
            $select->columns(array('productId', 'sku'));
        }

        $select->where->notEqualTo('prc.status', 'DELETED');
        $select->where->like('prc.sku', "$styleColour%");
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $productOrVariantIds = array();

        $currentKey = 0;
        foreach ($results as $key => $value) {
            if ($isVariant == 1) {
                $productOrVariantIds['id'][$currentKey] = $value['productAttributeId'];
                $productOrVariantIds['sku'][$currentKey] = $value['sku'];
            } else {
                $productOrVariantIds['id'][$currentKey] = $value['productId'];
                $productOrVariantIds['sku'][$currentKey] = $value['sku'];
            }
            $currentKey++;
        }
        return $productOrVariantIds;
    }

    /**
     * @param $productOrVariantId
     * @param $isVariant
     * @param $categoryId
     */
    private function _getOldPriorityForVariant($productOrVariantId, $isVariant, $categoryId)
    {
        $this->_sql->setTable($this->_productsRCategories);
        $select = $this->_sql->select();
        if ($isVariant == 1) {
            $select->where('productIdFrom = "PRODUCTS_R_ATTRIBUTES"');
        } else {
            $select->where('productIdFrom = "PRODUCTS"');
        }
        $select->where('categoryId = ' . $categoryId);
        $select->where('productId = ' . $productOrVariantId);
        $select->where('status = "ACTIVE"');
        $select->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $oldPriority = $results->next();

        return $oldPriority;
    }

    /**
     * Get product ids by sku name
     *
     * @param type $name
     * @return type
     */
    public function getVariantIdsByNameFromMainProductTable($name)
    {
        $this->_sql->setTable(array('p' => $this->_productsTable));
        $select = $this->_sql->select();
        $select->columns(array('productId'));
        $select->where->notEqualTo('p.status', 'DELETED');
        $select->where
            ->nest
            ->like('p.sku', $name . '%')
            ->or
            ->like('p.style', $name . '%')
            ->unnest;
        $select->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $variantIds = array();
        foreach ($results as $value) {
            $variantIds[$value['productId'] . '_0'] = $value['productId'];
        }

        return $variantIds;
    }

    /**
     * @param $search
     * @param $categoryId
     * @return array
     */
    public function getAllProductsFromSearch($search, $categoryId)
    {
        $this->_sql->setTable($this->_productsTable);
        // Get all products using the search parameter

        $action = $this->_sql->select()
            ->join(
                array('prd' => $this->_productsDefinitionsTable),
                new Expression($this->_productsTable . '.productId = prd.productId AND prd.productIdFrom = "'.$this->_productsTable.'"'),
                array('*'),
                Select::JOIN_LEFT
            )
            ->join(
                array('prc' => $this->_productsRCategoriesTable),
                new Expression( $this->_productsTable . '.productId = prc.productId AND prc.status != "DELETED" AND prc.categoryId = ' . $categoryId),
                array('relationStatus' => 'status', 'categoryId' => 'categoryId', 'priority'),
                Select::JOIN_LEFT
            )
            ->where('prd.productName LIKE "%'.$search.'%" ')
            ->where($this->_productsTable . '.status != "DELETED"')
            ->where('prd.languageId = ' . $this->_currentLanguage);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $products = array();
        $activeColours = $this->getColorDefinitions();

        foreach ($result as $res) {
            if ($res['relationStatus'] != 'DELETED' || $res['relationStatus'] !== null) {
                //If the product doesn't have variant, add get the sku from the main product
                $res['sku'] = (isset($res['sku']) && $res['sku'] !== null) ? $res['sku'] : $res['skuMainProduct'];
                $styleArray[$res['style']] = $res['style'];
                //Get the variant colour
                $variantColourDetails = $this->getColourNameFromSku($res['sku'], $activeColours);
                $res['variantColour'] = $variantColourDetails['colourName'];
                $res['variantColourCode'] = $variantColourDetails['colourCode'];
                $res['status'] = $this->getAllVariantStatusByStyleColour($res['style'] . $res['variantColourCode']);
                $products[] = $res;
            }
        }
        return $this->_checkIfProductsAreUsedInCategory($products, $categoryId);
    }

    /**
     * @param $products
     * @param $categoryId
     * @return mixed
     */
    private function _checkIfProductsAreUsedInCategory($products, $categoryId)
    {
        foreach ($products as $key=>$product) {
            if ($product['categoryId'] == $categoryId && $product['relationStatus'] != 'DELETED') {
                $products[$key]['used'] = 1;
            } else {
                $products[$key]['used'] = 0;
            }
        }
        return $products;
    }

    /**
     * @param $productId
     * @param $categoryId
     * @param string $productIdFrom
     * @param int $priority
     */
    public function addProductToMarketingCategory($productId, $categoryId, $productIdFrom = 'PRODUCTS_R_ATTRIBUTES', $priority = 1, $isDefault = '0')
    {
        $data = array(
            'productId' => $productId,
            'categoryId' => $categoryId,
            'isDefault' => $isDefault,
            'status' => 'ACTIVE',
            'productIdFrom' => $productIdFrom,
            'priority' => $priority,
            'created' => new Expression("NOW()"),
            );
        $this->_sql->setTable($this->_productsRCategoriesTable);
        $action = $this->_sql->insert();
        $action->values($data);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        return $result;

    }
    /**
     * @param $categoryId
     * @param $productId
     */
    public function deleteProductFromMarketingCategory($categoryId, $productId)
    {
        $update = $this->_sql->update();
        $update->table($this->_productRCategories);
        $update->set(array('status' => 'DELETED'));
        $update->where(array(
            'categoryId' => $categoryId,
            'productId' => $productId
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     * @param $categoryId
     * @return mixed
     */
    public function getMarketingCategoryById($categoryId)
    {
        $this->_sql->setTable($this->_tableName);
        $action = $this->_sql->select()
            ->join(
                array('cd' => $this->_categoriesDefinitionsTable),
                $this->_tableName . '.categoryId = cd.categoryId',
                array('*'),
                Select::JOIN_LEFT
            )
            ->where('cd.languageId = ' . $this->_currentLanguage)
            ->where('cd.categoryId = ' . $categoryId);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute()->next();
        return $result['categoryName'];
    }

    /**
     * @param string $status
     * @param null $searchFor
     * @param int $page
     * @param null $recordPerpPage
     * @return mixed
     */
    public function fetchMarketingCategoriesByStatus($status= 'ACTIVE', $searchFor = null, $page = 1, $recordPerpPage = NULL)
    {
        if($recordPerpPage === null){
            $recordPerpPage = $this->_recordPerPage;
        }

        $this->_sql->setTable($this->_tableName);
        $action = $this->_sql->select()
        ->columns(array())
        ->join(array($this->_categoriesDefinitionsTable => $this->_categoriesDefinitionsTable),
                "$this->_categoriesDefinitionsTable.categoryId = $this->_tableName.categoryId", array('categoryId', 'categoryName', 'urlName'))

        ->where("$this->_tableName.status = '$status'")
        ->where("$this->_categoriesDefinitionsTable.status = '$status'")
        ->where("$this->_categoriesDefinitionsTable.languageId = $this->_currentLanguage");

        if($searchFor !== null){
            $action->where->like("$this->_categoriesDefinitionsTable.categoryName", "%$searchFor%");
        }
        $paginator = $this->_queryPaginator->getPaginatorForSelect($action, $this->_dbAdapter, $page, $recordPerpPage);
        return $paginator;
    }


    /**
     * @param $categoryId
     * @param $position
     * @param $direction
     * @return bool
     */
    public function updatePositionWithPreviousAvailableCategoryPosition($categoryId, $position, $direction)
    {

        $category = $this->_getMarketingCategoryById($categoryId);
        $lastCategoryPosition = $this->_getLastPositionForCategoryParentId($category);
        $newPosition = $this->_getPreviousAvailableCategoryPosition($category ,$categoryId, $position, $direction, $lastCategoryPosition);

        if (!$newPosition) {
            return false;
        } else {
            $this->_setNewPositionToCategory($categoryId, $newPosition['position']);
            $this->_setNewPositionToCategory($newPosition['categoryId'], $position);
            return true;
        }

    }

    /**
     * @param $category
     * @return mixed
     */
    private function _getLastPositionForCategoryParentId($category)
    {
        $this->_sql->setTable($this->_tableName);
        $select = $this->_sql->select()
            ->where('categoryParentId = ' . $category['categoryParentId'])
            ->where('status != "DELETED"')
            ->order('position DESC');

        $statement = $this->_sql->prepareStatementForSqlObject($select);

        return $statement->execute()->next()['position'];
    }

    /**
     * @param $categoryId
     * @param $position
     */
    private function _setNewPositionToCategory($categoryId, $position)
    {
        $update = $this->_sql->update();
        $update->table($this->_tableName);
        $update->set(array('position' => $position));
        $update->where(array(
            'categoryId' => $categoryId,
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();

    }

    /**
     * @param $category
     * @param $categoryId
     * @param $position
     * @param $direction
     * @param $lastCategoryPosition
     */
    private function _getPreviousAvailableCategoryPosition($category, $categoryId, $position, $direction, $lastCategoryPosition)
    {
        if ($direction == 'up') {
            $tryPosition = $position - 1;
        } else {
            $tryPosition = $position + 1;
        }

        $this->_sql->setTable($this->_tableName);
        $select = $this->_sql->select()
            ->where('categoryParentId = ' . $category['categoryParentId'])
            ->where('status != "DELETED"')
            ->where('position = ' . $tryPosition);
        $statement = $this->_sql->prepareStatementForSqlObject($select);

        $result = $statement->execute()->next();

        if (!$result ) {
            if ($direction == 'up') {
                if($tryPosition > 0) {
                    return $this->_getPreviousAvailableCategoryPosition($category, $categoryId, $tryPosition, $direction, $lastCategoryPosition);
                }
            } else {
                if($position < $lastCategoryPosition-1) {
                    return $this->_getPreviousAvailableCategoryPosition($category, $categoryId, $tryPosition, $direction, $lastCategoryPosition);
                }

            }

        }

        return $result;

    }

    /**
     * @param $categoryId
     */
    public function _getMarketingCategoryById($categoryId)
    {
        $this->_sql->setTable($this->_tableName);
        $select = $this->_sql->select()
            ->join(
                array('cd' => $this->_categoriesDefinitionsTable),
                $this->_tableName . '.categoryId = cd.categoryId'
            )
            ->where($this->_tableName .'.categoryId = ' . $categoryId);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();
        return $result;
    }

    /**
     * @param $categoryId
     * @param int $languageId
     * @return array
     */
    public function getCategoryTags($categoryId, $languageId = 1)
    {
        $this->_sql->setTable(array('c' => $this->_tableName));
        $action = $this->_sql->select()
            ->join(
                array('cd' => $this->_categoriesDefinitionsTable),
                'cd.categoryId = c.categoryId'
            )
            -> join(
                array('ct' => $this->_categoriesTagsTable),
                'ct.categoriesDefinitionsId = cd.categoryDefinitionsId',
                array('alias')
            )
            ->where(array('c.categoryId' => $categoryId))
            ->where(array('cd.languageId' => $this->_currentLanguage));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $tags = array();
        foreach ($result as $res) {
            $tags[] = $res['alias'];
        }
        return $tags;
    }

    /**
     * @param $categoryId
     * @return array
     */
    public function getCategoryWebsites($categoryId)
    {
        $this->_sql->setTable(array('c' => $this->_tableName));
        $action = $this->_sql->select()
            ->join(
                array('cw' => $this->_categoriesWebsitesTable),
                'cw.categoryId = c.categoryId',
                array('websiteId')
            )
            ->where(array('c.categoryId' => $categoryId));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $websites = array();
        foreach ($result as $res) {
            $websites[] = $res['websiteId'];
        }
        return $websites;

    }

    /**
     * @param $categoryId
     * @param $search
     * @return array
     */
    public function getAttributesGroupsForMarketingCategory($categoryId, $search)
    {
        $this->_sql->setTable($this->_attributesGroupsTable);
        $select = $this->_sql->select()
            ->join(
                array('agd' => $this->_attributesGroupDefinitionsTable),
                new Expression($this->_attributesGroupsTable . '.attributeGroupId = agd.attributeGroupId AND agd.languageId = ' . $this->_currentLanguage),
                array('*')
            )
            ->join(
                array('cra' => $this->_categoriesRAttributesTable),
                new Expression($this->_attributesGroupsTable . '.attributeGroupId = cra.attributeGroupId AND cra.status != "DELETED" AND cra.categoryId = ' . $categoryId),
                array('categoryRAttributeId' => 'categoryRAttributeId'),
                Select::JOIN_LEFT
            )
            ->where($this->_attributesGroupsTable . '.groupType = "Core" OR ' . $this->_attributesGroupsTable . '.groupType = "Marketing"')
            ->where('agd.status = "ACTIVE"')
            ->order('agd.name ASC');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $coreAttributes = array();
        $marketingAttributes = array();
        foreach ($result as $res) {
            switch($res['groupType']) {
                case 'Core':
                    $coreAttributes[] = $res;
                    break;
                case 'Marketing':
                    $marketingAttributes[] = $res;
                    break;
            }
        }
        return array('core' => $coreAttributes, 'marketing' => $marketingAttributes);
    }

    /**
     * @param $checked
     * @param $attributeGroup
     * @param $categoryId
     */
    public function addAttributeToMarketingGroup($checked, $attributeGroup, $categoryId)
    {
        switch($checked) {
            case 0:
                $this->_deleteAttributeFromCategory($attributeGroup, $categoryId);
                break;
            case 1:
                $this->_addAttributeToCategory($attributeGroup, $categoryId);
                break;
        }
    }

    /**
     * @param $attributeGroup
     * @param $categoryId
     */
    private function _deleteAttributeFromCategory($attributeGroup, $categoryId)
    {
        $update = $this->_sql->update();
        $update->table($this->_categoriesRAttributesTable);
        $update->set(array('status' => 'DELETED'));
        $update->where(array(
            'categoryId' => $categoryId,
            'attributeGroupId' => $attributeGroup
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     * @param $attributeGroup
     * @param $categoryId
     */
    private function _addAttributeToCategory($attributeGroup, $categoryId)
    {
        $data = array('categoryId' => $categoryId, 'attributeGroupId' => $attributeGroup, 'status' => 'ACTIVE', 'created' => date("Y-m-d H:i:s"));
        $this->_sql->setTable($this->_categoriesRAttributesTable);
        $action = $this->_sql->insert();
        $action->values($data);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     * @param $urlName
     * @param $websiteId
     * @param $languageId
     * @return array
     */
    public function getMarketingCategoryForSideBar($urlName, $websiteId, $languageId)
    {

        $allCategories = $this->fetchAllMarketingCategories('all');
        foreach ($allCategories as $key => $category) {
            if ($category['urlName'] == $urlName) {
                $allCategories[$key]['active'] = 1;
            } else {
                $allCategories[$key]['active'] = 0;
            }
        }
        $hierarchyCategories = $this->processGroupCategories($allCategories);

        return $hierarchyCategories;
    }

    /**
     * @return array
     */
    private function _fetchAllMarketingCategoriesByParentId()
    {
        $this->_sql->setTable($this->_tableName);
        $action = $this->_sql->select()
            ->join(
                array('cd' => 'CATEGORIES_Definitions'),
                'CATEGORIES.categoryId = cd.categoryId'
            )
            ->where('cd.languageId = '. $this->_currentLanguage)
            ->where('cd.status != "DELETED"')
            ->order('CATEGORIES.position ASC');
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        $returnArray = array();
        foreach ($result as $res) {
            $returnArray[] = $res;
        }
        unset($returnArray[0]);
        return $returnArray;
    }

    /**
     * @param $categoryId
     * @return array
     */
    private function _getChildCategoriesForCategoryId($categoryId)
    {
        $this->_sql->setTable($this->_tableName);
        $select = $this->_sql->select()
            ->join(
                array('cd' => $this->_categoriesDefinitionsTable),
                $this->_tableName . '.categoryId = cd.categoryId',
                array('*'),
                Select::JOIN_LEFT
            )
            ->where($this->_tableName . '.categoryParentId = ' . $categoryId)
            ->where('cd.languageId = ' . $this->_currentLanguage);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $returnArray = array();
        foreach ($result as $res) {
            $returnArray[] = $res;
        }

        return $returnArray;
    }

    /**
     * @param $category
     */
    private function _getCategoryTopParent($category)
    {

        $this->_sql->setTable($this->_tableName);
        $select = $this->_sql->select()
            ->join(
                array('cd' => $this->_categoriesDefinitionsTable),
                $this->_tableName . '.categoryId = cd.categoryId',
                array('*'),
                Select::JOIN_LEFT
            )
            ->where($this->_tableName . '.categoryId = ' . $category['categoryParentId']);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();

        if ($result['categoryParentId'] != 1) {
            $return = $this->_getCategoryTopParent($result);
        } else {
            $return = $result;
        }

        return $return;
    }

    /**
     * Get a specific category by the name
     * @param $urlName
     * @param $websiteId
     * @param $languageId
     * @return array
     */
    public function getCategoryByName($urlName, $websiteId, $languageId)
    {
        // Get the category id from the category name
        $this->_sql->setTable($this->_tableName);
        $action = $this->_sql->select()
            ->join(
                array('cd' => $this->_categoriesDefinitionsTable),
                $this->_tableName . '.categoryId = cd.categoryId',
                array('*'),
                Select::JOIN_LEFT
            )
            ->where('cd.urlName = "' . $urlName . '"')
            ->where( 'cd.languageId = "' . $this->_currentLanguage . '"');

        $statement = $this->_sql->prepareStatementForSqlObject($action);


        $result = $statement->execute()->next();
        $categoryId = (int)$result['categoryId'];

        return $result;
    }

    /**
     * @param $data
     * @param $websiteId
     * @param $languageId
     */
    public function getCategoryPageByName($data, $websiteId, $languageId)
    {
        $this->_sql->setTable($this->_tableName);
        $action = $this->_sql->select()
            ->join(
                array('cd' => $this->_categoriesDefinitionsTable),
                $this->_tableName . '.categoryId = cd.categoryId',
                array('*'),
                Select::JOIN_LEFT
            )
            ->join(
                array('crcw' => $this->_categoriesWebsitesTable),
                $this->_tableName . '.categoryId = crcw.categoryId',
                array('*'),
                Select::JOIN_LEFT
            )
            ->where('cd.languageId = ' . $this->_currentLanguage)
            ->where('crcw.websiteId = ' . $websiteId)
            ->where('cd.urlName = "' . $data . '"');

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute()->next();
        return $result;

    }

    /**
     * @param $category
     * @param $type
     * @param $languageId
     * @return array
     */
    public function getParentCategories($category, $type, $languageId)
    {
        $languageId = $this->_currentLanguage; //Override the language id
        $this->_categoriesForElasticSearch = array();
        switch ($type) {
            case 'url':
                $this->_categoriesForElasticSearch[] = $category['urlName'];
                break;
            case 'id':
                $this->_categoriesForElasticSearch[] = $category['categoryId'];
                break;
            case 'name':
                $this->_categoriesForElasticSearch[] = $category['categoryName'];
                break;
            default:
                break;
        }
        $this->_getRecursiveCategoryParent($category, $type, $languageId);
        krsort($this->_categoriesForElasticSearch);
        return array_values($this->_categoriesForElasticSearch);
    }

    /**
     * @param $category
     * @param $type
     * @param $languageId
     */
    private function _getRecursiveCategoryParent($category, $type, $languageId)
    {
        if ($category['categoryParentId'] > 1) {
            $newCategory = $this->getAllParentCategoriesForChildCategoryId($category['categoryParentId'], $languageId);
            $newCategoryRemade = array(
                'categoryParentId' => $newCategory['parentId'],
                'categoryId' => $newCategory['id'],
                'urlName' => $newCategory['url'],
                'categoryName' => $newCategory['name']
            );
            switch ($type) {
                case 'url':
                    $this->_categoriesForElasticSearch[] = $newCategoryRemade['urlName'];
                    break;
                case 'id':
                    $this->_categoriesForElasticSearch[] = $newCategoryRemade['categoryId'];
                    break;
                case 'name':
                    $this->_categoriesForElasticSearch[] = $newCategoryRemade['categoryName'];
                    break;
                default:
                    break;
            }
            $this->_getRecursiveCategoryParent($newCategoryRemade, $type, $languageId);
        }
    }

    /**
     * @param $categoryProductId
     * @param $isVariant
     * @internal param $languageId
     * @return array
     */
    public function getPrioritiesForElasticSearch($categoryProductId, $isVariant)
    {
        $this->_sql->setTable($this->_productsRCategoriesTable);
        $select = $this->_sql->select();
        if ($isVariant == 1) {
            $select->where('productIdFrom = "PRODUCTS_R_ATTRIBUTES"');
        } else {
            $select->where('productIdFrom = "PRODUCTS"');
        }
        $select->where('productId = ' . $categoryProductId);
        $select->where('status != "DELETED"');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $returnArray = array();
        foreach ($results as $result) {
            $returnArray['category_'.$result['categoryId']] = (int)$result['priority'];
        }

        return $returnArray;
    }

    /**
     * @param $categoryId
     * @return string
     */
    protected function _clearCategoryCache($categoryId)
    {
        $category = $this->_getMarketingCategoryById($categoryId);

        $websites =  $this->_siteConfig['websites']['clear-cache'];

        foreach ($websites as $website) {
            $url = sprintf(
                "%sadmin-tasks/clear-category-cache/%s/%s/%s",
                $website,
                $this->_currentWebsiteId,
                $this->_currentLanguage,
                $category['urlName']
            );

            $client = new Client();
            $client->setEncType(Client::ENC_URLENCODED);
            $client->setUri($url);
            $client->setMethod(Request::METHOD_GET);

            $response = $client->send();
            $result = $response->getBody();
            if (!$response->isSuccess()) {
                //@TODO add the action to cron so the cache will be updated later
            }
        }

        return $result;
    }
}
