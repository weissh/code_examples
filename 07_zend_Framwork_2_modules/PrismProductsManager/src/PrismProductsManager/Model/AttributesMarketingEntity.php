<?php
namespace PrismProductsManager\Model;


/**
 * Class AttributesMarketingEntity
 * @package PrismProductsManager\Model
 */
class AttributesMarketingEntity
{

    protected $_attributeId = null;

    protected $_attributeGroupId = null;

      public function __construct()
    {

    }

    /**
     * @param null $attributeGroupId
     */
    public function setAttributeGroupId($attributeGroupId)
    {
        $this->_attributeGroupId = $attributeGroupId;
    }

    /**
     * @return null
     */
    public function getAttributeGroupId()
    {
        return $this->_attributeGroupId;
    }

    /**
     * @param null $attributeId
     */
    public function setAttributeId($attributeId)
    {
        $this->_attributeId = $attributeId;
    }

    /**
     * @return null
     */
    public function getAttributeId()
    {
        return $this->_attributeId;
    }

}