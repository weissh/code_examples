<?php
namespace PrismProductsManager\Model;


class ProductsAttributesCombinationEntity
{

    protected $_productAttributeCombinationId = null;

    protected $_attributeGroupId;
    
    protected $_attributeValue = '';

    protected $_productOrVariantId = null;

    protected $_isVariant = 0;
    
    protected $_created;
    
    protected $_modified;

    protected $_status;
    
    public function __construct()
    {
        $currentDatetime = new \Zend\Db\Sql\Expression("NOW()");
        
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
        $this->_status = 'ACTIVE';
    }
    
    public function getAttributeValue()
    {
        return $this->_attributeValue;
    }

    public function setAttributeValue($_attributeValue)
    {
        $this->_attributeValue = $_attributeValue;
    }
    
    public function getAttributeGroupId()
    {
        return $this->_attributeGroupId;
    }

    public function setAttributeGroupId($_attributeGroupId)
    {
        $this->_attributeGroupId = $_attributeGroupId;
    }

        
    public function getProductAttributeCombinationId()
    {
        return $this->_productAttributeCombinationId;
    }

    public function setProductAttributeCombinationId($_productAttributeCombinationId)
    {
        $this->_productAttributeCombinationId = $_productAttributeCombinationId;
    }
    
    public function getStatus()
    {
        return $this->_status;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }
    
    /**
     * @param mixed $isVariant
     */
    public function setIsVariant($isVariant)
    {
        $this->_isVariant = $isVariant;
    }

    /**
     * @return mixed
     */
    public function getIsVariant()
    {
        return $this->_isVariant;
    }

    /**
     * @param null $productOrVariantId
     */
    public function setProductOrVariantId($productOrVariantId)
    {
        $this->_productOrVariantId = $productOrVariantId;
    }

    /**
     * @return null
     */
    public function getProductOrVariantId()
    {
        return $this->_productOrVariantId;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }

}