<?php

namespace PrismProductsManager\Model;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class ProductsClientsWebsitesEntity
{
    
    protected $_productWebsiteId;
    protected $_productIdFrom;
    protected $_productId;
    protected $_websiteId;
    protected $_elasticSearchIndexId;
    
    public function __construct()
    {
        $this->_elasticSearchIndexId = 0;
    }

        public function getProductWebsiteId()
    {
        return $this->_productWebsiteId;
    }

    public function getProductIdFrom()
    {
        return $this->_productIdFrom;
    }

    public function getProductId()
    {
        return $this->_productId;
    }

    public function getWebsiteId()
    {
        return $this->_websiteId;
    }

    public function getElasticSearchIndexId()
    {
        return $this->_elasticSearchIndexId;
    }

    public function setProductWebsiteId($_productWebsiteId)
    {
        $this->_productWebsiteId = $_productWebsiteId;
    }

    public function setProductIdFrom($_productIdFrom)
    {
        $this->_productIdFrom = $_productIdFrom;
    }

    public function setProductId($_productId)
    {
        $this->_productId = $_productId;
    }

    public function setWebsiteId($_websiteId)
    {
        $this->_websiteId = $_websiteId;
    }

    public function setElasticSearchIndexId($_elasticSearchIndexId)
    {
        $this->_elasticSearchIndexId = $_elasticSearchIndexId;
    }
    
}
