<?php

namespace PrismProductsManager\Model;

class Categories
{
    public $categoryId;
    public $parentId;
    public $overviewImageId;
    public $position;
    public $enabled;
    public $created;
    public $modified;
    
    public function exchangeArray($data)
    {
        $this->categoryId       = (isset($data['categoryId']))     ? $data['categoryId']     : null;
        $this->parentId         = (isset($data['parentId'])) ? $data['parentId'] : null;
        $this->overviewImageId  = (isset($data['overviewImageId']))  ? $data['overviewImageId']  : null;
        $this->position         = (isset($data['position']))  ? $data['position']  : null;
        $this->enabled          = (isset($data['enabled']))  ? $data['enabled']  : null;
        $this->created          = (isset($data['created']))  ? $data['created']  : null;
        $this->modified         = (isset($data['modified']))  ? $data['modified']  : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}