<?php
namespace PrismProductsManager\Model;

use Zend\Db\Adapter\Adapter;
use PrismProductsManager\Model\ProductPricesEntity;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Expression;

class CurrencyMapper
{
    protected $tableName = 'CURRENCY';
    protected $dbAdapter;
    protected $sql;
    protected $productsTable;
    protected $productVariantsTable;
    protected $_dbAdapter;
    protected $productPricesTable;
    protected $prodnos;
    protected $productsUpdated;

    public function __construct(Adapter $dbAdapter, $queryPaginator)
    {
        $this->_queryPaginator = $queryPaginator;
        $this->productsTable = 'PRODUCTS';
        $this->productVariantsTable = 'PRODUCTS_R_ATTRIBUTES';
        $this->productPricesTable ='PRODUCTS_Prices';
        $this->_dbAdapter = $dbAdapter;
        $this->sql = new Sql($dbAdapter);
        $this->sql->setTable($this->tableName);
        $this->sql2 = new Sql($dbAdapter);
        $this->sql2->setTable($this->productPricesTable);
        $this->_availableCurrencies = $this->getAvailableCurrenciesForForm();
    }

    public function getAllCurrencies ()
    {
        $select = $this->sql->select();
        $select->where(array('status' => 'ACTIVE'));
        $select->columns(array(
            'isoCode',
            'conversionRate',
            'plusFixed',
            'threshold',
            'currencyId'
        ));

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $returnArray = array();

        foreach ($results as $value){

            $returnArray[] = array(
                'currency'  => $value['isoCode'],
                'currencyId'  => $value['currencyId'],
                'exchange_rate'     => $value['conversionRate'],
                'plus_fix'=> $value['plusFixed'],
                'threshold' => $value['threshold']
            );

        }

        return $returnArray;
    }

    public function getAvailableCurrenciesForForm ()
    {
        $select = $this->sql->select();
        $select->where(array('status' => 'ACTIVE', 'isDefault' => '0'));
        $select->columns(array(
            'isoCode',
            'conversionRate',
            'plusFixed',
            'threshold',
            'currencyId'
        ));

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $returnArray = array();

        foreach ($results as $value){

            $returnArray[] = array(
                'currency'  => $value['isoCode'],
                'currencyId'  => $value['currencyId'],
                'exchange_rate'     => $value['conversionRate'],
                'plus_fix'=> $value['plusFixed'],
                'threshold' => $value['threshold']
            );

        }

        return $returnArray;
    }

    /**
    * Get all products and there price in all currencies
    * marking all the newly updated products by the array
    * of productIds
    *
    * @param $page
    * @return mixed
    */
    public function getProductList($page = 1)
    {
        $query = $this->_dbAdapter->query("SELECT
         V1.*, MAX(p.modified) lastUpdate
        FROM
         (SELECT
         T1.*
        FROM
         (
          SELECT
           `sel1and2`.*
          FROM
           (
            (
             SELECT
              `p`.`productId`,
              `p`.`sku` AS `sku`,
              'PRODUCTS' AS `productIdFrom`,
              `pp1`.`nowPrice` AS `eurPrice`,
              `pp2`.`nowPrice` AS `usdPrice`,
              `pp3`.`nowPrice` AS `gbpPrice`,
              `pp4`.`nowPrice` AS `audPrice`,
              `pp5`.`nowPrice` AS `cadPrice`
             FROM
              `PRODUCTS` AS `p`
             LEFT JOIN `PRODUCTS_Prices` AS `pp5` ON p.productId = pp5.productOrVariantId
             AND pp5.currencyId = '5'
             AND pp5. STATUS = 'ACTIVE'
             AND pp5.productIdFrom = 'PRODUCTS'
             LEFT JOIN `PRODUCTS_Prices` AS `pp4` ON p.productId = pp4.productOrVariantId
             AND pp4.currencyId = '4'
             AND pp4. STATUS = 'ACTIVE'
             AND pp4.productIdFrom = 'PRODUCTS'
             LEFT JOIN `PRODUCTS_Prices` AS `pp1` ON p.productId = pp1.productOrVariantId
             AND pp1.currencyId = '3'
             AND pp1. STATUS = 'ACTIVE'
             AND pp1.productIdFrom = 'PRODUCTS'
             LEFT JOIN `PRODUCTS_Prices` AS `pp2` ON p.productId = pp2.productOrVariantId
             AND pp2.currencyId = '2'
             AND pp2. STATUS = 'ACTIVE'
             AND pp2.productIdFrom = 'PRODUCTS'
             LEFT JOIN `PRODUCTS_Prices` AS `pp3` ON p.productId = pp3.productOrVariantId
             AND pp3.currencyId = '1'
             AND pp3. STATUS = 'ACTIVE'
             AND pp3.productIdFrom = 'PRODUCTS'
             WHERE
              p. STATUS = 'ACTIVE'
             AND p.productTypeId = '1'
             AND NOT EXISTS (
              SELECT
               1
              FROM
               `PRODUCTS_R_ATTRIBUTES` pra
              WHERE
               pra.productId = p.productId
             )
            )
            UNION
             (
              SELECT
               `sel1and22`.*
              FROM
               (
                (
                 SELECT
                  `p`.`productId`,
                  `p`.`sku` AS `sku`,
                  'PRODUCTS' AS `productIdFrom`,
                  `pp1`.`nowPrice` AS `eurPrice`,
                  `pp2`.`nowPrice` AS `usdPrice`,
                  `pp3`.`nowPrice` AS `gbpPrice`,
                  `pp4`.`nowPrice` AS `audPrice`,
                  `pp5`.`nowPrice` AS `cadPrice`
                 FROM
                  `PRODUCTS` AS `p`
                 LEFT JOIN `PRODUCTS_Prices` AS `pp5` ON p.productId = pp5.productOrVariantId
                 AND pp5.currencyId = '5'
                 AND pp5. STATUS = 'ACTIVE'
                 AND pp5.productIdFrom = 'PRODUCTS'
                 LEFT JOIN `PRODUCTS_Prices` AS `pp4` ON p.productId = pp4.productOrVariantId
                 AND pp4.currencyId = '4'
                 AND pp4. STATUS = 'ACTIVE'
                 AND pp4.productIdFrom = 'PRODUCTS'
                 LEFT JOIN `PRODUCTS_Prices` AS `pp1` ON p.productId = pp1.productOrVariantId
                 AND pp1.currencyId = '3'
                 AND pp1. STATUS = 'ACTIVE'
                 AND pp1.productIdFrom = 'PRODUCTS'
                 LEFT JOIN `PRODUCTS_Prices` AS `pp2` ON p.productId = pp2.productOrVariantId
                 AND pp2.currencyId = '2'
                 AND pp2. STATUS = 'ACTIVE'
                 AND pp2.productIdFrom = 'PRODUCTS'
                 LEFT JOIN `PRODUCTS_Prices` AS `pp3` ON p.productId = pp3.productOrVariantId
                 AND pp3.currencyId = '1'
                 AND pp3. STATUS = 'ACTIVE'
                 AND pp3.productIdFrom = 'PRODUCTS'
                 WHERE
                  p. STATUS = 'ACTIVE'
                 AND p.productTypeId = '1'
                 AND NOT EXISTS (
                  SELECT
                   1
                  FROM
                   `PRODUCTS_R_ATTRIBUTES` pra
                  WHERE
                   pra.productId = p.productId
                 )
                )
                UNION
                 (
                  SELECT
                   `p`.`productAttributeId` AS `productId`,
                   `p`.`sku` AS `sku`,
                   'PRODUCTS_R_ATTRIBUTES' AS `productIdFrom`,
                   `pp1`.`nowPrice` AS `eurPrice`,
                   `pp2`.`nowPrice` AS `usdPrice`,
                   `pp3`.`nowPrice` AS `gbpPrice`,
                   `pp4`.`nowPrice` AS `audPrice`,
                   `pp5`.`nowPrice` AS `cadPrice`
                  FROM
                   `PRODUCTS_R_ATTRIBUTES` AS `p`
                  LEFT JOIN `PRODUCTS_Prices` AS `pp5` ON p.productAttributeId = pp5.productOrVariantId
                  AND pp5.currencyId = '5'
                  AND pp5. STATUS = 'ACTIVE'
                  AND pp5.productIdFrom = 'PRODUCTS_R_ATTRIBUTES'
                  LEFT JOIN `PRODUCTS_Prices` AS `pp4` ON p.productAttributeId = pp4.productOrVariantId
                  AND pp4.currencyId = '4'
                  AND pp4. STATUS = 'ACTIVE'
                  AND pp4.productIdFrom = 'PRODUCTS_R_ATTRIBUTES'
                  LEFT JOIN `PRODUCTS_Prices` AS `pp1` ON p.productAttributeId = pp1.productOrVariantId
                  AND pp1.currencyId = '3'
                  AND pp1. STATUS = 'ACTIVE'
                  AND pp1.productIdFrom = 'PRODUCTS_R_ATTRIBUTES'
                  LEFT JOIN `PRODUCTS_Prices` AS `pp2` ON p.productAttributeId = pp2.productOrVariantId
                  AND pp2.currencyId = '2'
                  AND pp2. STATUS = 'ACTIVE'
                  AND pp2.productIdFrom = 'PRODUCTS_R_ATTRIBUTES'
                  LEFT JOIN `PRODUCTS_Prices` AS `pp3` ON p.productAttributeId = pp3.productOrVariantId
                  AND pp3.currencyId = '1'
                  AND pp3. STATUS = 'ACTIVE'
                  AND pp3.productIdFrom = 'PRODUCTS_R_ATTRIBUTES'
                  WHERE
                   p. STATUS = 'ACTIVE'
                 )
               ) AS `sel1and22`
             )
           ) AS `sel1and2`
         ) T1
        #GROUP BY `T1`.productId ,`T1`.sku , `T1`.productIdFrom
        ) V1
        LEFT JOIN PRODUCTS_Prices p ON V1.productId = p.productOrVariantId
        AND p.productIdFrom = V1.productIdFrom
        AND p.`status` = 'ACTIVE'
        GROUP BY 1,2,3,4,5,6,7,8  ");
        $results = $query->execute();

        $finalArray = array();
        foreach ($results as $result) {
            $finalArray[] = $result;
        }

        if ($page !== false){
            //Limit the query using the _queryPaginator
            $returnArray = $this->_queryPaginator->getPaginatorForArray($finalArray, $page);
        } else {
            $returnArray = $finalArray;
        }
        return $returnArray;
    }

    /**
     *
     * This will use the submited form data to
     * update all international prices
     *
     * @param $submitedData
     * @return mixed
     */
    public function convertPrices($submitedData)
    {
        if (!$submitedData) {
            return false;
        }
        $params = array();
        $currencies = $this->_availableCurrencies;

        foreach ($currencies as $value){
            if (isset($submitedData[$value['currency']])){
                $curr = strtolower($value['currency']);
                $params[strtoupper($curr)] = array(
                    'currencyId' => $value['currencyId'],
                    'plusFix' => (isset($submitedData[$curr.'PlusFix']))? $submitedData[$curr.'PlusFix'] : false,
                    'exchangeRate' => (isset($submitedData[$curr.'Rate']))? $submitedData[$curr.'Rate'] : false,
                    'threshold' => (isset($submitedData[$curr.'Threshold']))? $submitedData[$curr.'Threshold'] : false,
                    'roundup' => (isset($submitedData['roundupRule']))? strtoupper($submitedData['roundupRule']) : false,
                    'roundtofiveornine' => (isset($submitedData['round5or9']))? $submitedData['round5or9'] : false
                );

            }

        }
        return $this->convertProductPrices($params);
    }


    public function getDefaultCurrencyId()
    {
        $select = $this->sql->select();
        $select->where(array('status' => 'ACTIVE', 'isDefault' => '1'));
        $select->columns(array(
            'currencyId'
        ));

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = $results->current();
        return $row['currencyId'];
    }

    public function updateCurrencyData($currecnyId, $plusFix, $conversionRate, $threshold)
    {
        $data = array(
            'plusFixed' => $plusFix,
            'conversionRate' => $conversionRate,
            'threshold' => $threshold
        );

        $update = $this->sql->update();
        $update->set($data);
        $update->where(array('currencyId' => $currecnyId));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();

        return $results->getAffectedRows();
    }

    public function convertProductPrices($params)
    {
        if (!$params) {
            return false;
        }

        foreach ($params as $currency => $values) {
            //lets Save the conversion rate , threshold and plusfix onto the currency table
            $this->updateCurrencyData(
                $values['currencyId'],
                $values['plusFix'],
                $values['exchangeRate'],
                $values['threshold']
            );
        }

        $this->prodnos = array();
        $this->productsUpdated = 0;

        $productPrices = $this->getProductPrices($this->productsTable);
        $productVariantsPrices = $this->getProductPrices($this->productVariantsTable);
        foreach ($productPrices as $productPriceArray) {
            foreach ($params as $currency => $values) {
                // if the product Price is more than the set threshold then proceed with the conversion
                if ($productPriceArray['price'] >= intval($values['threshold'])){
                    $price = $productPriceArray['price'];
                    $convertedPrice = ($price  + $values['plusFix']) * $values['exchangeRate'] ;
                    if ($values['roundup']) {
                        $roundup = $values['roundup'];
                        $convertedPrice = $this->roundNumber($convertedPrice, $roundup, $values['roundtofiveornine']);

                        $productUpdate = $this->updateProductPrice(array(
                            'productOrVariantId' => $productPriceArray['productOrVariantId'],
                            'productIdFrom' => $productPriceArray['productIdFrom'],
                            'currencyId' => $values['currencyId'],
                            'regionId' => $productPriceArray['regionId'],
                            'convertedPrice' => $convertedPrice,
                        ));

                        if ($productUpdate){
                            $this->prodnos[$productPriceArray['productOrVariantId'].'|'.$productPriceArray['productIdFrom']] = $productPriceArray['productOrVariantId'];
                            $this->productsUpdated++;
                        }
                    }
                }
            }
        }

        foreach ($productVariantsPrices as $productPriceArray) {
            foreach ($params as $currency => $values) {
                // if the product Price is more than the set threshold then proceed with the conversion
                if ($productPriceArray['price'] >= intval($values['threshold'])){
                    $price = $productPriceArray['price'];
                    $convertedPrice = ($price  + $values['plusFix']) * $values['exchangeRate'] ;
                    if ($values['roundup']) {
                        $roundup = $values['roundup'];
                        $convertedPrice = $this->roundNumber($convertedPrice, $roundup, $values['roundtofiveornine']);
                        $productUpdate = $this->updateProductPrice(array(
                            'productOrVariantId' => $productPriceArray['productOrVariantId'],
                            'productIdFrom' => $productPriceArray['productIdFrom'],
                            'currencyId' => $values['currencyId'],
                            'regionId' => $productPriceArray['regionId'],
                            'convertedPrice' => $convertedPrice,
                        ));

                        if ($productUpdate){
                            $this->prodnos[$productPriceArray['productOrVariantId'].'|'.$productPriceArray['productIdFrom']] = $productPriceArray['productOrVariantId'];
                            $this->productsUpdated++;
                        }
                    }
                }
            }
        }

        if ($this->productsUpdated > 0) {
            return array(
                'updatedProductsCount' => $this->productsUpdated,
                'updatedProducts'      => $this->prodnos
            );
        } else {
            return false;
        }
    }

    public function updateProductPrice($params)
    {
        $productOrVariantId = (!empty($params['productOrVariantId'])) ? $params['productOrVariantId'] : 0;
        $productIdFrom = (!empty($params['productIdFrom'])) ? $params['productIdFrom'] : 0 ;
        $currencyId = (!empty($params['currencyId'])) ? $params['currencyId'] : 0 ;
        //$regionId = (!empty($params['regionId'])) ? $params['regionId'] : 0 ;
        $convertedPrice = (!empty($params['convertedPrice'])) ? $params['convertedPrice'] : 0 ;

        $select = $this->sql2->select();

        //Removed the regionId because it is unused.
        //'regionId' => $regionId,
        $select->where(array(
            'productOrVariantId' => $productOrVariantId,
            'productIdFrom' => $productIdFrom,
            'currencyId' => $currencyId,
            'status' => 'ACTIVE'
        ));
        $statement = $this->sql2->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results->count() > 1) {
            throw new \Exception("Invalid primary key specified for table PRODUCT_Prices");
        } else if ($results->count() == 1) {
            $productPriceArray = $results->next();
        } else {
            return false;
        }

        // update the current row to inactive
        $update = $this->sql2->update();
        $update->set(array('status' => 'INACTIVE'));
        $update->where(array('productPriceId' => $productPriceArray['productPriceId']));
        $statement = $this->sql2->prepareStatementForSqlObject($update);
        $statement->execute();

        // insert new record
        $dataInsert = array(
            'productIdFrom' => $productPriceArray['productIdFrom'],
            'productOrVariantId' => $productPriceArray['productOrVariantId'],
            'regionId' => $productPriceArray['regionId'],
            'currencyId' => $productPriceArray['currencyId'],
            'price' => $convertedPrice,
            'nowPrice' => $convertedPrice,
            'wasPrice' => $productPriceArray['nowPrice'],
            'costPrice' => $productPriceArray['costPrice'],
            'tradingCostPrice' => $productPriceArray['tradingCostPrice'],
            'wholesalePrice' => $productPriceArray['wholesalePrice'],
            'freightPrice' => $productPriceArray['freightPrice'],
            'dutyPrice' => $productPriceArray['dutyPrice'],
            'designPrice' => $productPriceArray['designPrice'],
            'packagingPrice' => $productPriceArray['packagingPrice'],
            'status' => 'ACTIVE',
            'created' => date('Y-m-d H:i:s')
        );
        $insert = $this->sql2->insert();
        $insert->values($dataInsert);

        $statement = $this->sql2->prepareStatementForSqlObject($insert);
        $statement->execute();

        return true;
    }

    public function getProductPrices($productIdFrom = 'PRODUCTS')
    {
        $defaultCurrency = $this->getDefaultCurrencyId();

        $results = array();
        switch ($productIdFrom) {
            case $this->productsTable:
                $select = $this->sql2->select();
                $select->join(
                    array('p' => $this->productsTable), //Set join table and alias
                    "p.productId = {$this->productPricesTable}.productOrVariantId",
                    array(
                        'status'
                    ));
                $select->where(array(
                    'p.status' => 'ACTIVE',
                    $this->productPricesTable.'.currencyId' => $defaultCurrency,
                    $this->productPricesTable.'.productIdFrom' => $this->productsTable
                ));
                $statement = $this->sql2->prepareStatementForSqlObject($select);
                $results = $statement->execute();
                break;
            case $this->productVariantsTable:
                $select = $this->sql2->select();
                $select->join(
                    array('p' => $this->productVariantsTable), //Set join table and alias
                    "p.productAttributeId = {$this->productPricesTable}.productOrVariantId",
                    array(
                        'status'
                    ));
                $select->where(array(
                    'p.status' => 'ACTIVE',
                    $this->productPricesTable.'.currencyId' => $defaultCurrency,
                    $this->productPricesTable.'.productIdFrom' => $this->productVariantsTable,
                ));
                $statement = $this->sql2->prepareStatementForSqlObject($select);
                $results = $statement->execute();
                break;
        }
        return $results;
    }

    public function productPriceExists($productOrVariantId, $currencyId, $regionId, $productIdFrom){

        $select = $this->sql2->select();
        $select->where(array(
            'productOrVariantId' => $productOrVariantId,
            'currencyId' => $currencyId,
            'regionId' => $regionId,
            'productIdFrom' => $productIdFrom
        ));
        $statement = $this->sql2->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        //debug($results->getAffectedRows()).'<br><br>';
        return $results->count();

    }


    public function roundNumber($number, $roundrule, $fiveOrNine = false)
    {
        if ($fiveOrNine != 'true') {
            if ($roundrule == 'UP') {
                $newNum = ceil($number);
            } elseif ($roundrule == 'DOWN') {
                $newNum = floor($number);
            } else {
                $newNum = round($number);
            }
        } else if ($fiveOrNine == 'true') {
            if (strstr(strval($number),'.') == true) {
                $decimal = explode('.',$number);
                $decimal = $decimal[1];
                //Get first number
                $decimal = $decimal[0];

                if ($roundrule == 'UP') {
                    $number = ceil($number);
                } elseif ($roundrule == 'DOWN') {
                    $number = floor($number);
                } else {
                    $number = ceil($number);
                }
            }

            $newNum = $number;
            $lastnum = str_split(strval($number));
            $lastnum = array_slice($lastnum, -1, 1, true);
            foreach ($lastnum as  $value) {
                $lastnum = $value;
            }

            if ($lastnum > 5 && $lastnum != 9) {
                if ($roundrule == 'UP') {
                    $newNum = substr(strval($number), 0, -1).'9';
                } else if ($roundrule == 'DOWN') {
                    $newNum = substr(strval($number), 0, -1).'5';
                } else {
                    $newNum = substr(strval($number), 0, -1).'9';
                }
            } else if ($lastnum < 5) {
                if ($roundrule == 'UP') {
                    $update = false;
                    while ($update == false) {
                        if (substr(strval($number),-1) == 5 || substr(strval($number),-1) == 9) {
                            $update = true;
                        } else {
                            $number++;
                        }
                    }
                    $newNum = $number;
                } elseif ($roundrule == 'DOWN') {
                    $update = false;
                    while ($update == false) {
                        if (substr(strval($number),-1) == 5 || substr(strval($number),-1) == 9) {
                            $update = true;
                        } else {
                            $number--;
                        }
                    }

                    $newNum = $number;
                } else {
                    if (intval($lastnum) < 3 && intval($lastnum) != 0) {
                        $number = substr(strval($number), 0, -1).'0';
                        $newNum = intval($number) - 1;
                    } elseif (intval($lastnum) < 3 && intval($lastnum) == 0) {
                        $newNum = substr(strval($number), 0, -1).'5';
                    } else if (intval($lastnum) >= 3) {
                        $newNum = substr(strval($number), 0, -1).'5';
                    }
                }
            }
        }
        return $newNum;
    }

    public function outputCSV($data,$filename = '')
    {
        if ($filename == ''){
            return false;
        }
        $outputBuffer = fopen($filename, 'a');
        fputcsv($outputBuffer, $data);
        fclose($outputBuffer);
    }

    /**
     *
     * Export mysql rows to CSV/EXCEL
     *
     * $data = mysql rows
     *
     */
    public function exportDataArray($data, $updatedProducts , $filename = 'Data_Extract'){

        $fieldName = array();
        $limit = count($data[0]);
        $counter = 0;

        // Let get all the key values in array for CSV column names
        foreach ($data[0] as $key => $value) {
            if ($counter < $limit) {
                $fieldName[] = $key;
            } else {
                break;
            }
            $counter++;
        }

        $filename = ROOT_PATH.'/public/uploads/'.$filename . date('Ymd') . ".csv";
        $this->outputCSV($fieldName , $filename);
        foreach($data as $product) {
            if($updatedProducts) {
                if (!in_array($product['productId'],$updatedProducts)) {
                    continue;
                }
            }
            $this->outputCSV($product , $filename);
        }

        $CSV = file_get_contents($filename);
        @unlink($filename);
        if ($CSV) {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
            header("Content-Disposition: attachment;filename=".$filename);
            header("Content-Transfer-Encoding: binary ");

            echo $CSV;
            exit;
        } else {
            echo 'failed';
            exit;
        }
    }
}