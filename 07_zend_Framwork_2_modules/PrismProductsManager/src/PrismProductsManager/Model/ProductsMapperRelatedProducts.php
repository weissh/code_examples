<?php

namespace PrismProductsManager\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
//use Zend\Db\ResultSet\HydratingResultSet;
use PrismProductsManager\Model\ProductsMapper;
use PrismProductsManager\Model\Entity\ProductsRelatedProductsEntity;

/**
 * This class is a sub class of ProductsMapper class.
 * It was created to reduce the length of the parent calss.
 *
 * It holds business logic for related products to a product.
 */
class ProductsMapperRelatedProducts extends ProductsMapper
{

    protected $_sql;
    protected $_hydrator;
    protected $_dbAdapter;
    protected $_productsMapperVariants;

    public function __construct(Sql $sql, $hydrator, Adapter $dbAdapter, $productsMapperVariants)
    {
        $this->_sql = $sql;
        $this->_hydrator = $hydrator;
        $this->_dbAdapter = $dbAdapter;
        $this->_productsMapperVariants = $productsMapperVariants;
    }

    /**
     * Save related product information
     *
     * @param array $params
     * @return int
     */
    protected function _saveRelatedProducts($params)
    {

        $relatedProducts = array();
        $savedRelatedProducts = 0;

        if (isset($params['relatedProduct']) && trim($params['relatedProduct']) != '') {

            $relatedProducts[] = $params['relatedProduct'];
        }

        if (isset($params['relatedProductVariants']) && count($params['relatedProductVariants']) > 0) {

            $relatedProducts = array_merge($relatedProducts, $params['relatedProductVariants']);
        }

        if (count($relatedProducts) > 0) {

            //array to map source table of related product
            $productIdFromArray = array('product' => $this->_tableName, 'variant' => $this->_productsVariantsRelationshipTable);

            $productsRelatedProductsentity = new ProductsRelatedProductsEntity();

            $dataArray = array('productId' => $params['productId']);

            foreach ($relatedProducts as $key => $relatedProductData) {

                if (trim($relatedProductData) != '') {

                    $relatedProductDataExploded = explode('@', $relatedProductData);

                    $dataArray['productIdFrom'] = $productIdFromArray[$relatedProductDataExploded[1]];
                    $dataArray['relatedProductId'] = $relatedProductDataExploded[0];
                    $dataArray['status'] = 'ACTIVE';

                    //Check if the related product exists so we skip saving it again.
                    $relatedProductExist = $this->_checkIfRelatedProductExist($dataArray);
                    if ($relatedProductExist > 0) {
                        continue;
                    }

                    $hydratedData = $this->_hydrator->hydrate($dataArray, $productsRelatedProductsentity);

                    $data = $this->_hydrator->extract($hydratedData);

                    $savedRelatedProduct = $this->_save($data, $this->_productsRelatedProductsTable);
                    if ($savedRelatedProduct > 0) {
                        $savedRelatedProducts++;
                    }
                }
            }
        }

        return $savedRelatedProducts;
    }

    /**
     * Checks if a related product exist
     *
     * @param type $dataArray
     * @return type
     */
    protected function _checkIfRelatedProductExist($dataArray = array())
    {
        $relatedProductId = 0;

        if (count($dataArray) > 0) {

            $this->_sql->setTable(array('pr' => $this->_productsRelatedProductsTable));
            $select = $this->_sql->select();
            $select->columns(array('productProductId'));
            $select->where(array(
                'pr.status' => $dataArray['status'],
                'pr.productIdFrom' => $dataArray['productIdFrom'],
                'pr.productId' => $dataArray['productId'],
                'pr.relatedProductId' => $dataArray['relatedProductId'],
            ));

            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            foreach ($results as $key => $value) {

                $relatedProductId = $value['productProductId'];
            }
        }

        return $relatedProductId;
    }

    /**
     * Get the related products to a product
     *
     * @param $productId
     * @param int $languageId
     * @return type
     */
    public function _getProductsRelatedProducts($productId = 0, $languageId = 1)
    {
        $returnArray = array();

        if ($productId > 0) {
            $this->_sql->setTable(array('pr' => $this->_productsRelatedProductsTable));
            $select = $this->_sql->select();
            $select->columns(array('productProductId', 'productIdFrom', 'productId', 'relatedProductId'));
            $select->where(array(
                'pr.status' => 'ACTIVE',
                'pr.productId' => $productId
            ));
            $select->order(array('pr.productProductId ASC'));

            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            foreach ($results as $key => $value) {

                $tableName = $value['productIdFrom'];
                $value['isVariant'] = true;
                if ($tableName == $this->_tableName) {
                    $value['isVariant'] = false;
                    $relatedProductId = $value['relatedProductId'];
                    $productId = $this->getProductIdFromVariantId($relatedProductId);
                    $value['relatedProduct'] = $this->getProductNameByProductId($productId, $languageId, $value['isVariant']);
                } else if ($tableName == $this->_productsVariantsRelationshipTable) {
                    $relatedVariantId = $value['relatedProductId'];
                    $value['isVariant'] = true;
                    $value['relatedProductVariant'] = $this->getProductNameByProductId($relatedVariantId, $languageId,$value['isVariant']);
                    $value['relatedProduct'] = $value['relatedProductVariant'];
                }
                $returnArray[] = $value;
            }
        }
        return $returnArray;
    }

    public function getVariantsRelatedVariants($variantId, $languageId = 1)
    {
        $returnArray = array();

        if ($variantId > 0) {
            $this->_sql->setTable(array('pr' => $this->_productsRelatedProductsTable));
            $select = $this->_sql->select();
            $select->columns(array('productProductId', 'productIdFrom', 'productId', 'relatedProductId'));
            $select->where(array(
                'pr.status' => 'ACTIVE',
                'pr.productId' => $variantId,
                'pr.productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                'pr.relatedProductIdFrom' => 'PRODUCTS_R_ATTRIBUTES'
            ));
            $select->order(array('pr.productProductId ASC'));

            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            foreach ($results as $key => $value) {
                $value['isVariant'] = true;
                $relatedVariantId = $value['relatedProductId'];
                $value['isVariant'] = true;
                $value['relatedProductVariant'] = $this->getProductNameByProductId($relatedVariantId, $languageId,$value['isVariant']);
                $value['relatedProduct'] = $value['relatedProductVariant'];
                $returnArray[] = $value;
            }
        }
        return $returnArray;
    }

    /**
     * Retrieve the product name based on the product id
     *
     * @param type $productId
     * @param int $languageId
     * @param boolean $isVariant
     * @return type
     */
    public function getProductNameByProductId($productId, $languageId, $isVariant = false)
    {
        $productIdFrom = 'PRODUCTS_R_ATTRIBUTES';

        if($isVariant == false)
        {
            $productIdFrom = 'PRODUCTS';
        }
        $relatedProduct = '';
        $this->_sql->setTable(array('t' => $this->_productsDefinitionsTable));
        $select = $this->_sql->select();
        $select->columns(array('relatedProduct' => 'productName'));
        $select->where(array(
            't.languageId' => $languageId,
            't.productId' => $productId,
            'productIdFrom' => $productIdFrom
        ));
        $select->limit(1);

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            $relatedProduct = isset($value['relatedProduct']) ? $value['relatedProduct'] : '';
        }

        return $relatedProduct;
    }

    /**
     * Delete related product
     *
     * @param array $productData
     * @return type
     */
    protected function _deleteRelatedProduct($productData)
    {
        //Default table for product
        $productIdFrom = 'PRODUCTS';
        if ($productData['isVariant'] > 0) {
            //Override the default table if it's a variant
            $productIdFrom = 'PRODUCTS_R_ATTRIBUTES';
        }
        $whereClause = array(
            'productId' => $productData['productId'],
            'relatedProductId' => $productData['relatedProductId'],
            'productIdFrom' => $productIdFrom
        );
        $table = $this->_productsRelatedProductsTable;
        $fieldsToUpdate = array('status' => 'DELETED');
        $relatedProductDeleted = $this->_update($fieldsToUpdate, $table, $whereClause);
        return $relatedProductDeleted;
    }

}
