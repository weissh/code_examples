<?php

namespace PrismProductsManager\Model;

/**
 * Class CategoriesTagsEntity
 * @package PrismProductsManager\Model
 */
class CategoriesTagsEntity
{
    /**
     * @var
     */
    protected $_categoriesTagId;

    /**
     * @var
     */
    protected $_categoriesDefinitionsId;

    /**
     * @var
     */
    protected $_alias;

    /**
     * @var
     */
    protected $_status;

    /**
     * @var
     */
    protected $_created;

    /**
     * @var
     */
    protected $_modified;

    /**
     * @param mixed $alias
     */
    public function setAlias($alias)
    {
        $this->_alias = $alias;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this->_alias;
    }

    /**
     * @param mixed $categoriesDefinitionsId
     */
    public function setCategoriesDefinitionsId($categoriesDefinitionsId)
    {
        $this->_categoriesDefinitionsId = $categoriesDefinitionsId;
    }

    /**
     * @return mixed
     */
    public function getCategoriesDefinitionsId()
    {
        return $this->_categoriesDefinitionsId;
    }

    /**
     * @param mixed $categoriesTagId
     */
    public function setCategoriesTagId($categoriesTagId)
    {
        $this->_categoriesTagId = $categoriesTagId;
    }

    /**
     * @return mixed
     */
    public function getCategoriesTagId()
    {
        return $this->_categoriesTagId;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->_created = $created;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->_created;
    }

    /**
     * @param mixed $modified
     */
    public function setModified($modified)
    {
        $this->_modified = $modified;
    }

    /**
     * @return mixed
     */
    public function getModified()
    {
        return $this->_modified;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->_status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->_status;
    }


}
