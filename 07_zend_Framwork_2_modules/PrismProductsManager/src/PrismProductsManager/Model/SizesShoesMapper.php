<?php
namespace PrismProductsManager\Model;

use Zend\Db\Adapter\Adapter;
use PrismProductsManager\Model\ColourEntity;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;


class SizesShoesMapper
{
    protected $tableName = 'SIZES_Shoes';
    protected $dbAdapter;
    protected $sql;

    protected $_dbAdapter;

    public function __construct(Adapter $dbAdapter)
    {
        $this->_dbAdapter = $dbAdapter;
        $this->sql = new Sql($dbAdapter);
        $this->sql->setTable($this->tableName);
    }

    public function fetchAll ()
    {
        $select = $this->sql->select();

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getValueById ($id)
    {
        $id = (int) $id;

        $select = $this->sql->select();
        $select->where('sizeId ='. $id);

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $id = array();
        foreach ($results as $result) {
            $id = $result;
        }

        return $id;
    }



}