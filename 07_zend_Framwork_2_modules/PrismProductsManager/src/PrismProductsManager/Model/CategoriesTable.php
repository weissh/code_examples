<?php

namespace PrismProductsManager\Model;

use Zend\Db\TableGateway\TableGateway;

class CategoriesTable {


    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getCategories($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('categoryId' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function deleteCategories($id)
    {
        $this->tableGateway->delete(array('categoryId' => $id));
    }

    /**
     * @return \Zend\Db\TableGateway\TableGateway
     */
    public function getTableGateway()
    {
        return $this->tableGateway;
    }

    /*protected $tableGateway;
    protected $table = 'CATEGORIES';

    public function getTableGateway() {
        return $this->tableGateway;
    }

    public function getTable() {
        return $this->table;
    }

    public function setTableGateway(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function setTable($table) {
        $this->table = $table;
    }*/

}
