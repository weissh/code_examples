<?php

namespace PrismProductsManager\Model;

/**
 *
 * @author emmanuel.edegbo
 *
 */
class ProductsSpectrumEntity
{

    protected $_productSpectrumId;
    protected $_productIdFrom;
    protected $_productId;
    protected $_spectrumId = 0;
    protected $_style;
    protected $_crossBorder = '';
    protected $_created;
    protected $_modified;

    public function __construct()
    {
        $currentDatetime = new \Zend\Db\Sql\Expression("NOW()");

        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;

    }

    public function geProductSpectrumId()
    {
        return $this->_productSpectrumId;
    }

    public function getProductIdFrom()
    {
        return $this->_productIdFrom;
    }

    public function getProductId()
    {
        return $this->_productId;
    }

    /**
     * @deprecated use getSpectrumId() instead
    */
    public function geSpectrumId()
    {
        return $this->getSpectrumId();
    }

    public function getSpectrumId()
    {
        return $this->_spectrumId;
    }

    public function getStyle()
    {
        return $this->_style;
    }

    public function getCrossBorder()
    {
        return $this->_crossBorder;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setProductSpectrumId($_productSpectrumId)
    {
        $this->_productSpectrumId = $_productSpectrumId;
    }

    public function setProductIdFrom($_productIdFrom)
    {
        $this->_productIdFrom = $_productIdFrom;
    }

    public function setProductId($_productId)
    {
        $this->_productId = $_productId;
    }

    public function setSpectrumId($_spectrumId)
    {
        $this->_spectrumId = $_spectrumId;
    }

    public function setStyle($_style)
    {
        $this->_style = $_style;
    }

    public function setCrossBorder($_crossBorder)
    {
        $this->_crossBorder = $_crossBorder;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }


}
