<?php

namespace PrismProductsManager\Model;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class ProductsCategoriesEntity
{
    
    protected $_productCategoryId;
    protected $_productId;
    protected $_categoryId;
    protected $_isDefault;
    protected $_status;
    protected $_created;
    protected $_modified;
    protected $_productIdFrom;
    protected $_priority;


    public function __construct()
    {
        $currentDatetime =  date("Y-m-d H:i:s");
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
        $this->_status = 'ACTIVE';
        $this->_isDefault = 0;
        $this->_productIdFrom = 'PRODUCTS';
        $this->_priority = 1;
    }
    
    public function getPriority()
    {
        return $this->_priority;
    }

    public function setPriority($_priority)
    {
        $this->_priority = $_priority;
    }
        
    public function getProductIdFrom()
    {
        return $this->_productIdFrom;
    }

    public function setProductIdFrom($_productIdFrom)
    {
        $this->_productIdFrom = $_productIdFrom;
    }
    
    public function getProductCategoryId()
    {
        return $this->_productCategoryId;
    }

    public function getProductId()
    {
        return $this->_productId;
    }

    public function getCategoryId()
    {
        return $this->_categoryId;
    }

    public function getIsDefault()
    {
        return $this->_isDefault;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setProductCategoryId($_productCategoryId)
    {
        $this->_productCategoryId = $_productCategoryId;
    }

    public function setProductId($_productId)
    {
        $this->_productId = $_productId;
    }

    public function setCategoryId($_categoryId)
    {
        $this->_categoryId = $_categoryId;
    }

    public function setIsDefault($_isDefault)
    {
        $this->_isDefault = $_isDefault;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }


    
}
    