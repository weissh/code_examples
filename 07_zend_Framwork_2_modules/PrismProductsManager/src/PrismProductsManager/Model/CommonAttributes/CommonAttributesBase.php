<?php

namespace PrismProductsManager\Model\CommonAttributes;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

/**
 * Common Attributes Base
 */
class CommonAttributesBase
{
    /**
     * @var CommonAttributesDefinitionsTable
     */
    public $commonAttributesDefinitionsTable;

    /**
     * @var CommonAttributesGUIRepresentationTable
     */
    public $commonAttributesGUIRepresentationTable;

    /**
     * @var CommonAttributesValuesDefinitionsTable
     */
    public $commonAttributesValuesDefinitionsTable;

    /**
     * @var CommonAttributesValuesEquivalenceTable
     */
    public $commonAttributesValuesEquivalenceTable;

    /**
     * @var CommonAttributesValuesGUIRepresentationTable
     */
    public $commonAttributesValuesGUIRepresentationTable;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->commonAttributesDefinitionsTable = new CommonAttributesDefinitionsTable();
        $this->commonAttributesGUIRepresentationTable = new CommonAttributesGUIRepresentationTable();
        $this->commonAttributesValuesDefinitionsTable = new CommonAttributesValuesDefinitionsTable();
        $this->commonAttributesValuesEquivalenceTable = new CommonAttributesValuesEquivalenceTable();
        $this->commonAttributesValuesGUIRepresentationTable = new CommonAttributesValuesGUIRepresentationTable();
        $this->commonAttributesValuesTable = new CommonAttributesValuesTable();
    }

    /**
     * @return mixed
     */
    public function getAllCommonAttributesWithDetails()
    {
        return $this->getCommonAttributesDetailsBy();
    }

    /**
     * @param array $columns
     * @param array $where
     * @return mixed
     */
    public function getCommonAttributesDetailsBy($where = array(), $columns = array())
    {
        $sqlSelect = $this->tableGateway->getSql()->select();

        if (!empty($columns)) {
            $sqlSelect->columns($columns);
        }
        if (!empty($where)) {
            $sqlSelect->where($where);
        }

        $sqlSelect->join(
            $this->commonAttributesDefinitionsTable->table,
            $this->table . '.commonAttributeId = ' . $this->commonAttributesDefinitionsTable->table . '.commonAttributeId',
            'description',
            Select::JOIN_LEFT
        );

        $sqlSelect->join(
            $this->commonAttributesGUIRepresentationTable->table,
            $this->table . '.commonAttributeId = ' . $this->commonAttributesGUIRepresentationTable->table . '.commonAttributeId',
            'commonAttributesViewTypeId',
            Select::JOIN_LEFT
        );

        $resultSet = $this->tableGateway->selectWith($sqlSelect);

        return $resultSet;
    }
}
