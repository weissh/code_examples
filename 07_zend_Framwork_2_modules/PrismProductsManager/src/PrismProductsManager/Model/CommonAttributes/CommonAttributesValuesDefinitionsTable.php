<?php

namespace PrismProductsManager\Model\CommonAttributes;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use PrismProductsManager\Model\CommonAttributes\Traits\CommonAttributesTrait;

/**
 * Common Attributes Values Definitions Table Model
 */
class CommonAttributesValuesDefinitionsTable
{
    use CommonAttributesTrait;
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * @var string table name
     */
    public $table = 'COMMON_ATTRIBUTES_VALUES_Definitions';

    /**
     * @var string
     */
    public $dbAdapter = 'db-pms';

    /**
     * set TableGateway
     * @param TableGateway $tableGateway
     * @retun void
     *
     */
    public function setTableGateway(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * get TableGateway
     * @return TableGateway
     */
    public function getTableGateway()
    {
        return $this->tableGateway;
    }
}