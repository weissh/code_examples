<?php

namespace PrismProductsManager\Model\CommonAttributes;

use PrismProductsManager\Model\CommonAttributes\Traits\CommonAttributesTrait;
use Zend\Db\TableGateway\TableGateway;

/**
 * Common Attributes Definitions Table Model
 */
class CommonAttributesDefinitionsTable
{
    use CommonAttributesTrait;
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * @var string table name
     */
    public $table = 'COMMON_ATTRIBUTES_Definitions';

    /**
     * @var string
     */
    public $dbAdapter = 'db-pms';

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }


    /**
     * set TableGateway
     * @param TableGateway $tableGateway
     * @retun void
     *
     */
    public function setTableGateway(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * get TableGateway
     * @return TableGateway
     */
    public function getTableGateway()
    {
        return $this->tableGateway;
    }
}