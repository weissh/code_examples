<?php

namespace PrismProductsManager\Model\CommonAttributes;

use Zend\Stdlib\ArraySerializableInterface;
/**
 * Common Attributes Values Equivalence
 */
class CommonAttributesValuesEquivalence implements ArraySerializableInterface
{
    /**
     * {@inheritdoc}
     */
    public function exchangeArray(array $data)
    {
        $this->commonAttributesValuesEquivalenceId = (isset($data['commonAttributesValuesEquivalenceId'])) ? $data['commonAttributesValuesEquivalenceId'] : null;
        $this->attributeValueId = (isset($data['attributeValueId'])) ? $data['attributeValueId'] : null;
        $this->uniqueGroupId  = (isset($data['uniqueGroupId']))  ? $data['uniqueGroupId']  : null;
        $this->modifiedAt  = (isset($data['modifiedAt']))  ? $data['modifiedAt']  : null;
        $this->createdAt  = (isset($data['createdAt']))  ? $data['createdAt']  : null;
        $this->modifiedBy  = (isset($data['modifiedBy']))  ? $data['modifiedBy']  : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}