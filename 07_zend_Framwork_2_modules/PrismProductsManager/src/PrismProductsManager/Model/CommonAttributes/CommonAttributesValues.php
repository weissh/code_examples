<?php

namespace PrismProductsManager\Model\CommonAttributes;

use Zend\Stdlib\ArraySerializableInterface;
/**
 * Common Attributes Values
 */
class CommonAttributesValues implements ArraySerializableInterface
{
    /**
     * {@inheritdoc}
     */
    public function exchangeArray(array $data)
    {
        $this->commonAttributeValueId     = (isset($data['commonAttributeValueId']))     ? $data['commonAttributeValueId']     : null;
        $this->commonAttributeId = (isset($data['commonAttributeId'])) ? $data['commonAttributeId'] : null;
        $this->enabled  = (isset($data['enabled']))  ? $data['enabled']  : null;
        $this->commonAttributeValueSkuCode  = (isset($data['commonAttributeValueSkuCode']))  ? $data['commonAttributeValueSkuCode']  : null;
        $this->territoryId  = (isset($data['territoryId']))  ? $data['territoryId']  : null;
        $this->modifiedAt  = (isset($data['modifiedAt']))  ? $data['modifiedAt']  : null;
        $this->createdAt  = (isset($data['createdAt']))  ? $data['createdAt']  : null;
        $this->modifiedBy  = (isset($data['modifiedBy']))  ? $data['modifiedBy']  : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}