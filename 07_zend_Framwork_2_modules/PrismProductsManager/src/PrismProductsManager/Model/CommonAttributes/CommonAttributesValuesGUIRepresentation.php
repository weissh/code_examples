<?php

namespace PrismProductsManager\Model\CommonAttributes;

use Zend\Stdlib\ArraySerializableInterface;
/**
 * Common Attributes Values Equivalence
 */
class CommonAttributesValuesGUIRepresentation implements ArraySerializableInterface
{
    /**
     * {@inheritdoc}
     */
    public function exchangeArray(array $data)
    {
        $this->commonAttributesValuesGuiRepresentationId = (isset($data['commonAttributesValuesGuiRepresentationId'])) ? $data['commonAttributesValuesGuiRepresentationId'] : null;
        $this->commonAttributeValueId = (isset($data['commonAttributeValueId'])) ? $data['commonAttributeValueId'] : null;
        $this->commonAttributeValueOrder  = (isset($data['commonAttributeValueOrder']))  ? $data['commonAttributeValueOrder']  : null;
        $this->pmsDisplay  = (isset($data['pmsDisplay']))  ? $data['pmsDisplay']  : null;
        $this->shopDisplay  = (isset($data['shopDisplay']))  ? $data['shopDisplay']  : null;
        $this->page  = (isset($data['page']))  ? $data['page']  : null;
        $this->modifiedAt  = (isset($data['modifiedAt']))  ? $data['modifiedAt']  : null;
        $this->createdAt  = (isset($data['createdAt']))  ? $data['createdAt']  : null;
        $this->modifiedBy  = (isset($data['modifiedBy']))  ? $data['modifiedBy']  : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}