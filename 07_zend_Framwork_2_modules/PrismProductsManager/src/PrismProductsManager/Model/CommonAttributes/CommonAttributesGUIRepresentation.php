<?php

namespace PrismProductsManager\Model\CommonAttributes;

use Zend\Stdlib\ArraySerializableInterface;
/**
 * Common Attributes
 */
class CommonAttributesDefinitions implements ArraySerializableInterface
{
    /**
     * {@inheritdoc}
     */
    public function exchangeArray(array $data)
    {
        $this->commonAttributesGuiRepresentationId = (isset($data['commonAttributesGuiRepresentationId'])) ? $data['commonAttributesGuiRepresentationId'] : null;
        $this->commonAttributeId = (isset($data['commonAttributeId'])) ? $data['commonAttributeId'] : null;
        $this->commonAttributesViewTypeId  = (isset($data['commonAttributesViewTypeId']))  ? $data['commonAttributesViewTypeId']  : null;
        $this->commonAttributesParentId  = (isset($data['commonAttributesParentId']))  ? $data['commonAttributesParentId']  : null;
        $this->pmsDisplay  = (isset($data['pmsDisplay']))  ? $data['pmsDisplay']  : null;
        $this->shopDisplay  = (isset($data['shopDisplay']))  ? $data['shopDisplay']  : null;
        $this->page  = (isset($data['page']))  ? $data['page']  : null;
        $this->modifiedAt  = (isset($data['modifiedAt']))  ? $data['modifiedAt']  : null;
        $this->createdAt  = (isset($data['createdAt']))  ? $data['createdAt']  : null;
        $this->modifiedBy  = (isset($data['modifiedBy']))  ? $data['modifiedBy']  : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}