<?php

namespace PrismProductsManager\Model\CommonAttributes\Traits;

/**
 * Common Attributes
 */
trait CommonAttributesTrait
{
    /**
     * @param array $resultSet
     * @param string $keyColumn
     * @param string $keyAppend
     * @param string $valueColumn
     * @return array
     */
    public function listResultsWithKeyAs(
        $resultSet = array(),
        $keyAppend = '',
        $keyColumn = 'commonAttributeId',
        $valueColumn = 'description',
        $separateArray = false)
    {
        $returnArray = array();
        try {
            if (!empty($resultSet) && !empty($keyColumn) && !empty($valueColumn)) {
                foreach ($resultSet as $newCommonAttribute) {

                    if ($separateArray) {
                        $returnArray[][$newCommonAttribute->$keyColumn . $keyAppend] = $newCommonAttribute->$valueColumn;
                    } else {
                                     $returnArray[$newCommonAttribute->$keyColumn . $keyAppend] = $newCommonAttribute->$valueColumn;
                    }
                }
            }
        } catch (\Exception $ex) {
            $returnArray = false;
        }
        return $returnArray;
    }
}