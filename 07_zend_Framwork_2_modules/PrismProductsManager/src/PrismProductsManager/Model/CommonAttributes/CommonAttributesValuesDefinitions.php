<?php

namespace PrismProductsManager\Model\CommonAttributes;

use Zend\Stdlib\ArraySerializableInterface;
/**
 * Common Attributes Values Definitions
 */
class CommonAttributesValuesDefinitions implements ArraySerializableInterface
{
    /**
     * {@inheritdoc}
     */
    public function exchangeArray(array $data)
    {
        $this->commonAttributesValuesDefinitionId = (isset($data['commonAttributesValuesDefinitionId'])) ? $data['commonAttributesValuesDefinitionId'] : null;
        $this->commonAttributeValueId = (isset($data['commonAttributeValueId'])) ? $data['commonAttributeValueId'] : null;
        $this->languageId  = (isset($data['languageId']))  ? $data['languageId']  : null;
        $this->value  = (isset($data['value']))  ? $data['value']  : null;
        $this->modifiedAt  = (isset($data['modifiedAt']))  ? $data['modifiedAt']  : null;
        $this->createdAt  = (isset($data['createdAt']))  ? $data['createdAt']  : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}