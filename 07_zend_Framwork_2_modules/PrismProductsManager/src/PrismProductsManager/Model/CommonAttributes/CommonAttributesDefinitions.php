<?php

namespace PrismProductsManager\Model\CommonAttributes;

use Zend\Stdlib\ArraySerializableInterface;
/**
 * Common Attributes
 */
class CommonAttributesDefinitions implements ArraySerializableInterface
{
    /**
     * {@inheritdoc}
     */
    public function exchangeArray(array $data)
    {
        $this->commonAttributeDefinitionId     = (isset($data['commonAttributeDefinitionId']))     ? $data['commonAttributeDefinitionId']     : null;
        $this->commonAttributeId = (isset($data['commonAttributeId'])) ? $data['commonAttributeId'] : null;
        $this->languageId  = (isset($data['languageId']))  ? $data['languageId']  : null;
        $this->description  = (isset($data['description']))  ? $data['description']  : null;
        $this->modifiedAt  = (isset($data['modifiedAt']))  ? $data['modifiedAt']  : null;
        $this->createdAt  = (isset($data['createdAt']))  ? $data['createdAt']  : null;
        $this->modifiedBy  = (isset($data['modifiedBy']))  ? $data['modifiedBy']  : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}