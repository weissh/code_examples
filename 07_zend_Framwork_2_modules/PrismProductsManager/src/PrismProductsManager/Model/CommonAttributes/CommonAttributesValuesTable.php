<?php

namespace PrismProductsManager\Model\CommonAttributes;

use PrismProductsManager\Model\CommonAttributes\Traits\CommonAttributesTrait;
use Zend\Db\TableGateway\TableGateway;


/**
 * Common Attributes Values Table Model
 */
class CommonAttributesValuesTable
{
    use CommonAttributesTrait;
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * @var string table name
     */
    public $table = 'COMMON_ATTRIBUTES_VALUES';

    /**
     * @var string
     */
    public $dbAdapter = 'db-pms';

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * set TableGateway
     * @param TableGateway $tableGateway
     * @retun void
     *
     */
    public function setTableGateway(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * get TableGateway
     * @return TableGateway
     */
    public function getTableGateway()
    {
        return $this->tableGateway;
    }

    /**
     *
     * Check if data found, return true
     *
     * @param $id
     *
     * @return bool
     */
    public function getIsDefaultValue($id)
    {
        $sql = $this->tableGateway->getSql();
        $select = $sql->select();
        $select->join('COMMON_ATTRIBUTES_VALUES_GUI_Representation',
                'COMMON_ATTRIBUTES_VALUES_GUI_Representation.commonAttributeValueId  = COMMON_ATTRIBUTES_VALUES.commonAttributeValueId ');
        $select->where(['isDefault' => 1, 'COMMON_ATTRIBUTES_VALUES.commonAttributeValueId' => $id]);
        $record = $this->tableGateway->selectWith($select);

        if ($record->count() === 0 ) {
            return false;
        }

        return true;
    }
}