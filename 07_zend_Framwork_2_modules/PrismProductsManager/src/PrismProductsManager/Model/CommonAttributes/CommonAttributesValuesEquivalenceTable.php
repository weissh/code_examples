<?php

namespace PrismProductsManager\Model\CommonAttributes;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use PrismProductsManager\Model\CommonAttributes\Traits\CommonAttributesTrait;

/**
 * Common Attributes Values Equivalence Table Model
 */
class CommonAttributesValuesEquivalenceTable
{
    use CommonAttributesTrait;
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * @var string table name
     */
    public $table = 'COMMON_ATTRIBUTES_VALUES_Equivalence';

    /**
     * @var string
     */
    public $dbAdapter = 'db-pms';

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }


    /**
     * set TableGateway
     * @param TableGateway $tableGateway
     * @retun void
     *
     */
    public function setTableGateway(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * get TableGateway
     * @return TableGateway
     */
    public function getTableGateway()
    {
        return $this->tableGateway;
    }
}