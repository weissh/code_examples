<?php

namespace PrismProductsManager\Model\CommonAttributes;

use Zend\Stdlib\ArraySerializableInterface;
/**
 * Common Attributes
 */
class CommonAttributes implements ArraySerializableInterface
{
    /**
     * {@inheritdoc}
     */
    public function exchangeArray(array $data)
    {
        $this->commonAttributeId     = (isset($data['commonAttributeId']))     ? $data['commonAttributeId']     : null;
        $this->usedForFiltering = (isset($data['usedForFiltering'])) ? $data['usedForFiltering'] : null;
        $this->usedForMerchantCategories  = (isset($data['usedForMerchantCategories']))  ? $data['usedForMerchantCategories']  : null;
        $this->isColour  = (isset($data['isColour']))  ? $data['isColour']  : null;
        $this->isSize  = (isset($data['isSize']))  ? $data['isSize']  : null;
        $this->enabled  = (isset($data['enabled']))  ? $data['enabled']  : null;
        $this->modifiedAt  = (isset($data['modifiedAt']))  ? $data['modifiedAt']  : null;
        $this->modifiedBy  = (isset($data['modifiedBy']))  ? $data['modifiedBy']  : null;
        $this->createdAt  = (isset($data['createdAt']))  ? $data['createdAt']  : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
