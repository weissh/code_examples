<?php
namespace PrismProductsManager\Model;

use Doctrine\Common\Collections\ArrayCollection;
use PrismProductsManager\Entity\CommonAttributesSpectrumMapping;
use PrismProductsManager\Entity\CommonAttributesSpectrumMappingOverride;
use PrismProductsManager\Entity\CommonAttributesValuesAutoFromDate;
use PrismProductsManager\Entity\HistoricalValue;
use PrismProductsManager\Entity\InputFieldRAttributes;
use PrismProductsManager\Entity\Territories;
use PrismProductsManager\Model\CommonAttributes\CommonAttributes;
use PrismProductsManager\Model\CommonAttributes\CommonAttributesValues;
use PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable;
use PrismProductsManager\Model\Entity\ProductsTabsEntity;
use PrismProductsManager\Model\Entity\TaxesRegionsEntity;
use PrismProductsManager\Service\CommonAttributesService;
use PrismProductsManager\Service\HistoricalService;
use PrismProductsManager\Service\InputService;
use PrismProductsManager\Service\LoggingService;
use PrismQueueManager\Service\QueueService;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Stdlib\Hydrator\ClassMethods;
use PrismCommon\Service\MailSender;

/**
 * Class ProductsMapper
 *
 * @package PrismProductsManager\Model
 */
class ProductsMapper
{

    /**
     *
     * @var string
     */
    protected $_tableName = 'PRODUCTS';

    /**
     *
     * @var string
     */
    protected $_currencyTable = 'CURRENCY';

    /**
     *
     * @var string
     */
    protected $_productsTypesTable = 'PRODUCTS_Types';

    /**
     *
     * @var string
     */
    protected $_productsTypesDefinitionsTable = 'PRODUCTS_Types_Definitions';

    /**
     *
     * @var string
     */
    protected $_productsPricesTable = 'PRODUCTS_Prices';

    /**
     *
     * @var string
     */
    protected $_productsCategoriesRelationshipTable = 'PRODUCTS_R_CATEGORIES';

    /**
     *
     * @var string
     */
    protected $_productsVariantsRelationshipTable = 'PRODUCTS_R_ATTRIBUTES';

    /**
     *
     * @var string
     */
    protected $_productsDefinitionsTable = 'PRODUCTS_Definitions';

    /**
     *
     * @var string
     */
    protected $_productsMerchantCategoriesTable = 'PRODUCTS_Merchant_Categories';

    /**
     *
     * @var string
     */
    protected $_productsMerchantCategoriesDefinitionsTable = 'PRODUCTS_Merchant_Categories_Definitions';

    /**
     *
     * @var string
     */
    protected $_productsTagsTable = 'PRODUCTS_Tags';

    /**
     *
     * @var string
     */
    protected $_productsMisspellsTable = 'PRODUCTS_Mis_Spells';

    /**
     *
     * @var string
     */
    protected $_productsAttributesCombinationTable = 'PRODUCTS_ATTRIBUTES_Combination';

    /**
     *
     * @var string
     */
    protected $_productsSpectrumTable = 'PRODUCTS_R_SPECTRUM';

    /**
     *
     * @var string
     */
    protected $_productsAttributesSkuRuleRelationsTable = 'PRODUCTS_R_ATTRIBUTES_R_SKU_Rules';

    /**
     *
     * @var string
     */
    protected $_productsChannelsTable = 'PRODUCTS_R_CHANNELS';

    /**
     *
     * @var string
     */
    protected $_productsClientsWebsitesTable = 'PRODUCTS_R_CLIENTS_Websites';

    /**
     *
     * @var string
     */
    protected $_productsRelatedProductsTable = 'PRODUCTS_R_PRODUCTS';

    /**
     *
     * @var string
     */
    protected $_productsAddOnTable = "PRODUCT_Add_Ons";

    /**
     *
     * @var string
     */
    protected $_productsTabsTable = "PRODUCT_Tabs";

    /**
     *
     * @var string
     */
    protected $_attributeGroupsTable = 'ATTRIBUTES_Groups';

    /**
     *
     * @var string
     */
    protected $_tabDefiniitonsTable = 'TABS_Definitions';

    /**
     *
     * @var string
     */
    protected $_coloursGroupsTable = 'COLOURS_Groups';
    /**
     * @var string
     */
    protected $_coloursGroupsDefinitionsTable = 'COLOURS_Groups_Definitions';

    /**
     *
     * @var string
     */
    protected $_tabsTable = 'TABS';

    /**
     *
     * @var string
     */
    protected $_tabGroupsTable = 'TABS_Groups';

    /**
     *
     * @var string
     */
    protected $_tabGroupsdefinitionsTable = 'TABS_Groups_Definitions';

    /**
     *
     * @var string
     */
    protected $_productTabsTable = 'PRODUCTS_Tabs';

    /**
     *
     * @var string
     */
    protected $_attributeGroupsDefinitionsTable = 'ATTRIBUTES_Group_Definitions';

    /**
     *
     * @var string
     */
    protected $_channelsTable = 'CHANNELS';

    /**
     *
     * @var string
     */
    protected $_channelsDefinitionsTable = 'CHANNELS_Definitions';

    /**
     *
     * @var string
     */
    protected $_categoriesTable = 'CATEGORIES';

    /**
     *
     * @var string
     */
    protected $_categoriesDefinitionsTable = 'CATEGORIES_Definitions';

    /**
     *
     * @var string
     */
    protected $_suppliersTable = 'SUPPLIERS';

    /**
     *
     * @var string
     */
    protected $_suppliersDefinitionsTable = 'SUPPLIERS_Definitions';

    /**
     *
     * @var string
     */
    protected $_skuRulesTable = 'SKU_Rules';

    /**
     *
     * @var string
     */
    protected $_productsAttributesCombinationDefinitions = 'PRODUCTS_ATTRIBUTES_Combination_Definitions';

    /**
     *
     * @var string
     */
    protected $_productBarcodesTable = 'PRODUCTS_Barcodes';

    /**
     *
     * @var string
     */
    protected $_productSupplierPriceTable = 'PRODUCTS_Supplier_Price';

    /**
     *
     * @var string
     */
    protected $productsCommonAttributesTable = 'PRODUCTS_Common_Attributes';

    /**
     *
     * @var string
     */
    protected $commonAttributeValuesDefinitionsTable = 'COMMON_ATTRIBUTES_VALUES_Definitions';

    /**
     *
     * @var string
     */
    protected $commonAttributesGuiRepresentationTable = 'COMMON_ATTRIBUTES_GUI_Representation';

    /**
     *
     * @var string
     */
    protected $commonAttributesTable = 'COMMON_ATTRIBUTES';

    /**
     * @var string
     */
    protected $commonAttributesDefinitionsTable = 'COMMON_ATTRIBUTES_Definitions';

    /**
     * @var string
     */
    protected $commonAttributesDefinitionTable = 'COMMON_ATTRIBUTES_Definitions';

    /**
     * @var string
     */
    protected $commonAttributesViewTypeTable = 'COMMON_ATTRIBUTES_View_Type';

    /**
     * @var string
     */
    protected $commonAttributesSpectrumMappingOverride = 'COMMON_ATTRIBUTES_Spectrum_Mapping_Override';

    /**
     * @var string
     */
    protected $commonAttributesSpectrumMapping = 'COMMON_ATTRIBUTES_Spectrum_Mapping';
    /**
     *
     * @var Adapter
     */
    protected $_dbAdapter;

    /**
     *
     * @var Sql
     */
    protected $_sql;

    /**
     *
     * @var ClassMethods
     */
    protected $_hydrator;

    /**
     *
     * @var
     *
     */
    protected $_siteConfig;

    /**
     *
     * @var
     *
     */
    protected $_languages;

    /**
     *
     * @var
     *
     */
    protected $_defaultLanguage;

    /**
     *
     * @var
     *
     */
    protected $_merchantCategoriesForm;

    /**
     *
     * @var
     *
     */
    protected $_attributesGroupMapper;

    /**
     *
     * @var
     *
     */
    protected $_categoriesMapper;

    /**
     *
     * @var
     *
     */
    protected $_merchantCategoriesMapper;

    /**
     *
     * @var ProductsMapperVariants
     */
    protected $_productsMapperVariants;

    /**
     *
     * @var ProductsMapperRelatedProducts
     */
    protected $_productsMapperRelatedProducts;

    /**
     *
     * @var ProductsMapperSearchProducts
     */
    protected $_productsMapperSearchProducts;

    /**
     *
     * @var ProductsMapperActivateProduct
     */
    protected $_productsMapperActivateProduct;

    /**
     *
     * @var ProductsMapperAttributes
     */
    protected $_productsMapperAttributes;

    /**
     *
     * @var ProductsMapperPrices
     */
    protected $_productsMapperPrices;

    /**
     *
     * @var
     *
     */
    protected $_actionLogger;

    /**
     *
     * @var int
     */
    protected $_loggedInUserId;

    /**
     *
     * @var
     *
     */
    protected $_taxMapper;

    /**
     * record per page when using paginator
     *
     * @var int
     */
    protected $_recordPerPage;

    /**
     *
     * @var object Query paginator service object
     */
    protected $_queryPaginator;

    /**
     * Webiste Id from session
     *
     * @var int
     */
    protected $_currentWebsiteId;

    /**
     *
     * @var
     *
     */
    protected $_productFolderMapper;

    /**
     * current set language from session
     *
     * @var int
     */
    protected $_currentLanguage;

    /**
     *
     * @var
     *
     */
    protected $_currencies;

    /**
     *
     * @var
     *
     */
    protected $_mediaServer;

    /**
     *
     * @var string
     */
    private $_sizesAccessoriesTable = 'SIZES_Accessories';

    /**
     *
     * @var string
     */
    private $_sizesShoesTable = 'SIZES_Shoes';

    /**
     *
     * @var string
     */
    private $_sizesClothesTable = 'SIZES_Clothes';

    /**
     *
     * @var string
     */
    private $_sizesShirtsTable = 'SIZES_Shirts';

    /**
     *
     * @var string
     */
    private $_sizesBeltsTable = 'SIZES_Belts';

    /**
     *
     * @var string
     */
    private $_sizesJacketsTable = 'SIZES_Jackets';

    /**
     *
     * @var string
     */
    private $_sizesTrousersTable = 'SIZES_Trousers';

    /**
     *
     * @var string
     */
    private $_sizesSwimShortsTable = 'SIZES_SwimShorts';

    /**
     *
     * @var string
     */
    protected $_currencyCodesTable = 'CURRENCY_Codes';

    /**
     *
     * @var string
     */
    protected $territoriesTable = 'TERRITORIES';

    /**
     *
     * @var string
     */
    protected $workflowTable = 'WORKFLOWS';

    /**
     *
     * @var ColoursMapper
     */
    protected $coloursMapper;

    /**
     *
     * @var InputService
     */
    protected $inputService;

    /**
     *
     * @var string
     */
    protected $productsRWorkflowsTable = 'PRODUCTS_R_WORKFLOWS';

    /**
     *
     * @var CommonAttributesService
     */
    protected $commonAttributesService;

    /**
     *
     * @var string
     */
    protected $territoriesRCurrenciesTable = 'TERRITORIES_R_CURRENCIES';

    /**
     *
     * @var string
     */
    protected $clientCode;

    /**
     *
     * @var QueueService
     */
    protected $queueService;

    /**
     *
     * @var
     *
     */
    protected $clientConfig;

    /**
     * @var array
     */
    private $_cache = [];

    /**
     *
     * @var unknown
     */
    private $mailSender;

    /**
     * Configuration
     * @var array
     */
    private $config;

    /** @var HistoricalService $historicalService */
    private $historicalService;
    /**
     * @var LoggingService
     */
    private $loggingService;

    /**
     * ProductsMapper constructor.
     * @param Adapter $dbAdapter
     * @param $siteConfig
     * @param AttributesGroupMapper $attributesGroupMapper
     * @param $categoriesMapper
     * @param $merchantCategoriesMapper
     * @param $actionLogger
     * @param $queryPaginator
     * @param $taxMapper
     * @param $productFolderMapper
     * @param $mediaServer
     * @param $logedInUserId
     * @param CommonAttributesValuesTable $commonAttributesValuesTable
     * @param InputService $inputService
     * @param ColoursMapper $coloursMapper
     * @param CommonAttributesService $commonAttributesService
     * @param $clientCode
     * @param QueueService $queueService
     * @param $clientConfig
     * @param MailSender $mailSender
     * @param $config
     * @param LoggingService $loggingService
     */
    public function __construct(Adapter $dbAdapter, $siteConfig,
        AttributesGroupMapper $attributesGroupMapper, $categoriesMapper,
        $merchantCategoriesMapper, $actionLogger, $queryPaginator, $taxMapper,
        $productFolderMapper, $mediaServer, $logedInUserId,
        CommonAttributesValuesTable $commonAttributesValuesTable,
        InputService $inputService, ColoursMapper $coloursMapper,
        CommonAttributesService $commonAttributesService, $clientCode,
        QueueService $queueService, $clientConfig, MailSender $mailSender,
        $config,
        HistoricalService $historicalService,
        LoggingService $loggingService
        )
    {
        $this->_siteConfig = $siteConfig;
        $this->_languages = $siteConfig['languages']['site-languages'];
        $this->_defaultLanguage = $siteConfig['languages']['current-language'];
        $this->_dbAdapter = $dbAdapter;
        $this->_sql = new Sql($dbAdapter);
        $this->_sql->setTable($this->_tableName);
        $this->commonAttributesService = $commonAttributesService;
        $this->_hydrator = new ClassMethods(false);
        $this->_attributesGroupMapper = $attributesGroupMapper;
        $this->_categoriesMapper = $categoriesMapper;
        $this->_merchantCategoriesMapper = $merchantCategoriesMapper;
        $this->_currentLanguage = $siteConfig['languages']['current-language'];
        $this->_currentWebsiteId = $siteConfig['websites']['current-website-id'];
        $this->_recordPerPage = $siteConfig['pagination']['records-per-page'];
        $this->_taxMapper = $taxMapper;
        $this->_productFolderMapper = $productFolderMapper;
        $this->_mediaServer = $mediaServer;
        $this->coloursMapper = $coloursMapper;
        $this->commonAttributesValuesTable = $commonAttributesValuesTable;
        $this->inputService = $inputService;
        $this->clientCode = $clientCode;
        $this->queueService = $queueService;
        $this->clientConfig = $clientConfig;
        $this->mailSender = $mailSender;
        $this->config = $config;
        $this->loggingService = $loggingService;

        // Create instance of product attributes sub class
        if (! $this->_productsMapperAttributes instanceof ProductsMapperAttributes) {
            $this->_productsMapperAttributes = new ProductsMapperAttributes($this->_sql, $this->_hydrator, $this->_dbAdapter, $this->_attributesGroupMapper, $actionLogger, $this->_languages, $this->_currentLanguage);
        }
        // Create instance of product prices sub class
        if (! $this->_productsMapperPrices instanceof ProductsMapperPrices) {
            $this->_productsMapperPrices = new ProductsMapperPrices($this->_sql, $this->_hydrator, $this->_dbAdapter, $this->_productsMapperVariants);
        }
        // Create instance of product variant sub class
        if (! $this->_productsMapperVariants instanceof ProductsMapperVariants) {
            $this->_productsMapperVariants = new ProductsMapperVariants($this->_sql, $this->_hydrator, $this->_dbAdapter, $this->_merchantCategoriesMapper, $this->_productsMapperAttributes, $this->_productsMapperPrices, $this->_taxMapper, $this->_productFolderMapper, $this->commonAttributesValuesTable);
        }
        // Create instance of related products sub class
        if (! $this->_productsMapperRelatedProducts instanceof ProductsMapperRelatedProducts) {
            $this->_productsMapperRelatedProducts = new ProductsMapperRelatedProducts($this->_sql, $this->_hydrator, $this->_dbAdapter, $this->_productsMapperVariants);
        }
        // Create instance of related products sub class
        if (! $this->_productsMapperSearchProducts instanceof ProductsMapperSearchProducts) {
            $this->_productsMapperSearchProducts = new ProductsMapperSearchProducts($this->_sql, $this->_hydrator, $this->_dbAdapter, $this->_productsMapperVariants);
        }
        // Create instance of related products sub class
        if (! $this->_productsMapperActivateProduct instanceof ProductsMapperActivateProduct) {
            $this->_productsMapperActivateProduct = new ProductsMapperActivateProduct($this->_sql, $this->_hydrator, $this->_dbAdapter, $this->_productsMapperVariants, $this->_categoriesMapper, $this->_productFolderMapper);
        }
        $this->_currencies = $siteConfig['currencies'];
        $this->_loggedInUserId = $logedInUserId;
        $this->_actionLogger = $actionLogger;
        $this->_queryPaginator = $queryPaginator;
        $this->historicalService = $historicalService;
    }

    /**
     * Retrieve some setup information required for product setup
     * Used to populate dropdown select menus on basic information tab
     *
     * @return array
     */
    public function fetchBasicDetails()
    {
        $return = array();

        $manufacturers = $this->getManufacturers();
        $channels = $this->getChannels();
        $productStatuses = $this->getProductStatus();

        // Build currency and price-type matrix
        $currenciesPriceTypeMatrix = $this->getCurrenciesTypeMatrix();

        $websites = $this->_categoriesMapper->fetchAllWebsites();
        $defaultWebsite = $this->_categoriesMapper->fetchDefaultWebsite();

        $careInformation = $this->getCareInformation();

        $return['websites'] = $websites;
        $return['defaultWebsite'] = $defaultWebsite;
        $return['manufacturers'] = $manufacturers;
        $return['channels'] = $channels;
        $return['statuses'] = $productStatuses;
        $return['careInformation'] = $careInformation;
        $return['currenciesPriceTypeMatrix'] = $currenciesPriceTypeMatrix;

        return $return;
    }

    /**
     *
     * @return array
     */
    public function getCurrenciesTypeMatrix()
    {
        // Retrieve currency and price-type to build a matrix
        /** @var array $currencies  */
        $currencies = $this->getCurrencies();
        /** @var array $priceTypes  */
        $priceTypes = $this->getPriceTypes();
        $currenciesPriceTypeMatrix = array();
        if (count($currencies) > 0 && count($priceTypes) > 0) {
            foreach ($currencies as $currencyKey => $currencyDetails) {
                if (isset($currencyDetails['isoCode']) && trim($currencyDetails['isoCode']) != '') {
                    foreach ($priceTypes as $priceTypeKey => $priceTypeValue) {
                        $currenciesPriceTypeMatrix[$currencyDetails['isoCode']][$priceTypeKey] = $priceTypeValue;
                    }
                }
            }
        }

        return $currenciesPriceTypeMatrix;
    }

    /**
     * Search for product based on productName
     *
     * @pram string $searchTerm
     *
     * @return array
     */
    public function searchByProductName($searchTerm)
    {
        $searchResults = $this->_productsMapperSearchProducts->_searchByProductName($searchTerm);
        return $searchResults;
    }

    /**
     * Search for product based on productName
     *
     * @pram string $searchTerm
     *
     * @return array
     */
    public function searchByProduct($searchTerm)
    {
        $searchResults = $this->_productsMapperSearchProducts->_searchBySku($searchTerm);
        return $searchResults;
    }

    /**
     * Activate a product
     *
     * @param array $data
     * @return array
     */
    public function activateProduct($data)
    {
        $this->_dbAdapter->getDriver()
            ->getConnection()
            ->beginTransaction();
        // when activating a product the logic should be:
        // - check if the product/all it's variants are meeting the requirements
        // - make API call to Spectrum to retrieve the spectrum Id for the product if the product doesn't have any variants
        // or for the variants if it has variants and save an entry for the product/each variant into PRODUCTS_R_SPECTRUM table
        // - set the status for the product/each variant in the main and related tables as ACTIVE
        $productId = isset($data['productId']) ? $data['productId'] : 0;
        if ($productId == 0) {
            return false;
        }
        $productActivatedSuccessfully = $this->_productsMapperActivateProduct->_activateProduct($data);
        if ($productActivatedSuccessfully['success']) {
            // at this point we can persist the changes to the database and index the product in elastic-search
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->commit();
        } else {
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->rollback();
        }
        return $productActivatedSuccessfully;
    }

    /**
     * Get products top be activated
     *
     * @param string $date
     * @return string
     */
    public function getProductsToMakeLive($date = '')
    {
        if (trim($date) == '') {
            $date = date('Y-m-d');
            ;
        }

        $this->_sql->setTable(array(
            'pd' => $this->_productsDefinitionsTable
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'productDefinitionsId',
            'productId',
            'productIdFrom'
        ));
        $select->where->like('pd.makeLive', "$date%");
        $select->where("pd.status = 'INACTIVE'");

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $productsIdsToActivate = array();
        $alreadyInArray = array();
        foreach ($results as $key => $value) {
            if (isset($value['productId'])) {
                if (! in_array($value['productId'], $alreadyInArray)) {
                    $alreadyInArray[] = $value['productId'];
                    $productsIdsToActivate[] = array(
                        'productId' => $value['productId']
                    );
                }
            }
        }

        return array(
            'dataCount' => count($alreadyInArray),
            'data' => $productsIdsToActivate
        );
    }

    /**
     * Activate a product
     *
     * @param array $data
     * @return array
     */
    public function deactivateProduct($data)
    {
        // when deactivating a product the logic should be:
        // - check if the product/all it's variants are meeting the requirements
        // - set the status for the product/each variant in the main and related tables as INACTIVE
        $this->_dbAdapter->getDriver()
            ->getConnection()
            ->beginTransaction();
        // remove it to the spectrum table
        $productDeactivatedSuccessfully = $this->_productsMapperActivateProduct->_deactivateProduct($data);
        $this->_dbAdapter->getDriver()
            ->getConnection()
            ->commit();
        return $productDeactivatedSuccessfully;
    }

    /**
     * Activate a variant
     *
     * @param int | boolean $variantId
     * @param int $productId
     * @return boolean
     */
    public function activateVariants($productId, $variantId = false)
    {
        // when activating a variant the logic should be:
        // - check if the variant is meeting the requirements
        // - set the status for the variant in the main and related tables as INACTIVE
        // check if main product is ACTIVE
        $productDetails = $this->getMinimumProductsDetails($productId);
        if ($productDetails['status'] == 'ACTIVE') {
            $table = $this->_productsVariantsRelationshipTable;
            // Activate the variants
            if ($variantId === false) {
                $whereClause = array(
                    'productId' => $productId
                );
            } else {
                $whereClause = array(
                    'productAttributeId' => $variantId
                );
            }
            $fieldsToUpdate = array(
                'status' => 'ACTIVE'
            );
            $whereNoEqualTo = array(
                0 => array(
                    'status',
                    'DELETED'
                )
            );
            $productVariantsActivated = $this->_update($fieldsToUpdate, $table, $whereClause, $whereNoEqualTo);
            return $productVariantsActivated;
        }
        return false;
    }

    /**
     * Deactivate a variant
     *
     * @param int | boolean $variantId
     * @param int $productId
     * @return boolean
     */
    public function deactivateVariants($productId, $variantId = false)
    {
        // when deactivating a variant the logic should be:
        // - check if the variant is meeting the requirements
        // - set the status for the variant in the main and related tables as INACTIVE
        // - allow all the variants on a product to be inactive, but at the last deactivated variant deactivate the product also
        // check if main product is ACTIVE
        $productDetails = $this->getMinimumProductsDetails($productId);
        if ($productDetails['status'] == 'ACTIVE') {
            $table = $this->_productsVariantsRelationshipTable;
            // Activate the variants
            if ($variantId === false) {
                $whereClause = array(
                    'productId' => $productId
                );
            } else {
                $whereClause = array(
                    'productAttributeId' => $variantId
                );
            }
            $whereNoEqualTo = array(
                0 => array(
                    'status',
                    'DELETED'
                )
            );
            $fieldsToUpdate = array(
                'status' => 'INACTIVE'
            );
            $productVariantsDeactivated = $this->_update($fieldsToUpdate, $table, $whereClause, $whereNoEqualTo);
            return $productVariantsDeactivated;
        }
        return false;
    }

    /**
     * Save related product
     *
     * @param array $params
     * @return array
     */
    public function saveRelatedProducts($params)
    {
        $savedRelatedProducts = $this->_productsMapperRelatedProducts->_saveRelatedProducts($params);
        return $savedRelatedProducts;
    }

    /**
     * Retrieve related products to a product
     *
     * @param int $productId
     * @return mixed
     */
    public function _getProductsRelatedProducts($productId)
    {
        $relatedProducts = $this->_productsMapperRelatedProducts->_getProductsRelatedProducts($productId);
        return $relatedProducts;
    }

    /**
     *
     * @param int $variantId
     * @param int $languageId
     * @return array
     */
    public function getVariantsRelatedVariants($variantId, $languageId = 1)
    {
        $relatedVariants = $this->_productsMapperRelatedProducts->getVariantsRelatedVariants($variantId, $languageId);
        return $relatedVariants;
    }

    /**
     * Get available price types
     *
     * @return array
     */
    public function getPriceTypes()
    {
        return array(
            'offerPrice' => array(
                'name' => 'Offer',
                'hoverText' => 'Offer Price'
            ),
            'nowPrice' => array(
                'name' => 'Now',
                'hoverText' => 'Now Price'
            ),
            'wasPrice' => array(
                'name' => 'Was',
                'hoverText' => 'Was Price'
            ),
            'costPrice' => array(
                'name' => 'Cost',
                'hoverText' => 'Cost Price'
            ),
            'tradingCostPrice' => array(
                'name' => 'Trading',
                'hoverText' => 'Trading Cost Price'
            ),
            'wholesalePrice' => array(
                'name' => 'Wholesale',
                'hoverText' => 'Wholesale Price'
            ),
            'freightPrice' => array(
                'name' => 'Freight',
                'hoverText' => 'Freight Price'
            ),
            'dutyPrice' => array(
                'name' => 'Duty',
                'hoverText' => 'Duty Price'
            ),
            'designPrice' => array(
                'name' => 'Design',
                'hoverText' => 'Design Price'
            ),
            'packagingPrice' => array(
                'name' => 'Packaging',
                'hoverText' => 'Packaging Price'
            )
        );
    }

    /**
     * Return Channels
     *
     * @return string
     */
    protected function getChannels()
    {
        $return = array(
            1 => 'WEB',
            2 => 'EPOS',
            3 => 'SPECTRUM'
        );

        return $return;
    }

    /**
     * return manufacturers
     *
     * @return string
     */
    protected function getManufacturers()
    {
        return array(); // Tempoarrily removed manufacturers

        $return = array(
            1 => 'PERONI',
            2 => 'MAVA',
            3 => 'MORITEX'
        );

        return $return;
    }

    /**
     * Function to return statuses of a product
     *
     * @param boolean $edit
     * @return string
     */
    public function getProductStatus($edit = false)
    {
        $return = array(
            'INACTIVE' => 'INACTIVE',
            'ACTIVE' => 'ACTIVE'
        );
        if ($edit == true) {
            $return['DELETED'] = 'DELETE';
        }
        return $return;
    }

    /**
     * Function to get product Types.
     *
     * @param int $languageId
     * @return array
     */
    public function getProductTypes($languageId = 1)
    {
        $returnArray = array();
        $this->_sql->setTable(array(
            'pt' => $this->_productsTypesTable
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'productTypeId'
        ));
        $select->where(array(
            'pt.status' => 'ACTIVE',
            'ptd.languageId' => $languageId
        ));
        $select->join(array(
            'ptd' => $this->_productsTypesDefinitionsTable
        ), // Set join table and alias
            'pt.productTypeId = ptd.productTypeId', array(
                'name',
                'description'
            ));
        $select->order(array(
            'pt.productTypeId ASC'
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $position = 0;
        foreach ($results as $key => $value) {
            $returnArray[$position]['productTypeId'] = $value['productTypeId'];
            $returnArray[$position]['name'] = ucwords(strtolower($value['name']));
            $returnArray[$position]['description'] = $value['description'];

            $position ++;
        }
        return $returnArray;
    }

    /**
     * Genneric function to save data in table
     *
     * @param array $data
     * @param string $tableName
     * @return int
     */
    protected function _save($data, $tableName)
    {
        $this->_sql->setTable($tableName);
        $action = $this->_sql->insert();
        $action->values($data);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();

        return $this->_dbAdapter->getDriver()
            ->getConnection()
            ->getLastGeneratedValue();
    }

    /**
     * Generic function to update tables
     *
     * $data format: array('fieldToUpdate' => 'newValue')
     * $whereClause format: array('whereFieldName' => 'whereFieldValue')
     * $whereNotEqualTo format: array(0 => array('whereFieldName' => 'whereFieldValue'))
     *
     * @param array $data
     * @param string $tableName
     * @param array $whereClause
     * @return int | bool
     */
    protected function _update($data, $tableName, $whereClause = array(), $whereNotEqualTo = array(), $whereLike = array())
    {
        if ($tableName == $this->_productsAttributesCombinationTable) {
            $this->_sql->setTable($this->_productsAttributesCombinationTable);
            $select = $this->_sql->select()->where($whereClause);
            $productAttributeCombination = $this->_sql->prepareStatementForSqlObject($select)
                ->execute()
                ->next();

            if (empty($productAttributeCombination)) {
                return false;
            }

            $this->_sql->setTable($this->_attributeGroupsTable);
            $select = $this->_sql->select()->where('attributeGroupId = ' . $productAttributeCombination['attributeGroupId']);
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $group = $statement->execute()->next();

            if ($group['viewAs'] == 'TEXT') {
                $update = $this->_sql->update();
                $update->table($this->_productsAttributesCombinationDefinitions);
                $update->set($data);
                $update->where(array(
                    'productAttributeCombinationId' => $productAttributeCombination['productAttributeCombinationId'],
                    'languageId' => $this->_currentLanguage
                ));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statementExecuted = $statement->execute();
            } else {
                $update = $this->_sql->update();
                $update->table($tableName);
                $update->set($data);
                if ((is_array($whereClause) && count($whereClause) > 0) || (is_string($whereClause) && trim($whereClause) != '')) {
                    $update->where($whereClause);
                }
                if (count($whereNotEqualTo) > 0) {
                    foreach ($whereNotEqualTo as $position => $whereNotEqualToString) {
                        $update->where->notEqualTo($whereNotEqualToString[0], $whereNotEqualToString[1]);
                    }
                }
                if (count($whereLike) > 0) {
                    $update->where->like($whereLike[0], $whereLike[1]);
                }

                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statementExecuted = $statement->execute();
            }
        } else {

            $update = $this->_sql->update();
            $update->table($tableName);
            $update->set($data);
            if ((is_array($whereClause) && count($whereClause) > 0) || (is_string($whereClause) && trim($whereClause) != '')) {
                $update->where($whereClause);
            }
            if (count($whereNotEqualTo) > 0) {
                foreach ($whereNotEqualTo as $position => $whereNotEqualToString) {
                    $update->where->notEqualTo($whereNotEqualToString[0], $whereNotEqualToString[1]);
                }
            }
            if (count($whereLike) > 0) {
                $update->where->like($whereLike[0], $whereLike[1]);
            }

            $statement = $this->_sql->prepareStatementForSqlObject($update);
            $statementExecuted = $statement->execute();

            $query = new Select();
            $query->from($tableName);
            if ((is_array($whereClause) && count($whereClause) > 0) || (is_string($whereClause) && trim($whereClause) != '')) {
                $query->where($whereClause);
            }
            if (count($whereNotEqualTo) > 0) {
                foreach ($whereNotEqualTo as $position => $whereNotEqualToString) {
                    $query->where->notEqualTo($whereNotEqualToString[0], $whereNotEqualToString[1]);
                }
            }
            if (count($whereLike) > 0) {
                $query->where->like($whereLike[0], $whereLike[1]);
            }

            $sql = new Sql($this->_dbAdapter);
            $stmtselect = $sql->prepareStatementForSqlObject($query);
            $result = $stmtselect->execute();

            foreach ($result as $key => $row) {
                if (isset($row['sku'])) {
                    $this->queueService->addSipEntryToQueue($row['sku'], 5, $this, false, true);
                }
                if (isset($row['productAttributeId'])) {
                    $this->queueService->addSpectrumEntryToQueueForCommonAttributes($row['productAttributeId']);
                }
            }
        }
        $affectedRows = $statementExecuted->getAffectedRows();
        return $affectedRows;
    }

    /**
     *
     * @param string $table
     * @param array $whereClause
     * @return int
     */
    protected function _delete($table, $whereClause)
    {
        $this->_sql->setTable($table);
        $action = $this->_sql->delete()->where($whereClause);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statementExecuted = $statement->execute();
        $affectedRows = $statementExecuted->getAffectedRows();
        return $affectedRows;
    }

    /**
     * Extract price information from data entered by the user.
     *
     * Get the currencies and price types.
     * Loop through to check if we have any set in data sent from the form
     *
     * @param array $productData
     * @return array
     */
    protected function _extractPriceInformation($productData = array())
    {
        $priceArray = array();
        if (count($productData) > 0) {
            $currencies = $this->getCurrencies();
            $priceTypes = $this->getPriceTypes();
            if (count($currencies) > 0 && count($priceTypes) > 0) {
                // Loop through currencies and pricetype then check if set in data sent from the form
                foreach ($currencies as $currencyKey => $currencyValue) {
                    foreach ($priceTypes as $priceTypeKey => $priceTypeValue) {
                        $priceKey = $priceTypeKey . ucfirst(strtolower($currencyValue['isoCode']));
                        if (isset($productData[$priceKey])) {
                            $priceArray[$currencyValue['currencyId']][$priceTypeKey] = $productData[$priceKey];
                        }
                    }
                }
            }
        }
        return $priceArray;
    }

    /**
     * Save product information entered by the user
     * Note: Product data are executed and saved in this order.
     * 1. Extract the price information from the submitted data
     * 2. Save product definitions
     * 3. Save tag information
     * 4. Save mis spell information
     * 5. Save prices
     *
     * @param array $productData
     * @param boolean $updateData
     * @param int $languageId
     * @return array
     */
    public function saveProductDetails($productData = array(), $updateData = false, $languageId = 1)
    {
        $this->_dbAdapter->getDriver()
            ->getConnection()
            ->beginTransaction();
        if (count($productData) > 0) {
            $savedProductId = $this->_saveProduct($productData, $updateData);
            if ($savedProductId > 0) {
                if ($updateData === false) {
                    // Save product definitions
                    $this->_saveProductDefinition($productData, $savedProductId);
                } else {
                    $this->_updateProductDefinition($productData, $savedProductId, $languageId);
                }
                // Extract prices from the data
                $productPrices = $this->_extractPriceInformation($productData);
                if (count($productPrices) > 0) {
                    // Save the product prices
                    $this->_saveProductPrices($productPrices, $savedProductId);
                }
                $taxesIds = explode(',', $productData['tax']);
                if (count($taxesIds) > 0) {
                    $this->_saveProductTaxes($taxesIds, $savedProductId, 0);
                }
                // Save in spectrum's table
                $this->_saveProductChannels($productData, $savedProductId);
                // Save in spectrum's table
                $this->_saveProductWebsites($productData, $savedProductId);
                // add it to the spectrum table
                $this->_saveProductSpectrum($productData, $savedProductId);
                // Check if care information is set so that we can save it.
                if (isset($productData['careInformation'])) {
                    $this->_saveProductTabs($productData['careInformation'], $savedProductId);
                }
                $this->_dbAdapter->getDriver()
                    ->getConnection()
                    ->commit();
                return array(
                    'success' => true,
                    'productId' => $savedProductId,
                    'productName' => $productData['productName']
                );
            }
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->rollback();
            return array(
                'success' => false
            );
        }
    }

    /**
     * Save variants for product
     *
     * @param type $params
     * @param type $productId
     * @return type
     */
    public function saveVariants($params, $productId)
    {
        $response = $this->_productsMapperVariants->_saveVariants($params, $productId);
        return $response;
    }

    /**
     * Get all variants for a product
     *
     * @param
     *            $productId
     * @param
     *            $notDeleted
     * @return type
     */
    public function getVariants($productId, $notDeleted = true)
    {
        $productVariants = $this->_productsMapperVariants->getProductVariantsByProductID($productId, $notDeleted);
        return array(
            'dataCount' => count($productVariants),
            'data' => $productVariants
        );
    }

    /**
     *
     * @param
     *            $productOrVariantId
     * @param
     *            $spectrumId
     * @param int $isVariant
     * @return int
     */
    public function saveSpectrumId($productOrVariantId, $spectrumId, $isVariant = 0)
    {
        return $this->_update(array(
            'spectrumId' => $spectrumId
        ), $this->_productsSpectrumTable, array(
            'productId' => $productOrVariantId,
            'productIdFrom' => (($isVariant == 0) ? $this->_tableName : $this->_productsVariantsRelationshipTable)
        ));
    }

    /**
     *
     * @param
     *            $productOrVariantId
     * @param
     *            $spectrumId
     * @param int $isVariant
     * @return bool
     */
    public function insertSpectrumId($productOrVariantId, $spectrumId, $isVariant = 1)
    {
        if ($isVariant == 1) {
            $productId = $this->getProductIdFromVariantId($productOrVariantId);
            $productIdFrom = 'PRODUCTS_R_ATTRIBUTES';
        } else {
            $productId = $productOrVariantId;
            $productIdFrom = 'PRODUCTS';
        }

        $productDetails = $this->getMinimumProductsDetails($productId);

        /*
         *Check if record already exist in table
         *      - We are checking if exact sme record exist if yes return true
         *      - If record for sku exist with different spectrum id then add it to db and notify client
         *      return 0 = not exist
         *      return 1 = exist with same spectrum id
         *      return 2 = exist with different spectrum id
         */
        $isSpectrumRecordExit = $this->isSpectrumRecordExist($productIdFrom, $productOrVariantId, $spectrumId);

        // record already exist with exact same spectrum Id
        if($isSpectrumRecordExit['exist'] =='1'){
            return true;
        }

        //record already exist with different spectrumId
        if($isSpectrumRecordExit['exist'] =='2'){
            //send notification email to client on duplicate spectrumIds
            try {
                $this->sendEmailDuplicateSpectrumId($isSpectrumRecordExit['spectrumtIds'], $productOrVariantId, $spectrumId, $productDetails['style']);
            } catch (Exception $e) {
               // continue
            }

        }

        $insertArray = array(
            'productIdFrom' => $productIdFrom,
            'productId' => $productOrVariantId,
            'spectrumId' => $spectrumId,
            'style' => $productDetails['style'],
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        );

        $this->_sql->setTable('PRODUCTS_R_SPECTRUM');
        $action = $this->_sql->insert();
        $action->values($insertArray);
        $statement = $this->_sql->prepareStatementForSqlObject($action);

        if ($statement->execute()) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Send email notification for multiple spectrum Id insertion
     * @param array $spectrumtIds
     * @param int $productOrVariantId
     * @param int $spectrumId
     * @param string $productStyle
     */
    protected function sendEmailDuplicateSpectrumId($spectrumtIds, $productOrVariantId, $spectrumId, $productStyle)
    {
        $headers = [
            'subject' => 'Duplicate Spectrum ID Insertion',
            'from' => 'contact@whistl.co.uk',
            'to' => [$this->config['bulk-error-report']['email_to_send'], 'hani.weiss@whistl.co.uk'],
        ];
        $template = 'prism-products-manager/email/duplicate-spectrum-id-insertion';
        $attachments = [];

        $variables = [
           'productStyle' => $productStyle,
            'productId'=> $productOrVariantId,
           'spectrumId'=> $spectrumId,
           'ExistingSpectrumIds' => $spectrumtIds
        ];

        try {
            $this->mailSender->sendMail($headers, $template, $variables, $attachments);
        } catch (\Exception $e) {
            // sending mail failed do not take any other action the error will be logged in error.log
        }
    }

    /**
     *Check if record already exist in table
     * @param
     *
     *      - We are checking if exact sme record exist if yes return true
     *      - If record for sku exist with different spectrum id then add it to db and notify client
     *      return 0 = not exist
     *      return 1 = exist with same spectrum id
     *      return 2 = exist with different spectrum id
     */
    protected function isSpectrumRecordExist($productIdFrom, $productOrVariantId, $spectrumId)
    {
        $checkResult = [
            'exist' => 0,
            'spectrumtIds' =>[]
        ];

        $sql = new Sql($this->_dbAdapter);

        $select = $sql->select();
        $select->columns(['spectrumId']);
        $select->from('PRODUCTS_R_SPECTRUM');
        $select->where->equalTo('productIdFrom', $productIdFrom);
        $select->where->equalTo('productId', $productOrVariantId);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $duplicationFound = false;
        $sameRecordFound = false;

        foreach ($result as $row) {

            $checkResult['spectrumtIds'][] = $row['spectrumId'];

            if($duplicationFound == false){
                $checkResult['exist'] = 2;
                $duplicationFound = true;
            }

            if($sameRecordFound == false){
                if($row['spectrumId'] == $spectrumId){
                    $checkResult['exist'] = 1;
                    $sameRecordFound = true;
                }
            }

        }
        return $checkResult;
    }

    /**
     * Save in PRODUCTS table
     *
     * @param array $productData
     * @param int $savedProductId
     * @param string $productIdFrom
     * @param int $productId
     * @return int
     */
    protected function _saveProductSpectrum($productData, $savedProductId, $productIdFrom = 'PRODUCTS', $productId = 0)
    {
        // if it is a variant we are saving in here, check if the parent product is already set up and delete it if so
        if ($productIdFrom == $this->_productsVariantsRelationshipTable && ! empty($productId)) {
            // we are dealing with a variant
            $this->_sql->setTable(array(
                'ps' => $this->_productsSpectrumTable
            ));
            $select = $this->_sql->select();
            $select->where(array(
                'ps.productId' => $productId,
                'ps.productIdFrom' => $this->_tableName
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            if ($results->count() > 0) {
                // we must delete the product first
                $this->_delete($this->_productsSpectrumTable, array(
                    'productId' => $productId,
                    'productIdFrom' => $this->_tableName
                ));
            }
            $this->_sql->setTable(array(
                'ps' => $this->_productsSpectrumTable
            ));
            $select = $this->_sql->select();
            $select->where(array(
                'ps.productId' => $savedProductId,
                'ps.productIdFrom' => $productIdFrom
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            if ($results->count() == 0) {
                $productsSpectrumEntity = new ProductsSpectrumEntity();
                $productData['productIdFrom'] = $productIdFrom;
                $productData['productId'] = $savedProductId;
                $hydratedData = $this->_hydrator->hydrate($productData, $productsSpectrumEntity);
                $data = $this->_hydrator->extract($hydratedData);
                $savedProductId = $this->_save($data, $this->_productsSpectrumTable);
            }
        } else {
            // we are dealing with a product
            // if product has variants already in this table or is set do not update it
            $this->_sql->setTable(array(
                'ps' => $this->_productsSpectrumTable
            ));
            $select = $this->_sql->select();
            $select->where(array(
                'ps.productId' => $savedProductId,
                'ps.productIdFrom' => $productIdFrom
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            if ($results->count() == 0) {
                // check if there are any variants of this product in the table
                $productVariants = $this->getVariants($savedProductId);
                if ($productVariants['dataCount'] > 0) {
                    return $savedProductId;
                }
                $productsSpectrumEntity = new ProductsSpectrumEntity();
                $productData['productIdFrom'] = $productIdFrom;
                $productData['productId'] = $savedProductId;
                $hydratedData = $this->_hydrator->hydrate($productData, $productsSpectrumEntity);
                $data = $this->_hydrator->extract($hydratedData);
                $savedProductId = $this->_save($data, $this->_productsSpectrumTable);
            }
        }

        return $savedProductId;
    }

    /**
     * Save the channels selected for the product
     *
     * @param
     *            $productData
     * @param
     *            $savedProductId
     * @param
     *            $productIdFrom
     * @return int
     */
    protected function _saveProductChannels($productData, $savedProductId, $productIdFrom = 'PRODUCTS')
    {
        $savedProductChannelsCount = 0;
        if (isset($productData['channels']) && is_array($productData['channels']) && count($productData['channels']) > 0) {
            $productsChannelsEntity = new ProductsChannelsEntity();
            $productChannelArray = array();
            foreach ($productData['channels'] as $key => $channelId) {
                $this->_sql->setTable(array(
                    'pch' => $this->_productsChannelsTable
                ));
                $select = $this->_sql->select();
                $select->where(array(
                    'productId' => $savedProductId,
                    'channelId' => $channelId,
                    'status' => 'ACTIVE'
                ));
                $select->limit(1);
                $statement = $this->_sql->prepareStatementForSqlObject($select);
                $results = $statement->execute();

                if ($results->count() == 0) {
                    // $productChannelArray['productIdFrom'] = $productIdFrom;
                    $productChannelArray['productId'] = $savedProductId;
                    $productChannelArray['channelId'] = $channelId;

                    $hydratedData = $this->_hydrator->hydrate($productChannelArray, $productsChannelsEntity);
                    $data = $this->_hydrator->extract($hydratedData);
                    $savedProductChannelId = $this->_save($data, $this->_productsChannelsTable);
                    if ($savedProductChannelId > 0) {
                        $savedProductChannelsCount ++;
                    }
                } else {
                    $savedProductChannelsCount ++;
                }
            }
        }
        return $savedProductChannelsCount;
    }

    /**
     * This function is used to save tab information for a product.
     * An example tab information is care information
     * tabGroupId 1 = care information
     *
     * @param type $params
     * @param type $productId
     */
    protected function _saveProductTabs($tabId, $productId, $tabGroupId = 1)
    {
        $tableName = $this->_productTabsTable;
        if ($tabId > 0 && $productId > 0) {
            $productTabsEntity = new ProductsTabsEntity();
            $tabArray = array(
                'tabId' => $tabId,
                'tabGroupId' => 1,
                'productId' => $productId
            );
            $hydratedData = $this->_hydrator->hydrate($tabArray, $productTabsEntity);
            $data = $this->_hydrator->extract($hydratedData);

            // lets try to update the data.
            // If it doesn't update, insert the data
            $unsetDataArray = $data;
            unset($unsetDataArray['created']);
            unset($unsetDataArray['productId']);
            unset($unsetDataArray['tabGroupId']);
            $fieldsToUpdate = $unsetDataArray;

            $whereClause = array(
                'productId' => $data['productId']
            );
            $rowsAffected = $this->_update($fieldsToUpdate, $tableName, $whereClause);
            if ($rowsAffected < 1) {
                $rowsAffected = $this->_save($data, $this->_productTabsTable);
            }
            return $rowsAffected;
        } else {
            $whereClause = array(
                'productId' => $productId
            );
            $this->_delete($tableName, $whereClause);
        }
    }

    /**
     * Save the websites where this product will be available
     *
     * @param
     *            $productData
     * @param
     *            $savedProductId
     * @param
     *            $productIdFrom
     * @return int
     */
    protected function _saveProductWebsites($productData, $savedProductId, $productIdFrom = 'PRODUCTS')
    {
        $savedProductWebsitesCount = 0;
        if (isset($productData['websites']) && is_array($productData['websites']) && count($productData['websites']) > 0) {
            $productsClientsWebsitesEntity = new ProductsClientsWebsitesEntity();
            $productClientsWebsitesArray = array();
            foreach ($productData['websites'] as $key => $websiteId) {
                $this->_sql->setTable(array(
                    'pch' => $this->_productsClientsWebsitesTable
                ));
                $select = $this->_sql->select();
                $select->where(array(
                    'productId' => $savedProductId,
                    'productIdFrom' => $productIdFrom,
                    'websiteId' => $websiteId
                ));
                $select->limit(1);
                $statement = $this->_sql->prepareStatementForSqlObject($select);
                $results = $statement->execute();

                if ($results->count() == 0) {
                    $productClientsWebsitesArray['productId'] = $savedProductId;
                    $productClientsWebsitesArray['websiteId'] = $websiteId;
                    $productClientsWebsitesArray['productIdFrom'] = $productIdFrom;

                    $hydratedData = $this->_hydrator->hydrate($productClientsWebsitesArray, $productsClientsWebsitesEntity);
                    $data = $this->_hydrator->extract($hydratedData);
                    $savedProductClientsWebsiteId = $this->_save($data, $this->_productsClientsWebsitesTable);
                    if ($savedProductClientsWebsiteId > 0) {
                        $savedProductWebsitesCount ++;
                    }
                } else {
                    $savedProductWebsitesCount ++;
                }
            }
        }
        return $savedProductWebsitesCount;
    }

    /**
     * Save in PRODUCTS table
     *
     * @param array $productData
     * @param
     *            $updateData
     * @return int
     */
    protected function _saveProduct($productData, $updateData = false)
    {
        $productsEntity = new ProductsEntity();
        // If sku is empty and style is not empty, set the style as sku
        if (! isset($productData['sku']) || (isset($productData['sku']) && trim($productData['sku']) == '')) {
            $productData['sku'] = $productData['style'];
        }
        $hydratedData = $this->_hydrator->hydrate($productData, $productsEntity);
        $data = $this->_hydrator->extract($hydratedData);

        // If there is product id in the data, it means we want to perform an update
        if ($updateData === true && isset($data['productId']) && $data['productId'] > 0) {
            $whereClause = array(
                'productId' => $data['productId']
            );

            // Unset some data we don't want to be updated here
            if (isset($data['status'])) {
                unset($data['status']);
            }
            if ($data['created']) {
                unset($data['created']);
            }
            if (isset($data['productMerchantCategoryId']) || $data['productMerchantCategoryId'] === null) {
                unset($data['productMerchantCategoryId']);
            }
            $fieldsToUpdate = $data;
            $table = $this->_tableName;
            // If the product data was updated successfully, log the action
            $productUpdated = $this->_update($fieldsToUpdate, $table, $whereClause);
            if ($productUpdated > 0) {
                $userId = $this->_loggedInUserId;
                $actionName = 'Updated products data';
                $actionData = $data;
                $this->_actionLogger->logCurrentAction($userId, $actionName, $actionData, $table);
            }

            return $data['productId'];
        }
        // If the new product data was saved successfully, log the action
        $savedProductId = $this->_save($data, $this->_tableName);
        if ($savedProductId > 0) {
            $userId = $this->_loggedInUserId;
            $actionName = 'Saved new product data';
            $actionData = $data;
            $this->_actionLogger->logCurrentAction($userId, $actionName, $actionData, $this->_tableName);
        }
        return $savedProductId;
    }

    /**
     *
     * @param int $productId
     * @param data $params
     *            productId and $params array to save product add on
     *
     * @return array|mixed
     */
    public function saveProductAddOn($params)
    {
        $response = false;
        $productId = $params['productId'];
        $searchVariant = $this->getProductIdFromSku($params['product-add-on-search']);

        if (isset($searchVariant[1]['variantId']) && ! empty($searchVariant[1]['variantId'])) {
            $variantIdOrProductAddOnId = $searchVariant[1]['variantId'];
            $variantIdFrom = 'PRODUCTS_R_ATTRIBUTES';
        } else {
            $variantIdOrProductAddOnId = $searchVariant[0]['productId'];
            $variantIdFrom = 'PRODUCTS';
        }

        foreach ($params['add-on-variants-select'] as $variantOrProductId) {

            $this->_sql->setTable($this->_productsAddOnTable);
            $action = $this->_sql->insert();
            $action->values(array(
                'productId' => $productId,
                'productVariantId' => $variantOrProductId,
                'productOrVariant' => $params['selectedProductIdFrom'],
                'productAddOnId' => $variantIdOrProductAddOnId,
                'addOnProductOrVariant' => $variantIdFrom
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($action);

            if ($statement->execute()) {
                $response = true;
            } else {
                $response = false;
            }
        }

        return $response;
    }

    /**
     *
     * @param int $productId
     *            Takes in product ID and returns all product add ons associated with this product ID
     *            Then uses returned values to get specific product details (sku)
     * @return array|mixed
     */
    public function getProductAddOnsByVariantId($variantId)
    {
        $response = false;
        $this->_sql->setTable($this->_productsAddOnTable);
        $select = $this->_sql->select();

        $select->where(array(
            "productVariantId" => $variantId
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        foreach ($result as $rows) {
            if ($rows['productOrVariant'] == 'PRODUCTS_R_ATTRIBUTES') {
                $sku = $this->getVariantsSkus($rows['productAddOnId']);
                $rows['sku'] = $sku[0];
            } else
                if ($rows['productOrVariant'] == 'PRODUCTS') {
                    $sku = $this->getProductSku($rows['productId']);
                    $rows['sku'] = $sku[0];
                }

            $response[] = $rows;
        }
        return $response;
    }

    /**
     *
     * @param int  $id (productId/variantId)
     *
     *            Takes in product ID and returns all product add ons associated with this product ID
     *            Then uses retured values to get specific product details (sku)
     * @return array|mixed
     */
    public function getProductAddOnsByID($id)
    {
        $response = false;
        $this->_sql->setTable($this->_productsAddOnTable);
        $select = $this->_sql->select();

        $select->where(array(
            "productVariantId" => $id
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        foreach ($result as $rows) {
            $return = false;
            if ($rows['addOnProductOrVariant'] == 'PRODUCTS_R_ATTRIBUTES') {
                $productId = $this->getProductIdFromVariantId($rows['productAddOnId']);
                $return['addonId'] = $productId . "_" . $rows['productAddOnId'];
            } else
                if ($rows['addOnProductOrVariant'] == 'PRODUCTS') {
                    $return['addonId'] = $rows['productAddOnId'] . "_0";
                }
            $response[] = $return;
        }
        return $response;
    }

    /**
     *
     * Returns all add ons related to product specified in $productId
     *
     * @param int $productId
     * @return array
     */
    public function getAllVariantsWithAddOns($productId)
    {
        $response = false;
        $this->_sql->setTable($this->_productsAddOnTable);
        $select = $this->_sql->select();
        $select->columns(array(
            'ID',
            'productId',
            'productVariantId',
            'productOrVariant'
        ));
        $select->where(array(
            "productId" => $productId
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $usedVariants = array();

        foreach ($result as $rows) {
            $productId = $rows['productVariantId'];
            if (! in_array($productId, $usedVariants)) {

                if ($rows['productOrVariant'] == 'PRODUCTS_R_ATTRIBUTES') {
                    $sku = $this->getVariantsSkus($rows['productVariantId']);
                    $rows['sku'] = $sku[0];
                } else
                    if ($rows['productOrVariant'] == 'PRODUCTS') {
                        $sku = $this->getProductSku($rows['productVariantId']);
                        $rows['sku'] = $sku[0];
                    }
                $response[] = $rows;
                array_push($usedVariants, $productId);
            }
        }
        return $response;
    }

    /**
     *
     * @param int $productId
     * @param string $productFrom
     * @param null $status
     * @param array $columns
     * @return array|bool
     */
    public function getAllProductsTabs($productId, $productFrom = 'PRODUCTS', $status = null, $columns = array('*'))
    {
        $productTabs = array();

        $this->_sql->setTable($this->_productsTabsTable);
        $select = $this->_sql->select();
        $select->columns($columns);
        $select->where(array(
            "productId" => $productId
        ));
        $select->where(array(
            "productIdFrom" => $productFrom
        ));
        $select->where(array(
            "status != 'DELETED'"
        ));

        if (! is_null($status)) {
            $select->where(array(
                "status = '" . $status . "'"
            ));
        }

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $rows = $statement->execute();

        foreach ($rows as $row) {
            $productTabs[] = $row;
        }
        return empty($productTabs) ? true : $productTabs;
    }

    /**
     *
     * @param array $data
     * @param string $productIdFrom
     */
    public function insertNewProductTab($data, $productIdFrom = 'PRODUCTS')
    {
        $this->_sql->setTable($this->_productsTabsTable);
        $action = $this->_sql->insert();
        $action->values(array(
            'productId' => $data['productId'],
            'productTabTitle' => $data['product-tab-title'],
            'productTabContent' => $data['product-tab-content'],
            'productIdFrom' => $productIdFrom
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     *
     * @param array $data
     * @param string $productIdFrom
     */
    public function updateProductTab($data, $productIdFrom = 'PRODUCTS')
    {
        $updateData = array(
            'productTabTitle' => $data['product-tab-title'],
            'productTabContent' => $data['product-tab-content']
        );
        $whereClause = array(
            'productId' => $data['productId'],
            'productIdFrom' => $productIdFrom,
            'productTabId' => $data['productTabId']
        );
        $update = $this->_sql->update();
        $update->table($this->_productsTabsTable);
        $update->set($updateData);
        $update->where($whereClause);
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     *
     * Removes product add on based on $addOnId
     *
     * @param int $addOnId
     * @return true
     */
    public function removeAddOnProduct($addOnId)
    {
        $response = false;

        if (isset($addOnId)) {
            $this->_sql->setTable($this->_productsAddOnTable);
            $action = $this->_sql->delete();
            $action->where(array(
                "ID" => $addOnId
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            if ($statement->execute()) {
                $response = true;
            }
        }

        return $response;
    }

    /**
     *
     * @param int $productId
     * @param int $languageId
     * @return array|mixed
     */
    protected function _getProductDefinitions($productId, $languageId = false, $status = false)
    {
        $this->_sql->setTable(array(
            'pdt' => $this->_productsDefinitionsTable
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'productId',
            'productIdFrom',
            'versionId',
            'languageId',
            'versionDescription',
            'productDefinitionsId',
            'shortProductName',
            'productName',
            'urlName',
            'metaTitle',
            'metaKeyword',
            'metaDescription',
            'shortDescription',
            'longDescription',
            'makeLive',
            'status'
        ));
        if ($languageId === false) {
            $select->where(array(
                'productId' => $productId,
                'productIdFrom' => $this->_tableName
            ));
            if ($status) {
                $select->where('status != "DELETED"');
            } else {
                $select->where('status = "ACTIVE"');
            }
        } else {
            $select->where(array(
                'productId' => $productId,
                'productIdFrom' => $this->_tableName,
                'languageId' => $languageId,
                'status' => 'ACTIVE'
            ));
            $select->limit(1);
        }

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $productDefinition = array();
        foreach ($results as $key => $value) {
            if (isset($value)) {
                if ($languageId === false) {
                    $productDefinition[] = $value;
                } else {
                    $productDefinition = $value;
                }
            }
        }
        return $productDefinition;
    }

    /**
     *
     * @param int $taxesIds
     * @param int $productOrVariantId
     * @param int $isVariant
     * @return bool
     */
    protected function _saveProductTaxes($taxesIds, $productOrVariantId, $isVariant = 0)
    {
        $this->_taxMapper->addTaxForProduct($taxesIds, $productOrVariantId, $isVariant);
        return true;
    }

    /**
     * Update product definitions
     *
     * @param array $productData
     * @param int $productId
     * @param int $languageId
     * @return mixed
     */
    protected function _updateProductDefinition($productData, $productId, $languageId)
    {
        $productDefinitionEntity = new ProductsDefinitionsEntity();
        $hydratedData = $this->_hydrator->hydrate($productData, $productDefinitionEntity);
        $data = $this->_hydrator->extract($hydratedData);
        $productDefinitionId = $data['productDefinitionsId'];
        // Unset some fields we don't want to update here.
        unset($data['created']);
        unset($data['productDefinitionsId']);
        unset($data['modified']);
        unset($data['productId']);
        unset($data['languageId']);
        unset($data['versionId']);
        unset($data['versionDescription']);
        unset($data['status']);

        $fieldsToUpdate = $data;
        $tableName = $this->_productsDefinitionsTable;
        $whereClause = array(
            'productId' => $productId,
            'productIdFrom' => $this->_tableName,
            'languageId' => $languageId,
            'productDefinitionsId' => $productDefinitionId
        );
        $rowUpdated = $this->_update($fieldsToUpdate, $tableName, $whereClause);

        // If row was updated successfully, log the action
        if ($rowUpdated > 0) {
            $userId = $this->_loggedInUserId;
            $actionName = 'Updated products description';
            $this->_actionLogger->logCurrentAction($userId, $actionName, $productData, $tableName);
        }

        return $rowUpdated;
    }

    /**
     * Deactivate product definition
     *
     * @param int $productDefinitionId
     * @return mixed
     */
    protected function _deactivateExistingProductDefinition($productDefinitionId)
    {
        $whereClause = array(
            'productDefinitionsId' => $productDefinitionId,
            'status' => 'ACTIVE'
        );
        $fieldsToUpdate = array(
            'status' => 'INACTIVE'
        );
        $table = $this->_productsDefinitionsTable;
        $productDefinitionDeactivated = $this->_update($fieldsToUpdate, $table, $whereClause);
        return $productDefinitionDeactivated;
    }

    /**
     * Deactivate marketing category
     *
     * @param int $productId
     * @param int $categoryId
     * @return mixed
     */
    public function deleteMarketingCategory($productId, $categoryId, $isVariant = false)
    {
        // Set all isDefault flag to 0 for all categories that have been assigned to this product ID
        $productIdFrom = ($isVariant === true) ? $this->_productsVariantsRelationshipTable : $this->_tableName;
        $whereClause = array(
            'productId' => $productId,
            'categoryId' => $categoryId,
            'productIdFrom' => $productIdFrom
        );
        $fieldsToUpdate = array(
            'status' => 'DELETED'
        );
        $table = $this->_productsCategoriesRelationshipTable;
        $productCategoryDeactivated = $this->_update($fieldsToUpdate, $table, $whereClause);

        // If default category was changed, log the action
        if ($productCategoryDeactivated > 0) {
            $userId = $this->_loggedInUserId;
            $actionName = 'Deactivated product marketing category';
            $actionData = array(
                $productId,
                $categoryId
            );
            $this->_actionLogger->logCurrentAction($userId, $actionName, $actionData, $table);
        }

        return $productCategoryDeactivated;
    }

    /**
     * Deactivate marketing category
     *
     * @param int $productId
     * @param int $categoryId
     * @return type
     */
    public function deactivateMarketingCategory($productId, $categoryId, $isVariant = false)
    {
        // Set all isDefault flag to 0 for all categories that have been assigned to this product ID
        $productIdFrom = ($isVariant === true) ? $this->_productsVariantsRelationshipTable : $this->_tableName;
        $whereClause = array(
            'productId' => $productId,
            'categoryId' => $categoryId,
            'productIdFrom' => $productIdFrom
        );
        $fieldsToUpdate = array(
            'status' => 'INACTIVE'
        );
        $table = $this->_productsCategoriesRelationshipTable;
        $productCategoryDeactivated = $this->_update($fieldsToUpdate, $table, $whereClause);

        // If default category was changed, log the action
        if ($productCategoryDeactivated > 0) {
            $userId = $this->_loggedInUserId;
            $actionName = 'Deactivated product marketing category';
            $actionData = array(
                $productId,
                $categoryId
            );
            $this->_actionLogger->logCurrentAction($userId, $actionName, $actionData, $table);
        }

        return $productCategoryDeactivated;
    }

    /**
     * Set a category as default category
     *
     * @param type $productId
     * @param type $categoryId
     * @return type
     */
    public function setAsDefaultCategory($productId, $categoryId, $isVariant = false)
    {
        // Set all isDefault flag to 0 for all categories that have been assigned to this product ID
        $productIdFrom = ($isVariant === true) ? $this->_productsVariantsRelationshipTable : $this->_tableName;
        $whereClause = array(
            'productId' => $productId,
            'productIdFrom' => $productIdFrom
        );
        $fieldsToUpdate = array(
            'isDefault' => 0
        );
        $table = $this->_productsCategoriesRelationshipTable;
        $this->_update($fieldsToUpdate, $table, $whereClause);

        // Set the selected category id as default
        $whereClause = array(
            'productId' => $productId,
            'categoryId' => $categoryId,
            'status' => 'ACTIVE',
            'productIdFrom' => $productIdFrom
        );
        $fieldsToUpdate = array(
            'isDefault' => 1
        );
        $table = $this->_productsCategoriesRelationshipTable;
        $setAsDefaultCategory = $this->_update($fieldsToUpdate, $table, $whereClause);

        // If default category was changed, log the action
        if ($setAsDefaultCategory > 0) {
            $userId = $this->_loggedInUserId;
            $actionName = 'Changed default product marketing category';
            $actionData = array(
                $productId,
                $categoryId
            );
            $this->_actionLogger->logCurrentAction($userId, $actionName, $actionData, $table);
        }

        return $setAsDefaultCategory;
    }

    /**
     *
     * @param
     *            $productId
     * @param
     *            $categoryId
     */
    public function _setAsDefaultCategoryForVariants($productId, $categoryId)
    {
        $this->_sql->setTable($this->_productsVariantsRelationshipTable);
        $action = $this->_sql->select()
            ->join(array(
                'prc' => $this->_productsCategoriesRelationshipTable
            ), // Set join table and alias
                new Expression('prc.productId = ' . $this->_productsVariantsRelationshipTable . '.productAttributeId
                    AND prc.categoryId = ' . $categoryId . ' AND `prc`.status != "DELETED"'), array(
                    'addedToCategory' => new Expression('IFNULL(prc.productId,0)')
                ), Select::JOIN_LEFT)
            ->where($this->_productsVariantsRelationshipTable . '.productId = ' . $productId)
            ->where($this->_productsVariantsRelationshipTable . '.status != "DELETED"');
        $statement = $this->_sql->prepareStatementForSqlObject($action);

        $results = $statement->execute();

        foreach ($results as $result) {
            if ($result['addedToCategory'] == 0) {
                // add first the variant to the category
                $this->_sql->setTable($this->_productsCategoriesRelationshipTable);
                $action = $this->_sql->insert();
                $action->values(array(
                    'productId' => $result['productAttributeId'],
                    'categoryId' => $categoryId,
                    'isDefault' => 0,
                    'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                    'status' => 'active'
                ));
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
                $this->setAsDefaultCategory($result['productAttributeId'], $categoryId, true);
            } else {
                // set the category as default
                $this->setAsDefaultCategory($result['productAttributeId'], $categoryId, true);
            }
        }
    }

    /**
     * For each language,
     * 1.
     * Save product definitions
     * 2. Save tags
     * 3. Save mis spells
     *
     * @param array $productData
     * @param int $productId
     * @return int
     */
    protected function _saveProductDefinition($productData, $productId)
    {
        $productDefinitionEntity = new ProductsDefinitionsEntity();
        $definitionsSavedCount = 0;
        if (count($this->_languages) > 0) {
            foreach ($this->_languages as $key => $languageId) {
                $productData['productId'] = $productId;
                $productData['languageId'] = $languageId;
                // Convert the date time to a format the database will understand
                if (isset($productData['makeLive']) && trim($productData['makeLive']) != '') {
                    $makeLiveString = strtotime($productData['makeLive']);
                    // $productData['makeLive'] = new \Zend\Db\Sql\Expression($productData['makeLive']);
                    $productData['makeLive'] = date('Y-m-d H:i:s', $makeLiveString);
                }
                $hydratedData = $this->_hydrator->hydrate($productData, $productDefinitionEntity);
                $data = $this->_hydrator->extract($hydratedData);
                $data['productIdFrom'] = $this->_tableName;
                $this->_save($data, $this->_productsDefinitionsTable);

                // Save product tags if any
                if (isset($productData['tags']) && trim($productData['tags']) != '') {
                    $this->_saveProductTags($productData['tags'], $productId, $languageId);
                }
                // Save product mis spells if any
                if (isset($productData['misSpells']) && trim($productData['misSpells']) != '') {
                    $this->_saveProductMisSpells($productData['misSpells'], $productId, $languageId);
                }
                $definitionsSavedCount ++;
            }
        }
        return $definitionsSavedCount;
    }

    /**
     * Save relationship between products and marketing categories
     *
     * @param type $productCategories
     * @param type $productId
     * @return int
     */
    public function _saveProductCategories($productCategories = array(), $productId, $variantId = 0)
    {
        $savedProductCategoriesCount = 0;

        if (count($productCategories) > 0) {
            $productCategoriesEntity = new ProductsCategoriesEntity();
            $productCategoryArray = array();
            foreach ($productCategories as $key => $categoryId) {
                if ($categoryId > 0) {
                    $productId = $variantId > 0 ? $variantId : $productId;
                    // Check if this category has been assigned to this product/variant so we skip adding it again
                    $productCategoryExists = $this->checkIfProductCategoryExists($categoryId, $productId, 0);

                    if ($productCategoryExists > 0) {
                        continue;
                    }

                    $productCategoryArray['productIdFrom'] = $this->_tableName;
                    if ($variantId > 0) {
                        $productCategoryArray['productIdFrom'] = $this->_productsVariantsRelationshipTable;
                    }

                    // Save the variants against the product/variant
                    $productCategoryArray['productId'] = $productId;
                    $productCategoryArray['categoryId'] = $categoryId;
                    // $productCategoryArray['productIdFrom'] = $this->_productsVariantsRelationshipTable;
                    $hydratedData = $this->_hydrator->hydrate($productCategoryArray, $productCategoriesEntity);
                    $data = $this->_hydrator->extract($hydratedData);

                    $savedProductCategoryId = $this->_save($data, $this->_productsCategoriesRelationshipTable);
                    if ($savedProductCategoryId > 0) {
                        $savedProductCategoriesCount ++;
                    }

                    // If the main category was saved against a main product and not a variant,
                    // cascade saving this category for all variant of this main product
                    if ($variantId == 0) {
                        // get the variants of this main product
                        $productVariants = $this->getVariants($productId);

                        if (count($productVariants) > 0 && isset($productVariants['dataCount']) && $productVariants['dataCount'] > 0 && isset($productVariants['data']) && count($productVariants['dataCount']) > 0) {
                            foreach ($productVariants['data'] as $variantposition => $variantArray) {
                                if (isset($variantArray['productAttributeId'])) {
                                    // Check if this category has been assigned to this variant so we skip adding it again
                                    $productCategoryExists = $this->checkIfProductCategoryExists($categoryId, $variantArray['productAttributeId'], 1);
                                    if ($productCategoryExists > 0) {
                                        continue;
                                    }
                                    $productCategoryArray['productId'] = $variantArray['productAttributeId'];
                                    $productCategoryArray['categoryId'] = $categoryId;
                                    $productCategoryArray['productIdFrom'] = $this->_productsVariantsRelationshipTable;
                                    $hydratedData = $this->_hydrator->hydrate($productCategoryArray, $productCategoriesEntity);
                                    $data = $this->_hydrator->extract($hydratedData);
                                    $savedProductCategoryId = $this->_save($data, $this->_productsCategoriesRelationshipTable);
                                    if ($savedProductCategoryId > 0) {
                                        $savedProductCategoriesCount ++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $savedProductCategoriesCount;
    }

    /**
     * Check if a category has been assigned to a product and is active
     *
     * @param
     *            $categoryId
     * @param
     *            $productId
     * @return type
     */
    protected function checkIfProductCategoryExists($categoryId = 0, $productId = 0, $isVariant = 0)
    {
        $productCategoryId = 0;

        if ($isVariant == 0) {
            $tableName = $this->_tableName;
        } else {
            $tableName = $this->_productsVariantsRelationshipTable;
        }
        if ($categoryId > 0 && $productId > 0) {
            $this->_sql->setTable(array(
                'pcr' => $this->_productsCategoriesRelationshipTable
            ));
            $select = $this->_sql->select();
            $select->columns(array(
                'productCategoryId'
            ));
            $select->where(array(
                'pcr.productId' => $productId,
                'pcr.categoryId' => $categoryId,
                'pcr.productIdFrom' => $tableName,
                'pcr.status' => 'ACTIVE'
            ));
            $select->limit(1);

            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            foreach ($results as $key => $value) {
                if (isset($value['productCategoryId'])) {
                    $productCategoryId = $value['productCategoryId'];
                }
            }
        }

        return $productCategoryId;
    }

    /**
     * Save product prices in PRODUCT_Prices table.
     *
     * @param
     *            $productPrices
     * @param
     *            $savedProductId
     * @param
     *            $isVariant
     * @return int
     */
    protected function _saveProductPrices($productPrices, $savedProductId, $isVariant = 0)
    {
        $productPricesEntity = new ProductsPricesEntity();

        $productPriceArray = array();
        $savedProductPricesCount = 0;
        $productVariants = $this->_productsMapperVariants->getProductVariantsByProductID($savedProductId);
        $productVariantsIds = array();
        foreach ($productVariants as $variantArray) {
            $productVariantsIds[] = $variantArray['productAttributeId'];
        }
        foreach ($productPrices as $currencyId => $priceArray) {
            if ($currencyId > 0 && is_array($priceArray) && count($priceArray) > 0) {
                $productPriceArray['productOrVariantId'] = $savedProductId;
                $productPriceArray['productIdFrom'] = ($isVariant == 0) ? $this->_tableName : $this->_productsVariantsRelationshipTable;
                $productPriceArray['currencyId'] = $currencyId;
                $productPriceArray['price'] = $priceArray['nowPrice'];
                $productPriceArray['nowPrice'] = $priceArray['nowPrice'];
                $productPriceArray['offerPrice'] = $priceArray['offerPrice'];
                $productPriceArray['wasPrice'] = $priceArray['wasPrice'];
                $productPriceArray['costPrice'] = $priceArray['costPrice'];
                $productPriceArray['tradingCostPrice'] = $priceArray['tradingCostPrice'];
                $productPriceArray['wholesalePrice'] = $priceArray['wholesalePrice'];
                $productPriceArray['freightPrice'] = $priceArray['freightPrice'];
                $productPriceArray['dutyPrice'] = $priceArray['dutyPrice'];
                $productPriceArray['designPrice'] = $priceArray['designPrice'];
                $productPriceArray['packagingPrice'] = $priceArray['packagingPrice'];

                $continue = true;

                // Check if this product has prices already set for each currency
                $productPricesExistsArray = $this->_productsMapperPrices->_checkIfProductPricesExists($productPriceArray['productOrVariantId'], $productPriceArray['currencyId'], $productPriceArray['productIdFrom']);

                if ($productPricesExistsArray !== false) {
                    if ($productPriceArray['price'] == 'not_set') {
                        // we need to deactivate the prices for this currency if they already exist for the
                        // main product and all the variants
                        // if not set ignore the prices for this currency since they don't apply for this product
                        $this->_productsMapperPrices->_deactivateProductPrices($productPriceArray['productOrVariantId'], $productPriceArray['currencyId'], $productPriceArray['productIdFrom']);
                        foreach ($productVariantsIds as $variantId) {
                            $this->_productsMapperPrices->_deactivateProductPrices($variantId, $productPriceArray['currencyId'], $this->_productsVariantsRelationshipTable);
                        }
                        continue;
                    }
                    $identicalPriceData = true;
                    if (is_array($productPricesExistsArray) && count($productPricesExistsArray) > 0) {
                        // Compare the prices if they are the same
                        foreach ($productPricesExistsArray as $priceKey => $existingPriceValue) {
                            if (isset($productPriceArray[$priceKey]) && $productPriceArray[$priceKey] != $existingPriceValue) {
                                $identicalPriceData = false;
                                break;
                            }
                        }
                        // If they are not the same, deactivate the existing price else skip furthe actions
                        if ($identicalPriceData === false) {
                            $existingPriceUpdated = $this->_productsMapperPrices->_deactivateProductPrices($productPriceArray['productOrVariantId'], $productPriceArray['currencyId'], $productPriceArray['productIdFrom']);
                            // Id deactivation was successful, set the flag to insert the new price
                            if ($existingPriceUpdated < 1) {
                                $continue = false;
                            }
                        } else {
                            continue;
                        }
                    }
                }

                if ($continue === true && $productPriceArray['price'] != 'not_set') {
                    $hydratedData = $this->_hydrator->hydrate($productPriceArray, $productPricesEntity);
                    $data = $this->_hydrator->extract($hydratedData);
                    $savedProductPriceId = $this->_save($data, $this->_productsPricesTable);
                    if ($savedProductPriceId > 0) {
                        $savedProductPricesCount ++;
                    }

                    // check if all the variants have the new added currency and if not insert it
                    foreach ($productVariantsIds as $variantId) {
                        $variantPricesExistsArray = $this->_productsMapperPrices->_checkIfProductPricesExists($variantId, $productPriceArray['currencyId'], $this->_productsVariantsRelationshipTable);
                        if ($variantPricesExistsArray === false) {
                            // if does not exist, insert it
                            $variantPricesInsertArray = array(
                                'price' => $productPriceArray['nowPrice'],
                                'offerPrice' => $productPriceArray['offerPrice'],
                                'nowPrice' => $productPriceArray['nowPrice'],
                                'wasPrice' => $productPriceArray['wasPrice'],
                                'costPrice' => $productPriceArray['costPrice'],
                                'tradingCostPrice' => $productPriceArray['tradingCostPrice'],
                                'wholesalePrice' => $productPriceArray['wholesalePrice'],
                                'freightPrice' => $productPriceArray['freightPrice'],
                                'dutyPrice' => $productPriceArray['dutyPrice'],
                                'designPrice' => $productPriceArray['designPrice'],
                                'packagingPrice' => $productPriceArray['packagingPrice']
                            );
                            $this->_productsMapperPrices->insertNewVariantPrices($variantId, $productPriceArray['currencyId'], $variantPricesInsertArray);
                        }
                    }
                }
            }
        }
        return $savedProductPricesCount;
    }

    /**
     * Get Spectrum Product Id
     *
     * Returns the Spectrum Product Id depends of the PMS product / variant id.
     * If variant id, then $productIdFrom needs to be PRODUCTS_R_ATTRIBUTES
     *
     * @param integer $productId
     * @param string $productIdFrom
     * @return integer
     */
    public function getSpectrumId($productId, $productIdFrom = 'PRODUCTS')
    {
        $returnArray = array();
        $this->_sql->setTable($this->_productsSpectrumTable);
        $select = $this->_sql->select();
        $select->columns(array(
            'spectrumId'
        ));
        $select->where(array(
            'productId' => $productId,
            'productIdFrom' => $productIdFrom
        ))->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $returnArray[] = $val['spectrumId'];
            break;
        }
        return isset($returnArray[0]) ? $returnArray[0] : 0;
    }

    /**
     *
     * @param
     *            $spectrumId
     * @return bool
     */
    public function getProductBySpectrumId($spectrumId)
    {
        $returnArray = array();
        $this->_sql->setTable($this->_productsSpectrumTable);
        $select = $this->_sql->select();
        $select->where(array(
            'spectrumId' => $spectrumId
        ))->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $returnArray[] = array(
                'productId' => $val['productId'],
                'productIdFrom' => $val['productIdFrom']
            );
            break;
        }
        return isset($returnArray[0]) ? $returnArray[0] : false;
    }

    /**
     * Delete Selected Products
     * Update status to DELETED for selected products.
     *
     * @param array $selectedProducts
     * @return int
     */
    public function deleteSelectedProducts(array $selectedProducts)
    {
        $deletedProductsCount = 0;
        $table = $this->_tableName;
        $fieldsToUpdate = array(
            'status' => 'DELETED'
        );

        foreach ($selectedProducts as $productId) {
            $whereClause = array(
                'productId' => $productId
            );
            $variants = $this->getVariants($productId, false);
            $productsDeleted = $this->_update($fieldsToUpdate, $table, $whereClause);
            if ($productsDeleted > 0) {
                // If main product was deleted, remove it from being a related product
                $relatedProductDeleted = $this->deleteRelatedProductOrVariants($productId);
                $this->deleteProductDefinitionsForVariantId($productId); // Delete the definition of the product
                $this->deleteProductsSpectrum($productId, $table); // Delete the style in the local spectrum table
            }
            if ($variants['dataCount']) {
                // Create array with variants Ids and set each variant to be inactive in Spectrum
                foreach ($variants['data'] as $variantValue) {
                    $variantsIds[] = $variantValue['productAttributeId'];
                }
                $this->deleteSelectedVariants($variantsIds);
            }
            if ($productsDeleted > 0) {
                $deletedProductsCount ++;
            }
        }
        return $deletedProductsCount;
    }

    /**
     *
     * @param
     *            $selectedVariants
     * @return int
     */
    public function deleteSelectedVariants($selectedVariants)
    {
        $deletedVariantsCount = 0;
        $table = $this->_productsVariantsRelationshipTable;
        $fieldsToUpdate = array(
            'status' => 'DELETED'
        );

        foreach ($selectedVariants as $variantKey => $variantId) {
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->beginTransaction();
            $whereClause = array(
                'productAttributeId' => $variantId
            );
            $variantDeleted = $this->_update($fieldsToUpdate, $table, $whereClause);

            if ($variantDeleted > 0) {
                // If main product was deleted, remove it from being a related product
                $relatedProductDeleted = $this->deleteRelatedProductOrVariants($variantId);
                $this->deleteAttributesForVariantId($variantId);
                $this->deleteVariantDefinitionsForVariantId($variantId);
                $this->deleteSkuRules($variantId);
                $this->deleteMediaProducts($variantId);
                $this->deleteProductPrices($variantId);
                $this->deleteProductCategories($variantId);
                $this->deleteProductsClientsWebsites($variantId);
                $this->deleteProductsSpectrum($variantId);
                $this->deleteProductsTax($variantId);
            }

            if ($variantDeleted > 0) {
                $deletedVariantsCount ++;
            }
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->commit();
        }
        return $deletedVariantsCount;
    }

    /**
     *
     * @param
     *            $productId
     */
    public function deleteProductDefinitionsForVariantId($productId)
    {
        $updateArray = array(
            'status' => 'DELETED'
        );
        $where = array(
            'productId' => $productId,
            'productIdFrom' => $this->_tableName
        );
        $this->_update($updateArray, $this->_productsDefinitionsTable, $where);
    }

    /**
     *
     * @param
     *            $variantId
     */
    public function deleteProductsTax($variantId)
    {
        $this->_sql->setTable('PRODUCTS_R_TAXES');
        $action = $this->_sql->delete()
            ->where('productOrVariantId =' . $variantId)
            ->where('isVariant = 1');

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     *
     * @param
     *            $productOrVariantId
     * @param string $productIdFrom
     */
    public function deleteProductsSpectrum($productOrVariantId, $productIdFrom = "PRODUCTS_R_ATTRIBUTES")
    {
        $this->_sql->setTable('PRODUCTS_R_SPECTRUM');
        $action = $this->_sql->delete()
            ->where('productId =' . $productOrVariantId)
            ->where("productIdFrom = '{$productIdFrom}'");

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     *
     * @param
     *            $variantId
     */
    public function deleteProductsClientsWebsites($variantId)
    {
        $this->_sql->setTable('PRODUCTS_R_CLIENTS_Websites');
        $action = $this->_sql->delete()
            ->where('productId =' . $variantId)
            ->where('productIdFrom = "PRODUCTS_R_ATTRIBUTES"');

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     *
     * @param
     *            $variantId
     */
    public function deleteProductCategories($variantId)
    {
        $updateArray = array(
            'status' => 'DELETED'
        );
        $where = array(
            'productId' => $variantId,
            'productIdFrom' => "PRODUCTS_R_ATTRIBUTES",
            'status' => 'ACTIVE'
        );
        $this->_update($updateArray, 'PRODUCTS_R_CATEGORIES', $where);
    }

    /**
     *
     * @param
     *            $variantId
     */
    public function deleteProductPrices($variantId)
    {
        $updateArray = array(
            'status' => 'DELETED'
        );
        $where = array(
            'productOrVariantId' => $variantId,
            'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES'
        );
        $this->_update($updateArray, 'PRODUCTS_Prices', $where);
    }

    /**
     *
     * @param
     *            $variantId
     */
    public function deleteMediaProducts($variantId)
    {
        $where = array(
            'variantId' => $variantId
        );
        $this->_delete('MEDIA_Products', $where);
    }

    /**
     *
     * @param
     *            $variantId
     */
    public function deleteSkuRules($variantId)
    {
        $this->_sql->setTable('PRODUCTS_R_ATTRIBUTES_R_SKU_Rules');
        $action = $this->_sql->delete()->where('productAttributeId = ' . $variantId);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     *
     * @param
     *            $variantId
     */
    public function deleteVariantDefinitionsForVariantId($variantId)
    {
        $updateArray = array(
            'status' => 'DELETED'
        );
        $where = array(
            'productId' => $variantId,
            'productIdFrom' => "PRODUCTS_R_ATTRIBUTES"
        );
        $this->_update($updateArray, 'PRODUCTS_Definitions', $where);
    }

    /**
     *
     * @param
     *            $variant
     */
    public function deleteAttributesForVariantId($variant)
    {
        $updateArray = array(
            'status' => 'DELETED'
        );
        $where = array(
            'productOrVariantId' => $variant,
            'isVariant' => 1,
            'status' => 'ACTIVE'
        );
        $this->_update($updateArray, 'PRODUCTS_ATTRIBUTES_Combination', $where);
    }

    /**
     * Function to delete related product
     *
     * @param type $relatedProductId
     * @return type
     */
    public function deleteRelatedProductOrVariants($relatedProductId)
    {
        $table = $this->_productsRelatedProductsTable;
        $fieldsToUpdate = array(
            'status' => 'DELETED'
        );
        $whereClause = array(
            'relatedProductId' => $relatedProductId
        );
        $relatedProductDeleted = $this->_update($fieldsToUpdate, $table, $whereClause);
        return $relatedProductDeleted;
    }

    /**
     * Save product tags in PRODUCTS_Tags table
     *
     * @param string | array $productTags
     * @param int $savedProductId
     * @param int $languageId
     * @return int
     */
    protected function _saveProductTags($productTags, $savedProductId, $languageId)
    {
        // If tags is in string format, convert to array
        if (! is_array($productTags)) {
            $productTags = explode(',', $productTags);
        }
        if (count($productTags) > 0) {
            $productTagEntity = new ProductsTagsEntity();
            $productTagArray = array();
            $savedProductTagCount = 0;
            foreach ($productTags as $key => $alias) {
                $productTagArray['productId'] = $savedProductId;
                $productTagArray['languageId'] = $languageId;
                $productTagArray['alias'] = trim($alias);
                $productTagArray['status'] = 'ACTIVE';
                $productTagArray['created'] = date('Y-m-d H:i:s');
                $hydratedData = $this->_hydrator->hydrate($productTagArray, $productTagEntity);
                $data = $this->_hydrator->extract($hydratedData);
                $savedProductTagId = $this->_save($data, $this->_productsTagsTable);
                if ($savedProductTagId > 0) {
                    $savedProductTagCount ++;
                }
            }
            return $savedProductTagCount;
        }
    }

    /**
     * Save product mis spells in PRODUCTS_Mis_Spells table
     *
     * @param string | array $productMisSpells
     * @param int $productId
     * @param int $languageId
     * @return int
     */
    protected function _saveProductMisSpells($productMisSpells, $productId, $languageId)
    {
        // If mis spells is in string format, convert to array
        if (! is_array($productMisSpells)) {
            $productMisSpells = explode(',', $productMisSpells);
        }
        if (count($productMisSpells) > 0) {
            $productsMisSpellsEntity = new ProductsMisSpellsEntity();
            $productMisSpellsArray = array();
            $savedProductMisSpellsCount = 0;
            foreach ($productMisSpells as $key => $misSpell) {
                $productMisSpellsArray['productId'] = $productId;
                $productMisSpellsArray['languageId'] = $languageId;
                $productMisSpellsArray['misSpell'] = trim($misSpell);
                $productMisSpellsArray['created'] = date('Y-m-d H:i:s');
                $hydratedData = $this->_hydrator->hydrate($productMisSpellsArray, $productsMisSpellsEntity);
                $data = $this->_hydrator->extract($hydratedData);
                $savedProductMisSpellId = $this->_save($data, $this->_productsMisspellsTable);
                if ($savedProductMisSpellId > 0) {
                    $savedProductMisSpellsCount ++;
                }
            }
            return $savedProductMisSpellsCount;
        }
    }

    /**
     * Function to extract some columns from an entity
     *
     * @param string  $entityName
     * @param string  $dataToExtractFrom
     * @param array $extraColumnToUnset
     * @return mixed
     */
    protected function extractColumnsFromEntity($entityName, $dataToExtractFrom, $extraColumnToUnset = array())
    {
        $hydratedData = $this->_hydrator->hydrate($dataToExtractFrom, $entityName);
        $data = $this->_hydrator->extract($hydratedData);
        foreach ($data as $columnName => $value) {
            if (empty($value)) {
                unset($data[$columnName]);
            }
        }
        if (count($extraColumnToUnset) > 0) {
            foreach ($extraColumnToUnset as $columns) {
                unset($data[$columns]);
            }
        }
        return $data;
    }

    /**
     *
     * @param
     *            $variantId
     * @param
     *            $ean13
     * @return int
     */
    public function updateVariantBarcode($variantId, $ean13)
    {
        $tableName = $this->_productsVariantsRelationshipTable;
        $whereClause = array(
            'productAttributeId' => $variantId
        );
        $fieldsToUpdate = array(
            'ean13' => $ean13
        );

        $variantUpdated = $this->_update($fieldsToUpdate, $tableName, $whereClause);

        return $variantUpdated;
    }

    /**
     * Function to update META information
     *
     * @param type $submitedFormData
     * @param type $tableName
     * @param type $languageId
     * @return type
     */
    public function updateMetaDetails($submitedFormData, $tableName, $languageId)
    {
        /**
         * Meta information is held in products definitions table.
         * We need to update it.
         */
        $productDefinitionsEntity = new ProductsDefinitionsEntity();
        $fieldsToUpdate = $this->extractColumnsFromEntity($productDefinitionsEntity, $submitedFormData, array(
            'created',
            'productId'
        ));
        // Update Meta information
        if (count($fieldsToUpdate) > 0) {
            $whereClause = array(
                'productId' => $submitedFormData['productId'],
                'languageId' => $languageId
            );
            $metaUpdated = $this->_update($fieldsToUpdate, $tableName, $whereClause);
        }
        return $metaUpdated;
    }

    /**
     * Save product attributes
     *
     * @param
     *            $submitedFormData
     * @return int
     */
    public function saveProductAttributes($submitedFormData)
    {
        // this should be changed to account only for attributes that were sent when the form was submitted
        // if any new attributes were created meantime we should ignore them at this point
        // we will exclude now attributes that we don't want to use
        foreach ($submitedFormData as $attributeName => $attributeValue) {
            if (strpos($attributeName, 'not_use_') === 0 && $attributeValue == 1) {
                $realAttributeName = str_replace('not_use_', '', $attributeName);
                // we need to unset this attribute
                unset($submitedFormData[$attributeName]);
                // we need to unset the attribute this one is pointing at
                unset($submitedFormData[$realAttributeName]);
            }
        }

        // Fetch the marketing attributes so we can check the attributes before saving them
        $allProductMarketingAttributes = $this->_attributesGroupMapper->fetchAllAttributeGroups();
        $savedProductsAttributesCombinationCount = 0;
        if (count($allProductMarketingAttributes) > 0) {
            $extractedAttribute = array();
            foreach ($allProductMarketingAttributes as $key => $attributesArray) {
                if (isset($attributesArray) && is_array($attributesArray) && count($attributesArray) > 0) {
                    foreach ($attributesArray as $attributesArrayKey => $attributesArrayValues) {
                        if (isset($attributesArrayValues['name']) && trim($attributesArrayValues['name']) != '') {
                            // Extract some details
                            $extractedAttribute[] = array(
                                'attributeFormName' => strtolower($attributesArrayValues['groupType']) . '_' . $attributesArrayValues['attributeGroupId'],
                                'attributeId' => $attributesArrayValues['attributeGroupId'],
                                'name' => $attributesArrayValues['name'],
                                'viewAs' => $attributesArrayValues['viewAs']
                            );
                        }
                    }
                }
            }

            $isVariant = $submitedFormData['variantId'] != '0' ? 1 : 0;
            $productOrVariantId = $submitedFormData['variantId'] != '0' ? $submitedFormData['variantId'] : $submitedFormData['productId'];

            // if it is a main product, get the number of attributes this product has at this point
            // if it has 0 then after we insert the new ones we need to insert them for all the variants also
            // if ($isVariant == 0) {
            // $this->_attributesGroupMapper->getAttributesNumberForProduct($productOrVariantId);
            // }

            // begin product attributes saving transaction
            // we don't want any actions recorded on the database if anything goes wrong
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->beginTransaction();
            if (count($extractedAttribute) > 0) {
                foreach ($extractedAttribute as $key => $extractedData) {
                    // Check if the attribute is an array
                    if (isset($submitedFormData[$extractedData['attributeFormName']])) {
                        $attributeFormName = $extractedData['attributeFormName'];
                    } else
                        if (isset($submitedFormData['new_' . $extractedData['attributeFormName']])) {
                            $attributeFormName = 'new_' . $extractedData['attributeFormName'];
                        } else {
                            continue;
                        }
                    $attributeGroupId = $extractedData['attributeId'];
                    $attributeValue = ($submitedFormData[$attributeFormName] == 'on') ? 1 : $submitedFormData[$attributeFormName];

                    if ($attributeValue == '' && $extractedData['viewAs'] == 'TEXT') {
                        // do nothing it is just an empty text field
                        continue;
                    }
                    // we need to save this attribute for this product
                    // first we will check if we need to create an new attribute
                    // then we need to check if we need to update an existing one

                    // Ensure this attribute doesn't exist before trying to save it
                    $productAttributeExistsValue = $this->_productsMapperAttributes->checkIfProductAttributeExists($productOrVariantId, $attributeGroupId, $isVariant);

                    if ($productAttributeExistsValue !== false) {
                        // If the attribute exists, compare the new value with the old one
                        // If the values are not equal, deactivate the old value else skip further actions
                        if ($attributeValue != $productAttributeExistsValue) {
                            $this->_productsMapperAttributes->updateProductAttribute($productOrVariantId, $attributeGroupId, $isVariant, $attributeValue, $extractedData['viewAs'], $this->_currentLanguage);
                            $savedProductsAttributesCombinationCount ++;
                        } else {
                            continue;
                        }
                    } else {
                        // If the attribute does not exist, add it
                        $this->_productsMapperAttributes->insertNewProductAttribute($productOrVariantId, $attributeGroupId, $isVariant, $attributeValue);
                        $savedProductsAttributesCombinationCount ++;
                    }
                }
            }
            if ($isVariant == 0) {}
            // at this point we can commit the transaction because all the queries are successful
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->commit();
        }
        return $savedProductsAttributesCombinationCount;
    }

    /**
     * Update the merchant category column in Products table
     *
     * @param array $submittedFormData
     * @return bool
     */
    public function updateMerchantCategory($submittedFormData = array())
    {
        if (count($submittedFormData) > 0) {
            $fieldsToUpdate['productMerchantCategoryId'] = $submittedFormData['productMerchantCategoryId'];
            $tableName = $this->_tableName;
            $whereClause = array(
                'productId' => $submittedFormData['productId']
            );
            $rowUpdated = $this->_update($fieldsToUpdate, $tableName, $whereClause);
        }
        return $rowUpdated;
    }

    /**
     * Retrieve product information
     *
     * @param
     *            $productId
     * @return array
     */
    public function getMinimumProductsDetails($productId, $fullDetails = false)
    {
        $this->_sql->setTable(array(
            'p' => $this->_tableName
        ));
        $select = $this->_sql->select();
        if ($fullDetails) {
            $select->join(array(
                'ptd' => $this->_productsTypesDefinitionsTable
            ), "p.productTypeId = ptd.productTypeId", array(
                'name'
            ));
            $select->columns(array(
                '*'
            ));
        } else {
            $select->columns(array(
                'productId',
                'status',
                'ean13',
                'style'
            ));
        }

        $select->where(array(
            'p.productId' => $productId
        ));
        $select->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $product = $statement->execute()->next();
        return $product;
    }

    /**
     * Retrieve product information
     *
     * @param int $languageId
     * @param
     *            $productIdsArray
     * @return array
     */
    public function getBasicProductsDetails($languageId = 1, $productIdsArray = array())
    {
        $returnArray = array();

        $this->_sql->setTable(array(
            'p' => $this->_tableName
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'productId',
            'sku' => 'sku',
            'status',
            'style',
            'ean13',
            'productTypeId'
        ));
        $select->join(array(
            'pd' => $this->_productsDefinitionsTable
        ), // Set join table and alias
            new Expression('p.productId = pd.productId AND pd.productIdFrom = "' . $this->_tableName . '"'), array(
                'productName' => 'productname',
                'shortProductName'
            ));
        /*
         * $select->join(
         * array('ps' => $this->_productsSpectrumTable), //Set join table and alias
         * "p.productId = ps.productId",
         * array('style')
         * );
         */
        $select->join(array(
            'ptd' => $this->_productsTypesDefinitionsTable
        ), // Set join table and alias
            "p.productTypeId = ptd.productTypeId", array(
                'productType' => 'name'
            ));
        $select->where(array(
            'pd.languageId' => $languageId,
            'ptd.languageId' => $languageId
        ));
        $select->where->notEqualTo('p.status', 'DELETED');

        // We may want to limit the result to only one product Id
        if (count($productIdsArray) > 0) {
            $productsIdsCondition = implode(",", $productIdsArray);
            $select->where("pd.productId IN($productsIdsCondition)");
            if (count($productIdsArray) == 1) {
                $select->limit(1);
            }
        }
        $select->order('p.productId DESC');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            $variants = $this->getVariants($value['productId']);
            $value['variantCount'] = $variants['dataCount'];
            $returnArray[] = $value;
        }

        return $returnArray;
    }

    /**
     * Retrieve product information in pages
     *
     * @param int $languageId
     * @param
     *            $productIdsArray
     * @return array
     */
    public function getPaginatedBasicProductsDetails($languageId = 1, $productIdsArray = array(), $page = 1, $nrPerPage = 2, $usePagination = false, $orderBy = false, $orderbyOption = false, $search = false)
    {
        $this->_sql->setTable(array(
            'p' => $this->_tableName
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'productId',
            'sku' => 'sku',
            'status',
            'productTypeId',
            'style'
        ));
        $select->join(array(
            'pd' => $this->_productsDefinitionsTable
        ), // Set join table and alias
            new Expression('p.productId = pd.productId AND pd.productIdFrom = "' . $this->_tableName . '"'), array(
                'productName' => 'productname',
                'shortProductName'
            ));
        $select->join(array(
            'ptd' => $this->_productsTypesDefinitionsTable
        ), // Set join table and alias
            "p.productTypeId = ptd.productTypeId", array(
                'productType' => 'name'
            ));
        $select->join(array(
            'pvr' => $this->_productsVariantsRelationshipTable
        ), 'pvr.productId = p.productId', array(
            'variantCount' => new Expression('COUNT(productAttributeId)')
        ), Select::JOIN_LEFT);
        $select->where(array(
            'pd.languageId' => $languageId,
            'ptd.languageId' => $languageId
        ));
        $select->where->notEqualTo('p.status', 'DELETED');
        $select->where(new Expression("pvr.productId IS NULL OR pvr.status != 'DELETED'"));
        // We may want to limit the result to only one product Id
        if (count($productIdsArray) > 0) {
            $productsIdsCondition = implode(",", $productIdsArray);
            $select->where("pd.productId IN($productsIdsCondition)");
            if (count($productIdsArray) == 1) {
                $select->limit(1);
            }
        }
        $select->group('p.productId');
        if ($orderBy && $orderbyOption) {
            if ($orderBy == 'productName') {
                $select->order('pd.' . $orderBy . ' ' . $orderbyOption);
            } else {
                $select->order('p.' . $orderBy . ' ' . $orderbyOption);
            }
        } else {
            $select->order('p.productId DESC');
        }
        if ($search && ! empty($search)) {
            $select->where->nest->like('p.style', '%' . $search . '%')->or->like('pd.productName', '%' . $search . '%')->or->like('p.sku', '%' . $search . '%')->or->like('pd.shortProductName', '%' . $search . '%')->unnest;
        }

        $results = $this->_queryPaginator->getPaginatorForSelect($select, $this->_dbAdapter, $page, $nrPerPage);

        return $results;
    }

    /**
     *
     * @param
     *            $productMerchantCategoryId
     * @param int $languageId
     * @return bool
     */
    public function getMerchantCategoryNameById($productMerchantCategoryId, $languageId = 1)
    {
        $this->_sql->setTable(array(
            'mad' => $this->_productsMerchantCategoriesDefinitionsTable
        ));
        $select = $this->_sql->select()
            ->where(array(
                'productMerchantCategoryId' => $productMerchantCategoryId,
                'languageId' => $languageId
            ))
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() > 0) {
            return $results->next()['categoryName'];
        } else {
            return false;
        }
    }

    /**
     * Retrieve care information for a product
     *
     * @param int $productId
     * @param int $tabGroupId
     *            1 = care information
     * @param boolean $returnTabId
     * @return string
     */
    public function getCareInformationByProductId($productId, $returnTabId = false, $tabGroupId = 1)
    {
        $languageId = $this->_currentLanguage;

        $this->_sql->setTable(array(
            'pt' => $this->_productTabsTable
        ));
        $select = $this->_sql->select();
        $select->join(array(
            'td' => $this->_tabDefiniitonsTable
        ), "pt.tabId = td.tabId", array(
            'content',
            'tabId'
        ));
        $select->where(array(
            'pt.tabGroupId' => $tabGroupId,
            'pt.productId' => $productId,
            'td.languageId' => $languageId,
            'td.status' => 'ACTIVE'
        ));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $return = '';
        foreach ($results as $key => $value) {
            if (isset($value['content']) && trim($value['content']) != '') {
                if ($returnTabId === true) {
                    $return = array(
                        'careInformation' => $value['tabId'],
                        'careInformationString' => $value['content']
                    );
                } else {
                    $return = $value['content'];
                }
            }
        }
        return $return;
    }

    /**
     * Function to retrieve care information
     *
     * @return array
     */
    protected function getCareInformation()
    {
        $languageId = $this->_currentLanguage;

        $this->_sql->setTable(array(
            't' => $this->_tabsTable
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'tabId'
        ));
        $select->join(array(
            'td' => $this->_tabDefiniitonsTable
        ), "t.tabId = td.tabId", array(
            'title'
        ));
        $select->where(array(
            't.tabGroupId' => 1,
            'td.languageId' => $languageId,
            'td.status' => 'ACTIVE'
        ));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $returnArray = array(
            0 => 'Choose care information'
        );
        foreach ($results as $key => $value) {
            if (isset($value['title']) && trim($value['title']) != '') {
                $returnArray[$value['tabId']] = $value['title'];
            }
        }
        return $returnArray;
    }

    /**
     * Function to retrieve product details based on the product id
     *
     * @param int $productId
     * @param int $languageId
     * @param boolean $namedArrayFlag -
     *            decides if the keys of the returned array will be the attributes names
     *            optimized for elastic-search
     * @return array
     */
    public function getProductDetailsById($productId, $languageId, $namedArrayFlag = false)
    {
        $returnArray = array();

        $this->_sql->setTable(array(
            'p' => $this->_tableName
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'manufacturerId',
            'productTypeId',
            'productMerchantCategoryId',
            'ean13',
            'ecotax',
            'quantity',
            'sku',
            'style',
            'supplierReference',
            'status',
            'created',
            'modified'
        ));
        $select->join(array(
            'pdef' => $this->_productsDefinitionsTable
        ), new Expression('p.productId = pdef.productId AND pdef.productIdFrom = "' . $this->_tableName . '"'), array(
            'productDefinitionsId',
            'versionId',
            'versionDescription',
            'shortProductName',
            'productName',
            'urlName',
            'metaTitle',
            'metaDescription',
            'metaKeyword',
            'shortDescription',
            'longDescription',
            'status',
            'misSpells',
            'tags',
            'makeLive',
            'created',
            'modified'
        ));
        $select->join(array(
            'ptd' => $this->_productsTypesDefinitionsTable
        ), // Set join table and alias
            "ptd.productTypeId = p.productTypeId", array(
                'productTypeName' => 'name'
            ));
        $select->join(array(
            'pt' => $this->_productTabsTable
        ), "pt.productId = p.productId", array(), Select::JOIN_LEFT);
        $select->join(array(
            'td' => $this->_tabDefiniitonsTable
        ), "td.tabId = pt.tabId", array(
            "careInformationString" => "content"
        ), Select::JOIN_LEFT);

        /*
         * Commented out because of the fact that if the product dosen't have variants it is removed from this table
         * $select->join(
         * array('ps' => $this->_productsSpectrumTable), //Set join table and alias
         * "ps.productId = p.productId",
         * array('style')
         * );
         */
        $select->where(array(
            'p.productId' => $productId,
            'pdef.languageId' => $languageId,
            'ptd.languageId' => $languageId
        ));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            $returnArray[$productId] = $value;
        }
        // Get the product prices
        $productPrices = $this->getProductPrices($productId);
        // If we have any price information, add it to the array
        if (is_array($productPrices) && count($productPrices) > 0) {
            $returnArray[$productId] = empty($returnArray[$productId]) ? array() : $returnArray[$productId];
            $returnArray[$productId] = $returnArray[$productId] + $productPrices;
        }
        // Get product channels
        // $channels = $this->_getProductChannels($productId);
        $returnArray[$productId]['channels'] = $this->_getProductChannels($productId);
        $returnArray[$productId]['websites'] = $this->_getProductWebsites($productId);
        $returnArray[$productId]['relatedProducts'] = $this->_getProductsRelatedProducts($productId);
        $returnArray[$productId]['status'] = $this->getProductStatusByProductId($productId);
        $isVariant = 0; // This is a main product and not a variant
        $returnArray[$productId]['attributes'] = $this->_productsMapperAttributes->getAttributesAssignedToProduct($productId, $isVariant, $languageId, $namedArrayFlag);
        $returnTabId = true; // Flag for care information to return the selected id
        $careInformation = $this->getCareInformationByProductId($productId, $returnTabId);
        if (is_array($careInformation)) {
            $returnArray[$productId]['careInformation'] = $careInformation['careInformation'];
            $returnArray[$productId]['careInformationString'] = $careInformation['careInformationString'];
        } else {
            $returnArray[$productId]['careInformationString'] = $careInformation;
        }
        // If product definition id set, check if we have information for tags and mispells
        if (isset($returnArray[$productId]['productDefinitionsId']) && $returnArray[$productId]['productDefinitionsId'] > 0) {
            // Get product mis spells
            // $misSpells = $this->_getProductMisSpells($productId);
            // $productMisSpells = '';
            // if (is_array($misSpells) && count($misSpells) > 0) {
            // $productMisSpells = implode(', ', $misSpells);
            // }
            // $returnArray[$productId]['misSpells'] = $productMisSpells;
            // Get product tags
            // $tags = $this->getProductTags($productId);
            // $productTags = '';
            // if (is_array($tags) && count($tags) > 0) {
            // $productTags = implode(', ', $tags);
            // }
            // $returnArray[$productId]['tags'] = $productTags;
        }

        return $returnArray;
    }

    /**
     *
     * @param int $languageId
     * @return array
     */
    public function getSuppliers($languageId)
    {
        $this->_sql->setTable(array(
            's' => $this->_suppliersTable
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'code'
        ));
        $select->join(array(
            'sd' => $this->_suppliersDefinitionsTable
        ), "s.supplierId=sd.supplierId", array(
            'supplierName'
        ));
        $select->where('sd.languageId="' . $languageId . '"');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $finalArray = array();
        foreach ($results as $result) {
            $finalArray[$result['code']] = $result['supplierName'] . ' (' . $result['code'] . ')';
        }

        return $finalArray;
    }

    /**
     *
     * @param int $productId
     * @return string
     */
    public function getProductStatusByProductId($productId)
    {
        $productStatus = 'INACTIVE';

        if ($productId > 0) {
            $this->_sql->setTable(array(
                't' => $this->_tableName
            ));
            $select = $this->_sql->select();
            $select->columns(array(
                'status'
            ));
            $select->where(array(
                't.productId' => $productId
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            foreach ($results as $key => $value) {
                $productStatus = $value['status'];
            }
        }
        return $productStatus;
    }

    /**
     *
     * @param int $productId
     * @param bool $isVariant
     * @param int $languageId
     * @param bool $namedArrayFlag
     * @return mixed
     * @throws \Exception
     */
    public function getAllAttributesAssignedToProduct($productId, $isVariant, $languageId, $namedArrayFlag = false)
    {
        return $this->_productsMapperAttributes->getAttributesAssignedToProduct($productId, $isVariant, $languageId, $namedArrayFlag);
    }

    /**
     * Retrieve product channels
     *
     * @param int $productId
     * @return array
     */
    protected function _getProductChannels($productId)
    {
        $returnArray = array();
        $this->_sql->setTable($this->_productsChannelsTable);
        $select = $this->_sql->select();
        $select->columns(array(
            'channelId'
        ));
        $select->where(array(
            'productId' => $productId,
            'status' => 'ACTIVE'
        ));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {

            $returnArray[] = $value['channelId'];
        }
        return $returnArray;
    }

    /**
     * Retrieve the websites the product has been setup to be sold
     *
     * @param int $productId
     * @return array
     */
    protected function _getProductWebsites($productId)
    {
        $returnArray = array();
        $this->_sql->setTable($this->_productsClientsWebsitesTable);
        $select = $this->_sql->select();
        $select->columns(array(
            'websiteId'
        ));
        $select->where(array(
            'productId' => $productId
        ));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {

            $returnArray[] = $value['websiteId'];
        }
        return $returnArray;
    }

    /**
     * Function to retrieve product prices based on the product id
     *
     * @param int $productOrVariantId
     * @param bool $currencyIdAsKey
     * @param int $isVariant
     * @return mixed
     */
    public function getProductPrices($productOrVariantId, $currencyIdAsKey = false, $isVariant = 0)
    {
        $returnArray = array();
        $currencies = $this->getLimitedCurrenciesDetails();
        $this->_sql->setTable($this->_productsPricesTable);
        $select = $this->_sql->select();
        $select->columns(array(
            'currencyId',
            'offerPrice',
            'nowPrice',
            'offerPrice',
            'wasPrice',
            'costPrice',
            'tradingCostPrice',
            'wholesalePrice',
            'freightPrice',
            'dutyPrice',
            'designPrice',
            'packagingPrice'
        ));
        $select->where(array(
            'status' => 'ACTIVE',
            'productOrVariantId' => $productOrVariantId
        ));
        if ($isVariant === 0) {
            $select->where(array(
                'productIdFrom' => $this->_tableName
            ));
        } else {
            $select->where(array(
                'productIdFrom' => $this->_productsVariantsRelationshipTable
            ));
        }
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        // We want to convert the currency isoCode to upper case first later
        $currencies = array_map('strtolower', $currencies);
        $currencies = array_map('ucfirst', $currencies);

        foreach ($results as $key => $value) {
            if (isset($currencies[$value['currencyId']])) {
                if ($currencyIdAsKey) {
                    $returnArray[$value['currencyId']]['offerPrice'] = $value['offerPrice'];
                    $returnArray[$value['currencyId']]['nowPrice'] = $value['nowPrice'];
                    $returnArray[$value['currencyId']]['wasPrice'] = $value['wasPrice'];
                    $returnArray[$value['currencyId']]['costPrice'] = $value['costPrice'];
                    $returnArray[$value['currencyId']]['tradingCostPrice'] = $value['tradingCostPrice'];
                    $returnArray[$value['currencyId']]['wholesalePrice'] = $value['wholesalePrice'];
                    $returnArray[$value['currencyId']]['freightPrice'] = $value['freightPrice'];
                    $returnArray[$value['currencyId']]['dutyPrice'] = $value['dutyPrice'];
                    $returnArray[$value['currencyId']]['designPrice'] = $value['designPrice'];
                    $returnArray[$value['currencyId']]['packagingPrice'] = $value['packagingPrice'];
                } else {
                    $returnArray['offerPrice' . $currencies[$value['currencyId']]] = $value['offerPrice'];
                    $returnArray['nowPrice' . $currencies[$value['currencyId']]] = $value['nowPrice'];
                    $returnArray['wasPrice' . $currencies[$value['currencyId']]] = $value['wasPrice'];
                    $returnArray['costPrice' . $currencies[$value['currencyId']]] = $value['costPrice'];
                    $returnArray['tradingCostPrice' . $currencies[$value['currencyId']]] = $value['tradingCostPrice'];
                    $returnArray['wholesalePrice' . $currencies[$value['currencyId']]] = $value['wholesalePrice'];
                    $returnArray['freightPrice' . $currencies[$value['currencyId']]] = $value['freightPrice'];
                    $returnArray['dutyPrice' . $currencies[$value['currencyId']]] = $value['dutyPrice'];
                    $returnArray['designPrice' . $currencies[$value['currencyId']]] = $value['designPrice'];
                    $returnArray['packagingPrice' . $currencies[$value['currencyId']]] = $value['packagingPrice'];
                }
            }
        }

        return $returnArray;
    }

    /**
     * Retrieve product mis spells information
     *
     * @param int $productId
     * @return array
     */
    protected function _getProductMisSpells($productId)
    {
        $returnArray = array();
        $this->_sql->setTable($this->_productsMisspellsTable);
        $select = $this->_sql->select();
        $select->columns(array(
            'misSpell'
        ));
        $select->where(array(
            'productId' => $productId,
            'languageId' => $this->_currentLanguage
        ));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            $returnArray[] = trim($value['misSpell']);
        }
        return $returnArray;
    }

    /**
     * Retrieve product tags information
     *
     * @param int $productId
     * @return array
     */
    public function getProductTags($productId)
    {
        $returnArray = array();
        $this->_sql->setTable($this->_productsTagsTable);
        $select = $this->_sql->select();
        $select->columns(array(
            'alias'
        ));
        $select->where(array(
            'productId' => $productId,
            'languageId' => $this->_currentLanguage
        ));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {

            $returnArray[] = trim($value['alias']);
        }
        return $returnArray;
    }

    /**
     * Function to generate full product categories details based on the product ID.
     * Note: This function return the full information on the product categories
     *
     *
     * @param int $productId
     * @param int $languageId
     * @return array
     */
    public function getProductCategoriesByProductId($productId, $languageId = 1)
    {
        $returnArray = array();

        $this->_sql->setTable(array(
            'pcr' => $this->_productsCategoriesRelationshipTable
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'productCategoryId',
            'categoryId',
            'isDefault',
            'status',
            'created',
            'modified'
        ));
        $select->where(array(
            'pcr.productId' => $productId,
            'cdef.languageId' => $languageId
        ));
        $select->join(array(
            'cdef' => $this->_categoriesDefinitionsTable
        ), 'pcr.categoryId = cdef.categoryId', array(
            'categoryName',
            'versionId',
            'versionDescription',
            'urlName',
            'metaTitle',
            'metaDescription',
            'metaKeyword',
            'shortDescription',
            'longDescription',
            'status',
            'makeLive',
            'created',
            'modified'
        ));
        $select->join(array(
            'ct' => $this->_categoriesTable
        ), 'pcr.categoryId = ct.categoryId', array(
            'categoryParentId',
            'treeDepth',
            'position',
            'status'
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            $returnArray[$productId][] = $value;
        }
        return $returnArray;
    }

    /**
     * Function to retrieve currencies
     *
     * @return array
     */
    public function getCurrencies()
    {
        $returnArray = array();
        $this->_sql->setTable($this->_currencyTable);
        $select = $this->_sql->select();
        $select->columns(array(
            'currencyId',
            'isoCode',
            'isDefault'
        ));
        $select->where(array(
            'status' => 'ACTIVE'
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $position = 0;
        foreach ($results as $key => $value) {
            $returnArray[$position]['currencyId'] = $value['currencyId'];
            $returnArray[$position]['isoCode'] = $value['isoCode'];
            $returnArray[$position]['isDefault'] = $value['isDefault'];

            $position ++;
        }
        return $returnArray;
    }

    /**
     * return list of active Currencies
     * @return []
     */
    public function getActiveCurrencies()
    {
        $returnArray = array();
        $this->_sql->setTable($this->_currencyTable);
        $select = $this->_sql->select();
        $select->columns(array(
            'isoCode',
        ));
        $select->where(array(
            'status' => 'ACTIVE'
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $position = 0;
        foreach ($results as $key => $value) {
            $returnArray[] = $value['isoCode'];
        }
        return $returnArray;
    }

    /**
     * Retrieve limited information about product currencies
     *
     * Returns an array with currencyId as key and isoCode as value
     * e.g array(1 => 'USD', 2 => 'EUR', 3 => 'GBP)
     *
     * @return array
     */
    public function getLimitedCurrenciesDetails()
    {
        $returnArray = array();
        $this->_sql->setTable($this->_currencyTable);
        $select = $this->_sql->select();
        $select->columns(array(
            'currencyId',
            'isoCode'
        ));
        $select->where(array(
            'status' => 'ACTIVE'
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $position = 0;
        foreach ($results as $key => $value) {
            $returnArray[$value['currencyId']] = $value['isoCode'];
            $position ++;
        }

        return $returnArray;
    }

    /**
     *
     * @param int $productId
     * @param int $indexId
     */
    public function addIndexIdForProduct($productId, $indexId)
    {}

    /**
     * Get the product details need for spectrum system
     *
     * @param int $productId
     * @return mixed
     */
    public function getProductDetailsForSpectrum($productId)
    {
        $this->_sql->setTable(array(
            'p' => $this->_tableName
        ));
        $action = $this->_sql->select()
            ->columns(array(
                'product_code' => 'sku',
                'barcode' => 'ean13',
                'status',
                'style',
                'supplier_code' => 'supplierReference',
                'productMerchantCategoryId'
            ))
            ->join(array(
                'pd' => $this->_productsDefinitionsTable
            ), new Expression('p.productId = pd.productId AND pd.productIdFrom = "' . $this->_tableName . '"'), array(
                'description' => 'productName'
            ))
            ->join(array(
                'pt' => $this->_productsTypesDefinitionsTable
            ), 'pt.productTypeId = p.productTypeId', array(
                'product_type' => 'name'
            ))
            ->join(array(
                'pspt' => $this->_productSupplierPriceTable
            ), new Expression('pspt.productOrVariantId = p.productId AND pd.productIdFrom = "' . $this->_tableName . '"'), array(
                'supplierPrice' => 'price',
                'supplierCurrencyCode' => 'currencyCode'
            ), Select::JOIN_LEFT)
            ->join(array(
                's' => $this->_suppliersTable
            ), new Expression('s.code=p.supplierReference'), array(
                'merchandizeInitials'
            ), Select::JOIN_LEFT)
            ->join(array(
                'ptt' => $this->_productTabsTable
            ), 'ptt.productId = p.productId', array(
                'tabId'
            ), Select::JOIN_LEFT)
            ->join(array(
                'tdt' => $this->_tabDefiniitonsTable
            ), new Expression('tdt.tabId = ptt.tabId'), array(
                'styleType' => 'code'
            ), Select::JOIN_LEFT)
            ->where(array(
                'pd.languageId' => $this->_defaultLanguage,
                'pt.languageId' => $this->_defaultLanguage,
                'p.productId' => $productId
            ));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();

        $coreAttributes = $this->_getProductCoreAttributes($productId, false);
        $supplierCode = '';
        $subCategory = '';
        $category = '';
        $supplierPrice = 0;
        $supplierCurrencyCode = '';
        $pmsProductId = 0;
        $styleType = '';
        $merchandizeInitials = '';
        foreach ($results as $result) {
            $productDetails['productType'] = $result['product_type'];
            $productDetails['active'] = ($result['status'] == 'ACTIVE') ? 1 : 0;
            $productDetails['productCode'] = $result['product_code'];
            $productDetails['style'] = $result['style'];
            $productDetails['variant'] = '';
            $productDetails['description'] = $result['description'];
            $productDetails['reportCode'] = $result['product_code'];
            $productDetails['reportDescription'] = $result['description'];
            $productDetails['barcode'] = $result['barcode'];
            $productDetails["allowExpress"] = $coreAttributes["allow_express"];
            $productDetails["allowGiftWrap"] = $coreAttributes["allow_gift_wrap"];
            $productDetails["ageRestricted"] = $coreAttributes["age_restricted"];
            $productDetails["postageExempt"] = $coreAttributes["postage_exempt"];
            $productDetails["allowBackOrders"] = $coreAttributes["allow_back_orders"];
            $productDetails["suppressFromSale"] = $coreAttributes["suppress_from_sale"];
            $productDetails["screeningQty"] = $coreAttributes["screening_qty"];

            $productDetails["colour"] = '';
            $productDetails["size"] = '';

            $supplierCode = ! empty($result['supplier_code']) ? $result['supplier_code'] : $result['main_product_supplier_code'];
            if (! empty($result['supplierPrice'])) {
                $supplierPrice = $result['supplierPrice'];
            }
            if (! empty($result['supplierCurrencyCode'])) {
                $supplierCurrencyCode = $this->_translateCurrencyCodeToLegacy($result['supplierCurrencyCode']);
            }
            if (isset($result['styleType'])) {
                $styleType = $result['styleType'];
            }
            if (isset($result['merchandizeInitials'])) {
                $merchandizeInitials = $result['merchandizeInitials'];
            }
            $merchantCategoriesDetails = $this->extractProductMerchantCategory($result["productMerchantCategoryId"]);

            $supplierCode = $result['supplier_code'];
            $merchantCategoriesDetails = $this->extractProductMerchantCategory($result["productMerchantCategoryId"]);
        }

        $productPrices = $this->_getProductPricesForSpectrum($productId, false);
        $wholeSalePrices = $this->getProductWholesalePricesForSpectrum($productId, 'PRODUCTS');
        $productDetails['prices'] = $productPrices;

        $costPrice = 0.00;
        $freightPrice = 0.00;
        if (! empty($productPrices)) {
            foreach ($productPrices as $currencyType => $priceTypeArray) {
                if (isset($priceTypeArray['costPrice'])) {
                    $costPrice = $priceTypeArray['costPrice'];
                    unset($productPrices[$currencyType]['costPrice']); // Cost price is needed as an attribute so let's unset it from the main product array.
                }
                if (isset($priceTypeArray['freightPrice'])) {
                    $freightPrice = $priceTypeArray['freightPrice'];
                    unset($productPrices[$currencyType]['freightPrice']); // Cost price is needed as an attribute so let's unset it from the main product array.
                }
            }
        }

        $consolidationCode = (isset($productDetails['product_code'])) ? $this->_stripProductConsolidationCodeFromSku($productDetails['product_code']) : '';

        $productTaxes = $this->_taxMapper->getTaxValueForProduct($productId, 0);
        foreach ($productTaxes as $taxTerritory => $taxValue) {
            $taxName = 'taxRate' . strtoupper($taxTerritory);
            $productDetails['attributes'][$taxName] = round($taxValue / 100, 2);
        }

        // Get the images used by the products on Spectrum
        $spectrumImages = $this->getSpectrumImagesForProduct($productId, 0);
        $productDetails['attributes']['thumbnail'] = (isset($spectrumImages['thumbnail']) && is_array($spectrumImages['thumbnail']) && isset($spectrumImages['thumbnail']['imageUrl'])) ? $spectrumImages['thumbnail']['imageUrl'] : '';
        $productDetails['attributes']['image'] = (isset($spectrumImages['image']) && is_array($spectrumImages['image']) && isset($spectrumImages['image']['imageUrl'])) ? $spectrumImages['image']['imageUrl'] : '';
        $productDetails['attributes']['consolidationCode'] = $consolidationCode;
        $productDetails['attributes']['costPrice'] = $costPrice;
        $productDetails['attributes']['freightCost'] = $freightPrice;
        $productDetails['attributes']['brand'] = isset($coreAttributes['brand']) ? $coreAttributes['brand'] : '';
        ;
        $productDetails['attributes']['length'] = isset($coreAttributes['length']) ? $coreAttributes['length'] : '';
        ;
        $productDetails['attributes']['width'] = isset($coreAttributes['width']) ? $coreAttributes['width'] : '';
        ;
        $productDetails['attributes']['height'] = isset($coreAttributes['height']) ? $coreAttributes['height'] : '';
        ;
        $productDetails['attributes']['UKDeliveryOnly'] = isset($coreAttributes['uk_delivery_only']) ? $coreAttributes['uk_delivery_only'] : '0';
        ;
        $productDetails['attributes']['originCountry'] = isset($coreAttributes['country_of_origin']) ? $coreAttributes['country_of_origin'] : '';
        $productDetails['attributes']['supplierCode'] = $supplierCode;
        $productDetails['attributes']["personalisationType"] = isset($coreAttributes['personalised_items']) ? "OPTIONAL" : '';

        $productDetails['attributes']['wholesalePriceGB'] = ! empty($wholeSalePrices['wholesalePriceGB']) ? $wholeSalePrices['wholesalePriceGB'] : '0.00';
        $productDetails['attributes']['wholesalePriceUS'] = ! empty($wholeSalePrices['wholesalePriceUS']) ? $wholeSalePrices['wholesalePriceUS'] : '0.00';
        $productDetails['attributes']['wholesalePriceEU'] = ! empty($wholeSalePrices['wholesalePriceEU']) ? $wholeSalePrices['wholesalePriceEU'] : '0.00';
        $productDetails['attributes']['wholesalePriceRW'] = ! empty($wholeSalePrices['wholesalePriceRW']) ? $wholeSalePrices['wholesalePriceRW'] : '0.00';

        $productDetails['attributes']['costPriceTradeCurrency'] = $supplierPrice;
        $productDetails['attributes']['tradeCurrency'] = $this->_translateCurrencyCodeToLegacy($supplierCurrencyCode);
        $productDetails['attributes']['cmsProductId'] = $productId;
        $productDetails['attributes']['mcInitials'] = $merchandizeInitials;
        $productDetails['attributes']['styleType'] = $styleType;
        $productDetails['attributes']['merchantCategoryName'] = isset($merchantCategoriesDetails['category']['categoryName']) ? $merchantCategoriesDetails['category']['categoryName'] : '';
        $productDetails['attributes']['merchantCategoryID'] = isset($merchantCategoriesDetails['category']['categoryId']) ? $merchantCategoriesDetails['category']['categoryId'] : '';
        $productDetails['attributes']['merchantSubCategoryName'] = isset($merchantCategoriesDetails['subCategory']['categoryName']) ? $merchantCategoriesDetails['subCategory']['categoryName'] : '';
        $productDetails['attributes']['merchantSubCategoryID'] = isset($merchantCategoriesDetails['subCategory']['categoryId']) ? $merchantCategoriesDetails['subCategory']['categoryId'] : '';

        return $productDetails;
    }

    /**
     * This function returns the images used for products on Spectrum
     *
     * @param int $productId
     * @return array
     */
    protected function getSpectrumImagesForProduct($productId, $variantId)
    {
        $thumbNail = $this->_productFolderMapper->getSpecialImageByProductAndVariant($productId, $variantId, 'default', 'spectrum-main-image');
        $mainImage = $this->_productFolderMapper->getSpecialImageByProductAndVariant($productId, $variantId, 'default', 'spectrum-main-image');

        return array(
            'thumbnail' => $thumbNail,
            'image' => $mainImage
        );
    }
    public function startTransaction()
    {
        $this->_dbAdapter->getDriver()
            ->getConnection()
            ->beginTransaction();
    }

    public function rollbackTransaction()
    {
        $this->_dbAdapter->getDriver()
            ->getConnection()
            ->rollback();
    }

    public function commitTransaction()
    {
        $this->_dbAdapter->getDriver()
            ->getConnection()
            ->commit();
    }
    /**
     * Strip the consolid out of the sku
     *
     * @param string $sku
     * @param int $consolidLength
     * @param int $start
     * @return boolean | string
     */
    protected function _stripProductConsolidationCodeFromSku($sku, $consolidLength = 9, $start = 0)
    {
        if (trim($sku) != '' && strlen($sku) > $consolidLength) {
            return substr($sku, $start, $consolidLength);
        }

        return false;
    }

    /**
     * Translate currency code to format used by other systems
     *
     * @param string $currencyCode
     * @return string
     */
    protected function _translateCurrencyCodeToLegacy($currencyCode)
    {
        $currencyCode = strtolower($currencyCode);
        $allowLegacyCurrencyTranslation = isset($this->_currencies['allow-legacy-trade-currency-map-translation']) ? $this->_currencies['allow-legacy-trade-currency-map-translation'] : true;
        $legacyTradeCurrencyMap = isset($this->_currencies['legacy-trade-currency-map']) ? $this->_currencies['legacy-trade-currency-map'] : array();
        if ($allowLegacyCurrencyTranslation === true && ! empty($legacyTradeCurrencyMap)) {
            return (isset($legacyTradeCurrencyMap[$currencyCode]) ? $legacyTradeCurrencyMap[$currencyCode] : $currencyCode);
        }

        return $currencyCode;
    }

    /**
     *
     * @param int $variantId
     * @return mixed
     */
    public function getVariantDetailsForSpectrumForCommonAttributes($variantId)
    {
        $this->_sql->setTable(array(
            'pv' => $this->_productsVariantsRelationshipTable
        ));
        $action = $this->_sql->select()
            ->columns(array(
                'product_code' => 'sku',
                'barcode' => 'ean13',
                'status',
                'supplier_code' => 'supplierReference',
                'productId' => 'productId'
            ))
            ->join(array(
                'p' => $this->_tableName
            ), 'pv.productId = p.productId', array(
                'style',
                'main_product_supplier_code' => 'supplierReference',
                'productMerchantCategoryId'
            ))
            ->join(array(
                'pd' => $this->_productsDefinitionsTable
            ), new Expression('pv.productAttributeId = pd.productId AND pd.productIdFrom = "' . $this->_productsVariantsRelationshipTable . '"'), array(
                'description' => 'shortProductName'
            ))
            ->join(array(
                'pt' => $this->_productsTypesDefinitionsTable
            ), 'pt.productTypeId = p.productTypeId', array(
                'product_type' => 'name'
            ))
            ->join(array(
                'pspt' => $this->_productSupplierPriceTable
            ), new Expression('pspt.productOrVariantId = p.productId AND pd.productIdFrom = "' . $this->_tableName . '"'), array(
                'supplierPrice' => 'price',
                'supplierCurrencyCode' => 'currencyCode'
            ), Select::JOIN_LEFT)
            ->join(array(
                's' => $this->_suppliersTable
            ), new Expression('s.code=p.supplierReference'), array(
                'merchandizeInitials'
            ), Select::JOIN_LEFT)
            ->where(array(
                'pd.languageId' => $this->_defaultLanguage,
                'pt.languageId' => $this->_defaultLanguage,
                'pv.productAttributeId' => $variantId
            ));

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();
        $result = $results->next();

        $productDetails['productType'] = $result['product_type'];
        $productDetails['active'] = ($result['status'] == 'ACTIVE') ? 1 : 0;
        $productDetails['productCode'] = $result['product_code'];
        $productDetails['style'] = $result['style'];
        $productDetails['styleForPms'] = $result['style'];
        $productDetails['variant'] = '';
        $productDetails['description'] = $result['description'];
        $productDetails['reportCode'] = $result['product_code'];
        $productDetails['reportDescription'] = $result['description'];
        $productDetails['barcode'] = $result['barcode'];
        $productDetails['productId'] = $result['productId'];

        /**
         * Get colour and sizes
         */
        $this->_sql->setTable($this->_productsAttributesSkuRuleRelationsTable);
        $select = $this->_sql->select()->where('productAttributeId = ' . $variantId);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $skuResults = $statement->execute();

        foreach ($skuResults as $skuResult) {
            switch ($skuResult['skuRuleTable']) {
                case 'COMMON_ATTRIBUTES_VALUES':
                    $skuRuleId = $skuResult["skuRuleId"];
                    break;
                case 'COLOURS_Groups':
                    $colourGroupId = $skuResult["skuRuleId"];
                    // we need to take the colour code
                    $colourCode = $this->_attributesGroupMapper->getColourGroupCode($colourGroupId);
                    $colourName = $this->_attributesGroupMapper->getColourGroupName($colourGroupId);

                    $productDetails['colour'] = $colourCode;
                    $productDetails['colourName'] = ! empty($colourName) ? $colourName : $colourCode;
                    break;
            }
        }

        if (isset($this->clientConfig['settings']['addSkuPadding']) && $this->clientConfig['settings']['addSkuPadding']) {
            $paddedStyle = str_pad($result['style'], 6);

            if ((strlen($result['style']) < 6) && ! strpos($productDetails['productCode'], ' ')) {
                $productDetails['productCode'] = $paddedStyle . substr($productDetails['productCode'], strlen($result['style']));
            }
        } else {
            $paddedStyle = $result['style'];
        }

        if (! empty($skuRuleId)) {
            $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($skuRuleId);
            $territoryOverridesArray = array();
            if (! is_null($commonAttributeValueEntity)) {
                $commonAttributeEntity = $commonAttributeValueEntity->getCommonAttributeId();
                if ($commonAttributeEntity->getIsSize()) {
                    $sizeCode = $commonAttributeValueEntity->getCommonAttributeValueSkuCode();
                    $productDetails['sizeDescription'] = $commonAttributeValueEntity->getCommonAttributeValuesDefinitions()->getValue();
                    $productDetails['size'] = $sizeCode;

                    $territoryOverrides = $commonAttributeValueEntity->getCommonAttributesValuesOverrides();

                    foreach ($territoryOverrides as $territoryOverride) {
                        // $skuDefaultUsed = str_replace($productDetails['colour'] . $sizeCode, $productDetails['colour'] . $territoryOverride->getCommonAttributeValueSkuCode(), $productDetails['productCode']);
                        $skuDefaultUsed = $paddedStyle . $productDetails['colour'] . $territoryOverride->getCommonAttributeValueSkuCode();
                        $territoryOverridesArray[] = array(
                            'territory' => $territoryOverride->getTerritoryId()->getIso2Code(),
                            'productCode' => $skuDefaultUsed,
                            'style' => $productDetails['styleForPms'],
                            'colour' => $productDetails['colour'],
                            'size' => $territoryOverride->getCommonAttributeValueSkuCode(),
                            'sizeDescription' => $territoryOverride->getCommonAttributeValue()
                        );
                    }
                    $productDetails['territoryOverrides'] = $territoryOverridesArray;
                }


            }
        }

        if (isset($this->clientConfig['settings']['addSkuPadding']) && $this->clientConfig['settings']['addSkuPadding']) {
            $optionCode = str_pad($productDetails['styleForPms'], 6, ' ', STR_PAD_RIGHT) . $productDetails['colour'];
        } else {
            $skuSeparator = isset($this->clientConfig['settings']['skuGenerationSeparator']) ? $this->clientConfig['settings']['skuGenerationSeparator'] : '';
            $optionCode = $productDetails['styleForPms'] . $skuSeparator . $productDetails['colour'];
        }

        $productDetails['prices'] = $this->getProductPricesForStyleOrOption($productDetails['styleForPms'], $optionCode);

        unset($productDetails['prices']['nowPrice']);
        unset($productDetails['prices']['wasPrice']);
        $sellingPrice = $this->getVariantPrices($variantId);

        $productDetails['prices']['salePrice'] = $sellingPrice;

        $productDetails['attributes'] = $this->getAttributesRequiredForSpectrum($variantId);
        $productDetails['attributes'] = $this->overrideAttributesForSpectrum($variantId, $productDetails['attributes']);

        //add size property to  attributes array with territory overrides
        if (!empty($commonAttributeEntity)) {
            $sizePropertyAttributes = $this->getSziePropertyAttributesDetails($skuRuleId, $commonAttributeEntity->getCommonattributeid());
            if (is_array($sizePropertyAttributes) && !empty($sizePropertyAttributes)) {
                foreach ($sizePropertyAttributes as $sizePropertyAttributeKey => $sizePropertyAttributeValue) {
                    $productDetails['attributes'][$sizePropertyAttributeKey] = $sizePropertyAttributeValue;
                }
            }
        }

        $thumbnail = false;
        if (isset($productDetails['attributes']['thumbnail'])) {
            $thumbnailExploded = explode("\n", $productDetails['attributes']['thumbnail']);
            if (isset($thumbnailExploded[0])) {
                $thumbnail =  trim($thumbnailExploded[0]);
            }
        }


        $highResImage = false;
        if (isset($productDetails['attributes']['high_res_image'])) {
            $highResImageExploded = explode("\n", $productDetails['attributes']['high_res_image']);
            if (isset($highResImageExploded[0])) {
                $highResImage =  trim($highResImageExploded[0]);
            }
        }

        if (isset($this->clientConfig['settings']['spectrumImages']) && $this->clientConfig['settings']['spectrumImages']) {
            $spectrumImages = $this->getSpectrumImagesForProduct($productDetails['productId'], $variantId);
            if ($thumbnail) {
                $productDetails['attributes']['thumbnail'] = $thumbnail;
            } else {
                $productDetails['attributes']['thumbnail'] = (isset($spectrumImages['thumbnail']) && is_array($spectrumImages['thumbnail']) && isset($spectrumImages['thumbnail']['imageUrl'])) ? $spectrumImages['thumbnail']['imageUrl'] : '';
            }

            if ($highResImage) {
                $productDetails['attributes']['image'] = $highResImage;
            } else {
                $productDetails['attributes']['image'] = (isset($spectrumImages['image']) && is_array($spectrumImages['image']) && isset($spectrumImages['image']['imageUrl'])) ? $spectrumImages['image']['imageUrl'] : '';
            }
        }



        /**
         * Set Supplier price as attribute
         */
        if (isset($productDetails['prices']['supplierPrice']['currencyCode'])) {
            $productDetails['attributes']['tradeCurrency'] = $productDetails['prices']['supplierPrice']['currencyCode'];
        }
        if (isset($productDetails['prices']['supplierPrice']['price'])) {
            $productDetails['attributes']['costPriceTradeCurrency'] = $productDetails['prices']['supplierPrice']['price'];
        }
        /**
         * End Set Supplier price as attribute
         */

        if ($this->clientCode == 'lch' && empty($productDetails['size'])) {
            $productDetails['size'] = 'NOS';
        }
        return $productDetails;
    }


    protected function getSziePropertyAttributesDetails($commonAttributeValueId, $commonattributeid)
    {
        $data = [];
        // get realted values
        $commonAttributeValuePropertySzizeIds = $this->commonAttributesService->getCommonAttributeValueSizePropertyIdsByParnetValueId($commonAttributeValueId);
        if(!empty($commonAttributeValuePropertySzizeIds)){
            foreach ($commonAttributeValuePropertySzizeIds as $commonAttributeValuechildId)
            {
                $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($commonAttributeValuechildId);
                $id = $commonAttributeValueEntity->getcommonAttributeValueId();

                $commonAttributeEntity = $commonAttributeValueEntity->getCommonAttributeId();

                //get mapping name from spectrum
                $spectrumMappingEntity = $commonAttributeEntity->getcommonAttributeSpectrumMapping();
                if (!is_null($spectrumMappingEntity)) {
                    $spectrumMapping = $spectrumMappingEntity->getCommonAttributeSpectrumMapping();
                    $data[$spectrumMapping] = $commonAttributeValueEntity->getCommonAttributeValuesDefinitions()->getValue();
                    $territoryOverrides = $commonAttributeValueEntity->getCommonAttributesValuesOverrides();
                    foreach ($territoryOverrides as $territoryOverride) {
                        $data[$spectrumMapping.$territoryOverride->getTerritoryId()->getIso2Code()] = $territoryOverride->getCommonAttributeValue();
                    }
                }

            }
        }
        return $data;
    }

    /**
     * @return bool
     */
    public function getForceSyncWithoutBarcode()
    {
        if (isset($this->clientConfig['settings']['forceSyncWithoutBarcode'])) {
            return $this->clientConfig['settings']['forceSyncWithoutBarcode'];
        }
        return false;
    }

    /**
     *
     * @param null $verification
     */
    public function changeSkuForAllProducts($verification = null)
    {
        if ($verification == 'just test') {
            $allVariants = $this->getAllVariants();

            foreach ($allVariants as $variant) {

                $productId = $this->getProductIdFromVariantId($variant['productAttributeId']);
                $productData = $this->getProductDetailsById($productId, $this->_currentLanguage);

                $colour = $this->getColourForVariantId($variant['productAttributeId']);

                $colourCode = $colour['groupCode'];
                $style = $productData[key($productData)]['style'];
                $skuCode = $variant['sku'];

                $newSize = $this->getSizeUsedForVariantId($variant['productAttributeId']);

                $newSku = $style . $colourCode . $newSize;

                $update = $this->_sql->update();
                $update->table('PRODUCTS_R_ATTRIBUTES');
                $update->set(array(
                    'sku' => $newSku
                ));
                $update->where("productAttributeId = '" . $variant['productAttributeId'] . "'");

                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();

                var_dump($skuCode . '    <==>    ' . $newSku);
            }
        }
    }


    /**
     * @param int $variantId
     * @param bool $fullDetails
     */
    public function getSizeUsedForVariantId($variantId, $fullDetails = false)
    {
        $this->_sql->setTable($this->_productsAttributesSkuRuleRelationsTable);
        $select = $this->_sql->select()
            ->where('skuRuleTable = "COMMON_ATTRIBUTES_VALUES"')
            ->where('productAttributeId = ' . $variantId);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute()->next();

        $this->_sql->setTable(array('cav' => 'COMMON_ATTRIBUTES_VALUES'));
        $select = $this->_sql->select()
            ->join(
                array(
                    'cavd' => 'COMMON_ATTRIBUTES_VALUES_Definitions'
                ),
                'cavd.commonAttributeValueId = cav.commonAttributeValueId',
                array('*')
            )
            ->where('cav.commonAttributeValueId = ' . $results['skuRuleId']);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute()->next();
        if ($fullDetails) {
            return $results;
        }
        return $results['commonAttributeValueSkuCode'];
    }

    /**
     *
     * @param
     *            $variantId
     * @return mixed
     */
    public function getBaseSizeUsedForVariantId($variantId)
    {
        $this->_sql->setTable($this->_productsAttributesSkuRuleRelationsTable);
        $select = $this->_sql->select()
            ->where('skuRuleTable = "COMMON_ATTRIBUTES_VALUES"')
            ->where('productAttributeId = ' . $variantId);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute()->next();

        $this->_sql->setTable('COMMON_ATTRIBUTES_VALUES');
        $select = $this->_sql->select()->where('commonAttributeValueId = ' . $results['skuRuleId']);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute()->next();

        return $results['commonAttributeValueBaseSize'];
    }

    /**
     *
     * @param string $characters
     * @param int $length
     * @return string
     */
    public function generateRandomString($characters = null, $length = 10)
    {
        if ($characters === null) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }

        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i ++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     *
     * @throws \Exception
     */
    public function setAttributesToProducts()
    {
        $products = $this->fetchAllProducts();

        foreach ($products as $product) {

            $options = $this->getAllOptionsForStyle($product['style']);

            foreach ($options as $option) {
                $harmonisationCodeUS = $this->generateRandomString(); // ID 140
                $midCodeArray = array(
                    115,
                    1034,
                    1035,
                    1036,
                    1037,
                    1038,
                    1039,
                    1040,
                    1041,
                    1042,
                    1043,
                    1044,
                    1045,
                    1046,
                    1047,
                    1048,
                    1049,
                    1050,
                    1051,
                    1052,
                    1053,
                    1054,
                    1055
                );
                $midCode = $midCodeArray[array_rand($midCodeArray)]; // ID 22
                $fibreContent = '100% cotton'; // 126
                $fabricType = '129'; // 27 SELECT 129

                foreach ($option as $variant) {
                    $this->addCommonAttributeToProductOrVariant($variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES', 140, $harmonisationCodeUS);
                    $this->addCommonAttributeToProductOrVariant($variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES', 22, $midCode);
                    $this->addCommonAttributeToProductOrVariant($variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES', 126, $fibreContent);
                    $this->addCommonAttributeToProductOrVariant($variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES', 27, $fabricType);
                }
            }
        }
    }

    /**
     *
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getAllVariants()
    {
        $this->_sql->setTable($this->_productsVariantsRelationshipTable);
        $select = $this->_sql->select();
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     *
     * @param
     *            $variantId
     * @return array
     */
    public function getVariantPrices($variantId)
    {
        $this->_sql->setTable(array(
            'pp' => $this->_productsPricesTable
        ));
        $select = $this->_sql->select()
            ->where('pp.productIdFrom = "PRODUCTS_R_ATTRIBUTES"')
            ->where('pp.productOrVariantId = ' . $variantId)
            ->where('pp.status = "ACTIVE"')
            ->join(array(
                'c' => $this->_currencyTable
            ), new Expression('pp.currencyId = c.currencyId'), array(
                '*'
            ))
            ->join(array(
                't' => $this->territoriesTable
            ), new Expression('pp.regionId = t.territoryId'), array(
                '*'
            ));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $pricesArray = array();
        foreach ($results as $result) {
            $pricesArray[$result['isoCode']][$result['iso2Code']] = array(
                'nowPrice' => $result['nowPrice'],
                'wasPrice' => $result['wasPrice']
            );
        }

        return $pricesArray;
    }

    /**
     *
     * @param
     *            $variantId
     * @return array
     */
    private function getAttributesRequiredForSpectrum($variantId)
    {
        $this->_sql->setTable(array(
            'pca' => $this->productsCommonAttributesTable
        ));
        $select = $this->_sql->select()
            ->join(array(
                'ca' => $this->commonAttributesTable
            ), new Expression('pca.commonAttributeId = ca.commonAttributeId'), array(
                '*'
            ))
            ->where(array(
                'pca.productOrVariantId' => $variantId,
                'pca.productIdFrom' => 'PRODUCTS_R_ATTRIBUTES'
            ));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $attributesArray = array();

        foreach ($results as $result) {
            $commonAttributeEntity = $this->commonAttributesService->getCommonAttribute($result['commonAttributeId']);
            $attributeType = $commonAttributeEntity->getCommonAttributeGuiRepresentation()
                ->getCommonAttributesViewTypeId()
                ->getCommonAttributeViewType();
            switch ($attributeType) {
                case 'MULTISELECT':
                    $this->_sql->setTable($this->productsCommonAttributesTable);
                    $select = $this->_sql->select()->where(array(
                        'productOrVariantId' => $result['productOrVariantId'],
                        'productIdFrom' => $result['productIdFrom'],
                        'commonAttributeId' => $result['commonAttributeId']
                    ));
                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $multivalues = $statement->execute();
                    $multiValueArray = array();
                    foreach ($multivalues as $multiValue) {
                        $valueId = $multiValue['commonAttributeValue'];
                        if (! empty($valueId)) {
                            $valueEntity = $this->commonAttributesService->getCommonAttributeValueById($valueId);
                            if (!empty($valueEntity)) {
                                $multiValueArray[] = $valueEntity->getCommonAttributeValuesDefinitions()->getValue();
                            }
                        } else {
                            $multiValueArray[] = $result['commonAttributeValue'];
                        }
                    }
                    $value = implode(' | ', $multiValueArray);
                    break;
                case 'SELECT':
                    $valueId = $result['commonAttributeValue'];
                    if (! empty($valueId)) {
                        $valueEntity = $this->commonAttributesService->getCommonAttributeValueById($valueId);
                        if (! is_null($valueEntity)) {
                            $abbrevCode = $valueEntity->getCommonAttributeValueAbbrev();
                            if (! empty($abbrevCode)) {
                                $value = $abbrevCode;
                            } else {
                                $value = $valueEntity->getCommonAttributeValuesDefinitions()->getValue();
                            }
                        }
                    } else {
                        $value = $result['commonAttributeValue'];
                    }
                    break;
                default:
                    $value = $result['commonAttributeValue'];
                    break;
            }

            /** @var CommonAttributesSpectrumMapping $spectrumMappingEntity */
            $spectrumMappingEntity = $commonAttributeEntity->getCommonAttributeSpectrumMapping();

            $spectrumMapping = '';
            if (! empty($spectrumMappingEntity)) {
                $spectrumMapping = $spectrumMappingEntity->getcommonAttributeSpectrumMapping();
            }

            if (empty($spectrumMapping)) {
                $attributeName = $commonAttributeEntity->getCommonAttributeDefinitions()->getDescription();
                $attributeName = strtolower($attributeName);
                $attributeName = str_replace(' ', '_', $attributeName);
            } else {
                $attributeName = $spectrumMapping;
            }

            $attributesArray[$attributeName] = $value;
        }
        return $attributesArray;
    }

    /**
     * @param $variantId
     * @param $attributes
     * @return mixed
     */
    private function overrideAttributesForSpectrum($variantId, $attributes)
    {
        $this->_sql->setTable([
            'pca' => $this->productsCommonAttributesTable
        ]);
        $action = $this->_sql->select()
            ->columns([
                'productOrVariantId',
            ])
            ->join(
                ['so' => $this->commonAttributesSpectrumMappingOverride],
                new Expression('pca.commonAttributeId = so.enabledForCommonAttributeId AND pca.commonAttributeValue = so.enabledForCommonAttributeValueId'),
                ['commonAttributeId', 'enabledForCommonAttributeId', 'enabledForCommonAttributeValueId'],
                Select::JOIN_INNER
            )
            ->join(
                ['sm' => $this->commonAttributesSpectrumMapping],
                new Expression('so.commonAttributeSpectrumMappingId = sm.commonAttributeSpectrumMappingId'),
                ['commonAttributeSpectrumMapping'],
                Select::JOIN_INNER
            )
            ->where([
                'pca.productOrVariantId' => $variantId,
                'pca.productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
            ]);

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();
        $overrides = [];

        foreach ($results as $result) {
            $commonAttributeValue = $this->getCommonAttributeValueForProduct($result['commonAttributeId'], $result['productOrVariantId']);
            $commonAttributeValue = $this->getCommonAttributeValue($result['commonAttributeId'], $commonAttributeValue);

            if (!empty($commonAttributeValue)) {
                $overrideMapping = $result['commonAttributeSpectrumMapping'];

                if (isset($overrides[$overrideMapping])) {
                    $overrides[$overrideMapping] = $overrides[$overrideMapping] . ', ' . $commonAttributeValue;
                } else {
                    $overrides[$overrideMapping] = $commonAttributeValue;
                }
            }
        }

        foreach ($overrides as $overrideKey => $overrideValue) {
            $attributes[$overrideKey] = $overrideValue;
        }

        return $attributes;
    }

    private function getCommonAttributeValueForProduct($commonAttributeId, $productOrVariantId, $productIdFrom = 'PRODUCTS_R_ATTRIBUTES')
    {
        $values = [];
        $this->_sql->setTable([
            'pca' => $this->productsCommonAttributesTable
        ]);
        $action = $this->_sql->select()
            ->columns([
                'commonAttributeValue',
            ])
            ->where([
                'pca.commonAttributeId' => $commonAttributeId,
                'pca.productOrVariantId' => $productOrVariantId,
                'pca.productIdFrom' => $productIdFrom,
            ]);

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();

        foreach ($results as $result) {
            $values[] = $result['commonAttributeValue'];
        }

        if (count($values) > 1) {
            return $values;
        } else {
            $key = key($values);
            return $values[$key];
        }
    }

    /**
     * @param array $spectrumMappingOverrides
     * @param int $variantId
     */
    public function checkIfWeCanOverrideSpectrumMapping($spectrumMappingOverrides, $variantId)
    {

    }

    /**
     * Get the product details needed for spectrum system
     *
     * @param int $variantId
     * @return mixed
     */
    public function getVariantDetailsForSpectrum($variantId)
    {
        return $this->getDefaultVariantDetailsForSpectrum($variantId);
    }

    /**
     *
     * @param int $variantId
     * @return mixed
     */
    private function getDefaultVariantDetailsForSpectrum($variantId)
    {
        $this->_sql->setTable(array(
            'pv' => $this->_productsVariantsRelationshipTable
        ));
        $action = $this->_sql->select()
            ->columns(array(
                'product_code' => 'sku',
                'barcode' => 'ean13',
                'status',
                'supplier_code' => 'supplierReference'
            ))
            ->join(array(
                'p' => $this->_tableName
            ), 'pv.productId = p.productId', array(
                'style',
                'main_product_supplier_code' => 'supplierReference',
                'productMerchantCategoryId'
            ))
            ->join(array(
                'pd' => $this->_productsDefinitionsTable
            ), new Expression('pv.productAttributeId = pd.productId AND pd.productIdFrom = "' . $this->_productsVariantsRelationshipTable . '"'), array(
                'description' => 'shortProductName'
            ))
            ->join(array(
                'pt' => $this->_productsTypesDefinitionsTable
            ), 'pt.productTypeId = p.productTypeId', array(
                'product_type' => 'name'
            ))
            ->join(array(
                'ps' => $this->_productsAttributesSkuRuleRelationsTable
            ), 'ps.productAttributeId = pv.productAttributeId', array(
                'skuRuleId',
                'skuRuleTable'
            ), Select::JOIN_LEFT)
            ->join(array(
                'pspt' => $this->_productSupplierPriceTable
            ), new Expression('pspt.productOrVariantId = p.productId AND pd.productIdFrom = "' . $this->_tableName . '"'), array(
                'supplierPrice' => 'price',
                'supplierCurrencyCode' => 'currencyCode'
            ), Select::JOIN_LEFT)
            ->join(array(
                's' => $this->_suppliersTable
            ), new Expression('s.code=p.supplierReference'), array(
                'merchandizeInitials'
            ), Select::JOIN_LEFT)
            ->join(array(
                'ptt' => $this->_productTabsTable
            ), 'ptt.productId = p.productId', array(
                'tabId'
            ), Select::JOIN_LEFT)
            ->join(array(
                'tdt' => $this->_tabDefiniitonsTable
            ), new Expression('tdt.tabId = ptt.tabId'), array(
                'styleType' => 'code'
            ), Select::JOIN_LEFT)
            ->where(array(
                'pd.languageId' => $this->_defaultLanguage,
                'pt.languageId' => $this->_defaultLanguage,
                'pv.productAttributeId' => $variantId
            ));

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();

        $coreAttributes = $this->_getProductCoreAttributes($variantId, true);
        $supplierCode = '';
        $subCategory = '';
        $category = '';
        $supplierPrice = 0;
        $supplierCurrencyCode = '';
        $pmsProductId = 0;
        $styleType = '';
        $merchandizeInitials = '';
        $finalDescription = false;
        foreach ($results as $result) {
            $productDetails['productType'] = $result['product_type'] == 'COLLECTION' || $result['product_type'] == 'MATRIX' ? 'STANDARD' : $result['product_type'];
            $productDetails['active'] = ($result['status'] == 'ACTIVE') ? 1 : 0;
            $productDetails['productCode'] = $result['product_code'];
            $productDetails['style'] = $result['style'];
            $productDetails['variant'] = '';
            $productDetails['description'] = $result['description'];
            $productDetails['reportCode'] = $result['product_code'];
            $productDetails['reportDescription'] = $result['description'];
            $productDetails['barcode'] = $result['barcode'];
            $productDetails["allowExpress"] = $coreAttributes["allow_express"];
            $productDetails["allowGiftWrap"] = $coreAttributes["allow_gift_wrap"];
            $productDetails["ageRestricted"] = $coreAttributes["age_restricted"];
            $productDetails["postageExempt"] = $coreAttributes["free_postage"];
            $productDetails["allowBackOrders"] = $coreAttributes["allow_back_orders"];
            $productDetails["suppressFromSale"] = $coreAttributes["suppress_from_sale"];
            $productDetails["screeningQty"] = $coreAttributes["screening_qty"];
            $productDetails["freePostage"] = $coreAttributes["free_postage"];

            $supplierCode = ! empty($result['supplier_code']) ? $result['supplier_code'] : $result['main_product_supplier_code'];
            if (! empty($result['supplierPrice'])) {
                $supplierPrice = $result['supplierPrice'];
            }
            if (! empty($result['supplierCurrencyCode'])) {
                $supplierCurrencyCode = $this->_translateCurrencyCodeToLegacy($result['supplierCurrencyCode']);
            }
            if (isset($result['styleType'])) {
                $styleType = $result['styleType'];
            }
            if (isset($result['merchandizeInitials'])) {
                $merchandizeInitials = $result['merchandizeInitials'];
            }
            $merchantCategoriesDetails = $this->extractProductMerchantCategory($result["productMerchantCategoryId"]);

            switch ($result["skuRuleTable"]) {
                case $this->_coloursGroupsTable:
                    $colourGroupId = $result["skuRuleId"];
                    // we need to take the colour code
                    $colourCode = $this->_attributesGroupMapper->getColourGroupCode($colourGroupId);
                    $colourName = $this->_attributesGroupMapper->getColourGroupName($colourGroupId);
                    $productDetails['colour'] = $colourCode;

                    if (strpos(strtolower($result['description']), strtolower($colourName)) === false) {
                        // Changed to use the shortProductName and inserting the colour at the end on request of Alex Barbier #1792
                        $finalDescription = $result['description'] . ' ' . $colourName;
                    } else {
                        $finalDescription = $result['description'];
                    }
                    break;
                case $this->_sizesAccessoriesTable:
                case $this->_sizesShoesTable:
                case $this->_sizesClothesTable:
                case $this->_sizesBeltsTable:
                case $this->_sizesShirtsTable:
                case $this->_sizesJacketsTable:
                case $this->_sizesTrousersTable:
                case $this->_sizesSwimShortsTable:
                    $sizeId = $result["skuRuleId"];
                    // we need to take the size
                    $size = $this->_attributesGroupMapper->getSizeByIdAndTable($sizeId, $result["skuRuleTable"]);
                    $productDetails['size'] = $size;
                    break;
            }
        }
        if (! empty($finalDescription)) {
            $productDetails['description'] = $finalDescription;
            $productDetails['reportDescription'] = $finalDescription;
        }

        $productPrices = $this->_getProductPricesForSpectrum($variantId, true);
        $wholeSalePrices = $this->getProductWholesalePricesForSpectrum($variantId);
        $productDetails['prices'] = $productPrices;

        $costPrice = 0.00;
        $freightPrice = 0.00;
        if (! empty($productPrices)) {
            foreach ($productPrices as $currencyType => $priceTypeArray) {
                if (strtolower($currencyType) == 'gbp') {
                    if (isset($priceTypeArray['costPrice'])) {
                        $costPrice = $priceTypeArray['costPrice'];
                        unset($productPrices[$currencyType]['costPrice']); // Cost price is needed as an attribute so let's unset it from the main product array.
                    }
                    if (isset($priceTypeArray['freightPrice'])) {
                        $freightPrice = $priceTypeArray['freightPrice'];
                        unset($productPrices[$currencyType]['freightPrice']); // Cost price is needed as an attribute so let's unset it from the main product array.
                    }
                }
            }
        }

        $consolidationCode = (isset($productDetails['product_code'])) ? $this->_stripProductConsolidationCodeFromSku($productDetails['product_code']) : '';

        $productTaxes = $this->_taxMapper->getTaxValueForProduct($variantId, 1);
        foreach ($productTaxes as $taxTerritory => $taxValue) {
            $taxName = 'taxRate' . strtoupper($taxTerritory);
            $productDetails['attributes'][$taxName] = round($taxValue / 100, 2);
        }
        $productId = $this->getProductIdFromVariantId($variantId);
        // Get the images used by the products on Spectrum
        $spectrumImages = $this->getSpectrumImagesForProduct($productId, $variantId);
        $productDetails['attributes']['thumbnail'] = (isset($spectrumImages['thumbnail']) && is_array($spectrumImages['thumbnail']) && isset($spectrumImages['thumbnail']['imageUrl'])) ? $spectrumImages['thumbnail']['imageUrl'] : '';
        $productDetails['attributes']['image'] = (isset($spectrumImages['image']) && is_array($spectrumImages['image']) && isset($spectrumImages['image']['imageUrl'])) ? $spectrumImages['image']['imageUrl'] : '';
        $productDetails['attributes']['consolidationCode'] = $consolidationCode;
        $productDetails['attributes']['costPrice'] = $costPrice;
        $productDetails['attributes']['freightCost'] = $freightPrice;
        $productDetails['attributes']['brand'] = isset($coreAttributes['brand']) ? $coreAttributes['brand'] : '';
        ;
        $productDetails['attributes']['length'] = isset($coreAttributes['length']) ? $coreAttributes['length'] : '';
        ;
        $productDetails['attributes']['width'] = isset($coreAttributes['width']) ? $coreAttributes['width'] : '';
        ;
        $productDetails['attributes']['height'] = isset($coreAttributes['height']) ? $coreAttributes['height'] : '';
        ;
        $productDetails['attributes']['UKDeliveryOnly'] = isset($coreAttributes['uk_delivery_only']) ? $coreAttributes['uk_delivery_only'] : '0';
        ;
        $productDetails['attributes']['originCountry'] = isset($coreAttributes['country_of_origin']) ? $coreAttributes['country_of_origin'] : '';
        $productDetails['attributes']['supplierCode'] = $supplierCode;

        $productDetails['attributes']['wholesalePriceGB'] = ! empty($wholeSalePrices['wholesalePriceGBP']) ? $wholeSalePrices['wholesalePriceGBP'] : '0.00';
        $productDetails['attributes']['wholesalePriceUS'] = ! empty($wholeSalePrices['wholesalePriceUSD']) ? $wholeSalePrices['wholesalePriceUSD'] : '0.00';
        $productDetails['attributes']['wholesalePriceEU'] = ! empty($wholeSalePrices['wholesalePriceEUR']) ? $wholeSalePrices['wholesalePriceEUR'] : '0.00';
        $productDetails['attributes']['wholesalePriceRW'] = ! empty($wholeSalePrices['wholesalePriceRW']) ? $wholeSalePrices['wholesalePriceRW'] : '0.00';
        $productDetails['attributes']["personalisationType"] = isset($coreAttributes['personalised_items']) ? "OPTIONAL" : '';

        $productDetails['attributes']['costPriceTradeCurrency'] = $supplierPrice;
        $productDetails['attributes']['tradeCurrency'] = $this->_translateCurrencyCodeToLegacy($supplierCurrencyCode);
        $productDetails['attributes']['cmsProductId'] = $variantId;
        $productDetails['attributes']['mcInitials'] = $merchandizeInitials;
        $productDetails['attributes']['styleType'] = $styleType;
        $productDetails['attributes']['merchantCategoryName'] = isset($merchantCategoriesDetails['category']['categoryName']) ? $merchantCategoriesDetails['category']['categoryName'] : '';
        $productDetails['attributes']['merchantCategoryID'] = isset($merchantCategoriesDetails['category']['categoryId']) ? $merchantCategoriesDetails['category']['categoryId'] : '';
        $productDetails['attributes']['merchantSubCategoryName'] = isset($merchantCategoriesDetails['subCategory']['categoryName']) ? $merchantCategoriesDetails['subCategory']['categoryName'] : '';
        $productDetails['attributes']['merchantSubCategoryID'] = isset($merchantCategoriesDetails['subCategory']['categoryId']) ? $merchantCategoriesDetails['subCategory']['categoryId'] : '';
        return $productDetails;
    }

    /**
     *
     * @param int $productOrVariantId
     * @param string $fromTable
     * @return array
     */
    private function getProductWholesalePricesForSpectrum($productOrVariantId, $fromTable = 'PRODUCTS_R_ATTRIBUTES')
    {
        $this->_sql->setTable($this->_productsPricesTable);
        $select = $this->_sql->select()
            ->columns(array(
                'wholesalePrice',
                'currencyId'
            ))
            ->where('productIdFrom = "' . $fromTable . '"')
            ->where('status = "ACTIVE"')
            ->where('productOrVariantId = ' . $productOrVariantId);

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $pricesAttributes = array();
        foreach ($results as $result) {
            if (! empty($this->_siteConfig['currencies']['currencies-list'][$result['currencyId']]['currency-code'])) {
                $capitalCurrency = $this->_siteConfig['currencies']['currencies-list'][$result['currencyId']]['currency-code'];
            } else {
                $capitalCurrency = 'RW';
            }
            $wholesaleKey = 'wholesalePrice' . $capitalCurrency;
            $pricesAttributes[$wholesaleKey] = $result['wholesalePrice'];
        }
        return $pricesAttributes;
    }

    /**
     * Get the merchant category list and extract only the ids and names
     *
     * @param int $productMerchantCategoryId
     * @return array
     */
    public function extractProductMerchantCategory($productMerchantCategoryId)
    {
        $categoryArray = array();
        $subCategoryArray = array();
        if (! empty($productMerchantCategoryId)) {
            $merchantCategoriesArray = $this->_merchantCategoriesMapper->rebuildMerchantCategoryByMerchantID($productMerchantCategoryId, $this->_currentLanguage);
            $categoriesCount = count($merchantCategoriesArray);
            if ($categoriesCount > 0) {
                $merchantCategoriesArray = array_reverse($merchantCategoriesArray);
                $position = 0;
                $returnArray = array();
                foreach ($merchantCategoriesArray as $parentId => $merchantCategoryArray) {
                    if (! empty($merchantCategoryArray['selected']['categoryName'])) {
                        if ($position == 0) {
                            $subCategoryArray = array(
                                'categoryName' => $merchantCategoryArray['selected']['categoryName'],
                                'categoryId' => $merchantCategoryArray['selected']['productMerchantCategoryId']
                            );
                        } else
                            if ($position == 1) {
                                $categoryArray = array(
                                    'categoryName' => $merchantCategoryArray['selected']['categoryName'],
                                    'categoryId' => $merchantCategoryArray['selected']['productMerchantCategoryId']
                                );
                            } else {
                                break;
                            }
                    }
                    $position ++;
                }
            }
        }

        return array(
            'category' => $categoryArray,
            'subCategory' => $subCategoryArray
        );
    }

    /**
     *
     * @param int $productId
     * @param boolean $isVariant
     * @return mixed
     */
    protected function _getProductPricesForSpectrum($productId, $isVariant = true)
    {
        $prices = array();
        $currenciesList = $this->_currencies['currencies-list'];

        if ($isVariant) {
            $tableName = $this->_productsVariantsRelationshipTable;
        } else {
            $tableName = $this->_tableName;
        }

        foreach ($currenciesList as $currencyId => $currencyArray) {
            $productPriceByCurrency = $this->_getProductPriceByCurrencyId($productId, $currencyId, $tableName);
            if (! empty($productPriceByCurrency)) {
                $prices[$currencyArray['currency-code']]['nowPrice'] = $productPriceByCurrency['nowPrice'];
                $prices[$currencyArray['currency-code']]['wasPrice'] = $productPriceByCurrency['wasPrice'];
                $prices[$currencyArray['currency-code']]['offerPrice'] = $productPriceByCurrency['offerPrice'];
                $prices[$currencyArray['currency-code']]['costPrice'] = $productPriceByCurrency['costPrice'];
                $prices[$currencyArray['currency-code']]['freightPrice'] = $productPriceByCurrency['freightPrice'];
                $prices[$currencyArray['currency-code']]['wholesalePrice'] = $productPriceByCurrency['wholesalePrice'];
            }
        }
        return $prices;
    }

    /**
     *
     * @param string $string
     * @return bool|int|string
     */
    public function getCurrencyIdByString($string)
    {
        $currenciesList = $this->_currencies['currencies-list'];
        foreach ($currenciesList as $currencyId => $value) {
            if ($value['currency-code'] == $string) {
                return $currencyId;
            }
        }

        return false;
    }

    /**
     *
     * @param int $productId
     * @param string $productIdFrom
     * @param int $currencyId
     * @param float | int $price
     * @param float | int $wasPrice
     */
    public function updatePriceFromSpectrum($productId, $productIdFrom, $currencyId, $price, $wasPrice)
    {
        $update = $this->_sql->update();
        $update->table($this->_productsPricesTable);
        $update->set(array(
            'price' => $price,
            'nowPrice' => $price,
            'wasPrice' => $wasPrice
        ));
        $update->where("productIdFrom = '$productIdFrom'");
        $update->where("productOrVariantId = '$productId'");
        $update->where("currencyId = '$currencyId'");
        $update->where("status = 'ACTIVE'");
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     *
     * @param int $productId
     * @param int $currencyId
     * @param string $productIdFrom
     * @return bool|mixed
     */
    protected function _getProductPriceByCurrencyId($productId, $currencyId, $productIdFrom = 'PRODUCTS_R_ATTRIBUTES')
    {
        $this->_sql->setTable($this->_productsPricesTable);
        $action = $this->_sql->select()
            ->columns(array(
                'nowPrice',
                'offerPrice',
                'wasPrice',
                'costPrice',
                'freightPrice',
                'wholesalePrice'
            ))
            ->where(array(
                "productOrVariantId" => $productId,
                "currencyId" => $currencyId,
                "productIdFrom" => $productIdFrom,
                "status" => "ACTIVE"
            ));

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        if ($result->count() == 1) {
            $price = $this->_getResultData($result, true);
        } else {
            $price = false;
        }

        return $price;
    }

    /**
     * Convert db result to array
     *
     * @param array $result
     * @param boolean $returnOne
     * @return mixed
     */
    protected function _getResultData($result, $returnOne = false, $returnNotAssoc = false)
    {
        $records = array();

        if ($returnNotAssoc) {
            foreach ($result as $key => $value) {
                $records[] = $value['productId'];
            }
        } else {
            foreach ($result as $key => $value) {
                $records[] = $value;
            }
        }

        if ($returnOne === true) {
            if ($records) {
                return $records[0];
            }
        }

        return $records;
    }

    /**
     *
     * @param int $productId
     * @param bool $isVariant
     * @return array
     */
    protected function _getProductCoreAttributes($productId, $isVariant = false, $code = false)
    {
        $finalArray = array();
        $attributes = $this->_attributesGroupMapper->fetchCoreAttributesForProduct($productId, ($isVariant ? 1 : 0), $this->_currentLanguage, $code);
        $finalArray["allow_express"] = ! empty($attributes["allow_express"]) ? $attributes["allow_express"] : 0;
        $finalArray["allow_gift_wrap"] = ! empty($attributes["allow_gift_wrap"]) ? $attributes["allow_gift_wrap"] : 0;
        $finalArray["age_restricted"] = ! empty($attributes["age_restricted"]) ? $attributes["age_restricted"] : 0;
        $finalArray["postage_exempt"] = ! empty($attributes["postage_exempt"]) ? $attributes["postage_exempt"] : 0;
        $finalArray["allow_back_orders"] = ! empty($attributes["allow_back_orders"]) ? $attributes["allow_back_orders"] : 0;
        $finalArray["suppress_from_sale"] = ! empty($attributes["suppress_from_sale"]) ? $attributes["suppress_from_sale"] : 0;
        $finalArray["screening_qty"] = ! empty($attributes["screening_qty"]) ? $attributes["screening_qty"] : 1;
        $finalArray["brand"] = ! empty($attributes["brand"]) && $attributes["brand"] ? $attributes["brand"] : '';
        $finalArray["length"] = ! empty($attributes["length"]) && $attributes["length"] ? $attributes["length"] : 0;
        $finalArray["width"] = ! empty($attributes["width"]) && $attributes["width"] ? $attributes["width"] : 0;
        $finalArray["height"] = ! empty($attributes["height"]) && $attributes["height"] ? $attributes["height"] : (isset($attributes["height_in_cm"]) ? $attributes["height_in_cm"] : 0);
        $finalArray["country_of_origin"] = ! empty($attributes["country_of_origin"]) ? $attributes["country_of_origin"] : '';
        $finalArray["free_postage"] = ! empty($attributes["free_postage"]) ? $attributes["free_postage"] : '';
        $finalArray["uk_delivery_only"] = ! empty($attributes["uk_delivery_only"]) ? $attributes["uk_delivery_only"] : '0';
        $finalArray["personalised_items"] = ! empty($attributes["personalised_items"]) ? $attributes["personalised_items"] : '';
        $finalArray["supplier"] = ! empty($attributes["supplier"]) ? $attributes["supplier"] : '';
        return $finalArray;
    }

    /**
     * This will get the vaule of the attributes
     *
     * @param int $productId
     * @param int $attributeId
     */
    protected function _getProductAttributesValueByAttributeId($productId, $attributeId)
    {}

    /**
     * Delete related product
     *
     * @param array $submitedFormData
     * @return type
     */
    public function deleteRelatedProduct($submitedFormData = array())
    {
        if (count($submitedFormData) > 0) {
            $relatedProductDeleted = $this->_productsMapperRelatedProducts->_deleteRelatedProduct($submitedFormData);
            return $relatedProductDeleted;
        }
    }

    /**
     *
     * @param
     *            $productId
     * @param int $languageId
     * @return bool
     */
    public function getProductUrl($productId, $onlyActive = true, $languageId = 1)
    {
        $this->_sql->setTable($this->_tableName);
        $action = $this->_sql->select()
            ->columns(array())
            ->join(array(
                $this->_productsDefinitionsTable => $this->_productsDefinitionsTable
            ), new Expression($this->_tableName . '.productId = ' . $this->_productsDefinitionsTable . '.productId AND ' . $this->_productsDefinitionsTable . '.productIdFrom = "' . $this->_tableName . '"'), array(
                'urlName'
            ))
            ->where("$this->_tableName.productId = '$productId'")
            ->where("$this->_productsDefinitionsTable.languageId = '$languageId'");

        if ($onlyActive) {
            $action->where("$this->_tableName.status = 'ACTIVE'");
        }
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        if ($result->count() == 1) {
            return $result->next()['urlName'];
        } else {
            return false;
        }
    }

    /**
     *
     * @param
     *            $variantId
     * @param
     *            $languageId
     * @return string
     */
    public function getVariantDetails($variantId, $languageId)
    {
        $this->_sql->setTable(array(
            'pdt' => $this->_productsDefinitionsTable
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'shortProductName',
            'productName',
            'shortDescription',
            'longDescription',
            'metaTitle',
            'metaDescription',
            'metaKeyword'
        ));
        $select->where(array(
            'productId' => $variantId,
            'productIdFrom' => $this->_productsVariantsRelationshipTable,
            'languageId' => $languageId
        ));
        $select->where('status != "DELETED"');
        $select->limit(1);

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() > 0) {
            return $results->next();
        } else {
            return '';
        }
    }

    /**
     *
     * @param
     *            $variantId
     * @return array | null
     */
    public function getVariantFullDetails($variantId)
    {
        $this->_sql->setTable(array('pra' => $this->_productsVariantsRelationshipTable));
        $select = $this->_sql->select()
            ->where('pra.productAttributeId = ' . $variantId)
            ->join(array(
                'pad' => $this->_productsDefinitionsTable
            ), new Expression('pra.productAttributeId = pad.productId AND pad.productIdFrom = "PRODUCTS_R_ATTRIBUTES"'), array(
                'shortProductName', 'productName', 'shortDescription', 'longDescription'
            ))
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        return $statement->execute()->next();
    }

    /**
     * This method is used by navigation tree
     *
     * @param string $status
     * @param string $searchFor
     * @param int $page
     * @param string $recordPerPage
     * @return array
     */
    public function fetchProductsByStatus($status = 'ACTIVE', $searchFor = null, $page = 1, $recordPerPage = NULL)
    {
        if ($recordPerPage === null) {
            $recordPerPage = $this->_recordPerPage;
        }

        $this->_sql->setTable($this->_tableName);
        $action = $this->_sql->select()
            ->columns(array())
            ->join(array(
                $this->_productsDefinitionsTable => $this->_productsDefinitionsTable
            ), new Expression($this->_tableName . '.productId = ' . $this->_productsDefinitionsTable . '.productId AND ' . $this->_productsDefinitionsTable . '.productIdFrom = "' . $this->_tableName . '"'), array(
                'productName',
                'urlName',
                'productId'
            ))
            ->join(array(
                $this->_productsClientsWebsitesTable => $this->_productsClientsWebsitesTable
            ), "$this->_productsClientsWebsitesTable.productId = $this->_tableName.productId", array())
            ->join(array(
                $this->_productsCategoriesRelationshipTable => $this->_productsCategoriesRelationshipTable
            ), "$this->_productsCategoriesRelationshipTable.productId = $this->_tableName.productId", array())
            ->join(array(
                $this->_categoriesDefinitionsTable => $this->_categoriesDefinitionsTable
            ), "$this->_categoriesDefinitionsTable.categoryId = $this->_productsCategoriesRelationshipTable.categoryId", array(
                'categoryUrlName' => 'urlName'
            ))
            ->where("$this->_tableName.status = '$status'")
            ->where("$this->_productsDefinitionsTable.status = '$status'")
            ->where("$this->_productsClientsWebsitesTable.websiteId = $this->_currentWebsiteId")
            ->where("$this->_productsClientsWebsitesTable.productIdFrom = 'PRODUCTS'")
            ->where("$this->_productsCategoriesRelationshipTable.isDefault = '1'")
            ->where("$this->_productsDefinitionsTable.languageId = $this->_currentLanguage")
            ->where("$this->_categoriesDefinitionsTable.languageId = $this->_currentLanguage");

        if ($searchFor !== null) {
            $action->where->like("$this->_productsDefinitionsTable.productName", "%$searchFor%");
        }
        return $this->_queryPaginator->getPaginatorForSelect($action, $this->_dbAdapter, $page, $recordPerPage);
    }

    /**
     * Get the productId fro urlName
     *
     * @param string $urlName
     * @param int $languageId
     * @param int $status
     * @return s mixed
     */
    public function getProductIdByUrlName($urlName, $languageId, $status)
    {
        $this->_sql->setTable($this->_tableName);
        $action = $this->_sql->select()
            ->columns(array(
                'productId'
            ))
            ->join(array(
                $this->_productsDefinitionsTable => $this->_productsDefinitionsTable
            ), new Expression($this->_tableName . '.productId = ' . $this->_productsDefinitionsTable . '.productId AND ' . $this->_productsDefinitionsTable . '.productIdFrom = "' . $this->_tableName . '"'))
            ->where("$this->_tableName.status = '$status'")
            ->where("$this->_productsDefinitionsTable.languageId = '$languageId'")
            ->where("$this->_productsDefinitionsTable.urlName = '$urlName'");
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $productId = $this->_getResultData($result, true);
        return $productId;
    }

    /**
     *
     * @param
     *            $variantIdsArray
     * @return array
     */
    public function getVariantsSkus($variantIdsArray)
    {
        $this->_sql->setTable($this->_productsVariantsRelationshipTable);
        $action = $this->_sql->select()
            ->columns(array(
                'sku'
            ))
            ->where(array(
                'productAttributeId' => $variantIdsArray
            ));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();

        $finalArray = array();
        foreach ($results as $result) {
            $finalArray[] = $result['sku'];
        }
        return $finalArray;
    }

    /**
     * @param $values
     * @return array
     */
    public function assignBarcodesAgainstOptions($values)
    {
        $returnResults = array();
        foreach ($values as $value) {
            $skuCode = $value['sku'];
            $barcode = trim($value['value']);

            $min = 4;
            $max = 30;
            if (strlen($barcode) >= $max || strlen($barcode) <= $min) {
                $returnResults[$skuCode][] = "Barcode is not between {$min} and {$max}";
                continue;
            }

            if (!ctype_alnum($barcode)) {
                $returnResults[$skuCode][] = "Barcode is not alphanumeric only";
                continue;
            }

            /** Check if barcode is already saved to DB */
            $this->_sql->setTable($this->_productBarcodesTable);
            $select = $this->_sql->select()
                ->where(array('barcode' => $barcode));
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            if (count($results) > 0) {
                $returnResults[$skuCode][] = 'Barcode is already used';
                continue;
            } else {
                $variantId = $this->getVariantIdForSku($skuCode);

                if (!$variantId) {
                    $returnResults[$skuCode][] = 'Could not find SKU in the database. Please contact PRISM-DM quoting BDB-001';
                    continue;
                }
                /** Barcode is not used - we need to mark it at used */
                $insertData = array(
                    'barcode' => $barcode,
                    'inUse' => 1
                );
                $this->_sql->setTable($this->_productBarcodesTable);
                $action = $this->_sql->insert();
                $action->values($insertData);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();

                /** After marking at used we need to assign it to the SKU */
                $update = $this->_sql->update();
                $update->table($this->_productsVariantsRelationshipTable);
                $update->set(array(
                    'ean13' => $barcode
                ));
                $update->where(array('productAttributeId' => $variantId));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();

                $optionCode = $this->getOptionCodeForVariantId($variantId);

                $this->queueService->addSpectrumEntryToQueueForCommonAttributes($variantId);
                $this->queueService->addSipEntryToQueue($optionCode, 5, $this);
                $this->queueService->addSipEntryToQueue($optionCode, 6, $this);
            }
        }
        return $returnResults;
    }

    /**
     * @param $skuCode
     * @return bool
     */
    public function getVariantIdForSku($skuCode)
    {
        $this->_sql->setTable($this->_productsVariantsRelationshipTable);
        $select = $this->_sql->select()
            ->where(array('sku' => $skuCode))
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();
        if (!empty($result['productAttributeId'])) {
            return $result['productAttributeId'];
        }
        return false;
    }

    /**
     * @param $productId
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getVariantsBasedOnProductId($productId)
    {
        $this->_sql->setTable($this->_productsVariantsRelationshipTable);
        $select = $this->_sql->select()
            ->where(array('productId' => $productId));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    /**
     *
     * @param
     *            $productId
     * @return array
     */
    public function getProductSku($productId)
    {
        $this->_sql->setTable($this->_tableName);
        $action = $this->_sql->select()
            ->columns(array(
                'sku'
            ))
            ->where(array(
                'productId' => $productId
            ));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();

        $finalArray = array();
        foreach ($results as $result) {
            $finalArray[] = $result['sku'];
        }
        return $finalArray;
    }

    /**
     *
     * @param
     *            $productId
     * @return array
     */
    public function getProductStyle($productId)
    {
        $this->_sql->setTable($this->_tableName);
        $action = $this->_sql->select()
            ->columns(array(
                'style'
            ))
            ->where(array(
                'productId' => $productId
            ));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();

        $finalArray = array();
        foreach ($results as $result) {
            $finalArray[] = $result['style'];
        }
        return $finalArray;
    }

    /**
     *
     * @param
     *            $productOrVariantId
     * @param int $isVariant
     * @param string $status
     * @return bool
     */
    public function updateStatus($productOrVariantId, $isVariant = 0, $status = 'ACTIVE')
    {
        if ($isVariant == 0) {
            $table = $this->_tableName;
            $column = 'productId';
        } else {
            $table = $this->_productsVariantsRelationshipTable;
            $column = 'productAttributeId';
        }
        $update = $this->_sql->update();
        $update->table($table);
        $update->set(array(
            'status' => $status
        ));
        $update->where($column . " = '" . $productOrVariantId . "'");
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
        return true;
    }

    /**
     * Function to activate product
     *
     * @param type $skuName
     * @param type $status
     * @return type
     */
    public function updateProductStatusByName($productCodesArray = array(), $status, $activateVariant = false, $updateMainProduct = true)
    {
        $productUpdated = 0;
        $variantStatusUpdated = 0;
        $productUpdatedArray = array();
        $productNotUpdatedArray = array();
        if (count($productCodesArray) > 0) {
            foreach ($productCodesArray as $skuName) {
                if (trim($skuName) != '') {

                    if ($updateMainProduct) {
                        $table = $this->_tableName;
                        $whereClause = '';
                        $fieldsToUpdate = array(
                            'status' => $status
                        );
                        $whereNoEqualTo = array(
                            0 => array(
                                'status',
                                'DELETED'
                            )
                        );
                        $whereLike = array(
                            'sku',
                            "{$skuName}%"
                        );
                        $productStatusUpdated = $this->_update($fieldsToUpdate, $table, $whereClause, $whereNoEqualTo, $whereLike);

                        if ($productStatusUpdated > 0) {
                            $productUpdated ++;
                        }
                    }

                    if ($activateVariant === true) {
                        // Update variants with the sanme sku
                        $variantStatusUpdated = $this->updateVariantStatusByName($skuName, $status);
                    }

                    if (! empty($productStatusUpdated) || $variantStatusUpdated > 0) {
                        // Get the id of the updated product so we can update elastic search and spectrum
                        $productIds = $this->getProductIdFromSku($skuName);
                        if (is_array($productIds) && ! empty($productIds)) {
                            reset($productIds);
                            $firstArrayKey = key($productIds);
                            if (! empty($productIds[$firstArrayKey]['productId'])) {
                                $productUpdatedArray[$skuName] = $productIds[$firstArrayKey]['productId'];
                            }
                        }
                    } else {
                        $productNotUpdatedArray[] = $skuName;
                    }
                }
            }

            // Cascade the status change to other tables
            if (count($productUpdatedArray) > 0) {
                foreach ($productUpdatedArray as $productId) {
                    $tablesToUpdate = $this->getTableListToChangeStatus($productId);
                    foreach ($tablesToUpdate as $key => $updatedetails) {
                        $table = $updatedetails['tableName'];
                        $whereClause = $updatedetails['whereClause'];
                        $this->cascadeStatusChanges($status, $table, $whereClause);
                    }
                }
            }
        }

        return array(
            'productUpdatedArray' => $productUpdatedArray,
            'productNotUpdatedArray' => $productNotUpdatedArray
        ); // $returnArray;
    }

    /**
     * A list of tables whose staus needs to be updated when a product status is updated
     *
     * @param type $productId
     * @return type
     */
    protected function getTableListToChangeStatus($productId)
    {
        return array(
            // 0 => array('tableName' => $this->_productsChannelsTable, 'whereClause' => "productId={$productId} AND status <> 'DELETED'"),
            // 1 => array('tableName' => $this->_productsRelatedProductsTable, 'whereClause' => "(productId={$productId} OR relatedproductid = {$productId}) AND status <> 'DELETED'"),
            // 2 => array('tableName' => $this->_productsCategoriesRelationshipTable, 'whereClause' => "productId={$productId} AND status <> 'DELETED'"),
            3 => array(
                'tableName' => $this->_productsDefinitionsTable,
                'whereClause' => "productId={$productId} AND status <> 'DELETED'"
            )
        );
        // 4 => array('tableName' => $this->_productsTagsTable, 'whereClause' => "productId={$productId} AND status <> 'DELETED'"),
        // 5 => array('tableName' => $this->_productTabsTable, 'whereClause' => "productId={$productId} AND status <> 'DELETED'"),

    }

    /**
     * Cascade the status change to other tables
     *
     * @param type $status
     * @param type $table
     * @param type $whereClause
     * @return type
     */
    public function cascadeStatusChanges($status, $table, $whereClause)
    {
        $whereClause = $whereClause;
        $fieldsToUpdate = array(
            'status' => $status
        );
        $productVariantsDeactivated = $this->_update($fieldsToUpdate, $table, $whereClause);

        return $productVariantsDeactivated;
    }

    /**
     * Function to update variant status
     *
     * @param string $skuName
     * @param string $status
     * @param int|boolean $variantId
     * @return boolean
     */
    public function updateVariantStatusByName($skuName, $status, $variantId = false)
    {
        $variantStatusUpdated = 0;
        $variantUpdated = 0;
        if (trim($skuName) != '') {

            $table = $this->_productsVariantsRelationshipTable;
            $whereClause = '';
            $fieldsToUpdate = array(
                'status' => $status
            );
            $whereLike = array();
            if ($variantId) {
                $whereClause = array(
                    'productAttributeId' => $variantId
                );
            } else {
                $whereLike = array(
                    'sku',
                    "{$skuName}%"
                );
            }
            $whereNoEqualTo = array(
                0 => array(
                    'status',
                    'DELETED'
                )
            );

            $variantStatusUpdated = $this->_update($fieldsToUpdate, $table, $whereClause, $whereNoEqualTo, $whereLike);
        }

        return $variantStatusUpdated;
    }

    /**
     *
     * @param
     *            $submitedData
     * @return int
     */
    public function updateVariantsPrices($submitedData)
    {
        $productId = $submitedData['productId'];
        $variantId = $submitedData['variantId'];
        $productPrices = $this->_extractPriceInformation($submitedData);
        $supplierPrice = $this->_extractSupplierPrice($submitedData);
        $this->_saveSupplierPriceForVariants($supplierPrice, $submitedData);

        return $this->_saveProductPrices($productPrices, $variantId, 1);
    }

    /**
     *
     * @param
     *            $variantId
     * @return bool|void
     */
    public function getSupplierPrices($productOrVariantId, $productIdFrom = 'PRODUCTS_R_ATTRIBUTES')
    {
        $this->_sql->setTable($this->_productSupplierPriceTable);
        $select = $this->_sql->select()
            ->columns(array(
                'currencyCode',
                'price'
            ))
            ->where(array(
                'productOrVariantId' => $productOrVariantId,
                'productIdFrom' => $productIdFrom
            ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results->count() > 0) {
            return $results->next();
        } else {
            return false;
        }
    }

    /**
     *
     * @param
     *            $supplierPrice
     * @param
     *            $submitedData
     * @return bool
     */
    public function _saveSupplierPriceForVariants($supplierPrice, $submitedData, $sqlObject = null)
    {
        if ($sqlObject !== null) {
            $this->_sql = $sqlObject;
        }
        // Try to find the old supplier price for variant
        $this->_sql->setTable($this->_productSupplierPriceTable);
        $action = $this->_sql->select()->where(array(
            'productOrVariantId' => $submitedData['variantId'],
            'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES'
        ));

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        if ($result->count() > 0) {
            // Update old record
            $update = $this->_sql->update();
            $update->table($this->_productSupplierPriceTable);
            $update->set(array(
                'currencyCode' => $supplierPrice['productSupplierCurrency'],
                'price' => $supplierPrice['supplierPrice']
            ));
            $update->where("supplierPriceId = " . $result->next()['supplierPriceId']);
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            $finalResult = $statement->execute();
        } else {
            // Insert new record
            $this->_sql->setTable($this->_productSupplierPriceTable);
            $action = $this->_sql->insert();
            $action->values(array(
                'currencyCode' => $supplierPrice['productSupplierCurrency'],
                'price' => $supplierPrice['supplierPrice'],
                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                'productOrVariantId' => $submitedData['variantId'],
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $finalResult = $statement->execute();
        }

        if ($finalResult->getAffectedRows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param
     *            $submitedData
     * @return array
     */
    private function _extractSupplierPrice($submitedData)
    {
        if (empty($submitedData['supplierPrice'])) {
            $submitedData['supplierPrice'] = 0;
        }
        if (empty($submitedData['productSupplierCurrency'])) {
            $submitedData['productSupplierCurrency'] = $this->_siteConfig['currencies']['currencies-list'][$this->_siteConfig['currencies']['default-currency']]['currency-code'];
        }

        return array(
            'supplierPrice' => $submitedData['supplierPrice'],
            'productSupplierCurrency' => $submitedData['productSupplierCurrency']
        );
    }

    /**
     *
     * @param
     *            $productId
     * @param int $isVariant
     * @return bool
     */
    public function checkIfProductHasBeenSavedInSpectrum($productId, $isVariant = 0)
    {
        if ($isVariant == 0) {
            $tableFrom = $this->_tableName;
        } else {
            $tableFrom = $this->_productsVariantsRelationshipTable;
        }
        $this->_sql->setTable($this->_productsSpectrumTable);
        $action = $this->_sql->select()
            ->where(array(
                'productId' => $productId,
                'productIdFrom' => $tableFrom
            ))
            ->where('spectrumId != 0');
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();

        return ($results->count() == 0) ? false : $results->next()['spectrumId'];
    }

    /**
     * @param $productOrVariantId
     * @param int $isVariant
     * @return array
     */
    public function getHighResImageForProduct($productOrVariantId, $isVariant = 0)
    {
        if ($isVariant == 0) {
            $variantId = 0;
            $productId = $productOrVariantId;
        } else {
            $variantId = $productOrVariantId;
            $productId = $this->getProductIdFromVariantId($variantId);
        }
        $finalImages = array();
        $productImagesFolder = $this->_productFolderMapper->getProductFolderByProductAndVariant($productId, $variantId);
        $defaultImage = '';
        if (! empty($productImagesFolder)) {
            $mediaFolderContent = $this->_productFolderMapper->getProductImagesFromMediaFolderId($productImagesFolder['mediaFolderId']);


            if (! empty($mediaFolderContent)) {
                foreach ($mediaFolderContent as $image) {
                    $imageUrl = $this->_mediaServer->getImageUrl($image->ref, 'lpr', true, true);
                    if ($productImagesFolder['defaultImageId'] == $image->ref) {
                        $defaultImage = $imageUrl;
                    }
                    $finalImages[] = $imageUrl;
                }
            }
        }

        if (empty($defaultImage) && !empty($productImagesFolder['defaultImageId'])) {
            $defaultImage = $imageUrl = $this->_mediaServer->getImageUrl($productImagesFolder['defaultImageId'], 'lpr', true, true);
        }

        return array('images' => $finalImages, 'default' => $defaultImage);
    }
    /**
     *
     * @param
     *            $productId
     * @param int $isVariant
     * @return array
     */
    public function getImagesForProduct($productId, $isVariant = 0)
    {
        // TODO
        // TO BE REFACTORED
        // Use the MediaManager\ProductFolderMapper functions and not direct access to the table
        if ($isVariant == 0) {
            $variantId = 0;
        } else {
            $variantId = $productId;
            $productId = $this->getProductIdFromVariantId($variantId);
        }

        $productImagesFolder = $this->_productFolderMapper->getProductFolderByProductAndVariant($productId, $variantId);
        if (! empty($productImagesFolder)) {
            $finalResponse = array();
            $finalResponse['images'] = array();
            $finalResponse['images_medium'] = array();

            $mediaFolderContent = $this->_productFolderMapper->getProductImagesFromMediaFolderId($productImagesFolder['mediaFolderId']);
            $defaultUrl = $this->_mediaServer->getImageUrl($productImagesFolder['defaultImageId'], 'elastic-search-large', true); // mediaId, size
            $mediumDefaultUrl = $this->_mediaServer->getImageUrl($productImagesFolder['defaultImageId'], 'elastic-search-medium', true); // mediaId, size
            $thumbDefaultUrl = $this->_mediaServer->getImageUrl($productImagesFolder['defaultImageId'], 'elastic-search-thumb', true); // mediaId, size
            $menuImageUrl = $this->_mediaServer->getImageUrl($productImagesFolder['menuImageId'], 'elastic-search-menu', true); // mediaId, size
            $hoverImageUrl = $this->_mediaServer->getImageUrl($productImagesFolder['hoverImageId'], 'elastic-search-hover', true); // mediaId, size
            $basketImageUrl = $this->_mediaServer->getImageUrl($productImagesFolder['basketImageId'], 'elastic-search-basket', true); // mediaId, size

            if (! empty($mediaFolderContent)) {
                foreach ($mediaFolderContent as $image) {
                    $urlPrefix = '';
                    $imageLargeUrl = '';
                    $imageMediumUrl = '';
                    if ($image->resource_type == 7 && isset($image->youtubeLink)) {
                        $imageLargeUrl = $image->youtubeLink;
                        $imageMediumUrl = $image->youtubeLink;
                        $videoID = '';

                        if (isset($image->youtubeVideoId) && ! empty($image->youtubeVideoId)) {
                            $videoID = $image->youtubeVideoId;
                        }
                        $finalResponse['product_page_media'][] = array(
                            'Type' => 'Video',
                            'Link' => $imageLargeUrl,
                            'Source' => 'Youtube',
                            'videoID' => $videoID
                        );
                    } else {

                        $imageLargeUrl = $this->_mediaServer->getImageUrl($image->ref, 'elastic-search-large', true);
                        $imageMediumUrl = $this->_mediaServer->getImageUrl($image->ref, 'elastic-search-medium', true);

                        $finalResponse['product_page_media'][] = array(
                            'Type' => 'Image',
                            'Link' => $image->ref,
                            'LinkMedium' => $imageMediumUrl,
                            'LinkLarge' => $imageLargeUrl
                        );
                    }
                    // Append a prefix to the url so we can differentiate between an image and a video url
                    // $urlPrefix = isset($image->resource_type) && $image->resource_type == 7 ? 'video-' : 'image-';

                    $finalResponse['images'][] = ! empty($imageLargeUrl) ? $urlPrefix . $imageLargeUrl : '';
                    $finalResponse['images_medium'][] = ! empty($imageMediumUrl) ? $urlPrefix . $imageMediumUrl : '';
                }
            } else {
                $finalResponse['product_page_media'][] = array(
                    'Type' => 'Image',
                    'Link' => $defaultUrl,
                    'LinkMedium' => $mediumDefaultUrl,
                    'LinkLarge' => $mediumDefaultUrl
                );
            }
            $defaultUrl = $this->_mediaServer->getImageUrl($productImagesFolder['defaultImageId'], 'elastic-search-large', true); // mediaId, size
            $mediumDefaultUrl = $this->_mediaServer->getImageUrl($productImagesFolder['defaultImageId'], 'elastic-search-medium', true); // mediaId, size
            $thumbDefaultUrl = $this->_mediaServer->getImageUrl($productImagesFolder['defaultImageId'], 'elastic-search-thumb', true); // mediaId, size
            $menuImageUrl = $this->_mediaServer->getImageUrl($productImagesFolder['menuImageId'], 'elastic-search-menu', true); // mediaId, size
            $hoverImageUrl = $this->_mediaServer->getImageUrl($productImagesFolder['hoverImageId'], 'elastic-search-hover', true); // mediaId, size
            $basketImageUrl = $this->_mediaServer->getImageUrl($productImagesFolder['basketImageId'], 'elastic-search-basket', true); // mediaId, size
            $overlayImageUrl = $this->_mediaServer->getImageUrl($productImagesFolder['overlayImageId'], 'elastic-search-medium', false); // mediaId, size

            // $finalResponse['default'] = !empty($productMedia['defaultImageId']) ? $productMedia['defaultImageId'] : '';
            $finalResponse['default_url'] = ! empty($defaultUrl) ? $defaultUrl : '';
            $finalResponse['default_url_thumb'] = ! empty($thumbDefaultUrl) ? $thumbDefaultUrl : '';
            $finalResponse['default_url_medium'] = ! empty($mediumDefaultUrl) ? $mediumDefaultUrl : '';
            $finalResponse['menu'] = ! empty($menuImageUrl) ? $menuImageUrl : '';
            $finalResponse['hover'] = ! empty($hoverImageUrl) ? $hoverImageUrl : '';
            $finalResponse['basket'] = ! empty($basketImageUrl) ? $basketImageUrl : '';
            $finalResponse['overlay'] = ! empty($overlayImageUrl) ? $overlayImageUrl : '';

            return $finalResponse;
        } else {
            return array(
                'images' => array(),
                'images_medium' => array(),
                'default' => '',
                'default_url' => '',
                'default_url_thumb' => '',
                'default_url_medium' => '',
                'menu' => '',
                'basket' => '',
                'hover' => '',
                'overlay' => '',
                'product_page_media' => ''
            );
        }
    }

    /**
     *
     * @param
     *            $colourImageId
     * @return int
     */
    public function getColourImage($colourImageId)
    {
        if (empty($colourImageId)) {
            return 0;
        }
        $mediaDetails = json_decode($this->_mediaServer->getImage($colourImageId, 'colour-image')); // mediaId, size
        return isset($mediaDetails[0]->preview) ? $mediaDetails[0]->preview : 0;
    }

    /**
     * Function to get product id based on sku
     *
     * $returnAsBatchArray is a flag use to determine if the returned array
     * should be an associative array with the sku as the key. This will be
     * useful for retrieving product id in batches.
     *
     * @param type $skuString
     * @return type
     */
    public function getProductIdFromSku($skuStringArray)
    {
        $productIds = array();

        // Check if the sku supplied is a string
        if (is_string($skuStringArray)) {
            // Attempt to get the product ids based on the main product
            $productIds = $this->getMainProductIdByStyle($skuStringArray);
            // If no ids were returned, attempt to get the ids based on style and colour
            if (count($productIds) < 1) {
                $productIds = $this->getMainProductIdByStyleAndColour($skuStringArray);
            }
        } else
            if (is_array($skuStringArray)) {

                // Get a count of the sku
                // If the count is greater than 1, we want to return an associative array with the skus as keys
                $skuStringArrayCount = count($skuStringArray);

                foreach ($skuStringArray as $skuString) {

                    if (is_string($skuString) && trim($skuString) != '') {

                        if ($skuStringArrayCount > 1) {

                            // Attempt to get the product ids based on the main product
                            $productIds[$skuString] = $this->getMainProductIdByStyle($skuString);
                            // If no ids were returned, attempt to get the ids based on style and colour
                            if (count($productIds) < 1) {
                                $productIds[$skuString] = $this->getMainProductIdByStyleAndColour($skuString);
                            }
                        } else {

                            // Attempt to get the product ids based on the main product
                            $productIds = $this->getMainProductIdByStyle($skuString);
                            // If no ids were returned, attempt to get the ids based on style and colour
                            if (count($productIds) < 1) {
                                $productIds = $this->getMainProductIdByStyleAndColour($skuString);
                            }
                        }
                    }
                }
            }

        return $productIds;
    }

    /**
     *
     * @param
     *            $variantId
     * @return mixed
     */
    public function getProductIdFromVariantId($variantId)
    {
        $this->_sql->setTable(array(
            'prv' => $this->_productsVariantsRelationshipTable
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'productId'
        ));
        $select->where('productAttributeId = "' . $variantId . '"');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() == 1) {
            return $results->next()['productId'];
        } else {
            return false;
        }
    }

    /**
     * Function to get product id based on style and colour
     *
     * $returnAsBatchArray is a flag use to determine if the returned array
     * should be an associative array with the sku as the key. This will be
     * useful for retrieving product id in batches.
     *
     * @param type $skuString
     * @return type
     */
    public function getMainProductIdByStyleAndColour($skuString)
    {
        $returnArray = array();

        $colourCode = substr($skuString, - 3);
        $styleLength = strlen($skuString) - 3;
        $style = substr($skuString, 0, $styleLength);

        $this->_sql->setTable(array(
            'cgt' => $this->_coloursGroupsTable
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'colourGroupId'
        ));
        $select->join(array(
            'pasrrt' => $this->_productsAttributesSkuRuleRelationsTable
        ), 'cgt.colourGroupId = pasrrt.skuRuleId', array(
            'productAttributeId'
        ));
        $select->join(array(
            'pvr' => $this->_productsVariantsRelationshipTable
        ), 'pvr.productAttributeId = pasrrt.productAttributeId', array(
            'productId',
            'variantId' => 'productAttributeId',
            'sku'
        ));
        $select->where(array(
            'cgt.groupCode' => $colourCode,
            'cgt.enabled' => 1,
            'pasrrt.skuRuleTable' => $this->_coloursGroupsTable
        ));
        $select->where->notEqualTo('pvr.status', 'DELETED');

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $position = 1;
        $variantIds = array();
        $productId = 0;
        foreach ($results as $value) {
            // Check if the returned sku's contain the style and colour
            if (stripos($value['sku'], $style) !== FALSE && stripos($value['sku'], $colourCode) !== FALSE) {
                $variantId = $value['variantId'] === null ? 0 : $value['variantId'];
                $variantIds[] = $variantId;
                $productId = $value['productId'];
                $returnArray[$position] = array(
                    'productId' => $value['productId'],
                    'variantId' => $variantId
                );
                $position ++;
            }
        }

        if (count($returnArray) > 0 && ! in_array(0, $variantIds)) {
            $returnArray[$position ++] = array(
                'productId' => $productId,
                'variantId' => 0
            );
        }
        // Sort the multidimensional array
        // ksort($returnArray);

        return $returnArray;
    }

    /**
     *
     * @param string  $sku
     * @return bool
     */
    public function getVariantIdFromSku($sku)
    {
        $this->_sql->setTable(array(
            'pra' => $this->_productsVariantsRelationshipTable
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'productAttributeId'
        ));
        $select->where("pra.sku = '$sku'");
        $select->where("pra.status != 'DELETED'");
        $select->limit(1);

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() == 1) {
            return $results->next()['productAttributeId'];
        } else {
            return false;
        }
    }

    /**
     * This function gets product id based on sku for the main product
     *
     * $returnAsBatchArray is a flag use to determine if the returned array
     * should be an associative array with the sku as the key. This will be
     * useful for retrieving product id in batches.
     *
     * @param string $skuString
     * @return array
     */
    public function getMainProductIdByStyle($skuString)
    {
        $returnArray = array();

        $this->_sql->setTable(array(
            'p' => $this->_tableName
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'productId'
        ));
        $select->join(array(
            'pvr' => $this->_productsVariantsRelationshipTable
        ), 'pvr.productId = p.productId', array(
            'variantId' => 'productAttributeId'
        ), Select::JOIN_LEFT);
        $select->where->nest->equalTo('pvr.sku', $skuString)->or->equalTo('p.style', $skuString)->unnest;

        $select->where->notEqualTo('p.status', 'DELETED');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $position = 1;
        $variantIds = array();
        $productId = 0;
        foreach ($results as $value) {
            $variantId = $value['variantId'] === null ? 0 : $value['variantId'];
            $variantIds[] = $variantId;
            $productId = $value['productId'];
            $returnArray[$position] = array(
                'productId' => $value['productId'],
                'variantId' => $variantId
            );

            $position ++;
        }
        if (count($returnArray) > 0 && ! in_array(0, $variantIds)) {
            $returnArray[0] = array(
                'productId' => $productId,
                'variantId' => 0
            );
        }
        // Sort the multidimensional array
        ksort($returnArray);

        return $returnArray;
    }

    /**
     *
     * @param
     *            $data
     * @throws \Exception
     */
    public function bulkUploadBarcodes($data)
    {
        // Check the file and Save it physical to the server
        $httpAdapter = new \Zend\File\Transfer\Adapter\Http();
        $fileSize = new \Zend\Validator\File\Size(array(
            'max' => 20000000
        )); // 20MB
        $extension = new \Zend\Validator\File\Extension(array(
            'extension' => array(
                'xls',
                'xlsx'
            )
        ));
        $httpAdapter->setValidators(array(
            $fileSize,
            $extension
        ), $data['barcodesFile']['name']);

        if ($httpAdapter->isValid()) {
            $uploadPath = ROOT_PATH . '/public/uploads/';
            $httpAdapter->setDestination($uploadPath);
            if ($httpAdapter->receive($data['barcodesFile']['name'])) {
                $newFile = $httpAdapter->getFileName();
            }
        }
        if (empty($newFile)) {
            return array();
        }
        // Reads the xls file and converts content into array
        $excelReader = \PHPExcel_IOFactory::createReaderForFile($newFile);
        $excelReader->setReadDataOnly();

        // The default behavior is to load all sheets
        $excelReader->setLoadAllSheets();
        /** @var PHPExcel $excelObj */
        $excelObj = $excelReader->load($newFile);
        $excelObj->getActiveSheet()->toArray(null, true, true, true);
        $worksheetNames = $excelObj->getSheetNames($newFile);
        $rows = array();
        foreach ($worksheetNames as $sheetName) {
            // set the current active worksheet by name
            $excelObj->setActiveSheetIndexByName($sheetName);
            // create an assoc array with the sheet name as key and the sheet contents array as value
            $rows = $excelObj->getActiveSheet()->toArray(null, true, true, true);
            break;
        }

        /**
         * begin mysql transaction if something goes wrong we will need to rollback all the changes so we don't mess data up
         */
        $this->_dbAdapter->getDriver()
            ->getConnection()
            ->beginTransaction();
        try {
            $errors = 0;
            foreach ($rows as $rowNo => $row) {
                $row['A'] = trim($row['A']);
                if (! empty($row['A']) && preg_match('/[0-9A-Za-z]+/', $row['A'])) {
                    // we need to save all the rows in the database
                    $this->_sql->setTable($this->_productBarcodesTable);
                    $select = $this->_sql->select();
                    $select->where('barcode = "' . $row['A'] . '"');
                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $results = $statement->execute();

                    if ($results->count() > 0) {
                        $errors ++;
                        $rows[$rowNo]['errors'][] = 'This barcode is already in use!';
                    } else {
                        $this->_sql->setTable($this->_productBarcodesTable);
                        $action = $this->_sql->insert();
                        $action->values(array(
                            'barcode' => $row['A'],
                            'inUse' => 0,
                            'created' => date('Y-m-d H:i:s')
                        ));
                        $statement = $this->_sql->prepareStatementForSqlObject($action);
                        $statement->execute();
                    }
                } else {
                    $errors ++;
                    $rows[$rowNo]['errors'][] = 'The value specified is invalid for a barcode! It must contain only letters and numbers!';
                }
            }

            if ($errors > 0) {
                // revert the changes done to the database and download excel spreadsheet with errors
                $this->_dbAdapter->getDriver()
                    ->getConnection()
                    ->rollback();
                $this->downloadExcelBarcodesTemplate($rows);
            }

            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->commit();
        } catch (\Exception $e) {
            // revert the changes done to the database and throw exception
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->rollback();
            throw new \Exception($e);
        }
    }

    /**
     *
     * @param array $rows
     * @throws \Exception
     */
    public function downloadExcelBarcodesTemplate($rows = array())
    {
        $excelReader = new \PHPExcel();
        $excelReader->getProperties()->setCreator('Prism DM');
        $excelReader->getProperties()->setTitle('Barcode Upload');
        $excelReader->getProperties()->setSubject('Barcode Upload');
        $excelReader->getProperties()->setDescription('Barcode Upload');

        $i = 0;
        $excelReader->createSheet($i);
        $excelReader->getActiveSheetIndex($i);
        $excelReader->setActiveSheetIndex($i);

        if (empty($rows)) {
            // populate some test data
            $rows = array(
                '1' => array(
                    'A' => '100000000001'
                ),
                '2' => array(
                    'A' => '100000000002'
                ),
                '3' => array(
                    'A' => '100000000003'
                ),
                '4' => array(
                    'A' => '10000ABC0003'
                )
            );
        }
        foreach ($rows as $rowNumber => $row) {
            $excelReader->getActiveSheet()->setCellValue('A' . $rowNumber, $row['A']);
            if (isset($rows[$rowNumber]['errors'])) {
                $excelReader->getActiveSheet()->setCellValue('B' . $rowNumber, implode(', ', $rows[$rowNumber]['errors']));
                $excelReader->getActiveSheet()
                    ->getStyle('B' . $rowNumber)
                    ->getFill()
                    ->applyFromArray(array(
                        'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'rgb' => 'F28A8C'
                        )
                    ));
            }
        }

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="barcodeUpload.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($excelReader, 'Excel5');
        $objWriter->save('php://output');
        exit();
    }

    /**
     *
     * @param
     *            $variantId
     * @param
     *            $currencyId
     * @param
     *            $territoryId
     * @param
     *            $priceColumn
     */
    public function getVariantPriceBasedOnCurrencyAndTerritory($variantId, $currencyId, $territoryId, $priceColumn)
    {
        $this->_sql->setTable($this->_productsPricesTable);
        $select = $this->_sql->select()
            ->columns(array(
                $priceColumn,
                'productPriceId'
            ))
            ->where(array(
                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                'productOrVariantId' => $variantId,
                'currencyId' => $currencyId,
                'regionId' => $territoryId,
                'status' => 'ACTIVE'
            ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();

        return $result;
    }

    /**
     *
     * @param
     *            $productPriceEntryId
     * @param
     *            $priceColumn
     * @param
     *            $value
     * @return bool
     */
    public function updatePriceEntry($productPriceEntryId, $priceColumn, $value)
    {
        try {
            $this->_sql->setTable($this->_productsPricesTable);
            $update = $this->_sql->update()
                ->set(array(
                    $priceColumn => $value
                ))
                ->where(array(
                    'productPriceId' => $productPriceEntryId
                ));
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();

            if ($result->getAffectedRows() > 0) {
                return true;
            }
        } catch (\Exception $ex) {
            return false;
        }
        return false;
    }

    /**
     *
     * @param
     *            $variantId
     * @param
     *            $priceColumn
     * @param
     *            $value
     * @param
     *            $currencyId
     * @param
     *            $territoryId
     * @return bool
     */
    public function insertNewPriceEntryToVariant($variantId, $priceColumn, $value, $currencyId, $territoryId)
    {
        try {
            $this->_sql->setTable($this->_productsPricesTable);
            $insert = $this->_sql->insert()->values(array(
                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                'productOrVariantId' => $variantId,
                'regionId' => $territoryId,
                'currencyId' => $currencyId,
                $priceColumn => $value
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($insert);
            $result = $statement->execute();
            if ($result->getAffectedRows() > 0) {
                return true;
            }
        } catch (\Exception $ex) {
            return false;
        }
        return false;
    }

    /**
     *
     * @return mixed
     */
    public function getNextAvailableBarcodeForProduct()
    {
        $this->_sql->setTable($this->_productBarcodesTable);
        $select = $this->_sql->select();
        $select->where('inUse = "0"');
        $select->order('barcodeId ASC');
        $select->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() == 0) {
            return false;
        } else {
            $result = $results->next();
            $update = $this->_sql->update();
            $update->table($this->_productBarcodesTable);
            $update->set(array(
                'inUse' => 1
            ));
            $update->where("barcodeId = '$result[barcodeId]'");
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            $statement->execute();

            return $result['barcode'];
        }
    }

    /**
     *
     * @param
     *            $languageId
     */
    public function getVoucherTypeId($languageId = null)
    {
        // Get the Voucher product type id from the db
        $this->_sql->setTable($this->_productsTypesTable);
        $select = $this->_sql->select()
            ->join(array(
                'ptd' => $this->_productsTypesDefinitionsTable
            ), $this->_productsTypesTable . '.productTypeId = ptd.productTypeId', array(
                '*'
            ), Select::JOIN_LEFT)
            ->where('ptd.name LIKE "%EVOUCHER%"');
        if ($languageId !== null) {
            $select->where('ptd.languageId = ' . $languageId);
        }

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result->next();
    }

    /**
     *
     * @param
     *            $excludeProduct
     * @return bool
     */
    public function checkIfAnyVoucherProductIsActive($excludeProduct)
    {
        $voucherTypeId = $this->getVoucherTypeId($this->_currentLanguage);
        $this->_sql->setTable($this->_tableName);
        $select = $this->_sql->select()
            ->where('status = "ACTIVE"')
            ->where('productTypeId = ' . $voucherTypeId['productTypeId']);

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $product = $result->next();

        if ($result->count() > 0 && $product['productId'] != $excludeProduct) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param
     *            $productId
     * @return bool
     */
    public function checkIfProductIsVoucher($productId)
    {
        $voucherTypeId = $this->getVoucherTypeId($this->_currentLanguage);

        $this->_sql->setTable($this->_tableName);
        $select = $this->_sql->select()->where('productId = ' . $productId);
        $statement = $this->_sql->prepareStatementForSqlObject($select);

        $product = $statement->execute()->next();

        if ($product['productTypeId'] == $voucherTypeId['productTypeId']) {
            return true;
        }

        return false;
    }

    /**
     * Return all definition that should be activated today
     */
    public function productDefinitionMakeLiveList()
    {
        $today = date('Y-m-d H:i:s');

        // $this-> _productsDefinitionsTable;

        $this->_sql->setTable($this->_productsDefinitionsTable);
        $select = $this->_sql->select();
        $select->where("makeLive <> '0000-00-00 00:00:00'");
        $select->where("makeLive <= '$today'");
        $select->where("status = 'INACTIVE'");
        $statement = $this->_sql->prepareStatementForSqlObject($select);

        $sqlRresult = $statement->execute();

        return $this->_dbResutToArray($sqlRresult);
    }

    /**
     * Process the product definition
     *
     * @param array $productDefinitionList
     */
    public function proccessProductDefinitionList($productDefinitionList = null)
    {
        $processedProducts = array();
        if ($productDefinitionList === null) {
            $productDefinitionList = $this->productDefinitionMakeLiveList();
        }

        for ($i = 0, $count = count($productDefinitionList); $i < $count; $i ++) {
            $productDefinitionsId = $productDefinitionList[$i]['productDefinitionsId'];
            $productId = $productDefinitionList[$i]['productId'];
            $languageId = $productDefinitionList[$i]['languageId'];
            $processedProducts[] = $productId;
            try {
                $inacitvateProductDefinition = $this->inacitvateProductDefinition($productId, $languageId);
                $acitvateProductDefinition = $this->acitvateProductDefinition($productDefinitionsId);
            } catch (Exception $e) {}
        }
        return $processedProducts;
    }

    /**
     * Inactivate product definition
     *
     * @param int $productId
     * @param int $languageId
     */
    public function inacitvateProductDefinition($productId, $languageId)
    {
        $update = $this->_sql->update();
        $update->table($this->_productsDefinitionsTable);
        $update->set(array(
            'status' => 'INACTIVE'
        ));
        $update->where("productId = '$productId'");
        $update->where("languageId = '$languageId'");
        $update->where("status = 'ACTIVE'");
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     * Activate product definition
     *
     * @param int $productId
     * @param int $languageId
     */
    public function acitvateProductDefinition($productDefinitionsId)
    {
        $update = $this->_sql->update();
        $update->table($this->_productsDefinitionsTable);
        $update->set(array(
            'status' => 'ACTIVE',
            'makeLive' => '0000-00-00 00:00:00'
        ));
        $update->where("productDefinitionsId = '$productDefinitionsId'");
        $update->where("status = 'INACTIVE'");
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     *
     * @param
     *            $sqlRresult
     * @return array
     */
    protected function _dbResutToArray($sqlRresult)
    {
        $result = array();

        if ($sqlRresult->count() > 0) {
            $results = new ResultSet();
            $result = $results->initialize($sqlRresult)->toArray();
        }

        return $result;
    }

    /**
     * Get the product name based on product ID
     *
     * @param type $productId
     * @param type $tableName
     * @return type
     */
    public function getNameByProductId($productId = 0, $tableName = '')
    {
        if ($productId > 0 && trim($tableName) != '') {
            $productIdfrom = $tableName;
            $this->_sql->setTable($this->_productsDefinitionsTable);
            $action = $this->_sql->select()
                ->columns(array(
                    'productName'
                ))
                ->where($this->_productsDefinitionsTable . '.productId = ' . $productId)
                ->where($this->_productsDefinitionsTable . ".productIdFrom = '{$tableName}'")
                ->where($this->_productsDefinitionsTable . '.status != "DELETED"');

            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $results = $statement->execute();
            $returnData = '';
            if ($statement->execute()->count() > 0) {
                foreach ($results as $key => $value) {
                    $isVariant = ($tableName != $this->_tableName ? true : false);
                    $returnData = $value['productName'];
                }
            }

            return $returnData;
        }
    }

    /**
     *
     * @param type $defaultCategoryProducts
     * @return type
     */
    public function getDefaultProductDetailsByProductIdAndProductIdFrom($defaultCategoryProducts)
    {
        // If there's any product having this category as it's default
        // Get the names to display to the user
        $defaultCategoryProductsArray = array();
        $isSetAsDefaultCategoryForAProduct = false;
        if (count($defaultCategoryProducts) > 0) {
            foreach ($defaultCategoryProducts as $key => $productIdArray) {
                if (isset($productIdArray['productId']) && isset($productIdArray['productIdFrom']) && $productIdArray['productId'] > 0 && trim($productIdArray['productIdFrom']) != '') {
                    $isSetAsDefaultCategoryForAProduct = true;
                    $defaultCategoryProductsArray[] = $this->getNameByProductId($productIdArray['productId'], $productIdArray['productIdFrom']);
                }
            }
            $defaultCategoryProductsArray = array_unique($defaultCategoryProductsArray);
        }

        return array(
            'isSetAsDefaultCategoryForAProduct' => $isSetAsDefaultCategoryForAProduct,
            'defaultCategoryProductsArray' => $defaultCategoryProductsArray
        );
    }

    /**
     *
     * @param
     *            $allOptions
     * @return array
     */
    public function getSupplierCurrenciesForOptions($allOptions)
    {
        $returnArray = array();
        foreach ($allOptions as $optionCode => $option) {
            $supplierPrices = $this->getSupplierPrices($option['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
            if (! empty($supplierPrices['currencyCode'])) {
                $returnArray[$optionCode] = $supplierPrices['currencyCode'];
            } else {
                $returnArray[$optionCode] = '';
            }
        }
        return $returnArray;
    }

    /**
     *
     * @param
     *            $allOptions
     * @return array
     */
    public function getSupplierPriceForOptions($allOptions)
    {
        $returnArray = array();
        foreach ($allOptions as $optionCode => $option) {
            $supplierPrices = $this->getSupplierPrices($option['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
            if (! empty($supplierPrices['price'])) {
                $returnArray[$optionCode] = $supplierPrices['price'];
            } else {
                $returnArray[$optionCode] = '';
            }
        }
        return $returnArray;
    }

    /**
     *
     * @param
     *            $submitedFormData
     * @param
     *            $style
     * @param string $productIdFrom
     */
    public function savePriceDataAgainst($submitedFormData, $style, $productIdFrom = 'PRODUCTS')
    {
        switch ($productIdFrom) {
            case 'PRODUCTS':
                $productId = $this->getProductIdFromStyle($style);
                if (isset($submitedFormData['values'])) {
                    $firstValue = key($submitedFormData['values']);
                    $this->insertPriceForOptionOrStyle($firstValue, $submitedFormData['values'][$firstValue], $productId, 'PRODUCTS');
                }
                break;
            case 'PRODUCTS_R_ATTRIBUTES':
                if (! empty($submitedFormData['values'])) {
                    foreach ($submitedFormData['values'] as $optionData => $value) {
                        $this->insertPriceForOptionOrStyle($optionData, $value, null, 'PRODUCTS_R_ATTRIBUTES');
                    }
                }
                break;
        }
    }

    /**
     *
     * @param
     *            $submitedFormData
     * @return array
     */
    public function getOptionsCode($submitedFormData)
    {
        $optionsArray = array();
        if (isset($submitedFormData['values'])) {
            foreach ($submitedFormData['values'] as $optionData => $value) {
                $values = explode('=', $optionData);
                if (isset($values[0])) {
                    $optionsArray[] = $values[0];
                }
            }
        }
        return $optionsArray;
    }

    /**
     *
     * @param
     *            $key
     * @param
     *            $value
     * @param
     *            $styleProductId
     * @param
     *            $productIdFrom
     */
    public function insertPriceForOptionOrStyle($key, $value, $styleProductId, $productIdFrom)
    {
        $keys = explode('=', $key);
        switch ($productIdFrom) {
            case 'PRODUCTS':
                $productOrVariantId = $styleProductId;
                $this->savePricesAgainstProducts($keys, $productOrVariantId, $productIdFrom, $value);
                break;
            case 'PRODUCTS_R_ATTRIBUTES':
                if (isset($keys[0])) {
                    $optionCode = $keys[0];
                    $variants = $this->getVariantsIdForOption($optionCode);
                    if (count($variants) == 0) {
                        $optionCode = str_replace(' ', '', $keys[0]);
                        $variants = $this->getVariantsIdForOption($optionCode);
                    }
                    foreach ($variants as $variant) {
                        if (isset($variant['productAttributeId'])) {
                            $productOrVariantId = $variant['productAttributeId'];
                            $this->savePricesAgainstProducts($keys, $productOrVariantId, $productIdFrom, $value);
                        }
                    }
                }
                break;
        }
    }

    /**
     *
     * @param
     *            $keys
     * @param
     *            $productOrVariantId
     * @param
     *            $productIdFrom
     * @param
     *            $value
     */
    private function savePricesAgainstProducts($keys, $productOrVariantId, $productIdFrom, $value)
    {
        $currencyId = $this->getCurrencyIdFromCode($keys[3]);
        $currencyCode = $keys[3];
        $territoryId = $this->getTerritoryId($keys[2]);
        $territoryCode = $keys[2];
        $isStyle = false;
        if ($productIdFrom == 'PRODUCTS') {
            $isStyle = true;
        }
        $optionCode = '';
        if (!$isStyle) {
            $optionCode = $this->getOptionCodeForVariantId($productOrVariantId);
        }
        $historicalEntriesCollection = new ArrayCollection();

        if (isset($keys[1]) && $productOrVariantId) {
            switch ($keys[1]) {
                case 'supplier-currency':
                    $supplierPrice = $this->getSupplierPrices($productOrVariantId, $productIdFrom);
                    if (! empty($supplierPrice['price'])) {
                        $pricesArray = array(
                            'supplier' => array(
                                'price' => $supplierPrice['price'],
                                'currency' => $value
                            )
                        );
                    } else {
                        $pricesArray = array(
                            'supplier' => array(
                                'price' => '0',
                                'currency' => $value
                            )
                        );
                    }
                    $this->updateSupplierPrices($productOrVariantId, $productIdFrom, $pricesArray);
                    break;
                case 'supplier-price':
                    $supplierPrice = $this->getSupplierPrices($productOrVariantId, $productIdFrom);
                    if (! empty($supplierPrice['price'])) {
                        $pricesArray = array(
                            'supplier' => array(
                                'price' => $value,
                                'currency' => $supplierPrice['currencyCode']
                            )
                        );
                    } else {
                        $pricesArray = array(
                            'supplier' => array(
                                'price' => $value,
                                'currency' => 'GBP'
                            )
                        );
                    }
                    $this->updateSupplierPrices($productOrVariantId, $productIdFrom, $pricesArray);
                    break;
                case 'costPrice':
                    $territoriesForCurrency = $this->getTerritoriesForCurrency($currencyId);
                    foreach ($territoriesForCurrency as $territoryId) {
                        $territoryData = $this->getTerritoryCodeById($territoryId);
                        $territoryCodeTmp = $territoryData['iso2Code'];

                        $productPrices = $this->getProductPricesBasedOnCurrencyAndTerritory($productOrVariantId, $productIdFrom, $currencyId, $territoryId);
                        if (! empty($productPrices)) {
                            /**
                             * Update price
                             */
                            $this->updatePriceEntry($productPrices['productPriceId'], 'costPrice', $value);
                            if ($productPrices['costPrice'] != number_format($value, 2)) {
                                if (($territoryCodeTmp == 'GB' && $currencyCode == 'GBP') ||
                                    ($territoryCodeTmp == 'US' && $currencyCode == 'USD') ||
                                    ($territoryCodeTmp == 'CA' && $currencyCode == 'CAD')
                                ) {
                                    $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                                        'productOrVariantId' => $productOrVariantId,
                                        'isStyle' => $isStyle,
                                        'commonAttribute' => null,
                                        'attributeName' => "landed-cost-{$currencyCode}",
                                        'value' => $value,
                                        'fromDate' => null,
                                        'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                        'optionCode' => $optionCode
                                    ]);
                                    $historicalEntriesCollection->add($collectionItem);
                                }
                            }

                        } else {
                            /**
                             * Insert Price
                             */
                            $newPriceEntry = array(
                                'productIdFrom' => $productIdFrom,
                                'productOrVariantId' => $productOrVariantId,
                                'regionId' => $territoryId,
                                'currencyId' => $currencyId,
                                'costPrice' => $value,
                                'nowPrice' => 0,
                                'wasPrice' => 0,
                                'price' => 0
                            );
                            $this->_sql->setTable($this->_productsPricesTable);
                            $action = $this->_sql->insert();
                            $action->values($newPriceEntry);
                            $statement = $this->_sql->prepareStatementForSqlObject($action);
                            $statement->execute();

                            if (($territoryCodeTmp == 'GB' && $currencyCode == 'GBP') ||
                                ($territoryCodeTmp == 'US' && $currencyCode == 'USD') ||
                                ($territoryCodeTmp == 'CA' && $currencyCode == 'CAD')
                            ) {
                                $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                                    'productOrVariantId' => $productOrVariantId,
                                    'isStyle' => $isStyle,
                                    'commonAttribute' => null,
                                    'attributeName' => "landed-cost-{$currencyCode}",
                                    'value' => $value,
                                    'fromDate' => null,
                                    'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                    'optionCode' => $optionCode
                                ]);
                                $historicalEntriesCollection->add($collectionItem);
                            }
                        }
                    }
                    break;
                case 'nowPrice':

                    if (! empty($territoryId) && ! empty($currencyId)) {
                        $productPrices = $this->getProductPricesBasedOnCurrencyAndTerritory($productOrVariantId, $productIdFrom, $currencyId, $territoryId);
                        if (! empty($productPrices)) {
                            /**
                             * Update price
                             */
                            $this->updatePriceEntry($productPrices['productPriceId'], 'nowPrice', $value);
                            if ($productPrices['nowPrice'] != number_format($value, 2)) {
                                $historicalEntry = $this->historicalService->hydrateHistoricalEntity([
                                    'productOrVariantId' => $productOrVariantId,
                                    'isStyle' => $isStyle,
                                    'commonAttribute' => null,
                                    'attributeName' => "current-price-{$territoryCode}-{$currencyCode}",
                                    'value' => $value,
                                    'fromDate' => null,
                                    'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                    'optionCode' => $optionCode
                                ]);
                                $historicalEntriesCollection->add($historicalEntry);
                            }
                        } else {
                            /**
                             * Insert Price
                             */
                            $newPriceEntry = array(
                                'productIdFrom' => $productIdFrom,
                                'productOrVariantId' => $productOrVariantId,
                                'regionId' => $territoryId,
                                'currencyId' => $currencyId,
                                'costPrice' => 0,
                                'nowPrice' => $value,
                                'wasPrice' => 0,
                                'price' => 0
                            );
                            $this->_sql->setTable($this->_productsPricesTable);
                            $action = $this->_sql->insert();
                            $action->values($newPriceEntry);
                            $statement = $this->_sql->prepareStatementForSqlObject($action);
                            $statement->execute();
                            $historicalEntry = $this->historicalService->hydrateHistoricalEntity([
                                'productOrVariantId' => $productOrVariantId,
                                'isStyle' => $isStyle,
                                'commonAttribute' => null,
                                'attributeName' => "current-price-{$territoryCode}-{$currencyCode}",
                                'value' => $value,
                                'fromDate' => null,
                                'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                'optionCode' => $optionCode
                            ]);
                            $historicalEntriesCollection->add($historicalEntry);
                        }
                    }
                    /**
                     * If changes are done at GBP-GB 1-1 we need to cascade to
                     * ROW-GBP 2-1 spectrum will use this price for countries in tax region RW
                     * EU-GBP 3-1 spectrum will use this price for countries in tax region DK, GB, RW, SE
                     */
                    if ($territoryId == 1 && $currencyId == 1) {
                        $this->updateRelatedTerritoryCurrency($productOrVariantId, $productIdFrom, 1, 2, $value);
                        $this->updateRelatedTerritoryCurrency($productOrVariantId, $productIdFrom, 1, 3, $value);
                    }

                    /**
                     * If changes are done at USD-US 5-2 we need to cascade to
                     * GB-USD 1-2 spectrum will use this price for countries in tax region
                     */

                    if ($territoryId == 5 && $currencyId == 2) {
                        $this->updateRelatedTerritoryCurrency($productOrVariantId, $productIdFrom, 2, 1, $value);
                    }
                    break;
                case 'wasPrice':

                    if (! empty($territoryId) && ! empty($currencyId)) {
                        $productPrices = $this->getProductPricesBasedOnCurrencyAndTerritory($productOrVariantId, $productIdFrom, $currencyId, $territoryId);
                        if (! empty($productPrices)) {
                            /**
                             * Update price
                             */
                            $this->updatePriceEntry($productPrices['productPriceId'], 'wasPrice', $value);

                            if ($productPrices['wasPrice'] != number_format($value, 2)) {
                                $historicalEntry = $this->historicalService->hydrateHistoricalEntity([
                                    'productOrVariantId' => $productOrVariantId,
                                    'isStyle' => $isStyle,
                                    'commonAttribute' => null,
                                    'attributeName' => "original-price-{$territoryCode}-{$currencyCode}",
                                    'value' => $value,
                                    'fromDate' => null,
                                    'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                    'optionCode' => $optionCode
                                ]);
                                $historicalEntriesCollection->add($historicalEntry);
                            }

                        } else {
                            /**
                             * Insert Price
                             */
                            $newPriceEntry = array(
                                'productIdFrom' => $productIdFrom,
                                'productOrVariantId' => $productOrVariantId,
                                'regionId' => $territoryId,
                                'currencyId' => $currencyId,
                                'costPrice' => 0,
                                'nowPrice' => 0,
                                'wasPrice' => $value,
                                'price' => 0
                            );
                            $this->_sql->setTable($this->_productsPricesTable);
                            $action = $this->_sql->insert();
                            $action->values($newPriceEntry);
                            $statement = $this->_sql->prepareStatementForSqlObject($action);
                            $statement->execute();

                            $historicalEntry = $this->historicalService->hydrateHistoricalEntity([
                                'productOrVariantId' => $productOrVariantId,
                                'isStyle' => $isStyle,
                                'commonAttribute' => null,
                                'attributeName' => "original-price-{$territoryCode}-{$currencyCode}",
                                'value' => $value,
                                'fromDate' => null,
                                'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                'optionCode' => $optionCode
                            ]);
                            $historicalEntriesCollection->add($historicalEntry);
                        }
                    }
                    if ($territoryId == 1 && $currencyId == 1) {
                        $this->updateRelatedTerritoryCurrency($productOrVariantId, $productIdFrom, 1, 2, $value);
                        $this->updateRelatedTerritoryCurrency($productOrVariantId, $productIdFrom, 1, 3, $value);
                    }

                    /**
                     * If changes are done at USD-US 5-2 we need to cascade to
                     * GB-USD 1-2 spectrum will use this price for countries in tax region
                     */

                    if ($territoryId == 5 && $currencyId == 2) {
                        $this->updateRelatedTerritoryCurrency($productOrVariantId, $productIdFrom, 2, 1, $value);
                    }
                    break;
            }
        }
        $this->historicalService->recordChanges($historicalEntriesCollection);
    }

    /**
     * @param int $productOrVariantId
     * @param string $productIdFrom
     * @param int $currencyId
     * @param int $territoryId
     * @param float $value
     */
    protected function updateRelatedTerritoryCurrency($productOrVariantId, $productIdFrom, $currencyId, $territoryId, $value, $nowPrice = true)
    {
        if($nowPrice){
            $columnToUpdate = 'nowPrice';
            $newPriceEntry = array(
                'productIdFrom' => $productIdFrom,
                'productOrVariantId' => $productOrVariantId,
                'regionId' => $territoryId,
                'currencyId' => $currencyId,
                'costPrice' => 0,
                'nowPrice' => $value,
                'wasPrice' => 0,
                'price' => 0
            );
        }else{
            $columnToUpdate = 'wasPrice';
            $newPriceEntry = array(
                'productIdFrom' => $productIdFrom,
                'productOrVariantId' => $productOrVariantId,
                'regionId' => $territoryId,
                'currencyId' => $currencyId,
                'costPrice' => 0,
                'nowPrice' => 0,
                'wasPrice' => $value,
                'price' => 0
            );
        }
        $productPrices = $this->getProductPricesBasedOnCurrencyAndTerritory($productOrVariantId, $productIdFrom, $currencyId, $territoryId);
        if (! empty($productPrices)) {
            /**
             * Update price
             */
            $this->updatePriceEntry($productPrices['productPriceId'], $columnToUpdate, $value);
        } else {
            /**
             * Insert Price
             */
            $this->_sql->setTable($this->_productsPricesTable);
            $action = $this->_sql->insert();
            $action->values($newPriceEntry);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        }
    }

    /**
     *
     * @param
     *            $productOrVariantId
     * @param
     *            $productIdFrom
     * @param
     *            $currencyId
     * @param
     *            $territoryId
     * @return array|bool
     */
    public function getProductPricesBasedOnCurrencyAndTerritory($productOrVariantId, $productIdFrom, $currencyId, $territoryId)
    {
        $this->_sql->setTable($this->_productsPricesTable);
        $select = $this->_sql->select()->where(array(
            'productIdFrom' => $productIdFrom,
            'productOrVariantId' => $productOrVariantId,
            'regionId' => $territoryId,
            'currencyId' => $currencyId,
            'status' => 'ACTIVE'
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        return $statement->execute()->next();
    }

    /**
     *
     * @param
     *            $allOptions
     * @param
     *            $priceType
     * @param
     *            $territory
     * @param
     *            $currency
     * @return array
     */
    public function getPriceForAllOptions($allOptions, $priceType, $territory, $currency)
    {
        $returnArray = array();
        foreach ($allOptions as $optionCode => $optionData) {
            if ($territory) {
                $territoryId = $this->getTerritoryId($territory);
            }
            if ($currency) {
                $currencyId = $this->getCurrencyIdFromCode($currency);
            }

            $this->_sql->setTable($this->_productsPricesTable);
            $select = $this->_sql->select()
                ->columns(array(
                    $priceType
                ))
                ->where(array(
                    'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                    'productOrVariantId' => $optionData['productAttributeId']
                ));
            if (isset($territoryId)) {
                $select->where(array(
                    'regionId' => $territoryId
                ));
            }
            if (isset($currencyId)) {
                $select->where(array(
                    'currencyId' => $currencyId
                ));
            }

            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute()->next();
            if (isset($results[$priceType])) {
                $returnArray[$optionCode] = $results[$priceType];
            } else {
                $returnArray[$optionCode] = '';
            }
        }

        return $returnArray;
    }

    /**
     *
     * @return array|mixed
     */
    public function getSuppliersCurrency($isShortList = false)
    {
        $sql = "SELECT * FROM ( SELECT * FROM `CURRENCY_Codes` ORDER BY priority ASC) t1 GROUP BY alphabeticCode ORDER BY priority, currencyName ASC;";

        if ($isShortList === true) {
            $sql = "SELECT *
                    FROM
                        (SELECT *
                        FROM
                        `CURRENCY_Codes`
                        ORDER BY priority ASC) t1
                    WHERE priority <= '10'
                    GROUP BY alphabeticCode
                    ORDER BY priority , currencyName ASC;";
        }

        $statement = $this->_dbAdapter->query($sql); // $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();

        $finalArray = array();
        foreach ($results as $result) {
            $finalArray[$result['alphabeticCode']] = $result['alphabeticCode'] . ' - ' . $result['currencyName'];
        }

        return $finalArray;

        $currencyList = $this->_getResultData($result);
        return $currencyList;
    }

    /**
     * Insert or update the proudct supplier price row
     * @TODO PLEASE NOTE currntly working only on proudct level and later need to be changed to work with variants
     *
     * @param int $productId
     * @param float $productSupplierPrice
     * @param string $productSupplierCurrency
     */
    public function saveProductSupplierPrice($productId, $productSupplierPrice, $productSupplierCurrency, $productIdFrom = 'PRODUCTS')
    {
        if(!$productSupplierCurrency){
            return false;
        }
        
        $today = date('Y-m-d H:i:s');

        $isProcductSupplierPriceRecord = $this->_isProcductSupplierPriceRecord($productId);

        if (! $isProcductSupplierPriceRecord) {
            $sql = "INSERT INTO PRODUCTS_Supplier_Price (productIdFrom, productOrVariantId, currencyCode, price, created) VALUES ('$productIdFrom', '$productId', '$productSupplierCurrency','$productSupplierPrice', '$today');";
        } else {
            $sql = "UPDATE PRODUCTS_Supplier_Price SET currencyCode = '$productSupplierCurrency', price = '$productSupplierPrice' WHERE productOrVariantId = '$productId' AND productIdFrom = '$productIdFrom';";
        }

        $statement = $this->_dbAdapter->query($sql); // $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        return true;
    }

    /**
     * Update the proudct supplier price row on option level used by bluk price upload
     * @TODO PLEASE NOTE currntly working only on proudct level and later need to be changed to work with variants
     *
     * @param int $productId
     * @param float $productSupplierPrice
     * @param string $productSupplierCurrency
     */
    public function upateProductSupplierPriceonOptionLevel($productId, $productSupplierPrice, $productSupplierCurrency)
    {
        $sql = "UPDATE PRODUCTS_Supplier_Price
                SET currencyCode = '$productSupplierCurrency',
                price = '$productSupplierPrice'
                WHERE productOrVariantId = '$productId' AND
                productIdFrom = 'PRODUCTS_R_ATTRIBUTES';";

        $statement = $this->_dbAdapter->query($sql);
        $result = $statement->execute();
        return;
    }

    /**
     * Check if record exist
     *
     * @param unknown $productId
     * @return boolean
     */
    protected function _isProcductSupplierPriceRecord($productId)
    {
        $isRecord = false;
        $sql = "SELECT * FROM PRODUCTS_Supplier_Price WHERE productOrVariantId = '$productId' limit 1;";

        $statement = $this->_dbAdapter->query($sql); // $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $array = $this->_getResultData($result, true);
        if (! empty($array)) {
            $isRecord = true;
        }
        return $isRecord;
    }

    /**
     *
     * @param
     *            $productId
     * @return mixed
     */
    public function getProductSupplierPriceRecord($data, $specialFlag = false)
    {
        if ($specialFlag) {
            $sql = "SELECT * FROM PRODUCTS_Supplier_Price WHERE 
                    productOrVariantId = '" . $data['productId'] . "' AND productIdFrom = '" . $data['productIdFrom'] . "' 
                    order by supplierPriceId DESC limit 1;";
        } else {
            $sql = "SELECT * FROM PRODUCTS_Supplier_Price WHERE productOrVariantId = '" . $data . "' 
            AND productIdFrom='PRODUCTS' order by supplierPriceId DESC limit 1;";
        }

        $statement = $this->_dbAdapter->query($sql); // $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $record = $this->_getResultData($result, true);
        return $record;
    }

    /**
     *
     * @param int $productId
     * @param $variants
     */
    public function resetVariantsMarketingCategories($productId, $variants)
    {
        for ($i = 0, $count = count($variants); $i < $count; $i ++) {
            $variantId = $variants[$i];
            $this->_deleteProductMarketingCategory($variantId, true);
        }

        $productMarketingIds = $this->_getProductActiveCategoriesByProductId($productId);

        for ($i = 0, $count = count($variants); $i < $count; $i ++) {
            $variantId = $variants[$i];
            for ($n = 0, $count2 = count($productMarketingIds); $n < $count2; $n ++) {
                $categoryId = $productMarketingIds[$n]['categoryId'];
                $isDefault = $productMarketingIds[$n]['isDefault'];
                $cateforyAdded = $this->_categoriesMapper->addProductToMarketingCategory($variantId, $categoryId, 'PRODUCTS_R_ATTRIBUTES', 1, $isDefault);
            }
        }

        // reindex the product for spectrum and ES
    }

    /**
     *
     * @param int $productId
     * @param bool $isVariant
     */
    protected function _deleteProductMarketingCategory($productId, $isVariant = false)
    {
        $productIdFrom = ($isVariant === true) ? $this->_productsVariantsRelationshipTable : $this->_tableName;
        $whereClause = array(
            'productId' => $productId,
            'productIdFrom' => $productIdFrom
        );
        $fieldsToUpdate = array(
            'status' => 'DELETED'
        );
        $table = $this->_productsCategoriesRelationshipTable;
        $productCategoryDeactivated = $this->_update($fieldsToUpdate, $table, $whereClause);
        return;
    }

    /**
     *
     * @param int $productId
     * @return mixed
     */
    protected function _getProductActiveCategoriesByProductId($productId)
    {
        $isRecord = false;
        $sql = "SELECT * from PRODUCTS_R_CATEGORIES where productId = '$productId' and productIdFrom = 'PRODUCTS' and Status = 'ACTIVE';";

        $statement = $this->_dbAdapter->query($sql); // $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $array = $this->_getResultData($result);

        return $array;
    }

    /**
     * Get all product ids
     *
     * @return type
     */
    public function getAllProductIds($from = null, $to = null)
    {
        $productIdArray = array();

        $this->_sql->setTable(array(
            'p' => $this->_tableName
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'productId'
        ));
        $select->where('productId > ' . $from);
        $select->where('productId <= ' . $to);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            $productIdArray[] = $value['productId'];
        }

        return $productIdArray;
    }

    /**
     *
     * @param array $productsSkusArray
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function activateParentProductByVariantSku($productsSkusArray)
    {
        $parentProductidList = $this->_getParentProductIdByVariantSku($productsSkusArray);
        // $productIdString = $this->_convertProductIdForMysqlIN($parentProductidList);
        $productIdString = implode(',', $parentProductidList);

        $sql = "UPDATE PRODUCTS SET `status` = 'ACTIVE' WHERE productId IN ($productIdString);";
        $statement = $this->_dbAdapter->query($sql);
        $result = $statement->execute();

        return $result;
    }

    /**
     *
     * @param array $productsSkusArray
     * @return mixed
     */
    protected function _getParentProductIdByVariantSku($productsSkusArray)
    {
        $result = array();
        $productsSkusString = $this->_convertSkusArrayForMysqlIN($productsSkusArray);

        // $sql = "SELECT productId FROM PRODUCTS_R_ATTRIBUTES WHERE $productsSkusString;";
        $sql = "SELECT productId FROM PRODUCTS_R_ATTRIBUTES WHERE $productsSkusString GROUP BY productId;";

        $statement = $this->_dbAdapter->query($sql);
        $result = $statement->execute();
        $array = $this->_getResultData($result, false, true);

        return $array;
    }

    /**
     *
     * @param array $productsSkusArray
     * @return string
     */
    protected function _convertSkusArrayForMysqlIN($productsSkusArray)
    {
        $string = '';
        $last = count($productsSkusArray) - 1;
        for ($i = 0, $count = count($productsSkusArray); $i < $count; $i ++) {
            if ($i == $last) {
                $string .= "sku LIKE '$productsSkusArray[$i]%'";
            } else {
                $string .= "sku LIKE '$productsSkusArray[$i]%' OR ";
            }
        }

        return $string;
    }

    /**
     *
     * @param
     *            $parentProductidList
     * @return string
     */
    protected function _convertProductIdForMysqlIN($parentProductidList)
    {
        $string = '';
        $last = count($parentProductidList) - 1;
        for ($i = 0, $count = count($parentProductidList); $i < $count; $i ++) {
            $productId = $parentProductidList[$i]['productId'];
            if ($i == $last) {
                $string .= "'$productId'";
            } else {
                $string .= "'$productId', ";
            }
        }

        return $string;
    }

    /**
     *
     * @param
     *            $variantIds
     * @return bool
     */
    public function checkIfVariantsHaveSameDetails($variantIds)
    {
        $existingDetailsArray = false;
        $error = false;
        foreach ($variantIds as $variantId) {
            $details = $this->getVariantDetails($variantId, $this->_currentLanguage);

            if (empty($existingDetailsArray)) {
                $existingDetailsArray = $details;
            } else {
                if ($existingDetailsArray != $details) {
                    $error = true;
                }
            }
        }
        return $error;
    }

    /**
     *
     * @param
     *            $data
     */
    public function updateVariantsBasicDetails($data)
    {
        foreach ($data['variantIds'] as $variant) {
            $updateData = array(
                'shortProductName' => $data['shortProductName'],
                'productName' => $data['productName'],
                'shortDescription' => $data['shortDescription'],
                'longDescription' => $data['longDescription'],
                'metaTitle' => $data['metaTitle'],
                'metaDescription' => $data['metaDescription'],
                'metaKeyword' => $data['metaKeyword']
            );

            $whereClause = array(
                'productId' => $variant,
                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES'
            );
            $whereNoEqualTo = array();
            $this->_update($updateData, $this->_productsDefinitionsTable, $whereClause, $whereNoEqualTo);
        }
    }

    /**
     *
     * @return string
     */
    public function getAllVariantsSelect()
    {
        $this->_sql->setTable($this->_productsVariantsRelationshipTable);
        $select = $this->_sql->select()->where('status != "DELETED"');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $returnArray = '<select name="all-sku-variants" id="all-sku-variants">';
        foreach ($results as $result) {
            $returnArray .= '<option value="' . $result['productAttributeId'] . '">' . $result['sku'] . '</option>';
        }
        $returnArray .= '</select';
        return $returnArray;
    }

    /**
     *
     * @param
     *            $productId
     * @param
     *            $targetVariantId
     * @param
     *            $variants
     * @param
     *            $oldProductId
     * @return array
     */
    public function transferImagesToVariant($productId, $targetVariantId, $variants, $oldProductId)
    {
        // Get folder and date for the base sku
        $mediaFolderIdBase = $this->_productFolderMapper->getMediaFolderIdByProductAndVariant($productId, $targetVariantId);
        $imagesBase = $this->_productFolderMapper->getProductImagesFromMediaFolderId($mediaFolderIdBase);
        $pmsFolderBase = $this->_productFolderMapper->getProductFolderByProductAndVariant($productId, $targetVariantId);
        $errors = array();

        foreach ($variants as $variant) {
            // Get target from pms
            try {
                $this->_dbAdapter->getDriver()
                    ->getConnection()
                    ->beginTransaction();
                $oldFolderId = $this->_productFolderMapper->getProductFolderByProductAndVariant($oldProductId, $variant);
                $oldMediaId = $this->_productFolderMapper->getMediaFolderIdByProductAndVariant($oldProductId, $variant);
                $oldImages = $this->_productFolderMapper->getProductImagesFromMediaFolderId($oldMediaId);

                $this->_productFolderMapper->setProductSpecialImage('default', $oldFolderId['id'], $pmsFolderBase['defaultImageId']);
                $this->_productFolderMapper->setProductSpecialImage('basket', $oldFolderId['id'], $pmsFolderBase['basketImageId']);
                $this->_productFolderMapper->setProductSpecialImage('menu', $oldFolderId['id'], $pmsFolderBase['menuImageId']);
                $this->_productFolderMapper->setProductSpecialImage('hover', $oldFolderId['id'], $pmsFolderBase['hoverImageId']);
                $this->_productFolderMapper->setProductSpecialImage('overlay', $oldFolderId['id'], $pmsFolderBase['overlayImageId']);
                // Remove old product images
                foreach ($oldImages as $oldImage) {
                    $this->_productFolderMapper->removeImageFromProductFolder($oldImage->ref, $oldFolderId['id']);
                }
                // Add new product Images
                foreach ($imagesBase as $order => $newImage) {
                    $this->_productFolderMapper->addImageToProductFolderId($newImage->ref, $oldFolderId['id'], $order + 1);
                }
                $this->_dbAdapter->getDriver()
                    ->getConnection()
                    ->commit();
            } catch (\Exception $e) {
                // revert the changes done to the database and throw exception
                $this->_dbAdapter->getDriver()
                    ->getConnection()
                    ->rollback();
                $sku = $this->getVariantsSkus($variant, $this->_currentLanguage);
                $errors[] = ! empty($sku[0]) ? 'Sku ' . $sku[0] . ' was not updated because of an fatal error. Please retry or contact PRISM.' : 'Sku not found. Please contact PRISM.';
                continue;
            }
        }
        $status = true;
        if (! empty($errors)) {
            $status = false;
        }
        return array(
            'success' => $status,
            'error' => $errors
        );
    }

    /**
     *
     * @param
     *            $variants
     * @param
     *            $targetVariantId
     * @return array
     */
    public function transferBasicDetails($variants, $targetVariantId)
    {
        $errors = array();
        try {
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->beginTransaction();
            $data = $this->getVariantDetails($targetVariantId, $this->_currentLanguage);

            $data['variantIds'] = $variants;
            $this->updateVariantsBasicDetails($data);
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->commit();
        } catch (\Exception $e) {
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->rollback();
            $errors[] = 'The basic details could not be transferred. Please retry or contact PRISM.';
        }
        $status = true;
        if (! empty($errors)) {
            $status = false;
        }
        return array(
            'success' => $status,
            'error' => $errors
        );
    }

    /**
     *
     * @param
     *            $variants
     * @param
     *            $targetVariantId
     * @return array
     */
    public function transferPrices($variants, $targetVariantId)
    {
        $errors = array();
        try {
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->beginTransaction();
            $prices = $this->getProductPricesForTransfer($targetVariantId);
            $supplierPrice = $this->getSupplierPrices($targetVariantId);
            foreach ($variants as $variant) {
                foreach ($prices as $newPrice) {
                    /**
                     * Deactivate old Price
                     */
                    $this->_productsMapperPrices->_deactivateProductPrices($variant, $newPrice['currencyId'], $this->_productsVariantsRelationshipTable);
                    /**
                     * Add the new product Price
                     */
                    unset($newPrice['productPriceId']);
                    unset($newPrice['created']);
                    unset($newPrice['modified']);
                    $this->_sql->setTable($this->_productsPricesTable);
                    $action = $this->_sql->insert();
                    $newPrice['productOrVariantId'] = $variant;
                    $action->values($newPrice);
                    $statement = $this->_sql->prepareStatementForSqlObject($action);
                    $statement->execute();
                    /**
                     * Update supplier Price
                     */
                    $update = $this->_sql->update();
                    $update->table($this->_productSupplierPriceTable);
                    $update->set($supplierPrice);
                    $update->where(array(
                        'productOrVariantId' => $variant,
                        'productIdFrom' => "PRODUCTS_R_ATTRIBUTES"
                    ));
                    $statement = $this->_sql->prepareStatementForSqlObject($update);
                    $statement->execute();
                }
            }
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->commit();
        } catch (\Exception $ex) {
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->rollback();
            $errors[] = 'The prices could not be transferred. Please retry or contact PRISM.';
        }

        $status = true;
        if (! empty($errors)) {
            $status = false;
        }
        return array(
            'success' => $status,
            'error' => $errors
        );
    }

    /**
     *
     * @param
     *            $targetVariantId
     * @param bool|true $isVariant
     */
    protected function getProductSupplierPricesForTransfer($targetVariantId, $isVariant = true)
    {
        $this->_sql->setTable($this->_productSupplierPriceTable);
        $fromTable = $this->_tableName;
        if ($isVariant) {
            $fromTable = $this->_productsVariantsRelationshipTable;
        }

        $select = $this->_sql->select()
            ->where('productIdFrom = "' . $fromTable . '"')
            ->where('productOrVariantId = ' . $targetVariantId);
    }

    /**
     *
     * @param
     *            $productOrVariantId
     * @param bool $isVariant
     * @return array
     */
    protected function getProductPricesForTransfer($productOrVariantId, $isVariant = true)
    {
        $this->_sql->setTable($this->_productsPricesTable);
        $fromTable = $this->_tableName;
        if ($isVariant) {
            $fromTable = $this->_productsVariantsRelationshipTable;
        }
        $select = $this->_sql->select()
            ->where('productOrVariantId =' . $productOrVariantId)
            ->where('productIdFrom = "' . $fromTable . '"')
            ->where('status = "ACTIVE"');
        $statement = $this->_sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();
        $prices = array();
        foreach ($results as $result) {
            $prices[] = $result;
        }
        return $prices;
    }

    /**
     *
     * @param
     *            $search
     * @return array
     */
    public function searchRelatedProducts($search)
    {
        $this->_sql->setTable($this->_productsVariantsRelationshipTable);
        $select = $this->_sql->select()->join(array(
            'pd' => $this->_productsDefinitionsTable
        ), // Set join table and alias
            new Expression('pd.productId = ' . $this->_productsVariantsRelationshipTable . '.productAttributeId
                    AND pd.productIdFrom = "PRODUCTS_R_ATTRIBUTES"'), array(
                '*'
            ), Select::JOIN_LEFT);
        $select->where->nest->like('pd.shortProductName', '%' . $search . '%')->or->like('pd.productName', '%' . $search . '%')->or->like($this->_productsVariantsRelationshipTable . '.sku', '%' . $search . '%')->unnest;
        $select->where('pd.status != "DELETED"');
        $select->where($this->_productsVariantsRelationshipTable . '.status != "DELETED"');
        $select->limit(15);

        $statement = $this->_sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();

        if ($results->count() <= 15) {
            $reachedLimit = false;
        } else {
            $reachedLimit = true;
        }

        $products = array();
        foreach ($results as $result) {
            $tmpProduct = $result;
            $image = $this->getImagesForProduct($result['productAttributeId'], 1);
            $imageUrlBig = '';
            $imageUrlThumb = '';
            if (! empty($image['default_url'])) {
                $imageUrlBig = $image['default_url'];
                $imageUrlThumb = $image['default_url_medium'];
            }

            $tmpProduct['image_default'] = $imageUrlBig;
            $tmpProduct['image_thumb'] = $imageUrlThumb;
            $products[] = $tmpProduct;
        }

        return array(
            'reachedLimit' => $reachedLimit,
            'products' => $products
        );
    }

    /**
     *
     * @param
     *            $relatedVariants
     * @param
     *            $variantsIds
     * @return bool
     */
    public function saveRelatedVariants($relatedVariants, $variantsIds)
    {
        foreach ($variantsIds as $variantId) {
            $this->clearRelatedVariantsForVariantId($variantId);
            $this->addNewRelatedVariantsForVariantId($variantId, $relatedVariants);
        }
        return true;
    }

    /**
     *
     * @param
     *            $variantId
     * @param
     *            $relatedVariants
     */
    public function addNewRelatedVariantsForVariantId($variantId, $relatedVariants)
    {
        foreach ($relatedVariants as $relatedVariantId) {
            $data = array(
                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                'productId' => $variantId,
                'relatedProductId' => $relatedVariantId,
                'status' => 'ACTIVE'
            );
            $this->_sql->setTable($this->_productsRelatedProductsTable);
            $action = $this->_sql->insert();
            $action->values($data);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        }
    }

    /**
     *
     * @param
     *            $variantId
     */
    public function clearRelatedVariantsForVariantId($variantId)
    {
        $data = array(
            'status' => 'DELETED'
        );
        $update = $this->_sql->update();
        $update->table($this->_productsRelatedProductsTable);
        $update->set($data);
        $update->where(array(
            'productId' => $variantId
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     *
     * @param
     *            $selectedVariants
     * @return array
     */
    public function checkIfVariantsHaveSameRelatedVariants($selectedVariants)
    {
        $existingDetailsArray = false;
        $error = false;
        $details = array();
        foreach ($selectedVariants as $selectedVariantId) {
            $details = $this->getVariantRelatedVariants($selectedVariantId);

            if (empty($existingDetailsArray)) {
                $existingDetailsArray = $details;
            } else {
                if ($existingDetailsArray != $details) {
                    $error = true;
                }
            }
        }
        return array(
            'error' => $error,
            'relatedVariants' => $details
        );
    }

    /**
     *
     * @param
     *            $variantId
     * @return array
     */
    public function getVariantRelatedVariants($variantId)
    {
        $this->_sql->setTable($this->_productsRelatedProductsTable);
        $select = $this->_sql->select()
            ->columns(array(
                'relatedProductId'
            ))
            ->where('productIdFrom = "PRODUCTS_R_ATTRIBUTES"')
            ->where('relatedProductIdFrom = "PRODUCTS_R_ATTRIBUTES"')
            ->where('productId = ' . $variantId)
            ->where('status != "DELETED"');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $relatedProducts = array();
        foreach ($results as $result) {
            $relatedProducts[] = $result;
        }

        return $relatedProducts;
    }

    /**
     *
     * @param
     *            $relatedVariants
     * @return array
     */
    public function getVariantDetailsForRelated($relatedVariants)
    {
        $variantsArray = array();
        foreach ($relatedVariants as $variant) {
            $variantsArray[] = $variant['relatedProductId'];
        }
        $products = array();
        if (! empty($variantsArray) && is_array($variantsArray)) {
            $this->_sql->setTable($this->_productsVariantsRelationshipTable);
            $select = $this->_sql->select()
                ->join(array(
                    'pd' => $this->_productsDefinitionsTable
                ), // Set join table and alias
                    new Expression('pd.productId = ' . $this->_productsVariantsRelationshipTable . '.productAttributeId
                        AND pd.productIdFrom = "PRODUCTS_R_ATTRIBUTES"'), array(
                        '*'
                    ), Select::JOIN_LEFT)
                ->where($this->_productsVariantsRelationshipTable . '.productAttributeId IN (' . implode(',', $variantsArray) . ')');
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            foreach ($results as $result) {
                $tmpProduct = $result;
                $image = $this->getImagesForProduct($result['productAttributeId'], 1);
                $imageUrlBig = '';
                $imageUrlThumb = '';
                if (! empty($image['default_url'])) {
                    $imageUrlBig = $image['default_url'];
                    $imageUrlThumb = $image['default_url_medium'];
                }

                $tmpProduct['image_default'] = $imageUrlBig;
                $tmpProduct['image_thumb'] = $imageUrlThumb;
                $products[] = $tmpProduct;
            }
        }
        return $products;
    }

    /**
     *
     * @param
     *            $productsArray
     * @param
     *            $relatedProducts
     * @return mixed
     */
    public function excludeAlreadySavedProductsFromSearchDisplay($productsArray, $relatedProducts)
    {
        foreach ($productsArray['products'] as $productKey => $products) {
            foreach ($relatedProducts as $relatedKey => $relatedProduct) {
                if ($products['productAttributeId'] == $relatedProduct['productAttributeId']) {
                    unset($productsArray['products'][$productKey]);
                }
            }
        }

        return $productsArray;
    }

    /**
     *
     * @return int
     */
    public function getProductsCount()
    {
        $this->_sql->setTable($this->_tableName);
        $select = $this->_sql->select();
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->count();
    }

    /**
     *
     * @param
     *            $productId
     * @param
     *            $variantNumber
     * @return int
     */
    public function createCollationVariantsForProductId($productId, $variantNumber)
    {
        $response = $this->_productsMapperVariants->saveVariantsForCollation($productId, $variantNumber);
        return $response;
    }

    /**
     *
     * @param
     *            $variantId
     * @return bool
     */
    public function variantIsSavedInSpectrum($variantId)
    {
        $this->_sql->setTable($this->_productsSpectrumTable);
        $select = $this->_sql->select()
            ->where('productId = ' . $variantId)
            ->where('productIdFrom = "PRODUCTS_R_ATTRIBUTES"');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();

        if (! empty($result['spectrumId'])) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param int $variantId
     * @param string $sku
     */
    public function saveSkuForVariant($variantId, $sku)
    {
        $fieldsToUpdate = array(
            'sku' => strtoupper($sku)
        );
        $whereClause = array(
            'productAttributeId' => $variantId
        );
        $this->_update($fieldsToUpdate, $this->_productsVariantsRelationshipTable, $whereClause, array());
    }

    /**
     *
     * @param string $sku
     * @return bool
     */
    public function checkIfSkuIsUnique($sku)
    {
        $this->_sql->setTable($this->_productsVariantsRelationshipTable);
        $selectVariants = $this->_sql->select()
            ->columns(array(
                'sku'
            ))
            ->where("sku = '$sku'");

        $this->_sql->setTable($this->_tableName);
        $selectProducts = $this->_sql->select()
            ->columns(array(
                'sku'
            ))
            ->where("sku = '$sku'");

        $selectProducts->combine($selectVariants);
        $statement = $this->_sql->prepareStatementForSqlObject($selectProducts);
        $results = $statement->execute();

        if ($results->count() >= 1) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param
     *            $productId
     * @param
     *            $tabId
     * @param string $productIdFrom
     */
    public function deleteProductTab($productId, $tabId, $productIdFrom = 'PRODUCTS')
    {
        $update = $this->_sql->update();
        $update->table($this->_productsTabsTable);
        $update->set(array(
            'status' => 'DELETED'
        ));
        $update->where(array(
            'productId' => $productId,
            'productIdFrom' => $productIdFrom,
            'productTabId' => $tabId
        ));

        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     *
     * @param int $productId
     * @param int $tabId
     */
    public function getProductTab($productId, $tabId)
    {
        $this->_sql->setTable($this->_productsTabsTable);

        $select = $this->_sql->select()
            ->where('productId = ' . $productId)
            ->where('productTabId = ' . $tabId);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        return $statement->execute()->next();
    }

    /**
     *
     * @param string $status
     * @param int $tabId
     */
    public function updateProductTabStatus($status, $tabId)
    {
        $update = $this->_sql->update();
        $update->table($this->_productsTabsTable);
        $update->set(array(
            'status' => $status
        ));
        $update->where(array(
            'productTabId' => $tabId
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     *
     * @param int $productId
     * @param int $currentCurrencyId
     * @return mixed
     */
    public function getLowestVariantPriceForProduct($productId, $currentCurrencyId)
    {
        $variants = $this->getVariants($productId);
        $priceArray = $this->buildPricesArray($variants, $currentCurrencyId);
        asort($priceArray);
        reset($priceArray);
        $firstKey = key($priceArray);
        return ($priceArray[$firstKey]);
    }

    /**
     *
     * @param int $productId
     * @param int $currentCurrencyId
     * @return mixed
     */
    public function getHighestVariantPriceForProduct($productId, $currentCurrencyId)
    {
        $variants = $this->getVariants($productId);
        $priceArray = $this->buildPricesArray($variants, $currentCurrencyId);
        asort($priceArray);
        end($priceArray);
        $firstKey = key($priceArray);
        return ($priceArray[$firstKey]);
    }

    /**
     *
     * @param array $variants
     * @param int $currencyId
     * @return array
     */
    private function buildPricesArray($variants, $currencyId)
    {
        $pricesArray = array();
        if (! empty($variants['data'])) {
            foreach ($variants['data'] as $variant) {
                if ($variant['status'] == "ACTIVE") {
                    $prices = $this->getProductPrices($variant['productAttributeId'], $currencyId, 1);
                    $pricesArray[] = ! empty($prices[key($prices)]['nowPrice']) ? $prices[key($prices)]['nowPrice'] : '0.00';
                }
            }
        }
        return $pricesArray;
    }

    /**
     *
     * @param int $commonAttributeId
     * @return bool
     */
    public function commonAttributeIsUsedInProducts($commonAttributeId)
    {
        $this->_sql->setTable($this->productsCommonAttributesTable);
        $select = $this->_sql->select()->where('commonAttributeId = ' . $commonAttributeId->getCommonAttributeId());
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() > 0) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param int $commonAttributeValueId
     * @return bool
     */
    public function commonAttributeValueIsUsedInProducts($commonAttributeValueId)
    {
        $this->_sql->setTable($this->productsCommonAttributesTable);
        $select = $this->_sql->select()->where('commonAttributeValue = ' . $commonAttributeValueId);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() > 0) {
            return true;
        }

        return false;
    }

    /**
     *
     * @param array $formData
     * @return bool
     */
    public function createNewStyle($formData)
    {
        $inputArray = $this->formatSubmitedArray($formData);
        $ean13 = $this->getNextAvailableBarcodeForProduct();

        /** Check if custom barcodes option is enabled and let the product be created without any valid barcodes */
        $customBarcodes = isset($this->clientConfig['settings']['customBarcodes']) ? $this->clientConfig['settings']['customBarcodes'] : false;
        if ($customBarcodes && empty($ean13)) {
            $ean13 = '';
        }
        if(empty($ean13) && !$customBarcodes){
            return  false;
        }

        try {
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->beginTransaction();
            /**
             * Create PRODUCTS entry *
             */
            /**
             * Create insert array
             */
            $productsTableData = array(
                'manufacturerId' => null,
                'productTypeId' => 1,
                'productMerchantCategoryId' => null,
                'taxId' => 0,
                'ean13' => $ean13,
                'ecoTax' => '0.00',
                'style' => strtoupper($inputArray['isTableField']['PRODUCTS']['style']['value']),
                'supplierReference' => '',
                'weight' => '',
                'onSale' => null,
                'outOfStock' => null,
                'allowBackOrder' => 0,
                'customizable' => 1,
                'status' => 'ACTIVE',
                'created' => date('Y-m-d H:i:s')
            );
            /**
             * Insert data into PRODUCTS Table
             */
            $this->_sql->setTable($this->_tableName);
            $action = $this->_sql->insert();
            $action->values($productsTableData);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
            /**
             * Retrieve last generated product id
             */
            $productId = $this->_dbAdapter->getDriver()
                ->getConnection()
                ->getLastGeneratedValue();

            $this->assignCoreAttributeToProduct('Enabled', 1, $productId, 'PRODUCTS');
            /**
             * Link Common Attributes to Style
             */
            $historicalEntries = $this->createProductsLinksToCommonAttributes($inputArray['isAttribute'], 'PRODUCTS', $productId);

            /**
             * Add prices for style
             */
            /**
             * Commented it out as they did not want this.
             * Conversation between Henry and Allison 22.03.2016 15:06
             */
            // $this->createStandardPricesForProductOrVariant($productId, 'PRODUCTS');

            $this->createProductsLinksToChannels($productId);

            /**
             * Build array for Product Definitions
             */
            $productsDefinitionsTableData = array(
                'productId' => $productId,
                'productIdFrom' => "PRODUCTS",
                'languageId' => 1,
                'versionId' => 1,
                'versionDescription' => 'default',
                'shortProductName' => $inputArray['isTableField']['PRODUCTS_Definitions']['shortProductName']['value'],
                'productName' => $inputArray['isTableField']['PRODUCTS_Definitions']['productName']['value'],
                'urlName' => '',
                'metaTitle' => '',
                'metaDescription' => '',
                'metaKeyword' => '',
                'shortDescription' => '',
                'longDescription' => '',
                'misSpells' => '',
                'tags' => '',
                'status' => 'ACTIVE',
                'makeLive' => '',
                'created' => date('Y-m-d H:i:s')
            );
            /**
             * Insert data into PRODUCTS Definitions Table
             */
            $this->_sql->setTable($this->_productsDefinitionsTable);
            $action = $this->_sql->insert();
            $action->values($productsDefinitionsTableData);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();

            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->commit();
            $this->historicalService->recordChanges($historicalEntries);
            return $inputArray['isTableField']['PRODUCTS']['style']['value'];
        } catch (\Exception $ex) {

            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->rollback();
            return true;
        }
    }

    /**
     *
     * @param int $productOrVariantId
     * @param int $productIdFrom
     * @return bool
     */
    public function createStandardPricesForProductOrVariant($productOrVariantId, $productIdFrom)
    {
        try {
            $standardPrice = $this->clientConfig['settings']['standardStylePrice'];
            $this->_sql->setTable($this->territoriesRCurrenciesTable);
            $select = $this->_sql->select();
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            $pricesArray = array(
                'price' => $standardPrice,
                'offerPrice' => $standardPrice,
                'nowPrice' => $standardPrice,
                'wasPrice' => $standardPrice,
                'costPrice' => $standardPrice,
                'status' => 'ACTIVE'
            );
            foreach ($results as $result) {
                $this->createStandardPriceForProductOrVariant($productOrVariantId, $productIdFrom, $result['currencyId'], $result['territoryId'], $pricesArray);
            }
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     *
     * @param $coreAttributeName
     * @param $coreAttributeValue
     * @param $productOrVariantId
     * @param $productIdFrom
     * @return bool
     */
    public function assignCoreAttributeToProduct($coreAttributeName, $coreAttributeValue, $productOrVariantId, $productIdFrom)
    {
        try {
            $coreAttribute = $this->_attributesGroupMapper->fetchAllCoreAttributes($coreAttributeName);
            if (! empty($coreAttribute) && is_array($coreAttribute[key($coreAttribute)])) {
                $coreAttribute = $coreAttribute[key($coreAttribute)];

                if ($productIdFrom == 'PRODUCTS') {
                    $isVariant = 0;
                } else {
                    $isVariant = 1;
                }
                /**
                 * Build array to insert data
                 */
                $productsAttributesCombinationTableData = array(
                    'attributeGroupId' => $coreAttribute['attributeGroupId'],
                    'productOrVariantId' => $productOrVariantId,
                    'attributeValue' => $coreAttribute['viewAs'] == 'TEXT' ? '' : $coreAttributeValue,
                    'isVariant' => $isVariant,
                    'groupType' => $coreAttribute['viewAs'],
                    'status' => 'ACTIVE'
                );
                /**
                 * Insert data *
                 */
                $this->_sql->setTable($this->_productsAttributesCombinationTable);
                $action = $this->_sql->insert();
                $action->values($productsAttributesCombinationTableData);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
                /**
                 * Retrieve generated id after insert
                 */
                $productAttributeCombinationId = $this->_dbAdapter->getDriver()
                    ->getConnection()
                    ->getLastGeneratedValue();
                /**
                 * If the attribute is a TEXT attribute value needs to be inserted into other table
                 */
                if ($coreAttribute['viewAs'] == 'TEXT') {
                    $productsAttributesCombinationDefinitionData = array(
                        'productAttributeCombinationId' => $productAttributeCombinationId,
                        'attributeValue' => $coreAttributeValue,
                        'languageId' => $this->_defaultLanguage,
                        'status' => 'ACTIVE'
                    );
                    $this->_sql->setTable($this->_productsAttributesCombinationDefinitions);
                    $action = $this->_sql->insert();
                    $action->values($productsAttributesCombinationDefinitionData);
                    $statement = $this->_sql->prepareStatementForSqlObject($action);
                    $statement->execute();
                }
                return true;
            }
        } catch (\Exception $ex) {
            return false;
        }
        return false;
    }

    /**
     *
     * @param $partialStyleCode
     * @return bool|string
     */
    public function retrieveNextAvailableStyle($partialStyleCode)
    {
        try {
            $query = "SELECT `PRODUCTS`.*, CAST(REPLACE(style, '$partialStyleCode', '') as SIGNED) as replacedStyle
                        FROM `PRODUCTS`
                        WHERE style LIKE '$partialStyleCode%'
                        ORDER BY `replacedStyle` DESC";
            $result = $this->_dbAdapter->query($query)->execute();

            if ($result->count() > 0) {
                /**
                 * They are already styles built we need to increment the number
                 */
                $lastStyleUsed = $result->next();
                $nrToIncrement = str_replace($partialStyleCode, '', $lastStyleUsed['style']);
                $newIncrementedValue = sprintf("%03d", $nrToIncrement + 1);
                return strtoupper($partialStyleCode . $newIncrementedValue);
            } else {
                /**
                 * No styles are built using the current partial code.
                 */
                return $partialStyleCode . '001';
            }
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     *
     * @param int $productId
     */
    private function createProductsLinksToChannels($productId)
    {
        for ($i = 1; $i <= 3; $i ++) {
            $insertData = array(
                'productId' => $productId,
                'channelId' => $i,
                'status' => 'ACTIVE'
            );
            $this->_sql->setTable($this->_productsChannelsTable);
            $action = $this->_sql->insert();
            $action->values($insertData);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        }
    }

    /**
     * @param array $inputArray
     * @param $productIdFrom
     * @param $productId
     * @return ArrayCollection
     */
    public function createProductsLinksToCommonAttributes($inputArray = array(), $productIdFrom, $productId)
    {
        $historicalCollection = new ArrayCollection();

        if (! empty($inputArray)) {
            foreach ($inputArray as $inputField) {

                switch ($inputField['entity']->getInputFieldRAttributesInputFieldRAttributeId()->getAttributeTable()) {
                    case 'COMMON_ATTRIBUTES':
                        /**
                         * Retrieve common attribute
                         */
                        $commonAttributeId = $inputField['entity']->getInputFieldRAttributesInputFieldRAttributeId()
                            ->getCommonAttributes()
                            ->getCommonAttributeId();
                        /** @var \PrismProductsManager\Entity\CommonAttributes $commonAttribute */
                        $commonAttribute = $inputField['entity']->getInputFieldRAttributesInputFieldRAttributeId()
                            ->getCommonAttributes();

                        $insertData = array(
                            'productOrVariantId' => $productId,
                            'productIdFrom' => $productIdFrom,
                            'commonAttributeId' => $commonAttributeId,
                            'commonAttributeValue' => $inputField['value']
                        );
                        $this->_sql->setTable($this->productsCommonAttributesTable);
                        $action = $this->_sql->insert();
                        $action->values($insertData);
                        $statement = $this->_sql->prepareStatementForSqlObject($action);
                        $statement->execute();

                        if ($commonAttribute->isKeepHistory()) {
                            $isStyle = null;
                            switch ($productIdFrom) {
                                case 'PRODUCTS':
                                    $isStyle = true;
                                    break;
                                case 'PRODUCTS_R_ATTRIBUTES':
                                    $isStyle = false;
                                    break;
                            }
                            $optionCode = '';
                            if (!$isStyle) {
                                $optionCode = $this->getOptionCodeForVariantId($productId);
                            }
                            $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                'productOrVariantId' => $productId,
                                'isStyle' => $isStyle,
                                'commonAttribute' => $commonAttribute,
                                'attributeName' => '',
                                'value' => $inputField['value'],
                                'fromDate' => null,
                                'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                'optionCode' => $optionCode
                            ]);
                            $historicalCollection->add($historyEntity);
                        }


                        break;
                    case 'ATTRIBUTES_Groups':
                        /**
                         * Insert Attributes Relationship
                         */
                        $attributeGroupId = $inputField['entity']->getInputFieldRAttributesInputFieldRAttributeId()
                            ->getAttributeGroupId()
                            ->getAttributeGroupId();
                        $attributeGroupDetails = $this->_attributesGroupMapper->fetchAllAttributeGroups($this->_currentLanguage, $attributeGroupId);

                        if ($productIdFrom == 'PRODUCTS') {
                            $isVariant = 0;
                        } else {
                            $isVariant = 1;
                        }

                        if ($attributeGroupDetails['viewAs'] == "TEXT") {
                            $productsAttributesCombinationData = array(
                                'attributeGroupId' => $attributeGroupId,
                                'productOrVariantId' => $productId,
                                'attributeValue' => '',
                                'isVariant' => $isVariant,
                                'groupType' => $attributeGroupDetails['viewAs'],
                                'status' => 'ACTIVE'
                            );
                            $this->_sql->setTable($this->_productsAttributesCombinationTable);
                            $action = $this->_sql->insert();
                            $action->values($productsAttributesCombinationData);
                            $statement = $this->_sql->prepareStatementForSqlObject($action);
                            $statement->execute();
                            /**
                             * Retrieve last generated product id
                             */
                            $productsAttributesCombinationId = $this->_dbAdapter->getDriver()
                                ->getConnection()
                                ->getLastGeneratedValue();

                            $productsAttributesCombinationDefinitionsArray = array(
                                'productAttributeCombinationId' => $productsAttributesCombinationId,
                                'attributeValue' => $inputField['value'],
                                'languageId' => $this->_currentLanguage,
                                'status' => 'ACTIVE'
                            );
                            $this->_sql->setTable($this->_productsAttributesCombinationDefinitions);
                            $action = $this->_sql->insert();
                            $action->values($productsAttributesCombinationDefinitionsArray);
                            $statement = $this->_sql->prepareStatementForSqlObject($action);
                            $statement->execute();
                        } else {
                            $productsAttributesCombinationData = array(
                                'attributeGroupId' => $attributeGroupId,
                                'productOrVariantId' => $productId,
                                'attributeValue' => $inputField['value'],
                                'isVariant' => $isVariant,
                                'groupType' => $attributeGroupDetails['viewAs'],
                                'status' => 'ACTIVE'
                            );
                            $this->_sql->setTable($this->_productsAttributesCombinationTable);
                            $action = $this->_sql->insert();
                            $action->values($productsAttributesCombinationData);
                            $statement = $this->_sql->prepareStatementForSqlObject($action);
                            $statement->execute();
                        }
                        break;
                }
            }
        }
        return $historicalCollection;
    }

    /**
     *
     * @param string $styleOrOption
     * @param string $level
     * @return bool|void
     */
    public function getProductDataForStyleOrOption($styleOrOption, $level = 'style')
    {
        switch ($level) {
            case 'style':
                return $this->getProductDataForStyle($styleOrOption);
                break;
            case 'option':
                return $this->getProductDataForOption($styleOrOption);
                break;
        }
    }

    /**
     *
     * @param string $style
     * @return array
     */
    private function getProductDataForStyle($style)
    {
        $productData = $this->getMainProductDetailsByStyle($style);
        $productPrices = $this->getProductPricesForStyleOrOption($style, null, true);
        $inputFieldData = $this->getAttributeValuesByStyleOrOption($productData['productId'], 'style');

        return array_merge(array(
            'tableField' => $productData,
            'prices' => $productPrices
        ), $inputFieldData);
    }

    /**
     *
     * @param  $productIdOrOptionCode
     * @param string $level
     * @return array
     */
    private function getAttributeValuesByStyleOrOption($productIdOrVariantId, $level = 'style')
    {
        $returnArray = array();
        switch ($level) {
            case 'style':
                $commonAttributes = $this->getCommonAttributesFor($productIdOrVariantId, 'PRODUCTS');
                $attributeGroups = $this->getAttributesFor($productIdOrVariantId, 0);
                $returnArray = array(
                    'commonAttributes' => $commonAttributes,
                    'attributes' => $attributeGroups
                );
                break;
            case 'option':
                $commonAttributes = $this->getCommonAttributesFor($productIdOrVariantId, 'PRODUCTS_R_ATTRIBUTES');
                $attributeGroups = $this->getAttributesFor($productIdOrVariantId, 1);
                $returnArray = array(
                    'commonAttributes' => $commonAttributes,
                    'attributes' => $attributeGroups
                );
                break;
        }

        return $returnArray;
    }

    /**
     *
     * @param int $commonAttributeId
     * @param int $productOrVariantId
     * @param string $productIdFrom
     * @return bool
     */
    public function deleteProductCommonAttribute($commonAttributeId, $productOrVariantId, $productIdFrom = 'PRODUCTS_R_ATTRIBUTES')
    {
        try {
            /**
             * Delete all attribute values for $commonAttributeId and $ValueId
             */
            $whereClause = array(
                'commonAttributeId' => $commonAttributeId,
                'productOrVariantId' => $productOrVariantId,
                'productIdFrom' => $productIdFrom
            );
            $this->_sql->setTable($this->productsCommonAttributesTable);
            $action = $this->_sql->delete()->where($whereClause);
            $this->_sql->prepareStatementForSqlObject($action)->execute();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     *
     * @param int $commonAttributeId
     * @param array $productOrVariantIds
     * @param sting $productIdFrom
     * @return boolean
     */
    public function blukDeleteProductCommonAttribute($commonAttributeId, $productOrVariantIds, $productIdFrom)
    {
        try {
            $sql = new Sql($this->_dbAdapter);

            $delete = $sql->delete();
            $delete->from($this->productsCommonAttributesTable);
            $delete->where->equalTo('commonAttributeId', $commonAttributeId);
            $delete->where->equalTo('productIdFrom', $productIdFrom);
            $delete->where->in('productOrVariantId', $productOrVariantIds);

            //echo $sql->getSqlstringForSqlObject($delete); //die ;
            $statement = $sql->prepareStatementForSqlObject($delete);
            $result = $statement->execute();

            if($result->getAffectedRows() == 0)
            {
                return false;
            }

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $data
     * @param $productOrVariantIds
     * @param $productIdFrom
     * @return bool
     */
    public function blukUpdateProductTradingCurrencyOrPrice($data, $productOrVariantIds, $productIdFrom, $subbmitedUserId = null)
    {
        $historicalEntriesCollection = new ArrayCollection();

        $isStyle = false;
        if ($productIdFrom == 'PRODUCTS') {
            $isStyle = true;
        }
        $attributeName = false;
        $newValue = false;
        $column = false;
        if (isset($data['currencyCode'])) {
            $attributeName = 'trading-currency';
            $newValue = $data['currencyCode'];
            $column = 'currencyCode';
        }
        if (isset($data['price'])) {
            $attributeName = 'trading-currency-price';
            $newValue = $data['price'];
            $column = 'price';
        }

        try {
            $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();

            foreach ($productOrVariantIds as $productOrVariantId) {

                $this->_sql->setTable($this->_productSupplierPriceTable);
                $select = $this->_sql->select();
                $select->where->equalTo('productIdFrom', $productIdFrom);
                $select->where->equalTo('productOrVariantId', $productOrVariantId);

                $statement = $this->_sql->prepareStatementForSqlObject($select);
                $results = $statement->execute();

                $optionCode = '';
                if (!$isStyle) {
                    $optionCode = $this->getOptionCodeForVariantId($productOrVariantId);
                }

                if ($results->count() > 0) {
                    /** Product already has values - we need to update them. */
                    $olderData = $results->next();
                    $sql = new Sql($this->_dbAdapter);
                    $update = $sql->update();
                    $update->table($this->_productSupplierPriceTable);
                    $update->set($data);
                    $update->where->equalTo('productIdFrom', $productIdFrom);
                    $update->where->equalTo('productOrVariantId', $productOrVariantId);

                    $statement = $sql->prepareStatementForSqlObject($update);
                    $statement->execute();
                    if ($attributeName &&
                        isset($olderData[$column]) &&
                        $olderData[$column] != $newValue
                    ) {

                        $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                            'productOrVariantId' => $productOrVariantId,
                            'isStyle' => $isStyle,
                            'commonAttribute' => null,
                            'attributeName' => $attributeName,
                            'value' => $newValue,
                            'fromDate' => null,
                            'updateType' => HistoricalValue::UPDATE_TYPE_Bulk,
                            'optionCode' => $optionCode,
                            'userId' => $subbmitedUserId
                        ]);
                        $historicalEntriesCollection->add($collectionItem);
                    }
                } else {
                    /** Product has no values - we need to insert them. */
                    $insertArray = array(
                        'productIdFrom' => $productIdFrom,
                        'productOrVariantId' => $productOrVariantId,
                        $column => $newValue,
                    );
                    $this->_sql->setTable($this->_productSupplierPriceTable);
                    $action = $this->_sql->insert();
                    $action->values($insertArray);
                    $statement = $this->_sql->prepareStatementForSqlObject($action);

                    $statement->execute();
                    if (!empty($attributeName)) {
                        /** New value will be updated. Add to historical entries. */
                        $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                            'productOrVariantId' => $productOrVariantId,
                            'isStyle' => $isStyle,
                            'commonAttribute' => null,
                            'attributeName' => $attributeName,
                            'value' => $newValue,
                            'fromDate' => null,
                            'updateType' => HistoricalValue::UPDATE_TYPE_Bulk,
                            'optionCode' => $optionCode,
                            'userId' => $subbmitedUserId
                        ]);
                        $historicalEntriesCollection->add($collectionItem);
                    }
                }
            }

            $this->_dbAdapter->getDriver()->getConnection()->commit();
        } catch (\Exception $ex) {
            $this->_dbAdapter->getDriver()->getConnection()->rollback();
            return false;

        }
        $this->historicalService->recordChanges($historicalEntriesCollection);
        return true;
    }

    /**
     *
     * @param int $attributeValueId
     * @param int $commonAttributeId
     * @param int $productOrVariantId
     * @param string $productIdFrom
     * @return bool
     */
    public function saveCommonAttributeAgainstProduct($attributeValueId, $commonAttributeId, $productOrVariantId, $productIdFrom = 'PRODUCTS_R_ATTRIBUTES')
    {
        try {
            $this->_sql->setTable($this->productsCommonAttributesTable);
            $action = $this->_sql->insert();
            $action->values(array(
                'productOrVariantId' => $productOrVariantId,
                'productIdFrom' => $productIdFrom,
                'commonAttributeId' => $commonAttributeId,
                'commonAttributeValue' => $attributeValueId
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $attributeValueId
     * @param $commonAttributeId
     * @param $styleOrOptionIds
     * @param $productIdFrom
     * @return bool
     */
    public function blukSaveCommonAttributeAgainstProduct($attributeValueId, $commonAttributeId, $styleOrOptionIds, $productIdFrom)
    {
        try {
            foreach ($styleOrOptionIds as $styleOrOptionId) {

                $this->_sql->setTable($this->productsCommonAttributesTable);
                $action = $this->_sql->insert();
                $action->values(array(
                    'productOrVariantId' => $styleOrOptionId,
                    'productIdFrom' => $productIdFrom,
                    'commonAttributeId' => $commonAttributeId,
                    'commonAttributeValue' => $attributeValueId
                ));
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
                switch ($productIdFrom) {
                    case 'PRODUCTS':
                        $this->loggingService->logUpdate($styleOrOptionId, null, $commonAttributeId, $attributeValueId);
                        break;
                    case 'PRODUCTS_R_ATTRIBUTES':
                        $this->loggingService->logUpdate(null, $styleOrOptionId, $commonAttributeId, $attributeValueId);
                        break;
                }
            }
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $commonAttributeId
     * @param array $commonAttributeValues
     * @param string $productIdFrom
     * @param array $productOrVariantIds
     * @param null|int $submittedByUser
     * @return ArrayCollection
     */
    public function createHistoricalEntriesForCommonAttribute(
        $commonAttributeId,
        $commonAttributeValues = [],
        $productIdFrom = 'PRODUCTS',
        $productOrVariantIds = [],
        $submittedByUser = null)
    {

        $commonAttribute = $this->commonAttributesService->getCommonAttributeById($commonAttributeId);
        $historicalCollection = new ArrayCollection();
        $isStyle = false;
        if ($productIdFrom == 'PRODUCTS') {
            $isStyle = true;
        }

        foreach ($productOrVariantIds as $productOrVariantId) {
            $optionCode = '';
            if (!$isStyle) {
                $optionCode = $this->getOptionCodeForVariantId($productOrVariantId);
            }
            foreach ($commonAttributeValues as $commonAttributeValue) {
                /** For each product and value that we have
                 *  Check if we already had an entry in the db.
                 *  If we already have an entry in DB check if the values are different. If no entry then create historical record.
                 */

                $whereClause = array(
                    'productOrVariantId' => $productOrVariantId,
                    'productIdFrom' => $productIdFrom,
                    'commonAttributeId' => $commonAttributeId,
                    'commonAttributeValue' => $commonAttributeValue
                );
                $this->_sql->setTable($this->productsCommonAttributesTable);
                $select = $this->_sql->select();
                $select->where($whereClause);
                $statement = $this->_sql->prepareStatementForSqlObject($select);
                $result = $statement->execute();

                if ($result->count() == 0) {
                    $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                        'productOrVariantId' => $productOrVariantId,
                        'isStyle' => $isStyle,
                        'commonAttribute' => $commonAttribute,
                        'attributeName' => '',
                        'value' => $commonAttributeValue,
                        'fromDate' => null,
                        'updateType' => HistoricalValue::UPDATE_TYPE_Bulk,
                        'optionCode' => $optionCode,
                        'userId' => $submittedByUser
                    ]);

                    $historicalCollection->add($historyEntity);
                }
            }
        }

        return $historicalCollection;
    }

    public function recordHistoricalChanges(ArrayCollection $historicalEntries)
    {
        /** @var HistoricalValue $historicalEntry */
        foreach ($historicalEntries as $historicalEntry) {
            if (!$historicalEntry->isStyle()) {
                $optionCode = $this->getOptionCodeForVariantId($historicalEntry->getProductOrVariantId());
                $historicalEntry->setOptionCode($optionCode);
            }
        }
        $this->historicalService->recordChanges($historicalEntries);
    }

    /**
     *
     * @param int $productIdOrOptionCode
     * @param bool $isVariant
     * @return array
     */
    private function getAttributesFor($productIdOrOptionCode, $isVariant)
    {
        $sql = new Sql($this->_dbAdapter);
        $sql->setTable($this->_productsAttributesCombinationTable);
        $select = $sql->select();
        $select->where(["productOrVariantId" => $productIdOrOptionCode]);
        $select->where(["isVariant" => $isVariant]);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $returnArray = array();
        if ($result->count() > 0) {
            foreach ($result as $row) {
                if ($row['groupType'] == 'TEXT') {
                    $sql = new Sql($this->_dbAdapter);
                    $sql->setTable($this->_productsAttributesCombinationDefinitions);
                    $select = $sql->select();
                    $select->where("productAttributeCombinationId = " . $row['productAttributeCombinationId']);
                    $select->where('languageId = ' . $this->_currentLanguage);
                    $statement = $sql->prepareStatementForSqlObject($select);

                    $result = $statement->execute();
                    if ($result->count() > 0) {
                        $definitionResult = $result->next();
                        $row['attributeValue'] = $definitionResult['attributeValue'];
                    }
                }
                $returnArray[$row['attributeGroupId']] = $row;
            }
        }
        return $returnArray;
    }

    /**
     *
     * @param int $productIdOrOptionCode
     * @param string $productIdFrom
     * @return array
     */
    public function getCommonAttributesFor($productIdOrOptionCode, $productIdFrom = "PRODUCTS", $commonAttributesArray = array())
    {
        $sql = new Sql($this->_dbAdapter);
        $sql->setTable($this->productsCommonAttributesTable);
        $select = $sql->select();
        $select->where(["productOrVariantId" => $productIdOrOptionCode]);
        $select->where(["productIdFrom" => $productIdFrom]);

        if (! empty($commonAttributesArray)) {
            $select->where(array(
                'commonAttributeId' => $commonAttributesArray
            ));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $returnArray = array();
        if ($result->count() > 0) {
            foreach ($result as $row) {
                $returnArray[$row['commonAttributeId']][] = $row;
            }
        }
        return $returnArray;
    }

    /**
     *
     * @param $commonAttributes
     * @return array
     */
    public function processAndGroupAttributes($commonAttributes)
    {
        $masterStyleAttributeName = 'Master Style';
        $commonAttributesProcessed = array();
        foreach ($commonAttributes as $commonAttributeOriginal) {

            $commonAttribute = $commonAttributeOriginal[0];
            /** @var \PrismProductsManager\Entity\CommonAttributes $commonAttributeEntity */
            $commonAttributeEntity = $this->commonAttributesService->getCommonAttribute($commonAttribute['commonAttributeId']);

            $commonAttributeType = $commonAttributeEntity->getCommonAttributeGuiRepresentation()
                ->getCommonAttributesViewTypeId()
                ->getCommonAttributeViewType();
            $selectedValues = false;

            $skip = false;
            $masterStyle = false;
            if ($commonAttributeEntity->getCommonAttributeDefinitions()->getDescription() == $masterStyleAttributeName) {
                // check master style
                if (!empty($commonAttribute['commonAttributeValue'])) {
                    $selectedValues = $commonAttribute['commonAttributeValue'];
                    $masterStyle = true;
                } else {
                    $skip = true;
                }
            } else {
                switch ($commonAttributeType) {
                    case 'SELECT':
                        if (! empty($commonAttribute['commonAttributeValue'])) {
                            /** @var \PrismProductsManager\Entity\CommonAttributesValues $commonAttributeValueEntity */
                            $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($commonAttribute['commonAttributeValue']);
                            if (! is_null($commonAttributeValueEntity)) {
                                $commonAttributeValueAbbrev = $commonAttributeValueEntity->getCommonAttributeValueAbbrev();
                                if ($commonAttributeValueAbbrev !== '' && $commonAttributeValueAbbrev !== null) {
                                    $selectedValues = $commonAttributeValueAbbrev;
                                } else {
                                    $commonAttributeValue = $commonAttributeValueEntity->getCommonAttributeValuesDefinitions()->getValue();
                                    $selectedValues = $commonAttributeValue;
                                }
                            }
                        } else {
                            $skip = true;
                        }
                        break;

                    case 'MULTISELECT':
                        $selectedValues = array();
                        foreach ($commonAttributeOriginal as $attributeEntries) {
                            if (! empty($commonAttribute['commonAttributeValue'])) {
                                $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($attributeEntries['commonAttributeValue']);
                                if (!is_null($commonAttributeValueEntity)) {
                                    $commonAttributeValueAbbrev = $commonAttributeValueEntity->getCommonAttributeValueAbbrev();
                                    if ($commonAttributeValueAbbrev !== '' && $commonAttributeValueAbbrev !== null) {
                                        $selectedValues[] = $commonAttributeValueAbbrev;
                                    } else {
                                        $commonAttributeValue = $commonAttributeValueEntity->getCommonAttributeValuesDefinitions()->getValue();
                                        $selectedValues[] = $commonAttributeValue;
                                    }
                                }
                            } else {
                                $skip = true;
                            }
                        }
                        break;
                    case 'PRODUCT':
                        $selectedValues = array();

                        foreach ($commonAttributeOriginal as $attributeEntries) {
                            if (! empty($commonAttribute['commonAttributeValue'])) {

                                /** Check if the sku exists */
                                $variantId = $this->getVariantIdFromSku($commonAttribute['commonAttributeValue']);
                                if (!empty($variantId)) {
                                    $selectedValues[] = $attributeEntries['commonAttributeValue'];
                                }
                            } else {
                                $skip = true;
                            }
                        }
                        break;
                    case 'CHECKBOX':
                    case 'TEXT':
                    case 'TEXTAREA':
                    case 'DATEPICKER':
                    case 'DATEPICKER - DATE ONLY':
                        if (!empty($commonAttribute['commonAttributeValue'])) {
                            $selectedValues = $commonAttribute['commonAttributeValue'];
                        } else {
                            $skip = true;
                        }
                        break;
                }
            }

            if ($skip) {
                continue;
            }

            $mappingString = ! is_null($commonAttributeEntity->getCommonAttributeMapping()) ? $commonAttributeEntity->getCommonAttributeMapping()->getCommonAttributeMapping() : '';

            if ($masterStyle) {
                $level = 'master_style';
                $newMapping = $level;
            } else if (strpos($mappingString, 'STYLE/') !== false) {
                $level = 'style';
                $newMapping = str_replace('STYLE/', '', $mappingString);
            } elseif (strpos($mappingString, 'OPTION/') !== false) {
                $level = 'option';
                $newMapping = str_replace('OPTION/', '', $mappingString);
            } else {
                $level = 'none';
                $newMapping = $mappingString;
            }

            $commonAttributesProcessed[$level][] = array(
                'commonAttributeName' => $commonAttributeEntity->getCommonAttributeDefinitions()->getDescription(),
                'commonAttributeId' => $commonAttributeEntity->getCommonAttributeId(),
                'values' => $selectedValues,
                'mapping' => $newMapping,
                'modified' => $commonAttribute['modified']
            );
        }

        return $commonAttributesProcessed;
    }

    /**
     *
     * @param string $styleOrOption
     * @return bool
     */
    private function getProductDataForOption($styleOrOption)
    {
        $variantDetails = $this->getVariantDetailsByStyle($styleOrOption);
        if (empty($variantDetails)) {
            $styleOrOption = str_replace(' ', '', $variantDetails);
            $variantDetails = $this->getVariantDetailsByStyle($styleOrOption);
        }

        if (isset($variantDetails['productAttributeId'])) {
            $inputFieldData = $this->getAttributeValuesByStyleOrOption($variantDetails['productAttributeId'], 'option');

            return array_merge(array(
                'tableField' => $variantDetails
            ), $inputFieldData);
        }

        return array();
    }

    /**
     *
     * @param
     *            $optionCode
     * @param bool|true $onlyOne
     * @return bool|void|\Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getVariantDetailsByStyle($optionCode, $onlyOne = true)
    {
        $this->_sql->setTable(array(
            'pra' => $this->_productsVariantsRelationshipTable
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'productAttributeId',
            'sku',
            'status',
            'ean13'
        ));

        $select->join(array(
            'pd' => $this->_productsDefinitionsTable
        ), // Set join table and alias
            new Expression('pra.productAttributeId = pd.productId AND pd.productIdFrom = "PRODUCTS_R_ATTRIBUTES" AND pd.languageId = ' . $this->_currentLanguage), array(
                'productDefinitionsId',
                'productId',
                'productIdFrom',
                'languageId',
                'versionId',
                'versionDescription',
                'shortProductName',
                'productName',
                'urlName',
                'metaTitle',
                'metaDescription',
                'metaKeyword',
                'shortDescription',
                'longDescription',
                'misSpells',
                'tags',
                'makeLive',
                'created',
                'modified'
            ), Select::JOIN_LEFT);
        $select->join(array(
            'p' => $this->_tableName
        ), // Set join table and alias
            new Expression('pra.productId = p.productId'), array(
                'manufacturerId',
                'productTypeId',
                'productMerchantCategoryId',
                'taxId',
                'ecoTax',
                'quantity',
                'style',
                'supplierReference',
                'productId'
            ), Select::JOIN_LEFT);
        $select->where("pra.sku LIKE '$optionCode%'");

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $productData = false;
        if ($result->count() > 0) {
            if ($onlyOne) {
                $productData = $result->next();
            } else {
                $productData = $result;
            }
        }
        return $productData;
    }

    /**
     *
     * @param int $variantId
     * @param $productIdFrom
     * @param $territoryId
     * @param $currencyId
     * @return string
     */
    public function getPriceColumnForProduct($variantId, $productIdFrom, $territoryId, $currencyId)
    {
        return '33'; // TODO: Return price and test price API
    }

    /**
     *
     * @param string $style
     * @return bool|void
     */
    public function getMainProductDetailsByStyle($style)
    {
        $this->_sql->setTable(array(
            'p' => $this->_tableName
        ));
        $select = $this->_sql->select();
        $select->columns(array(
            'productId',
            'style',
            'status'
        ));

        $select->join(array(
            'pd' => $this->_productsDefinitionsTable
        ), // Set join table and alias
            new Expression('p.productId = pd.productId AND pd.productIdFrom = "PRODUCTS" AND pd.languageId = ' . $this->_currentLanguage), array(
                '*'
            ), Select::JOIN_LEFT);
        $select->where("p.style = '$style'");
        //echo  $this->_sql->getSqlstringForSqlObject($select); die;
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $productData = false;
        if ($result->count() > 0) {
            $productData = $result->next();
        }
        return $productData;
    }

    /**
     *
     * @param $style
     * @param $formData
     * @return bool
     */
    public function updateStyle($style, $formData, $updatePricesFlag = false)
    {
        $inputArray = $this->formatSubmitedArray($formData);
        $productDetails = $this->getMainProductDetailsByStyle($style);

        try {
            /**
             * Update table fields
             */
            if (! empty($inputArray['isTableField'])) {
                foreach ($inputArray['isTableField'] as $tableName => $fields) {

                    /**
                     * Retrieve all values for the fields
                     */
                    $updateArray = array();
                    /**
                     * Loop all values and make sure they are editable values
                     */

                    foreach ($fields as $databaseField => $value) {
                        if ($value['entity']->isEditable()) {
                            $updateArray[$databaseField] = $value['value'];
                        }
                    }

                    /**
                     * Save the new values to the $tableName table
                     */
                    if (! empty($updateArray)) {
                        /**
                         * Retrieve primary key of the table
                         */
                        $sql = "SHOW KEYS FROM " . $tableName . " WHERE Key_name = 'PRIMARY'";
                        $statement = $this->_dbAdapter->query($sql);
                        $primaryKey = $statement->execute()->next();
                        /**
                         * Check if the primary key is in the product data array
                         */
                        if (! empty($primaryKey['Column_name']) && ! empty($productDetails[$primaryKey['Column_name']])) {
                            /**
                             * Build the where clause
                             */
                            $primaryColumn = $primaryKey['Column_name'];
                            $primaryValue = $productDetails[$primaryKey['Column_name']];
                            $whereClause = array(
                                $primaryColumn => $primaryValue
                            );
                            /**
                             * Update table $tableName with $updateArray data using $whereClause
                             */
                            $update = $this->_sql->update();
                            $update->table($tableName);
                            $update->set($updateArray);
                            $update->where($whereClause);
                            $statement = $this->_sql->prepareStatementForSqlObject($update);
                            $statement->execute();
                        }
                    }
                }
            }

            if (! empty($inputArray['isAttribute'])) {
                /**
                 * Update input attribute fields
                 */
                foreach ($inputArray['isAttribute'] as $inputAttributeField) {
                    $attributeTable = $inputAttributeField['entity']->getInputFieldRAttributesInputFieldRAttributeId()->getAttributeTable();
                    switch ($attributeTable) {
                        case 'COMMON_ATTRIBUTES':
                            /**
                             * Build where clause
                             */
                            /** @var InputFieldRAttributes $entity */
                            $entity =  $inputAttributeField['entity']->getinputFieldRAttributesInputfieldrattributeid();

                            $commonAttributeId = $entity->getcommonAttributeId();
                            $commonAttributeEntity = $entity->getCommonAttributes();

                            $whereClause = array(
                                'commonAttributeId' => $commonAttributeId,
                                'productOrVariantId' => $productDetails['productId'],
                                'productIdFrom' => 'PRODUCTS'
                            );
                            $this->_sql->setTable($this->productsCommonAttributesTable);
                            $select = $this->_sql->select();
                            $select->where($whereClause);
                            $statement = $this->_sql->prepareStatementForSqlObject($select);
                            $count = $statement->execute()->count();

                            if ($count > 0) {
                                /**
                                 * Need to update
                                 */
                                if (is_array($inputAttributeField['value'])) {
                                    /**
                                     * Need to delete all previews values for multi-select
                                     */
                                    $this->_sql->setTable($this->productsCommonAttributesTable);
                                    $action = $this->_sql->delete()->where($whereClause);
                                    $this->_sql->prepareStatementForSqlObject($action)->execute();
                                    /**
                                     * Need to insert new values ( multi-select )
                                     */
                                    foreach ($inputAttributeField['value'] as $inputFieldValue) {
                                        $this->_sql->setTable($this->productsCommonAttributesTable);
                                        $action = $this->_sql->insert();
                                        $action->values(array(
                                            'productOrVariantId' => $productDetails['productId'],
                                            'productIdFrom' => 'PRODUCTS',
                                            'commonAttributeId' => $commonAttributeId,
                                            'commonAttributeValue' => $inputFieldValue
                                        ));
                                        $statement = $this->_sql->prepareStatementForSqlObject($action);
                                        $statement->execute();
                                        $this->loggingService->logUpdate($style, null, $commonAttributeId, $inputFieldValue);
                                        if ($commonAttributeEntity->isKeepHistory()) {
                                            $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                                'productOrVariantId' => $productDetails['productId'],
                                                'isStyle' => true,
                                                'commonAttribute' => $commonAttributeEntity,
                                                'attributeName' => '',
                                                'value' => $inputFieldValue,
                                                'fromDate' => null,
                                                'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                                'optionCode' => ''
                                            ]);
                                            $historicalCollection = new ArrayCollection([$historyEntity]);
                                            $this->historicalService->recordChanges($historicalCollection);
                                        }
                                    }
                                } else {
                                    /** Check if history is meant to be kept and also
                                     * if the current value has changed from the old value. */
                                    if ($commonAttributeEntity->isKeepHistory() &&
                                        $this->areValuesChanged(
                                            $productDetails['productId'],
                                            'PRODUCTS',
                                            $inputAttributeField['value'],
                                            $commonAttributeId,
                                            'Common_Attributes'
                                        )
                                    ) {
                                        $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                            'productOrVariantId' => $productDetails['productId'],
                                            'isStyle' => true,
                                            'commonAttribute' => $commonAttributeEntity,
                                            'attributeName' => '',
                                            'value' => $inputAttributeField['value'],
                                            'fromDate' => null,
                                            'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                            'optionCode' => ''
                                        ]);
                                        $historicalCollection = new ArrayCollection([$historyEntity]);
                                        $this->historicalService->recordChanges($historicalCollection);
                                    }

                                    $update = $this->_sql->update();
                                    $update->table($this->productsCommonAttributesTable);
                                    $update->set(array(
                                        'commonAttributeValue' => $inputAttributeField['value']
                                    ));
                                    $update->where($whereClause);
                                    $statement = $this->_sql->prepareStatementForSqlObject($update);
                                    $statement->execute();
                                    $this->loggingService->logUpdate($style, null, $commonAttributeId, $inputAttributeField['value']);
                                }
                            } else {
                                /**
                                 * Need to insert
                                 */
                                if (is_array($inputAttributeField['value'])) {
                                    /**
                                     * Need to insert all values ( multi-select )
                                     */
                                    foreach ($inputAttributeField['value'] as $inputFieldValue) {
                                        $this->_sql->setTable($this->productsCommonAttributesTable);
                                        $action = $this->_sql->insert();
                                        $action->values(array(
                                            'productOrVariantId' => $productDetails['productId'],
                                            'productIdFrom' => 'PRODUCTS',
                                            'commonAttributeId' => $commonAttributeId,
                                            'commonAttributeValue' => $inputFieldValue
                                        ));
                                        $statement = $this->_sql->prepareStatementForSqlObject($action);
                                        $statement->execute();
                                        $this->loggingService->logUpdate($style, null, $commonAttributeId, $inputFieldValue);
                                        /** Check if history is meant to be kept and also
                                         * if the current value has changed from the old value. */
                                        if ($commonAttributeEntity->isKeepHistory()) {
                                            $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                                'productOrVariantId' => $productDetails['productId'],
                                                'isStyle' => true,
                                                'commonAttribute' => $commonAttributeEntity,
                                                'attributeName' => '',
                                                'value' => $inputFieldValue,
                                                'fromDate' => null,
                                                'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                                'optionCode' => ''
                                            ]);
                                            $historicalCollection = new ArrayCollection([$historyEntity]);
                                            $this->historicalService->recordChanges($historicalCollection);
                                        }
                                    }
                                } else {
                                    $this->_sql->setTable($this->productsCommonAttributesTable);
                                    $action = $this->_sql->insert();
                                    $action->values(array(
                                        'productOrVariantId' => $productDetails['productId'],
                                        'productIdFrom' => 'PRODUCTS',
                                        'commonAttributeId' => $commonAttributeId,
                                        'commonAttributeValue' => $inputAttributeField['value']
                                    ));
                                    $statement = $this->_sql->prepareStatementForSqlObject($action);
                                    $statement->execute();
                                    $this->loggingService->logUpdate($style, null, $commonAttributeId, $inputAttributeField['value']);

                                    if ($commonAttributeEntity->isKeepHistory()) {
                                        $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                            'productOrVariantId' => $productDetails['productId'],
                                            'isStyle' => true,
                                            'commonAttribute' => $commonAttributeEntity,
                                            'attributeName' => '',
                                            'value' => $inputAttributeField['value'],
                                            'fromDate' => null,
                                            'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                            'optionCode' => ''
                                        ]);
                                        $historicalCollection = new ArrayCollection([$historyEntity]);
                                        $this->historicalService->recordChanges($historicalCollection);
                                    }
                                }
                            }
                            break;
                        case 'ATTRIBUTES_Groups':
                            $attributeGroupId = $inputAttributeField['entity']->getInputFieldRAttributesInputFieldRAttributeId()
                                ->getAttributeGroupId()
                                ->getAttributeGroupId();
                            $attributeGroupDetails = $this->_attributesGroupMapper->fetchAllAttributeGroups($this->_currentLanguage, $attributeGroupId);
                            $isVariant = 0;
                            $updateData = array(
                                'attributeValue' => $inputAttributeField['value']
                            );
                            $whereClause = array(
                                'attributeGroupId' => $attributeGroupId,
                                'productOrVariantId' => $productDetails['productId'],
                                'isVariant' => $isVariant
                            );

                            /**
                             * Check if the product has the attribute
                             */
                            $this->_sql->setTable(array(
                                'pac' => $this->_productsAttributesCombinationTable
                            ));
                            $select = $this->_sql->select();
                            $select->where($whereClause);
                            $select->limit(1);
                            $statement = $this->_sql->prepareStatementForSqlObject($select);
                            $results = $statement->execute();
                            $count = $results->count();
                            $results = $results->next();

                            /**
                             * Check if the product is already saved
                             */
                            if ($count > 0) {
                                /**
                                 * Update the attribute
                                 */
                                if (! empty($attributeGroupDetails['viewAs']) && $attributeGroupDetails['viewAs'] == 'TEXT') {
                                    /**
                                     * Need to update PRODUCTS_Attributes_Combination_Definitions
                                     */
                                    /**
                                     * Select the attributes combination to get the id
                                     */
                                    if (! empty($results['productAttributeCombinationId'])) {
                                        $whereClause = array(
                                            'productAttributeCombinationId' => $results['productAttributeCombinationId'],
                                            'languageId' => $this->_currentLanguage
                                        );
                                        $update = $this->_sql->update();
                                        $update->table($this->_productsAttributesCombinationDefinitions);
                                        $update->set($updateData);
                                        $update->where($whereClause);
                                        $statement = $this->_sql->prepareStatementForSqlObject($update);
                                        $statement->execute();
                                    }
                                } else {
                                    /**
                                     * Need to update PRODUCTS_Attributes_Combination
                                     */
                                    $update = $this->_sql->update();
                                    $update->table($this->_productsAttributesCombinationTable);
                                    $update->set($updateData);
                                    $update->where($whereClause);
                                    $statement = $this->_sql->prepareStatementForSqlObject($update);
                                    $statement->execute();
                                }
                            } else {
                                /**
                                 * Create new attribute
                                 */
                                /**
                                 * Need to create new entry in _productsAttributesCombinationTable table
                                 */
                                $this->_sql->setTable($this->_productsAttributesCombinationTable);
                                $action = $this->_sql->insert();
                                if (! empty($attributeGroupDetails['viewAs']) && $attributeGroupDetails['viewAs'] == 'TEXT') {
                                    $attributeValue = '';
                                } else {
                                    $attributeValue = $inputAttributeField['value'];
                                }
                                $action->values(array(
                                    'attributeGroupId' => $attributeGroupDetails['attributeGroupId'],
                                    'productOrVariantId' => $productDetails['productId'],
                                    'attributeValue' => $attributeValue,
                                    'isVariant' => $isVariant,
                                    'groupType' => $attributeGroupDetails['viewAs'],
                                    'status' => 'ACTIVE'
                                ));
                                $statement = $this->_sql->prepareStatementForSqlObject($action);
                                $statement->execute();
                                if (! empty($attributeGroupDetails['viewAs']) && $attributeGroupDetails['viewAs'] == 'TEXT') {
                                    /**
                                     * If the attribute is text we need to create new entry in the _productsAttributesCombinationDefinitions table
                                     */
                                    $this->_sql->setTable($this->_productsAttributesCombinationDefinitions);
                                    $action = $this->_sql->insert();
                                    $action->values(array(
                                        'productAttributeCombinationId' => $this->_dbAdapter->getDriver()
                                            ->getConnection()
                                            ->getLastGeneratedValue(),
                                        'attributeValue' => $inputAttributeField['value'],
                                        'languageId' => $this->_currentLanguage,
                                        'status' => 'ACTIVE'
                                    ));
                                    $statement = $this->_sql->prepareStatementForSqlObject($action);
                                    $statement->execute();
                                }
                            }
                            break;
                    }
                }
            }

            /**
             * Update Prices
             */
            if (! empty($inputArray['prices']) && $updatePricesFlag) {
                $this->updatePricesFor($productDetails['productId'], 'PRODUCTS', $inputArray['prices']);
            }
            return true;
        } catch (\Exception $ex) {
            var_dump($ex->getMessage());die;
            return false;
        }
    }

    /**
     * @param $productOrVariantId
     * @param $productIdFrom
     * @param $newValue
     * @param $commonAttributeId
     * @param $checkType
     * @return bool
     */
    private function areValuesChanged($productOrVariantId, $productIdFrom, $newValue, $commonAttributeId, $checkType)
    {
        switch ($checkType) {
            case 'Common_Attributes':
                $this->_sql->setTable($this->productsCommonAttributesTable);
                $select = $this->_sql->select()
                    ->where(array(
                        'productOrVariantId' => $productOrVariantId,
                        'productIdFrom' => $productIdFrom,
                        'commonAttributeId' => $commonAttributeId,
                        'commonAttributeValue' => $newValue,
                    ))
                    ->limit(1);
                $statement = $this->_sql->prepareStatementForSqlObject($select);
                $result = $statement->execute();
                $resultCount = $result->count();
                if ($resultCount > 0) {
                    $sql = new Sql($this->_dbAdapter);
                    $sql->setTable('HISTORICAL_Values');
                    $isStyle = 0;
                    $optionCode = '';
                    if ($productIdFrom == 'PRODUCTS') {
                        $isStyle = 1;
                    } else {
                        $optionCode = $this->getOptionCodeForVariantId($productOrVariantId);
                    }
                    $select = $sql->select()
                        ->where(array(
                            'productOrVariantId' => $productOrVariantId,
                            'style' => $isStyle,
                            'optionCode' => $optionCode,
                            'commonAttributeId' => $commonAttributeId,
                            'value' => $newValue
                        ))
                        ->limit(1);
                    $statement = $sql->prepareStatementForSqlObject($select);
                    try {
                        $result = $statement->execute();
                    } catch (\Exception $ex) {
                        var_dump(array(
                            'productOrVariantId' => $productOrVariantId,
                            'style' => $isStyle,
                            'optionCode' => $optionCode,
                            'commonAttributeId' => $commonAttributeId,
                            'value' => $newValue
                        ));
                        var_dump($ex->getMessage());die;
                    }

                    $resultSecondCount = $result->count();
                    if ($resultSecondCount == 0) {
                        /** No values are in the history - update them */
                        return true;
                    }
                    return false;
                }
                break;
            default:
                break;
        }

        return true;
    }

    /**
     *
     * @param int $productOrVariantId
     * @param $productIdFrom
     * @param $pricesArray
     */
    private function updatePricesFor($productOrVariantId, $productIdFrom, $pricesArray)
    {
        /**
         * SUPPLIER PRICES *
         */
        $this->updateSupplierPrices($productOrVariantId, $productIdFrom, $pricesArray);
        /**
         * END OF SUPPLIER PRICES *
         */

        /**
         * NORMAL PRICES *
         */
        $this->updateNormalPrices($productOrVariantId, $productIdFrom, $pricesArray);
        /**
         * END OF NORMAL PRICES *
         */
    }

    /**
     *
     * @param int $productOrVariantId
     * @param $productIdFrom
     * @param $pricesArray
     */
    private function updateNormalPrices($productOrVariantId, $productIdFrom, $pricesArray)
    {
        $isStyle = false;
        if ($productIdFrom == 'PRODUCTS') {
            $isStyle = true;
        }
        $optionCode = '';
        if (!$isStyle) {
            $optionCode = $this->getOptionCodeForVariantId($productOrVariantId);
        }

        $historicalPricesCollection = new ArrayCollection();

        foreach ($pricesArray['nowPrice'] as $currency => $territories) {
            foreach ($territories as $territory => $priceValue) {
                /**
                 * Check if product has already prices
                 */
                if (empty($this->getTerritoryId($territory))) {
                    continue;
                }

                if (empty($this->getCurrencyIdFromCode($currency))) {
                    continue;
                }

                $this->_sql->setTable($this->_productsPricesTable);
                $select = $this->_sql->select()
                    ->where(array(
                        'productIdFrom' => $productIdFrom,
                        'productOrVariantId' => $productOrVariantId,
                        'regionId' => $this->getTerritoryId($territory),
                        'currencyId' => $this->getCurrencyIdFromCode($currency),
                        'status' => 'ACTIVE'
                    ))
                    ->limit(1);
                $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $result = $statement->execute();
                    $resultCount = $result->count();
                    if ($resultCount > 0) {
                    /**
                     * We need to update the price
                     */
                    $resultData = $result->next();
                    $productPriceId = $resultData['productPriceId'];
                    /**
                     * Set up update data
                     */
                    $updateData = [];
                    if (!empty($pricesArray['costPrice'][$currency])) {
                        $updateData['costPrice'] = number_format($pricesArray['costPrice'][$currency], 2);
                    }
                    if (!empty($pricesArray['wasPrice'][$currency][$territory])) {
                        $updateData['wasPrice'] = number_format($pricesArray['wasPrice'][$currency][$territory], 2);
                    }
                    if (!empty($pricesArray['nowPrice'][$currency][$territory])) {
                        $updateData['nowPrice'] = number_format($pricesArray['nowPrice'][$currency][$territory], 2);
                        $updateData['price'] = number_format($pricesArray['nowPrice'][$currency][$territory], 2);
                    }

                    if (!empty($updateData)) {
                        $updateData['modified'] = date('Y-m-d H:i:s');

                        /**
                         * Update data
                         */
                        $this->_sql->setTable($this->_productsPricesTable);
                        $update = $this->_sql->update()
                            ->set($updateData)
                            ->where(array(
                                'productPriceId' => $productPriceId
                            ));
                        $statement = $this->_sql->prepareStatementForSqlObject($update);
                        $statement->execute();
                    }
                    /**
                     * If changes are done at GBP-GB 1-1 we need to cascade to
                     * ROW-GBP 2-1 spectrum will use this price for countries in tax region RW
                     * EU-GBP 3-1 spectrum will use this price for countries in tax region DK, GB, RW, SE
                     */
                    if ($this->getTerritoryId($territory) == 1 && $this->getCurrencyIdFromCode($currency) == 1) {
                        $this->normalUpdateRelatedTerritoryCurrency($productIdFrom, $productOrVariantId, 2, 1, $updateData);
                        $this->normalUpdateRelatedTerritoryCurrency($productIdFrom, $productOrVariantId, 3, 1, $updateData);
                    }

                    /**
                     * If changes are done at USD-US 5-2 we need to cascade to
                     * GB-USD 1-2 spectrum will use this price for countries in tax region
                     */
                    if ($this->getTerritoryId($territory) == 5 && $this->getCurrencyIdFromCode($currency) == 2) {
                        $this->normalUpdateRelatedTerritoryCurrency($productIdFrom, $productOrVariantId, 1, 2, $updateData);
                    }

                    if (isset($updateData['costPrice']) &&
                        isset($resultData['costPrice']) &&
                        $updateData['costPrice'] !== $resultData['costPrice']
                    ) {
                        if (($territory == 'GB' && $currency == 'GBP') ||
                            ($territory == 'US' && $currency == 'USD') ||
                            ($territory == 'CA' && $currency == 'CAD')
                        ) {
                            $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                                'productOrVariantId' => $productOrVariantId,
                                'isStyle' => $isStyle,
                                'commonAttribute' => null,
                                'attributeName' => "landed-cost-{$currency}",
                                'value' => $updateData['costPrice'],
                                'fromDate' => null,
                                'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                'optionCode' => $optionCode
                            ]);
                            $historicalPricesCollection->add($collectionItem);
                        }
                    }

                    if (isset($updateData['nowPrice']) &&
                        isset($resultData['nowPrice']) &&
                        $updateData['nowPrice'] !== $resultData['nowPrice']
                    ) {
                        $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                            'productOrVariantId' => $productOrVariantId,
                            'isStyle' => $isStyle,
                            'commonAttribute' => null,
                            'attributeName' => "current-price-{$territory}-{$currency}",
                            'value' => $updateData['nowPrice'],
                            'fromDate' => null,
                            'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                            'optionCode' => $optionCode
                        ]);
                        $historicalPricesCollection->add($collectionItem);
                    }

                    if (isset($updateData['wasPrice']) &&
                        isset($resultData['wasPrice']) &&
                        $updateData['wasPrice'] !== $resultData['wasPrice']
                    ) {
                        $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                            'productOrVariantId' => $productOrVariantId,
                            'isStyle' => $isStyle,
                            'commonAttribute' => null,
                            'attributeName' => "original-price-{$territory}-{$currency}",
                            'value' => $updateData['wasPrice'],
                            'fromDate' => null,
                            'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                            'optionCode' => $optionCode
                        ]);
                        $historicalPricesCollection->add($collectionItem);
                    }
                } else {
                    /**
                     * We need to create a new price for the product
                     */
                    $insertData = array(
                        'productIdFrom' => $productIdFrom,
                        'productOrVariantId' => $productOrVariantId,
                        'regionId' => $this->getTerritoryId($territory),
                        'currencyId' => $this->getCurrencyIdFromCode($currency),
                        'price' => '',
                        'offerPrice' => '',

                        'tradingCostPrice' => '',
                        'wholesalePrice' => '',
                        'freightPrice' => '',
                        'dutyPrice' => '',
                        'designPrice' => '',
                        'packagingPrice' => '',
                        'status' => 'ACTIVE',
                        'created' => date('Y-m-d H:i:s'),
                        'modified' => date('Y-m-d H:i:s')
                    );

                    if (! empty($pricesArray['costPrice'][$currency])) {
                        $insertData['costPrice'] = $pricesArray['costPrice'][$currency];
                    }
                    if (! empty($pricesArray['wasPrice'][$currency][$territory])) {
                        $insertData['wasPrice'] = $pricesArray['wasPrice'][$currency][$territory];
                    }
                    if (! empty($pricesArray['nowPrice'][$currency][$territory])) {
                        $insertData['nowPrice'] = $pricesArray['nowPrice'][$currency][$territory];
                    }

                    $this->_sql->setTable($this->_productsPricesTable);
                    $action = $this->_sql->insert();
                    $action->values($insertData);
                    $statement = $this->_sql->prepareStatementForSqlObject($action);
                    $statement->execute();

                    /**
                     * If changes are done at GBP-GB 1-1 we need to cascade to
                     * ROW-GBP 2-1 spectrum will use this price for countries in tax region RW
                     * EU-GBP 3-1 spectrum will use this price for countries in tax region DK, GB, RW, SE
                     */
                    if ($this->getTerritoryId($territory) == 1 && $this->getCurrencyIdFromCode($currency) == 1) {
                        $this->normalUpdateRelatedTerritoryCurrency($productIdFrom, $productOrVariantId, 2, 1, $insertData);
                        $this->normalUpdateRelatedTerritoryCurrency($productIdFrom, $productOrVariantId, 3, 1, $insertData);
                    }

                    /**
                     * If changes are done at USD-US 5-2 we need to cascade to
                     * GB-USD 1-2 spectrum will use this price for countries in tax region
                     */
                    if ($this->getTerritoryId($territory) == 5 && $this->getCurrencyIdFromCode($currency) == 2) {
                        $this->normalUpdateRelatedTerritoryCurrency($productIdFrom, $productOrVariantId, 1, 2, $insertData);
                    }

                    if (($territory == 'GB' && $currency == 'GBP') ||
                        ($territory == 'US' && $currency == 'USD') ||
                        ($territory == 'CA' && $currency == 'CAD')
                    ) {
                        $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                            'productOrVariantId' => $productOrVariantId,
                            'isStyle' => $isStyle,
                            'commonAttribute' => null,
                            'attributeName' => "landed-cost-{$currency}",
                            'value' => isset($insertData['costPrice'])? $insertData['costPrice'] : null ,
                            'fromDate' => null,
                            'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                            'optionCode' => $optionCode
                        ]);
                        $historicalPricesCollection->add($collectionItem);
                    }

                    $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                        'productOrVariantId' => $productOrVariantId,
                        'isStyle' => $isStyle,
                        'commonAttribute' => null,
                        'attributeName' => "current-price-{$territory}-{$currency}",
                        'value' => (isset($insertData['nowPrice'])) ? $insertData['nowPrice'] : null,
                        'fromDate' => null,
                        'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                        'optionCode' => $optionCode
                    ]);
                    $historicalPricesCollection->add($collectionItem);

                    $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                        'productOrVariantId' => $productOrVariantId,
                        'isStyle' => $isStyle,
                        'commonAttribute' => null,
                        'attributeName' => "original-price-{$territory}-{$currency}",
                        'value' => (isset($insertData['wasPrice'])) ? $insertData['wasPrice'] : null,
                        'fromDate' => null,
                        'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                        'optionCode' => $optionCode
                    ]);
                    $historicalPricesCollection->add($collectionItem);
                }
            }
        }
        $this->historicalService->recordChanges($historicalPricesCollection);
    }

    /**
     *
     * @param $productIdOrVariantId
     * @param $productIdFrom
     * @param array $pricesArray
     */
    private function createNewPricesFor($productIdOrVariantId, $productIdFrom, $pricesArray = array())
    {
        /**
         * Create normal prices
         */
        foreach ($pricesArray['nowPrice'] as $currency => $territories) {
            foreach ($territories as $territory => $priceValue) {
                $insertData = array(
                    'productIdFrom' => $productIdFrom,
                    'productOrVariantId' => $productIdOrVariantId,
                    'regionId' => $this->getTerritoryId($territory),
                    'currencyId' => $this->getCurrencyIdFromCode($currency),
                    'price' => '',
                    'offerPrice' => '',
                    'nowPrice' => $pricesArray['nowPrice'][$currency][$territory],
                    'wasPrice' => $pricesArray['wasPrice'][$currency][$territory],
                    'costPrice' => $pricesArray['costPrice'][$currency],
                    'tradingCostPrice' => '',
                    'wholesalePrice' => '',
                    'freightPrice' => '',
                    'dutyPrice' => '',
                    'designPrice' => '',
                    'packagingPrice' => '',
                    'status' => 'ACTIVE',
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                );
                $this->_sql->setTable($this->_productsPricesTable);
                $action = $this->_sql->insert();
                $action->values($insertData);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
            }
        }
    }

    /**
     *
     * @param int $productOrVariantId
     * @param $productIdFrom
     * @param $pricesArray
     */
    private function updateSupplierPrices($productOrVariantId, $productIdFrom, $pricesArray)
    {
        $historicalEntriesCollection = new ArrayCollection();
        $isStyle = false;
        if ($productIdFrom == 'PRODUCTS') {
            $isStyle = true;
        }
        $optionCode = '';
        if (!$isStyle) {
            $optionCode = $this->getOptionCodeForVariantId($productOrVariantId);
        }

        if (! empty($pricesArray['supplier']['currency'])) {
            /**
             * Check if product has already saved supplier
             */
            $this->_sql->setTable($this->_productSupplierPriceTable);
            $select = $this->_sql->select()
                ->where(array(
                    'productIdFrom' => $productIdFrom,
                    'productOrVariantId' => $productOrVariantId
                ))
                ->limit(1);
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();

            $resultCount = $result->count();

            /**
             * If the product has already supplier prices then we need to update them
             * Otherwise we need to insert new supplier prices
             */
            if ($resultCount > 0) {
                /**
                 * Update current supplier price
                 */
                $supplierData = $result->next();
                $updateValues = array(
                    'productIdFrom' => $productIdFrom,
                    'productOrVariantId' => $productOrVariantId,
                    'currencyCode' => $pricesArray['supplier']['currency'],
                    'price' => $pricesArray['supplier']['price']
                );

                $this->_sql->setTable($this->_productSupplierPriceTable);
                $update = $this->_sql->update()
                    ->set($updateValues)
                    ->where(array(
                        'supplierPriceId' => $supplierData['supplierPriceId']
                    ));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();

                if ($supplierData['currencyCode'] != $pricesArray['supplier']['currency']) {
                    /** Currency was updated - save historical entry */
                    $historicalEntry = $this->historicalService->hydrateHistoricalEntity([
                        'productOrVariantId' => $productOrVariantId,
                        'isStyle' => $isStyle,
                        'commonAttribute' => null,
                        'attributeName' => "trading-currency",
                        'value' => $pricesArray['supplier']['currency'],
                        'fromDate' => null,
                        'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                        'optionCode' => $optionCode
                    ]);
                    $historicalEntriesCollection->add($historicalEntry);
                }

                if ($supplierData['price'] != $pricesArray['supplier']['price']) {
                    /** Price was updated - save historical entry */
                    $historicalEntry = $this->historicalService->hydrateHistoricalEntity([
                        'productOrVariantId' => $productOrVariantId,
                        'isStyle' => $isStyle,
                        'commonAttribute' => null,
                        'attributeName' => "trading-currency-price",
                        'value' => $pricesArray['supplier']['price'],
                        'fromDate' => null,
                        'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                        'optionCode' => $optionCode
                    ]);
                    $historicalEntriesCollection->add($historicalEntry);
                }
            } else {
                /**
                 * Create new supplier Price
                 */
                $this->_sql->setTable($this->_productSupplierPriceTable);
                $action = $this->_sql->insert();
                $insertData = array(
                    'productIdFrom' => $productIdFrom,
                    'productOrVariantId' => $productOrVariantId,
                    'currencyCode' => $pricesArray['supplier']['currency'],
                    'price' => $pricesArray['supplier']['price']
                );
                $action->values($insertData);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();

                $historicalEntry = $this->historicalService->hydrateHistoricalEntity([
                    'productOrVariantId' => $productOrVariantId,
                    'isStyle' => $isStyle,
                    'commonAttribute' => null,
                    'attributeName' => "trading-currency",
                    'value' => $pricesArray['supplier']['currency'],
                    'fromDate' => null,
                    'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                    'optionCode' => $optionCode
                ]);
                $historicalEntriesCollection->add($historicalEntry);

                $historicalEntry = $this->historicalService->hydrateHistoricalEntity([
                    'productOrVariantId' => $productOrVariantId,
                    'isStyle' => $isStyle,
                    'commonAttribute' => null,
                    'attributeName' => "trading-currency-price",
                    'value' => $pricesArray['supplier']['price'],
                    'fromDate' => null,
                    'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                    'optionCode' => $optionCode
                ]);
                $historicalEntriesCollection->add($historicalEntry);
            }
        }
        $this->historicalService->recordChanges($historicalEntriesCollection);
    }

    /**
     *
     * @param string $currencyCode
     * @return mixed
     */
    private function getCurrencyIdFromCode($currencyCode)
    {
        $this->_sql->setTable($this->_currencyTable);
        $select = $this->_sql->select();
        $select->where('isoCode = "' . $currencyCode . '"');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $currencyDetails = $statement->execute()->next();
        return $currencyDetails['currencyId'];
    }

    /**
     *
     * @param $territory
     * @return bool
     */
    public function getTerritoryId($territory)
    {
        $this->_sql->setTable($this->territoriesTable);
        $select = $this->_sql->select();
        $select->columns(['territoryId']);
        $select->where('iso2Code = "' . $territory . '"');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $territory = $statement->execute()->next();
        return ! empty($territory['territoryId']) ? $territory['territoryId'] : false;
    }

    /**
     *
     * @param int $productIdOrVariantId
     * @param string $productIdFrom
     * @return int
     */
    private function removeOldPricesFor($productIdOrVariantId, $productIdFrom = 'PRODUCTS')
    {
        /**
         * Remove normal prices
         */
        $update = $this->_sql->update();
        $update->table($this->_productsPricesTable);
        $update->set(array(
            'status' => 'DELETED'
        ));
        $update->where(array(
            'productIdFrom' => $productIdFrom,
            'productOrVariantId' => $productIdOrVariantId
        ));
        $this->_sql->prepareStatementForSqlObject($update)->execute();

        /**
         * Remove supplier prices
         */
        $this->_sql->setTable($this->_productSupplierPriceTable);
        $action = $this->_sql->delete()->where(array(
            'productIdFrom' => $productIdFrom,
            'productOrVariantId' => $productIdOrVariantId
        ));
        $this->_sql->prepareStatementForSqlObject($action)->execute();
    }

    /**
     *
     * @param $formData
     * @return array
     */
    private function formatSubmitedArray($formData)
    {
        $inputArray = array();
        foreach ($formData as $inputFieldName => $inputFieldValue) {
            /**
             * Check if the key is a input type field *
             */
            if (strpos($inputFieldName, 'inputfield[') !== false && strpos($inputFieldName, 'button-') === false) {
                $inputFieldId = str_replace('inputfield[', '', $inputFieldName);
                $inputFieldId = str_replace(']', '', $inputFieldId);
                $inputFieldEntity = $this->inputService->getInputField($inputFieldId);
                if (! is_null($inputFieldEntity->getInputTableFieldsInputTableFieldId())) {
                    $table = $inputFieldEntity->getInputTableFieldsInputTableFieldId()->getTable();
                    $column = $inputFieldEntity->getInputTableFieldsInputTableFieldId()->getColumn();
                    $inputArray['isTableField'][$table][$column] = array(
                        'entity' => $inputFieldEntity,
                        'value' => $inputFieldValue
                    );
                } else {
                    $inputArray['isAttribute'][] = array(
                        'entity' => $inputFieldEntity,
                        'value' => $inputFieldValue
                    );
                }
            } elseif (strpos($inputFieldName, 'prices[') !== false) {
                $priceValue = $inputFieldValue;
                /**
                 * Check if data is a price
                 */
                $priceDetails = str_replace('prices[', '', $inputFieldName);
                $priceDetails = str_replace(']', '', $priceDetails);
                /**
                 * Save the currency prices separate
                 */
                if ($priceDetails == 'supplier-currency' || $priceDetails == 'supplier-price') {
                    $inputArray['prices']['supplier'][str_replace('supplier-', '', $priceDetails)] = $priceValue;
                } else {
                    /**
                     * Group the prices based on their type, currency and teriroty
                     */
                    if (strpos($priceDetails, 'costPrice') !== false) {
                        /**
                         * Group Prices based on currency
                         */
                        $priceCurrency = str_replace('costPrice-', '', $priceDetails);
                        $inputArray['prices']['costPrice'][$priceCurrency] = $priceValue;
                    } elseif (strpos($priceDetails, 'nowPrice') !== false) {
                        /**
                         * Group Prices based on territory and currency
                         */
                        $priceTerritoryAndCurrency = str_replace('nowPrice-', '', $priceDetails);
                        $pricesData = explode('-', $priceTerritoryAndCurrency);
                        $currency = $pricesData[0];
                        $territory = $pricesData[1];
                        $inputArray['prices']['nowPrice'][$currency][$territory] = $priceValue;
                    } elseif (strpos($priceDetails, 'wasPrice') !== false) {
                        /**
                         * Group Prices based on territory and currency
                         */
                        $priceTerritoryAndCurrency = str_replace('wasPrice-', '', $priceDetails);
                        $pricesData = explode('-', $priceTerritoryAndCurrency);
                        $currency = $pricesData[0];
                        $territory = $pricesData[1];
                        $inputArray['prices']['wasPrice'][$currency][$territory] = $priceValue;
                    }
                }
            }
        }
        return $inputArray;
    }

    /**
     *
     * @param int $dynamicField
     * @param $allOptions
     * @return array
     */
    public function formatArrayForAllOptions($dynamicField, $allOptions)
    {
        $inputFields = array();
        $optionsWithValues = $this->getValuesForUpdatedFields(array(
            'inputfield' => array(
                $dynamicField['inputFieldId']->getInputFieldId() => ''
            )
        ), $allOptions, false, true);

        foreach ($optionsWithValues as $optionCode => $values) {
            $selectedValues = [];

            if (! empty($values[0]['oldValue'])) {
                $selectedValues = $values[0]['oldValue'];
            } else {
                if (isset($dynamicField['values'])) {
                    foreach ($dynamicField['values'] as $key => $dynamicOptions) {
                        $check = $this->commonAttributesValuesTable->getIsDefaultValue($key);

                        if ($check) {
                            $selectedValues = [
                                $key => 1
                            ];
                            break 1;
                        }
                    }
                }
            }
            $optionArray = array(
                'type' => $dynamicField['type'],
                'label' => $dynamicField['label'],
                'entity' => $dynamicField['inputFieldId'],
                'selected-values' => $selectedValues,
                'all-values' => isset($dynamicField['values']) ? $dynamicField['values'] : null
            );
            $inputFields[$optionCode] = $optionArray;
        }
        return $inputFields;
    }

    /**
     * @param $dynamicField
     * @param $style
     * @return array
     */
    public function formatArrayForStyle($dynamicField, $style)
    {
        $inputFields = array();
        $productDetails = $this->getMainProductDetailsByStyle($style);
        $product[$style] = $productDetails;

        $inputFields = $this->formatArrayForAllOptions($dynamicField, $product);

        return $inputFields;
    }

    /**
     *
     * @param string $style
     * @return mixed
     */
    public function getCommonAttributesUsedForStyle($style)
    {
        $productData = $this->getProductDataForStyle($style);
        return $productData['commonAttributes'];
    }

    /**
     *
     * @param  $productId
     * @param  $style
     * @param  $submitedFormData
     * @param  $prices
     * @return bool
     */
    public function createNewOptionForProduct($productId, $style, $submitedFormData, $prices = array(), $supplierPrice = array())
    {
        /**
         * Retrieve selected colour d
         * etails
         */
        $colourDetails = $this->coloursMapper->fetchAllColourGroups($this->_currentLanguage, $submitedFormData['option-creation-colour']);
        $errorArray = array();
        $variants = array();
        $historicalEntries = [];

        /**
         * Loop all sizes selected to create new SKU
         */
        foreach ($submitedFormData['option-creation-size'] as $sizeIds => $size) {
            try {
                $this->_dbAdapter->getDriver()
                    ->getConnection()
                    ->beginTransaction();
                // TODO: CHECK IF SKU HAS BEEN ALREADY CREATED
                $sizeIds = explode('-', $sizeIds);

                $commonAttributeValueId = $sizeIds[1];
                $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueBy(array(
                    'commonAttributeValueId' => $commonAttributeValueId
                ));
                $sizeSkuCode = $commonAttributeValueEntity->getCommonAttributeValueSkuCode();
                $skuSeparator = isset($this->clientConfig['settings']['skuGenerationSeparator']) ? $this->clientConfig['settings']['skuGenerationSeparator'] : '';
                $skuCode = strtoupper($style . $skuSeparator .  $colourDetails['groupCode'] . $skuSeparator . $sizeSkuCode);

                if ($this->checkIfSkuIsUnique($skuCode)) {
                    $errorArray[] = $skuCode . ' is already created';
                    $this->_dbAdapter->getDriver()
                        ->getConnection()
                        ->rollback();
                    continue;
                }
                $productDetails = $this->retrieveProductDetailsToCreateNewSku($productId);

                /**
                 * Create new SKU
                 */
                $variantId = $this->createProductsRAttributesEntry($productId, $skuCode, $productDetails['productData']);
                /**
                 * Create Products Definitions Entry
                 */
                $this->createProductsDefinitionEntry($variantId, $productDetails['productData']);
                /**
                 * Create Common Attributes entries
                 */
                $this->createCommonAttributeRelations($variantId, $productDetails['commonAttributes']);
                /**
                 * Create Attributes Groups
                 */
                $this->createAttributeGroupsRelations($variantId, $productDetails['attributesGroups']);
                /**
                 * Create products r attributes r sku rules
                 */
                $this->skuRuleAttributesEntry($variantId, $commonAttributeValueEntity, $colourDetails);

                /**
                 * Create prices for SKU
                 */
                $this->createPriceForSKU($variantId, $prices);
                /**
                 * Create supplier Price
                 */
                if(!empty($supplierPrice)){
                    $this->saveProductSupplierPrice($variantId, $supplierPrice['price'], $supplierPrice['currencyCode'], 'PRODUCTS_R_ATTRIBUTES');
                }

                $variants[] = $variantId;

                $this->_dbAdapter->getDriver()
                    ->getConnection()
                    ->commit();

                $historicalEntries['prices'][] = ['variantId' => $variantId, 'prices' => $prices];
                $historicalEntries['supplierPrices'][] = ['variantId' => $variantId, 'supplierPrice' => $supplierPrice];

            } catch (\Exception $ex) {
                $this->_dbAdapter->getDriver()
                    ->getConnection()
                    ->rollback();
                $errorArray[] = $ex->getMessage();
                continue;
            }
        }
        $this->addHistoricalPricesEntries($historicalEntries);
        return array(
            'errors' => $errorArray,
            'optionCode' => $style . $skuSeparator .$colourDetails['groupCode']
        );
    }

    /**
     * @param $historicalEntries
     */
    public function addHistoricalPricesEntries($historicalEntries)
    {
        $historicalPricesCollection = new ArrayCollection();

        if (!empty($historicalEntries['prices'])) {
            foreach ($historicalEntries['prices'] as $prices) {
                $variantId = $prices['variantId'];
                if (!empty($prices['prices'])) {
                    foreach ($prices['prices'] as $priceData) {
                        $region = $this->getTerritoryCodeById($priceData['regionId']);
                        $regionCode = isset($region['iso2Code']) ? $region['iso2Code'] : '';

                        $currency = $this->getCurrencyCodeById($priceData['currencyId']);
                        $currencyCode = isset($currency['isoCode']) ? $currency['isoCode'] : '';

                        $optionCode = $this->getOptionCodeForVariantId($variantId);

                        if (($regionCode == 'GB' && $currencyCode == 'GBP') ||
                            ($regionCode == 'US' && $currencyCode == 'USD') ||
                            ($regionCode == 'CA' && $currencyCode == 'CAD')
                        ) {
                            /** LANDED COST */
                            $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                                'productOrVariantId' => $variantId,
                                'isStyle' => false,
                                'commonAttribute' => null,
                                'attributeName' => "landed-cost-{$currencyCode}",
                                'value' => $priceData['costPrice'],
                                'fromDate' => null,
                                'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                'optionCode' => $optionCode
                            ]);
                            $historicalPricesCollection->add($collectionItem);
                        }
                        $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                            'productOrVariantId' => $variantId,
                            'isStyle' => false,
                            'commonAttribute' => null,
                            'attributeName' => "current-price-{$regionCode}-{$currencyCode}",
                            'value' => $priceData['nowPrice'],
                            'fromDate' => null,
                            'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                            'optionCode' => $optionCode
                        ]);
                        $historicalPricesCollection->add($collectionItem);
                        $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                            'productOrVariantId' => $variantId,
                            'isStyle' => false,
                            'commonAttribute' => null,
                            'attributeName' => "original-price-{$regionCode}-{$currencyCode}",
                            'value' => $priceData['wasPrice'],
                            'fromDate' => null,
                            'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                            'optionCode' => $optionCode
                        ]);
                        $historicalPricesCollection->add($collectionItem);
                    }
                }

            }
        }

        if (!empty($historicalEntries['supplierPrices'])) {
            foreach ($historicalEntries['supplierPrices'] as $variantData) {
                $variantId = $variantData['variantId'];

                $supplierPriceCode = isset($variantData['supplierPrice']['currencyCode']) ? $variantData['supplierPrice']['currencyCode'] : false;
                $supplierPriceValue = isset($variantData['supplierPrice']['price']) ? $variantData['supplierPrice']['price'] : false;

                $optionCode = $this->getOptionCodeForVariantId($variantId);

                if ($supplierPriceValue !== false) {
                    $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                        'productOrVariantId' => $variantId,
                        'isStyle' => false,
                        'commonAttribute' => null,
                        'attributeName' => "trading-currency-price",
                        'value' => $supplierPriceValue,
                        'fromDate' => null,
                        'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                        'optionCode' => $optionCode
                    ]);
                    $historicalPricesCollection->add($collectionItem);
                }

                if ($supplierPriceCode !== false) {
                    $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                        'productOrVariantId' => $variantId,
                        'isStyle' => false,
                        'commonAttribute' => null,
                        'attributeName' => "trading-currency",
                        'value' => $supplierPriceCode,
                        'fromDate' => null,
                        'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                        'optionCode' => $optionCode
                    ]);
                    $historicalPricesCollection->add($collectionItem);
                }
            }
        }
        $this->historicalService->recordChanges($historicalPricesCollection);
    }
    /**
     *
     * @param int  $variantId
     * @param array $prices
     */
    public function createPriceForSKU($variantId, $prices)
    {
        foreach ($prices as $price) {
            $insertArray = array(
                'productIdFrom' => "PRODUCTS_R_ATTRIBUTES",
                'productOrVariantId' => $variantId,
                'regionId' => $price['regionId'],
                'currencyId' => $price['currencyId'],
                'price' => $price['price'],
                'offerPrice' => $price['offerPrice'],
                'nowPrice' => $price['nowPrice'],
                'wasPrice' => $price['wasPrice'],
                'costPrice' => $price['costPrice'],
                'status' => 'ACTIVE'
            );

            $this->_sql->setTable($this->_productsPricesTable);
            $action = $this->_sql->insert();
            $action->values($insertArray);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        }
    }


    /**
     * @var string $currencyCode
     */
    public function getCurrencyIdByCurrencyCode($currencyCode)
    {
        $sql = new Sql($this->_dbAdapter);
        $sql->setTable($this->_currencyTable);
        $select = $sql->select();
        $select->where(["isoCode" => $currencyCode]);

        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute()->next()['currencyId'];
    }

    /**
     * @var string $currencyId
     */
    public function getRegionByCurrencyId($currencyId)
    {
        $sql = new Sql($this->_dbAdapter);
        $sql->setTable($this->territoriesRCurrenciesTable);
        $select = $sql->select();
        $select->where(["currencyId" => $currencyId]);

        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute()->next()['territoryId'];
    }

    /**
     * Get ISO CODE from SKUGROUP
     * @param string $skuGroup
     */
    public function getSelectedCurrencyBySKUGroup($skuGroup)
    {
        // get productAttributeId
        $sql = new Sql($this->_dbAdapter);
        $sql->setTable("PRODUCTS_R_ATTRIBUTES");
        $select = $sql->select();
        $select->where
            ->like('sku', "$skuGroup%");

        $statement = $sql->prepareStatementForSqlObject($select);
        $productAttributeId = $statement->execute()->next()['productAttributeId'];

        // get isoCode/currency code
        $sql = new Sql($this->_dbAdapter);
        $sql->setTable($this->_productSupplierPriceTable);
        $select = $sql->select();
        $select->where([
            'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
            'productOrVariantId' => $productAttributeId,
        ]);

        $statement = $sql->prepareStatementForSqlObject($select);
        $currencyCode =  $statement->execute()->next()['currencyCode'];

        if (empty($currencyCode)) {
            $sql = new Sql($this->_dbAdapter);
            $sql->setTable("PRODUCTS_Prices");
            $select = $sql->select();
            $select->where([
                'PRODUCTS_Prices.productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                'PRODUCTS_Prices.productOrVariantId' => $productAttributeId,
            ]);
            $select->join('CURRENCY', 'PRODUCTS_Prices.currencyId = CURRENCY.currencyId');

            $statement = $sql->prepareStatementForSqlObject($select);
            return $statement->execute()->next()['isoCode'];
        }

        return $currencyCode;
    }

    /**
     *
     * @param int $productOrVariantId
     * @param string $productIdFrom
     * @param int $commonAttributeId
     */
    public function getCommonAttributesUsedForProduct($productOrVariantId, $productIdFrom, $commonAttributeId)
    {
        $this->_sql->setTable($this->productsCommonAttributesTable);
        $select = $this->_sql->select()->where(array(
            'productOrVariantId' => $productOrVariantId,
            'productIdFrom' => $productIdFrom,
            'commonAttributeId' => $commonAttributeId
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        return $statement->execute()->next();
    }

    /**
     * @param        $variantId
     * @param        $productDetails
     * @param bool   $isVariant
     */
    public function createProductsDefinitionEntry($variantId, $productDetails, $isVariant = true)
    {
        $productDefinitionData = [
            'productId'          => $variantId,
            'productIdFrom'      => $isVariant ? $this->_productsVariantsRelationshipTable : $this->_tableName,
            'languageId'         => $this->_currentLanguage,
            'versionId'          => 1,
            'versionDescription' => 'default',
            'shortProductName'   => $productDetails['shortProductName'],
            'productName'        => $productDetails['productName'],
            'urlName'            => '',
            'metaTitle'          => '',
            'metaDescription'    => '',
            'metaKeyword'        => '',
            'shortDescription'   => isset($productDetails['shortDescription']) ? $productDetails['shortDescription'] : '',
            'longDescription'    => isset($productDetails['longDescription']) ? $productDetails['longDescription'] : '',
            'misSpells'          => '',
            'tags'               => '',
            'status'             => isset($productDetails['status']) ? $productDetails['status'] : 'INACTIVE',
            'created'            => date('Y-m-d H:i:s'),
            'modified'           => date('Y-m-d H:i:s'),
        ];
        $this->_sql->setTable($this->_productsDefinitionsTable);
        $action = $this->_sql->insert();
        $action->values($productDefinitionData);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     *
     * @param
     *            $variantId
     * @param
     *            $commonAttributeValueEntity
     * @param
     *            $colourDetails
     */
    private function skuRuleAttributesEntry($variantId, $commonAttributeValueEntity, $colourDetails)
    {
        $insertCommonAttribute = array(
            'productAttributeId' => $variantId,
            'skuRuleId' => $commonAttributeValueEntity->getCommonAttributeValueId(),
            'skuRuleTable' => 'COMMON_ATTRIBUTES_VALUES',
            'isCommonAttribute' => '1',
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        );
        $this->_sql->setTable($this->_productsAttributesSkuRuleRelationsTable);
        $action = $this->_sql->insert();
        $action->values($insertCommonAttribute);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();

        $insertColourAttribute = array(
            'productAttributeId' => $variantId,
            'skuRuleId' => $colourDetails['colourGroupId'],
            'skuRuleTable' => 'COLOURS_Groups',
            'isCommonAttribute' => '0',
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        );
        $this->_sql->setTable($this->_productsAttributesSkuRuleRelationsTable);
        $action = $this->_sql->insert();
        $action->values($insertColourAttribute);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     *
     * @param
     *            $variantId
     * @param
     *            $attributesGroups
     */
    private function createAttributeGroupsRelations($variantId, $attributesGroups)
    {
        foreach ($attributesGroups as $attributesGroup) {
            $insertArray = array(
                'attributeGroupId' => $attributesGroup['attributeGroupId'],
                'productOrVariantId' => $variantId,
                'attributeValue' => $attributesGroup['groupType'] == 'TEXT' ? '' : $attributesGroup['attributeValue'],
                'isVariant' => 1,
                'groupType' => $attributesGroup['groupType'],
                'status' => $attributesGroup['status'],
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            );
            $this->_sql->setTable($this->_productsAttributesCombinationTable);
            $action = $this->_sql->insert();
            $action->values($insertArray);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();

            $productAttributeCombinationId = $this->_dbAdapter->getDriver()
                ->getConnection()
                ->getLastGeneratedValue();

            if ($attributesGroup['groupType'] == 'TEXT') {
                /**
                 * Retrieve products combination definitions
                 */
                $this->_sql->setTable(array(
                    'pacd' => $this->_productsAttributesCombinationDefinitions
                ));
                $select = $this->_sql->select();
                $select->where("pacd.productAttributeCombinationId = " . $attributesGroup['productAttributeCombinationId']);
                $statement = $this->_sql->prepareStatementForSqlObject($select);
                $productsAttributesCombinationDefData = $statement->execute()->next();
                /**
                 * Insert products combination definitions
                 */
                $insertArray = array(
                    'productAttributeCombinationId' => $productAttributeCombinationId,
                    'attributeValue' => $productsAttributesCombinationDefData['attributeValue'],
                    'languageId' => $productsAttributesCombinationDefData['languageId'],
                    'status' => $productsAttributesCombinationDefData['status'],
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                );
                $this->_sql->setTable($this->_productsAttributesCombinationDefinitions);
                $action = $this->_sql->insert();
                $action->values($insertArray);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
            }
        }
    }

    /**
     *
     * @param
     *            $variantId
     * @param
     *            $commonAttributes
     */
    private function createCommonAttributeRelations($variantId, $commonAttributes)
    {
        $historicalCollection = new ArrayCollection();
        $optionCode = $this->getOptionCodeForVariantId($variantId);
        foreach ($commonAttributes as $commonAttribute) {

            $insertArray = array(
                'productOrVariantId' => $variantId,
                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                'commonAttributeId' => $commonAttribute[0]['commonAttributeId'],
                'commonAttributeValue' => $commonAttribute[0]['commonAttributeValue'],
                'modified' => date('Y-m-d H:i:s'),
                'created' => date('Y-m-d H:i:s')
            );
            $this->_sql->setTable($this->productsCommonAttributesTable);
            $action = $this->_sql->insert();
            $action->values($insertArray);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();

            $commonAttributeEntity = $this->commonAttributesService->getCommonAttribute($commonAttribute[0]['commonAttributeId']);

            if ($commonAttributeEntity->isKeepHistory()) {
                $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                    'productOrVariantId' => $variantId,
                    'isStyle' => false,
                    'commonAttribute' => $commonAttributeEntity,
                    'attributeName' => '',
                    'value' => $commonAttribute[0]['commonAttributeValue'],
                    'fromDate' => null,
                    'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                    'optionCode' => $optionCode
                ]);
                $historicalCollection->add($historyEntity);
            }
        }
        $this->historicalService->recordChanges($historicalCollection);

    }

    /**
     *
     * @param $productId
     * @param $skuCode
     * @param $productDetails
     * @return int
     * @throws \Exception
     * @internal param $ $skuCode*            $skuCode
     * @internal param $ $productDetails*            $productDetails
     */
    private function createProductsRAttributesEntry($productId, $skuCode, $productDetails)
    {
        $barcode = $this->getNextAvailableBarcodeForProduct();

        /** Check if custom barcodes option is enabled and let the product be created without any valid barcodes */
        $customBarcodes = isset($this->clientConfig['settings']['customBarcodes']) ? $this->clientConfig['settings']['customBarcodes'] : false;
        if ($customBarcodes && empty($barcode)) {
            $barcode = '';
        }

        if(empty($barcode) && !$customBarcodes){
            throw new \Exception('No barcodes are available.');
        }

        $insertData = array(
            'productId' => $productId,
            'manufacturerId' => $productDetails['manufacturerId'],
            'taxId' => '0',
            'quantity' => $productDetails['quantity'],
            'ean13' => $barcode,
            'ecoTax' => $productDetails['ecotax'],
            'sku' => strtoupper($skuCode),
            'supplierReference' => $productDetails['supplierReference'],
            'status' => $productDetails['status'],
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        );

        $this->_sql->setTable($this->_productsVariantsRelationshipTable);
        $action = $this->_sql->insert();
        $action->values($insertData);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
        $variantId = $this->_dbAdapter->getDriver()
            ->getConnection()
            ->getLastGeneratedValue();

        if (!empty($barcode)) {
            $this->updateBarcodeAsUsed($barcode);
        }

        return $variantId;
    }

    /**
     *
     * @param
     *            $barcode
     * @return bool
     */
    private function updateBarcodeAsUsed($barcode)
    {
        $update = $this->_sql->update();
        $update->table($this->_productBarcodesTable);
        $update->set(array(
            'inUse' => '1q'
        ));
        $update->where(array(
            'barcode' => $barcode
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $result = $statement->execute();
        if ($result->getAffectedRows() > 0) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param int $productId
     * @return array
     */
    private function retrieveProductDetailsToCreateNewSku($productId)
    {
        $commonAttributesUsed = $this->getCommonAttributesFor($productId);
        $attributesGroupsUsed = $this->getAttributesFor($productId, 0);
        $productData = $this->getProductDetailsById($productId, $this->_currentLanguage);
        $prices = array();

        return array(
            'commonAttributes' => $commonAttributesUsed,
            'attributesGroups' => $attributesGroupsUsed,
            'productData' => $productData[key($productData)],
            'price' => array()
        );
    }

    /**
     * @param string $style
     * @param bool $oneVariant
     * @return array
     * @throws \Exception
     */
    public function getAllOptionsForStyle($style, $oneVariant = false)
    {
        $productDetails = $this->getMainProductDetailsByStyle($style);

        /**
         * Get all variants for product
         */
        $this->_sql->setTable(array(
            'pra' => $this->_productsVariantsRelationshipTable
        ));
        $select = $this->_sql->select();
        $select->join(array(
            'pd' => $this->_productsDefinitionsTable
        ), new Expression('pra.productAttributeId = pd.productId
                    AND pd.productIdFrom = "PRODUCTS_R_ATTRIBUTES" AND `pd`.status != "DELETED" AND languageId = ' . $this->_currentLanguage),
            array(
                'productDefinitionsId',
                'productId',
                'productIdFrom',
                'languageId',
                'versionId',
                'versionDescription',
                'shortProductName',
                'productName',
                'urlName',
                'metaTitle',
                'metaDescription',
                'metaKeyword',
                'shortDescription',
                'longDescription',
                'misSpells',
                'tags',
                'statusDefinition'=>'status',
                'makeLive',
                'createdDefinition' => 'created',
                'modifiedDefinition' => 'modified',

        ), Select::JOIN_LEFT);

        $select->join(array(
            'p' => $this->_tableName
        ), new Expression('pra.productId = p.productId'), array(
            'style' => 'style'
        ), Select::JOIN_LEFT);

        $select->where(array(
            'pra.productId' => $productDetails['productId']
        ));
        $select->where('pra.status != "DELETE"');

        //echo $this->_sql->getSqlstringForSqlObject($select);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $returnArray = array();

        foreach ($results as $variant) {
            /**
             * Retrieve colour code used
             */
            $this->_sql->setTable(array(
                'prarkr' => $this->_productsAttributesSkuRuleRelationsTable
            ));
            $select = $this->_sql->select();
            $select->where(array(
                'productAttributeId' => $variant['productAttributeId'],
                'skuRuleTable' => 'COLOURS_Groups'
            ));
            //echo $this->_sql->getSqlstringForSqlObject($select);
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $row = $statement->execute()->next();
            if (! empty($row['skuRuleId'])) {
                $attributeGroupDetails = $this->coloursMapper->getColourGroupDetails($row['skuRuleId'], $this->_currentLanguage);

                if (! empty($attributeGroupDetails['groupCode'])) {
                    // $optionCode = str_pad($style,6,' ',STR_PAD_RIGHT) . $attributeGroupDetails['groupCode'];

                    $optionCode = $style . $this->clientConfig['settings']['skuGenerationSeparator'] . $attributeGroupDetails['groupCode'];

                    $productDetails = $this->getVariantDetailsByStyle($optionCode);

                    if (empty($productDetails)) {
                        $optionCode = str_replace($this->clientConfig['settings']['skuGenerationSeparator'], '',$optionCode);
                        $productDetails = $this->getVariantDetailsByStyle($optionCode);
                        if (empty($productDetails)) {
                            /** Option Code = SKU  */
                            $optionCode = $variant['sku'];
                            $productDetails = $this->getVariantDetailsByStyle($optionCode);
                        }
                        if (empty($productDetails)) {
                            throw new \Exception('Could not find valid option Code.');
                        }
                    }

                    /**
                     * Set up colour code
                     */
                    $variant['colourCode'] = $attributeGroupDetails['groupCode'];
                    $variant['colourName'] = $attributeGroupDetails['name'];

                    /**
                     * Get the size code
                     */
                    $this->_sql->setTable(array(
                        'prarkr' => $this->_productsAttributesSkuRuleRelationsTable
                    ));
                    $select = $this->_sql->select();
                    $select->where(array(
                        'productAttributeId' => $variant['productAttributeId'],
                        'skuRuleTable' => 'COMMON_ATTRIBUTES_VALUES'
                    ));
                    $select->join(array(
                        'cavd' => $this->commonAttributeValuesDefinitionsTable
                    ), new Expression('prarkr.skuRuleId = cavd.commonAttributeValueId AND cavd.languageId = ' . $this->_currentLanguage), array(
                        '*'
                    ), Select::JOIN_LEFT);

                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $row = $statement->execute()->next();
                    if (! empty($row['skuRuleId'])) {
                        /**
                         * Set up size code
                         */
                        $variant['sizeCode'] = $row['value'];
                    }
                    if ($oneVariant) {
                        $returnArray[$optionCode] = $variant;
                    } else {
                        $returnArray[$optionCode][] = $variant;
                    }
                }
            } else {
                /** Variant does not have a colour or size */
                /** We need to display the sku instead of the variant */
                if ($oneVariant) {
                    $returnArray[$variant['sku']] = $variant;
                } else {
                    $returnArray[$variant['sku']][] = $variant;
                }
            }
        }
        return $returnArray;
    }

    /**
     *
     * @param $option
     * @param $formData
     * @param bool|false $updatePricesFlag
     * @return bool
     */
    public function updateOption($option, $formData, $updatePricesFlag = false, $savedFromOptionLevel = false)
    {
        $inputArray = $this->formatSubmitedArray($formData);
        $productDetails = $this->getVariantDetailsByStyle($option, false);

        if (empty($productDetails)) {
            $option = str_replace(' ', '', $option);
            $productDetails = $this->getVariantDetailsByStyle($option, false);
        }
        if (empty($productDetails)) {
            return false;
        }

        $variants = array();
        foreach ($productDetails as $variant) {
            /**
             * Update table fields
             */
            if (! empty($inputArray['isTableField'])) {
                foreach ($inputArray['isTableField'] as $tableName => $fields) {
                    /**
                     * Retrieve all values for the fields
                     */
                    $updateArray = array();
                    /**
                     * Loop all values and make sure they are editable values
                     */
                    foreach ($fields as $databaseField => $value) {
                        if ($value['entity']->isEditable() && (! $savedFromOptionLevel || ($savedFromOptionLevel && $value['entity']->getEditableAtLevel() == 'OPTION'))) {
                            $updateArray[$databaseField] = $value['value'];
                        }
                    }
                    /**
                     * Save the new values to the $tableName table
                     */
                    if (! empty($updateArray)) {
                        /**
                         * Retrieve primary key of the table
                         */
                        $sql = "SHOW KEYS FROM " . $tableName . " WHERE Key_name = 'PRIMARY'";
                        $statement = $this->_dbAdapter->query($sql);
                        $primaryKey = $statement->execute()->next();
                        /**
                         * Check if the primary key is in the product data array
                         */
                        if (! empty($primaryKey['Column_name']) && ! empty($variant[$primaryKey['Column_name']])) {
                            /**
                             * Build the where clause
                             */
                            $primaryColumn = $primaryKey['Column_name'];
                            $primaryValue = $variant[$primaryKey['Column_name']];
                            $whereClause = array(
                                $primaryColumn => $primaryValue
                            );
                            /**
                             * Update table $tableName with $updateArray data using $whereClause
                             */
                            $update = $this->_sql->update();
                            $update->table($tableName);
                            $update->set($updateArray);
                            $update->where($whereClause);
                            $statement = $this->_sql->prepareStatementForSqlObject($update);
                            $statement->execute();
                        }
                    }
                }
            }

            if (! empty($inputArray['isAttribute'])) {
                /**
                 * Update input attribute fields
                 */
                foreach ($inputArray['isAttribute'] as $inputAttributeField) {
                    if (! $savedFromOptionLevel || ($savedFromOptionLevel && $inputAttributeField['entity']->getEditableAtLevel() == 'OPTION')) {
                        $attributeTable = $inputAttributeField['entity']->getInputFieldRAttributesInputFieldRAttributeId()->getAttributeTable();
                        switch ($attributeTable) {
                            case 'COMMON_ATTRIBUTES':
                                /**
                                 * Build where clause
                                 */
                                /** @var InputFieldRAttributes $entity */
                                $entity =  $inputAttributeField['entity']->getinputFieldRAttributesInputfieldrattributeid();
                                $commonAttributeId = $entity->getcommonAttributeId();
                                $commonAttributeEntity = $entity->getCommonAttributes();

                                $whereClause = array(
                                    'commonAttributeId' => $commonAttributeId,
                                    'productOrVariantId' => $variant['productAttributeId'],
                                    'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES'
                                );

                                $this->_sql->setTable($this->productsCommonAttributesTable);
                                $select = $this->_sql->select();
                                $select->where($whereClause);
                                $statement = $this->_sql->prepareStatementForSqlObject($select);
                                $count = $statement->execute()->count();

                                if ($count > 0) {
                                    /**
                                     * Need to update
                                     */
                                    if (is_array($inputAttributeField['value'])) {
                                        /**
                                         * Need to delete all previews values for multi-select
                                         */
                                        $this->_sql->setTable($this->productsCommonAttributesTable);
                                        $action = $this->_sql->delete()->where($whereClause);
                                        $this->_sql->prepareStatementForSqlObject($action)->execute();
                                        /**
                                         * Need to insert new values ( multi-select )
                                         */
                                        foreach ($inputAttributeField['value'] as $inputFieldValue) {
                                            $this->_sql->setTable($this->productsCommonAttributesTable);
                                            $action = $this->_sql->insert();
                                            $action->values(array(
                                                'productOrVariantId' => $variant['productAttributeId'],
                                                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                                                'commonAttributeId' => $commonAttributeId,
                                                'commonAttributeValue' => $inputFieldValue
                                            ));
                                            $statement = $this->_sql->prepareStatementForSqlObject($action);
                                            $statement->execute();
                                            $this->loggingService->logUpdate(null, $option, $commonAttributeId, $inputFieldValue);

                                            if ($commonAttributeEntity->isKeepHistory()) {
                                                $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                                    'productOrVariantId' => $variant['productAttributeId'],
                                                    'isStyle' => false,
                                                    'commonAttribute' => $commonAttributeEntity,
                                                    'attributeName' => '',
                                                    'value' => $inputFieldValue,
                                                    'fromDate' => null,
                                                    'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                                    'optionCode' => $option
                                                ]);
                                                $historicalCollection = new ArrayCollection([$historyEntity]);
                                                $this->historicalService->recordChanges($historicalCollection);
                                            }
                                        }
                                    } else {
                                        /** Check if history is meant to be kept and also
                                         * if the current value has changed from the old value. */
                                        if ($commonAttributeEntity->isKeepHistory() &&
                                            $this->areValuesChanged(
                                                $variant['productAttributeId'],
                                                'PRODUCTS_R_ATTRIBUTES',
                                                $inputAttributeField['value'],
                                                $commonAttributeId,
                                                'Common_Attributes'
                                            )
                                        ) {
                                            $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                                'productOrVariantId' => $variant['productAttributeId'],
                                                'isStyle' => false,
                                                'commonAttribute' => $commonAttributeEntity,
                                                'attributeName' => '',
                                                'value' => $inputAttributeField['value'],
                                                'fromDate' => null,
                                                'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                                'optionCode' => $option
                                            ]);
                                            $historicalCollection = new ArrayCollection([$historyEntity]);
                                            $this->historicalService->recordChanges($historicalCollection);
                                        }

                                        $update = $this->_sql->update();
                                        $update->table($this->productsCommonAttributesTable);
                                        $update->set(array(
                                            'commonAttributeValue' => $inputAttributeField['value']
                                        ));
                                        $update->where($whereClause);
                                        $statement = $this->_sql->prepareStatementForSqlObject($update);
                                        $statement->execute();
                                        $this->loggingService->logUpdate(null, $option, $commonAttributeId, $inputAttributeField['value']);
                                    }
                                } else {
                                    /**
                                     * Need to insert
                                     */
                                    if (is_array($inputAttributeField['value'])) {
                                        /**
                                         * Need to insert all values ( multi-select )
                                         */
                                        foreach ($inputAttributeField['value'] as $inputFieldValue) {
                                            $this->_sql->setTable($this->productsCommonAttributesTable);
                                            $action = $this->_sql->insert();
                                            $action->values(array(
                                                'productOrVariantId' => $variant['productAttributeId'],
                                                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                                                'commonAttributeId' => $commonAttributeId,
                                                'commonAttributeValue' => $inputFieldValue
                                            ));
                                            $statement = $this->_sql->prepareStatementForSqlObject($action);
                                            $statement->execute();
                                            $this->loggingService->logUpdate(null, $option, $commonAttributeId, $inputFieldValue);
                                            /** Check if history is meant to be kept. */
                                            if ($commonAttributeEntity->isKeepHistory()) {
                                                $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                                    'productOrVariantId' => $variant['productAttributeId'],
                                                    'isStyle' => false,
                                                    'commonAttribute' => $commonAttributeEntity,
                                                    'attributeName' => '',
                                                    'value' => $inputFieldValue,
                                                    'fromDate' => null,
                                                    'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                                    'optionCode' => $option
                                                ]);
                                                $historicalCollection = new ArrayCollection([$historyEntity]);
                                                $this->historicalService->recordChanges($historicalCollection);
                                            }
                                        }
                                    } else {
                                        $this->_sql->setTable($this->productsCommonAttributesTable);
                                        $action = $this->_sql->insert();
                                        $action->values(array(
                                            'productOrVariantId' => $variant['productAttributeId'],
                                            'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                                            'commonAttributeId' => $commonAttributeId,
                                            'commonAttributeValue' => $inputAttributeField['value']
                                        ));
                                        $statement = $this->_sql->prepareStatementForSqlObject($action);
                                        $statement->execute();
                                        $this->loggingService->logUpdate(null, $option, $commonAttributeId, $inputAttributeField['value']);

                                        if ($commonAttributeEntity->isKeepHistory()) {
                                            $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                                'productOrVariantId' => $variant['productAttributeId'],
                                                'isStyle' => false,
                                                'commonAttribute' => $commonAttributeEntity,
                                                'attributeName' => '',
                                                'value' => $inputAttributeField['value'],
                                                'fromDate' => null,
                                                'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                                'optionCode' => $option
                                            ]);
                                            $historicalCollection = new ArrayCollection([$historyEntity]);
                                            $this->historicalService->recordChanges($historicalCollection);
                                        }
                                    }
                                }
                                break;
                            case 'ATTRIBUTES_Groups':
                                $attributeGroupId = $inputAttributeField['entity']->getInputFieldRAttributesInputFieldRAttributeId()
                                    ->getAttributeGroupId()
                                    ->getAttributeGroupId();
                                $attributeGroupDetails = $this->_attributesGroupMapper->fetchAllAttributeGroups($this->_currentLanguage, $attributeGroupId);
                                $isVariant = 1;
                                $updateData = array(
                                    'attributeValue' => $inputAttributeField['value']
                                );
                                $whereClause = array(
                                    'attributeGroupId' => $attributeGroupId,
                                    'productOrVariantId' => $variant['productAttributeId'],
                                    'isVariant' => $isVariant
                                );

                                /**
                                 * Check if the variant has the attribute
                                 */
                                $this->_sql->setTable(array(
                                    'pac' => $this->_productsAttributesCombinationTable
                                ));
                                $select = $this->_sql->select();
                                $select->where($whereClause);
                                $select->limit(1);
                                $statement = $this->_sql->prepareStatementForSqlObject($select);
                                $results = $statement->execute();
                                $count = $results->count();
                                $results = $results->next();

                                /**
                                 * Check if the attribute is already saved
                                 */
                                if ($count > 0) {
                                    /**
                                     * Update the attribute
                                     */
                                    if (! empty($attributeGroupDetails['viewAs']) && $attributeGroupDetails['viewAs'] == 'TEXT') {
                                        /**
                                         * Need to update PRODUCTS_Attributes_Combination_Definitions
                                         */
                                        /**
                                         * Select the attributes combination to get the id
                                         */
                                        if (! empty($results['productAttributeCombinationId'])) {
                                            $whereClause = array(
                                                'productAttributeCombinationId' => $results['productAttributeCombinationId'],
                                                'languageId' => $this->_currentLanguage
                                            );
                                            $update = $this->_sql->update();
                                            $update->table($this->_productsAttributesCombinationDefinitions);
                                            $update->set($updateData);
                                            $update->where($whereClause);
                                            $statement = $this->_sql->prepareStatementForSqlObject($update);
                                            $statement->execute();
                                        }
                                    } else {
                                        /**
                                         * Need to update PRODUCTS_Attributes_Combination
                                         */
                                        $update = $this->_sql->update();
                                        $update->table($this->_productsAttributesCombinationTable);
                                        $update->set($updateData);
                                        $update->where($whereClause);
                                        $statement = $this->_sql->prepareStatementForSqlObject($update);
                                        $statement->execute();
                                    }
                                } else {
                                    /**
                                     * Create new attribute
                                     */
                                    /**
                                     * Need to create new entry in _productsAttributesCombinationTable table
                                     */
                                    $this->_sql->setTable($this->_productsAttributesCombinationTable);
                                    $action = $this->_sql->insert();
                                    if (! empty($attributeGroupDetails['viewAs']) && $attributeGroupDetails['viewAs'] == 'TEXT') {
                                        $attributeValue = '';
                                    } else {
                                        $attributeValue = $inputAttributeField['value'];
                                    }
                                    $action->values(array(
                                        'attributeGroupId' => $attributeGroupDetails['attributeGroupId'],
                                        'productOrVariantId' => $variant['productAttributeId'],
                                        'attributeValue' => $attributeValue,
                                        'isVariant' => $isVariant,
                                        'groupType' => $attributeGroupDetails['viewAs'],
                                        'status' => 'ACTIVE'
                                    ));
                                    $statement = $this->_sql->prepareStatementForSqlObject($action);
                                    $statement->execute();
                                    if (! empty($attributeGroupDetails['viewAs']) && $attributeGroupDetails['viewAs'] == 'TEXT') {
                                        /**
                                         * If the attribute is text we need to create new entry in the _productsAttributesCombinationDefinitions table
                                         */
                                        $this->_sql->setTable($this->_productsAttributesCombinationDefinitions);
                                        $action = $this->_sql->insert();
                                        $action->values(array(
                                            'productAttributeCombinationId' => $this->_dbAdapter->getDriver()
                                                ->getConnection()
                                                ->getLastGeneratedValue(),
                                            'attributeValue' => $inputAttributeField['value'],
                                            'languageId' => $this->_currentLanguage,
                                            'status' => 'ACTIVE'
                                        ));
                                        $statement = $this->_sql->prepareStatementForSqlObject($action);
                                        $statement->execute();
                                    }
                                }
                                break;
                        }
                    }
                }
            }

            /**
             * Update Prices
             */
            if (! empty($inputArray['prices']) && $updatePricesFlag) {
                $this->updatePricesFor($variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES', $inputArray['prices']);
            }
            $variants = array(
                'variantId' => $variant['productAttributeId'],
                'productId' => $variant['productId']
            );

        }
        return $variants;
    }

    /**
     *
     * @param
     *            $style
     * @param null $option
     * @return array
     */
    public function getProductPricesForStyleOrOption($style, $option = null, $simplePrices = false)
    {
        $productPrices = array();
        switch ($option) {
            case null:
                /**
                 * Get prices from product level
                 */
                $productDetails = $this->getMainProductDetailsByStyle($style);
                $productPrices = $this->getProductPricesForProductOrVariant($productDetails['productId'], 'PRODUCTS');

                // no result was found
                if($productPrices === null){
                    $productPrices = [];
                }

                break;
            default:
                /**
                 * Get prices from option level
                 */
                $productDetails = $this->getVariantDetailsByStyle($option, true);

                if (empty($productDetails)) {
                    $option = str_replace(' ', '', $option);
                    $productDetails = $this->getVariantDetailsByStyle($option, true);
                }
                if (! empty($productDetails)) {
                    $productPrices = $this->getProductPricesForProductOrVariant($productDetails['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
                    // no result was found
                    if($productPrices === null){
                        $productPrices = [];
                    }
                } else {
                    return array();
                }
                break;
        }

        $returnArray = array();
        foreach ($productPrices as $price) {
            if ($simplePrices) {
                $returnArray[] = $price;
            } else {
                $currencyDetails = $this->getCurrencyCodeById($price['currencyId']);
                $territoryDetails = $this->getTerritoryCodeById($price['regionId']);
                if (! empty($option)) {
                    /**
                     * Supplier Price for variant
                     */
                    $supplierPrice = $this->getSupplierPrices($productDetails['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
                } else {
                    /**
                     * Supplier Price for product
                     */
                    $supplierPrice = $this->getSupplierPrices($productDetails['productId'], 'PRODUCTS');
                }

                $currencyCode = $currencyDetails['isoCode'];
                $territoryCode = $territoryDetails['iso2Code'];

                $returnArray['costPrice'][$currencyCode] = $price['costPrice'];
                $returnArray['nowPrice'][$currencyCode][$territoryCode] = $price['nowPrice'];
                $returnArray['wasPrice'][$currencyCode][$territoryCode] = $price['wasPrice'];
                $returnArray['supplierPrice'] = $supplierPrice;
            }
        }

        return $returnArray;
    }

    /**
     *
     * @param int $territoryId
     * @return string
     */
    public function getTerritoryCodeById($territoryId)
    {
        $this->_sql->setTable($this->territoriesTable);
        $select = $this->_sql->select();

        $select->where("territoryId = " . $territoryId);

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        return $statement->execute()->next();
    }

    /**
     *
     * @param $productOrVariantId
     * @param string $productIdFrom
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getProductPricesForProductOrVariant($productOrVariantId, $productIdFrom = "PRODUCTS")
    {
        $sql = new Sql($this->_dbAdapter);
        $sql->setTable($this->_productsPricesTable);
        $select = $sql->select();

        $select->where(["productIdFrom" => $productIdFrom]);
        $select->where(["productOrVariantId" => $productOrVariantId]);
        $select->where(["status" => 'ACTIVE']);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results =  $statement->execute();

        if ($results->count() > 0)
        {
            return $results;
        }

        return null;
    }

    /**
     *
     * @param $currencyId
     * @return string
     */
    public function getCurrencyCodeById($currencyId)
    {
        $this->_sql->setTable($this->_currencyTable);
        $select = $this->_sql->select();
        $select->where("currencyId = " . $currencyId);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        return $statement->execute()->next();
    }

    /**
     * @param $product
     * @return array
     */
    public function fetchAllOptionsForProduct($product)
    {
        $options = $this->getAllOptionsForStyle($product['style'], true);

        $lessCompleteWorkflows = array();
        $lessCompletedWorkflows = array();

        $returnArray = array();
        foreach ($options as $optionCode => $variant) {

            $commonAttributeArray = array();
            $commonAttributes = $this->getAllCommonAttributesForVariantId($variant['productAttributeId'], true);

            foreach ($commonAttributes as $commonAttribute) {
                try {
                    $commonAttributeEntity = $this->commonAttributesService->getCommonAttribute($commonAttribute['commonAttributeId']);
                    $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($commonAttribute['commonAttributeValue']);

                    if (! is_null($commonAttributeValueEntity)) {
                        $name = $commonAttributeEntity->getcommonAttributeDefinitions()->getdescription();

                        $definition = $commonAttributeValueEntity->getcommonAttributeValuesDefinitions();

                        $value = ! empty($definition) ? $commonAttributeValueEntity->getcommonAttributeValuesDefinitions()->getvalue() : $commonAttribute['commonAttributeValue'];
                        $commonAttributeArray[$name] = array(
                            'name' => $name,
                            'value' => $value
                        );
                    }
                } catch (\Exception $ex) {
                    continue;
                }
            }
            $usedWorkflows = $this->getUsedWorkflowsFor($variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
            $workflows = $this->getWorkflows();

            $workflowsArray = array();
            $percentage = 0;

            foreach ($workflows as $workflow) {
                $completed = 0;
                if (isset($usedWorkflows[$workflow['workflowId']])) {
                    $completed = 1;
                }

                $workflow['completed'] = $completed;
                $workflowsArray[] = $workflow;
                if ($completed == 1) {
                    $percentage = $workflow['percentagePart'];
                }
            }
            $variant['workflows'] = $workflowsArray;
            $variant['workflowsPercentage'] = $percentage;
            $variant['optionCode'] = $optionCode;
            $returnArray[] = array(
                'commonAttributes' => $commonAttributeArray,
                'details' => $variant
            );

            $lessCompleteWorkflows[] = array(
                'count' => count($usedWorkflows),
                'workflows' => array(
                    'workflows' => $workflowsArray,
                    'percentage' => $percentage
                )
            );
        }
        /**
         * Get the least completed workflow for options
         */
        $smallest = 0;
        foreach ($lessCompleteWorkflows as $lessCompleteWorkflow) {
            if ($smallest <= $lessCompleteWorkflow['count']) {
                $smallest = $lessCompleteWorkflow['count'];
                $lessCompletedWorkflows = $lessCompleteWorkflow['workflows'];
            }
        }

        return array(
            'options' => $returnArray,
            'lessCompletedWorkflows' => $lessCompletedWorkflows
        );
    }

    /**
     *
     * @return array
     */
    public function fetchAllStyles()
    {
        $products = $this->fetchAllProducts();

        $returnArray = array();
        foreach ($products as $product) {

            $commonAttributeArray = array();
            $commonAttributesArray = array(
                2,
                6,
                7,
                8,
                9
            );
            $commonAttributes = $this->getCommonAttributesFor($product['productId'], "PRODUCTS", $commonAttributesArray);
            ;

            foreach ($commonAttributes as $commonAttribute) {

                $commonAttributeEntity = $this->commonAttributesService->getCommonAttribute($commonAttribute[0]['commonAttributeId']);
                $name = $commonAttributeEntity->getcommonAttributeDefinitions()->getdescription();
                if (! empty($commonAttribute[0]['commonAttributeValue'])) {
                    $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($commonAttribute[0]['commonAttributeValue']);
                }
                $value = ! empty($commonAttributeValueEntity) ? $commonAttributeValueEntity->getcommonAttributeValuesDefinitions()->getvalue() : $commonAttribute[0]['commonAttributeValue'];
                $commonAttributeArray[$name] = array(
                    'name' => $name,
                    'value' => $value
                );
            }

            $options = $this->fetchAllOptionsForProduct($product);

            $returnArray[] = array(
                'commonAttributes' => $commonAttributeArray,
                'details' => $product,
                'options' => $options['options'],
                'lessCompletedWorkflow' => $options['lessCompletedWorkflows']
            );
        }
        return $returnArray;
    }

    /**
     *
     * @param
     *            $commonAttributeId
     * @return bool
     */
    public function deleteCommonAttributeAssignedToProductsValues($commonAttributeId)
    {
        try {
            $whereClause = array(
                'commonAttributeId' => $commonAttributeId
            );

            /**
             * Select products where common attribute is used
             */
            $this->_sql->setTable($this->productsCommonAttributesTable);
            $select = $this->_sql->select()->where($whereClause);
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            if ($results->count() > 0) {
                /**
                 * Delete the attribute from the product
                 */
                $this->_delete($this->productsCommonAttributesTable, $whereClause);

                /**
                 * Add the products into the queue manager to sync without the attribute
                 */
                foreach ($results as $result) {
                    switch ($result['productIdFrom']) {
                        case 'PRODUCTS_R_ATTRIBUTES':
                            $optionCode = $this->getOptionCodeForVariantId($result['productOrVariantId']);
                            $this->queueService->addSipEntryToQueue($optionCode, 5, $this, true);

                            /**
                             * Add spectrum entry
                             */
                            $variants = $this->getVariantsIdForOption($optionCode);
                            foreach ($variants as $variant) {
                                $this->queueService->addSpectrumEntryToQueueForCommonAttributes($variant['productAttributeId']);
                            }
                            break;
                    }
                }
            }

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     *
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getWorkflows()
    {
        $this->_sql->setTable($this->workflowTable);
        $select = $this->_sql->select();
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     *
     * @param
     *            $style
     * @return array|mixed
     */
    public function getStyleInfo($style)
    {
        $productId = $this->getProductIdFromStyle($style);
        return $this->_getProductDefinitions($productId);
    }

    /**
     *
     * @param
     *            $variantId
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    private function getAllCommonAttributesForVariantId($variantId, $searchPage = false)
    {
        $this->_sql->setTable(array(
            'pca' => $this->productsCommonAttributesTable
        ));
        $select = $this->_sql->select()
            ->where('pca.productOrVariantId = ' . $variantId)
            ->where('pca.productIdFrom = "PRODUCTS_R_ATTRIBUTES"');

        if ($searchPage) {
            $select->join(array(
                'cagr' => $this->commonAttributesGuiRepresentationTable
            ), new Expression('pca.commonAttributeId = cagr.commonAttributeId'), array(
                'commonAttributesGuiRepresentationId'
            ), Select::JOIN_LEFT);
            $select->where('cagr.displayOnSearch = 1');
        }

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     *
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    private function fetchAllProducts()
    {
        $this->_sql->setTable(array(
            'p' => $this->_tableName
        ));
        $select = $this->_sql->select();
        $select->join(array(
            'pd' => $this->_productsDefinitionsTable
        ), new Expression('pd.productId = p.productId
                    AND pd.productIdFrom = "PRODUCTS" AND `pd`.status != "DELETED" AND languageId = ' . $this->_currentLanguage), array(
            '*'
        ), Select::JOIN_LEFT);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     * @param $formData
     * @param array $options
     * @param $prices
     * @param bool $justOne
     * @return array
     * @throws \Exception
     */
    public function getValuesForUpdatedFields($formData, $options = array(), $prices, $justOne = false)
    {
        try {
            $preparedArray = $this->prepareArrayOfSubmittedData($formData);
            $optionsDataArray = array();
            /**
             * Loop all options
             */
            foreach ($options as $optionCode => $option) {
                /**
                 * Get first variant from option
                 */
                if ($justOne) {
                    $variant = $option;
                } else {
                    $variant = $option[0];
                }

                /**
                 * Loop all input fields
                 */
                foreach ($preparedArray as $inputField) {
                    switch (true) {
                        case $inputField['entity']->getInputTableFieldsInputTableFieldId():
                            /**
                             * Input is Table field
                             */
                            if ($inputField['table'] == 'PRODUCTS_Definitions') {
                                $this->_sql->setTable($inputField['table']);
                                $select = $this->_sql->select()
                                    ->where('productId = ' . $variant['productAttributeId'])
                                    ->where('productIdFrom = "PRODUCTS_R_ATTRIBUTES"')
                                    ->where('status != "DELETED"');
                                $statement = $this->_sql->prepareStatementForSqlObject($select);
                                $result = $statement->execute()->next();
                                $inputField['optionValue'] = $result[$inputField['column']];
                            } else {
                                $inputField['optionValue'] = $inputField['value'];
                            }

                            break;
                        case $inputField['entity']->getInputFieldRAttributesInputFieldRAttributeId():
                            /**
                             * Input is attribute
                             */
                            switch ($inputField['entity']->getInputFieldRAttributesInputFieldRAttributeId()->getAttributeTable()) {
                                case 'COMMON_ATTRIBUTES':
                                    $commonAttributeId = $inputField['entity']->getInputFieldRAttributesInputFieldRAttributeId()->getCommonAttributeId();
                                    $commonAttributeType = $inputField['entity']->getInputFieldRAttributesInputFieldRAttributeId()
                                        ->getcommonAttributes()
                                        ->getCommonAttributeGuiRepresentation()
                                        ->getCommonAttributesViewTypeId()
                                        ->getCommonAttributeViewType();
                                    $selectedValues = array();

                                    switch ($commonAttributeType) {
                                        case 'SELECT':
                                            $commonAttributeValues = $this->getCommonAttributeValuesAssignedToProduct($commonAttributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
                                            foreach ($commonAttributeValues as $commonAttributeValue) {
                                                if ($commonAttributeValue['commonAttributeValue'] !== '') {
                                                    $abbrevCode = NULL;
                                                    $value = '';

                                                    $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById((int) $commonAttributeValue['commonAttributeValue']);

                                                    if($commonAttributeValueEntity !== null){
                                                        $abbrevCode = $commonAttributeValueEntity->getCommonAttributeValueAbbrev();
                                                        $value = $commonAttributeValueEntity->getcommonAttributeValuesDefinitions()->getvalue();
                                                    }

                                                    if (!empty($abbrevCode)) {
                                                        $value = $abbrevCode . ' - ' . $value;
                                                    }

                                                    $selectedValues[$commonAttributeValue['commonAttributeValue']] = $value;
                                                }
                                            }
                                            $inputField['optionValue'] = $selectedValues;
                                            break;
                                        case 'MULTISELECT':
                                            $commonAttributeValues = $this->getCommonAttributeValuesAssignedToProduct($commonAttributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');

                                            foreach ($commonAttributeValues as $commonAttributeValue) {
                                                if (!empty($commonAttributeValue['commonAttributeValue'])) {
                                                    $abbrevCode = NULL;
                                                    $value = '';

                                                    $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($commonAttributeValue['commonAttributeValue']);

                                                    if($commonAttributeValueEntity !== null){
                                                        $abbrevCode = $commonAttributeValueEntity->getCommonAttributeValueAbbrev();
                                                        $value = $commonAttributeValueEntity->getcommonAttributeValuesDefinitions()->getvalue();
                                                    }

                                                    if (!empty($abbrevCode)) {
                                                        $value = $abbrevCode . ' - ' . $value;
                                                    }


                                                    $selectedValues[$commonAttributeValue['commonAttributeValue']] = $value;
                                                } else {
                                                    // Remove bad data from db
                                                }
                                            }
                                            $inputField['optionValue'] = $selectedValues;
                                            break;
                                        case 'PRODUCT':
                                            $commonAttributeValues = $this->getCommonAttributeValuesAssignedToProduct($commonAttributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');
                                            foreach ($commonAttributeValues as $commonAttributeValue) {
                                                $variantId = $commonAttributeValue['commonAttributeValue'];

                                                $skus = $this->getVariantsSkus(array($variantId));
                                                $sku = isset($skus[0]) ? $skus[0] : false;
                                                $colour = $this->getColourForVariantId($variantId);
                                                $colourName = $colour['name'];
                                                $size = $this->getSizeUsedForVariantId($variantId);

                                                $selectedValues[$variantId] = array('variantId' => $variantId,'sku' => $sku, 'colour' => $colourName, 'size' => $size);
                                            }
                                            $inputField['optionValue'] = $selectedValues;
                                            break;
                                        case 'CHECKBOX':
                                            $commonAttributeValues = $this->getCommonAttributeValuesAssignedToProduct($commonAttributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES', true);
                                            if ($commonAttributeValues['commonAttributeValue'] == 1) {
                                                $inputField['optionValue'] = 'Checked';
                                            } else {
                                                $inputField['optionValue'] = 'Unchecked';
                                            }
                                            break;
                                        case 'TEXT':
                                            $commonAttributeValues = $this->getCommonAttributeValuesAssignedToProduct($commonAttributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES', true);
                                            $inputField['optionValue'] = $commonAttributeValues['commonAttributeValue'];
                                            break;
                                        case 'DATEPICKER':
                                        case 'DATEPICKER - DATE ONLY':
                                            $commonAttributeValues = $this->getCommonAttributeValuesAssignedToProduct($commonAttributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES', true);
                                            $inputField['optionValue'] = $commonAttributeValues['commonAttributeValue'];
                                            break;
                                    }

                                    break;
                                case 'ATTRIBUTES_Groups':
                                    $attributeGroupEntity = $inputField['entity']->getInputFieldRAttributesInputFieldRAttributeId()->getAttributeGroupId();

                                    $attributeType = $attributeGroupEntity->getViewAs();

                                    switch ($attributeType) {
                                        case 'TEXT':
                                            $attributeValue = $this->getAttributeGroupValueAssignedToProduct($attributeGroupEntity->getattributegroupid(), $variant['productAttributeId'], 1, true);
                                            $inputField['optionValue'] = $attributeValue['attributeValue'];
                                            break;
                                        case 'CHECKBOX':
                                            $attributeValue = $this->getAttributeGroupValueAssignedToProduct($attributeGroupEntity->getattributegroupid(), $variant['productAttributeId']);
                                            if ($attributeValue['attributeValue'] == 1) {
                                                $inputField['optionValue'] = 'Checked';
                                            } else {
                                                $inputField['optionValue'] = 'Unchecked';
                                            }
                                            break;
                                        case 'SELECT':
                                            $attributeValue = $this->getAttributeGroupValueAssignedToProduct($attributeGroupEntity->getattributegroupid(), $variant['productAttributeId']);
                                            if (! empty($attributeValue['attributeValue'])) {
                                                $attributeValue = $this->_attributesGroupMapper->getAttributeValue($attributeValue['attributeValue']);
                                                $inputField['optionValue'] = array(
                                                    $attributeValue['attributeId'] => $attributeValue['attributeValue']
                                                );
                                            } else {
                                                $inputField['optionValue'] = '';
                                            }
                                            break;
                                    }
                                    break;
                            }
                            break;
                    }
                    $inputArray = array(
                        'newValue' => isset($inputField['value']) ? $inputField['value'] : null,
                        'oldValue' => isset($inputField['optionValue']) ? $inputField['optionValue'] : null,
                        'inputField' => $inputField['entity'],
                        'label' => $inputField['label']
                    );
                    $optionsDataArray[$optionCode][] = $inputArray;
                }
                /**
                 * Check if the prices must moved to options
                 */
                if ($prices == 1) {
                    $pricesData = $formData['prices'];

                    $supplierPrices = $this->getSupplierPrices($variant['productAttributeId']);
                    /**
                     * Set up supplier currency
                     */
                    $optionsDataArray[$optionCode][] = array(
                        'newValue' => $formData['prices']['supplier-currency'],
                        'oldValue' => $supplierPrices['currencyCode'],
                        'inputField' => null,
                        'label' => 'Supplier Currency'
                    );
                    unset($pricesData['supplier-currency']);
                    /**
                     * Set up supplier price
                     */
                    $optionsDataArray[$optionCode][] = array(
                        'newValue' => $formData['prices']['supplier-price'],
                        'oldValue' => $supplierPrices['price'],
                        'inputField' => null,
                        'label' => 'Supplier Price'
                    );
                    unset($pricesData['supplier-price']);
                    /**
                     * Set up Cost Prices
                     */
                    $this->_sql->setTable(array(
                        'pp' => $this->_productsPricesTable
                    ));
                    $select = $this->_sql->select()
                        ->where('pp.productOrVariantId = ' . $variant['productAttributeId'])
                        ->where('pp.productIdFrom = "PRODUCTS_R_ATTRIBUTES"')
                        ->where('pp.status = "ACTIVE"');
                    $select->join(array(
                        'c' => $this->_currencyTable
                    ), // Set join table and alias
                        new Expression('pp.currencyId = c.currencyId'), array(
                            '*'
                        ), Select::JOIN_LEFT);
                    $select->group('pp.currencyId');
                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $results = $statement->execute();
                    foreach ($results as $result) {
                        $currencyCode = $result['isoCode'];
                        /**
                         * Set up cost prices for each currency
                         */
                        $optionsDataArray[$optionCode][] = array(
                            'newValue' => $formData['prices']['costPrice-' . $currencyCode],
                            'oldValue' => $result['costPrice'],
                            'inputField' => null,
                            'label' => 'Landed Cost (' . $currencyCode . ')'
                        );
                        unset($pricesData['costPrice-' . $currencyCode]);
                    }

                    /**
                     * Set up Current Price and Original Price
                     */
                    $this->_sql->setTable(array(
                        'pp' => $this->_productsPricesTable
                    ));
                    $select = $this->_sql->select()
                        ->where('pp.productOrVariantId = ' . $variant['productAttributeId'])
                        ->where('pp.productIdFrom = "PRODUCTS_R_ATTRIBUTES"')
                        ->where('pp.status = "ACTIVE"');
                    $select->join(array(
                        'c' => $this->_currencyTable
                    ), // Set join table and alias
                        new Expression('pp.currencyId = c.currencyId'), array(
                            'currencyCode' => 'isoCode'
                        ), Select::JOIN_LEFT);
                    $select->join(array(
                        't' => $this->territoriesTable
                    ), // Set join table and alias
                        new Expression('pp.regionId = t.territoryId'), array(
                            'territoryCode' => 'iso2Code'
                        ), Select::JOIN_LEFT);
                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $results = $statement->execute();
                    foreach ($results as $result) {
                        /**
                         * Current Price
                         */
                        $optionsDataArray[$optionCode][] = array(
                            'newValue' => $formData['prices']['nowPrice-' . $result['currencyCode'] . '-' . $result['territoryCode']],
                            'oldValue' => $result['nowPrice'],
                            'inputField' => null,
                            'label' => 'Current Price (' . $result['currencyCode'] . '-' . $result['territoryCode'] . ')'
                        );
                        unset($pricesData['nowPrice-' . $result['currencyCode'] . '-' . $result['territoryCode']]);
                        /**
                         * Original Price
                         */
                        $optionsDataArray[$optionCode][] = array(
                            'newValue' => $formData['prices']['wasPrice-' . $result['currencyCode'] . '-' . $result['territoryCode']],
                            'oldValue' => $result['wasPrice'],
                            'inputField' => null,
                            'label' => 'Original Price (' . $result['currencyCode'] . '-' . $result['territoryCode'] . ')'
                        );
                        unset($pricesData['wasPrice-' . $result['currencyCode'] . '-' . $result['territoryCode']]);
                    }
                }
                /**
                 * Variants did not have the following prices therefor we need to include them with empty value
                 */
                if (! empty($pricesData)) {
                    foreach ($pricesData as $priceName => $priceValue) {
                        if (strpos($priceName, 'costPrice') !== false) {
                            $priceName = explode('-', $priceName);
                            $currencyCode = $priceName[1];

                            $optionsDataArray[$optionCode][] = array(
                                'newValue' => $formData['prices']['costPrice-' . $currencyCode],
                                'oldValue' => '-',
                                'inputField' => null,
                                'label' => 'Landed Cost (' . $currencyCode . ')'
                            );
                        } elseif (strpos($priceName, 'nowPrice') !== false) {
                            $priceName = explode('-', $priceName);
                            $currencyCode = $priceName[1];
                            $territoryCode = $priceName[2];

                            $optionsDataArray[$optionCode][] = array(
                                'newValue' => $formData['prices']['nowPrice-' . $currencyCode . '-' . $territoryCode],
                                'oldValue' => '-',
                                'inputField' => null,
                                'label' => 'Current Price (' . $currencyCode . '-' . $territoryCode . ')'
                            );
                        } elseif (strpos($priceName, 'wasPrice') !== false) {
                            $priceName = explode('-', $priceName);
                            $currencyCode = $priceName[1];
                            $territoryCode = $priceName[2];

                            $optionsDataArray[$optionCode][] = array(
                                'newValue' => $formData['prices']['wasPrice-' . $currencyCode . '-' . $territoryCode],
                                'oldValue' => '-',
                                'inputField' => null,
                                'label' => 'Original Price (' . $currencyCode . '-' . $territoryCode . ')'
                            );
                        }
                    }
                }
            }

            return $optionsDataArray;

        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * @param $submitedFormData
     */
    public function saveDataAgainstStyle($submitedFormData)
    {
        if (! empty($submitedFormData)) {
            foreach ($submitedFormData as $optionCodeInputField => $values) {
                $optionCodeAndInputFieldId = explode('-', $optionCodeInputField);
                $optionCode = $optionCodeAndInputFieldId[0];
                $inputFieldId = $optionCodeAndInputFieldId[1];

                $inputFieldEntity = $this->inputService->getInputField($inputFieldId);

                switch ($inputFieldEntity->getInputFieldRAttributesInputfieldrattributeid()->getAttributeTable()) {
                    case 'COMMON_ATTRIBUTES':
                        $commonAttributeId = $inputFieldEntity->getInputFieldRAttributesInputfieldrattributeid()->getCommonAttributeId();
                        $success = $this->saveAttributeValueAgainstStyle($commonAttributeId, $values, 'COMMON_ATTRIBUTES', false, $optionCode);
                        break;
                    case 'ATTRIBUTES_Groups':
                        $attributeId = $inputFieldEntity->getInputFieldRAttributesInputfieldrattributeid()
                            ->getAttributeGroupId()
                            ->getAttributeGroupId();
                        $viewAs = $inputFieldEntity->getInputFieldRAttributesInputfieldrattributeid()
                            ->getAttributeGroupId()
                            ->getViewAs();
                        $success = $this->saveAttributeValueAgainstStyle($attributeId, $values, 'ATTRIBUTES_Groups', $viewAs, $optionCode);
                        break;
                }

                break;
            }
        }
    }

    /**
     * Get the option from input field data sent by popup windows genrated by blue button to edit field value
     * @param array $inputFieldData
     * @return array[]
     */
    public function getOptionsFromInputFieldData($inputFieldData)
    {
        $options = [];
        foreach ($inputFieldData as $optionCodeInputField => $values) {
            $optionCodeAndInputFieldId = explode('-', $optionCodeInputField);
            $options[] = $optionCodeAndInputFieldId[0];
        }
        return $options;
    }

    /**
     * @param $submitedFormData
     */
    public function saveStyleValue($submitedFormData)
    {
        if (! empty($submitedFormData)) {
            if(isset($submitedFormData['values']))
            {
                if(count($submitedFormData['values']) != 0)
                {
                    $submitedFormData = $submitedFormData['values'];
                }
            }
            foreach ($submitedFormData as $styleCodeInputField => $values) {
                $styleCodeAndInputFieldId = explode('-', $styleCodeInputField);
                $styleCode = $styleCodeAndInputFieldId[0];
                $inputFieldId = $styleCodeAndInputFieldId[1];

                $inputFieldEntity = $this->inputService->getInputField($inputFieldId);

                switch ($inputFieldEntity->getInputFieldRAttributesInputfieldrattributeid()->getAttributeTable()) {
                    case 'COMMON_ATTRIBUTES':
                        $commonAttributeId = $inputFieldEntity->getInputFieldRAttributesInputfieldrattributeid()->getCommonAttributeId();
                        $success = $this->saveAttributeStyleValue($commonAttributeId, $values, 'COMMON_ATTRIBUTES', false, $styleCode);
                        break;
                    case 'ATTRIBUTES_Groups':
                        $attributeId = $inputFieldEntity->getInputFieldRAttributesInputfieldrattributeid()
                            ->getAttributeGroupId()
                            ->getAttributeGroupId();
                        $viewAs = $inputFieldEntity->getInputFieldRAttributesInputfieldrattributeid()
                            ->getAttributeGroupId()
                            ->getViewAs();
                        $success = $this->saveAttributeStyleValue($attributeId, $values, 'ATTRIBUTES_Groups', $viewAs, $styleCode);
                        break;
                }

                break;
            }
        }
    }

    /**
     * @param $attributeId
     * @param $values
     * @param $attributeTable
     * @param $attributeType
     * @param $optionCode
     */
    private function saveAttributeValueAgainstStyle($attributeId, $values, $attributeTable, $attributeType, $optionCode)
    {
        $variants = $this->getVariantsIdForOption($optionCode);
        switch ($attributeTable) {
            case 'COMMON_ATTRIBUTES':
                foreach ($variants as $variantData) {
                    $this->_sql->setTable($this->productsCommonAttributesTable);
                    $whereArray = array(
                        'productOrVariantId' => $variantData['productId'],
                        'productIdFrom' => 'PRODUCTS',
                        'commonAttributeId' => $attributeId
                    );
                    $select = $this->_sql->select()->where($whereArray);
                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $results = $statement->execute();

                    if ($results->count() > 0) {
                        $this->_delete($this->productsCommonAttributesTable, $whereArray);
                    }

                    $insertData = array(
                        'productOrVariantId' => $variantData['productId'],
                        'productIdFrom' => 'PRODUCTS',
                        'commonAttributeId' => $attributeId
                    );

                    /**
                     * Insert Data
                     */
                    if (is_array($values)) {
                        foreach ($values as $value) {
                            $insertData['commonAttributeValue'] = $value;
                            $this->_sql->setTable($this->productsCommonAttributesTable);
                            $action = $this->_sql->insert();
                            $action->values($insertData);
                            $statement = $this->_sql->prepareStatementForSqlObject($action);
                            $statement->execute();
                            $this->loggingService->logUpdate($variantData['productId'], null, $attributeId, $value);
                        }
                    } else {
                        $insertData['commonAttributeValue'] = $values === 'on' ? 1 : $values;
                        $this->_sql->setTable($this->productsCommonAttributesTable);
                        $action = $this->_sql->insert();
                        $action->values($insertData);
                        $statement = $this->_sql->prepareStatementForSqlObject($action);
                        $statement->execute();
                        $this->loggingService->logUpdate($variantData['productId'], null, $attributeId, $insertData['commonAttributeValue']);

                    }
                    break;
                }
                break;
            case 'ATTRIBUTES_Groups':
                foreach ($variants as $variantData) {
                    if ($attributeType == 'CHECKBOX' && $values == 'on') {
                        $values = 1;
                    }
                    $this->updateAttributeForProductOrVariant($variantData['productId'], 'PRODUCTS', array(
                        'attributeGroupId' => $attributeId
                    ), $values);
                    break;
                }
                break;
        }
    }

    /**
     * @param $attributeId
     * @param $values
     * @param $attributeTable
     * @param $attributeType
     * @param $styleCode
     */
    private function saveAttributeStyleValue($attributeId, $values, $attributeTable, $attributeType, $styleCode)
    {
        $productId = $this->getProductIdFromStyle($styleCode);
        switch ($attributeTable) {
            case 'COMMON_ATTRIBUTES':
                $this->_sql->setTable($this->productsCommonAttributesTable);
                $whereArray = array(
                    'productOrVariantId' => $productId,
                    'productIdFrom' => 'PRODUCTS',
                    'commonAttributeId' => $attributeId
                );
                $select = $this->_sql->select()->where($whereArray);
                $statement = $this->_sql->prepareStatementForSqlObject($select);
                $results = $statement->execute();

                /** @var \PrismProductsManager\Entity\CommonAttributes $commonAttribute */
                $commonAttribute = $this->commonAttributesService->getCommonAttribute($attributeId);

                if ($commonAttribute->isKeepHistory()) {
                    if ($results->count() > 0) {
                        $historicalCollection = new ArrayCollection();

                        if (is_array($values)) {
                            foreach ($values as $value) {
                                if ($this->areValuesChanged(
                                    $productId,
                                    'PRODUCTS',
                                    $value,
                                    $commonAttribute->getCommonattributeid(),
                                    'Common_Attributes'
                                )) {
                                    $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                        'productOrVariantId' => $productId,
                                        'isStyle' => true,
                                        'commonAttribute' => $commonAttribute,
                                        'attributeName' => '',
                                        'value' => $value,
                                        'fromDate' => null,
                                        'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                        'optionCode' => ''
                                    ]);
                                    $historicalCollection->add($historyEntity);
                                }
                            }
                        } else {
                            if ($this->areValuesChanged(
                                $productId,
                                'PRODUCTS',
                                $values,
                                $commonAttribute->getCommonattributeid(),
                                'Common_Attributes'
                            )) {
                                $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                    'productOrVariantId' => $productId,
                                    'isStyle' => true,
                                    'commonAttribute' => $commonAttribute,
                                    'attributeName' => '',
                                    'value' => $values,
                                    'fromDate' => null,
                                    'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                ]);
                                $historicalCollection->add($historyEntity);
                            }
                        }

                        $this->historicalService->recordChanges($historicalCollection);

                        $this->_delete($this->productsCommonAttributesTable, $whereArray);
                    } else {
                        $historicalCollection = new ArrayCollection();
                        /** New Values to be inserted */
                        if (is_array($values)) {
                            foreach ($values as $value) {
                                $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                    'productOrVariantId' => $productId,
                                    'isStyle' => true,
                                    'commonAttribute' => $commonAttribute,
                                    'attributeName' => '',
                                    'value' => $value,
                                    'fromDate' => null,
                                    'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                ]);
                                $historicalCollection->add($historyEntity);
                            }
                        } else {
                            $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                'productOrVariantId' => $productId,
                                'isStyle' => true,
                                'commonAttribute' => $commonAttribute,
                                'attributeName' => '',
                                'value' => $values,
                                'fromDate' => null,
                                'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                            ]);
                            $historicalCollection->add($historyEntity);
                        }
                        $this->historicalService->recordChanges($historicalCollection);
                    }
                } else {
                    if ($results->count() > 0) {
                        $this->_delete($this->productsCommonAttributesTable, $whereArray);
                    }
                }



                $insertData = array(
                    'productOrVariantId' => $productId,
                    'productIdFrom' => 'PRODUCTS',
                    'commonAttributeId' => $attributeId
                );

                /**
                 * Insert Data
                 */
                if (is_array($values)) {
                    foreach ($values as $value) {
                        $insertData['commonAttributeValue'] = $value;
                        $this->_sql->setTable($this->productsCommonAttributesTable);
                        $action = $this->_sql->insert();
                        $action->values($insertData);
                        $statement = $this->_sql->prepareStatementForSqlObject($action);
                        $statement->execute();
                        $this->loggingService->logUpdate($styleCode, null, $attributeId, $value);
                    }
                } else {
                    $insertData['commonAttributeValue'] = $values === 'on' ? 1 : $values;
                    $this->_sql->setTable($this->productsCommonAttributesTable);
                    $action = $this->_sql->insert();
                    $action->values($insertData);
                    $statement = $this->_sql->prepareStatementForSqlObject($action);
                    $statement->execute();

                    $this->loggingService->logUpdate($styleCode, null, $attributeId, $insertData['commonAttributeValue']);
                }
                break;
            case 'ATTRIBUTES_Groups':
                if ($attributeType == 'CHECKBOX' && $values == 'on') {
                    $values = 1;
                }
                $this->updateAttributeForProductOrVariant($productId, 'PRODUCTS', array(
                    'attributeGroupId' => $attributeId
                ), $values);
                break;
        }
    }

    /**
     * @param $submitedFormData
     */
    public function saveDataAgainstOption($submitedFormData)
    {
        if (! empty($submitedFormData)) {
            foreach ($submitedFormData as $optionCodeInputField => $values) {
                $optionCodeAndInputFieldId = explode('-', $optionCodeInputField);
                $optionCodeCount = count($optionCodeAndInputFieldId)- 2;
                $optionCode  = '';
                for ($i=0; $i<= $optionCodeCount;$i++) {
                    $optionCode .= $optionCodeAndInputFieldId[$i] . '-';
                }
                $optionCode = trim($optionCode, '-');
                $optionCode = $this->formatOptionCode($optionCode); // format the option to remove _
                // $optionCode = $optionCodeAndInputFieldId[0];
                $inputFieldId = $optionCodeAndInputFieldId[count($optionCodeAndInputFieldId) -1];
                $inputFieldEntity = $this->inputService->getInputField($inputFieldId);

                switch ($inputFieldEntity->getInputFieldRAttributesInputfieldrattributeid()->getAttributeTable()) {
                    case 'COMMON_ATTRIBUTES':
                        $commonAttributeId = $inputFieldEntity->getInputFieldRAttributesInputfieldrattributeid()->getCommonAttributeId();
                        $success = $this->saveAttributeValueAgainstOption($commonAttributeId, $values, 'COMMON_ATTRIBUTES', false, $optionCode);
                        break;
                    case 'ATTRIBUTES_Groups':
                        $attributeId = $inputFieldEntity->getInputFieldRAttributesInputfieldrattributeid()
                            ->getAttributeGroupId()
                            ->getAttributeGroupId();
                        $viewAs = $inputFieldEntity->getInputFieldRAttributesInputfieldrattributeid()
                            ->getAttributeGroupId()
                            ->getViewAs();
                        $success = $this->saveAttributeValueAgainstOption($attributeId, $values, 'ATTRIBUTES_Groups', $viewAs, $optionCode);
                        break;
                }
            }
        }
    }

    /**
     * To remove the "_" cahr from the option code before color code
     * the _ is been added by javascript code
     * The option code ruel are (style+color+size):
     * Style = first 2 char from category + first 2 char from season + 3 digit incrmitnt from 000 to 999
     * Color 3 char color code
     * Size
     *
     * @param string $optionCode
     * @return string
     */
    public function formatOptionCode($optionCode)
    {
        if ($optionCode == null) {
            return;
        }
        // remove any spaces
        $optionCode = str_replace(" ", "", $optionCode);

        if (substr($optionCode, - 4, 1) == '_') {
            $optionCode = substr_replace($optionCode, "", - 4, 1);
        }

        return $optionCode;
    }

    /**
     * @param $attributeId
     * @param $values
     * @param $attributeTable
     * @param $attributeType
     * @param $optionCode
     */
    private function saveAttributeValueAgainstOption($attributeId, $values, $attributeTable, $attributeType, $optionCode)
    {
        $variants = $this->getVariantsIdForOption($optionCode);
        switch ($attributeTable) {
            case 'COMMON_ATTRIBUTES':
                foreach ($variants as $variantData) {
                    $this->_sql->setTable($this->productsCommonAttributesTable);
                    $whereArray = array(
                        'productOrVariantId' => $variantData['productAttributeId'],
                        'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                        'commonAttributeId' => $attributeId
                    );
                    $select = $this->_sql->select()->where($whereArray);
                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $results = $statement->execute();

                    /** @var \PrismProductsManager\Entity\CommonAttributes $commonAttribute */
                    $commonAttribute = $this->commonAttributesService->getCommonAttribute($attributeId);

                    if ($commonAttribute->isKeepHistory()) {
                        if ($results->count() > 0) {
                            $historicalCollection = new ArrayCollection();

                            if (is_array($values)) {
                                foreach ($values as $value) {
                                    if ($this->areValuesChanged(
                                        $variantData['productAttributeId'],
                                        'PRODUCTS_R_ATTRIBUTES',
                                        $value,
                                        $commonAttribute->getCommonattributeid(),
                                        'Common_Attributes'
                                    )) {
                                        $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                            'productOrVariantId' => $variantData['productAttributeId'],
                                            'isStyle' => false,
                                            'commonAttribute' => $commonAttribute,
                                            'attributeName' => '',
                                            'value' => $value,
                                            'fromDate' => null,
                                            'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                            'option' => $optionCode
                                        ]);
                                        $historicalCollection->add($historyEntity);
                                    }
                                }
                            } else {
                                if ($this->areValuesChanged(
                                    $variantData['productAttributeId'],
                                    'PRODUCTS_R_ATTRIBUTES',
                                    $values,
                                    $commonAttribute->getCommonattributeid(),
                                    'Common_Attributes'
                                )) {
                                    $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                        'productOrVariantId' => $variantData['productAttributeId'],
                                        'isStyle' => false,
                                        'commonAttribute' => $commonAttribute,
                                        'attributeName' => '',
                                        'value' => $values,
                                        'fromDate' => null,
                                        'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                        'optionCode' => $optionCode
                                    ]);
                                    $historicalCollection->add($historyEntity);
                                }
                            }

                            $this->historicalService->recordChanges($historicalCollection);

                            $this->_delete($this->productsCommonAttributesTable, $whereArray);
                        } else {
                            $historicalCollection = new ArrayCollection();
                            /** New Values to be inserted */
                            if (is_array($values)) {
                                foreach ($values as $value) {
                                    $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                        'productOrVariantId' => $variantData['productAttributeId'],
                                        'isStyle' => false,
                                        'commonAttribute' => $commonAttribute,
                                        'attributeName' => '',
                                        'value' => $value,
                                        'fromDate' => null,
                                        'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                        'optionCode' => $optionCode
                                    ]);
                                    $historicalCollection->add($historyEntity);
                                }
                            } else {
                                $historyEntity = $this->historicalService->hydrateHistoricalEntity([
                                    'productOrVariantId' => $variantData['productAttributeId'],
                                    'isStyle' => false,
                                    'commonAttribute' => $commonAttribute,
                                    'attributeName' => '',
                                    'value' => $values,
                                    'fromDate' => null,
                                    'updateType' => HistoricalValue::UPDATE_TYPE_Interface,
                                    'optionCode' => $optionCode
                                ]);
                                $historicalCollection->add($historyEntity);
                            }
                            $this->historicalService->recordChanges($historicalCollection);
                        }
                    } else {
                        if ($results->count() > 0) {
                            $this->_delete($this->productsCommonAttributesTable, $whereArray);
                        }
                    }



                    $insertData = array(
                        'productOrVariantId' => $variantData['productAttributeId'],
                        'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                        'commonAttributeId' => $attributeId
                    );

                    /**
                     * Insert Data
                     */
                    if (is_array($values)) {
                        foreach ($values as $value) {
                            $insertData['commonAttributeValue'] = $value;
                            $this->_sql->setTable($this->productsCommonAttributesTable);
                            $action = $this->_sql->insert();
                            $action->values($insertData);
                            $statement = $this->_sql->prepareStatementForSqlObject($action);
                            $statement->execute();
                            $this->loggingService->logUpdate(null, $optionCode, $attributeId, $value);
                        }
                    } else {
                        $insertData['commonAttributeValue'] = $values === 'on' ? 1 : $values;
                        $this->_sql->setTable($this->productsCommonAttributesTable);
                        $action = $this->_sql->insert();
                        $action->values($insertData);
                        $statement = $this->_sql->prepareStatementForSqlObject($action);
                        $statement->execute();
                        $this->loggingService->logUpdate(null, $optionCode, $attributeId, $insertData['commonAttributeValue']);
                    }
                }
                break;
            case 'ATTRIBUTES_Groups':
                foreach ($variants as $variantData) {
                    if ($attributeType == 'CHECKBOX' && $values == 'on') {
                        $values = 1;
                    }
                    $this->updateAttributeForProductOrVariant($variantData['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES', array(
                        'attributeGroupId' => $attributeId
                    ), $values);
                }
                break;
        }
    }

    /**
     *
     * @param int $variantId
     * @return string
     */
    public function getOptionCodeForVariantId($variantId)
    {
        $colourUsed = $this->getColourForVariantId($variantId);
        $productId = $this->getProductIdFromVariantId($variantId);
        $minimumDetails = $this->getMinimumProductsDetails($productId);

        $productStyle = '';
        $colourCode = '';

        if (isset($minimumDetails['style'])) {
            $productStyle = $minimumDetails['style'];
        }

        if (isset($colourUsed['groupCode'])) {
            $colourCode = $colourUsed['groupCode'];
        }
        $separator = '';
        if (!empty($this->clientConfig['settings']['skuGenerationSeparator'])) {
            $separator = $this->clientConfig['settings']['skuGenerationSeparator'];
        }

        return $productStyle . $separator .  $colourCode;
    }

    /**
     *
     * @param int $variantId
     * @return array
     */
    public function getColourForVariantId($variantId)
    {
        $this->_sql->setTable(array(
            'prarsr' => $this->_productsAttributesSkuRuleRelationsTable
        ));
        $select = $this->_sql->select()
            ->where('prarsr.productAttributeId = ' . $variantId)
            ->where('prarsr.skuRuleTable = "COLOURS_Groups"')
            ->join(array(
                'cg' => $this->_coloursGroupsTable
            ), new Expression('cg.colourGroupId = prarsr.skuRuleId'), array(
                '*'
            ), Select::JOIN_LEFT)
            ->join(array(
                'cd' => $this->_coloursGroupsDefinitionsTable
            ), new Expression('cd.colourGroupId = cg.colourGroupId'), array(
                '*'
            ), Select::JOIN_LEFT)
            ->where('cd.languageId = 1');
        $select->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();

        return is_array($result) ? $result : array();
    }

    /**
     * @param $attributeGroupId
     * @param $productOrVariantId
     * @param int $isVariant
     * @param bool $getDefinition
     */
    private function getAttributeGroupValueAssignedToProduct($attributeGroupId, $productOrVariantId, $isVariant = 1, $getDefinition = false)
    {
        $this->_sql->setTable(array(
            'pac' => $this->_productsAttributesCombinationTable
        ));
        $select = $this->_sql->select()
            ->where('pac.attributeGroupId = ' . $attributeGroupId)
            ->where('pac.productOrVariantId = ' . $productOrVariantId)
            ->where('pac.isVariant = ' . $isVariant);
        if ($getDefinition) {
            $select->join(array(
                'pacd' => $this->_productsAttributesCombinationDefinitions
            ), // Set join table and alias
                new Expression('pac.productAttributeCombinationId = pacd.productAttributeCombinationId AND pacd.languageId = ' . $this->_currentLanguage), array(
                    '*'
                ), Select::JOIN_LEFT);
        }
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        return $statement->execute()->next();
    }

    /**
     * @param $commonAttributeId
     * @param $productOrVariantId
     * @param $productIdFrom
     * @param bool $nextOnly
     * @return void|\Zend\Db\Adapter\Driver\ResultInterface
     */
    private function getCommonAttributeValuesAssignedToProduct($commonAttributeId, $productOrVariantId, $productIdFrom, $nextOnly = false)
    {
        $this->_sql->setTable($this->productsCommonAttributesTable);
        $select = $this->_sql->select()
            ->where('productOrVariantId = ' . $productOrVariantId)
            ->where('productIdFrom = "' . $productIdFrom . '"')
            ->where('commonAttributeId = ' . $commonAttributeId);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        if ($nextOnly) {
            return $statement->execute()->next();
        } else {
            return $statement->execute();
        }
    }

    /**
     * @param $formData
     * @return array
     */
    private function prepareArrayOfSubmittedData($formData)
    {
        $returnArray = array();
        /**
         * Loop InputFields, get fields values and build array
         */
        foreach ($formData['inputfield'] as $inputFieldId => $inputFieldValue) {
            $inputFieldEntity = $this->inputService->getInputField($inputFieldId);
            switch (true) {
                case $inputFieldEntity->getInputTableFieldsInputTableFieldId():
                    /**
                     * Input is table field
                     */
                    $returnArray[$inputFieldId]['table'] = $inputFieldEntity->getinputTablefieldsInputTableFieldId()->getTable();
                    $returnArray[$inputFieldId]['label'] = $inputFieldEntity->getinputTablefieldsInputTableFieldId()->getColumn();
                    $returnArray[$inputFieldId]['value'] = $inputFieldValue;
                    break;
                case $inputFieldEntity->getInputFieldRAttributesInputFieldRAttributeId():
                    /**
                     * Input is attribute
                     */
                    switch ($inputFieldEntity->getInputFieldRAttributesInputFieldRAttributeId()->getAttributeTable()) {
                        case 'COMMON_ATTRIBUTES':
                            $commonAttributeEntity = $inputFieldEntity->getInputFieldRAttributesInputFieldRAttributeId()->getCommonAttributes();
                            $commonAttributeType = $commonAttributeEntity->getCommonAttributeGuiRepresentation()
                                ->getCommonAttributesViewTypeId()
                                ->getCommonAttributeViewType();

                            $label = $commonAttributeEntity->getCommonAttributeDefinitions()->getDescription();
                            $returnArray[$inputFieldId]['label'] = $label;
                            $selectedValues = array();
                            switch ($commonAttributeType) {
                                case 'SELECT':
                                    if ($inputFieldValue !== '') {
                                        $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($inputFieldValue);
                                        $selectedValues[$inputFieldValue] = $commonAttributeValueEntity->getcommonAttributeValuesDefinitions()->getvalue();
                                        $returnArray[$inputFieldId]['value'] = $selectedValues;
                                    }

                                    break;
                                case 'MULTISELECT':
                                    if (! empty($inputFieldValue)) {
                                        foreach ($inputFieldValue as $value) {
                                            $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($value);
                                            $selectedValues[$value] = $commonAttributeValueEntity->getcommonAttributeValuesDefinitions()->getvalue();
                                        }
                                    }
                                    $returnArray[$inputFieldId]['value'] = $selectedValues;
                                    break;
                                case 'CHECKBOX':
                                    if ($inputFieldValue == 1) {
                                        $returnArray[$inputFieldId]['value'] = 'Checked';
                                    } else {
                                        $returnArray[$inputFieldId]['value'] = 'Unchecked';
                                    }
                                    break;
                                case 'TEXT':
                                case 'DATEPICKER':
                                    $returnArray[$inputFieldId]['value'] = $inputFieldValue;
                                    break;
                            }

                            break;
                        case 'ATTRIBUTES_Groups':
                            $attributeGroupEntity = $inputFieldEntity->getInputFieldRAttributesInputFieldRAttributeId()->getAttributeGroupId();
                            $label = $attributeGroupEntity->getattributeGroupDefinition()->getname();
                            $returnArray[$inputFieldId]['label'] = $label;
                            $attributeType = $attributeGroupEntity->getViewAs();
                            switch ($attributeType) {
                                case 'TEXT':
                                    $returnArray[$inputFieldId]['value'] = $inputFieldValue;
                                    break;
                                case 'CHECKBOX':
                                    if ($inputFieldValue == 1) {
                                        $returnArray[$inputFieldId]['value'] = 'Checked';
                                    } else {
                                        $returnArray[$inputFieldId]['value'] = 'Unchecked';
                                    }
                                    break;
                                case 'SELECT':
                                    if (! empty($inputFieldValue)) {
                                        $attributeValue = $this->_attributesGroupMapper->getAttributeValue($inputFieldValue);
                                        $returnArray[$inputFieldId]['value'] = $attributeValue['attributeValue'];
                                    } else {
                                        $returnArray[$inputFieldId]['value'] = '';
                                    }

                                    break;
                            }
                            break;
                    }
                    break;
            }
            $returnArray[$inputFieldId]['entity'] = $inputFieldEntity;
        }

        return $returnArray;
    }

    /**
     * @param $coreAttributeName
     * @param $productOrVariantId
     * @param int $isVariant
     * @return array
     */
    public function getCoreAttributeAssignedToProduct($coreAttributeName, $productOrVariantId, $isVariant = 1)
    {
        $coreAttribute = $this->_attributesGroupMapper->fetchAllCoreAttributes($coreAttributeName);
        $coreAttribute = $coreAttribute[key($coreAttribute)];
        $attributeGroupValues = $this->getAttributeGroupValueAssignedToProduct($coreAttribute['attributeGroupId'], $productOrVariantId, $isVariant);

        if (isset($attributeGroupValues['attributeValue'])) {
            $value = $attributeGroupValues['attributeValue'];
            $attributeExists = 1;
        } else {
            $value = '0';
            $attributeExists = 0;
        }

        return array(
            'name' => $coreAttribute['name'],
            'attributeGroupId' => $attributeGroupValues['attributeGroupId'],
            'value' => $value,
            'attributeExists' => $attributeExists
        );
    }

    /**
     * @param $sku
     * @param $newStatus
     * @param $attributeGroupId
     * @param $attributeExists
     * @return bool
     */
    public function updateEnableCoreAttributeForSKU($sku, $newStatus, $attributeGroupId, $attributeExists)
    {
        try {
            $variantId = $this->getVariantIdFromSku($sku);
            if ($attributeExists) {
                /**
                 * Update table entry
                 */
                $update = $this->_sql->update();
                $update->table($this->_productsAttributesCombinationTable);
                $update->set(array(
                    'attributeValue' => $newStatus
                ));
                $update->where(array(
                    'attributeGroupId' => $attributeGroupId,
                    'productOrVariantId' => $variantId,
                    'isVariant' => 1
                ));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();
            } else {
                /**
                 * Attribute is NOT assigned to SKU - we need to add it to the SKU
                 */
                $enabledAttribute = $this->_attributesGroupMapper->fetchAllCoreAttributes('Enabled');
                if (! empty($enabledAttribute[key($enabledAttribute)])) {
                    $attributeGroupId = $enabledAttribute[key($enabledAttribute)]['attributeGroupId'];
                    $this->_sql->setTable($this->_productsAttributesCombinationTable);
                    $action = $this->_sql->insert();
                    $action->values(array(
                        'attributeGroupId' => $attributeGroupId,
                        'productOrVariantId' => $variantId,
                        'attributeValue' => $newStatus,
                        'groupType' => 'CHECKBOX',
                        'isVariant' => 1,
                        'created' => date('Y-m-d H:i:s'),
                        'modified' => date('Y-m-d H:i:s'),
                        'status' => 'ACTIVE'
                    ));
                    $statement = $this->_sql->prepareStatementForSqlObject($action);
                    $statement->execute();
                }
            }
            return true;
        } catch (\Exception $ex) {
            return false;
        }
        /**
         * Attribute is already assigned to SKU - we need to update the value
         */
    }

    /**
     * @param $style
     * @return bool
     */
    public function getProductIdFromStyle($style)
    {
        $this->_sql->setTable($this->_tableName);
        $select = $this->_sql->select()->where('style = "' . $style . '"');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();
        if (! empty($result['productId'])) {
            return $result['productId'];
        }
        return false;
    }

    /**
     * @param $optionCode
     * @return bool
     */
    public function getFirstVariantIdForOption($optionCode)
    {
        $this->_sql->setTable($this->_productsVariantsRelationshipTable);
        $select = $this->_sql->select()
            ->where('sku LIKE "' . $optionCode . '%"')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute()->next();
        if (! empty($results['productAttributeId'])) {
            return $results['productAttributeId'];
        }
        return false;
    }

    /**
     * @param $productOrVariantId
     * @param $productIdFrom
     * @return array
     */
    public function getUsedWorkflowsFor($productOrVariantId, $productIdFrom)
    {
        $this->_sql->setTable($this->productsRWorkflowsTable);
        $select = $this->_sql->select()->where(array(
            'productIdFrom' => $productIdFrom,
            'productOrVariantId' => $productOrVariantId
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $returnArray = array();
        foreach ($results as $result) {
            $returnArray[$result['workflowId']] = $result['workflowId'];
        }

        return $returnArray;
    }

    /**
     * @param $productOrVariantId
     * @param $productIdFrom
     * @param $commonAttributeId
     * @return bool
     */
    public function checkIfCommonAttributeIsUsed($productOrVariantId, $productIdFrom, $commonAttributeId)
    {
        $this->_sql->setTable($this->productsCommonAttributesTable);
        $select = $this->_sql->select()
            ->where('productOrVariantId = ' . $productOrVariantId)
            ->where('productIdFrom = "' . $productIdFrom . '"')
            ->where('commonAttributeId = ' . $commonAttributeId)
            ->limit(1);

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();

        /**
         * Check if the value is set for the attribute and if it is just blank spaces or data
         */
        if (isset($result['commonAttributeValue']) && strlen(str_replace(' ', '', $result['commonAttributeValue'])) > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $productOrVariantId
     * @param $productIdFrom
     * @param $attributeGroupId
     * @param $attributeViewType
     * @return bool
     */
    public function checkIfAttributeIsUsed($productOrVariantId, $productIdFrom, $attributeGroupId, $attributeViewType)
    {
        /**
         * Set up isVariant Flag depending on value passed in the $productIdFrom var
         */
        $isVariant = 1;
        if ($productIdFrom == 'PRODUCTS') {
            $isVariant = 0;
        }

        $this->_sql->setTable(array(
            'pac' => $this->_productsAttributesCombinationTable
        ));
        $select = $this->_sql->select();

        $select->where(array(
            'pac.attributeGroupId' => $attributeGroupId,
            'pac.productOrVariantId' => $productOrVariantId,
            'pac.isVariant' => $isVariant,
            'pac.status' => 'ACTIVE'
        ));
        /**
         * If the $attributeViewType is TEXT we need to join with another table
         */
        if ($attributeViewType == 'TEXT') {
            $select->join(array(
                'pacd' => $this->_productsAttributesCombinationDefinitions
            ), // Set join table and alias
                new Expression('pac.productAttributeCombinationId = pacd.productAttributeCombinationId
                    AND pacd.status = "ACTIVE"'), array(
                    '*'
                ), Select::JOIN_LEFT);
        }
        $select->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();

        /**
         * Check if value has been set against this attribute
         */
        if (isset($result['attributeValue']) && strlen(str_replace(' ', '', $result['attributeValue'])) > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $mapping
     * @return bool
     */
    public function getCommonAttributeIdFromMapping($mapping)
    {
        $this->_sql->setTable('COMMON_ATTRIBUTES_Mapping');
        $select = $this->_sql->select()
            ->where('commonAttributeMapping = "' . $mapping . '"')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();

        return isset($result['commonAttributeId']) ? $result['commonAttributeId'] : false;
    }

    /**
     * @param $productOrVariantId
     * @param $productIdFrom
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getAllProductPrices($productOrVariantId, $productIdFrom)
    {
        $this->_sql->setTable($this->_productsPricesTable);
        $select = $this->_sql->select()
            ->where('productIdFrom = "' . $productIdFrom . '"')
            ->where('productOrVariantId = ' . $productOrVariantId)
            ->where('status = "ACTIVE"');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     *
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getTerritories()
    {
        $this->_sql->setTable($this->territoriesTable);
        $select = $this->_sql->select();
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * @param $option
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getVariantsIdForOption($option)
    {
        $this->_sql->setTable(array('pr' => $this->_productsVariantsRelationshipTable));
        $select = $this->_sql->select();
        $select->where('pr.sku LIKE "'.$option.'%"');
        $select->join(
            array('p' => $this->_tableName), //Set join table and alias
            'pr.productId = p.productId',
            array('style')
        );
        $select->order(array('pr.productAttributeId ASC'));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     * @param $productOrVariantId
     * @param $productIdFrom
     * @param $attributeData
     * @param $attributeValue
     * @throws \Exception
     */
    public function addAttributeToProductOrVariant($productOrVariantId, $productIdFrom, $attributeData, $attributeValue)
    {
        /**
         * Check if the product/variant already has this attribute assign
         */
        $alreadyAssigned = $this->checkIfAttributeIsUsed($productOrVariantId, $productIdFrom, $attributeData['attributeGroupId'], $attributeData['viewAs']);

        if ($alreadyAssigned) {
            /**
             * The product/variant already has the attribute, in which case we need to update the value
             */
            $successFlag = $this->updateAttributeForProductOrVariant($productOrVariantId, $productIdFrom, $attributeData, $attributeValue);

            if (! $successFlag) {
                /**
                 * Attribute could not be updated - throw exception that will be catched in the bubble
                 */
                throw new \Exception('Attribute could not be edited !');
            }
        } else {
            /**
             * The product doesn't have the attribute, in which case we need to create the value
             */
            $successFlag = $this->createNewAttributeForProductOrVariant($productOrVariantId, $productIdFrom, $attributeData, $attributeValue);
            if (! $successFlag) {
                /**
                 * Attribute could not be created - throw exception that will be catched in the bubble
                 */
                throw new \Exception('Attribute could not be created !');
            }
        }
    }

    /**
     * @param $productOrVariantId
     * @param $productIdFrom
     * @param $attributeData
     * @param $attributeValue
     * @return bool
     */
    private function updateAttributeForProductOrVariant($productOrVariantId, $productIdFrom, $attributeData, $attributeValue)
    {
        try {
            /**
             * Set up flag
             */
            if ($productIdFrom == 'PRODUCTS') {
                $isVariant = 0;
            } else {
                $isVariant = 1;
            }
            /**
             * Check if the attribute is inserted as text or something else
             */
            $this->_sql->setTable($this->_productsAttributesCombinationTable);
            $select = $this->_sql->select()
                ->where('attributeGroupId = ' . $attributeData['attributeGroupId'])
                ->where('productOrVariantId = ' . $productOrVariantId)
                ->where('isVariant = ' . $isVariant)
                ->limit(1);
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $productAttributeCombination = $statement->execute()->next();

            $updateData = array(
                'attributeValue' => $attributeValue
            );
            if ($productAttributeCombination['groupType'] == 'TEXT') {
                /**
                 * If the attribute is TEXT than we need to update $this->_productsAttributesCombinationDefinitions table
                 */
                $this->_sql->setTable($this->_productsAttributesCombinationDefinitions);
                $update = $this->_sql->update();
                $update->table($this->_productsAttributesCombinationDefinitions);
                $update->set($updateData);
                $update->where(array(
                    'productAttributeCombinationId' => $productAttributeCombination['productAttributeCombinationId'],
                    'languageId' => $this->_defaultLanguage
                ));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();
            } else {
                /**
                 * If the attribute is TEXT than we need to update $this->_productsAttributesCombinationTable table
                 */
                $update = $this->_sql->update();
                $update->table($this->_productsAttributesCombinationTable);
                $update->set($updateData);
                $update->where(array(
                    'productAttributeCombinationId' => $productAttributeCombination['productAttributeCombinationId']
                ));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();
            }
            return true;
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
    }

    /**
     * @param $productOrVariantId
     * @param $productIdFrom
     * @param $attributeData
     * @param $attributeValue
     * @return bool
     */
    private function createNewAttributeForProductOrVariant($productOrVariantId, $productIdFrom, $attributeData, $attributeValue)
    {
        try {
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->beginTransaction();
            /**
             * Set up flag
             */
            if ($productIdFrom == 'PRODUCTS') {
                $isVariant = 0;
            } else {
                $isVariant = 1;
            }
            /**
             * Build array to insert data
             */
            $productsAttributesCombinationTableData = array(
                'attributeGroupId' => $attributeData['attributeGroupId'],
                'productOrVariantId' => $productOrVariantId,
                'attributeValue' => $attributeData['viewAs'] == 'TEXT' ? '' : $attributeValue,
                'isVariant' => $isVariant,
                'groupType' => $attributeData['viewAs'],
                'status' => 'ACTIVE'
            );
            /**
             * Insert data *
             */
            $this->_sql->setTable($this->_productsAttributesCombinationTable);
            $action = $this->_sql->insert();
            $action->values($productsAttributesCombinationTableData);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
            /**
             * Retrieve generated id after insert
             */
            $productAttributeCombinationId = $this->_dbAdapter->getDriver()
                ->getConnection()
                ->getLastGeneratedValue();
            /**
             * If the attribute is a TEXT attribute value needs to be inserted into other table
             */
            if ($attributeData['viewAs'] == 'TEXT') {
                $productsAttributesCombinationDefinitionData = array(
                    'productAttributeCombinationId' => $productAttributeCombinationId,
                    'attributeValue' => $attributeValue,
                    'languageId' => $this->_defaultLanguage,
                    'status' => 'ACTIVE'
                );
                $this->_sql->setTable($this->_productsAttributesCombinationDefinitions);
                $action = $this->_sql->insert();
                $action->values($productsAttributesCombinationDefinitionData);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
            }
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->commit();
            return true;
        } catch (\Exception $ex) {
            $this->_dbAdapter->getDriver()
                ->getConnection()
                ->rollback();
            return false;
        }
    }

    /**
     * @param $productOrVariantId
     * @param $productIdFrom
     * @param $commonAttributeId
     * @param $commonAttributeValue
     * @param bool $checkSelect
     * @return bool
     * @throws \Exception
     */
    public function addCommonAttributeToProductOrVariant($productOrVariantId, $productIdFrom, $commonAttributeId, $commonAttributeValue, $checkSelect = true)
    {
        try {
            $isStyle = false;
            if ($productIdFrom == 'PRODUCTS') {
                $isStyle = true;
            }
            $optionCode = '';
            if (!$isStyle) {
                $optionCode = $this->getOptionCodeForVariantId($productOrVariantId);
            }
            $historicalEntriesCollection = new ArrayCollection();
            $commonAttribute = $this->commonAttributesService->getCommonAttribute($commonAttributeId);

            $this->_sql->setTable($this->productsCommonAttributesTable);
            $select = $this->_sql->select()
                ->where('productOrVariantId = ' . $productOrVariantId)
                ->where('productIdFrom = "' . $productIdFrom . '"')
                ->where('commonAttributeId = ' . $commonAttributeId)
                ->limit(1);

            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $productsCommonAttributesEntry = $statement->execute()->next();

            if (! empty($productsCommonAttributesEntry['productsCommonAttributesId']) && $checkSelect) {
                /**
                 * We need to update the current entry *
                 */
                $update = $this->_sql->update();
                $update->table($this->productsCommonAttributesTable);
                $update->set(array(
                    'commonAttributeValue' => $commonAttributeValue
                ));
                $update->where(array(
                    'productsCommonAttributesId' => $productsCommonAttributesEntry['productsCommonAttributesId']
                ));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();
                if ($productsCommonAttributesEntry['commonAttributeValue'] != $commonAttributeValue) {
                    $historicalEntry = $this->historicalService->hydrateHistoricalEntity([
                        'productOrVariantId' => $productOrVariantId,
                        'isStyle' => $isStyle,
                        'commonAttribute' => $commonAttribute,
                        'attributeName' => '',
                        'value' => $commonAttributeValue,
                        'fromDate' => null,
                        'updateType' => HistoricalValue::UPDATE_TYPE_Scheduled,
                        'optionCode' => $optionCode
                    ]);
                    $historicalEntriesCollection->add($historicalEntry);
                }
            } else {
                /**
                 * We need to insert new entry for the common attributes
                 */
                $this->_sql->setTable($this->productsCommonAttributesTable);
                $action = $this->_sql->insert();
                $action->values(array(
                    'productOrVariantId' => $productOrVariantId,
                    'productIdFrom' => $productIdFrom,
                    'commonAttributeId' => $commonAttributeId,
                    'commonAttributeValue' => $commonAttributeValue
                ));
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();

                $historicalEntry = $this->historicalService->hydrateHistoricalEntity([
                    'productOrVariantId' => $productOrVariantId,
                    'isStyle' => $isStyle,
                    'commonAttribute' => $commonAttribute,
                    'attributeName' => '',
                    'value' => $commonAttributeValue,
                    'fromDate' => null,
                    'updateType' => HistoricalValue::UPDATE_TYPE_Scheduled,
                    'optionCode' => $optionCode
                ]);
                $historicalEntriesCollection->add($historicalEntry);
            }
        } catch (\Exception $ex) {
            throw new \Exception('Common attribute could not be created or updated.');
        }

        $this->historicalService->recordChanges($historicalEntriesCollection);
        return true;
    }

    /**
     *
     * @param $params
     * @return bool
     */
    public function triggerWarningAutoFromDate($params)
    {
        if (empty($params['style']) || empty($params['selectedValue'])) {
            return false;
        }
        $productIdFrom = 'PRODUCTS';
        $productId = $this->getProductIdFromStyle($params['style']);

        if (!empty($params['option'])) {
            $variantId = $this->getFirstVariantIdForOption($params['option']);
            $productIdFrom = 'PRODUCTS_R_ATTRIBUTES';
        }
        /** @var \PrismProductsManager\Entity\CommonAttributesValues $commonAttributeValue */
        $newCommonAttributeValue = $this->commonAttributesService->getCommonAttributeValueBy(['commonAttributeValueId' => $params['selectedValue']]);

        if (!empty($newCommonAttributeValue)) {
            $commonAttributeEntity = $newCommonAttributeValue->getCommonattributeid();
            if (!empty($commonAttributeEntity)) {
                $commonAttributeId = $commonAttributeEntity->getCommonattributeid();
                switch ($productIdFrom) {
                    case 'PRODUCTS':
                        $currentValue = $this->getCommonAttributesUsedForProduct($productId, $productIdFrom, $commonAttributeId);
                        break;
                    case 'PRODUCTS_R_ATTRIBUTES':
                        $currentValue = $this->getCommonAttributesUsedForProduct($variantId, $productIdFrom, $commonAttributeId);
                        break;
                }

                if (!empty($currentValue['commonAttributeValue'])) {
                    /** @var \PrismProductsManager\Entity\CommonAttributesValues $currentSavedValue */
                    $currentSavedValue = $this->commonAttributesService
                        ->getCommonAttributeValueBy(['commonAttributeValueId' => $currentValue['commonAttributeValue']]);
                    /** Now we have the old value and the new value - retrieve from dates for both */
                    /** @var CommonAttributesValuesAutoFromDate $currentValueFromDate */
                    $currentValueFromDate = $currentSavedValue->getCommonAttributesValuesAutoFromDate();
                    /** @var CommonAttributesValuesAutoFromDate $newValueFromDate */
                    $newValueFromDate = $newCommonAttributeValue->getCommonAttributesValuesAutoFromDate();

                    if (!empty($currentValueFromDate) && !empty($newValueFromDate)) {

                        if ($newValueFromDate->getFromDate() < $currentValueFromDate->getFromDate()) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param $scheduleEvent
     * @param $productOrVariantId
     * @param $productIdFrom
     * @throws \Exception
     */
    public function addPriceToProductOrVariant($scheduleEvent, $productOrVariantId, $productIdFrom)
    {
        try {
            $historicalEntriesCollection = new ArrayCollection();
            $isStyle = false;
            if ($productIdFrom == 'PRODUCTS') {
                $isStyle = true;
            }
            $optionCode = '';
            if (!$isStyle) {
                $optionCode = $this->getOptionCodeForVariantId($productOrVariantId);
            }
            switch ($scheduleEvent->getSchedulePrices()->getPriceTable()) {
                case 'PRODUCTS_Supplier_Price':
                    /**
                     * Check if the product or variant has a supplier price already set up
                     */
                    $supplierPrices = $this->checkIfProductOrVariantHasSupplierPrice($productOrVariantId, $productIdFrom);
                    $supplierPriceAlreadyExists = isset($supplierPrices['supplierPriceId']) ? $supplierPrices['supplierPriceId'] : false;

                    $supplierPriceColumn = $scheduleEvent->getSchedulePrices()->getPriceColumn();
                    $supplierPriceValue = $scheduleEvent->getSchedulePrices()->getValue();

                    $attributeName = false;
                    switch ($supplierPriceColumn) {
                        case 'currencyCode':
                            $attributeName = 'trading-currency';
                            break;
                        case 'price':
                            $attributeName = 'trading-currency-price';
                            break;
                    }

                    if ($supplierPriceAlreadyExists) {
                        /**
                         * We need to update the value for the supplier Price
                         */
                        $update = $this->_sql->update();
                        $update->table($this->_productSupplierPriceTable);
                        $update->set(array(
                            $supplierPriceColumn => $supplierPriceValue
                        ));
                        $update->where(array(
                            'supplierPriceId' => $supplierPriceAlreadyExists
                        ));
                        $statement = $this->_sql->prepareStatementForSqlObject($update);
                        $statement->execute();

                        if (isset($supplierPrices[$supplierPriceColumn]) &&
                            $supplierPrices[$supplierPriceColumn] != $supplierPriceValue
                        ) {
                            $historicalEntry = $this->historicalService->hydrateHistoricalEntity([
                                'productOrVariantId' => $productOrVariantId,
                                'isStyle' => $isStyle,
                                'commonAttribute' => null,
                                'attributeName' => $attributeName,
                                'value' => $supplierPriceValue,
                                'fromDate' => null,
                                'updateType' => HistoricalValue::UPDATE_TYPE_Scheduled,
                                'optionCode' => $optionCode
                            ]);
                            $historicalEntriesCollection->add($historicalEntry);
                        }
                    } else {
                        /**
                         * We need to create a new entry for the supplier Price
                         */

                        $this->_sql->setTable($this->_productSupplierPriceTable);
                        $action = $this->_sql->insert();
                        $action->values(array(
                            'productIdFrom' => $productIdFrom,
                            'productOrVariantId' => $productOrVariantId,
                            $supplierPriceColumn => $supplierPriceValue
                        ));
                        $statement = $this->_sql->prepareStatementForSqlObject($action);
                        $statement->execute();

                        $historicalEntry = $this->historicalService->hydrateHistoricalEntity([
                            'productOrVariantId' => $productOrVariantId,
                            'isStyle' => $isStyle,
                            'commonAttribute' => null,
                            'attributeName' => $attributeName,
                            'value' => $supplierPriceValue,
                            'fromDate' => null,
                            'updateType' => HistoricalValue::UPDATE_TYPE_Scheduled,
                            'optionCode' => $optionCode
                        ]);
                        $historicalEntriesCollection->add($historicalEntry);
                    }
                    break;
                case 'PRODUCTS_Prices':
                    $productHasPrice = $this->checkIfProductOrVariantHasPrices($productOrVariantId, $productIdFrom, $scheduleEvent->getSchedulePrices());
                    $priceColumn = $scheduleEvent->getSchedulePrices()->getPriceColumn();
                    $priceValue = $scheduleEvent->getSchedulePrices()->getValue();
                    /** @var CurrencyEntity $currency */
                    $currency = $scheduleEvent->getSchedulePrices()->getCurrencyId();
                    $currencyId = $currency->getCurrencyId();
                    $currencyCode = $currency->getIsoCode();
                    /** @var Territories $territory */
                    $territory = $scheduleEvent->getSchedulePrices()->getTerritoryId();
                    $territoryCode = $territory->getIso2code();

                    try {
                        $this->_dbAdapter->getDriver()
                            ->getConnection()
                            ->beginTransaction();
                        if ($productHasPrice) {
                            /**
                             * We need to update the prices
                             */
                            foreach ($productHasPrice as $productPrice) {
                                $this->updatePriceForProductOrVariant($productPrice['productPriceId'], $priceColumn, $priceValue);

                                if (isset($productPrice[$priceColumn]) &&
                                    $productPrice[$priceColumn] !=  $priceValue
                                ) {
                                    $collectionItem = false;
                                    switch ($priceColumn) {
                                        case 'costPrice':
                                            if (($territoryCode == 'GB' && $currencyCode == 'GBP') ||
                                                ($territoryCode == 'US' && $currencyCode == 'USD') ||
                                                ($territoryCode == 'CA' && $currencyCode == 'CAD')
                                            ) {
                                                $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                                                    'productOrVariantId' => $productOrVariantId,
                                                    'isStyle' => $isStyle,
                                                    'commonAttribute' => null,
                                                    'attributeName' => "landed-cost-{$currencyCode}",
                                                    'value' => $priceValue,
                                                    'fromDate' => null,
                                                    'updateType' => HistoricalValue::UPDATE_TYPE_Scheduled,
                                                    'optionCode' => $optionCode
                                                ]);
                                            }
                                            break;
                                        case 'nowPrice':
                                            $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                                                'productOrVariantId' => $productOrVariantId,
                                                'isStyle' => $isStyle,
                                                'commonAttribute' => null,
                                                'attributeName' => "current-price-{$territoryCode}-{$currencyCode}",
                                                'value' => $priceValue,
                                                'fromDate' => null,
                                                'updateType' => HistoricalValue::UPDATE_TYPE_Scheduled,
                                                'optionCode' => $optionCode
                                            ]);
                                            break;
                                        case 'wasPrice':
                                            $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                                                'productOrVariantId' => $productOrVariantId,
                                                'isStyle' => $isStyle,
                                                'commonAttribute' => null,
                                                'attributeName' => "original-price-{$territoryCode}-{$currencyCode}",
                                                'value' => $priceValue,
                                                'fromDate' => null,
                                                'updateType' => HistoricalValue::UPDATE_TYPE_Scheduled,
                                                'optionCode' => $optionCode
                                            ]);
                                            break;
                                    }
                                    if ($collectionItem) {
                                        $historicalEntriesCollection->add($collectionItem);
                                    }
                                }

                            }
                        } else {
                            /**
                             * We need to insert new prices
                             */
                            if (is_null($scheduleEvent->getSchedulePrices()->getTerritoryId())) {
                                $territories = $this->getTerritoriesForCurrency($currencyId);
                                foreach ($territories as $territory) {
                                    $this->createPriceForProductOrVariant(
                                        $productOrVariantId,
                                        $productIdFrom,
                                        $currencyId,
                                        $territory,
                                        $priceColumn,
                                        $priceValue
                                    );
                                    $territory = $this->getTerritoryCodeById($territory);
                                    $territoryCode = isset($territory['iso2Code']) ? $territory['iso2Code'] : false;
                                    $attributeName = false;
                                    switch ($priceColumn) {
                                        case 'nowPrice':
                                            $attributeName = 'current-price';
                                            break;
                                        case 'wasPrice':
                                            $attributeName = 'original-price';
                                            break;
                                    }

                                    if ($territoryCode && $attributeName) {
                                        $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                                            'productOrVariantId' => $productOrVariantId,
                                            'isStyle' => $isStyle,
                                            'commonAttribute' => null,
                                            'attributeName' => "{$attributeName}-{$territoryCode}-{$currencyCode}",
                                            'value' => $priceValue,
                                            'fromDate' => null,
                                            'updateType' => HistoricalValue::UPDATE_TYPE_Scheduled,
                                            'optionCode' => $optionCode
                                        ]);
                                        $historicalEntriesCollection->add($collectionItem);
                                    }
                                }
                            } else {
                                $territory = $scheduleEvent->getSchedulePrices()
                                    ->getTerritoryId()
                                    ->getTerritoryId();

                                $this->createPriceForProductOrVariant(
                                    $productOrVariantId,
                                    $productIdFrom,
                                    $currencyId,
                                    $territory,
                                    $priceColumn,
                                    $priceValue
                                );

                                $attributeName = false;
                                switch ($priceColumn) {
                                    case 'nowPrice':
                                        $attributeName = 'current-price';
                                        break;
                                    case 'wasPrice':
                                        $attributeName = 'original-price';
                                        break;
                                }
                                if ($attributeName) {
                                    $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                                        'productOrVariantId' => $productOrVariantId,
                                        'isStyle' => $isStyle,
                                        'commonAttribute' => null,
                                        'attributeName' => "{$attributeName}-{$territoryCode}-{$currencyCode}",
                                        'value' => $priceValue,
                                        'fromDate' => null,
                                        'updateType' => HistoricalValue::UPDATE_TYPE_Scheduled,
                                        'optionCode' => $optionCode
                                    ]);
                                    $historicalEntriesCollection->add($collectionItem);
                                }

                            }
                        }
                        $this->_dbAdapter->getDriver()
                            ->getConnection()
                            ->commit();
                    } catch (\Exception $ex) {
                        $this->_dbAdapter->getDriver()
                            ->getConnection()
                            ->rollback();
                        throw $ex;
                    }
                    break;
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            throw new \Exception('Error occured !');
        }

        $this->historicalService->recordChanges($historicalEntriesCollection);
    }

    /**
     *
     * @param
     *            $productPriceId
     * @param
     *            $priceColumn
     * @param
     *            $priceValue
     */
    private function updatePriceForProductOrVariant($productPriceId, $priceColumn, $priceValue)
    {
        $update = $this->_sql->update();
        $update->table($this->_productsPricesTable);
        $update->set(array(
            $priceColumn => $priceValue
        ));
        $update->where(array(
            'productPriceId' => $productPriceId
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     * @param $productOrVariantId
     * @param $productIdFrom
     * @param $currencyId
     * @param $territory
     * @param $priceColumn
     * @param $priceValue
     */
    private function createPriceForProductOrVariant($productOrVariantId, $productIdFrom, $currencyId, $territory, $priceColumn, $priceValue)
    {
        $insertData = array(
            'productIdFrom' => $productIdFrom,
            'productOrVariantId' => $productOrVariantId,
            'regionId' => $territory,
            'currencyId' => $currencyId,
            $priceColumn => $priceValue,
            'status' => 'ACTIVE'
        );
        $this->_sql->setTable($this->_productsPricesTable);
        $action = $this->_sql->insert();
        $action->values($insertData);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     * @param $productOrVariantId
     * @param $productIdFrom
     * @param $currencyId
     * @param $territory
     * @param $pricesArray
     */
    private function createStandardPriceForProductOrVariant($productOrVariantId, $productIdFrom, $currencyId, $territory, $pricesArray)
    {
        $array = array(
            'productIdFrom' => $productIdFrom,
            'productOrVariantId' => $productOrVariantId,
            'regionId' => $territory,
            'currencyId' => $currencyId
        );
        $insertData = array_merge($array, $pricesArray);
        $this->_sql->setTable($this->_productsPricesTable);
        $action = $this->_sql->insert();
        $action->values($insertData);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     * @param $currencyId
     * @return array
     */
    private function getTerritoriesForCurrency($currencyId)
    {
        $this->_sql->setTable($this->territoriesRCurrenciesTable);
        $select = $this->_sql->select()->where('currencyId = ' . $currencyId);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $returnArray = array();
        foreach ($results as $result) {
            $returnArray[] = $result['territoryId'];
        }
        return $returnArray;
    }

    /**
     * @param $productOrVariantId
     * @param $productIdFrom
     * @param $schedulePricesEntity
     * @return bool|\Zend\Db\Adapter\Driver\ResultInterface
     */
    private function checkIfProductOrVariantHasPrices($productOrVariantId, $productIdFrom, $schedulePricesEntity)
    {
        $this->_sql->setTable($this->_productsPricesTable);
        $select = $this->_sql->select()
            ->where('productIdFrom = "' . $productIdFrom . '"')
            ->where('productOrVariantId = ' . $productOrVariantId);

        if (! is_null($schedulePricesEntity->getTerritoryId())) {
            $select->where('regionId = ' . $schedulePricesEntity->getTerritoryId()
                    ->getTerritoryId());
        }
        if (! is_null($schedulePricesEntity->getCurrencyId())) {
            $select->where('currencyId = ' . $schedulePricesEntity->getCurrencyId()
                    ->getCurrencyId());
        }

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results->count() == 0) {
            return false;
        }
        return $results;
    }

    /**
     * @param $productOrVariantId
     * @param $productIdFrom
     */
    private function checkIfProductOrVariantHasSupplierPrice($productOrVariantId, $productIdFrom)
    {
        $this->_sql->setTable($this->_productSupplierPriceTable);
        $select = $this->_sql->select()
            ->where('productOrVariantId = ' . $productOrVariantId)
            ->where('productIdFrom = "' . $productIdFrom . '"')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);

        return $statement->execute()->next();
    }

    /**
     * @param $productId
     * @param $newProductName
     * @param string $ProductIdFrom
     * @param string $languageId
     * @param string $versionId
     * @return bool
     */
    public function updateProductNameByProductId($productId, $newProductName, $ProductIdFrom = 'PRODUCTS_R_ATTRIBUTES', $languageId = '1', $versionId = '1')
    {
        $update = $this->_sql->update();
        $update->table($this->_productsDefinitionsTable);
        $update->set(array(
            'productName' => $newProductName
        ));
        $update->where(array(
            'productId' => $productId,
            // 'productIdFrom' => $ProductIdFrom,
            'languageId' => $languageId,
            'versionId' => $versionId
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $result = $statement->execute();
        if ($result->getAffectedRows() > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $productId
     * @param $newShortProductName
     * @param string $ProductIdFrom
     * @param string $languageId
     * @param string $versionId
     * @return bool
     */
    public function updateShortProductNameByProductId($productId, $newShortProductName, $ProductIdFrom = 'PRODUCTS_R_ATTRIBUTES', $languageId = '1', $versionId = '1')
    {
        $update = $this->_sql->update();
        $update->table($this->_productsDefinitionsTable);
        $update->set(array(
            'shortProductName' => $newShortProductName
        ));
        $update->where(array(
            'productId' => $productId,
            // 'productIdFrom' => $ProductIdFrom,
            'languageId' => $languageId,
            'versionId' => $versionId
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $result = $statement->execute();
        if ($result->getAffectedRows() > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $commonAttributeId
     * @param $variantsIds
     * @param $commonAttributeIdValue
     * @param $commonAttributeIdDataType
     * @return bool
     */
    public function isSameValueCrossOptions($commonAttributeId, $variantsIds, $commonAttributeIdValue, $commonAttributeIdDataType)
    {
        return $this->commonAttributesService->isSameValueCrossOptions($commonAttributeId, $variantsIds, $commonAttributeIdValue, $commonAttributeIdDataType);
    }

    /**
     * @param $productOrVariantId
     * @param $commonAttributeId
     * @param $commonAttributeIdValue
     * @return bool
     */
    public function updateCommonAttbuteValueOnStyleLevel($productOrVariantId, $commonAttributeId, $commonAttributeIdValue)
    {
        return $this->commonAttributesService->updateCommonAttbuteValueOnStyleLevel($productOrVariantId, $commonAttributeId, $commonAttributeIdValue);
    }

    /**
     * @param $variantId
     * @return bool|\Zend\Db\Adapter\Driver\ResultInterface
     */
    public function syncToSpectrum($variantId)
    {
        return $this->queueService->addSpectrumEntryToQueueForCommonAttributes($variantId);
    }

    /**
     * @param $variantsIds
     * @param $columnName
     * @param $columnNameValue
     * @param int $languageId
     * @param int $versionId
     * @return bool
     */
    public function isSameColummnValueForAllVariants($variantsIds, $columnName, $columnNameValue, $languageId = 1, $versionId = 1)
    {
        $isSame = false;
        try {
            $query = "SELECT $columnName FROM $this->_productsDefinitionsTable
            	where
                    productId IN($variantsIds)
                    AND productIdFrom = 'PRODUCTS_R_ATTRIBUTES'
                    AND languageId = '$languageId'
                    AND versionId = '$versionId'
                    AND status = 'ACTIVE'
                group by $columnName";

            $result = $this->_dbAdapter->query($query)->execute();

            if ($result->count() == 1) {
                if ($result->current()[$columnName] == $columnNameValue) {
                    $isSame = true;
                }
            }
        } catch (\Exception $ex) {
            return false;
        }
        return $isSame;
    }

    /**
     * @param $productId
     * @param $columnName
     * @param $tableDataValue
     * @param int $languageId
     * @param int $versionId
     * @return bool
     */
    public function UpdateStyleLevelColummnValueOnsameVariantsValue($productId, $columnName, $tableDataValue, $languageId = 1, $versionId = 1)
    {
        $isUpdated = false;
        try {
            $query = "update PRODUCTS_Definitions SET $columnName = '$tableDataValue'
                    	where
                        productId ='$productId'
                        AND productIdFrom = 'PRODUCTS'
                    	AND languageId = '$languageId'
                        AND versionId = '$versionId'
                        AND status = 'ACTIVE'";

            $this->_dbAdapter->query($query)->execute();
            $isUpdated = false;
        } catch (\Exception $ex) {
            return $isUpdated;
        }
        return $isUpdated;
    }

    /**
     * Check if the common attribute "ready for web" is slected
     * @param string $optionCode
     * @return boolean
     *
     * NOTE: The value of the common attribute "ready for web" is been hard coded if the client will chang the vlaue this will stop work
     * Cleint was informed about this issue. ticket: https://www.assembla.com/spaces/long-tall-sally/tickets/3678-sip-product-and-price-data-entries-should-only-be-sent-when-product-is-ready-for-web/details#
     */
    public function isReadyForWeb($optionCode)
    {
        $isready = false;
        try {
            $query = "SELECT
                            t1 . *
                        FROM
                            lts_prism_pms.PRODUCTS_Common_Attributes AS t1
                                JOIN
                            PRODUCTS_R_ATTRIBUTES as t2 ON t1.productOrVariantId = t2.productAttributeId
                        where
                            t2.sku like ('$optionCode%')
                                AND t1.productIdFrom = 'PRODUCTS_R_ATTRIBUTES'
                                AND t1.commonAttributeId = '77'
                        GROUP BY t1.commonAttributeValue";

            $result = $this->_dbAdapter->query($query)->execute();

            if ($result->count() == 1) {
                $current = (array) $result->current();
                if ($current['commonAttributeValue'] == 1) {
                    $isready = true;
                }
            }

        } catch (\Exception $ex) {
            return $isready;
        }
        return $isready;
    }

    /**
     * Get the value of the common attribute with definition Master Style.
     * This been used in QueueServie::addSipEntryToQueue()
     *
     * NOTE: the commont attibute is had coded once changes thi will not work
     * QA UAT should have same commonAttributeId set to allow correct testing. This is not maintainable.
     * MUST be replaced with dynamic solution
     * Hani 05/07/2016
     *
     * @param string $optionCode
     * @throws Exception
     * @return NULL|string
     */
    public function getMasterStyle($optionCode)
    {
        $masterStyle = null;

        try {
            $query = "SELECT
            t1 . *
            FROM
            lts_prism_pms.PRODUCTS_Common_Attributes AS t1
            JOIN
            PRODUCTS_R_ATTRIBUTES as t2 ON t1.productOrVariantId = t2.productAttributeId
            where
            t2.sku like ('$optionCode%')
            AND t1.productIdFrom = 'PRODUCTS_R_ATTRIBUTES'
            AND t1.commonAttributeId = '217'
            GROUP BY t1.commonAttributeValue";

            $result = $this->_dbAdapter->query($query)->execute();

            if ($result->count() == 1) {
                $current = (array) $result->current();
                if (!empty($current['commonAttributeValue'])) {
                    $masterStyle = $current['commonAttributeValue'];
                }
            }

        } catch (\Exception $ex) {
            //$e->getMessage()
            throw new Exception("Couldn't get Master Style for" . $optionCode . "Please Check you data");
        }

        return $masterStyle;
    }

    /**
     * @param $searchString
     * @return array
     */
    public function getVariantData($searchString)
    {
        $variants = $this->getVariantDetailsByStyle($searchString, false);
        $returnArray = array();
        foreach ($variants as $variant) {
            if (!empty($variant['productAttributeId'])) {
                $colourData = $this->getColourForVariantId($variant['productAttributeId']);
                $colour = '';
                if (isset($colourData['name'])) {
                    $colour = $colourData['name'];
                }
                $size = $this->getSizeUsedForVariantId($variant['productAttributeId']);

                $returnArray[] = array(
                    'variantId' => $variant['productAttributeId'],
                    'sku' => $variant['sku'],
                    'colour' => $colour,
                    'size' => $size
                );
            }


        }
        return $returnArray;
    }

    # region Dropship

    /**
     * Create new variant from dropship data
     *
     * @param array $data
     *
     * @return null|string
     * @throws \Exception
     */
    public function createVariantFromDropship(array $data, $sip = false)
    {

        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
        try {
            // Find product IDs by variant SKUs
            $productIds = $this->getProductIdsFromVariantSku(array_merge($data['styleGroup'], [$data['sku']]));

            // If there are more than one parent products in this style group, panic
            if (count($productIds) > 1) {
                throw new \Exception(sprintf('The SKUs %s have more than one parents %s', json_encode($data['styleGroup']),
                    json_encode($productIds)));
            } elseif (count($productIds) === 0) {
                // If product not found, create new product
                $productId = $this->createProduct($data);
            } else {
                $productId = reset($productIds);
            }

            $data['productId'] = $productId;

            $variantId = $this->createProduct($data, false);

            $this->_dbAdapter->getDriver()->getConnection()->commit();

            $this->queueService->addSpectrumEntryToQueueForCommonAttributes($variantId);

            if ($sip) {
                $optionCode = $this->getOptionCodeForVariantId($variantId);
                $this->queueService->addSipEntryToQueue($optionCode, 5, $this);
                $this->queueService->addSipEntryToQueue($optionCode, 6, $this);
            }
            return $variantId;
        } catch (\Exception $e) {
            $this->_dbAdapter->getDriver()->getConnection()->rollback();
            throw $e;
        }
    }

    /**
     * Update variant from dropship data
     *
     * @param string $id
     * @param array  $data
     * @param bool   $checkDropshipSource
     *
     * @throws \Exception
     */
    public function updateVariantFromDropship($id, array $data, $checkDropshipSource = true)
    {
        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
        try {
            if ($checkDropshipSource) {
                // Check if this variant is from dropship
                $dropshipSource = $this->getDropshipSource($id);
                // If not from dropship, panic
                if (!$dropshipSource) {
                    throw new \Exception(sprintf('Variant %s is not from dropship', $id));
                }
                if ($dropshipSource !== $data['attributes']['dropship']) {
                    throw new \Exception(sprintf("Variant %s is from other dropship '%s'", $id, $dropshipSource));
                }
            }

            // Update variant
            $this->dbUpdateVariant($id, $data);

            $this->processProductRelatedData($id, $data, true);

            $this->_dbAdapter->getDriver()->getConnection()->commit();

            $this->queueService->addSpectrumEntryToQueueForCommonAttributes($id);
        } catch (\Exception $e) {
            $this->_dbAdapter->getDriver()->getConnection()->rollback();
            throw $e;
        }
    }

    /**
     * @param array $data
     * @param bool  $isProduct
     *
     * @return null|string
     * @throws \Exception
     */
    public function createProduct(array $data, $isProduct = true)
    {
        if ($isProduct) {
            /** check is style is already in */
            $productId = $this->getProductIdFromStyle($data['style']);
            if (!$productId) {
                $productId = $this->dbInsertProduct($data);
            }
        } else {
            $productId = $this->dbInsertVariant($data['productId'], $data);
        }

        $this->processProductRelatedData($productId, $data, !$isProduct);

        if ($isProduct) {
            // Add product to channels
            $this->createProductsLinksToChannels($productId);
        }

        return $productId;
    }

    /**
     * Process product definition, price, SKU rules and attributes
     *
     * @param string $id
     * @param array  $data
     * @param bool   $isVariant
     *
     * @throws \Exception
     */
    public function processProductRelatedData($id, array $data, $isVariant)
    {
        $updatePricesFlag = false;

        // Update price
        foreach ($data['prices'] as $currencyCode => $prices) {
            $currencyId = $this->getCurrencyIdByCurrencyCode($currencyCode);
            $territoryIds = $this->getTerritoriesForCurrency($currencyId);
            foreach ($territoryIds as $territoryId) {
                if (isset($data['options']['nowPriceUpdate']) && isset($data['options']['wasPriceUpdate'])) {
                    $priceArray = array();
                    if ($data['options']['nowPriceUpdate']) {
                        $priceArray['price'] = $prices['nowPrice'];
                        $priceArray['nowPrice'] = $prices['nowPrice'];
                    }
                    if ($data['options']['wasPriceUpdate']) {
                        $priceArray['wasPrice'] = $prices['wasPrice'];
                    }
                    if (!empty($priceArray)) {
                        $this->dbUpsertProductPrice($id, $isVariant, $currencyId, $territoryId, $priceArray);
                        $updatePricesFlag = true;
                    }
                } else {
                    $this->dbUpsertProductPrice($id, $isVariant, $currencyId, $territoryId, [
                        'price'    => $prices['nowPrice'],
                        'nowPrice' => $prices['nowPrice'],
                        'wasPrice' => $prices['wasPrice'],
                    ]);
                }

            }
        }

        // Update product definition
        $this->dbUpsertProductDefinition($id, [
            'shortProductName' => $data['productName'],
            'productName'      => $data['productName'],
            'shortDescription' => isset($data['productShortDescription']) ? $data['productShortDescription'] : '',
            'longDescription'  => isset($data['productDescription']) ? $data['productDescription'] : $data['productLongDescription'],
            'status'           => 'ACTIVE',
        ], $isVariant);

        // Prep images
        $data['attributes']['lowRes'] = implode(PHP_EOL, array_column($data['images'], 'lowRes'));
        $data['attributes']['highRes'] = implode(PHP_EOL, array_column($data['images'], 'highRes'));

        // Update attributes
        $errors = [];

        // Check color
        $colorCode = $data['colour']['colourCode'];
        $colorName = $data['colour']['colourName'];
        $colorId = $this->getColorId($colorCode, $colorName);
        if ($colorId === false) {
            $errors[] = sprintf("Invalid colour code '%s' and name '%s'", $colorCode, $colorName);
        }

        // Check size
        $sizeCode = $data['size']['sizeCode'];
        $sizeValue = $data['size']['sizeValue'];
        $sizeId = $this->getSizeId($sizeCode, $sizeValue);
        if ($sizeId === false) {
            $sizeId = $this->createNewSize($sizeCode, $sizeValue);
            if ($sizeId === false) {
                $errors[] = sprintf("Invalid size code '%s' and value '%s'", $sizeCode, $sizeValue);
            }

        }

        $attributes = [];
        foreach ($data['attributes'] as $attribute => $value) {
            $attributeId = $this->getCommonAttributeIdFromMapping($attribute);
            if (!$attributeId) {
                continue;
            }
            $commonAttributeValue = $this->getCommonAttributeValue($attributeId, $value);
            if ($commonAttributeValue === false) {
                // Continue without error
                //$errors[] = sprintf("Invalid value '%s' for attribute '%s'", $value, $attribute);
                continue;
            }
            $attributes[$attributeId] = $commonAttributeValue;
        }

        if (empty($errors)) {
            if ($isVariant) {
                // Add color
                $this->dbUpsertVariantSkuRule($id, $colorId, true);
                // Add size
                $this->dbUpsertVariantSkuRule($id, $sizeId, false);
            }

            $table = $isVariant ? $this->_productsVariantsRelationshipTable : $this->_tableName;
            foreach ($attributes as $attributeId => $value) {
                if (is_array($value)) {
                    $this->deleteCommonAttributeValuesAssignedToProducts($id, $table, $attributeId);
                    foreach ($value as $actualValue) {
                        $this->addCommonAttributeToProductOrVariant($id, $table, $attributeId, $actualValue, false);
                    }
                } else {
                    $this->addCommonAttributeToProductOrVariant($id, $table, $attributeId, $value);
                }
            }
        }

        if (!empty($errors)) {
            throw new \Exception(json_encode($errors));
        }
    }

    /**
     * @param $sizeCode
     * @param $sizeValue
     * @return int
     */
    private function createNewSize($sizeCode, $sizeValue)
    {
        /** Get the first common attribute group for sizes */
        $sql = $this->_sql;
        $sql->setTable($this->commonAttributesTable);
        $select = $sql->select()
            ->columns(array('commonAttributeId'))
            ->where(array(
                'isSize' => 1
            ));
        $select->limit(1);
        $statement = $sql->prepareStatementForSqlObject($select);

        $commonAttributeId = $statement->execute()->next();

        if (!empty($commonAttributeId['commonAttributeId'])) {
            /** @var array $commonAttributeValueArray */
            $commonAttributeValueArray = array(
                'value' => empty($sizeValue) ? $sizeCode : $sizeValue,
                'commonAttributeValueSkuCode' => $sizeCode,
                'commonAttributeValueAbbrev' => '',
                'commonAttributeValueBaseSize' => ''
            );
            $this->commonAttributesService->createNewCommonAttributeValue(
                $commonAttributeValueArray,
                $commonAttributeId['commonAttributeId']
            );
            return $this->_dbAdapter->getDriver()
                ->getConnection()
                ->getLastGeneratedValue();
        }
    }

    /**
     * @param $id
     * @param $table
     * @param $attributeId
     */
    private function deleteCommonAttributeValuesAssignedToProducts($id, $table, $attributeId)
    {
        $whereClause = array(
            'productOrVariantId' => $id,
            'productIdFrom' => $table,
            'commonAttributeId' => $attributeId
        );
        $this->_sql->setTable($this->productsCommonAttributesTable);
        $action = $this->_sql->delete()->where($whereClause);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
    }
    /**
     * Find all product IDs from variant SKUs
     *
     * @param array $skuArray
     *
     * @return array
     */
    public function getProductIdsFromVariantSku(array $skuArray)
    {
        $sql = new Sql($this->_dbAdapter);

        $select = $sql->select();
        $select->columns(['productId']);
        $select->from($this->_productsVariantsRelationshipTable);
        $select->where->in('sku', $skuArray);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $productIds = [];

        foreach ($result as $row) {
            $productIds[] = $row['productId'];
        }

        return array_unique($productIds);
    }

    /**
     * Convert value to id if applicable
     *
     * @param string $attributeId
     * @param mixed  $value
     *
     * @return bool|\DateTime|string|array
     */
    public function getCommonAttributeValue($attributeId, $value)
    {

        list($type, $availableValues) = $this->getTypeAndAvailableValuesForAttribute($attributeId);

        switch ($type) {
            case 'CHECKBOX':
                $availableValues = ['0', '1'];
                if (!in_array($value, $availableValues)) {
                    return false;
                }

                return $value;

            case 'SELECT':
                $arrayKey = array_search($value, $availableValues);
                if ($arrayKey === false) {
                    return false;
                }
                return $arrayKey;

            case 'TEXT':
            case 'TEXTAREA':
                return $value;

            case 'MULTISELECT':
                if (!is_array($value)) {
                    return false;
                }
                $values = [];
                foreach ($value as $choice) {
                    if (!array_key_exists($choice, $availableValues)) {
                        return false;
                    }
                    $values[] = $availableValues[$choice];
                }

                return $values;

            case 'DATEPICKER':
            case 'DATEPICKER - DATE ONLY':
                $timestamp = strtotime($value);
                if ($timestamp === false) {
                    return false;
                }
                $datetime = new \DateTime('@' . $timestamp);

                return $datetime;

            case 'IMAGE':
                // @todo cover this case, for now it's just not valid
            case 'PRODUCT':
                return $value;
            default:
                return false;
        }
    }

    /**
     * Get type and allowed values if applicable
     *
     * @param string $attributeId
     *
     * @return array
     */
    public function getTypeAndAvailableValuesForAttribute($attributeId)
    {
        $cacheKey = 'type_and_available_values_' . $attributeId;

        if (!array_key_exists($cacheKey, $this->_cache)) {
            $sql = new Sql($this->_dbAdapter);

            $select = $sql->select();
            $select->columns([]);
            $select->from(['CAGR' => $this->commonAttributesGuiRepresentationTable]);
            $select->join(
                ['CAVT' => $this->commonAttributesViewTypeTable],
                'CAGR.commonAttributesViewTypeId = CAVT.commonAttributeViewTypeId',
                ['type' => 'commonAttributeViewType']
            );
            $select->join(
                ['CAV' => 'COMMON_ATTRIBUTES_VALUES'],
                'CAGR.commonAttributeId = CAV.commonAttributeId',
                ['id' => 'commonAttributeValueId', 'abbrevCode' => 'commonAttributeValueAbbrev'],
                Select::JOIN_LEFT
            );
            $select->join(
                ['CAVD' => $this->commonAttributeValuesDefinitionsTable],
                'CAV.commonAttributeValueId = CAVD.commonAttributeValueId',
                ['value'],
                Select::JOIN_LEFT
            );
            $select->where->equalTo('CAGR.commonAttributeId', $attributeId);

            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();

            $type = null;
            $availableValues = [];
            foreach ($result as $row) {
                $type = $row['type'];
                $availableValues[$row['value']] = $row['id'];
                //add abbrevCode from COMMON_ATTRIBUTES_VALUES to result
                $abbCode[$row['id']] = $row['abbrevCode'];
            }

            $this->_cache[$cacheKey] = [$type, $availableValues, $abbCode];
        }

        return $this->_cache[$cacheKey];
    }

    /**
     * Get Dropship source or null if not found
     *
     * @param string $id
     *
     * @return string|null
     */
    public function getDropshipSource($id)
    {
        $sql = new Sql($this->_dbAdapter);

        $select = $sql->select();
        $select->from(['PCA' => 'PRODUCTS_Common_Attributes']);
        $select->columns([
            'id'    => 'commonAttributeId',
            'value' => 'commonAttributeValue',
        ]);
        $select->join(['CAM' => 'COMMON_ATTRIBUTES_Mapping'], 'CAM.commonAttributeId = PCA.commonAttributeId', []);
        $select->join(['CAGR' => 'COMMON_ATTRIBUTES_GUI_Representation'], 'CAGR.commonAttributeId = PCA.commonAttributeId', []);
        $select->join([
            'CAVT' => 'COMMON_ATTRIBUTES_View_Type'],
            'CAVT.commonAttributeViewTypeId = CAGR.commonAttributesViewTypeId',
            ['type' => 'commonAttributeViewType']
        );
        $select->join([
            'CAVD' => 'COMMON_ATTRIBUTES_VALUES_Definitions'],
            'CAVD.commonAttributeValueId = PCA.commonAttributeValue',
            ['actual_value' => 'value']
        );
        $select->where->equalTo('PCA.productOrVariantId', $id);
        $select->where->AND->equalTo('PCA.productIdFrom', 'PRODUCTS_R_ATTRIBUTES');
        $select->where->AND->equalTo('CAM.commonAttributeMapping', 'dropship');

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        foreach ($result as $row) {
            return $row['actual_value'];
        }

        return null;
    }

    /**
     * Find color by code and/or name. Return false if not found or null if both code and name are empty
     *
     * @param string|null $colorCode
     * @param string|null $colorName
     *
     * @return string|bool
     */
    public function getColorId($colorCode, $colorName)
    {
        if (empty($colorCode) && empty($colorName)) {
            return null;
        }

        $sql = new Sql($this->_dbAdapter);

        $select = $sql->select();
        $select->columns(['colourGroupId']);
        $select->from(['cg' => $this->_coloursGroupsTable]);
        $select->join(['cgd' => $this->_coloursGroupsDefinitionsTable], 'cgd.colourGroupId = cg.colourGroupId', []);
        if (!empty($colorCode)) {
            $select->where->equalTo('cg.groupCode', $colorCode);
        }
        if (!empty($colorName)) {
            $select->where->equalTo('cgd.name', $colorName);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        foreach ($result as $row) {
            return $row['colourGroupId'];
        }

        return false;
    }

    /**
     * Get size id by code and/or value. Return false if not found or null if both code and value are empty
     *
     * @param string|null $sizeCode
     * @param string|null $sizeValue
     *
     * @return string|bool
     */
    public function getSizeId($sizeCode, $sizeValue)
    {
        if (empty($sizeCode) && empty($sizeValue)) {
            return null;
        }

        $sql = new Sql($this->_dbAdapter);

        $select = $sql->select();
        $select->columns([]);
        $select->from(['ca' => $this->commonAttributesTable]);
        $select->join(
            ['cav' => 'COMMON_ATTRIBUTES_VALUES'],
            'cav.commonAttributeId = ca.commonAttributeId',
            [
                'id' => 'commonAttributeValueId',
                'code' => 'commonAttributeValueSkuCode',
            ]
        );
        $select->join([
            'cavd' => $this->commonAttributeValuesDefinitionsTable],
            'cavd.commonAttributeValueId = cav.commonAttributeValueId',
            ['value']
        );
        $select->where->equalTo('ca.isSize', 1);
        if (!empty($sizeCode)) {
            $select->where->equalTo('cav.commonAttributeValueSkuCode', $sizeCode);
        }
        if (!empty($sizeValue)) {
            $select->where->equalTo('cavd.value', $sizeValue);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        foreach ($result as $row) {
            return $row['id'];
        }

        return false;
    }

    # endregion

    # region Product

    /**
     * Insert new entry into table PRODUCTS
     * @param array $data
     * @return string|null
     */
    public function dbInsertProduct(array $data)
    {
        $sql = new Sql($this->_dbAdapter);

        $insert = $sql->insert();
        $insert->into($this->_tableName);
        $insert->values([
            'productTypeId'             => 1,
            'productMerchantCategoryId' => null,
            'taxId'                     => 0,
            'ean13'                     => $data['barcode'],
            'style'                     => $data['style'],
            'status'                    => $data['active'] == 1 ? 'ACTIVE' : 'INACTIVE',
            'created'                   => new Expression('NOW()'),
            'modified'                  => new Expression('NOW()'),
        ]);

        $statement = $sql->prepareStatementForSqlObject($insert);
        $result = $statement->execute();
        $productId = $result->getGeneratedValue();

        return $productId;
    }

    # endregion

    # region Variant

    /**
     * Insert new entry into table PRODUCTS_R_ATTRIBUTES
     *
     * @param string $productId
     * @param array  $data
     *
     * @return string|null
     */
    public function dbInsertVariant($productId, array $data)
    {
        $sql = new Sql($this->_dbAdapter);

        $insert = $sql->insert();
        $insert->into($this->_productsVariantsRelationshipTable);
        $insert->values([
            'productId' => $productId,
            'taxId'     => 0,
            'quantity'  => 0,
            'ean13'     => $data['barcode'],
            'sku'       => strtoupper($data['sku']),
            'status'    => $data['active'] == 1 ? 'ACTIVE' : 'INACTIVE',
            'created'   => new Expression('NOW()'),
            'modified'  => new Expression('NOW()')
        ]);

        $statement = $sql->prepareStatementForSqlObject($insert);
        $result = $statement->execute();
        $variantId = $result->getGeneratedValue();

        return $variantId;
    }

    /**
     * Update variant by id
     *
     * @param string $id
     * @param array  $data
     *
     * @return bool
     */
    public function dbUpdateVariant($id, array $data)
    {

        $sql = new Sql($this->_dbAdapter);

        $update = $sql->update();
        $update->table($this->_productsVariantsRelationshipTable);

        $runQuery = false;

        /** Check if the active status update flag is set */
        if (isset($data['options']['activeStatusUpdate'])) {

            /** If flag is true update only status */
            if ($data['options']['activeStatusUpdate']) {
                $update->set([
                    'status'   => $data['active'] == '1' ? 'ACTIVE' : 'INACTIVE',
                    'modified' => new Expression('NOW()'),
                ]);
                $runQuery = true;
            }
        } else {
            $update->set([
                'status'   => $data['active'] == '1' ? 'ACTIVE' : 'INACTIVE',
                'modified' => new Expression('NOW()'),
            ]);
            $runQuery = true;
        }

        $update->where->equalTo('productAttributeId', $id);

        if (!$runQuery) {
            return false;
        }
        $statement = $sql->prepareStatementForSqlObject($update);

        $result = $statement->execute();

        return $result->getAffectedRows() === 1;
    }

    # endregion

    # region Product Definition

    /**
     * Attempt to insert/update PRODUCTS_Definitions base on product/variant ID & flag
     *
     * @param string $productOrVariantId
     * @param array  $data
     * @param bool   $isVariant
     *
     * @return bool|mixed|null
     */
    public function dbUpsertProductDefinition($productOrVariantId, array $data, $isVariant)
    {
        $productDefinitionId = $this->getProductDefinition($productOrVariantId, $isVariant);
        if ($productDefinitionId === false) {
            return $this->dbInsertProductDefinition($productOrVariantId, $data, $isVariant);
        } else {
            return $this->dbUpdateProductDefinition($productDefinitionId, $data);
        }
    }

    /**
     * Find product definition by product/variant id
     *
     * @param string $productOrVariantId
     * @param bool   $isVariant
     *
     * @return string|bool
     */
    public function getProductDefinition($productOrVariantId, $isVariant)
    {
        $sql = new Sql($this->_dbAdapter);

        $table = $isVariant ? $this->_productsVariantsRelationshipTable : $this->_tableName;

        $select = $sql->select();
        $select->columns(['id' => 'productDefinitionsId']);
        $select->from(['pd' => $this->_productsDefinitionsTable]);
        $select->where->equalTo('pd.productId', $productOrVariantId);
        $select->where->equalTo('pd.productIdFrom', $table);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        foreach ($result as $row) {
            return $row['id'];
        }

        return false;
    }

    /**
     * Insert new entry to PRODUCTS_Definitions, return generated ID
     *
     * @param $productOrVariantId
     * @param $data
     * @param $isVariant
     *
     * @return mixed|null
     */
    public function dbInsertProductDefinition($productOrVariantId, $data, $isVariant)
    {
        $table = $isVariant ? $this->_productsVariantsRelationshipTable : $this->_tableName;

        $sql = new Sql($this->_dbAdapter);

        $insert = $sql->insert();
        $insert->into($this->_productsDefinitionsTable);
        $insert->values([
            'productId'          => $productOrVariantId,
            'productIdFrom'      => $table,
            'languageId'         => $this->_currentLanguage,
            'versionId'          => 1,
            'versionDescription' => 'default',
            'shortProductName'   => $data['shortProductName'],
            'productName'        => $data['productName'],
            'urlName'            => '',
            'metaTitle'          => '',
            'metaDescription'    => '',
            'metaKeyword'        => '',
            'shortDescription'   => $data['shortDescription'],
            'longDescription'    => $data['longDescription'],
            'misSpells'          => '',
            'tags'               => '',
            'status'             => isset($data['status']) ? $data['status'] : 'ACTIVE',
            'created'            => new Expression('NOW()'),
            'modified'           => new Expression('NOW()'),
        ]);

        $statement = $sql->prepareStatementForSqlObject($insert);
        $result = $statement->execute();
        $productDefinitionId = $result->getGeneratedValue();

        return $productDefinitionId;
    }

    /**
     * Update product/variant definition by id
     *
     * @param string $id Product definition ID
     * @param array  $data
     *
     * @return bool
     */
    public function dbUpdateProductDefinition($id, array $data)
    {
        $sql = new Sql($this->_dbAdapter);

        $update = $sql->update();
        $update->table($this->_productsDefinitionsTable);
        $update->set([
            'shortProductName' => $data['shortProductName'],
            'productName'      => $data['productName'],
            'shortDescription' => $data['shortDescription'],
            'longDescription'  => $data['longDescription'],
            'status'           => isset($data['status']) ? $data['status'] : 'ACTIVE',
            'modified'         => new Expression('NOW()'),
        ]);
        $update->where->equalTo('productDefinitionsId', $id);

        $statement = $sql->prepareStatementForSqlObject($update);
        $result = $statement->execute();

        return $result->getAffectedRows() === 1;
    }

    # endregion

    # region Product Price

    /**
     * Attempt to insert/update product price base on product/variant ID & flag, currency & region
     *
     * @param string $productOrVariantId
     * @param bool   $isVariant
     * @param string $currencyId
     * @param string $territoryId
     * @param array $data
     *
     * @return bool|null|string
     */
    public function dbUpsertProductPrice($productOrVariantId, $isVariant, $currencyId, $territoryId, array $data)
    {
        $productPriceId = $this->getProductPrice($productOrVariantId, $isVariant, $currencyId, $territoryId);
        if ($productPriceId === false) {
            return $this->dbInsertProductPrice($productOrVariantId, $isVariant, $currencyId, $territoryId, $data);
        } else {
            return $this->dbUpdateProductPrice($productPriceId, $data);
        }
    }

    /**
     * Find product price by product/variant ID & flag, currency & region
     *
     * @param string $productOrVariantId
     * @param bool   $isVariant
     * @param string $currencyId
     * @param string $territoryId
     *
     * @return string|bool
     */
    public function getProductPrice($productOrVariantId, $isVariant, $currencyId, $territoryId)
    {
        $sql = new Sql($this->_dbAdapter);

        $table = $isVariant ? $this->_productsVariantsRelationshipTable : $this->_tableName;

        $select = $sql->select();
        $select->columns(['id' => 'productPriceId']);
        $select->from($this->_productsPricesTable);
        $select->where->equalTo('productIdFrom', $table);
        $select->where->equalTo('productOrVariantId', $productOrVariantId);
        $select->where->equalTo('regionId', $territoryId);
        $select->where->equalTo('currencyId', $currencyId);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        foreach ($result as $row) {
            return $row['id'];
        }

        return false;
    }

    /**
     * Insert new entry to PRODUCTS_Prices
     *
     * @param string $productOrVariantId
     * @param bool   $isVariant
     * @param string $currencyId
     * @param string $territoryId
     * @param array  $data
     *
     * @return string|null
     */
    public function dbInsertProductPrice($productOrVariantId, $isVariant, $currencyId, $territoryId, array $data)
    {
        $table = $isVariant ? $this->_productsVariantsRelationshipTable : $this->_tableName;

        $sql = new Sql($this->_dbAdapter);

        // Filter data by keys
        $insertData = array_intersect_key($data, array_flip([
            'price',
            'offerPrice',
            'nowPrice',
            'wasPrice',
            'costPrice',
            'tradingCostPrice',
            'wholesalePrice',
            'freightPrice',
            'dutyPrice',
            'designPrice',
            'packagingPrice',
        ]));
        // Add data
        $insertData = array_merge($insertData, [
            'productIdFrom'      => $table,
            'productOrVariantId' => $productOrVariantId,
            'regionId'           => $territoryId,
            'currencyId'         => $currencyId,
            'status'             => isset($data['status']) ? $data['status'] : 'ACTIVE',
            'created'            => new Expression('NOW()'),
            'modified'           => new Expression('NOW()'),
        ]);

        $insert = $sql->insert();
        $insert->into($this->_productsPricesTable);
        $insert->values($insertData);

        $statement = $sql->prepareStatementForSqlObject($insert);
        $result = $statement->execute();
        $productDefinitionId = $result->getGeneratedValue();

        return $productDefinitionId;
    }

    /**
     * Update product price by productPriceId
     *
     * @param string $id Product Price ID
     * @param array  $data
     *
     * @return bool
     */
    public function dbUpdateProductPrice($id, array $data)
    {
        $sql = new Sql($this->_dbAdapter);

        $updateData = array_intersect_key($data, array_flip([
            'price',
            'offerPrice',
            'nowPrice',
            'wasPrice',
            'costPrice',
            'tradingCostPrice',
            'wholesalePrice',
            'freightPrice',
            'dutyPrice',
            'designPrice',
            'packagingPrice',
        ]));
        $updateData = array_merge($updateData, [
            'status'   => isset($data['status']) ? $data['status'] : 'ACTIVE',
            'modified' => new Expression('NOW()'),
        ]);

        $update = $sql->update();
        $update->table($this->_productsPricesTable);
        $update->set($updateData);
        $update->where->equalTo('productPriceId', $id);

        $statement = $sql->prepareStatementForSqlObject($update);
        $result = $statement->execute();

        return $result->getAffectedRows() === 1;
    }

    # endregion

    # region Variant SKU Rule

    /**
     * Attempt to create/update PRODUCTS_R_ATTRIBUTES_R_SKU_Rules base on variant id and color/size flag
     *
     * @param string $variantId
     * @param string $id
     * @param bool   $isColor
     *
     * @return bool|null|string
     *
     */
    public function dbUpsertVariantSkuRule($variantId, $id, $isColor)
    {
        if ($id === null) {
            return null;
        }
        $skuRuleId = $this->getVariantSkuRule($variantId, $isColor);
        if ($skuRuleId === false) {
            // Insert
            return $this->dbInsertVariantSkuRule($variantId, $id, $isColor);
        } else {
            // Update
//            return $this->dbUpdateVariantSkuRule($skuRuleId, $id, $isColor);
        }
    }

    /**
     * Find variant SKU rule by table (size & color)
     *
     * @param string $variantId
     * @param bool   $isColor
     *
     * @return string|bool
     */
    public function getVariantSkuRule($variantId, $isColor)
    {
        $sql = new Sql($this->_dbAdapter);

        $table = $isColor ? $this->_coloursGroupsTable : 'COMMON_ATTRIBUTES_VALUES';

        $select = $sql->select();
        $select->columns(['id' => 'productAttributeSkuRuleId']);
        $select->from(['pas' => $this->_productsAttributesSkuRuleRelationsTable]);
        $select->where->equalTo('pas.productAttributeId', $variantId);
        $select->where->equalTo('pas.skuRuleTable', $table);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        foreach ($result as $row) {
            return $row['id'];
        }

        return false;
    }

    /**
     * Insert new entry to PRODUCTS_R_ATTRIBUTES_R_SKU_Rules and return the new id
     *
     * @param string $variantId
     * @param string $id
     * @param bool   $isColor
     *
     * @return string|null
     */
    public function dbInsertVariantSkuRule($variantId, $id, $isColor)
    {
        $sql = new Sql($this->_dbAdapter);

        $table = $isColor ? $this->_coloursGroupsTable : 'COMMON_ATTRIBUTES_VALUES';

        $insert = $sql->insert();
        $insert->into($this->_productsAttributesSkuRuleRelationsTable);
        $insert->values([
            'productAttributeId' => $variantId,
            'skuRuleId'          => $id,
            'skuRuleTable'       => $table,
            'isCommonAttribute'  => $isColor ? 0 : 1,
            'created'            => new Expression('NOW()'),
            'modified'           => new Expression('NOW()'),
        ]);

        $statement = $sql->prepareStatementForSqlObject($insert);
        $result = $statement->execute();
        $variantSkuRuleId = $result->getGeneratedValue();

        return $variantSkuRuleId;
    }

    /**
     * Update PRODUCTS_R_ATTRIBUTES_R_SKU_Rules with new skuRuleId
     *
     * @param string $variantSkuRuleId
     * @param string $skuRuleId
     * @param bool   $isColor
     *
     * @return bool
     */
    public function dbUpdateVariantSkuRule($variantSkuRuleId, $skuRuleId, $isColor)
    {
        $sql = new Sql($this->_dbAdapter);

        $update = $sql->update();
        $update->table($this->_productsAttributesSkuRuleRelationsTable);
        $update->set([
            'skuRuleId'         => $skuRuleId,
            'isCommonAttribute' => $isColor ? 0 : 1,
            'modified'          => new Expression('NOW()'),
        ]);
        $update->where->equalTo('productAttributeSkuRuleId', $variantSkuRuleId);

        $statement = $sql->prepareStatementForSqlObject($update);
        $result = $statement->execute();

        return $result->getAffectedRows() === 1;
    }

    /**
     * @param mixed $processValue
     * @param string $productIdFrom
     * @param array $styleOrOptionIds
     * @param string $territoryCurrencyValue
     * @return bool|\Zend\Db\Adapter\Driver\ResultInterface
     */
    public function bulkUpdatePrice($processValue, $productIdFrom, $styleOrOptionIds, $territoryCurrencyValue, $submittedUserId = null)
    {
        $result = false;
        $historicalEntriesCollection = new ArrayCollection();
        $isStyle = false;
        if ($productIdFrom == 'PRODUCTS') {
            $isStyle = true;
        }
        $optionCode = '';
        $count = count($territoryCurrencyValue);
        for ($i = 0; $i < $count; $i++) {

            $regionId = $this->getTerritoryId($territoryCurrencyValue[$i]['territory']);
            $territoryCode = $territoryCurrencyValue[$i]['territory'];

            if (empty($regionId)) return $result;

            $currencyId = $this->getCurrencyIdFromCode($territoryCurrencyValue[$i]['currency']);
            $currencyCode = $territoryCurrencyValue[$i]['currency'];

            if (empty($currencyId)) {
                return $result;
            }

            $column = $territoryCurrencyValue[$i]['column'];

            $attributeName = false;
            switch ($column) {
                case 'nowPrice':
                        $attributeName = "current-price-{$territoryCode}-{$currencyCode}";
                    break;
                case 'wasPrice':
                    $attributeName = "original-price-{$territoryCode}-{$currencyCode}";
                    break;
                case 'costPrice':
                    if (($territoryCode == 'GB' && $currencyCode == 'GBP') ||
                        ($territoryCode == 'US' && $currencyCode == 'USD') ||
                        ($territoryCode == 'CA' && $currencyCode == 'CAD')
                    ) {
                        $attributeName = "landed-cost-{$currencyCode}";
                    }
                    break;
            }

            $data = array(
                $column => $processValue
            );

            try {
                $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();

                foreach ($styleOrOptionIds as $styleOrOptionId) {

                    $this->_sql->setTable($this->_productsPricesTable);
                    $select = $this->_sql->select();
                    $select->where->equalTo('productIdFrom', $productIdFrom);
                    $select->where->equalTo('regionId', $regionId);
                    $select->where->equalTo('currencyId', $currencyId);
                    $select->where->equalTo('productOrVariantId', $styleOrOptionId);
                    $select->where->equalTo('status', 'ACTIVE');
                    if (!$isStyle) {
                        $optionCode = $this->getOptionCodeForVariantId($styleOrOptionId);
                    }
                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $results = $statement->execute();
                    if ($results->count() > 0) {
                        /** Product already has values - we need to update them. */
                        $olderData = $results->next();

                        $sql = new Sql($this->_dbAdapter);
                        $update = $sql->update();
                        $update->table($this->_productsPricesTable);
                        $update->set($data);
                        $update->where->equalTo('productIdFrom', $productIdFrom);
                        $update->where->equalTo('regionId', $regionId);
                        $update->where->equalTo('currencyId', $currencyId);
                        $update->where->equalTo('productOrVariantId', $styleOrOptionId);

                        $statement = $sql->prepareStatementForSqlObject($update);
                        $statement->execute();

                        /**
                         * If changes are done at GBP-GB 1-1 we need to cascade to
                         * ROW-GBP 2-1 spectrum will use this price for countries in tax region RW
                         * EU-GBP 3-1 spectrum will use this price for countries in tax region DK, GB, RW, SE
                         */
                        if ($regionId == 1 && $currencyId == 1) {
                            $this->bulkUpdateRelatedTerritoryCurrency($column, $styleOrOptionId, $data, $productIdFrom, 1, 2);
                            $this->bulkUpdateRelatedTerritoryCurrency($column, $styleOrOptionId, $data, $productIdFrom, 1, 3);
                        }

                        /**
                         * If changes are done at USD-US 5-2 we need to cascade to
                         * GB-USD 1-2 spectrum will use this price for countries in tax region
                         */

                        if ($regionId == 5 && $currencyId == 2) {
                            $this->bulkUpdateRelatedTerritoryCurrency($column, $styleOrOptionId, $data, $productIdFrom, 2, 1);
                        }

                        if ($attributeName &&
                            isset($olderData[$column]) &&
                            $olderData[$column] != $processValue
                        ) {
                            /** New value will be updated. Add to historical entries. */
                            $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                                'productOrVariantId' => $styleOrOptionId,
                                'isStyle' => $isStyle,
                                'commonAttribute' => null,
                                'attributeName' => $attributeName,
                                'value' => $processValue,
                                'fromDate' => null,
                                'updateType' => HistoricalValue::UPDATE_TYPE_Bulk,
                                'optionCode' => $optionCode,
                                'userId' => $submittedUserId
                            ]);
                            $historicalEntriesCollection->add($collectionItem);
                        }

                    } else {
                        /** Product has no values - we need to insert them. */
                        $insertArray = array(
                            'productIdFrom' => $productIdFrom,
                            'productOrVariantId' => $styleOrOptionId,
                            'regionId' => $regionId,
                            'currencyId' => $currencyId,
                            $column => $processValue
                        );
                        $this->_sql->setTable($this->_productsPricesTable);
                        $action = $this->_sql->insert();
                        $action->values($insertArray);
                        $statement = $this->_sql->prepareStatementForSqlObject($action);

                        $statement->execute();
                        if (!empty($attributeName)) {
                            /** New value will be updated. Add to historical entries. */
                            $collectionItem = $this->historicalService->hydrateHistoricalEntity([
                                'productOrVariantId' => $styleOrOptionId,
                                'isStyle' => $isStyle,
                                'commonAttribute' => null,
                                'attributeName' => $attributeName,
                                'value' => $processValue,
                                'fromDate' => null,
                                'updateType' => HistoricalValue::UPDATE_TYPE_Bulk,
                                'optionCode' => $optionCode,
                                'userId' => $submittedUserId
                            ]);
                            $historicalEntriesCollection->add($collectionItem);
                        }

                    }
                }
                $this->_dbAdapter->getDriver()->getConnection()->commit();
            } catch (\Exception $ex) {
                $this->_dbAdapter->getDriver()->getConnection()->rollback();
                return false;
            }
        }

        $this->historicalService->recordChanges($historicalEntriesCollection);
        return true;
    }

    /**
     * update price per currency and region this will be used with  GBP-GB, GB-USD
     * @param string $column
     * @param int $styleOrOptionId
     * @param array $data
     * @param string $productIdFrom
     * @param int $regionId
     * @param int $currencyId
     * @return bool
     */
    protected function bulkUpdateRelatedTerritoryCurrency(
        $column,
        $styleOrOptionId,
        $data,
        $productIdFrom,
        $regionId,
        $currencyId
    ) {
        if ($column == 'nowPrice' || $column == 'wasPrice') {

            /*
             * USE IF WE WANT TO COMBINE WITH INSERT
            // check if record exit
            $this->_sql->setTable($this->_productsPricesTable);
            $select = $this->_sql->select();
            $select->where->equalTo('productIdFrom', $productIdFrom);
            $select->where->equalTo('regionId', $regionId);
            $select->where->equalTo('currencyId', $currencyId);
            $select->where->equalTo('productOrVariantId', $styleOrOptionId);
            $select->where->equalTo('status', 'ACTIVE');
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            */
            try {
                $sql = new Sql($this->_dbAdapter);
                $update = $sql->update();
                $update->table($this->_productsPricesTable);
                $update->set($data);
                $update->where->equalTo('productIdFrom', $productIdFrom);
                $update->where->equalTo('regionId', $regionId);
                $update->where->equalTo('currencyId', $currencyId);
                $update->where->equalTo('productOrVariantId', $styleOrOptionId);

                $statement = $sql->prepareStatementForSqlObject($update);
                $statement->execute();
            } catch (\Exception $ex) {
                return false;
            }
        }
        return true;

    }

    /**
     * to be used with updateNormalPrices()
     * @param string $productIdFrom
     * @param int $productOrVariantId
     * @param int $regionId
     * @param int $currencyId
     * @param array $updateData
     */
    protected function normalUpdateRelatedTerritoryCurrency($productIdFrom, $productOrVariantId, $regionId, $currencyId, $updateData)
    {
        $this->_sql->setTable($this->_productsPricesTable);
        $select = $this->_sql->select()
            ->where(array(
                'productIdFrom' => $productIdFrom,
                'productOrVariantId' => $productOrVariantId,
                'regionId' => $regionId,
                'currencyId' => $currencyId,
                'status' => 'ACTIVE'
            ))
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $resultCount = $result->count();

        if ($resultCount > 0) {
            if(!empty($updateData)){
                $this->_sql->setTable($this->_productsPricesTable);
                $update = $this->_sql->update()
                    ->set($updateData)
                    ->where(array(
                        'productIdFrom' => $productIdFrom,
                        'productOrVariantId' => $productOrVariantId,
                        'regionId' => $regionId,
                        'currencyId' => $currencyId,
                        'status' => 'ACTIVE'
                    ));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();
            }

        }else {
            $insertData = array(
                'productIdFrom' => $productIdFrom,
                'productOrVariantId' => $productOrVariantId,
                'regionId' => $regionId,
                'currencyId' => $currencyId,
                'price' => '',
                'costPrice' => '',
                'wasPrice' => '',
                'nowPrice' => '',
                'offerPrice' => '',
                'tradingCostPrice' => '',
                'wholesalePrice' => '',
                'freightPrice' => '',
                'dutyPrice' => '',
                'designPrice' => '',
                'packagingPrice' => '',
                'status' => 'ACTIVE',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            );
                if (! empty($updateData['costPrice'])) {
                    $insertData['costPrice'] = number_format($updateData['costPrice'], 2);
                }
                if (! empty($updateData['wasPrice'])) {
                    $insertData['wasPrice'] = number_format($updateData['wasPrice'], 2);
                }
                if (! empty($updateData['nowPrice'])) {
                    $insertData['nowPrice'] = number_format($updateData['nowPrice'], 2);
                    $insertData['price'] = number_format($updateData['nowPrice'], 2);
                }

                $this->_sql->setTable($this->_productsPricesTable);
                $action = $this->_sql->insert();
                $action->values($insertData);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
        }

    }
}
