<?php
namespace PrismProductsManager\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\EventManager\EventManagerInterface;
use Zend\Db\Sql\Select;
use PHPExcel_IOFactory;
use PHPExcel_Style_Fill;
use PHPExcel;
use PrismMediaManager\Service\RSMapper;

/**
 * Class AttributesGroupMapper
 * @package PrismProductsManager\Model
 */
class AttributesGroupMapper
{
    /**
     * @var string
     * Default Table name
     */

    protected $_attributesGroupTable = 'ATTRIBUTES_Groups';
    /**
     * @var string
     * Core / Marketing Attributes Table Name
     */
    protected $_attributesTable = 'ATTRIBUTES';
    /**
     * @var string
     * Core / Marketing Attributes Definitions Table name
     */
    protected $_attributesDefinitionsTable = 'ATTRIBUTES_Definitions';
    /**
     * @var string
     * Core / Marketing Attributes Validations Table name
     */
    protected $_attributesGroupValidation = 'ATTRIBUTES_Groups_Validations';
    /**
     * @var string
     * Core / Merchant Attributes Table name
     */
    protected $_attributesMerchantsTable = 'ATTRIBUTES_Merchants';
    /**
     * @var string
     * Default Table Definitions
     */
    protected $_attributeGroupDefinitionsTable = 'ATTRIBUTES_Group_Definitions';
    /**
     * @var string
     */
    protected $_productsAttributesSkuRules = 'PRODUCTS_R_ATTRIBUTES_R_SKU_Rules';
    /**
     * @var string
     */
    protected $_productsAttributesCombinationTable = 'PRODUCTS_ATTRIBUTES_Combination';
    /**
     * @var string
     */
    protected $_productsAttributesCombinationDefinitions = 'PRODUCTS_ATTRIBUTES_Combination_Definitions';
    /**
     * @var string
     */
    protected $_productsCategoriesRelationshipTable = 'PRODUCTS_R_CATEGORIES';
    /**
     * @var string
     */
    protected $_productsVariantsRelationshipTable = 'PRODUCTS_R_ATTRIBUTES';
    /**
     * @var string
     */
    protected $_categoriesRelationshipAttributesTable = 'CATEGORIES_R_ATTRIBUTES';

    /**
     * @var \Zend\Db\Adapter\Adapter
     */
    protected $_dbAdapter;
    /**
     * @var \Zend\Db\Sql\Sql
     */
    protected $_sql;
    /**
     * @var array
     * Submited data from the forms
     */
    protected $_submittedData;
    /**
     * @var array
     * Languages from the config file
     */
    protected $_languages;
    /**
     * @var object
     * Entities for the specified tables
     */
    protected $AttributesGroupEntity, $attributesGroupDefinitionsEntity, $attributesEntity, $attributesDefinitionsEntity, $attributesMerchantsEntity;

    /**
     * @var object
     * Zend 2 ClassMethods object
     */
    protected $hydrator;
    /**
     * @var ServiceLocatorInterface $serviceLocator
     */
    protected $serviceLocator;
    /**
     * @var array
     * Used for deleting groups or attributes
     */
    protected $_deleteData;

    /**
     * @var object
     * Used to read xls files
     */
    protected $_excelReader;

    /**
     * @var RSMapper
     */
    protected $rsMapper;

    /**
     * @var string
     */
    protected $_coloursGroupsTable = 'COLOURS_Groups';
    /**
     * @var string
     */
    protected $_coloursGroupsDefinitionsTable = 'COLOURS_Groups_Definitions';
    /**
     * @var string
     */
    protected $_coloursTable = 'COLOURS';
    /**
     * @var string
     */
    protected $_coloursDefinitionsTable = 'COLOURS_Definitions';
    /**
     * @var string
     */
    protected $_coloursRGroupsTable = 'COLOURS_R_GROUPS';

    /**
     * @var array
     */
    protected $_processingTimeIds = array();
    /**
     * @param AttributesGroupEntity $AttributesGroupEntity
     * @param AttributesGroupDefinitionsEntity $attributesGroupDefinitionsEntity
     * @param AttributesMarketingEntity $attributesMarketingEntity
     * @param AttributesMarketingDefinitionsEntity $attributesMarketingDefinitionsEntity
     * @param AttributesMerchantsEntity $attributesMerchantsEntity
     * @param Adapter $dbAdapter
     * @param $languages
     * @param \PHPExcel $excel
     * @internal param $coloursMapper
     * @internal param $shoeMapper
     */
    public function __construct(AttributesGroupEntity $AttributesGroupEntity,
                                AttributesGroupDefinitionsEntity $attributesGroupDefinitionsEntity,
                                AttributesMarketingEntity $attributesMarketingEntity,
                                AttributesMarketingDefinitionsEntity $attributesMarketingDefinitionsEntity,
                                AttributesMerchantsEntity $attributesMerchantsEntity,
                                Adapter $dbAdapter,
                                $languages ,
                                PHPExcel $excel,
                                RSMapper $rsMapper)
    {

        //Set variables from the factory inject
        $this->AttributesGroupEntity = $AttributesGroupEntity;
        $this->attributesGroupDefinitionsEntity = $attributesGroupDefinitionsEntity;
        $this->attributesMarketingEntity = $attributesMarketingEntity;
        $this->attributesMarketingDefinitionsEntity = $attributesMarketingDefinitionsEntity;
        $this->attributesMerchantsEntity = $attributesMerchantsEntity;

        $this->_excelReader = $excel;
        $this->hydrator = new ClassMethods(false);
        $this->_dbAdapter = $dbAdapter;
        $this->_languages = $languages;
        $this->_sql = new Sql($dbAdapter);
        $this->_sql->setTable($this->_attributesGroupTable);

        $this->rsMapper = $rsMapper;
    }

    /**
     * @param $submittedData
     * @returns int $attributesGroupId
     */
    public function saveAttributeGroup($submittedData)
    {
        $this->_submittedData = $submittedData;

        //Processing and saving for the Attributes_Group Tabel
        $this->_processAttributesGroup();
        $attributesGroupId = $this->_saveAttributesGroup();

        //Processing and saving for the Attributes_Group_Definitions Tabel
        $this->_processAttributesGroupDefinitions($attributesGroupId);
        $this->_saveAttributesGroupDefinitions();

        //Processing and saving for the Attributes Table
        $this->_processAttributes($attributesGroupId);
        $attributesIds = $this->_saveAttributes();

        $this->_saveAttributeValidation($attributesGroupId, 'add');

        //Saving for the Attributes_Definitions Table
        $this->_saveAttributesDefinitions($attributesIds);

        return $attributesGroupId;
    }

    /**
     * @param $attributesGroupId
     * @param $action
     */
    private function _saveAttributeValidation($attributesGroupId, $action)
    {
        $data = array(
            'attributeGroupId' => $attributesGroupId,
            'isRequired' => $this->_submittedData['isRequired'],
            'validationType' => $this->_submittedData['validationType']);

        switch ($action) {
            case 'add':
                $this->_sql->setTable($this->_attributesGroupValidation);
                $action = $this->_sql->insert();
                $action->values($data);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
                break;
            case 'edit':
                unset($data['attributeGroupId']);
                $update = $this->_sql->update();
                $update->table($this->_attributesGroupValidation);
                $update->set($data);
                $update->where(array(
                    'attributeGroupId' => $attributesGroupId,
                ));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();
                break;
        }
    }


    /**
     * @param $submittedData
     * @param $id
     * @return bool
     */
    public function saveAttributeValues($submittedData, $id)
    {
        $this->_submittedData = $submittedData;

        if (!empty($submittedData['attributeGroupId']) && !empty($submittedData['attributeValueId'])) {
            $this->_updateAttribute();
        } else {

            $this->_processAttributesValue($id);
            $attributeId = $this->_saveAttributesValue();

            $this->_saveAttributesValueDefinitions($attributeId, $id);
        }
        if ($id === null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     *
     */
    protected function _updateAttribute()
    {
        $attributeMarketingDefinition = array (
            'attributeValue' => $this->_submittedData['attributeValue']
        );
        $update = $this->_sql->update();
        $update->table($this->_attributesDefinitionsTable);
        $update->set($attributeMarketingDefinition);
        $update->where(array(
            'attributeId' => $this->_submittedData['attributeValueId'],
            'languageId' => $this->_languages['default-language']
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        try {
            $statement->execute();
        } catch (\Exception $e) {
            die('Error: ' . $e->getMessage());
        }
    }

    /**
     * @param $data
     * @return bool
     */
    public function delete($data)
    {
        //Switches and deletes either group or attribute
        $this->_deleteData = $data;
        switch ($data['delete']) {
            case 'group':
                return $this->__deleteGroup();
                break;
            case 'attribute':
                return $this->__checkAttr();
                break;
        }
    }

    /**
     * @return bool
     */
    private function __checkAttr()
    {
        $this->_attributesGroupTable = $this->_attributesTable;
        $this->_sql->setTable($this->_attributesGroupTable);

        $action = $this->_sql->select()
            ->join(
                array('pac' => $this->_productsAttributesCombinationTable),
                $this->_attributesTable . '.attributeGroupId = pac.attributeGroupId',
                array('isUsedAsVariant' => 'productOrVariantId'),
                Select::JOIN_LEFT
            )
            ->where($this->_attributesTable . '.attributeId = ' . $this->_deleteData['id']);

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        $ready = true;
        foreach ($result as $res) {
            if (!empty($res['isUsedAsVariant'])) {
                $ready = false;
                break;
            }
            $attributes[] = $res;
        }
        if ($ready) {
            if (!empty($attributes)) {
                foreach ($attributes as $attribute) {
                    // Delete every attribute of the group
                    $this->__deleteAttribute($attribute);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    private function __deleteGroup()
    {

        $this->_sql->setTable($this->_attributesTable);

        $action = $this->_sql->select()
            ->join(
                array('amd' => $this->_attributesDefinitionsTable),
                $this->_attributesTable . '.attributeId = amd.attributeId'
            )
            ->join(
                array('ag' => $this->_attributesGroupTable),
                $this->_attributesTable . '.attributeGroupId = ag.attributeGroupId',
                array('*'),
                Select::JOIN_LEFT
            )
            ->join(
                array('pac' => $this->_productsAttributesCombinationTable),
                $this->_attributesTable . '.attributeGroupId = pac.attributeGroupId',
                array('isUsedAsVariant' => 'productOrVariantId'),
                Select::JOIN_LEFT
            )
            ->where('ag.groupType = "Marketing"')
            ->where('ag.attributeGroupId = ' . $this->_deleteData['id']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $attributes = array();
        $ready = true;
        foreach ($result as $res) {

            //Verify if the group as used attributes
            if (!empty($res['isUsedAsVariant'])) {
                $ready = false;
                break;
            }
            $attributes[] = $res;
        }

        if ($ready) {
            if (!empty($attributes)) {
                foreach ($attributes as $attribute) {
                    // Delete every attribute of the group
                    $this->__deleteAttribute($attribute);
                }
            }

            //If the group has children subgroups when deleted
            //the children must jump in the groups parent
            $this->__childrenGroupJumpToParent();


            //Delete the actual group from the group tables
            $this->__deleteActualGroup(array($this->_attributeGroupDefinitionsTable, $this->_attributesGroupTable));


            return true;
        } else {
            return false;
        }
    }

    /**
     *
     */
    private function __childrenGroupJumpToParent()
    {
        $this->_sql->setTable($this->_attributesGroupTable);

        //Getting all the childs of the group
        $action = $this->_sql->select()
            ->where($this->_attributesGroupTable . '.groupParentId = ' . $this->_deleteData['id']);

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $childs = array();
        foreach ($result as $res) {
            $childs[] = $res;
        }

        //Getting the parent info to see what parent id the group has.
        $action = $this->_sql->select()
            ->where($this->_attributesGroupTable . '.attributeGroupId = ' . $this->_deleteData['id']);

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $parents = array();
        foreach ($result as $res) {
            $parents[] = $res;
        }

        foreach ($childs as $child) {
            foreach ($parents as $parent) {
                $attributeGroup = array(
                    'groupParentId' => $parent['groupParentId']
                );
                $update = $this->_sql->update();
                $update->table($this->_attributesGroupTable);
                $update->set($attributeGroup);
                $update->where(array('attributeGroupId' => $child['attributeGroupId']));

                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();        // works fine
            }
        }
    }

    /**
     * @param $tables
     */
    private function __deleteActualGroup($tables)
    {
        foreach ($tables as $table) {
            //Delete From table Attributes_Group_definitions the actual Group
            $update = $this->_sql->update();
            $update->table($table);
            $update->set(array('status' => 'DELETED'));
            $update->where(array('attributeGroupId' => $this->_deleteData['id']));
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            $statement->execute();
        }
    }

    /**
     * @param $attribute
     */
    private function __deleteAttribute($attribute)
    {
        //Delete from table Attributes_Marketing_Definitions

        $update = $this->_sql->update();
        $update->table($this->_attributesDefinitionsTable);
        $update->set(array('status' => 'DELETED'));
        $update->where(array('attributeId' => $attribute['attributeId']));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();

        //Delete from table $this->_attributesTable
        $update = $this->_sql->update();
        $update->table($this->_attributesTable);
        $update->set(array('status' => 'DELETED'));
        $update->where(array('attributeId' => $attribute['attributeId']));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     * @param $id
     */
    protected function _processAttributesValue($id)
    {
        if ($id === null) {
            $this->attributesMarketingEntity->setAttributeGroupId($this->_submittedData['attributeGroupId']);
        } else {
            $this->attributesMarketingEntity->setAttributeGroupId($id);
        }
    }

    /**
     * @param $attributeId
     * @throws \Exception
     */
    protected function _saveAttributesValueDefinitions($attributeId)
    {
        $this->_attributesGroupTable = $this->_attributesDefinitionsTable;
        $this->_sql->setTable($this->_attributesGroupTable);

        $this->attributesMarketingDefinitionsEntity->setAttributeId($attributeId);
        $this->attributesMarketingDefinitionsEntity->setAttributeValue($this->_submittedData['attributeValue']);

        foreach ($this->_languages['site-languages'] as $language) {
            $this->attributesMarketingDefinitionsEntity->setLanguageId($language);
            $data = $this->hydrator->extract($this->attributesMarketingDefinitionsEntity);
            $action = $this->_sql->insert();
            $action->values($data);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        }
    }

    /**
     * @return int|string
     * @throws \Exception
     */
    protected function _saveAttributesValue()
    {
        $this->_attributesGroupTable = $this->_attributesTable;
        $this->_sql->setTable($this->_attributesGroupTable);
        $data = $this->hydrator->extract($this->attributesMarketingEntity);
        $action = $this->_sql->insert();
        $action->values($data);

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();

        $id = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
        return $id;
    }

    /**
     * @param $submittedData
     * @return bool
     */
    public function updateAttributeGroup($submittedData)
    {
        $this->_submittedData = $submittedData;
        $attributeGroup = array(
            'groupParentId' => $submittedData['attributeGroupParent'],
            'viewAs' => $submittedData['attributeType']
        );
        $attributeGroupDefinition = array(
            'name' => $submittedData['attributeInternalName'],
            'publicName' => $submittedData['attributePublicName']
        );

        $group = $this->getGroupById($submittedData['attributeId']);

        if (($group['viewAs'] == 'SELECT' || $group['viewAs'] == 'RADIO') && ($submittedData['attributeType'] == 'TEXT' || $submittedData['attributeType'] == 'CHECKBOX')) {
            return false;
        }

        $this->_updateGroup($attributeGroup, $submittedData['attributeId']);
        $this->_updateGroupDefinition($attributeGroupDefinition, $submittedData['attributeId']);
        $this->_saveAttributeValidation($group['attributeGroupId'], 'edit');
        return true;
    }

    /**
     * @param $attributeGroup
     * @param $id
     */
    protected  function _updateGroup($attributeGroup, $id)
    {
        $update = $this->_sql->update();
        $update->table($this->_attributesGroupTable);
        $update->set($attributeGroup);
        $update->where(array('attributeGroupId' => $id));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     * @param $attributeGroupDefinition
     * @param $id
     * @param $languageId
     */
    protected  function _updateGroupDefinition($attributeGroupDefinition, $id, $languageId = false)
    {
        $update = $this->_sql->update();
        $update->table($this->_attributeGroupDefinitionsTable);
        $update->set($attributeGroupDefinition);
        $update->where(array('attributeGroupId' => $id));
        if ($languageId !== false) {
            $update->where(array('languageId' => $languageId));
        }
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     * Processing Attributes_Group Table
     */
    private function _processAttributesGroup()
    {
        $this->AttributesGroupEntity->setGroupParentId($this->_submittedData['attributeGroupParent']);
        $this->AttributesGroupEntity->setViewAs($this->_submittedData['attributeType']);
    }

    /**
     * @return int
     * @throws \Exception
     * Saving Attributes_Group Table
     */
    private function _saveAttributesGroup()
    {
        $data = $this->hydrator->extract($this->AttributesGroupEntity);
        if ($data['groupType'] == 'Marketing') {
            $action = $this->_sql->insert();
            $action->values($data);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
            return $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
        }
    }

    /**
     * @param $attributesGroupId
     * Processing Attributes_Group_Definitions Table
     */
    private function _processAttributesGroupDefinitions($attributesGroupId)
    {
        $this->attributesGroupDefinitionsEntity->setAttributeGroupId($attributesGroupId);
        $this->attributesGroupDefinitionsEntity->setName($this->_submittedData['attributeInternalName']);
        $this->attributesGroupDefinitionsEntity->setPublicName($this->_submittedData['attributePublicName']);
    }

    /**
     * @throws \Exception
     * Saving Attributes_Group_Definitions Table
     */
    private function _saveAttributesGroupDefinitions()
    {
        $this->_attributesGroupTable = $this->_attributeGroupDefinitionsTable;
        $this->_sql->setTable($this->_attributesGroupTable);
        $data = $this->hydrator->extract($this->attributesGroupDefinitionsEntity);

        foreach ($this->_languages['site-languages'] as $languages) {
            $data['languageId'] = $languages;
            $action = $this->_sql->insert();
            $action->values($data);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        }
    }

    /**
     * @param $attributesGroupId
     * Processing Attributes Table
     */
    private function _processAttributes($attributesGroupId)
    {
        if ($this->_submittedData['attributeType'] == 'TEXT' || $this->_submittedData['attributeType'] == 'CHECKBOX') {
            $this->_submittedData['attributeValues'] = array(0 => $this->_submittedData['attributePublicName']);
        } else {
            $this->_submittedData['attributeValues'] = explode(PHP_EOL, $this->_submittedData['attributeValues']);
        }
        $this->attributesMarketingEntity->setAttributeGroupId($attributesGroupId);
    }

    /**
     * @return array
     * @throws \Exception
     * Saving Attributes_ Table
     */
    private function _saveAttributes()
    {
        $tmp = array();
        $this->_attributesGroupTable = $this->_attributesTable;
        $this->_sql->setTable($this->_attributesGroupTable);
        $data = $this->hydrator->extract($this->attributesMarketingEntity);

            foreach($this->_submittedData['attributeValues'] as $values) {
                if (strlen(str_replace(' ','',$values)) > 1) {
                    $action = $this->_sql->insert();
                    $action->values($data);
                    $statement = $this->_sql->prepareStatementForSqlObject($action);
                    $statement->execute();

                    $tmp['ids'][] = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
                }
            }

        return $tmp;
    }

    /**
     * @param $attributesIds
     * @throws \Exception.
     * Saving Attributes_Definitions Table
     */
    private function _saveAttributesDefinitions($attributesIds)
    {
        $this->_attributesGroupTable = $this->_attributesDefinitionsTable;
        $this->_sql->setTable($this->_attributesGroupTable);
        foreach ($attributesIds['ids'] as $key=>$attributesId) {
            foreach ($this->_languages['site-languages'] as $language) {
                $this->attributesMarketingDefinitionsEntity->setAttributeId($attributesId);
                $this->attributesMarketingDefinitionsEntity->setLanguageId($language);

                if (strlen(str_replace(' ','',$this->_submittedData['attributeValues'][$key])) > 1) {
                    $this->attributesMarketingDefinitionsEntity->setAttributeValue($this->_submittedData['attributeValues'][$key]);
                    $data = $this->hydrator->extract($this->attributesMarketingDefinitionsEntity);
                    $action = $this->_sql->insert();
                    $action->values($data);

                    $statement = $this->_sql->prepareStatementForSqlObject($action);
                    $statement->execute();
                }
            }
        }
    }

    /**
     * @param $attributeGroupId
     * @return array
     */
    public function getAllProductsWithThisAttribute($attributeGroupId)
    {
        $this->_sql->setTable($this->_productsAttributesCombinationTable);
        $action = $this->_sql->select()
            ->where(array('attributeGroupId' => $attributeGroupId));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();
        $finalProductIdsArray = array();
        foreach ($results as $result) {
            if ($result['isVariant'] == 0) {
                $finalProductIdsArray[] = $result['productOrVariantId'];
            } else {
                // get the product Id from variant Id
                $variantId = $result['productOrVariantId'];
                $this->_sql->setTable($this->_productsVariantsRelationshipTable);
                $action = $this->_sql->select()
                    ->where(array('productAttributeId' => $variantId))
                    ->limit(1);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $variantDetails = $statement->execute()->next();
                $finalProductIdsArray[] = $variantDetails['productId'];
            }
        }

        $finalProductIdsArray = array_unique($finalProductIdsArray);
        return $finalProductIdsArray;
    }

    /**
     * @param bool $asArray
     * @param array $columns
     * @return array|\Zend\Db\Adapter\Driver\ResultInterface
     */
    public function fetchAll($asArray = false, $columns = array())
    {
        $action = $this->_sql->select()
            ->join(
                array('agd' => $this->_attributeGroupDefinitionsTable),
                $this->_attributesGroupTable . '.attributeGroupId = agd.attributeGroupId'
            )
            ->where('agd.languageId = '. $this->_languages['default-language'])
            ->where($this->_attributesGroupTable . '.groupType = "Marketing"')
            ->order($this->_attributesGroupTable . '.groupParentId ASC, ' . $this->_attributesGroupTable . '.attributeGroupId ASC');

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        if ($asArray) {
            $finalArray = array();
            foreach ($result as $group) {
                if (!empty($columns)) {
                    $finalEntry = array();
                    foreach ($columns as $column) {
                        if(array_key_exists($column,$group)) {
                            $finalEntry[$column] = $group[$column];
                        }
                    }
                    $finalArray[] = $finalEntry;
                } else {
                    $finalArray = $group;
                }
            }
            return $finalArray;
        } else {
            return $result;
        }
    }

    /**
     * @param null $search
     * @return array|null
     */
    public function fetchAllMarketingAttributes($search = null , $orderBy = null, $orderbyOption = null)
    {
        if($orderBy === null){
            $orderBy = 'agd.modified';
        }

        if($orderbyOption === null){
            $orderbyOption = 'desc';
        }

        $action = $this->_sql->select()
            ->join(
                array('agd' => $this->_attributeGroupDefinitionsTable),
                $this->_attributesGroupTable . '.attributeGroupId = agd.attributeGroupId'
            )
            ->join(
                array('am' => $this->_attributesTable),
                new \Zend\Db\Sql\Expression($this->_attributesGroupTable . '.attributeGroupId = am.attributeGroupId AND am.status != "DELETED"'),
                array('attributesCount' => new \Zend\Db\Sql\Expression('COUNT(attributeId)')),
                Select::JOIN_LEFT
            )
            ->join(
                array('agv' => $this->_attributesGroupValidation),
                'agv.attributeGroupId = am.attributeGroupId',
                array('validationType', 'isRequired', 'attributeGroupValidationId'),
                Select::JOIN_LEFT
            )
            ->where('agd.languageId = '. $this->_languages['default-language'])
            ->where($this->_attributesGroupTable . '.groupType = "Marketing"')
            ->where('agd.status != "DELETED"')
            ->group($this->_attributesGroupTable . '.attributeGroupId');

        // Search
        if ($search !== null) {
            $action->where
                ->nest
                ->like('agd.name', '%' . $search . '%')
                ->or
                ->like('agd.publicName', '%' . $search . '%')
                ->unnest;
        }
        $action->order($orderBy ." " .  $orderbyOption);
        $a = $this->_sql->getSqlStringForSqlObject($action);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        return $this->_processGroupAttributes($result, $search);
    }

    /**
     * @param null $search
     * @param $orderBy
     * @param $orderbyOption
     * @return array|null
     */
    public function fetchAllMerchantsAttributes($search = null, $orderBy = null, $orderbyOption = null)
    {
        if($orderBy === null){
            $orderBy = 'agd.modified';
        }

        if($orderbyOption === null){
            $orderbyOption = 'desc';
        }

        $this->_sql->setTable($this->_attributesGroupTable);
        $action = $this->_sql->select()
            ->join(
                array('agd' => $this->_attributeGroupDefinitionsTable),
                $this->_attributesGroupTable . '.attributeGroupId = agd.attributeGroupId'
            )
            ->join(
                array('am' => $this->_attributesMerchantsTable),
                $this->_attributesGroupTable . '.attributeGroupId = am.attributeGroupId',
                array('attributesCount' => new \Zend\Db\Sql\Expression('COUNT(attributeId)')),
                Select::JOIN_LEFT
            )
            ->where('agd.languageId = '. $this->_languages['default-language'])
            ->where($this->_attributesGroupTable . '.groupType = "Merchant"')
            ->group($this->_attributesGroupTable . '.attributeGroupId');

        // Search
        if ($search !== null) {
            $action->where
                ->nest
                    ->like('agd.name', '%' . $search . '%')
                    ->or
                    ->like('agd.publicName', '%' . $search . '%')
                ->unnest;
        }
        $action->order($orderBy ." " .  $orderbyOption);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        return $this->_processGroupAttributes($result, $search);
    }

    /**
     * @param $groups
     * @param int $parentKey
     * @return array|null
     */
    protected function _processGroupChildren($groups, $parentKey = 0)
    {
        $return = array();

        //Traverse the tree and search for direct children of the root
        foreach($groups as $attributeGroupId => $group) {

            //A direct child is found
            if($group['groupParentId'] == $parentKey) {

                //Remove item from tree (we don't need to traverse this again)
                $newArrayElement = $group;
                unset($groups[$attributeGroupId]);

                //Append the child into result array and parse its children
                $newArrayElement['children'] = $this->_processGroupChildren($groups, $newArrayElement['attributeGroupId']);
                $return[] = $newArrayElement;

            }
        }
        if (empty($return) && $parentKey == 0) {
            return $groups;
        }
        return empty($return) ? null : $return;
    }

    /**
     * @param $result
     * @param array $search
     * @return array|null
     */
    protected function _processGroupAttributes($result, $search = array())
    {
        $groups = array();
        foreach ($result as $group) {
            $groups[$group['attributeGroupId']] = $group;
            if (!empty($search)) {
                $groups[$group['attributeGroupId']]['children'] = null;
            }
        }

        if (!empty($groups) && empty($search)) {
            $finalArray = $this->_processGroupChildren($groups, 0);
        } else {

            $finalArray = $groups;
        }

        return $finalArray;
    }

    /**
     * @param $groupCode
     * @return bool|void
     */
    public function getMerchantAttributeGroupByGroupCode($groupCode)
    {
        $this->_sql->setTable($this->_attributesGroupTable);
        $action = $this->_sql->select()
            ->where('groupCode = "'.$groupCode.'"')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        if ($result->count() === 0) {
            return false;
        } else {
            return $result->next();
        }
    }

    /**
     * @param $groupCode
     * @return bool|void
     */
    public function getColourGroupByGroupCode($groupCode)
    {
        $this->_sql->setTable($this->_coloursGroupsTable);
        $action = $this->_sql->select()
            ->where('groupCode = "'.$groupCode.'"')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        if ($result->count() === 0) {
            return false;
        } else {
            return $result->next();
        }
    }

    /**
     * @param $groupId
     * @return bool
     */
    public function getColourGroupCode($groupId)
    {
        $this->_sql->setTable($this->_coloursGroupsTable);
        $action = $this->_sql->select()
            ->where('colourGroupId = "'.$groupId.'"')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        if ($result->count() === 0) {
            return false;
        } else {
            return $result->next()['groupCode'];
        }
    }

    /**
     * @param $groupId
     * @param $languageId
     * @return bool
     */
    public function getColourGroupName($groupId, $languageId = 1)
    {
        $this->_sql->setTable(array('CG' => $this->_coloursGroupsTable));
        $action = $this->_sql->select()
            ->join(
                array('CGD' => $this->_coloursGroupsDefinitionsTable),
                'CG.colourGroupId = CGD.colourGroupId',
                array(
                    'groupName' => 'name'
                )
            )
            ->where('CG.colourGroupId = "'.$groupId.'" AND CGD.languageId="'.$languageId.'"')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        if ($result->count() === 0) {
            return false;
        } else {
            return $result->next()['groupName'];
        }
    }

    /**
     * Get the group and with the group all of the attributes to that group
     * Function used to merchant groups ( colours, shoes sizes, clothes sizes, accessories sizes ) and marketing groups
     * Notes :
     * - Get the group details with group source table and parent source table.
     * - If the group does not have parent group id then
     * -- Get the attribute values for the marketing or core groups
     * -- If the group is colour get the values for the colours table
     * -- Else group is sizes and get from source table.
     * - If the group has parent group id then:
     * -- If the group is Merchant then :
     * --- Get the values of the attributes of that group from their respective table ( COLOURS, SHOES_Sizes, CLOTHES_Sizes, ACC_Sizes)
     * -- If the group is marketing or core then:
     * --- Get the values of the attributes of that group from the SourceTable in the database.
     * @param $id
     * @return array
     */
    public function getGroupById($id)
    {
        $this->_attributesGroupTable = 'ATTRIBUTES_Groups';
        $this->_sql->setTable($this->_attributesGroupTable);

        $group = array ();
        if ($id !== null) {
            $action = $this->_sql->select()
                ->join(array('agd' => $this->_attributeGroupDefinitionsTable),
                    $this->_attributesGroupTable . '.attributeGroupId = agd.attributeGroupId')
                ->join(
                    array('parent' => $this->_attributesGroupTable),
                    'parent.attributeGroupId = ' . $this->_attributesGroupTable . '.groupParentId',
                    array('parentSourceTable' => 'sourceTable'),
                    Select::JOIN_LEFT
                )
                ->where('agd.languageId = '. $this->_languages['default-language'])
                ->where('agd.attributeGroupId = '. $id)
                ->limit(1);

            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute()->next();

            $result['where'] = 'marketing';
            if ($result['groupParentId'] == 0) {
                $this->_sql->setTable($result['sourceTable']);
                if ($result['sourceTable'] == $this->_attributesTable) {
                    $select = $this->_sql->select()
                        ->join(
                            array('amd' => $this->_attributesDefinitionsTable),
                            'amd.attributeId = ' . $this->_attributesTable . '.attributeId',
                            array('*'),
                            Select::JOIN_LEFT
                        )
                    ->where('amd.languageId = '. $this->_languages['default-language'])
                    ->where('amd.status != "DELETED"')
                    ->where($result['sourceTable'].'.attributeGroupId = '. $result['attributeGroupId']);
                    $result['where'] = 'marketing';
                } else {
                    if ($result['sourceTable'] == $this->_coloursGroupsTable) {
                        $select = $this->_sql->select()
                            ->join(
                                array('cd' => $this->_coloursGroupsDefinitionsTable),
                                'cd.colourGroupId = COLOURS_Groups.colourGroupId',
                                array('*'),
                                Select::JOIN_LEFT
                            )
                            ->where('cd.languageId = '. $this->_languages['default-language']);
                        $result['where'] = 'colour';
                    }else {
                        $result['where'] = 'size';
                        $select = $this->_sql->select();
                    }
                }
                $statement = $this->_sql->prepareStatementForSqlObject($select);
                $attributes = $statement->execute();
                foreach ($attributes as $attr) {
                    $result['children'][] = $attr;
                }
                $group[] = $result;
            } else {
                if ($result['groupType'] == 'Merchant') {

                    $this->_sql->setTable($result['sourceTable']);
                    $select = $this->_sql->select()->where('attributeGroupId = '.$result['attributeGroupId']);
                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $attributes = $statement->execute();

                    $merchantAttributeIds = array();
                    if ($attributes->count() > 0) {
                        foreach ($attributes as $attr) {
                            $merchantAttributeIds[]= $attr['merchantAttributeId'];
                        }
                        $merchantAttributeIds = implode(",", $merchantAttributeIds);
                    }
                    $group = array();

                    if ($result['parentSourceTable'] !== null) {
                        $this->_sql->setTable($result['parentSourceTable']);
                        $childrenArray = array();
                        $select1 = false;
                        switch($result['parentSourceTable']){
                            case $this->_coloursGroupsTable:
                                if (!empty($merchantAttributeIds)) {
                                    $select1 = $this->_sql->select()
                                        ->columns(array(
                                            'colourGroupHex' => 'hex',
                                            'colourGroupCode' => 'groupCode',
                                            'colourGroupEnabled' => 'enabled',
                                            'hasImage',
                                            'imageId',
                                            'colourGroupId'
                                        ))
                                        ->join(
                                            array('cgd' => $this->_coloursGroupsDefinitionsTable),
                                            'cgd.colourGroupId = COLOURS_Groups.colourGroupId',
                                            array('colourGroupName' => 'name')
                                        )
                                        ->join(
                                            array('crg' => 'COLOURS_R_GROUPS'),
                                            'crg.colourGroupId = COLOURS_Groups.colourGroupId',
                                            array('colourId')
                                        )
                                        ->where('COLOURS_Groups.colourGroupId IN('.$merchantAttributeIds.')')
                                        ->where('cgd.languageId = '.$this->_languages['current-language']);
                                }
                                $result['where'] = 'colour';
                                break;
                            case 'SIZES_Clothes':
                                if (!empty($merchantAttributeIds)) {
                                    $select1 = $this->_sql->select()
                                        ->join(
                                            array('szc' => 'SIZES_Clothes_Info'),
                                            $result['parentSourceTable'] . '.uk = szc.size',
                                            array('*')
                                        )
                                        ->where('SIZES_Clothes.sizeId IN ('.$merchantAttributeIds.')');
                                }
                                $result['where'] = 'size-clothes';
                                break;
                            case 'SIZES_Jackets':
                                if (!empty($merchantAttributeIds)) {
                                    $select1 = $this->_sql->select()
                                        ->join(
                                            array('szj' => 'SIZES_Jackets_Info'),
                                            $result['parentSourceTable'] . '.uk = szj.size',
                                            array('*')
                                        )
                                        ->where('SIZES_Jackets.sizeId IN ('.$merchantAttributeIds.')');
                                }
                                $result['where'] = 'size-jackets';
                                break;
                            case 'SIZES_Trousers':
                                if (!empty($merchantAttributeIds)) {
                                    $select1 = $this->_sql->select()
                                        ->join(
                                            array('szj' => 'SIZES_Trousers_Info'),
                                            $result['parentSourceTable'] . '.sizeId = szj.sizeId',
                                            array('*')
                                        )
                                        ->where('SIZES_Trousers.sizeId IN ('.$merchantAttributeIds.')');
                                }
                                $result['where'] = 'size-trousers';
                                break;
                            case 'SIZES_SwimShorts':
                                if (!empty($merchantAttributeIds)) {
                                    $select1 = $this->_sql->select()
                                        ->join(
                                            array('sws' => 'SIZES_SwimShorts_Info'),
                                            $result['parentSourceTable'] . '.sizeId = sws.sizeId',
                                            array('*')
                                        )
                                        ->where('sws.sizeId IN ('.$merchantAttributeIds.')');
                                }
                                $result['where'] = 'size-swimshorts';
                                break;
                            case 'SIZES_Shirts':
                                if (!empty($merchantAttributeIds)) {
                                    $select1 = $this->_sql->select()
                                        ->join(
                                            array('ssi' => 'SIZES_Shirts_Info'),
                                            $result['parentSourceTable'] . '.uk = ssi.size',
                                            array('*')
                                        )
                                        ->where('SIZES_Shirts.sizeId IN ('.$merchantAttributeIds.')');
                                }
                                $result['where'] = 'size-shirts';
                                break;
                            case 'SIZES_Belts':
                                if (!empty($merchantAttributeIds)) {
                                    $select1 = $this->_sql->select()
                                        ->join(
                                            array('sbl' => 'SIZES_Belts_Info'),
                                            $result['parentSourceTable'] . '.uk = sbl.size',
                                            array('*')
                                        )
                                        ->where('SIZES_Belts.sizeId IN ('.$merchantAttributeIds.')');
                                }
                                $result['where'] = 'size-belts';
                                break;
                            case 'SIZES_Shoes':
                                if (!empty($merchantAttributeIds)) {
                                    $select1 = $this->_sql->select()
                                        ->where('SIZES_Shoes.sizeId IN ('.$merchantAttributeIds.')');
                                }
                                $result['where'] = 'size-shoes';
                                break;
                            case 'SIZES_Accessories':
                                if (!empty($merchantAttributeIds)) {
                                    $select1 = $this->_sql->select()
                                        ->where('SIZES_Accessories.sizeId IN ('.$merchantAttributeIds.')');
                                }
                                $result['where'] = 'size-accessories';
                                break;
                        }

                        if ($select1 !== false) {
                            $statement = $this->_sql->prepareStatementForSqlObject($select1);
                            $attributes = $statement->execute();
                        } else {
                            $attributes = array();
                        }

                        //childrenArray is used for the sizes management to know which
                        //size is added to group
                        if ($result['where'] == 'colour') {
                            $combinedColorsArray = array();
                            foreach ($attributes as $attr) {
                                if (isset($combinedColorsArray[$attr['colourGroupId']])) {
                                    $combinedColorsArray[$attr['colourGroupId']]['colours'][] = $attr['colourId'];
                                } else {
                                    $combinedColorsArray[$attr['colourGroupId']] = array(
                                        'colourGroupId' => $attr['colourGroupId'],
                                        'colourGroupCode' => $attr['colourGroupCode'],
                                        'colourGroupName' => $attr['colourGroupName'],
                                        'colourGroupHex' => $attr['colourGroupHex'],
                                        'colourGroupEnabled' => $attr['colourGroupEnabled'],
                                        'hasImage' => $attr['hasImage'],
                                        'imageId' => $attr['imageId'],
                                        'imageUrl' => ($attr['hasImage'] == 1 ? $this->rsMapper->getImageUrl($attr['imageId'], 'colour-image-list', true) : ''),
                                        'colours' => array(
                                            $attr['colourId']
                                        ),
                                    );
                                }
                            }
                            foreach ($combinedColorsArray as $combinedArray) {
                                $result['children'][] = $combinedArray;
                            }
                        } else {

                            foreach ($attributes as $attr) {

                                $result['children'][] = $attr;
                                $childrenArray = array_merge($childrenArray, array($attr['sizeId']));
                            }
                        }

                        $group[] = $result;
                        $group[count($group)-1]['childrenArray'] = $childrenArray;
                        $group[count($group)-1]['usedChildrenArray'] = array();

                    }
                } else {

                    $this->_sql->setTable($result['sourceTable']);
                    $select = $this->_sql->select()
                        ->join(
                            array('amd' => $this->_attributesDefinitionsTable),
                            $result['sourceTable'] . '.attributeId = amd.attributeId',
                            array('*'),
                            Select::JOIN_LEFT
                        )
                        ->where('amd.languageId = '. $this->_languages['default-language'])
                        ->where('amd.status != "DELETED"')
                        ->where($result['sourceTable'].'.attributeGroupId = '. $result['attributeGroupId']);

                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $attributes = $statement->execute();
                    foreach ($attributes as $attr) {
                        $result['children'][] = $attr;
                    }
                    $group[] = $result;
                }
            }
        }

        return isset($group[0]) ? $group[0] : $group;
    }
    /**
     * Getting the form request from the bulk upload
     * and separate them based on the request type ( download template / upload template )
     *
     * @param $data
     * @returns mixed
     * @throws \Exception
     */
    public function bulk($data)
    {
        $this->_submittedData = $data;
        switch ($this->_submittedData['submit']) {
            case 'Download Template':
                $this->_bulkDownloadTemplate();
                return true;
                break;
            case 'Upload Template':
                return $this->_bulkUploadTemplate();
                break;
            default:
                throw new \Exception('Invalid case: '.$this->_submittedData['submit']);
        }
    }
    /**
     * Based on the data submitted in the form it downloads
     * in xls format the template needed for the later upload
     */
    protected function _bulkDownloadTemplate()
    {
        switch($this->_submittedData['downloadType']) {
            case $this->_coloursGroupsTable:
                $coloursColumns = array(
                    'AttributesGroups' => array(
                        'Record no',
                        'Merchant Attribute Group Name',
                        'Merchant Attribute Group Code',
                        'Language',
                        'Action Type (NEW/UPDATE/DELETE or empty)'
                    ),
                    'ColourGroups' => array(
                        'Record no',
                        'Attributes Group',
                        'Colour Group Name',
                        'Colour Group Code',
                        'Colour Group Hex',
                        'Colour Group Has Image',
                        'Language',
                        'Enable',
                        'Action Type (NEW/UPDATE/DELETE or empty)'
                    ),
                    'Colours' => array(
                        'Record no',
                        'Colour Group',
                        'Colour Name',
                        'Colour Code',
                        'Colour Hex',
                        'Language',
                        'Action Type (NEW/UPDATE/DELETE or empty)'
                    )
                );
                $this->_getBulkColoursTemplate($coloursColumns);
                break;
            case 'SHOE':
                break;
            case 'CLOTHES':
                break;
            case 'ACCESORIES':
                break;
        }
    }
    /**
     * Get The Bulk template for the columns passed in the function
     */
    protected function _getBulkColoursTemplate($coloursColumns)
    {
        $this->_excelReader->getProperties()->setCreator('Prism DM');
        $this->_excelReader->getProperties()->setTitle('Bulk Upload Colour Template');
        $this->_excelReader->getProperties()->setSubject('Bulk Upload Colour Template');
        $this->_excelReader->getProperties()->setDescription('Template document for colour attributes');

        $i = 0;
        foreach ($coloursColumns as $sheetName => $columns) {
            if ($i > 0) {
                $this->_excelReader->createSheet($i);
            }
            $this->_excelReader->setActiveSheetIndex($i);
            $this->_excelReader->getActiveSheetIndex($i);

            $current_character = 'A';
            foreach ($columns as $columnName) {
                $this->_excelReader->getActiveSheet()->setCellValue($current_character.'1', $columnName);
                $current_character = chr(ord($current_character) + 1);
            }
            $this->_excelReader->getActiveSheet()->setTitle($sheetName);
            $i++;
        }
        $this->_excelReader->setActiveSheetIndex(0);

        //Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="template.xls"');
        header('Cache-Control: max-age=0');

        // Add File download cookie to trigger loader stop event
        setcookie('fileDownload', '1', time() + 60, '/');

        //If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        //If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($this->_excelReader, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    /**
     * @throws \Exception
     */
    protected function _bulkUploadTemplate()
    {
        //Check the file and Save it physical to the server
        $httpAdapter = new \Zend\File\Transfer\Adapter\Http();
        $fileSize  = new \Zend\Validator\File\Size(array('max' => 1000000 )); //1MB
        $extension = new \Zend\Validator\File\Extension(array('extension' => array('xls','xlsx')));
        $httpAdapter->setValidators(array($fileSize, $extension), $this->_submittedData['feedAttachment']['name']);

        if($httpAdapter->isValid()) {
            $uploadPath = ROOT_PATH.'/public/uploads/';
            $httpAdapter->setDestination($uploadPath);
            if($httpAdapter->receive($this->_submittedData['feedAttachment']['name'])) {
                $newFile = $httpAdapter->getFileName();
            }
        }
        if (empty($newFile)) {
            return array();
        }
        //Reads the xls file and converts content into array
        $excelReader = PHPExcel_IOFactory::createReaderForFile($newFile);
        $excelReader->setReadDataOnly();

        //The default behavior is to load all sheets
        $excelReader->setLoadAllSheets();
        /** @var PHPExcel $excelObj */
        $excelObj = $excelReader->load($newFile);
        $excelObj->getActiveSheet()->toArray(null, true,true,true);
        $worksheetNames = $excelObj->getSheetNames($newFile);
        $rows = array();
        foreach($worksheetNames as $sheetName){
            //set the current active worksheet by name
            $excelObj->setActiveSheetIndexByName($sheetName);
            //create an assoc array with the sheet name as key and the sheet contents array as value
            $rows[$sheetName] = $excelObj->getActiveSheet()->toArray(null, true,true,true);
        }
        /** begin mysql transaction if something goes wrong we will need to rollback all the changes so we don't mess data up */
        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
        try {
            $processedArray = array();
            switch($this->_submittedData['downloadType']) {
                case $this->_coloursGroupsTable:
                    $processedArray = $this->_bulkUploadColorsTemplate($rows);
                    break;
                case 'SHOE':
                    break;
                case 'CLOTHES':
                    break;
                case 'ACCESORIES':
                    break;
            }

            //If Error rows are more than 5 an excel with errors will download
            //if error rows are less then 5 they will be shown in an modal
            //Remove temporary uploaded file
            @unlink($newFile);
            $showErrors = false;
            foreach ($worksheetNames as $sheetName) {
                if (count($processedArray[$sheetName]) > 1) {
                    // we have errors
                    $showErrors = true;
                }
            }
            if ($showErrors) {
                // revert the changes done to the database and download excel spreadsheet with errors
                $this->_dbAdapter->getDriver()->getConnection()->rollback();
                $this->_bulkDownloadResult($processedArray);
            }

            // we are error free so we can commit the changes to the database and show success message
            $this->_dbAdapter->getDriver()->getConnection()->commit();
            return array();
        } catch(\Exception $e) {
            // revert the changes done to the database and throw exception
            $this->_dbAdapter->getDriver()->getConnection()->rollback();
            throw new \Exception($e);
        }
    }

    /**
     * @param $rows
     * @return array
     * @throws \Exception
     */
    protected function _bulkUploadColorsTemplate($rows)
    {
        //Iterate all rows and return error on rows that have problems
        //Rows that are ok will be processed
        $processedArray = array();
        foreach ($rows as $styleSheetName => $styleSheetArray) {
            $processedArray[$styleSheetName] = array();
            $this->_processingTimeIds[$styleSheetName] = array();
            $groupCodeLetter = false;
            switch ($styleSheetName) {
                case 'AttributesGroups':
                    $verificationMethod = '_checkBulkEntryForAttributeGroup';
                    $groupCodeLetter = 'C';
                    break;
                case 'ColourGroups':
                    $verificationMethod = '_checkBulkEntryForColourGroup';
                    $groupCodeLetter = 'D';
                    break;
                case 'Colours':
                    $verificationMethod = '_checkBulkEntryForColour';
                    break;
                default:
                    throw new \Exception('Unrecognized stylesheet!');
            }

            foreach ($styleSheetArray as $key => $row) {
                if ($key != 1) {
                    //Case I for the insert new
                    $row = $this->{$verificationMethod}($row);
                    // setup an array with the codes of the groups from the previous spreadsheet so we can relate the records
                    if ($groupCodeLetter !== false) {
                        $this->_processingTimeIds[$styleSheetName][$row['A']] = $row[$groupCodeLetter];
                    }
                } else {
                    // header row -- do nothing
                }
                if (!empty($row['error'])) {
                    $processedArray[$styleSheetName][] = $row;
                }
            }
        }
        return $processedArray;
    }

    /**
     *  Verify every record of the AttributesGroups spreadsheet and saves array with status
     *      and do the correct opperations
     *  Excel mapper (
     *      A = Record no,
     *      B = Merchant Attribute Group Name,
     *      C = Merchant Attribute Group Code,
     *      D = Language,
     *      E = Action Type
     *  )
     */
    protected function _checkBulkEntryForAttributeGroup($row)
    {
        $row['error'] = array();

        // Initial validation of the excel columns
        $tmp = trim($row['A']);
        if (empty($tmp) || !is_numeric($row['A'])) {
            $row['error'][] = 'The current number of the entry is not correct';
        }

        $tmp = trim($row['B']);
        if (empty($tmp)) {
            $row['error'][] = 'The merchant attribute group name cannot be empty';
        }

        $tmp = trim($row['C']);
        if (empty($tmp)) {
            $row['error'][] = 'The merchant attribute group code cannot be empty';
        }
        if (strlen($tmp) > 6) {
            $row['error'][] = 'The merchant attribute group code can be maximum 6 character';
        }

        if (!in_array((string)$row['D'], $this->_languages['site-languages'])) {
            $row['error'][] = 'The language must be an existing language';
        }

        if (!empty($row['E']) && !in_array($row['E'], array('','NEW','UPDATE','DELETE'))) {
            $row['error'][] = 'The action must be NEW,UPDATE,DELETE or empty';
        }

        if (!empty($row['error'])) {
            return $row;
        }

        // Database validation
        switch ($row['E']) {
            case 'NEW':
                // try to create a new group
                $row['error'] = $this->_checkIfAttributeGroupExistAndInsertNew($row['B'], $row['C'], $row['D']);
                break;
            case 'UPDATE':
                // try to update existing group
                $row['error'] = $this->_checkIfAttributeGroupExistAndUpdateIt($row['B'], $row['C'], $row['D']);
                break;
            case 'DELETE':
            default:
                // do nothing the group is just informative for the other spreadsheets
                break;
        }

        return $row;
    }

    /**
     * @param $attributeGroupName
     * @param $attributeGroupCode
     * @param $languageId
     * @return array
     */
    protected function _checkIfAttributeGroupExistAndInsertNew($attributeGroupName, $attributeGroupCode, $languageId)
    {
        $errors = array();

        $this->_sql->setTable(array('AG' => $this->_attributesGroupTable));
        $action = $this->_sql->select()
            ->columns(array('groupCode'))
            ->join(
                array('AGD' => $this->_attributeGroupDefinitionsTable),
                'AG.attributeGroupId = AGD.attributeGroupId'
            )
            ->where('
            (AGD.publicName = "'.$attributeGroupName.'" OR AGD.name = "'.$attributeGroupName.'")
            AND AGD.languageId = "'.$languageId.'"
            ')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        if ($result->count() == 1) {
            if ($attributeGroupCode == $result->next()['groupCode']) {
                $errors[] = 'The attribute group already exists';
            } else {
                $errors[] = 'The attribute group name is already used';
            }
        } else {
            $this->_sql->setTable($this->_attributesGroupTable);
            $action = $this->_sql->select()
                ->where('groupCode = "'.$attributeGroupCode.'"')
                ->limit(1);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();

            if ($result->count() === 0) {
                // we can insert the record
                $this->_sql->setTable($this->_attributesGroupTable);
                $data = array(
                    'groupParentId' => 1,
                    'groupCode' => $attributeGroupCode,
                    'sourceTable' => $this->_attributesMerchantsTable,
                    'groupType' => 'Merchant',
                    'viewAs' => 'SELECT',
                    'usedForFiltering' => '0',
                    'status' => 'ACTIVE',
                    'created' => date('Y-m-d H:i:s')
                );
                $action = $this->_sql->insert();
                $action->values($data);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
                $merchantAttributeGroupId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
            } else {
                $merchantAttributeGroupId = $result->next()['attributeGroupId'];
            }

            // add the definition
            $this->_sql->setTable($this->_attributeGroupDefinitionsTable);
            $data = array(
                'attributeGroupId' => $merchantAttributeGroupId,
                'languageId' => $languageId,
                'name' => $attributeGroupName,
                'publicName' => $attributeGroupName,
                'status' => 'ACTIVE',
                'created' => date('Y-m-d H:i:s')
            );
            $action = $this->_sql->insert();
            $action->values($data);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        }

        return $errors;
    }

    /**
     * @param $attributeGroupName
     * @param $attributeGroupCode
     * @param $languageId
     * @return array
     */
    protected function _checkIfAttributeGroupExistAndUpdateIt($attributeGroupName, $attributeGroupCode, $languageId)
    {
        $errors = array();

        $this->_sql->setTable(array('AG' => $this->_attributesGroupTable));
        $action = $this->_sql->select()
            ->columns(array('groupCode'))
            ->join(
                array('AGD' => $this->_attributeGroupDefinitionsTable),
                'AG.attributeGroupId = AGD.attributeGroupId'
            )
            ->where('
            (AGD.publicName = "'.$attributeGroupName.'" OR AGD.name = "'.$attributeGroupName.'")
            AND AGD.languageId = "'.$languageId.'"
            ')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        if ($result->count() == 1) {
            $attributeGroupId = $result->next()['attributeGroupId'];
            // we can use the name-language to update the document
            // we also need to check if the group code is setup also
            $this->_sql->setTable($this->_attributesGroupTable);
            $action = $this->_sql->select()
                ->where('groupCode = "'.$attributeGroupCode.'"')
                ->limit(1);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();

            if ($result->count() == 1) {
                // nothing to update - throw error
                $errors[] = 'Both specified attribute group name and code are in use';
            } else {
                // update group code
                $this->_updateGroup(array(
                    'groupCode' => $attributeGroupCode
                ), $attributeGroupId);
            }
        } else {
            // we need to check if we can use the group-code
            $this->_sql->setTable($this->_attributesGroupTable);
            $action = $this->_sql->select()
                ->where('groupCode = "'.$attributeGroupCode.'"')
                ->limit(1);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();

            if ($result->count() == 1) {
                // update the group name for that language
                $attributeGroupId = $result->next()['attributeGroupId'];
                $this->_updateGroupDefinition(array(
                    'name' => $attributeGroupName,
                    'publicName' => $attributeGroupName
                ), $attributeGroupId, $languageId);
            } else {
                // nothing to update by. this should be a new entry
                $errors[] = 'Could not use UPDATE. This looks like a new entry.';
            }
        }

        return $errors;
    }

    /**
     *  Verify every record of the ColoursGroups spreadsheet and saves array with status
     *  Excel mapper (
     *      A = Record no,
     *      B = Attributes Group,
     *      C = Colour Group Name,
     *      D = Colour Group Code,
     *      E = Colour Group Hex,
     *      F = Colour Group Has Image,
     *      G = Language,
     *      H = Enable,
     *      I = Action Type
     *  )
     */
    protected function _checkBulkEntryForColourGroup($row)
    {
        $row['error'] = array();

        // Initial validation of the excel columns
        $tmp = trim($row['A']);
        if (empty($tmp) || !is_numeric($row['A'])) {
            $row['error'][] = 'The current number of the entry is not correct';
        }

        $tmp = trim($row['B']);
        $arrayKeys = array_keys($this->_processingTimeIds['AttributesGroups']);
        if (!in_array($tmp, $arrayKeys)) {
            $row['error'][] = 'The Attributes Group is not declared in the first spreadsheet';
        }

        $tmp = trim($row['C']);
        if (empty($tmp)) {
            $row['error'][] = 'The colour group name cannot be empty';
        }

        $tmp = trim($row['D']);
        if (empty($tmp)) {
            $row['error'][] = 'The colour group code cannot be empty';
        }
        if (strlen($tmp) > 6) {
            $row['error'][] = 'The colour group code can be maximum 6 character';
        }

        $tmp = trim($row['E']);
        $tmp1 = trim($row['F']);
        if (empty($tmp) && empty($tmp1)) {
            $row['error'][] = 'The colour group hex and has image flag cannot both be empty';
        }
        if (!in_array($tmp1, array('0','1'))) {
            $row['error'][] = 'The colour group has image flag must be 0 or 1';
        }
        if (strlen($tmp) > 7) {
            $row['error'][] = 'The colour group hex can be maximum 7 character';
        }

        $tmp = trim($row['G']);
        if ( ($tmp1 == '1') && ( empty($tmp) || !is_numeric($tmp)) ) {
            $row['error'][] = 'The has image flag is set and the imageId is either empty or not a valid number';
        }

        if (!in_array((string)$row['H'], $this->_languages['site-languages'])) {
            $row['error'][] = 'The language must be an existing language';
        }

        $tmp = trim($row['I']);
        if (!in_array($tmp, array('0','1'))) {
            $row['error'][] = 'The colour group enabled flag must be 0 or 1';
        }

        if (!empty($row['J']) && !in_array($row['I'], array('','NEW','UPDATE','DELETE'))) {
            $row['error'][] = 'The action must be NEW,UPDATE,DELETE or empty';
        }

        if (!empty($row['error'])) {
            return $row;
        }

        $attributeGroupCode = $this->_processingTimeIds['AttributesGroups'][$row['B']];
        // Database validation
        switch ($row['I']) {
            case 'NEW':
                // try to create a new group
                // get attribute group reference first
                $row['error'] = $this->_checkIfColourGroupExistAndInsertNew($attributeGroupCode, $row['C'], $row['D'], $row['E'], $row['F'], $row['G'], $row['H'], $row['I']);
                break;
            case 'UPDATE':
                // try to update existing group
                $row['error'] = $this->_checkIfColourGroupExistAndUpdateIt($attributeGroupCode, $row['C'], $row['D'], $row['E'], $row['F'], $row['G'], $row['H'], $row['I']);
                break;
            case 'DELETE':
            default:
                // do nothing the group is just informative for the other spreadsheets
                break;

            //TODO: we need to add another action for colour groups so we can add existing ones to the merchant attributes groups
        }

        return $row;
    }

    /**
     * @param $attributeGroupCode
     * @param $colourGroupName
     * @param $colourGroupCode
     * @param $colourGroupHex
     * @param $colourGroupHasImage
     * @param $colourGroupImageId
     * @param $languageId
     * @param $enabled
     * @return array
     */
    protected function _checkIfColourGroupExistAndInsertNew($attributeGroupCode, $colourGroupName, $colourGroupCode, $colourGroupHex, $colourGroupHasImage, $colourGroupImageId, $languageId, $enabled)
    {
        $errors = array();

        // get merchant attribute group id
        $attributeGroupArray = $this->getMerchantAttributeGroupByGroupCode($attributeGroupCode);
        $attributeGroupId = $attributeGroupArray['attributeGroupId'];
        if (empty($attributeGroupId)) {
            $errors[] = 'The parent attribute group for this colour group was not inserted';
            return $errors;
        }
        // check if the name is unique for the language
        $this->_sql->setTable(array('CG' => $this->_coloursGroupsTable));
        $action = $this->_sql->select()
            ->columns(array('groupCode'))
            ->join(
                array('CGD' => $this->_coloursGroupsDefinitionsTable),
                'CG.colourGroupId = CGD.colourGroupId'
            )
            ->where('CGD.name = "'.$colourGroupName.'" AND CGD.languageId = "'.$languageId.'"')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        if ($result->count() == 1) {
            // name is already used
            if ($colourGroupCode == $result->next()['groupCode']) {
                $errors[] = 'The colour group already exists';
            } else {
                $errors[] = 'The colour group name is already used';
            }
        } else {
            // name is not used check if the group code is used
            $this->_sql->setTable($this->_coloursGroupsTable);
            $action = $this->_sql->select()
                ->where('groupCode = "'.$colourGroupCode.'"')
                ->limit(1);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();

            if ($result->count() === 0) {
                // we can insert the record
                $this->_sql->setTable($this->_coloursGroupsTable);
                $data = array(
                    'groupCode' => $colourGroupCode,
                    'hasImage' => $colourGroupHasImage,
                    'imageId' => $colourGroupImageId,
                    'hex' => $colourGroupHex,
                    'enabled' => $enabled
                );
                $action = $this->_sql->insert();
                $action->values($data);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
                $colourGroupId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

                // insert the record into the merchant attributes table
                $this->_sql->setTable($this->_attributesMerchantsTable);
                $data = array(
                    'attributeGroupId' => $attributeGroupId,
                    'merchantAttributeId' => $colourGroupId,
                );
                $action = $this->_sql->insert();
                $action->values($data);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
            } else {
                $colourGroupId = $result->next()['colourGroupId'];
            }

            // add the definition
            $this->_sql->setTable($this->_coloursGroupsDefinitionsTable);
            $data = array(
                'colourGroupId' => $colourGroupId,
                'languageId' => $languageId,
                'name' => $colourGroupName
            );
            $action = $this->_sql->insert();
            $action->values($data);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        }

        return $errors;
    }

    /**
     * @param $attributeGroupCode
     * @param $colourGroupName
     * @param $colourGroupCode
     * @param $colourGroupHex
     * @param $colourGroupHasImage
     * @param $languageId
     * @param $enabled
     * @return array
     */
    protected function _checkIfColourGroupExistAndUpdateIt($attributeGroupCode, $colourGroupName, $colourGroupCode, $colourGroupHex, $colourGroupHasImage, $languageId, $enabled)
    {
        $errors = array();

        // check if the name is unique for the language
        $this->_sql->setTable(array('CG' => $this->_coloursGroupsTable));
        $action = $this->_sql->select()
            ->columns(array('groupCode'))
            ->join(
                array('CGD' => $this->_coloursGroupsDefinitionsTable),
                'CG.colourGroupId = CGD.colourGroupId'
            )
            ->where('CGD.name = "'.$colourGroupName.'" AND CGD.languageId = "'.$languageId.'"')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        if ($result->count() == 1) {
            $colourGroupId = $result->next()['colourGroupId'];
            // we can use the name-language to update the document
            // we also need to check if the group code is setup also
            $this->_sql->setTable($this->_coloursGroupsTable);
            $action = $this->_sql->select()
                ->where('groupCode = "'.$colourGroupCode.'"')
                ->where('hex = "'.$colourGroupHex.'"')
                ->where('hasImage = "'.$colourGroupHasImage.'"')
                ->where('enabled = "'.$enabled.'"')
                ->limit(1);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();

            if ($result->count() == 1) {
                // nothing to update - throw error
                $errors[] = 'All the attributes for this group are up to date';
            } else {
                // update group code
                $update = $this->_sql->update();
                $update->table($this->_coloursGroupsTable);
                $update->set(array(
                    'groupCode' => $colourGroupCode,
                    'hex' => $colourGroupHex,
                    'hasImage' => $colourGroupHasImage,
                    'enabled' => $enabled
                ));
                $update->where(array('colourGroupId' => $colourGroupId));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();
            }
        } else {
            // we need to check if we can use the group-code
            $this->_sql->setTable($this->_coloursGroupsTable);
            $action = $this->_sql->select()
                ->where('groupCode = "'.$colourGroupCode.'"')
                ->limit(1);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();

            if ($result->count() == 1) {
                $colourGroupId = $result->next()['colourGroupId'];

                // update the group name for that language
                $update = $this->_sql->update();
                $update->table($this->_coloursGroupsDefinitionsTable);
                $update->set(array(
                    'name' => $colourGroupName
                ));
                $update->where(array('colourGroupId' => $colourGroupId));
                $update->where(array('languageId' => $languageId));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();

                // update the group table
                $update = $this->_sql->update();
                $update->table($this->_coloursGroupsTable);
                $update->set(array(
                    'groupCode' => $colourGroupCode,
                    'hex' => $colourGroupHex,
                    'hasImage' => $colourGroupHasImage,
                    'enabled' => $enabled
                ));
                $update->where(array('colourGroupId' => $colourGroupId));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();
            } else {
                // nothing to update by. this should be a new entry
                $errors[] = 'Could not use UPDATE. This looks like a new entry.';
            }
        }

        return $errors;
    }

    /**
     *  Verify every record of the ColoursGroups spreadsheet and saves array with status
     *  Excel mapper (
     *      A = Record no,
     *      B = Colour Group,
     *      C = Colour Name,
     *      D = Colour Code,
     *      E = Colour Hex,
     *      F = Language,
     *      G = Action Type
     *  )
     */
    protected function _checkBulkEntryForColour($row)
    {
        $row['error'] = array();

        // Initial validation of the excel columns
        $tmp = trim($row['A']);
        if (empty($tmp) || !is_numeric($row['A'])) {
            $row['error'][] = 'The current number of the entry is not correct';
        }

        $tmp = trim($row['B']);
        $arrayKeys = array_keys($this->_processingTimeIds['ColourGroups']);
        if (!in_array($tmp, $arrayKeys)) {
            $row['error'][] = 'The Colour Group is not declared in the first spreadsheet';
        }

        $tmp = trim($row['C']);
        if (empty($tmp)) {
            $row['error'][] = 'The colour name cannot be empty';
        }

        $tmp = trim($row['D']);
        if (empty($tmp)) {
            $row['error'][] = 'The colour code cannot be empty';
        }
        if (strlen($tmp) > 6) {
            $row['error'][] = 'The colour code can be maximum 6 character';
        }

        $tmp = trim($row['E']);
        if (empty($tmp)) {
            $row['error'][] = 'The colour hex cannot be empty';
        }
        if (strlen($tmp) > 7) {
            $row['error'][] = 'The colour hex can be maximum 7 character';
        }

        if (!in_array((string)$row['F'], $this->_languages['site-languages'])) {
            $row['error'][] = 'The language must be an existing language';
        }

        if (!empty($row['G']) && !in_array($row['G'], array('','NEW','UPDATE','DELETE'))) {
            $row['error'][] = 'The action must be NEW,UPDATE,DELETE or empty';
        }

        if (!empty($row['error'])) {
            return $row;
        }

        $colourGroupCode = $this->_processingTimeIds['ColourGroups'][$row['B']];
        // Database validation
        switch ($row['G']) {
            case 'NEW':
                // try to create a new group
                // get attribute group reference first
                $row['error'] = $this->_checkIfColourExistAndInsertNew($colourGroupCode, $row['C'], $row['D'], $row['E'], $row['F']);
                break;
            case 'UPDATE':
                // try to update existing group
                $row['error'] = $this->_checkIfColourExistAndUpdateIt($colourGroupCode, $row['C'], $row['D'], $row['E'], $row['F']);
                break;
            case 'DELETE':
            default:
                // do nothing the group is just informative for the other spreadsheets
                break;
        }

        return $row;
    }

    /**
     * @param $colourGroupCode
     * @param $colourName
     * @param $colourCode
     * @param $colourHex
     * @param $languageId
     * @return array
     */
    protected function _checkIfColourExistAndInsertNew($colourGroupCode, $colourName, $colourCode, $colourHex, $languageId)
    {
        $errors = array();

        // get merchant attribute group id
        $colourGroupArray = $this->getColourGroupByGroupCode($colourGroupCode);
        $colourGroupId = $colourGroupArray['colourGroupId'];

        if (empty($colourGroupId)) {
            $errors[] = 'The parent colour group for this colour was not inserted';
            return $errors;
        }
        // check if the name is unique for the language
        $this->_sql->setTable(array('C' => $this->_coloursTable));
        $action = $this->_sql->select()
            ->columns(array('colourCode'))
            ->join(
                array('CD' => $this->_coloursDefinitionsTable),
                'C.colourId = CD.colourId'
            )
            ->where('CD.colourName = "'.$colourName.'" AND CD.languageId = "'.$languageId.'"')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        if ($result->count() == 1) {
            // name is already used
            if ($colourCode == $result->next()['colourCode']) {
                $errors[] = 'The colour already exists';
            } else {
                $errors[] = 'The colour is already used';
            }
        } else {
            // name is not used check if the code is used
            $this->_sql->setTable($this->_coloursTable);
            $action = $this->_sql->select()
                ->where('colourCode = "'.$colourCode.'"')
                ->limit(1);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();

            if ($result->count() === 0) {
                // we can insert the record
                $this->_sql->setTable($this->_coloursTable);
                $data = array(
                    'colourCode' => $colourCode,
                    'hex' => $colourHex
                );
                $action = $this->_sql->insert();
                $action->values($data);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
                $colourId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

                // insert the record into the colours_r_groups table
                $this->_sql->setTable($this->_coloursRGroupsTable);
                $data = array(
                    'colourGroupId' => $colourGroupId,
                    'colourId' => $colourId,
                );
                $action = $this->_sql->insert();
                $action->values($data);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
            } else {
                $colourId = $result->next()['colourId'];
            }

            // add the definition
            $this->_sql->setTable($this->_coloursDefinitionsTable);
            $data = array(
                'colourId' => $colourId,
                'languageId' => $languageId,
                'colourName' => $colourName
            );
            $action = $this->_sql->insert();
            $action->values($data);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        }

        return $errors;
    }

    /**
     * @param $colourGroupCode
     * @param $colourName
     * @param $colourCode
     * @param $colourHex
     * @param $languageId
     * @return array
     */
    protected function _checkIfColourExistAndUpdateIt($colourGroupCode, $colourName, $colourCode, $colourHex, $languageId)
    {
        $errors = array();

        // check if the name is unique for the language
        $this->_sql->setTable(array('C' => $this->_coloursTable));
        $action = $this->_sql->select()
            ->columns(array('colourCode'))
            ->join(
                array('CD' => $this->_coloursDefinitionsTable),
                'C.colourId = CD.colourId'
            )
            ->where('CD.colourName = "'.$colourName.'" AND CD.languageId = "'.$languageId.'"')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        if ($result->count() == 1) {
            $colourId = $result->next()['colourId'];
            // we can use the name-language to update the document
            // we also need to check if the group code is setup also
            $this->_sql->setTable($this->_coloursTable);
            $action = $this->_sql->select()
                ->where('colourCode = "'.$colourCode.'"')
                ->where('hex = "'.$colourHex.'"')
                ->limit(1);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();

            if ($result->count() == 1) {
                // nothing to update - throw error
                $errors[] = 'All the attributes for this colour are up to date';
            } else {
                // update group code
                $update = $this->_sql->update();
                $update->table($this->_coloursTable);
                $update->set(array(
                    'colourCode' => $colourCode,
                    'hex' => $colourHex
                ));
                $update->where(array('colourId' => $colourId));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();
            }
        } else {
            // we need to check if we can use the colour-code
            $this->_sql->setTable($this->_coloursTable);
            $action = $this->_sql->select()
                ->where('colourCode = "'.$colourCode.'"')
                ->limit(1);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();

            if ($result->count() == 1) {
                $colourId = $result->next()['colourId'];

                // update the group name for that language
                $update = $this->_sql->update();
                $update->table($this->_coloursDefinitionsTable);
                $update->set(array(
                    'colourName' => $colourName
                ));
                $update->where(array('colourId' => $colourId));
                $update->where(array('languageId' => $languageId));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();

                // update the group table
                $update = $this->_sql->update();
                $update->table($this->_coloursTable);
                $update->set(array(
                    'colourCode' => $colourCode,
                    'hex' => $colourHex
                ));
                $update->where(array('colourId' => $colourId));
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();
            } else {
                // nothing to update by. this should be a new entry
                $errors[] = 'Could not use UPDATE. This looks like a new entry.';
            }
        }

        return $errors;
    }

    /**
     *  Process array after submit and download excel file
     *  that contains the results of the process
     */
    protected function _bulkDownloadResult($processedArray)
    {
        $this->_excelReader->getProperties()->setCreator('Prism DM');
        $this->_excelReader->getProperties()->setTitle('Bulk Upload Errors');
        $this->_excelReader->getProperties()->setSubject('Bulk Upload Errors');
        $this->_excelReader->getProperties()->setDescription('Document with colour attributes bulk errors');

        $coloursColumns = array(
            'AttributesGroups' => array(
                'Record no',
                'Merchant Attribute Group Name',
                'Merchant Attribute Group Code',
                'Language',
                'Action Type (NEW/UPDATE/DELETE or empty)'
            ),
            'ColourGroups' => array(
                'Record no',
                'Attributes Group',
                'Colour Group Name',
                'Colour Group Code',
                'Colour Group Hex',
                'Colour Group Has Image',
                'Language',
                'Enable',
                'Action Type (NEW/UPDATE/DELETE or empty)'
            ),
            'Colours' => array(
                'Record no',
                'Colour Group',
                'Colour Name',
                'Colour Code',
                'Colour Hex',
                'Language',
                'Action Type (NEW/UPDATE/DELETE or empty)'
            )
        );

        $i = 0;
        foreach ($coloursColumns as $sheetName => $columns) {
            if ($i > 0) {
                $this->_excelReader->createSheet($i);
            }
            $this->_excelReader->setActiveSheetIndex($i);
            $this->_excelReader->getActiveSheetIndex($i);

            $current_character = 'A';
            foreach ($columns as $columnName) {
                $this->_excelReader->getActiveSheet()->setCellValue($current_character.'1', $columnName);
                $current_character = chr(ord($current_character) + 1);
            }
            $nr = 2;
            foreach ($processedArray[$sheetName] as $row) {
                $lastRowNumber = '';
                foreach ($row as $rowNumber => $rowValue) {
                    if($rowNumber != 'error') {
                        $lastRowNumber = $rowNumber;
                        $this->_excelReader->getActiveSheet()->setCellValue($rowNumber . $nr, $row[$rowNumber]);
                    } else {
                        $errorString = '';
                        foreach ($row['error'] as $error) {
                            $errorString .= $error . '; ';
                        }
                        $current_character = chr(ord($lastRowNumber) + 1);
                        $this->_excelReader->getActiveSheet()->setCellValue($current_character . $nr, $errorString);
                        $this->_excelReader->getActiveSheet()->getStyle($current_character . $nr)->getFill()
                            ->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array('rgb' => 'F28A8C')
                            ));
                    }
                }
            }
            $this->_excelReader->getActiveSheet()->setTitle($sheetName);
            $i++;
        }
        $this->_excelReader->setActiveSheetIndex(0);

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="bulkUploadErrors.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($this->_excelReader, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    /**
     * @param $submittedData
     * @throws \Exception
     * @returns int
     */
    public function saveMerchantGroup($submittedData)
    {
        $this->_submittedData = $submittedData;
        $this->_attributesGroupTable = 'ATTRIBUTES_Groups';
        $this->_sql->setTable($this->_attributesGroupTable);

        $action = $this->_sql->select()
            ->where($this->_attributesGroupTable . '.sourceTable = "'. $this->_submittedData['attributeMerchantType'] . '"' );
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute()->next();

        $data = array(
            'groupParentId' => $result['attributeGroupId'],
            'groupCode' => $submittedData['attributeMerchantGroupCode'],
            'sourceTable' => $this->_attributesMerchantsTable,
            'groupType' => 'Merchant',
            'viewAs' => 'SELECT'
        );
        $action = $this->_sql->insert();
        $action->values($data);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        try {
            $statement->execute();
        } catch (\Exception $e) {
            throw new \Exception($e);
            // log any error
        }
        $id = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
        $this->_sql->setTable($this->_attributeGroupDefinitionsTable);
        $data = array(
            'attributeGroupId' => $id,
            'name' => $submittedData['attributeMerchantGroupName'],
            'publicName' => $submittedData['attributeMerchantGroupPublicName'],
        );
        foreach ($this->_languages['site-languages'] as $language) {
            $data['languageId'] = $language;
            $action = $this->_sql->insert();
            $action->values($data);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            try {
                $statement->execute();
            } catch (\Exception $e) {
                throw new \Exception($e);
                // log any error
            }
        }
        return $id;
    }

    /**
     * Edit merchant group
     */
    public function updateMerchantGroup($submitedformData)
    {
        $data = array (
            'name' => $submitedformData['attributeMerchantGroupName'],
            'publicName' => $submitedformData['attributeMerchantGroupPublicName'],
        );
        $update = $this->_sql->update();
        $update->table($this->_attributeGroupDefinitionsTable);
        $update->set($data);
        $update->where(array(
            'attributeGroupId' => $submitedformData['attributeGroupId'],
            'languageId' => $this->_languages['default-language']
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);

        try {
            $statement->execute();
        } catch (\Exception $e) {
            die('Error: ' . $e->getMessage());
        }
        return $submitedformData['attributeGroupId'];
    }

    /**
     * Delete Merchant Group or Merchant attributes
     */
    public function deleteMerchant($data)
    {
        // Switches and deletes either group or attribute
        $this->_deleteData = $data;
        switch ($data['delete']) {
            case 'group':
                return $this->__deleteColourMerchantGroup();
                break;
            case 'attribute':
                return $this->__deleteColourMerchantAttribute();
                break;
        }
    }

    /**
     * Check the attributes from the group and delete the group
     * If the group has attributes used in products returns false
     * @returns boolean
     */
    private function __deleteColourMerchantGroup()
    {
        // Get the group and check the Source Table
        $attributes = $this->getGroupSourceTable($this->_deleteData['id']);

        // If the group doesn't exists or it's a parent group return
        if (!$attributes || is_null($attributes['parentSource'])) {
            return false;
        }

        $this->_attributesGroupTable = $this->_attributesMerchantsTable;
        $this->_sql->setTable($this->_attributesGroupTable);

        // Verify if the group has used attributes
        if ($attributes['parentSource'] == $this->_coloursGroupsTable) {
            $action = $this->_sql->select()
            ->join(
                array('col' => $attributes['parentSource']),
                'ATTRIBUTES_Merchants.merchantAttributeId = col.colourGroupId'
            )
            ->join(
                array('psr' => 'PRODUCTS_R_ATTRIBUTES_R_SKU_Rules'),
                'ATTRIBUTES_Merchants.attributeId = psr.skuRuleId'
            )
            ->where('ATTRIBUTES_Merchants.attributeGroupId = '. $attributes['attributeGroupId'] )
            ->where('psr.skuRuleTable = "'.$attributes['parentSource'].'"');
        } else {
            $action = $this->_sql->select()
            ->join(
                array('jn' => $attributes['parentSource']),
                'ATTRIBUTES_Merchants.merchantAttributeId = jn.sizeId',
                array(),
                Select::JOIN_RIGHT
            )
            ->join(
                array('pac' => $this->_productsAttributesCombinationTable),
                'ATTRIBUTES_Merchants.attributeGroupId = pac.attributeGroupId',
                array(),
                Select::JOIN_RIGHT
            )
            ->where('ATTRIBUTES_Merchants.attributeGroupId = '. $attributes['attributeGroupId'] )
            ->where('pac.groupType = "Merchant"')
            ->group('jn.sizeId');
        }
        $statement = $this->_sql->prepareStatementForSqlObject($action);

        if ($statement->execute()->count() > 0) {
            return false;
        }

        // Select all the childs of the group that will be deleted
        $this->_sql->setTable($this->_attributesGroupTable);
        $action = $this->_sql->select()
            ->where($this->_attributesGroupTable . '.groupParentId = '. $attributes['attributeGroupId']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        // Promote all the childs in the hierarchy one level
        foreach ($result as $res) {
            $update = $this->_sql->update();
            $update->table($this->_attributesGroupTable);
            $update->set(array('groupParentId' => $attributes['groupParentId'], 'sourceTable' => $attributes['sourceTable'],));
            $update->where(array(
                'attributeGroupId' => $res['attributeGroupId'],
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            $statement->execute();
        }

        // If the group doesn't have used attributes then delete the group and all the attributes entries
        // to that group. Do not delete the attributes from their table except for colours table;
        if ($attributes['parentSource'] == $this->_coloursGroupsTable) {
            $this->_attributesGroupTable = $this->_attributesMerchantsTable;
            $this->_sql->setTable($this->_attributesGroupTable);
            $action = $this->_sql->select()
                ->where('ATTRIBUTES_Merchants.attributeGroupId = '. $attributes['attributeGroupId'] );
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();

            foreach ($result as $res) {
                $this->_attributesGroupTable = $this->_coloursRGroupsTable;
                $this->_sql->setTable($this->_attributesGroupTable);
                $action = $this->_sql->delete()
                    ->where('colourGroupId = ' . $res['merchantAttributeId']);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();

                $this->_attributesGroupTable = $this->_coloursGroupsDefinitionsTable;
                $this->_sql->setTable($this->_attributesGroupTable);
                $action = $this->_sql->delete()
                    ->where('colourGroupId = ' . $res['merchantAttributeId']);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();

                $this->_sql->setTable($this->_coloursGroupsTable);
                $action = $this->_sql->delete()
                    ->where('colourGroupId = ' . $res['merchantAttributeId']);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
            }
        }

        $this->_attributesGroupTable = $this->_attributesMerchantsTable;
        $this->_sql->setTable($this->_attributesGroupTable);
        $action = $this->_sql->delete()
            ->where('attributeGroupId = ' . $this->_deleteData['id']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();

        $this->_attributesGroupTable = $this->_attributeGroupDefinitionsTable;
        $this->_sql->setTable($this->_attributesGroupTable);
        $action = $this->_sql->delete()
            ->where('attributeGroupId = ' . $this->_deleteData['id']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();

        $this->_attributesGroupTable = 'ATTRIBUTES_Groups';
        $this->_sql->setTable($this->_attributesGroupTable);
        $action = $this->_sql->delete()
            ->where('attributeGroupId = ' . $this->_deleteData['id']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();

        return true;
    }

    /**
     * @return bool
     */
    private function __deleteColourMerchantAttribute()
    {
        $this->_sql->setTable($this->_productsAttributesSkuRules);
        $action = $this->_sql->select()
            ->where($this->_productsAttributesSkuRules.'.skuRuleTable = "'.$this->_coloursGroupsTable.'"')
            ->where($this->_productsAttributesSkuRules.'.skuRuleId = '.$this->_deleteData['id']);

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        // If the colour attribute is used in a product variation exit
        if ($statement->execute()->count() > 0 ) {
            return false;
        }

        // Delete From table ATTRIBUTES_Merchants
        $this->_sql->setTable($this->_attributesMerchantsTable);
        $action = $this->_sql->delete()
            ->where('ATTRIBUTES_Merchants.attributeGroupId = ' . $this->_deleteData['from'])
            ->where('ATTRIBUTES_Merchants.merchantAttributeId = ' . $this->_deleteData['id']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();

        // Delete From table COLOURS_R_GROUPS
        $this->_sql->setTable($this->_coloursRGroupsTable);
        $action = $this->_sql->delete()
            ->where('COLOURS_R_GROUPS.colourGroupId = ' . $this->_deleteData['id']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();

        // Delete From COLOURS_Groups_Definitions
        $this->_sql->setTable($this->_coloursGroupsDefinitionsTable);
        $action = $this->_sql->delete()
            ->where('COLOURS_Groups_Definitions.colourGroupId = ' . $this->_deleteData['id']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();

        // Delete From COLOURS
        $this->_sql->setTable($this->_coloursGroupsTable);
        $action = $this->_sql->delete()
            ->where('COLOURS_Groups.colourGroupId = ' . $this->_deleteData['id']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();

        return true;
    }

    /**
     * Gets the group source table
     * Used for merchant groups to see if they are colours/sizes etc.
     * @param $id
     * @returns array
     */
    public function getGroupSourceTable($id)
    {
        $this->_attributesGroupTable = 'ATTRIBUTES_Groups';
        $this->_sql->setTable($this->_attributesGroupTable);

        $action = $this->_sql->select()
            ->join(
                array('pac' => $this->_attributesGroupTable),
                $this->_attributesGroupTable . '.groupParentId = pac.attributeGroupId',
                array('parentSource' => 'sourceTable', 'parentId' => 'attributeGroupId'),
                Select::JOIN_LEFT
            )
            ->where($this->_attributesGroupTable . '.attributeGroupId = ' . $id)
            ->group($this->_attributesGroupTable . '.attributeGroupId');

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $attributes = $statement->execute()->next();

        // If the group is a child then get the group type from the parent
        if ($attributes['parentSource'] == $this->_attributesMerchantsTable) {
            $action = $this->_sql->select()
                ->join(
                    array('pac' => $this->_attributesGroupTable),
                    $this->_attributesGroupTable . '.groupParentId = pac.attributeGroupId',
                    array('parentSource' => 'sourceTable', 'parentId' => 'attributeGroupId'),
                    Select::JOIN_LEFT
                )
                ->where($this->_attributesGroupTable . '.attributeGroupId = ' . $attributes['parentId'])
                ->group($this->_attributesGroupTable . '.attributeGroupId');

            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $attributes['parentSource'] = $statement->execute()->next()['parentSource'];
        }

        return $attributes;
    }

    /**
     *   Retrive all sizes from the table passed into function
     * @param $table
     * @return array
     */
    public function fetchAllSizesFromTable($table)
    {
        $this->_attributesGroupTable = $table;
        $this->_sql->setTable($this->_attributesGroupTable);

        $action = $this->_sql->select()
            ->order('type ASC')
            ->order('uk ASC');

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        $sizes = array();
        foreach ($result as $res) {
            $sizes[] = $res;
        }
        return $sizes;
    }

    /**
     * @param $sizeId
     * @param $table
     * @return mixed
     */
    public function getSizeByIdAndTable($sizeId, $table)
    {
        $this->_sql->setTable($table);
        $action = $this->_sql->select()
            ->where(array('sizeId' => $sizeId));

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute()->next();

        return $result['uk'];
    }

    /**
     * @param $table
     * @return array
     */
    public function fetchAllSizesShirtsAndClothes($table)
    {
        $this->_attributesGroupTable = $table;
        $this->_sql->setTable($this->_attributesGroupTable);

        if ($table == 'SIZES_Belts'  ) {
            $action = $this->_sql->select()
                ->columns(array('sizeId', 'type', 'uk', 'us', 'eur', 'priority'))
                ->join(
                    array('szinfo' => $table . '_Info'),
                    $table . '.uk = szinfo.size',
                    array('size', 'waistInch', 'waistCm'),
                    Select::JOIN_LEFT
                )
                ->order('type ASC')
                ->order('size ASC');
        } elseif ($table == 'SIZES_Trousers') {
            $action = $this->_sql->select()
                ->columns(array('sizeId', 'type', 'uk', 'us', 'eur', 'priority'))
                ->join(
                    array('szinfo' => $table . '_Info'),
                    $table . '.sizeId = szinfo.sizeId',
                    array('size', 'waistInch', 'waistCm', 'legInch', 'legCm'),
                    Select::JOIN_LEFT
                )
                ->order('type ASC')
                ->order('size ASC');
        } elseif ($table == 'SIZES_SwimShorts') {
            $action = $this->_sql->select()
                ->columns(array('sizeId', 'type', 'uk', 'us', 'eur', 'priority'))
                ->join(
                    array('szinfo' => $table . '_Info'),
                    $table . '.sizeId = szinfo.sizeId',
                    array('size', 'waistInch', 'waistCm'),
                    Select::JOIN_LEFT
                )
                ->order('type ASC')
                ->order('size ASC');
        } else {
            $action = $this->_sql->select()
                ->columns(array('sizeId', 'type', 'uk', 'us', 'eur', 'priority'))
                ->join(
                    array('szinfo' => $table . '_Info'),
                    $table . '.uk = szinfo.size',
                    array('size', 'chestInch', 'chestCm', 'eur'),
                    Select::JOIN_LEFT
                )
                ->order('type ASC')
                ->order('size ASC');

        }

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        $sizes = array();
        foreach ($result as $res) {
            $sizes[] = $res;
        }
        return $sizes;
    }

    /**
     *  Modify sizes group based on the form submitted data
     */
    public function modifySizesGroup($submittedData)
    {
        $this->_submittedData = $submittedData;

        // Get all current saved merchant attributes from the size group
        $this->_sql->setTable($this->_attributesMerchantsTable);

        $action = $this->_sql->select()
            ->join(
                array('pac' => $this->_productsAttributesCombinationTable),
                'ATTRIBUTES_Merchants.attributeId = pac.attributeGroupId',
                array('isUsedAsVariant' => 'productOrVariantId', 'groupType' => 'groupType'),
                Select::JOIN_LEFT
            )
            ->where('ATTRIBUTES_Merchants.attributeGroupId = ' . $this->_submittedData['size-group-id']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);

        $result = $statement->execute();

        foreach ($result as $res) {
            // Verify if attributes are used in product combinations
            if ($res['groupType'] != 'Marketing') {
                if ($res['groupType'] == 'Merchant') {
                    if (!in_array($res['merchantAttributeId'],$this->_submittedData['size'])) {
                        return false;
                    }
                }

                // Check if the attributes from the database are send from the post
                // If not then remove them from database because the user has dedeed them
                if (!in_array($res['merchantAttributeId'],$this->_submittedData['size'])) {

                    // Delete from ATTRIBUTES_Merchants
                    $this->_attributesGroupTable = $this->_attributesMerchantsTable;
                    $this->_sql->setTable($this->_attributesGroupTable);

                    $action = $this->_sql->delete()
                        ->where('attributeId = ' . $res['attributeId']);

                    $statement = $this->_sql->prepareStatementForSqlObject($action);
                    $statement->execute();
                }
            }
        };

        // Verify if the sizes are already added to the group
        // If not add them
        foreach ($this->_submittedData['size'] as $sizes) {
            $action = $this->_sql->select()
                ->where('ATTRIBUTES_Merchants.attributeGroupId = ' . $this->_submittedData['size-group-id'])
                ->where('ATTRIBUTES_Merchants.merchantAttributeId = ' . $sizes);

            $statement = $this->_sql->prepareStatementForSqlObject($action);

            // If the size attribute is not part of the group add it
            if ($statement->execute()->count() == 0) {
                $data = array(
                    'attributeGroupId' => $this->_submittedData['size-group-id'],
                    'merchantAttributeId' => $sizes
                );
                try
                {
                    $action = $this->_sql->insert();
                    $action->values($data);

                    $statement = $this->_sql->prepareStatementForSqlObject($action);
                    $statement->execute();
                }
                catch (\Exception $e)
                {
                    throw new \Exception($e);
                }
            }
        }
        return true;
    }

    /**
     * Converting Array keys to CamelCase
     * Needed for the database design
     */
    protected function convertToCamelCase($data = array(), $characterUsed = '_')
    {
        foreach ($data as $key=>$value) {
            $tmpKey = str_replace(' ', '', ucwords(str_replace($characterUsed, ' ', $key)));
            unset ($data[$key]);
            $data[$tmpKey] = $value;
        }
        return $data;
    }

    /**
     * @param $params
     */
    public function enableColours($params)
    {
        $attributeDefinition = array (
            'enabled' => $params['checked']
        );
        $update = $this->_sql->update();
        $update->table($this->_coloursGroupsTable);
        $update->set($attributeDefinition);
        $update->where(array(
            'colourGroupId' => $params['id'],
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        try {
            $statement->execute();
        } catch (\Exception $e) {
            die('Error: ' . $e->getMessage());
        }
    }

    /**
     * Function to extract attribute name and id
     *
     * @param type $attributeDetails
     * @return typ
     */
    public function extractAttributeIdAndName($attributeDetails)
    {
        $returnArray = array();
        if (is_array($attributeDetails) && count($attributeDetails) > 0) {
            $fieldName = strtolower(trim($attributeDetails['name']));
            $fieldNameWithoutBrackets = preg_replace('/\s*\([^)]*\)/', '', $fieldName);
            $returnArray['id'] = str_replace(' ', '-', $fieldNameWithoutBrackets);
            $returnArray['label'] = ucwords($fieldName);
            $returnArray['name'] = lcfirst(str_replace(' ', '', ucwords($fieldNameWithoutBrackets)));
        }

        return $returnArray;
    }

    /**
     * Retrieve all attribute groups
     *
     * @param Int $languageId
     * @param $attributeGroupId
     * @return mixed
     */
    public function fetchAllAttributeGroups($languageId = 1, $attributeGroupId = null)
    {
        $returnArray = array();
        $this->_sql->setTable(array('ag' => $this->_attributesGroupTable));
        $select = $this->_sql->select();
        $select->columns(array(
            'attributeGroupId',
            'viewAs',
            'groupType'
        ));
        $select->where->nest->equalTo('groupType', 'Marketing')->or->equalTo('groupType', 'Core')->unnest;
        $select->join(
            array(
                'agdt' => $this->_attributeGroupDefinitionsTable
            ),
            'ag.attributeGroupId = agdt.attributeGroupId',
            array(
                'attributeGroupDefinitionId',
                'name',
                'publicName',
            )
        );
        if (!is_null($attributeGroupId)) {
            $select->where('ag.attributeGroupId = ' . $attributeGroupId);
        }
        $select->where(array('ag.status' => 'ACTIVE'));
        $select->order(array('ag.groupType ASC', 'ag.viewAs DESC'));
        $select->group('name');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (is_null($attributeGroupId)) {
            $position = 0;
            foreach ($results as $key => $value) {
                if (strtolower($value['viewAs']) == 'select' || strtolower($value['viewAs']) == 'radio') {
                    $value['values'] = $this->getAttributeValues($value['attributeGroupId'], $languageId);
                }
                $returnArray[] = $value;
                $position++;
            }


            //Set keys to split core and marketing attributes
            $keysToSplit = array('Marketing', 'Core');
            $returnArray = $this->_splitAttributes($returnArray, $keysToSplit);
        } else {
            $returnArray = $results->next();
        }
        return $returnArray;
    }

    /**
     * @param $attributeId
     * @return mixed
     */
    public function fetchAttributeGroupById($attributeId)
    {

        return $this->fetchAllAttributeGroups($this->_languages['current-language'], $attributeId);
    }
    /**
     * This function is not currently used. Could be used in phase 2.
     * The function has't been written completely. Dn't want to delete it yet because it can be built upon.
     *
     * @param $languageId
     * @return mixed
     */
    public function fetchAllAttributesInAttributeGroups($languageId = 1)
    {
        $returnArray = array();
        $this->_sql->setTable(array('a' => $this->_attributesTable));
        $select = $this->_sql->select();
        $select->columns(array(
            'attributeId'
        ));
        $select->where->nest->equalTo('agdt.groupType', 'Marketing')->or->equalTo('agdt.groupType', 'Core')->unnest;
        $select->join(
            array('adt' => $this->_attributesDefinitionsTable),
            'a.attributeId = adt.attributeId',
            array('attributeDefinitionId','name')
        );
        $select->join(
            array('agdt' => $this->_attributesGroupTable),
            'a.attributeGroupId = agdt.attributeGroupId',
            array('viewAs', 'groupType')
        );
        $select->order(array('ag.groupType ASC', 'ag.viewAs DESC'));
        $select->group('name');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $position = 0;
        foreach ($results as $key => $value) {
            if (strtolower($value['viewAs']) == 'select' || strtolower($value['viewAs']) == 'radio') {
                $value['values'] = $this->getAttributeValues($value['attributeGroupId'], $languageId);
            }
            $returnArray[] = $value;
            $position++;
        }

        //Set keys to split core and marketing attributes
        $keysToSplit = array('Marketing', 'Core');
        $returnArray = $this->_splitAttributes($returnArray, $keysToSplit);

        return $returnArray;
    }

    /**
     * Function to split attributes array.
     * Currently used to split core and marketing attributes.
     *
     * @param $attributesArray
     * @param $keysToSplit
     * @returns mixed
     */
    private function _splitAttributes($attributesArray = array(), $keysToSplit = array())
    {
        $returnArray = array();
        if (count($attributesArray) > 0 && count($keysToSplit) > 0) {
            foreach ($attributesArray as $key => $attributesDetails) {
                foreach($keysToSplit as $splitKey => $splitValue) {
                    if (isset($attributesDetails['groupType']) && $attributesDetails['groupType'] == $splitValue) {
                        $returnArray[$splitValue][] = $attributesDetails;
                    }
                }
            }
        }

        return $returnArray;
    }

    /**
     * Retrieve marketing product attributes based on attribute group Id
     *
     * @param int $attributeGroupId
     * @param int $languageId
     * @param boolean $deletedOnes
     * @return array
     */
    public function getAttributeValues($attributeGroupId, $languageId = null, $deletedOnes = true)
    {
        if (is_null($languageId)) {
            $languageId = $this->_languages['current-language'];
        }
        $returnArray = array();
        $this->_sql->setTable(array('amt' => $this->_attributesTable));
        $select = $this->_sql->select();
        $select->columns(array('attributeId'));
        $select->where(array(
            'amt.attributeGroupId' => $attributeGroupId,
            'amdt.languageId' => $languageId,
            'amt.status = "ACTIVE"'
        ));
        $select->join(
            array('amdt' => $this->_attributesDefinitionsTable), //Set join table and alias
            'amt.attributeId = amdt.attributeId',
            array('attributeValue', 'attributeCode')
        );

        if (!$deletedOnes) {
            $select->where('amdt.status != "DELETED"');
        }

        $select->order(array('amt.attributeId ASC'));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $position = 0;
        foreach ($results as $key => $value) {
            if (!empty($value['attributeCode'])) {
                $returnArray[$value['attributeId']] = $value['attributeValue'] . ' - ' . $value['attributeCode'];
            } else {
                $returnArray[$value['attributeId']] = $value['attributeValue'];
            }

            $position++;
        }
        return $returnArray;
    }

    /**
     * Get all Core Attributes and builds hierarchy
     * @param null $search
     * @return array|null
     */
    public function fetchAllCoreAttributes($search = null, $orderBy = null, $orderbyOption = null)
    {
        if($orderBy === null){
            $orderBy = 'agd.modified';
        }

        if($orderbyOption === null){
            $orderbyOption = 'desc';
        }

        $this->_sql->setTable($this->_attributesGroupTable);
        $action = $this->_sql->select()
            ->join(
                array('agd' => $this->_attributeGroupDefinitionsTable),
                $this->_attributesGroupTable . '.attributeGroupId = agd.attributeGroupId'
            )
            ->join(
                array('am' => $this->_attributesTable),
                $this->_attributesGroupTable . '.attributeGroupId = am.attributeGroupId',
                array('attributesCount' => new \Zend\Db\Sql\Expression('COUNT(attributeId)')),
                Select::JOIN_LEFT
            )
            ->where('agd.languageId = '. $this->_languages['default-language'])
            ->where($this->_attributesGroupTable . '.groupType = "Core"')
            ->group($this->_attributesGroupTable . '.attributeGroupId');

        // Search
        if ($search !== null) {
            $action->where
                ->nest
                ->like('agd.name', '%' . $search . '%')
                ->or
                ->like('agd.publicName', '%' . $search . '%')
                ->unnest;
        }
        $action->order($orderBy ." " .  $orderbyOption);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        return $this->_processGroupAttributes($result, $search);
    }

    /**
     * Save Core group attribute public name
     * @param $submittedData
     * @throw \Exception
     */
    public function saveCoreGroupAttribute($submittedData)
    {
        $data = array (
            'publicName' => $submittedData['publicName']
        );
        $update = $this->_sql->update();
        $update->table($this->_attributeGroupDefinitionsTable);
        $update->set($data);
        $update->where(array(
            'attributeGroupId' => $submittedData['attributeGroupId'],
            'languageId' => $this->_languages['default-language']
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        try {
            $statement->execute();
        } catch (\Exception $e) {
            echo '<pre>';
            print_r($e);
        }
    }
    /**
     * Note: Update the set filter flag for the group.
     * Update the set filter flag for all parents of the group.
     * Update the set filter flag for all children of the group
     *
     */
    public function setFilter($params)
    {
        $data = array ('usedForFiltering' => $params['checked']);
        // Set the table

        // Update the set filter flag for the group
        $update = $this->_sql->update();
        $update->table($this->_attributesGroupTable);
        $update->set($data);
        $update->where(array(
            'attributeGroupId' => $params['id'],
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        try {
            $statement->execute();
        } catch (\Exception $e) {
            echo '<pre>';
            print_r($e);
        }

        if ($params['checked'] == 1) {
            // Update the set filter flag for all parent of the group
            $this->_updateFilterFlagForParent($params['id'], $data);
        }

        // Update the set filter flag for all children of the group
        $this->_updateFilterFlagForChildren($params['id'], $data);

    }

    /**
     * Note : Select the first parent , update the flag, set the param id for the current group, Recursive
     * @param $id
     * @param $data
     * @return bool
     * @throw \Exception
     */
    private function _updateFilterFlagForParent($id, $data)
    {
        // Get the current group
        $action = $this->_sql->select()
            ->where($this->_attributesGroupTable . '.attributeGroupId = ' . $id);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        try {
            $result = $statement->execute()->next();
        } catch (\Exception $e) {
            echo '<pre>';
            print_r($e);
        }

        // Update parent group with flag
        $update = $this->_sql->update();
        $update->table($this->_attributesGroupTable);
        $update->set($data);
        $update->where(array(
            'attributeGroupId' => $result['groupParentId'],
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        try {
            $statement->execute();
        } catch (\Exception $e) {
            echo '<pre>';
            print_r($e);
        }
        // If parent of group is top level stop recursive function
        if ($result['groupParentId'] == '0') {
            return false;
        }
        // Recursiv function call
        $this->_updateFilterFlagForParent($result['groupParentId'], $data);
    }
    /**
     * Note : Select the first children , update the flag, set the param id for the current group, Recursive
     * @param $id
     * @param $data
     * @return bool
     * @throw \Exception
     */
    private function _updateFilterFlagForChildren($id, $data)
    {
        // Get the current group
        $action = $this->_sql->select()
            ->where($this->_attributesGroupTable . '.attributeGroupId = ' . $id);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        try {
            $result = $statement->execute()->next();
        } catch (\Exception $e) {
            echo '<pre>';
            print_r($e);
        }

        // Get the children group and check if the group has children
        $action = $this->_sql->select()
            ->where($this->_attributesGroupTable . '.groupParentId = ' . $id);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        try {
            $result = $statement->execute();
        } catch (\Exception $e) {
            echo '<pre>';
            print_r($e);
        }

        // If the group dosen't have children kill it
        if (empty($result)) {
            return false;
        }
        foreach ($result as $res) {
            // Recursive function to update all children of $res
            $this->_updateChildrenFlagRecursive($res, $data);
            // Update children group with flag
            $update = $this->_sql->update();
            $update->table($this->_attributesGroupTable);
            $update->set($data);
            $update->where(array(
                'attributeGroupId' => $res['attributeGroupId'],
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            try {
                $statement->execute();
            } catch (\Exception $e) {
                echo '<pre>';
                print_r($e);
            }
        }
    }

    /**
     * Note : Selects all of the children groups
     * Iterate the groups and foreach group set the filter flag and recursive access the function
     * @param $group
     * @param $data
     * @return bool
     * @throw \Exception
     */
    private function _updateChildrenFlagRecursive($group, $data)
    {
        // Get all of group childrens
        $action = $this->_sql->select()
            ->where($this->_attributesGroupTable . '.groupParentId = ' . $group['attributeGroupId']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        try {
            $result = $statement->execute();
        } catch (\Exception $e) {
            echo '<pre>';
            print_r($e);
        }

        if (empty($result)) {
            return false;
        }
        // Foreach children of $group loops again in recursive function _updateChildrenFlagRecursive
        foreach ($result as $res) {
            $update = $this->_sql->update();
            $update->table($this->_attributesGroupTable);
            $update->set($data);
            $update->where(array(
                'attributeGroupId' => $res['attributeGroupId'],
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            try {
                $statement->execute();
            } catch (\Exception $e) {
                echo '<pre>';
                print_r($e);
            }
            // Recursiv function call
            $this->_updateChildrenFlagRecursive($res, $data);
        }
    }
    /**
     * Get all top level ( 0 ) Merchant Attributes Groups
     */
    public function fetchTopMerchantGroups()
    {
        $this->_sql->setTable($this->_attributesGroupTable);

        $action = $this->_sql->select()
            ->join(
                array('agd' => $this->_attributeGroupDefinitionsTable),
                $this->_attributesGroupTable . '.attributeGroupId = agd.attributeGroupId',
                array('*'),
                Select::JOIN_LEFT
            )
            ->where($this->_attributesGroupTable . '.groupParentId = 0')
            ->where($this->_attributesGroupTable . '.groupType = "Merchant"');

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $topMerchantGroups = array();
        foreach ($result as $res) {
            $topMerchantGroups[$res['attributeGroupId']] = $res['name'];
        }

        return $topMerchantGroups;
    }

    /**
     * @param $attributeId
     * @return bool
     */
    public function activateAttributeValue($attributeId)
    {
        $this->_sql->setTable($this->_attributesDefinitionsTable);
        $select = $this->_sql->select()->where(array('attributeId = ' . $attributeId, 'languageId' => $this->_languages['current-language']));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();

        if ($result['status'] == 'ACTIVE') {
            $newStatus = 'INACTIVE';
            $flag = true;
        } else {
            $newStatus = 'ACTIVE';
            $flag = false;
        }

        $update = $this->_sql->update();
        $update->table($this->_attributesDefinitionsTable);
        $update->set(array('status' => $newStatus));
        $update->where(array(
            'attributeId' => $attributeId,
            'languageId' => $this->_languages['current-language']
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();


        return $flag;
    }

    /**
     * Fetches the attribute groups that are new for that specific product.
     * It is used in the product edit screen to differentiate between
     *  existing attributes on that product and new ones
     * @param $productOrVariantId
     * @param $isVariant
     * @returns array
     */
    public function fetchExistingAttributeGroupsIdsForProduct($productOrVariantId, $isVariant = 0)
    {
        $this->_sql->setTable($this->_productsAttributesCombinationTable);
        $action = $this->_sql->select()
            ->where(array("productOrVariantId" => $productOrVariantId))
            ->where(array("isVariant" => $isVariant));

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        $existingAttributeGroupsIds = array();
        foreach ($result as $res) {
            $existingAttributeGroupsIds[] = $res['attributeGroupId'];
        }

        return $existingAttributeGroupsIds;
    }

    /**
     * @param $productId
     * @return array
     */
    public function fetchAllAttributeGroupsForMarketingCategory($productId)
    {
        $this->_sql->setTable(array('pcr' => $this->_productsCategoriesRelationshipTable));
        $select = $this->_sql->select()
            ->join(
                array('cra' => $this->_categoriesRelationshipAttributesTable),
                'cra.categoryId = pcr.categoryId',
                array('attributeGroupId')
            );
        $select->where(array(
            'pcr.productId' => $productId,
            'cra.status' => 'ACTIVE',
            'pcr.status' => 'ACTIVE'
        ));
        $select->group('cra.attributeGroupId');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $attributeGroupIdsArray = array();
        foreach ($results as $result) {
            $attributeGroupIdsArray[] = $result['attributeGroupId'];
        }

        return $attributeGroupIdsArray;
    }

    /**
     * @param $productOrVariantId
     * @param int $isVariant
     * @param int $languageId
     * @return array
     */
    public function fetchCoreAttributesForProduct($productOrVariantId, $isVariant = 0, $languageId = 1, $code = false)
    {

        $this->_sql->setTable(array('pac' => $this->_productsAttributesCombinationTable));
        $action = $this->_sql->select()
            ->columns(array('attributeValue', 'groupType'))
            ->join(
                array('agd' => $this->_attributeGroupDefinitionsTable),
                'agd.attributeGroupId = pac.attributeGroupId',
                array('publicName')
            )
            ->join(
                array('pacd' => $this->_productsAttributesCombinationDefinitions),
                "pac.productAttributeCombinationId = pacd.productAttributeCombinationId",
                array('textValue' => 'attributeValue'),
                Select::JOIN_LEFT
            );

        if ($code) {
            $action->join(
                array('ad' => $this->_attributesDefinitionsTable),
                new \Zend\Db\Sql\Expression("ad.attributeId = pac.attributeValue AND pac.groupType = 'SELECT'"),
                array('selectAttributeValue' => 'attributeCode'),
                Select::JOIN_LEFT
            );
        } else {
            $action->join(
                array('ad' => $this->_attributesDefinitionsTable),
                new \Zend\Db\Sql\Expression("ad.attributeId = pac.attributeValue AND pac.groupType = 'SELECT'"),
                array('selectAttributeValue' => 'attributeValue'),
                Select::JOIN_LEFT
            );
        }

           $action->where(array(
                "pac.productOrVariantId" => $productOrVariantId,
                "pac.isVariant" => $isVariant,
                "agd.languageId" => $languageId,
                "agd.status" => 'ACTIVE',
                "pac.status" => 'ACTIVE',
            ));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();

        $finalArray = array();
        foreach ($results as $result) {
            $attributeName = str_replace(' ', '_', strtolower($result['publicName']));
            $attributeValue = $result['attributeValue'];
            //SELECT boxes and TEXT fields get their values from other fields
            //so let's attempt to override it
            $groupType = !empty($result['groupType']) ? trim(strtolower($result['groupType'])) : '';
            if ($groupType == 'select' && !empty($result['selectAttributeValue'])) {
                $attributeValue = $result['selectAttributeValue'];
            } 
            if ($groupType == 'text' && !empty($result['textValue'])) {
                $attributeValue = $result['textValue'];
            }
            $finalArray[$attributeName] = $attributeValue;
        }
        return $finalArray;
    }

    /**
     * @param $productId
     */
    public function getAttributesNumberForProduct($productId)
    {

    }

    /**
     * @param $category
     * @param $table
     * @return array
     */
    public function getSizeGuideBySizeCategoryAndTable($category, $table)
    {
        $this->_sql->setTable($table);
        $select = $this->_sql->select();
        if ($table == 'SIZES_Clothes' || $table == 'SIZES_Shirts' || $table == 'SIZES_Belts' || $table == 'SIZES_Jackets' ) {
            $select->join(
                array('info' => $table . '_Info'),
                "info.size = " . $table . ".uk",
                array('*')
            );
        } elseif ($table == 'SIZES_Trousers' || $table == 'SIZES_SwimShorts') {
            $select->join(
                array('info' => $table . '_Info'),
                "info.sizeId = " . $table . ".sizeId",
                array('*')
            );
        }
        $select->where($table . '.type = "' . $category . '"');
        $select->order($table. '.priority');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $sizes = array();
        foreach ($results as $result) {
            $sizes[] = $result;
        }
        return $sizes;
    }

    /**
     * @param $attributeValueId
     */
    public function getAttributeValue($attributeValueId)
    {
        $this->_sql->setTable($this->_attributesDefinitionsTable);
        $select = $this->_sql->select()
            ->where('attributeId = ' . $attributeValueId)
            ->where('languageId = ' . $this->_languages['current-language']);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        return $statement->execute()->next();
    }

    /**
     * @param $attributeId
     * @param $productOrVariantId
     * @param int $isVariant
     * @return bool
     */
    public function getAttributeValueForProduct($attributeId, $productOrVariantId, $isVariant = 1)
    {
        $this->_sql->setTable('PRODUCTS_ATTRIBUTES_Combination');
        $this->_sql->select();
        $select = $this->_sql->select()
            ->where(array(
                'productOrVariantId' => $productOrVariantId,
                'isVariant' => $isVariant,
                'attributeGroupId' => $attributeId,
                'status' => 'ACTIVE'
            ));
        $select->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();
        if (!empty($result)) {
            if ($result['groupType'] == 'TEXT') {
                $this->_sql->setTable('PRODUCTS_ATTRIBUTES_Combination_Definitions');
                $select = $this->_sql->select()
                    ->where(array(
                        'productAttributeCombinationId' => $result['productAttributeCombinationId'],
                        'languageId' => $this->_languages['current-language']
                    ));
                $statement = $this->_sql->prepareStatementForSqlObject($select);
                $result = $statement->execute()->next();
                if (!empty($result)) {
                    return $result['attributeValue'];
                }
            } else {
                return $result['attributeValue'];
            }
        }
        return false;
    }

    /**
     * @param $supplier
     * @param $supplierCode
     * @param $attributeGroupIdForSupplier
     * @return bool|int
     */
    public function updateSupplierRecord($supplier, $supplierCode, $attributeGroupIdForSupplier)
    {
        try {
            $this->_sql->setTable($this->_attributesDefinitionsTable);
            $select = $this->_sql->select()
                ->where(array(
                    'languageId' => $this->_languages['current-language'],
                    'attributeCode' => $supplierCode,
                ));
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();

            if (count($result) > 0) {
                $attributeValueId = $result->next()['attributeId'];

                $update = $this->_sql->update();
                $update->table($this->_attributesTable);
                $update->set(array(
                    'status' => $supplier['active'] ? 'ACTIVE' : 'INACTIVE',
                ));
                $update->where(array(
                    'attributeId' => $attributeValueId,
                ));
                $update->where('status != "DELETED"');
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();

                $update = $this->_sql->update();
                $update->table($this->_attributesDefinitionsTable);
                $update->set(array(
                    'attributeValue' => $supplier['supplierName'],
                    'status' => $supplier['active'] ? 'ACTIVE' : 'INACTIVE',
                    'modified' => date('Y-m-d H:i:s'),
                    'attributeCode' => $supplierCode
                ));
                $update->where(array(
                    'languageId' => $this->_languages['current-language'],
                    'attributeId' => $attributeValueId,
                ));
                $update->where('status != "DELETED"');
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $result = $statement->execute();
                return $result->getAffectedRows();
            }
            return false;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $supplier
     * @param $supplierCode
     * @param $attributeGroupIdForSupplier
     */
    public function insertNewSupplier($supplier, $supplierCode, $attributeGroupIdForSupplier)
    {
        $this->_sql->setTable($this->_attributesTable);
        $action = $this->_sql->insert();
        $action->values(array(
            'attributeGroupId' => $attributeGroupIdForSupplier,
            'status' => $supplier['active'] ? 'ACTIVE' : 'INACTIVE',
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();

        $id = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

        $this->_sql->setTable($this->_attributesDefinitionsTable);
        $action = $this->_sql->insert();
        $action->values(array(
            'languageId' => $this->_languages['current-language'],
            'attributeId' => $id,
            'attributeValue' => $supplier['supplierName'],
            'attributeCode' => $supplierCode,
            'status' => $supplier['active'] ? 'ACTIVE' : 'INACTIVE',
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();

    }
}
