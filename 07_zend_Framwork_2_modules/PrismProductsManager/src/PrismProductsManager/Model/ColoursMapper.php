<?php
namespace PrismProductsManager\Model;

use Zend\Db\Adapter\Adapter;
use PrismProductsManager\Model\ColourEntity;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;

/**
 * Class ColoursMapper
 * @package PrismProductsManager\Model
 */
class ColoursMapper
{
    /**
     * @var string
     */
    private $_tableName = 'COLOURS';
    /**
     * @var string
     */
    private $_coloursDefinitionsTable = 'COLOURS_Definitions';
    /**
     * @var string
     */
    private $_coloursGroupsTable = 'COLOURS_Groups';
    /**
     * @var string
     */
    private $_coloursGroupsDefinitionsTable = 'COLOURS_Groups_Definitions';
    /**
     * @var string
     */
    private $_coloursRelationGroupsTable = 'COLOURS_R_GROUPS';
    /**
     * @var string
     */
    private $_attributesMerchantsTable = 'ATTRIBUTES_Merchants';
    /**
     * @var string
     */
    private $_variantsTable = 'PRODUCTS_R_ATTRIBUTES';
    /**
     * @var string
     */
    private $_variantsSkuRulesTable = 'PRODUCTS_R_ATTRIBUTES_R_SKU_Rules';
    /**
     * @var Sql
     */
    private $_sql;
    /**
     * @var
     */
    private $_languagesArray;
    /**
     * @var
     */
    private $_currentLanguage;

    /**
     * @var Adapter
     */
    private $_dbAdapter;
    /**
     * @var
     */
    private $_rsMapper;

    /**
     * @param Adapter $dbAdapter
     * @param $languagesConfig
     * @param $rsMapper
     */
    public function __construct(Adapter $dbAdapter, $languagesConfig, $rsMapper)
    {
        $this->_dbAdapter = $dbAdapter;
        $this->_sql = new Sql($dbAdapter);
        $this->_sql->setTable($this->_tableName);
        $this->_languagesArray = $languagesConfig['site-languages'];
        $this->_currentLanguage = $languagesConfig['current-language'];
        $this->_rsMapper = $rsMapper;
    }

    /**
     * @param null $languageId
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function fetchAll($languageId = null)
    {
        $this->_sql->setTable($this->_tableName);
        $select = $this->_sql->select();
        $select->join(
            array('CD' => $this->_coloursDefinitionsTable),
            "COLOURS.colourId = CD.colourId",
            array(
                'colourName'
            )
        );
        $select->where(
            array(
                "CD.languageId" => !empty($languageId) ? $languageId : $this->_currentLanguage
            )
        );
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * @param $languageId
     * @param null $colourGroupId
     * @param bool|false $activeOnly
     * @return void|\Zend\Db\Adapter\Driver\ResultInterface
     */
    public function fetchAllColourGroups($languageId, $colourGroupId = null, $activeOnly = false)
    {
        $this->_sql->setTable(array('CG' => $this->_coloursGroupsTable));
        $select = $this->_sql->select();
        $select->columns(array('colourGroupId', 'groupCode'));
        $select->join(
            array('CGD' => $this->_coloursGroupsDefinitionsTable),
            "CG.colourGroupId = CGD.colourGroupId",
            array(
                'name'
            )
        );
        $select->where(
            array(
                "CGD.languageId" => $languageId
            )
        );

        if ($activeOnly) {
            $select->where('CG.enabled = 1');
        }

        if (!is_null($colourGroupId)) {
            $select->where('CG.colourGroupId = ' . $colourGroupId);
        }
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!is_null($colourGroupId)) {
            return $results->next();
        }
        return $results;
    }

    /**
     * @param $merchantAttributeId
     * @return array
     */
    public function fetchAllColourGroupsIdsForMerchant($merchantAttributeId)
    {
        $this->_sql->setTable(array('AM' => $this->_attributesMerchantsTable));
        $select = $this->_sql->select();
        $select->columns(array('merchantAttributeId'));
        $select->where(
            array(
                "AM.attributeGroupId" => $merchantAttributeId
            )
        );
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $finalArray = array();
        foreach($results as $result) {
            $finalArray[] = $result['merchantAttributeId'];
        }
        return $finalArray;
    }

    /**
     * @param $colourGroupId
     * @param int $languageId
     * @return bool|void
     */
    public function getColourGroupDetails($colourGroupId, $languageId = 1)
    {
        $colourGroupId = (int) $colourGroupId;

        $this->_sql->setTable(array('cg' => $this->_coloursGroupsTable));
        $select = $this->_sql->select();
        $select->columns(array('groupCode'));
        $select->join(
            array('cgd' => $this->_coloursGroupsDefinitionsTable),
            'cg.colourGroupId = cgd.colourGroupId',
            array(
                'name'
            )
        );
        $select->where(array(
            'cg.colourGroupId' => $colourGroupId,
            'cgd.languageId' => $languageId
        ));
        $select->limit(1);

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results->count() == 1) {
            return $results->next();
        } else {
            return false;
        }
    }


    /**
     * @param $id
     * @return array|int|mixed
     */
    public function getValueById($id)
    {
        $id = (int) $id;

        $select = $this->_sql->select();
        $select->where('colourid ='. $id);

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $id = array();
        foreach ($results as $result) {
            $id = $result;
        }
        return $id;
    }

    /**
     * @param int $languageId
     * @return array
     */
    public function fetchAllColorsOptions($languageId = 1)
    {
        $results = $this->fetchAll($languageId);

        $finalArray = array();
        foreach ($results as $result) {
            $finalArray[$result['colourId']] = $result['colourName'];
        }
        return $finalArray;
    }

    /**
     * @param $merchantAttributeGroupId
     * @param int $languageId
     * @return array
     */
    public function fetchAllColorsGroupsOptions($merchantAttributeGroupId, $languageId = 1)
    {
        $results = $this->fetchAllColourGroups($languageId);
        $allGroupsForThisMerchant = $this->fetchAllColourGroupsIdsForMerchant($merchantAttributeGroupId);
        $finalArray = array();
        foreach ($results as $result) {
            if (!in_array($result['colourGroupId'], $allGroupsForThisMerchant)) {
                $finalArray[$result['colourGroupId']] = $result['name'];
            }
        }
        return $finalArray;
    }

    /**
     * Delete colour if not used in creating sku
     * @param $colourId
     * @return bool
     */
    public function deleteColour($colourId)
    {
        $result = [
            'error' => true,
            'message' => 'The colour could not be deleted.'
        ];

        // get colour code by colourId
        $colourDetails  = $this->getValueById($colourId);

        if(!$colourDetails){
            $result['message'] = 'Colour details not found.';
            return $result;
        }

        $colourCode = $colourDetails['colourCode'];
        $allowDelete = $this->allowToDelete($colourId);

        if($allowDelete['allowDelete'] === true){

            try
            {
                $this->_sql->setTable($this->_coloursDefinitionsTable);
                $deleteAction = $this->_sql->delete()->where('colourId = ' . $colourId);
                $statement = $this->_sql->prepareStatementForSqlObject($deleteAction);
                $delete = $statement->execute();

                $this->_sql->setTable($this->_tableName);
                $deleteAction = $this->_sql->delete()->where('colourId = ' . $colourId);
                $statement = $this->_sql->prepareStatementForSqlObject($deleteAction);
                $delete = $statement->execute();

                return true;
            }catch (\Exception $ex)
            {
                return $result ;
            }

        }else{
            $result['message'] = 'The colour could not be deleted. It\'s used to create merchant attribute group or product SKU';
            return $result;
        }
        return $result;
    }

    protected function allowToDelete($colourId)
    {
        $result = [
            "allowDelete" => false,
            "groupList" => [],
        ];

        $groups = $this->getGroupByColourId($colourId);

        if(empty($groups))
        {
            $result['allowDelete'] = true;
            return $result;
        }

        $result['groupList'] = $groups;
        return $result;
    }

    public function getGroupByColourId($colourId)
    {
        $this->_sql->setTable($this->_coloursRelationGroupsTable);
        $select = $this->_sql->select();
        $select->where('colourId ='. $colourId);

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $ids = [];

        foreach ($results as $result) {
            $ids[] = $result;
        }
        return $ids;
    }

    /**
     * Used to insert / update new colour
     * @param $processedData
     * @return bool
     */
    public function saveColour($processedData)
    {
        if (empty($processedData['colourId'])) {
            $this->_insertColour($processedData);
            return true;
        } else {
            $this->_updateColour($processedData);
            return false;
        }
    }

    /**
     * @param $processedData
     */
    private function _insertColour($processedData)
    {
        // insert data into colour_groups table
        $insertAction = $this->_sql->insert();
        $insertAction->values(array(
            'colourCode' => strtoupper($processedData['colourCode']),
            'hex' => strtoupper($processedData['colourHex'])
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($insertAction);
        $statement->execute();
        $colourId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

        // insert data into colour_groups_definitions table for each language
        foreach ($this->_languagesArray as $languageId) {
            $this->_sql->setTable($this->_coloursDefinitionsTable);
            $insertAction = $this->_sql->insert();
            $insertAction->values(array(
                'colourId' => $colourId,
                'languageId' => $languageId,
                'colourName' => $processedData['colourName']
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($insertAction);
            $statement->execute();
        }
    }

    /**
     * @param $processedData
     */
    private function _updateColour($processedData)
    {
        // update colour_groups table
        $update = $this->_sql->update();
        $update->set(array(
            'colourCode' => strtoupper($processedData['colourCode']),
            'hex' => strtoupper($processedData['colourHex'])
        ));
        $update->where(array(
            'colourId' => $processedData['colourId'],
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();

        // update colour_groups_definitions table for current language
        $update = $this->_sql->update();
        $update->table($this->_coloursDefinitionsTable);
        $update->set(array('colourName' => $processedData['colourName']));
        $update->where(array(
            'colourId' => $processedData['colourId'],
            'languageId' => $this->_currentLanguage
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     * Used to insert / update new colour group
     * @param $processedData
     * @param mixed $attributeGroupId
     * @return bool
     */
    public function saveColourAttribute($processedData, $attributeGroupId)
    {
        $result = [
            'saveMethod' => null,
            'colourGroupId' => null,
        ];

        if (empty($processedData['colourGroupId'])) {
            $result['saveMethod'] = 'ADD';
            $result['colourGroupId'] = $this->_insertColourAttribute($processedData, $attributeGroupId);
        } else {
            $result['saveMethod'] = 'UPDATE';
            $result['colourGroupId'] = $this->_updateColourAttribute($processedData, $attributeGroupId);

        }
        return $result;
    }

    /**
     * @param $attributeGroupId
     * @return array
     */
    public function getAllProductsForColour($attributeGroupId)
    {
        $this->_sql->setTable(array('pasr' => $this->_variantsSkuRulesTable));
        $select = $this->_sql->select()
            ->join(
                array('pa' => $this->_variantsTable),
                'pa.productAttributeId = pasr.productAttributeId',
                array('productId')
            )
            ->where('pasr.skuRuleTable="'.$this->_coloursGroupsTable.'"')
            ->where('pasr.skuRuleId="'.$attributeGroupId.'"')
            ->where('pa.status="ACTIVE"')
            ->group('pa.productId');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $finalProductsArray = array();
        foreach ($results as $result) {
            $finalProductsArray[] = $result['productId'];
        }

        return $finalProductsArray;
    }

    /**
     * @param $processedData
     * @param $attributeGroupId
     * @return int
     */
    private function _insertColourAttribute($processedData, $attributeGroupId)
    {
        // insert data into colour_groups table
        $this->_sql->setTable($this->_coloursGroupsTable);
        $insertAction = $this->_sql->insert();
        $insertAction->values(array(
            'groupCode' => strtoupper($processedData['colourGroupCode']),
            'hasImage' => $processedData['hasImage'],
            'imageId' => $processedData['imageId'],
            'hex' => strtoupper($processedData['hex'])
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($insertAction);
        $statement->execute();
        $colourGroupId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

        // insert data into colour_groups_definitions table for each language
        foreach ($this->_languagesArray as $languageId) {
            $this->_sql->setTable($this->_coloursGroupsDefinitionsTable);
            $insertAction = $this->_sql->insert();
            $insertAction->values(array(
                'colourGroupId' => $colourGroupId,
                'languageId' => $languageId,
                'name' => $processedData['colourGroupName']
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($insertAction);
            $statement->execute();
        }
        // insert data into colour_r_groups table
        foreach ($processedData['colours'] as $colourId) {
            $this->_sql->setTable($this->_coloursRelationGroupsTable);
            $insertAction = $this->_sql->insert();
            $insertAction->values(array(
                'colourId' => $colourId,
                'colourGroupId' => $colourGroupId
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($insertAction);
            $statement->execute();
        }
        // insert data into attributes_merchants table
        $this->_sql->setTable($this->_attributesMerchantsTable);
        $insertAction = $this->_sql->insert();
        $insertAction->values(array(
            'attributeGroupId' => $attributeGroupId,
            'merchantAttributeId' => $colourGroupId
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($insertAction);
        $statement->execute();

        return $colourGroupId;
    }

    /**
     * @param $processedData
     * @param $attributeGroupId
     */
    private function _updateColourAttribute($processedData, $attributeGroupId)
    {
        // update colour_groups table
        $update = $this->_sql->update();
        $update->table($this->_coloursGroupsTable);
        $update->set(array(
            'groupCode' => strtoupper($processedData['colourGroupCode']),
            'hasImage' => $processedData['hasImage'],
            'imageId' => $processedData['imageId'],
            'hex' => strtoupper($processedData['hex'])
        ));
        $update->where(array(
            'colourGroupId' => $processedData['colourGroupId'],
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();

        // update colour_groups_definitions table for current language
        $update = $this->_sql->update();
        $update->table($this->_coloursGroupsDefinitionsTable);
        $update->set(array('name' => $processedData['colourGroupName']));
        $update->where(array(
            'colourGroupId' => $processedData['colourGroupId'],
            'languageId' => $this->_currentLanguage
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();

        // update colour_r_groups table
        // first delete all the colours saved for this group and after set up the new ones
        $this->_sql->setTable($this->_coloursRelationGroupsTable);
        $deleteAction = $this->_sql->delete()->where('colourGroupId = ' . $processedData['colourGroupId']);
        $statement = $this->_sql->prepareStatementForSqlObject($deleteAction);
        $statement->execute();

        foreach ($processedData['colours'] as $colourId) {
            $insertAction = $this->_sql->insert();
            $insertAction->values(array(
                'colourId' => $colourId,
                'colourGroupId' => $processedData['colourGroupId']
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($insertAction);
            $statement->execute();
        }

        return $processedData['colourGroupId'];
    }

    /**
     * @param $colourGroupName
     * @param $colourGroupCode
     * @return bool
     */
    public function checkIfColourGroupExists($colourGroupName, $colourGroupCode)
    {
        $this->_sql->setTable(array('cg' => $this->_coloursGroupsTable));
        $action = $this->_sql->select()
            ->join(
                array('cgd' => $this->_coloursGroupsDefinitionsTable),
                "cg.colourGroupId = cgd.colourGroupId"
            )
            ->where("cgd.name='".$colourGroupName."' OR cg.groupCode='".$colourGroupCode."'");
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        return ($result->count() > 0) ? true : false;
    }

    /**
     * @param $colourGroupsIds
     * @param $attributeGroupId
     * @return bool
     */
    public function bindColourGroupsToAttributeGroup($colourGroupsIds, $attributeGroupId)
    {
        $this->_sql->setTable($this->_attributesMerchantsTable);
        foreach ($colourGroupsIds as $colourGroupId) {
            $insertAction = $this->_sql->insert();
            $insertAction->values(array(
                'attributeGroupId' => $attributeGroupId,
                'merchantAttributeId' => $colourGroupId
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($insertAction);
            $statement->execute();
        }
        return true;
    }

    /**
     * @param $colourGroup
     * @return array
     */
    public function getColourGroupImage($colourGroup) {
        $imageId = $colourGroup->imageId;
        $image = [];
        if (!empty($imageId)) {
            $image = $this->_rsMapper->getImage($imageId ,'colour-image');
        }
        return $image;
    }

    public function importColours()
    {
        try {
//            $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=0;")->execute();
//            $this->_dbAdapter->query("TRUNCATE COLOURS;")->execute();
//            $this->_dbAdapter->query("TRUNCATE COLOURS_Definitions;")->execute();
//            $this->_dbAdapter->query("TRUNCATE COLOURS_Groups;")->execute();
//            $this->_dbAdapter->query("TRUNCATE COLOURS_Groups_Definitions;")->execute();
//            $this->_dbAdapter->query("TRUNCATE COLOURS_R_GROUPS;")->execute();
//            $this->_dbAdapter->query("TRUNCATE ATTRIBUTES_Merchants;")->execute();
//            $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=1;")->execute();
            $this->_sql->setTable('PRODUCTS_Colours');
            $select = $this->_sql->select();
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            if (empty($results)) {
                return false;
            }
            $colourNr = 1;
            foreach ($results as $key => $colour) {
                try {
                    echo 'Processing colour ' . $colourNr . ' out of ' . count($results) . "\r";
                    /** Insert Colour details */
                    $this->_sql->setTable("COLOURS_Groups");
                    $insert = $this->_sql->insert()
                        ->values(array(
                            'groupCode' => $colour['colour'],
                            'hasImage' => 0,
                            'hex' => '#000000',
                            'enabled' => 1
                        ));
                    $statement = $this->_sql->prepareStatementForSqlObject($insert);
                    $statement->execute();
                    $colourGroupId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

                    $this->_sql->setTable("COLOURS");
                    $insert = $this->_sql->insert()
                        ->values(array(
                            'colourCode' => $colour['colour'],
                            'hex' => '#000000',
                        ));
                    $statement = $this->_sql->prepareStatementForSqlObject($insert);
                    $statement->execute();
                    $colourId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();


                    $this->_sql->setTable("COLOURS_Groups_Definitions");
                    $insert = $this->_sql->insert()
                        ->values(array(
                            'colourGroupId' => $colourGroupId,
                            'languageId' => 1,
                            'name' => $colour['description']
                        ));
                    $statement = $this->_sql->prepareStatementForSqlObject($insert);
                    $statement->execute();

                    $this->_sql->setTable("COLOURS_Definitions");
                    $insert = $this->_sql->insert()
                        ->values(array(
                            'colourId' => $colourId,
                            'languageId' => 1,
                            'colourName' => $colour['description']
                        ));
                    $statement = $this->_sql->prepareStatementForSqlObject($insert);
                    $statement->execute();

                    $this->_sql->setTable("COLOURS_R_GROUPS");
                    $insert = $this->_sql->insert()
                        ->values(array(
                            'colourId' => $colourId,
                            'colourGroupId' => $colourGroupId,
                        ));
                    $statement = $this->_sql->prepareStatementForSqlObject($insert);
                    $statement->execute();

                    $colourNr++;
                } catch (\Exception $ex) {
                    continue;
                }
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            return false;
        }
    }
}