<?php
namespace PrismProductsManager\Model;


class AttributesMerchantsEntity
{

    protected $_attributeId = null;

    protected $_attributeGroupId = null;

    protected $_merchantAttributeId;

    public function __construct()
    {

    }

    /**
     * @param mixed $merchantAttributeId
     */
    public function setMerchantAttributeId($merchantAttributeId)
    {
        $this->_merchantAttributeId = $merchantAttributeId;
    }

    /**
     * @return mixed
     */
    public function getMerchantAttributeId()
    {
        return $this->_merchantAttributeId;
    }

    /**
     * @param null $attributeGroupId
     */
    public function setAttributeGroupId($attributeGroupId)
    {
        $this->_attributeGroupId = $attributeGroupId;
    }

    /**
     * @return null
     */
    public function getAttributeGroupId()
    {
        return $this->_attributeGroupId;
    }

    /**
     * @param null $attributeId
     */
    public function setAttributeId($attributeId)
    {
        $this->_attributeId = $attributeId;
    }

    /**
     * @return null
     */
    public function getAttributeId()
    {
        return $this->_attributeId;
    }

}