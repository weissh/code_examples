<?php

namespace PrismProductsManager\Model;

use Zend\Db\Adapter\Adapter;
use PrismProductsManager\Model\ProductsEntity;
use PrismProductsManager\Model\ProductsMerchantCategoriesEntity;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Expression;

class TaxMapper
{
    protected $_taxesTable = 'TAXES';
    protected $_taxesDefinitionsTable = 'TAXES_Definitions';
    protected $_taxesTypesTable = 'TAXES_Types';
    protected $_taxesTypesDefinitionsTable = 'TAXES_Types_Definitions';
    protected $_productTaxesTable = 'PRODUCTS_R_TAXES';
    protected $_languages;

    public function __construct( Adapter $dbAdapter,  $serviceLocator, $languages)
    {
        $this->_dbAdapter = $dbAdapter;
        $this->_sql = new Sql($dbAdapter);
        $this->_hydrator = new ClassMethods(false);
        $this->_languages = $languages;
    }

    /**
     * This function gets the list of all taxes
     *
     * @param $languageId
     * @return mixed
     */
    public function getTaxLists($languageId = 1, $onlyActive = false)
    {
        $returnArray = array();
        $this->_sql->setTable(array('t' => $this->_taxesTable));
        $select = $this->_sql->select();
        $select->columns(array('taxId', 'taxTerritory', 'taxValue', 'taxTypeId' => 'taxType', 'status'));
        $select->join(
            array('td' => $this->_taxesDefinitionsTable),
            't.taxId = td.taxId',
            array('name', 'description', 'created')
        );
        $select->join(
            array('tt' => $this->_taxesTypesTable),
            'tt.taxTypeId = t.taxType',
            array('representAs' => 'type')
        );
        $select->join(
            array('tdt' => $this->_taxesTypesDefinitionsTable),
            'tt.taxTypeId = tdt.taxTypeId',
            array(
                'taxType' => 'name'
            )
        );
        $select->where(array(
            't.status' => $onlyActive ? 'ACTIVE' : array('ACTIVE','INACTIVE'),
            'td.languageId' => $languageId,
            'tdt.languageId' => $languageId
        ));
        $select->order(array('t.taxTerritory ASC'));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $key => $value) {
            $returnArray[] = $value;
        }

        return $returnArray;
    }

    /**
     * This function gets the list of all taxes
     *
     * @param $productId
     * @param $isVariant
     * @return mixed
     */
    public function getProductTaxTerritoriesUsed($productOrVariantId, $isVariant = 0, $languageId = 1)
    {
        $returnArray = array();
        $this->_sql->setTable(array('pt' => $this->_productTaxesTable));
        $select = $this->_sql->select();
        $select->columns(array('taxId'));
        $select->join(
            array('t' => $this->_taxesTable),
            'pt.taxId = t.taxId',
            array('taxTerritory')
        );
        $select->join(
            array('td' => $this->_taxesDefinitionsTable),
            't.taxId = td.taxId',
            array('name')
        );
        $select->where(array(
            'pt.productOrVariantId' => $productOrVariantId,
            'pt.isVariant' => $isVariant,
            'td.languageId' => $languageId,
        ));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $key => $value) {
            $returnArray[$value['taxTerritory']] = array(
                'taxId' => $value['taxId'],
                'name' => $value['name']
            );
        }

        return $returnArray;
    }

    public function getTaxTypes($languageId = 1)
    {
        $returnArray = array();
        $this->_sql->setTable(array('tt' => $this->_taxesTypesTable));
        $select = $this->_sql->select();
        $select->columns(array('taxTypeId'));
        $select->join(
            array('ttd' => $this->_taxesTypesDefinitionsTable), //Set join table and alias
            'tt.taxTypeId = ttd.taxTypeId',
            array('name')
        );
        $select->where(array(
            'tt.status' => 'ACTIVE',
            // 'ttd.languageId' => $languageId,
        ));
        $select->order(array('tt.taxTypeId ASC'));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            $returnArray[$value['taxTypeId']] = $value['name'];
        }
        return $returnArray;
    }

    public function saveTax($validatedData, $id = 0, $currentLanguage = 1)
    {
        if ($id == 0) {
            return $this->_addTax($validatedData);
        } else {
            return $this->_editTax($validatedData, $id, $currentLanguage);
        }
    }

    private function _addTax($validatedData)
    {
        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
        $this->_sql->setTable($this->_taxesTable);
        $action = $this->_sql->insert();
        $createdValue = date('Y-m-d H:i:s');
        $action->values(array(
            'taxType' => $validatedData['tax_type'],
            'taxTerritory' => strtoupper($validatedData['tax_country']),
            'taxValue' => $validatedData['tax_value'],
            'status' => 'ACTIVE',
            'created' => $createdValue
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
        $taxId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

        foreach ($this->_languages as $languageId) {
            $this->_sql->setTable($this->_taxesDefinitionsTable);
            $action = $this->_sql->insert();
            $action->values(array(
                'languageId' => $languageId,
                'taxId' => $taxId,
                'name' => $validatedData['name'],
                'description' => $validatedData['description'],
                'created' => $createdValue
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        }
        $this->_dbAdapter->getDriver()->getConnection()->commit();
        return true;
    }

    private function _editTax($validatedData, $id, $currentLanguage)
    {
        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
        $this->_sql->setTable($this->_taxesTable);
        $update = $this->_sql->update();
        $update->set(array(
            'taxType' => $validatedData['tax_type'],
            'taxTerritory' => strtoupper($validatedData['tax_country']),
            'taxValue' => $validatedData['tax_value'],
        ));
        $update->where(array(
            'taxId' => $id
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();

        $this->_sql->setTable($this->_taxesDefinitionsTable);
        $update = $this->_sql->update();
        $update->set(array(
            'name' => $validatedData['name'],
            'description' => $validatedData['description']
        ));
        $update->where(array(
            'taxId' => $id,
            'languageId' => $currentLanguage
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
        $this->_dbAdapter->getDriver()->getConnection()->commit();
        return true;
    }

    public function toggleTax($taxId, $deactivate = true)
    {
        if ($deactivate) {
            $status = 'INACTIVE';
        } else {
            $status = 'ACTIVE';
        }
        $this->_sql->setTable($this->_taxesTable);
        $update = $this->_sql->update();
        $update->set(array(
            'status' => $status
        ));
        $update->where(array(
            'taxId' => $taxId
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
        return true;
    }

    public function deleteTax($taxId)
    {
        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
        $this->_sql->setTable($this->_taxesTable);
        $action = $this->_sql->delete()
            ->where(array(
                'taxId' => $taxId
            ));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();

        $this->_sql->setTable($this->_taxesDefinitionsTable);
        $action = $this->_sql->delete()
            ->where(array(
                'taxId' => $taxId
            ));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
        $this->_dbAdapter->getDriver()->getConnection()->commit();
        return true;
    }

    public function addTaxForProduct($taxIds, $productOrVariantId, $isVariant = 0)
    {
        // delete the old setup for this product
        $this->_sql->setTable($this->_productTaxesTable);
        $action = $this->_sql->delete()
            ->where(array(
                'productOrVariantId' => $productOrVariantId,
                'isVariant' => $isVariant
            ));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();

        // set the new one
        $this->_sql->setTable($this->_productTaxesTable);
        foreach ($taxIds as $taxId) {
            $action = $this->_sql->insert();
            $action->values(array(
                'productOrVariantId' => $productOrVariantId,
                'isVariant' => $isVariant,
                'taxId' => $taxId
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        }
        return true;
    }

    public function getTaxForProduct($productOrVariantId, $isVariant = 0)
    {
        // delete the old setup for this product
        $this->_sql->setTable($this->_productTaxesTable);
        $action = $this->_sql->select()
            ->where(array(
                'productOrVariantId' => $productOrVariantId,
                'isVariant' => $isVariant
            ));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();

        $finalArray = array();
        foreach ($results as $result) {
            $finalArray[] = $result['taxId'];
        }
        return $finalArray;
    }

    public function getTaxValueForProduct($productOrVariantId, $isVariant = 0)
    {
        // delete the old setup for this product
        $this->_sql->setTable(array('pt' => $this->_productTaxesTable));
        $action = $this->_sql->select()
            ->join(
                array('t' => $this->_taxesTable),
                't.taxId = pt.taxId',
                array('taxTerritory', 'taxValue')
            )
            ->where(array(
                'pt.productOrVariantId' => $productOrVariantId,
                'pt.isVariant' => $isVariant
            ));
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();

        $finalArray = array();
        foreach ($results as $result) {
            $finalArray[$result['taxTerritory']] = $result['taxValue'];
        }
        return $finalArray;
    }
}
