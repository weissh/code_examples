<?php

namespace PrismProductsManager\Model;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class ProductsPricesEntity
{
    
    protected $_productPriceId;
    protected $_productIdFrom;
    protected $_productOrVariantId;
    protected $_regionId;
    protected $_currencyId;
    protected $_price;
    protected $_offerPrice;
    protected $_nowPrice;
    protected $_wasPrice;
    protected $_costPrice;
    protected $_tradingCostPrice;
    protected $_wholesalePrice;
    protected $_freightPrice;
    protected $_dutyPrice;
    protected $_designPrice;
    protected $_packagingPrice;

    /**
     * @param mixed $designPrice
     */
    public function setDesignPrice($designPrice)
    {
        $this->_designPrice = $designPrice;
    }

    /**
     * @return mixed
     */
    public function getDesignPrice()
    {
        return $this->_designPrice;
    }

    /**
     * @param mixed $dutyPrice
     */
    public function setDutyPrice($dutyPrice)
    {
        $this->_dutyPrice = $dutyPrice;
    }

    /**
     * @return mixed
     */
    public function getDutyPrice()
    {
        return $this->_dutyPrice;
    }

    /**
     * @return mixed
     */
    public function getOfferPrice()
    {
        return $this->_offerPrice;
    }

    /**
     * @param mixed $offerPrice
     */
    public function setOfferPrice($offerPrice)
    {
        $this->_offerPrice = $offerPrice;
    }
    /**
     * @param mixed $freightPrice
     */
    public function setFreightPrice($freightPrice)
    {
        $this->_freightPrice = $freightPrice;
    }

    /**
     * @return mixed
     */
    public function getFreightPrice()
    {
        return $this->_freightPrice;
    }

    /**
     * @param mixed $packagingPrice
     */
    public function setPackagingPrice($packagingPrice)
    {
        $this->_packagingPrice = $packagingPrice;
    }

    /**
     * @return mixed
     */
    public function getPackagingPrice()
    {
        return $this->_packagingPrice;
    }
    protected $_status;
    protected $_created;
    protected $_modified;
    

    public function __construct()
    {
        $currentDatetime =  date("Y-m-d H:i:s");
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
        $this->_status = 'ACTIVE';
        $this->_regionId = 0;
    }
    
    public function getProductPriceId()
    {
        return $this->_productPriceId;
    }

    public function getProductIdFrom()
    {
        return $this->_productIdFrom;
    }

    public function getProductOrVariantId()
    {
        return $this->_productOrVariantId;
    }

    public function getRegionId()
    {
        return $this->_regionId;
    }

    public function getCurrencyId()
    {
        return $this->_currencyId;
    }

    public function getPrice()
    {
        return $this->_price;
    }

    public function getNowPrice()
    {
        return $this->_nowPrice;
    }

    public function getWasPrice()
    {
        return $this->_wasPrice;
    }

    public function getCostPrice()
    {
        return $this->_costPrice;
    }

    public function getTradingCostPrice()
    {
        return $this->_tradingCostPrice;
    }

    public function getWholesalePrice()
    {
        return $this->_wholesalePrice;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }

        
    public function setProductPriceId($_productPriceId)
    {
        $this->_productPriceId = $_productPriceId;
    }

    public function setProductIdFrom($_productIdFrom)
    {
        $this->_productIdFrom = $_productIdFrom;
    }

    public function setProductOrVariantId($_productOrVariantId)
    {
        $this->_productOrVariantId = $_productOrVariantId;
    }

    public function setRegionId($_regionId)
    {
        $this->_regionId = $_regionId;
    }

    public function setCurrencyId($_currencyId)
    {
        $this->_currencyId = $_currencyId;
    }

    public function setPrice($_price)
    {
        $this->_price = $_price;
    }

    public function setNowPrice($_nowPrice)
    {
        $this->_nowPrice = $_nowPrice;
    }

    public function setWasPrice($_wasPrice)
    {
        $this->_wasPrice = $_wasPrice;
    }

    public function setCostPrice($_costPrice)
    {
        $this->_costPrice = $_costPrice;
    }

    public function setTradingCostPrice($_tradingCostPrice)
    {
        $this->_tradingCostPrice = $_tradingCostPrice;
    }

    public function setWholesalePrice($_wholesalePrice)
    {
        $this->_wholesalePrice = $_wholesalePrice;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }


    
}
