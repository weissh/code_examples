<?php

namespace PrismProductsManager\Model;

use PrismMediaManager\Model\ImageMapper;
use Zend\Db\Adapter\Adapter;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

/**
 * Class ApiCategoriesMapper
 * @package PrismProductsManager\Model
 */
class ApiCategoriesMapper
{
    protected $_dbAdapter;
    protected $_sql;


    protected $_categoriesDefinitionsTable = 'CATEGORIES_Definitions';

    protected $_categoriesTable = 'CATEGORIES';

    protected $_categoriesTagsTable = 'CATEGORIES_Tags';

    protected $_categoryMedia = 'CATEGORIES_Media';

    protected $_categoriesRClientsWebsites = 'CATEGORIES_R_CLIENTS_Websites';

    protected $imageMapper;

    public function __construct(Adapter $dbAdapter,ImageMapper $imageMapper)
    {
        $this->_dbAdapter = $dbAdapter;
        $this->_sql = new Sql($this->_dbAdapter);
        $this->imageMapper = $imageMapper;
    }

    /**
     * Note: Get a specific category based on the id of the category
     * @param $categoryId
     * @param $websiteId
     * @param $languageId
     * @return array
     */
    public function getCategoryById($categoryId, $websiteId, $languageId)
    {
        $this->_sql->setTable($this->_categoriesTable);
        $action = $this->_sql->select()
            ->join(
                array('cd' => $this->_categoriesDefinitionsTable),
                $this->_categoriesTable . '.categoryId = cd.categoryId',
                array('*'),
                Select::JOIN_LEFT
            )
            ->join(
                array('crcw' => $this->_categoriesRClientsWebsites),
                $this->_categoriesTable . '.categoryId = crcw.categoryId',
                array('*'),
                Select::JOIN_LEFT
                )
            ->where('cd.languageId = ' . $languageId)
            ->where('crcw.websiteId = ' . $websiteId)
            ->where($this->_categoriesTable . '.categoryId = ' . $categoryId );

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        try {
            $result = $statement->execute();
        } catch (\Exception $ex) {
            echo '<pre>';
            print_r($ex);
        }

        $categories = array();
        foreach ($result as $res) {
            // Get all tags for the category
            $this->_sql->setTable($this->_categoriesTagsTable);
            $action = $this->_sql->select()
                ->where($this->_categoriesTagsTable . '.categoriesDefinitionsId =' . $res['categoryDefinitionsId']);

            $statement = $this->_sql->prepareStatementForSqlObject($action);

            try {
                $result = $statement->execute();
            } catch (\Exception $e) {
                echo '<pre>';
                print_r($e);
            }

            foreach ($result as $tag) {
                $res['tags'][] = $tag;
            }

            $categories[] = $res;
        }

        return empty($categories) ? array() : $categories;
    }

    /**
     * Get a specific category by the name
     * @param $urlName
     * @param $websiteId
     * @param $languageId
     * @return array
     */
    public function getCategoryByName($urlName, $websiteId, $languageId)
    {
        // Get the category id from the category name
        $this->_sql->setTable(array('cdt' => $this->_categoriesDefinitionsTable));
        $action = $this->_sql->select();
        $action->join(
            array('cm' => $this->_categoryMedia), //Set join table and alias
            new Expression('cm.categoryId = cdt.categoryId'),
            array('imageId', 'mediaPriority'),
            Select::JOIN_LEFT
        );
        $action->where('cdt.urlName = "' . $urlName . '"');
        $action->where('cdt.status = "ACTIVE"');
        $action->where('cdt.languageId = "' . $languageId . '"');
        $statement = $this->_sql->prepareStatementForSqlObject($action);

        $result = $statement->execute()->next();

        // Get image URL if image id is present
        if ( isset($result['imageId']) && !empty($result['imageId']) ) {

            $imgurl = $this->imageMapper->getImageById($result['imageId'],'hpr');

            if ($imgurl) {
                $result['categoryBanner'] = $imgurl->preview;
            }
        }

        return $result;
    }

}
