<?php
namespace PrismProductsManager\Model;


/**
 * Class AttributesGroupEntity
 * @package PrismProductsManager\Model
 */
class AttributesGroupEntity
{

    /**
     * @var
     */
    protected $_attributeGroupId;

    /**
     * @var
     */
    protected $_groupParentId;

    /**
     * @var string
     */
    protected $_groupCode = 'MKT';

    /**
     * @var string
     */
    protected $_sourceTable = 'ATTRIBUTES';

    /**
     * @var int
     */
    protected $_groupType = 'Marketing';

    /**
     * @var
     */
    protected $_viewAs;

    /**
     *
     */
    public function __construct()
    {

    }

    /**
     * @param null $attributeGroupId
     */
    public function setAttributeGroupId($attributeGroupId)
    {
        $this->_attributeGroupId = $attributeGroupId;
    }

    /**
     * @return null
     */
    public function getAttributeGroupId()
    {
        return $this->_attributeGroupId;
    }

    /**
     * @param mixed $groupCode
     */
    public function setGroupCode($groupCode)
    {
        $this->_groupCode = $groupCode;
    }

    /**
     * @return mixed
     */
    public function getGroupCode()
    {
        return $this->_groupCode;
    }

    /**
     * @param mixed $groupParentId
     */
    public function setGroupParentId($groupParentId)
    {
        $this->_groupParentId = $groupParentId;
    }

    /**
     * @return mixed
     */
    public function getGroupParentId()
    {
        return $this->_groupParentId;
    }

    /**
     * @param int $groupType
     */
    public function setGroupType($groupType)
    {
        $this->_groupType = $groupType;
    }

    /**
     * @return int
     */
    public function getGroupType()
    {
        return $this->_groupType;
    }

    /**
     * @param string $sourceTable
     */
    public function setSourceTable($sourceTable)
    {
        $this->_sourceTable = $sourceTable;
    }

    /**
     * @return string
     */
    public function getSourceTable()
    {
        return $this->_sourceTable;
    }

    /**
     * @param mixed $viewAs
     */
    public function setViewAs($viewAs)
    {
        $this->_viewAs = $viewAs;
    }

    /**
     * @return mixed
     */
    public function getViewAs()
    {
        return $this->_viewAs;
    }

}