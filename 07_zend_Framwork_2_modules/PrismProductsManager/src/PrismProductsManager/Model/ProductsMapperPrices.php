<?php

namespace PrismProductsManager\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
//use Zend\Db\ResultSet\HydratingResultSet;
use PrismProductsManager\Model\ProductsMapper;

/**
 * This class is a sub class of ProductsMapper class. 
 * It was created to reduce the length of the parent calss.
 * 
 * It holds business logic for managing product prices.
 */
class ProductsMapperPrices extends ProductsMapper
{

    /**
     * @var Sql
     */
    protected $_sql;
    /**
     * @var
     */
    protected $_hydrator;
    /**
     * @var Adapter
     */
    protected $_dbAdapter;

    /**
     * @param Sql $sql
     * @param $hydrator
     * @param Adapter $dbAdapter
     */
    public function __construct(Sql $sql, $hydrator, Adapter $dbAdapter)
    {
        $this->_sql = $sql;
        $this->_hydrator = $hydrator;
        $this->_dbAdapter = $dbAdapter;
    }

    /**
     * Check if product prices has been setup for this currency.
     * If yes - return the different prices in an array
     * Else - return false
     * 
     * @param $productOrVariantId
     * @param $currencyId
     * @param $productIdFrom
     * @return mixed
     */
    protected function _checkIfProductPricesExists($productOrVariantId, $currencyId, $productIdFrom = 'PRODUCTS')
    {
        $priceValues = false;

        if ($productOrVariantId > 0) {
            $this->_sql->setTable(array('t' => $this->_productsPricesTable));
            $select = $this->_sql->select();
            $select->columns(array(
                'price',
                'offerPrice',
                'nowPrice',
                'wasPrice',
                'costPrice',
                'tradingCostPrice',
                'wholesalePrice',
                'freightPrice',
                'dutyPrice',
                'designPrice',
                'packagingPrice',
            ));
            $select->where(array(
                't.productOrVariantId' => $productOrVariantId,
                'currencyId' => $currencyId,
                'productIdFrom' => $productIdFrom,
                'status' => 'ACTIVE'
            ));
            $select->limit(1);
            
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            foreach ($results as $key => $value) {
                if (isset($value)) {
                    $priceValues = $value;
                }
            }
        }
        return $priceValues;
    }
    
    /**
     * Deactivate product prices
     * 
     * @param $productOrVariantId
     * @param $currencyId
     * @param $productIdFrom
     * @return type
     */
    public function _deactivateProductPrices($productOrVariantId, $currencyId, $productIdFrom = 'PRODUCTS')
    {
        $whereClause = array(
            'productOrVariantId' => $productOrVariantId,
            'currencyId' => $currencyId,
            'productIdFrom' => $productIdFrom,
            'status' => 'ACTIVE'
        );
        $fieldsToUpdate = array('status' => 'INACTIVE');
        $table = $this->_productsPricesTable;
        $productPriceDeactivated = $this->_update($fieldsToUpdate, $table, $whereClause);
        return $productPriceDeactivated;
    }

    /**
     * @param $savedVariantId
     * @param $currencyId
     * @param $pricesArray
     * @return bool
     */
    public function insertNewVariantPrices($savedVariantId, $currencyId, $pricesArray)
    {

        $insertPricesArray = array(
            'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
            'productOrVariantId' => $savedVariantId,
            'currencyId' => $currencyId
        );
        foreach ($pricesArray as $column => $value) {
            $insertPricesArray[$column] = $value;
        }
        $this->_sql->setTable($this->_productsPricesTable);
        $action = $this->_sql->insert();
        $action->values($insertPricesArray);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        return $result->getAffectedRows();
    }



    /**
     * @param $data
     */
    public function updateProductPricesByPriceID($argProductPriceId = 0, $data = array())
    {
        $this->_sql->setTable($this->_productsPricesTable);
        $action = $this->_sql->update();
        $action->set($data);
        $action->where("productPriceId = '$argProductPriceId'");
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    public function deactivatePricesByProductId($producIdOrVariantId){
        $whereClause = array(
            'productOrVariantId' => $producIdOrVariantId,
            'status' => 'ACTIVE'
        );
        $fieldsToUpdate = array('status' => 'INACTIVE');
        $table = $this->_productsPricesTable;
        $productPriceDeactivated = $this->_update($fieldsToUpdate, $table, $whereClause);
        return true;
    }
}
