<?php
namespace PrismProductsManager\Model;


/**
 * Class AttributesGroupDefinitionsEntity
 * @package PrismProductsManager\Model
 */
class AttributesGroupDefinitionsEntity
{

    /**
     * @var null
     */
    protected $_attributeGroupDefinitionId = null;

    /**
     * @var null
     */
    protected $_attributeGroupId = null;

    /**
     * @var
     */
    protected $_languageId;

    /**
     * @var null
     */
    protected $_name = null;

    /**
     * @var null
     */
    protected $_publicName = null;

    /**
     *
     */
    public function __construct()
    {

    }

    /**
     *
     * @return the $_attributeGroupDefinitionId
     */
    public function getAttributeGroupDefinitionId()
    {
        return $this->_attributeGroupDefinitionId;
    }

    /**
     *
     * @return the $_attributeGroupId
     */
    public function getAttributeGroupId()
    {
        return $this->_attributeGroupId;
    }

    /**
     *
     * @return the $_languageId
     */
    public function getLanguageId()
    {
        return $this->_languageId;
    }

    /**
     *
     * @return the $_name
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     *
     * @return the $_publicName
     */
    public function getPublicName()
    {
        return $this->_publicName;
    }


    /**
     *
     * @param $attributeGroupDefinitionId
     */
    public function setAttributeGroupDefinitionId($attributeGroupDefinitionId)
    {
        $this->_attributeGroupDefinitionId = $attributeGroupDefinitionId;
    }

    /**
     *
     * @param $attributeGroupId
     */
    public function setAttributeGroupId($attributeGroupId)
    {
        $this->_attributeGroupId = $attributeGroupId;
    }

    /**
     *
     * @param languageId
     */
    public function setLanguageId($languageId)
    {
        $this->_languageId = $languageId;
    }

    /**
     *
     * @param name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     *
     * @param publicName
     */
    public function setPublicName($publicName)
    {
        $this->_publicName = $publicName;
    }

}