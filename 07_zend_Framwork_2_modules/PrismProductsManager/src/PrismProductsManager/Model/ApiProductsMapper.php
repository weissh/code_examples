<?php

namespace PrismProductsManager\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Expression;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;

/**
 * Class ApiProductsMapper
 * @package PrismProductsManager\Model
 */
class ApiProductsMapper
{
    /**
     * @var Adapter
     */
    protected $_dbAdapter;
    /**
     * @var Sql
     */
    protected $_sql;

    /**
     * @var string
     */
    protected $_productsTable = 'PRODUCTS';
    /**
     * @var string
     */
    protected $_productsDefinitionsTable = 'PRODUCTS_Definitions';
    /**
     * @var string
     */
    protected $_productsRCategoriesTable = 'PRODUCTS_R_CATEGORIES';
    /**
     * @var string
     */
    protected $_productsRSpectrumTable = 'PRODUCTS_R_SPECTRUM';
    /**
     * @var string
     */
    protected $_categoriesDefinitionsTable = 'CATEGORIES_Definitions';

    /**
     * @var string
     */
    protected $_categoriesRClientsWebsites = 'CATEGORIES_R_CLIENTS_Websites';

    /**
     * @var string
     */
    protected $_productsTypesTable = 'PRODUCTS_Types';

    /**
     * @var string
     */
    protected $_productsTypesDefinitionsTable = 'PRODUCTS_Types_Definitions';

    /**
     *
     */
    protected $_productsRClientsWebsitesTable = 'PRODUCTS_R_CLIENTS_Websites';
    /**
     * @var ProductsMapper
     */
    protected $_productsMapper;

    /**
     * @param Adapter $dbAdapter
     */
    public function __construct(Adapter $dbAdapter, ProductsMapper $productsMapper)
    {
        $this->_dbAdapter = $dbAdapter;
        $this->_productsMapper = $productsMapper;
        $this->_sql = new Sql($this->_dbAdapter);
    }

    /**
     * Note: Get all the products that are assigned to a category by id
     * @param $categoryId
     * @param $websiteId
     * @param $languageId
     * @param null $limit
     * @param null $page
     * @return array
     */
    public function getProductsByCategoryId($categoryId, $websiteId, $languageId, $limit = null, $page = null)
    {
        if ($limit !== null && $page !== null) {
            $offsetQuery = ($page-1) * $limit;
            $limitQuery = $page * $limit;
        }

        $this->_sql->setTable($this->_productsTable);
        $action = $this->_sql->select()
            ->join(
                array('prc' => $this->_productsRCategoriesTable),
                'prc.productId = ' . $this->_productsTable . '.productId',
                array('isUsedAsVariant' => 'productId'),
                Select::JOIN_LEFT
            )
            ->join(
                array('cd' => $this->_categoriesDefinitionsTable),
                'cd.categoryId = prc.categoryId',
                array('*'),
                Select::JOIN_LEFT
            )
            ->join(
                array('pd' => $this->_productsDefinitionsTable),
                new Expression($this->_productsTable . '.productId = pd.productId AND pd.productIdFrom = "'.$this->_productsTable.'" AND pd.status = "ACTIVE" AND pd.languageId = '. $languageId),
                array('*'),
                Select::JOIN_LEFT
            )
            ->join(
                array('crcw' => $this->_categoriesRClientsWebsites),
                'cd.categoryId = prc.categoryId',
                array('*'),
                Select::JOIN_LEFT
            )
            ->where('prc.categoryId = ' . $categoryId)
            ->where('crcw.categoryId = ' . $categoryId)
            ->where($this->_productsTable . '.status = "ACTIVE"')
            ->where('crcw.websiteId = ' . $websiteId)
            ->where('cd.languageId = ' . $languageId)
            ->where('prc.status = "ACTIVE"');

        if ($limit !== null && $page !== null) {
            $action->limit($limitQuery);
            $action->offset($offsetQuery);
        }

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        $products = array();
        foreach ($result as $res) {
            $products[] = $res;
        }

        return empty($products) ? array() : $products;
    }

    /**
     * Get all the products that are assigned to a category by name
     * @param $urlName
     * @param $websiteId
     * @param $languageId
     * @return array
     */
    public function getProductsByCategoryName($urlName, $websiteId, $languageId, $limit, $page)
    {
        // Get the category id from the category name
        $this->_sql->setTable($this->_categoriesDefinitionsTable);
        $action = $this->_sql->select()
            ->where($this->_categoriesDefinitionsTable . '.urlName = "' . $urlName . '"');

        $statement = $this->_sql->prepareStatementForSqlObject($action);

        $result = $statement->execute()->next();
        $categoryId = (int)$result['categoryId'];
        // get the products based on the category id
        $products = $this->getProductsByCategoryId($categoryId, $websiteId, $languageId, $limit, $page);

        return $products;
    }

    /**
     * Get the voucher product from the database
     * @param $websiteId
     * @param $languageId
     */
    public function getVoucherProduct($websiteId, $languageId)
    {
        $productTypeId = $this->_productsMapper->getVoucherTypeId($languageId);

        // Get the evoucher product based on
        $this->_sql->setTable($this->_productsTable);
        $select = $this->_sql->select()
            ->join(
                array('pd' => $this->_productsDefinitionsTable),
                new Expression($this->_productsTable . '.productId = pd.productId AND pd.productIdFrom = "PRODUCTS"'),
                array('*'),
                Select::JOIN_LEFT
            )
            ->join(
                array('prcw' => $this->_productsRClientsWebsitesTable),
                new Expression(
                    $this->_productsTable . '.productId = prcw.productId AND ' .
                    'prcw.productIdFrom = "PRODUCTS" AND ' .
                    'prcw.websiteId = ' . $websiteId),
                array(),
                Select::JOIN_LEFT
            )
            ->join(
                array('prs' => $this->_productsRSpectrumTable),
                new Expression(
                    'prs.productId = ' . $this->_productsTable . '.productId AND ' .
                    'prs.productIdFrom = "PRODUCTS"'
                ),
                array('spectrumId'),
                Select::JOIN_LEFT
            )
            ->where($this->_productsTable . '.status = "ACTIVE"')
            ->where($this->_productsTable . '.productTypeId = ' . $productTypeId['productTypeId']);

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results->next();
    }

    public function getVoucherProductBySpectrumId($spectrumId)
    {
        // Get the evoucher product based on
        $this->_sql->setTable($this->_productsTable);
        $select = $this->_sql->select()
            ->join(
                array('pd' => $this->_productsDefinitionsTable),
                new Expression($this->_productsTable . '.productId = pd.productId AND pd.productIdFrom = "PRODUCTS"'),
                array('*'),
                Select::JOIN_LEFT
            )
            ->join(
                array('prs' => $this->_productsRSpectrumTable),
                new Expression(
                    'prs.productId = ' . $this->_productsTable . '.productId AND ' .
                    'prs.productIdFrom = "PRODUCTS"'
                ),
                array('spectrumId'),
                Select::JOIN_LEFT
            )
            ->where($this->_productsTable . '.status = "ACTIVE"')
            ->where('prs.spectrumId = ' . $spectrumId);

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results->next();
    }
}
