<?php

namespace PrismProductsManager\Model;

use Zend\Db\TableGateway\TableGateway;

class CategoryDefinitionsTable {

    protected $tableGateway;
    public $table = 'CATEGORY_Definitions';

    public function setTableGateway(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function getTableGateway() {
        return $this->tableGateway;
    }

}
