<?php

namespace PrismProductsManager\Model;

use PrismProductsManager\Model\CommonAttributes\CommonAttributesTable;
use PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesDefinitionsTable;
use PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable;
use Zend\Db\Adapter\Adapter;
use PrismProductsManager\Model\ProductsMerchantCategoriesEntity;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Expression as Expression;

use PrismProductsManager\Model\AttributesGroupMapper;

/**
 * Class MerchantCategoriesMapper
 * @package PrismProductsManager\Model
 */
class MerchantCategoriesMapper
{

    /**
     * @var string
     */
    protected $_merchantCategoriesTable = 'PRODUCTS_Merchant_Categories';
    /**
     * @var string
     */
    protected $_merchantCategoriesDefinitionsTable = 'PRODUCTS_Merchant_Categories_Definitions';
    /**
     * @var string
     */
    protected $_skuRulesTable = 'SKU_Rules';
    /**
     * @var string
     */
    protected $commonAttributesDefinitions = 'COMMON_ATTRIBUTES_Definitions';
    /**
     * @var string
     */
    protected $commonAttributesGUIRepresentation = 'COMMON_ATTRIBUTES_GUI_Representation';
    /**
     * @var string
     */
    protected $_attributeGroupsTable = 'ATTRIBUTES_Groups';
    /**
     * @var string
     */
    protected $_attributeGroupDefinitionsTable = 'ATTRIBUTES_Group_Definitions';
    /**
     * @var string
     */
    protected $_attributesTable = 'ATTRIBUTES';
    /**
     * @var string
     */
    protected $_attributeDefinitionsTable = 'ATTRIBUTES_Definitions';
    /**
     * @var Adapter
     */
    protected $_dbAdapter;
    /**
     * @var Sql
     */
    protected $_sql;
    /**
     * @var ClassMethods
     */
    protected $_hydrator;
    /**
     * @var array
     */
    protected $_siteConfig;
    /**
     * @var
     */
    protected $_productsDefinitionsEntity;
    /**
     * @var
     */
    protected $_languages;
    /**
     * @var
     */
    protected $_defaultLanguage;
    /**
     * @var
     */
    protected $_merchantCategoriesForm;
    /**
     * @var MerchantCategoriesDefinitionsEntity
     */
    protected $_merchantCategoriesDefinitionsEntity;
    /**
     * @var MerchantCategoriesEntity
     */
    protected $_merchantCategoriesEntity;
    /**
     * @var SkuRulesEntity
     */
    protected $_skuRulesEntity;
    /**
     * @var AttributesGroupMapper
     */
    protected $_attributesGroupMapper;
    /**
     * @var
     */
    protected $_defaultOneSizeCode;
    /**
     * @var CommonAttributesTable
     */
    protected $commonAttributesTable;

    /**
     * @param Adapter $dbAdapter
     * @param array $siteConfig
     * @param AttributesGroupMapper $attributesGroupMapper
     * @param MerchantCategoriesDefinitionsEntity $merchantCategoriesDefinitionsEntity
     * @param MerchantCategoriesEntity $merchantCategoriesEntity
     * @param SkuRulesEntity $skuRulesEntity
     * @param CommonAttributesTable $commonAttributesTable
     */
    public function __construct(
        Adapter $dbAdapter,
        $siteConfig = array(),
        AttributesGroupMapper $attributesGroupMapper,
        MerchantCategoriesDefinitionsEntity $merchantCategoriesDefinitionsEntity,
        MerchantCategoriesEntity $merchantCategoriesEntity,
        SkuRulesEntity $skuRulesEntity,
        CommonAttributesTable $commonAttributesTable,
        CommonAttributesValuesTable $commonAttributesValuesTable,
        CommonAttributesValuesDefinitionsTable $commonAttributesValuesDefinitionsTable)
    {
        $this->_merchantCategoriesDefinitionsEntity = $merchantCategoriesDefinitionsEntity;
        $this->_merchantCategoriesEntity = $merchantCategoriesEntity;
        $this->_siteConfig = $siteConfig;
        $this->_languages = $siteConfig['languages']['site-languages'];
        $this->_defaultLanguage = $siteConfig['languages']['current-language'];
        $this->_dbAdapter = $dbAdapter;
        $this->_sql = new Sql($dbAdapter);
        $this->_sql->setTable($this->_merchantCategoriesTable);

        $this->commonAttributesTable = $commonAttributesTable;
        $this->commonAttributesValuesTable = $commonAttributesValuesTable;
        $this->commonAttributesValuesDefinitionsTable = $commonAttributesValuesDefinitionsTable;

        $this->_skuRulesEntity = $skuRulesEntity;
        $this->_hydrator = new ClassMethods(false);

        $this->_attributesGroupMapper = $attributesGroupMapper;
        $this->_defaultOneSizeCode = $siteConfig['defaultOneSizeCode'];
    }

    /**
     * Retrieve merchant categories.
     *
     * @param int|\PrismProductsManager\Model\type $parentId
     * @param int $languageId
     * @return type
     */
    public function fetchMerchantCategories($parentId = 0, $languageId = 1)
    {
        $returnArray = array();

        $this->_sql->setTable($this->_merchantCategoriesTable);
        $select = $this->_sql->select();
        $select->columns(array("productMerchantCategoryId"));
        $select->where(array(
                'parentId' => $parentId,
                'status' => 'ACTIVE',
                'mcd.languageId' => $languageId,
        ));
        $select->join(array(
            'mcd' => $this->_merchantCategoriesDefinitionsTable), //Set join table and alias
            "{$this->_merchantCategoriesTable}.productMerchantCategoryId = mcd.productMerchantCategoryId"
            , array("categoryName"
        ));
        $select->order(array("mcd.categoryName ASC"));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $returnArray['data'][0] = 'Choose category';
        foreach ($results as $key => $value) {
            $returnArray['data'][$value['productMerchantCategoryId']] = ucwords(strtolower(html_entity_decode($value['categoryName'], ENT_QUOTES)));
        }
        $returnArray['dataCount'] = $results->count();

        $childrenCount = $this->categoryChildrenCount($parentId);
        $returnArray['childrenCount'] = $childrenCount;

        return $returnArray;
    }

    /**
     * Check if children exist so we know if we need to display another dropdown
     *
     * @param type $parentId
     * @return type
     */
    public function categoryChildrenCount($parentId)
    {
        $this->_sql->setTable($this->_merchantCategoriesTable);
        $select = $this->_sql->select();
        $select->where(array('parentId' => $parentId, 'status' => 'ACTIVE'));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results->count();
    }

    /**
     *  Fetch all merchant categories
     * @param string $type
     * @param null $search
     * @internal param $type
     * @returns array
     */
    public function fetchAllMerchantCategories($type = '', $search = null, $orderBy, $orderbyOption)
    {
        if($orderBy === null){
            $orderBy = 'mcd.modified';
        } elseif ($orderBy != 'status') {
            $orderBy = 'mcd.' . $orderBy;
        }

        if($orderbyOption === null){
            $orderbyOption = 'desc';
        }

        $action = $this->_sql->select()
            ->join(
                array('mcd' => $this->_merchantCategoriesDefinitionsTable),
                $this->_merchantCategoriesTable.'.productMerchantCategoryId = mcd.productMerchantCategoryId'
            )
            ->where('mcd.languageId = '. $this->_defaultLanguage)
            ->where($this->_merchantCategoriesTable.'.status != "DELETED"');
        // Search
        if ($search !== null) {
            $action->where
                ->nest
                ->like('mcd.categoryName', '%' . $search . '%')
                ->or
                ->like('mcd.shortDescription', '%' . $search . '%')
                ->or
                ->like('mcd.fullDescription', '%' . $search . '%')
                ->unnest;
        }
        $action->order($orderBy ." " .  $orderbyOption);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        try {
            $result = $statement->execute();
        } catch (Exception $ex) {
            echo '<pre>';
            print_r($ex);
        }
        $categories = array();
        foreach ($result as $res) {


            // Get the Sku building rules for each category
            $this->_sql->setTable($this->_skuRulesTable);
            $action = $this->_sql->select()
                ->where($this->_skuRulesTable . '.merchantCategoryId = ' . $res['productMerchantCategoryId']);
            $statement = $this->_sql->prepareStatementForSqlObject($action);

            try {
                $skus = $statement->execute();
            } catch (\Exception $ex) {
                echo '<pre>';
                print_r($ex);
            }
            $res['sku'] = null;
            foreach ($skus as $sku) {
                $res['sku'][] = $sku;
            }

            if ($type == 'list') {
                $categories[$res['productMerchantCategoryId']] = $res['categoryName'];
            } else {
                $categories[] = $res;
            }
        }
        return $categories;
    }

    /**
     *  Used to parse all categories and return the data need to build the hierarchy
     * @param $allCategories
     * @param array $search
     * @return array
     */
    public function processGroupCategories($allCategories, $search = array())
    {

        $groups = array();
        foreach ($allCategories as $group) {
            $groups[$group['productMerchantCategoryId']] = $group;
            if (!empty($search)) {
                $groups[$group['productMerchantCategoryId']]['children'] = null;
            }
        }

        if (!empty($groups) && empty($search)) {
            $finalArray = $this->_processGroupChildren($groups, 0);
        } else {
            $finalArray = $groups;
        }

        return $finalArray;
    }

    /**
     * @param $groups
     * @param int $parentKey
     * @return array|null
     */
    protected function _processGroupChildren($groups, $parentKey = 0)
    {
        $return = array();
        // Traverse the tree and search for direct children of the root

        foreach($groups as $categoryGroupId => $group) {
            // A direct child is found
            if($group['parentId'] == $parentKey) {
                // Remove item from tree (we don't need to traverse this again)
                $newArrayElement = $group;
                unset($groups[$categoryGroupId]);
                // Append the child into result array and parse its children
                $newArrayElement['children'] = $this->_processGroupChildren($groups, $newArrayElement['productMerchantCategoryId']);
                $return[] = $newArrayElement;
            }
        }
        return empty($return) ? null : $return;
    }

    /**
     * Method used to enable Marketing categories
     * @param $params
     */
    public function enableCategory($params)
    {
        if ($params['checked'] == '0') {
            $status = 'Inactive';
        } else {
            $status = 'Active';
        }
        $data = array(
            'status' => $status
        );
        $update = $this->_sql->update();
        $update->table($this->_merchantCategoriesTable);
        $update->set($data);
        $update->where(array(
            'productMerchantCategoryId' => $params['id'],
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        try {
            $statement->execute();
        } catch (\Exception $e) {
            die('Error: ' . $e->getMessage());
        }

        if ($params['checked'] == '1') {
            // Update the set filter flag for all parent of the group
            $this->_updateFilterFlagForParent($params['id'], $data);
        }

        // Update the set filter flag for all children of the group
        $this->_updateFilterFlagForChildren($params['id'], $data);

    }

    /**
     * @param $attributeId
     * @param $tableName
     * @param int $languageId
     * @param string $territory
     * @return array
     */
    public function getAttributeValue($attributeId, $tableName, $languageId =1, $territory = 'uk')
    {
        $returnValue = array();

        $this->_sql->setTable(array('t' => $tableName));
        $select = $this->_sql->select();
        $select->columns(array('skuRuleId', 'skuRuleTable'));
        $select->where(array('t.productAttributeId' => $attributeId));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $skuRuleDetails = array();
        foreach ($results as $key => $value) {
            $tableName = $value['skuRuleTable'];
            $skuRuleId = $value['skuRuleId'];
            switch ($tableName) {
                case 'COLOURS_Groups' :
                    $skuRuleDetails = $this->getColourNameByColourId($tableName, $skuRuleId, $languageId);
                    break;
                case 'SIZES_Shoes' :
                case 'SIZES_Accessories' :
                case 'SIZES_Clothes' :
                case 'SIZES_Belts' :
                case 'SIZES_Shirts' :
                case 'SIZES_Jackets' :
                case 'SIZES_Trousers' :
                case 'SIZES_SwimShorts' :
                    $skuRuleDetails = $this->getSizeNameBySizeId($tableName, $skuRuleId);
                    break;
                case 'COMMON_ATTRIBUTES_VALUES':
                    $commonAttributeValuesEntry = $this->commonAttributesValuesTable->getTableGateway()
                        ->select('commonAttributeValueId = ' . $skuRuleId)->toArray();
                    $commonAttributeValuesDefinitionsEntry = $this->commonAttributesValuesDefinitionsTable->getTableGateway()
                        ->select('commonAttributeValueId = ' . $skuRuleId . ' AND languageId = ' . $this->_defaultLanguage)->toArray();
                    $skuRuleDetails = array($commonAttributeValuesEntry[0]['commonAttributeValueSkuCode'] => $commonAttributeValuesDefinitionsEntry[0]['value']);
                    if (isset($returnValue[$tableName])) {
                        $tableName = $tableName . '_';
                    }
                    break;
                default :
                    break;
            }
            $returnValue[$tableName] = $skuRuleDetails;
        }
        return $returnValue;
    }

    /**
     *
     * @param type $merchantCategoryId
     * @param type $languageId
     * @return type
     */
    public function fetchSkuBuildingAttribute($merchantCategoryId, $languageId)
    {

        //Let's get the rules for building the sku
        $skuRules = $this->getSkuRules($merchantCategoryId);

        $attributeGroups = array();
        if (count($skuRules) > 0) {
            $position = 0;
            foreach ($skuRules as $key => $rulesdetails) {
                if (isset($rulesdetails['merchantAttributeGroupId']) && $rulesdetails['merchantAttributeGroupId'] > 0) {
                    /**
                     * Set up new common attributes to work with new the product page screens
                     * Retrieve all the children common attributes for $rulesdetails['merchantAttributeGroupId'] including the normal one
                     * And process them into a list of key => value
                     */

                    if (!empty($rulesdetails['isCommonAttribute'])) {
                        $commonAttributesWithDetails = $this->commonAttributesTable->getCommonAttributesDetailsBy(
                            array(
                                $this->commonAttributesDefinitions . '.languageId = ' . $this->_defaultLanguage,
                                $this->commonAttributesGUIRepresentation . '.commonAttributeId = ' . $rulesdetails['merchantAttributeGroupId']
                            )
                        );


                        $listCommonAttributes = $this->commonAttributesTable->listResultsWithKeyAs($commonAttributesWithDetails, '_common', 'commonAttributeId', 'description');

                        $attributeGroups[$skuRules[$key]['description']] = $listCommonAttributes;
                    } else {
                        $attributeGroups[$skuRules[$key]['name']] = $this->getAttributeGroupDefinitionsById($rulesdetails['merchantAttributeGroupId'], $languageId);
                    }
                    $position++;
                }
            }
        }

        return $attributeGroups;
    }

    /**
     * @param $attributeParentId
     * @param $languageId
     * @return array
     */
    protected function getAttributeGroupDefinitionsById($attributeParentId, $languageId)
    {
        $returnArray = array();
        $this->_sql->setTable(array('ag' => $this->_attributeGroupsTable));
        $select = $this->_sql->select();
        $select->columns(array('attributeGroupId'));
        $select->join(
            array(
                'agd' => $this->_attributeGroupDefinitionsTable), //Set join table and alias
                "ag.attributeGroupId = agd.attributeGroupId",
            array("name")
        );
        $select->where(
            array(
                'ag.groupParentId' => $attributeParentId,
                'agd.languageId' => $languageId,
            )
        );
        $select->order(array("agd.name ASC"));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!empty($results)) {
            //$returnArray[0] = 'Select Group';
            foreach ($results as $key => $value) {
                $returnArray[$value['attributeGroupId']] = $value['name'];
            }
        }
        return $returnArray;
    }

    /**
     * @param $merchantCategoryId
     * @return array
     */
    protected function getSkuRules($merchantCategoryId)
    {
        $returnArray = array();
        $this->_sql->setTable(array('sr' => $this->_skuRulesTable));
        $select = $this->_sql->select();
        //$select->columns(array('attributeGroupId'));
        $select->join(
                array('agd' => $this->_attributeGroupDefinitionsTable), //Set join table and alias
                "sr.merchantAttributeGroupId = agd.attributeGroupId",
                array("name"),
                Select::JOIN_LEFT
            );
        $select->join(
                array('cad' => $this->commonAttributesDefinitions), //Set join table and alias
                new Expression("sr.merchantAttributeGroupId = cad.commonAttributeId AND cad.languageId = " . $this->_defaultLanguage),
                array("description"),
                Select::JOIN_LEFT
            );
        $select->where(array('merchantCategoryId' => $merchantCategoryId));
        $select->order(array("sr.position ASC"));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $key => $value) {
            $returnArray[] = $value;
        }

        return $returnArray;
    }

    /**
     * Retrieve values for selected Merchant Attribute.
     * Currently used in varian creation.
     *
     * Note: It follows the steps below
     * 1. Retrieves details about an attribute group using the attribute group id
     *    This will
     *      a. Return the tables where we can search for all attribute id's in the group
     *      b. Return the sourceTable of the parent Merchant group
     * 2. Retrieve all merchant attributes in the same Merchant group
     * 3. For each of the id's retrieved in step 2, get the values.
     *
     * @param $selectedAttributeId
     * @param $languageId
     * @return mixed
     */
    public function fetchMerchantAttributeValues($selectedAttributeId, $languageId)
    {
        //Retrieve the attribute details by group id
        $attributeGroupDetails = $this->getAttributeGroupDetailsByGroupId($selectedAttributeId, $languageId);
        $attributeArray = array();
        $attributeType = '';
        if (count($attributeGroupDetails) > 0) {
            foreach ($attributeGroupDetails as $key => $groupDetails) {
                if (isset($groupDetails['attributeGroupId'])
                && $groupDetails['attributeGroupId'] > 0
                && isset($groupDetails['sourceTable'])
                && trim($groupDetails['sourceTable']) != '') {
                    //Get all merchant attribute id's in the same attribute group.
                    $merchantAttributeId = $this->getMerchantAttributeId($groupDetails['attributeGroupId'], $groupDetails['sourceTable']);
                    if (count($merchantAttributeId) > 0) {
                        //$parentTableName = strtoupper($groupDetails['parentSourceTable']);
                        $parentTableName = $groupDetails['parentSourceTable'];
                        foreach ($merchantAttributeId as $merchantAttributeKey => $merchantAttribute) {
                            //var_dump($parentTableName . ' : ' . $merchantAttributeId['merchantAttributeId']);
                            //The next step retrieves the values.

                            switch ($parentTableName) {
                                case 'COLOURS_Groups':
                                    $attributeType = 'colour';
                                    $attributeArray[] = $this->getColours($parentTableName, $merchantAttribute['merchantAttributeId'], $languageId);
                                    break;
                                case 'SIZES_Accessories':
                                    $attributeType = 'size';

                                    $attributeArray[] = $this->getSizes($parentTableName, $merchantAttribute['merchantAttributeId'], $languageId);
                                    break;
                                case 'SIZES_Shoes':
                                    $attributeType = 'size';

                                    $attributeArray[] = $this->getSizes($parentTableName, $merchantAttribute['merchantAttributeId'], $languageId);
                                    break;
                                case 'SIZES_Shirts':
                                case 'SIZES_Jackets':
                                case 'SIZES_Trousers':
                                case 'SIZES_SwimShorts':
                                    $attributeType = 'size';
                                    $attributeArray[] = $this->getSizesComplex($parentTableName, $merchantAttribute['merchantAttributeId'], $languageId);
                                    break;
                                case 'SIZES_Belts':
                                    $attributeType = 'size';
                                    $attributeArray[] = $this->getSizesComplex($parentTableName, $merchantAttribute['merchantAttributeId'], $languageId);
                                    break;
                                case 'SIZES_Clothes':
                                    $attributeType = 'size';

                                    $attributeArray[] = $this->getSizesComplex($parentTableName, $merchantAttribute['merchantAttributeId'], $languageId);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }
        $returnArray = array_map("unserialize", array_unique(array_map("serialize", $attributeArray)));
        return array('dataCount' => count($returnArray), 'data' => $returnArray, 'attributeType' => $attributeType);
    }

    /**
     * Retrieve size based on size id and language
     *
     * @param $tableName
     * @param $sizeId
     * @param $languageId
     * @param $isoCountryCode
     * @return mixed
     */
    protected function getSizes($tableName, $sizeId, $languageId, $isoCountryCode = 'uk')
    {
        $returnArray = array();
        $this->_sql->setTable(array('s' => $tableName));
        $select = $this->_sql->select();
        $select->columns(array('type', $isoCountryCode));
        $select->where(array('s.sizeId' => $sizeId));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            //$returnArray[$value[$isoCountryCode] . '@' . $tableName] = 'Size ' . str_replace('.0', '', $value[$isoCountryCode]) . ' (' . $value['type'] . ')';
            $returnArray[$sizeId . '@' . $tableName] = 'Size ' . str_replace('.0', '', $value[$isoCountryCode]) . ' (' . $value['type'] . ')';
        }

        return $returnArray;
    }
    /**
     * Retrieve size for complex sisez based on size id and language
     *
     * @param $tableName
     * @param $sizeId
     * @param $languageId
     * @param $isoCountryCode
     * @return mixed
     */
    protected function getSizesComplex($tableName, $sizeId, $languageId, $isoCountryCode = 'uk')
    {
        $returnArray = array();
        $this->_sql->setTable(array('s' => $tableName));
        $select = $this->_sql->select();
        $select->join(
            array('complexSize' => $tableName . '_Info'),
            "s.".$isoCountryCode." = complexSize.size",
            array('*')
        );
        $select->columns(array('type', $isoCountryCode));
        $select->where(array('s.sizeId' => $sizeId));
        $statement = $this->_sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();
        foreach ($results as $key => $value) {
            if ($tableName == 'SIZES_Belts') {
                $returnArray[$sizeId . '@' . $tableName] = 'Size ' . str_replace('.0', '', $value[$isoCountryCode]) . ' (' . $value['type'] . '); ' .
                    'Waist size (inch/cm): ' . $value['waistInch'] . '/' . $value['waistCm'] . '; ';
            } elseif ( $tableName == 'SIZES_Trousers' ) {
                $returnArray[$sizeId . '@' . $tableName] = 'Size ' . str_replace('.0', '', $value[$isoCountryCode]) . ' (' . $value['type'] . '); ' .
                    'Waist size (inch/cm): ' . $value['waistInch'] . '/' . $value['waistCm'] . '; Leg Size (inch/cm): ' . $value['legInch'] . '/' . $value['legCm'] . ';';
            } elseif ( $tableName == 'SIZES_SwimShorts' ) {
                $returnArray[$sizeId . '@' . $tableName] = 'Size ' . str_replace('.0', '', $value[$isoCountryCode]) . ' (' . $value['type'] . '); ' .
                    'Waist size (inch/cm): ' . $value['waistInch'] . '/' . $value['waistCm'] . ';';
            } else {
                $returnArray[$sizeId . '@' . $tableName] = 'Size ' . str_replace('.0', '', $value[$isoCountryCode]) . ' (' . $value['type'] . '); ' .
                    'Chest size (inch/cm): ' . $value['chestInch'] . '/' . $value['chestCm'] . '; ' .
                    'Europe size: ' . $value['eur'];
            }
        }
        return $returnArray;
    }

    /**
     * Get size name based on size id
     *
     * @param $tableName
     * @param $sizeId
     * @param $isoCountryCode
     * @return string
     */
    public function getSizeNameBySizeId($tableName, $sizeId, $isoCountryCode = 'uk')
    {
        $returnArray = array();
        $this->_sql->setTable(array('s' => $tableName));
        $select = $this->_sql->select();
        $select->columns(array('type', $isoCountryCode));
        $select->where(array('s.sizeId' => $sizeId));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $key => $value) {
            if($value[$isoCountryCode] == 'One Size')
            {
                // todo get the text from config file
                $returnArray[$this->_defaultOneSizeCode] = str_replace('.0', '', $value[$isoCountryCode]) . ' (' .$value['type'] . ')';;
            }else{
                $returnArray[$value[$isoCountryCode]] = str_replace('.0', '', $value[$isoCountryCode]) . ' (' .$value['type'] . ')';
            }
        }

        return $returnArray;
    }

    /**
     * Retrieve colours based on colour id and language id
     *
     * @param \PrismProductsManager\Model\type $tableName
     * @param \PrismProductsManager\Model\type $colourId
     * @param \PrismProductsManager\Model\type $languageId
     * @return \PrismProductsManager\Model\type
     * @param type $tableName
     * @param type $colourId
     * @param type $languageId
     * @return type
     */
    protected function getColours($tableName, $colourId, $languageId)
    {
        $returnArray = array();
        $this->_sql->setTable(array('cd' => $tableName . '_Definitions'));
        $select = $this->_sql->select();
        $select->columns(array(
            'colourName' => 'name'
        ));
        $select->join(
            array('c' => 'COLOURS_Groups'), //Set join table and alias
            'c.colourGroupId = cd.colourGroupId',
            array(
                'colourCode' => 'groupCode'
            )
        );
        $select->where(array(
            'cd.colourGroupId' => $colourId,
            'cd.languageId' => $languageId,
            'c.enabled' => 1
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            $returnArray[$colourId .  '@' . $tableName] = $value['colourName'];
        }
        return $returnArray;
    }

    /**
     * Retrieve colour name by the id of the colour
     *
     * @param $tableName
     * @param $colourId
     * @param $languageId
     * @return mixed
     */
    public function getColourNameByColourId($tableName, $colourId, $languageId = 1)
    {
        $returnValue = array();
        $this->_sql->setTable(array('cd' => $tableName . '_Definitions'));
        $select = $this->_sql->select();
        $select->columns(array('colourName' => 'name'));
        $select->join(
                array('c' => 'COLOURS_Groups'), //Set join table and alias
                "c.colourGroupId = cd.colourGroupId",
                array('colourCode' => 'groupCode')
            );
        $select->where(array('cd.colourGroupId' => $colourId, 'cd.languageId' => $languageId));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            $returnValue[$value['colourCode']] = $value['colourName'];
        }

        return $returnValue;
    }

        /**
     * Retrieve merchant attribute id based on the attribute group id and source table.
     * Note: This will return all merchant attribute id's in the same attribute group.
     *
     * @param type $attributeGroupId
     * @param type $sourceTable
     * @return type
     */
    protected function getMerchantAttributeId($attributeGroupId, $sourceTable)
    {
        $returnArray = array();
        $this->_sql->setTable(array('s' => $sourceTable));
        $select = $this->_sql->select();
        $select->columns(array('merchantAttributeId'));
        $select->where(array('s.attributeGroupId' => $attributeGroupId));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            $returnArray[] = $value;
        }

        return $returnArray;
    }



    /**
     * Retrieve details about an attribute group based on the attribute group id.
     * Note: It gets information about the group and it's parent so it joins itself
     * Major reason for the join is to return information about the parent e.g (sourceTable)
     *
     * @param type $selectedAttributeId
     * @param type $languageId
     * @return type
     */
    protected function getAttributeGroupDetailsByGroupId($selectedAttributeId, $languageId)
    {
        $returnArray = array();
        $this->_sql->setTable(array('ag' => $this->_attributeGroupsTable));
        $select = $this->_sql->select();
        $select->join(
            array('ag2' => $this->_attributeGroupsTable), //Set join table and alias
            "ag.groupParentId = ag2.attributeGroupId",
            array(
                'parentSourceTable' => 'sourceTable'
            )
        );
        $select->where(array('ag.attributeGroupId' => $selectedAttributeId));
        $select->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            $returnArray[] = $value;
        }

        return $returnArray;
    }


    /**
     * Note : Save the merchant category and save the sku
     * @param $data
     * @throws \Exception
     */
    public function saveMerchantCategory($data)
    {
        // Set the data for merchant category
        $this->_merchantCategoriesEntity->setParentId($data['parentId']);
        $this->_merchantCategoriesEntity->setStatus($data['status']);
        $this->_merchantCategoriesEntity->setCreated(date("Y-m-d H:i:s"));
        $this->_merchantCategoriesEntity->setModified(date("Y-m-d H:i:s"));
        // Extract the data from the merchant
        $saveData = $this->_hydrator->extract($this->_merchantCategoriesEntity);
        // Save Into Merchant Categories
        $this->_sql->setTable($this->_merchantCategoriesTable);
        $action = $this->_sql->insert();
        $action->values($saveData);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        try {
            $statement->execute();
        }
        catch (\Exception $ex) {
            throw new \Exception($ex);
            // log any error
        }
        $merchantCategoryId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

        // Set the data for merchant category definitions
        $this->_merchantCategoriesDefinitionsEntity->setProductMerchantCategoryId($merchantCategoryId);
        $this->_merchantCategoriesDefinitionsEntity->setCategoryName($data['categoryName']);
        $this->_merchantCategoriesDefinitionsEntity->setShortDescription($data['shortDescription']);
        $this->_merchantCategoriesDefinitionsEntity->setFullDescription($data['fullDescription']);
        $this->_merchantCategoriesDefinitionsEntity->setCreated(date("Y-m-d H:i:s"));
        $this->_merchantCategoriesDefinitionsEntity->setModified(date("Y-m-d H:i:s"));
        // Set the table for Categories Definitions
        $this->_sql->setTable($this->_merchantCategoriesDefinitionsTable);
        // Set the data for each language and save for each language
        foreach ($this->_languages as $language) {
            $this->_merchantCategoriesDefinitionsEntity->setLanguageId($language);

            $saveData = $this->_hydrator->extract($this->_merchantCategoriesDefinitionsEntity);
            $action = $this->_sql->insert();
            $action->values($saveData);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            try {
                $statement->execute();
            }
            catch (\Exception $ex) {
                throw new \Exception($ex);
                // log any error
            }
        }

        // Save the sku building rules
        $this->_saveSkuRules($merchantCategoryId, $data['skuRules']);
    }
    /**
     * Note: First search for preview sku building rules for that category
     * Delete the preview sku building rules
     * Save the new sku building rules
     */
    private function _saveSkuRules($categoryId, $skuRules)
    {
        $this->_sql->setTable($this->_skuRulesTable);
        // Check if category has already sku building rules
        $action = $this->_sql->select()
            ->where($this->_skuRulesTable . '.merchantCategoryId = ' . $categoryId);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        // If category has sku building rules delete them before saving new ones
        if ($result->count() > 0) {
            $action = $this->_sql->delete()
                ->where($this->_skuRulesTable . '.merchantCategoryId = ' . $categoryId);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            try {
                $statement->execute();
            } catch (\Exception $ex) {
                throw new \Exception($ex);
            }
        }

        $this->_skuRulesEntity->setMerchantCategoryId($categoryId);
        // Insert new building sku rules
        if (!empty($skuRules)) {
            $position = 1;

            foreach ($skuRules as $sku) {
                if (strpos($sku, '_') !== false) {
                    $this->_skuRulesEntity->setIsCommonAttribute(1);
                }

                $this->_skuRulesEntity->setMerchantAttributeGroupId($sku);
                $this->_skuRulesEntity->setPosition($position);
                $position++;
                $data = $this->_hydrator->extract($this->_skuRulesEntity);

                $action = $this->_sql->insert();

                $action->values($data);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                try {
                    $statement->execute();
                }
                catch (\Exception $ex) {
                    throw new \Exception($ex);
                    // log any error
                }
            }
        }
    }

    /**
     * Note: Check if category name in the $submitedFormData is the same
     * as the one saved in the database
     * @param $submitedformData
     * @returns bool
     */
    public function checkCategoryNameDifference($submitedformData)
    {
        $flag = true;
        $this->_sql->setTable($this->_merchantCategoriesDefinitionsTable);
        $action = $this->_sql->select()
            ->where($this->_merchantCategoriesDefinitionsTable.'.productMerchantCategoryId = '. $submitedformData['productMerchantCategoryId'])
            ->where($this->_merchantCategoriesDefinitionsTable.'.languageId = '. $this->_defaultLanguage);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        try {
            $result = $statement->execute()->next();
        } catch (\Exception $ex) {
            echo '<pre>';
            print_r($ex);
        }
        // check if category name submitted is the same with category name from the database
        if ($submitedformData['categoryName'] == $result['categoryName']) {
            $flag = false;
        }

        return $flag;
    }
    /**
     *
     */
    public function updateMerchantCategory($data)
    {
        // Update Categories table
        $this->_sql->setTable($this->_merchantCategoriesTable);
        // Set the data for the entity
        $this->_merchantCategoriesEntity->setStatus($data['status']);
        $this->_merchantCategoriesEntity->setParentId($data['parentId']);
        $this->_merchantCategoriesEntity->setModified(date("Y-m-d H:i:s"));
        // Extract data from the entity with the hydrator
        $dataUpdate = $this->_hydrator->extract($this->_merchantCategoriesEntity);
        // Update the data
        $update = $this->_sql->update();
        $update->table($this->_merchantCategoriesTable);
        $update->set($dataUpdate);
        $update->where(array(
            $this->_merchantCategoriesTable . '.productMerchantCategoryId' => $data['productMerchantCategoryId'],
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();

        // Update Categories Definitions Table
        $this->_sql->setTable($this->_merchantCategoriesDefinitionsTable);
        // Set the data for the entity
        $this->_merchantCategoriesDefinitionsEntity->setCategoryName($data['categoryName']);
        $this->_merchantCategoriesDefinitionsEntity->setLanguageId($this->_defaultLanguage);
        $this->_merchantCategoriesDefinitionsEntity->setShortDescription($data['shortDescription']);
        $this->_merchantCategoriesDefinitionsEntity->setFullDescription($data['fullDescription']);
        $this->_merchantCategoriesDefinitionsEntity->setModified(date("Y-m-d H:i:s"));
        // Extract the data from the entity with the hydrator
        $dataUpdate = $this->_hydrator->extract($this->_merchantCategoriesDefinitionsEntity);
        // Update the data to definitions table
        unset($dataUpdate['productMerchantCategoryId']);
        unset($dataUpdate['created']);
        $update = $this->_sql->update();
        $update->table($this->_merchantCategoriesDefinitionsTable);
        $update->set($dataUpdate);
        $update->where(array(
            'productMerchantCategoryId' => $data['productMerchantCategoryId'],
            'languageId' => $this->_defaultLanguage,
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
        // Save the sku building rules
        $this->_saveSkuRules($data['productMerchantCategoryId'], $data['skuRules']);
    }
    /**
     * Delete merchant category
     * @params $id
     * @throws \Exception
     */
    public function deleteMerchantCategory($id)
    {
        // Select the category that will be deleted
        $this->_sql->setTable($this->_merchantCategoriesTable);
        $action = $this->_sql->select()
            ->where($this->_merchantCategoriesTable . '.productMerchantCategoryId = '. $id);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $category = $statement->execute()->next();

        // Select all the childs of the group that will be deleted
        $this->_sql->setTable($this->_merchantCategoriesTable);
        $action = $this->_sql->select()
            ->where($this->_merchantCategoriesTable . '.parentId = '. $id);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        // Promote all the childs in the hierarchy one level
        foreach ($result as $res) {
            $update = $this->_sql->update();
            $update->table($this->_merchantCategoriesTable);
            $update->set(array('parentId' => $category['parentId']));
            $update->where(array(
                'productMerchantCategoryId' => $res['productMerchantCategoryId'],
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            $statement->execute();
        }

        // Delete from Category table
        $dataUpdate = array('status' => 'DELETED');
        $update = $this->_sql->update();
        $update->table($this->_merchantCategoriesTable);
        $update->set($dataUpdate);
        $update->where(array(
            'productMerchantCategoryId' => $id,
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     * This function helps to rebuild merchant category tree from the selected id.
     * How it works:
     * 1. Get the selected merchant category id for a product
     * 2. Retrieve its siblings
     * 3. Get the parent categories and their siblings
     *
     * @param type $productMerchantCategoryId
     * @param type $languageId
     * @return type
     */
    public function rebuildMerchantCategoryByMerchantID($productMerchantCategoryId, $languageId)
    {
        $returnArray = array();
        $this->_sql->setTable(array('mc' => $this->_merchantCategoriesTable));
        $select = $this->_sql->select();
        $select->columns(array('parentId', 'productMerchantCategoryId'));
        $select->join(
                array(
                    'mcd' => $this->_merchantCategoriesDefinitionsTable), //Set join table and alias
                    'mc.productMerchantCategoryId = mcd.productMerchantCategoryId',
                    array('categoryName')
                );
        $select->where(
                array(
                    'mc.status' => 'ACTIVE',
                    'mc.productMerchantCategoryId' => $productMerchantCategoryId,
                    'mcd.languageId' => $languageId
                    )
                );

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            $returnArray[$value['parentId']]['selected'] = $value;
        }
        if (isset($returnArray)) {
            $parentId = array_keys($returnArray);
            if (isset($parentId[0])) {
                $parentId = $parentId[0];
            }
        }
        if (!empty($parentId) && $parentId > 0) {
            do {
                //Get the sibling information
                $returnArray[$parentId]['siblings'] = $this->_getMerchantCategorySiblingsById($parentId, $languageId);
                //Get information on the parents
                $parentDetails = $this->_getParentMerchantCategoryDetails($parentId, $languageId);

                if (count($parentDetails) > 0) {
                    //Merge the array together
                    $returnArray = $parentDetails + $returnArray;
                    //Extract the parentId
                    $parentId = array_keys($parentDetails);
                    $parentId = $parentId[0];
                }
            } while ($parentId > 0);
        }
        return $returnArray;
    }

    /**
     * Function to retrieve merchant category siblings
     *
     * @param type $parentId
     * @param type $languageId
     * @return type
     */
    protected function _getMerchantCategorySiblingsById($parentId, $languageId)
    {
        $returnArray = array();
        $this->_sql->setTable(array('mc' => $this->_merchantCategoriesTable));
        $select = $this->_sql->select();
        $select->columns(array('productMerchantCategoryId'));
        $select->join(
            array('mcd' => $this->_merchantCategoriesDefinitionsTable), //Set join table and alias
            'mc.productMerchantCategoryId = mcd.productMerchantCategoryId',
            array('categoryName')
        );
        $select->where(array(
            'mc.status' => 'ACTIVE',
            'mc.parentId' => $parentId,
            'mcd.languageId' => $languageId
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {
            $returnArray[$value['productMerchantCategoryId']] = $value['categoryName'];
        }
        return $returnArray;
    }

    /**
     * Function to retrieve details about merchant category parent
     *
     * @param type $parentId
     * @param type $languageId
     * @return type
     */
    protected function _getParentMerchantCategoryDetails($parentId, $languageId)
    {
        $returnArray = array();
        $this->_sql->setTable(array('mc' => $this->_merchantCategoriesTable));
        $select = $this->_sql->select();
        $select->columns(array('parentId', 'productMerchantCategoryId'));
        $select->join(
                array(
                    'mcd' => $this->_merchantCategoriesDefinitionsTable), //Set join table and alias
                    'mc.productMerchantCategoryId = mcd.productMerchantCategoryId',
                    array('categoryName')
                );
        $select->where(
                array(
                    'mc.status' => 'ACTIVE',
                    'mc.productMerchantCategoryId' => $parentId,
                    'mcd.languageId' => $languageId
                    )
                );

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $key => $value) {

            $returnArray[$value['parentId']]['selected'] = $value;
        }

        return $returnArray;

    }
    /**
     * Note : Select the first parent , update the flag, set the param id for the current group, Recursive
     * @param $id
     * @param $data
     * @return bool
     * @throw \Exception
     */
    private function _updateFilterFlagForParent($id, $data)
    {
        $this->_sql->setTable($this->_merchantCategoriesTable);
        // Get the current group
        $action = $this->_sql->select()
            ->where($this->_merchantCategoriesTable . '.productMerchantCategoryId = ' . $id);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute()->next();

        // Update parent group with flag
        $update = $this->_sql->update();
        $update->table($this->_merchantCategoriesTable);
        $update->set($data);
        $update->where(array(
            'productMerchantCategoryId' => $result['parentId'],
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();

        // If parent of group is top level stop recursive function
        if ($result['productMerchantCategoryId'] == $result['productMerchantCategoryId']) {
            return false;
        }
        // Recursiv function call
        $this->_updateFilterFlagForParent($result['categoryParentId'], $data);
    }
    /**
     * Note : Select the first children , update the flag, set the param id for the current group, Recursive
     * @param $id
     * @param $data
     * @return bool
     * @throw \Exception
     */
    private function _updateFilterFlagForChildren($id, $data)
    {
        $this->_sql->setTable($this->_merchantCategoriesTable);
        // Get the current group
        $action = $this->_sql->select()
            ->where($this->_merchantCategoriesTable . '.productMerchantCategoryId = ' . $id);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute()->next();

        // Get the children group and check if the group has children
        $action = $this->_sql->select()
            ->where($this->_merchantCategoriesTable . '.parentId = ' . $id);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        // If the group dosen't have children kill it
        if (empty($result)) {
            return false;
        }
        foreach ($result as $res) {
            // Recursive function to update all children of $res
            $this->_updateChildrenFlagRecursive($res, $data);
            // Update children group with flag
            $update = $this->_sql->update();
            $update->table($this->_merchantCategoriesTable);
            $update->set($data);
            $update->where(array(
                'productMerchantCategoryId' => $res['productMerchantCategoryId'],
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            $statement->execute();
        }
    }

    /**
     * Note : Selects all of the children groups
     * Iterate the groups and foreach group set the filter flag and recursive access the function
     * @param $group
     * @param $data
     * @return bool
     * @throw \Exception
     */
    private function _updateChildrenFlagRecursive($group, $data)
    {
        $this->_sql->setTable($this->_merchantCategoriesTable);
        // Get all of group childrens
        $action = $this->_sql->select()
            ->where($this->_merchantCategoriesTable . '.parentId = ' . $group['productMerchantCategoryId']);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();

        if (empty($result)) {
            return false;
        }
        // Foreach children of $group loops again in recursive function _updateChildrenFlagRecursive
        foreach ($result as $res) {
            $update = $this->_sql->update();
            $update->table($this->_merchantCategoriesTable);
            $update->set($data);
            $update->where(array(
                'productMerchantCategoryId' => $res['productMerchantCategoryId'],
            ));
            $statement = $this->_sql->prepareStatementForSqlObject($update);
            $statement->execute();
            // Recursiv function call
            $this->_updateChildrenFlagRecursive($res, $data);
        }
    }
}
