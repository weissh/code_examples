<?php

namespace PrismProductsManager\Model;

/**
 * Class MerchantCategoriesEntity
 * @package PrismProductsManager\Model
 * @author cristian.popescu
 */
class MerchantCategoriesEntity
{
    /**
     * @var
     */
    protected $_parentId;
    /**
     * @var
     */
    protected $_status;
    /**
     * @var
     */
    protected $_created;
    /**
     * @var
     */
    protected $_modified;

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->_created = $created;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->_created;
    }

    /**
     * @param mixed $modified
     */
    public function setModified($modified)
    {
        $this->_modified = $modified;
    }

    /**
     * @return mixed
     */
    public function getModified()
    {
        return $this->_modified;
    }

    /**
     * @param mixed $parentId
     */
    public function setParentId($parentId)
    {
        $this->_parentId = $parentId;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->_parentId;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->_status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->_status;
    }



}
    