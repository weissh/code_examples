<?php

namespace PrismProductsManager\Model;

use PrismProductsManager\Controller\ProductsController;
use Zend\Db\Adapter\Adapter;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
//use Zend\Db\ResultSet\HydratingResultSet;
use PrismCommon\Service\ActionLogger;

/**
 * This class is a sub class of ProductsMapper class. 
 * It was created to reduce the length of the parent calss.
 * 
 * It holds business logic for managing product attributes.
 */
class ProductsMapperAttributes extends ProductsMapper
{

    protected $_sql;
    protected $_hydrator;
    protected $_dbAdapter;
    protected $_attributesGroupMapper;
    protected $_productsMapperVariants;
    protected $_actionLogger;
    protected $_productsAttributesCombinationTable = 'PRODUCTS_ATTRIBUTES_Combination';
    protected $_attributeGroupsDefinitionsTable = 'ATTRIBUTES_Group_Definitions';
    protected $_attributeGroupsTable = 'ATTRIBUTES_Groups';
    protected $_attributesTable = 'ATTRIBUTES';
    protected $_attributesDefinitionsTable = 'ATTRIBUTES_Definitions';
    protected $_productsAttributesCombinationDefinitions = 'PRODUCTS_ATTRIBUTES_Combination_Definitions';
    protected $_languages;
    protected $currentLanguage;

    public function __construct(
        Sql $sql, $hydrator, Adapter $dbAdapter, $attributesGroupMapper,
        ActionLogger $actionLogger,
        $languages, $currentLanguage
    )
    {
        $this->_sql = $sql;
        $this->_hydrator = $hydrator;
        $this->_dbAdapter = $dbAdapter;
        $this->_attributesGroupMapper = $attributesGroupMapper;
        $this->_actionLogger = $actionLogger;
        $this->_languages = $languages;
        $this->_currentLanguage = $currentLanguage;
    }

    /**
     * Checks if an attribute exists
     * If yes - returns it's value
     * Else - returns false
     * 
     * @param $productOrVariantId
     * @param $attributeGroupId
     * @param $isVariant
     * @return mixed
     */
    public function checkIfProductAttributeExists($productOrVariantId, $attributeGroupId, $isVariant = 0)
    {
        $this->_sql->setTable($this->_attributeGroupsTable);
        $select = $this->_sql->select()
            ->where('attributeGroupId = ' . $attributeGroupId);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $group = $statement->execute()->next();

        $attributeValue = false;
        if ($group['viewAs'] == 'TEXT') {
            $this->_sql->setTable(array('pac' => $this->_productsAttributesCombinationTable));
            $select = $this->_sql->select();
            $select->join(
                array('pacd' => $this->_productsAttributesCombinationDefinitions),
                "pac.productAttributeCombinationId = pacd.productAttributeCombinationId",
                array('attributeValue')
            );
            $select->where(array(
                'pac.productOrVariantId' => $productOrVariantId,
                'pac.attributeGroupId' => $attributeGroupId,
                'pac.isVariant' => $isVariant,
                'pacd.languageId' => $this->_currentLanguage,
                'pac.status' => 'ACTIVE'
            ));
            $select->limit(1);
        } else {
            $this->_sql->setTable(array('pac' => $this->_productsAttributesCombinationTable));
            $select = $this->_sql->select();
            $select->columns(array('attributeValue'));
            $select->where(array(
                'pac.productOrVariantId' => $productOrVariantId,
                'pac.attributeGroupId' => $attributeGroupId,
                'pac.isVariant' => $isVariant,
                'pac.status' => 'ACTIVE'
            ));
            $select->limit(1);
        }

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if($result->count() == 1) {
            $attributeValue = $result->next()['attributeValue'];
        }

        return $attributeValue;
    }
    
    /**
     * Deactivate product Attribute
     * 
     * @param $productOrVariantId
     * @param $attributeGroupId
     * @param $isVariant
     * @param $attributeValue
     * @return mixed
     */
    public function deactivateProductAttribute($productOrVariantId, $attributeGroupId, $isVariant = 0, $attributeValue)
    {
        $whereClause = array(
            'productOrVariantId' => $productOrVariantId,
            'attributeGroupId' => $attributeGroupId,
            'isVariant' => $isVariant,
            'attributeValue' => $attributeValue,
            'status' => 'ACTIVE'
        );
        $fieldsToUpdate = array('status' => 'INACTIVE');
        $table = $this->_productsAttributesCombinationTable;
        $productAttributeActivated = $this->_update($fieldsToUpdate, $table, $whereClause);

        //TODO: log action correctly -- currently we don't have access to a correct userID
        $action = array(
            'data' => $fieldsToUpdate,
            'condition' => $whereClause
        );
        $this->_actionLogger->logCurrentAction(1, "Deactivate Product Attribute", json_encode($action), $table);

        return $productAttributeActivated;
    }

    /**
     * Update product Attribute
     *
     * @param $productOrVariantId
     * @param $attributeGroupId
     * @param $isVariant
     * @param $attributeValue
     * @return mixed
     */
    public function updateProductAttribute($productOrVariantId, $attributeGroupId, $isVariant = 0, $attributeValue, $viewAs, $languageId = 1)
    {
        $whereClause = array(
            'productOrVariantId' => $productOrVariantId,
            'attributeGroupId' => $attributeGroupId,
            'isVariant' => $isVariant
        );
        $fieldsToUpdate = array('attributeValue' => $attributeValue);
        $table = $this->_productsAttributesCombinationTable;
        $productAttributeActivated = $this->_update($fieldsToUpdate, $table, $whereClause);
        //TODO: log action correctly -- currently we don't have access to a correct userID
        $action = array(
            'data' => $fieldsToUpdate,
            'condition' => $whereClause
        );
        $this->_actionLogger->logCurrentAction(1, "Update Product Attribute", json_encode($action), $table);

        return $productAttributeActivated;
    }

    /**
     * Insert new product Attribute
     *
     * @param $productOrVariantId
     * @param $attributeGroupId
     * @param $isVariant
     * @param $attributeValue
     * @param $languageId
     * @return mixed
     */
    public function insertNewProductAttribute($productOrVariantId, $attributeGroupId, $isVariant = 0, $attributeValue, $languageId = false)
    {
        // Check if the group is text and if it is text save data in definitions table
        // If not insert the data in normal table
        $this->_sql->setTable($this->_attributeGroupsTable);
        $select = $this->_sql->select()
            ->where('attributeGroupId = ' . $attributeGroupId);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();

        $this->_sql->setTable($this->_productsAttributesCombinationTable);

        if ($languageId !== false) {
            $select = $this->_sql->select()
                ->where(array(
                    'attributeGroupId' => $attributeGroupId,
                    'productOrVariantId' => $productOrVariantId,
                    'isVariant' => $isVariant,
                    'status' => 'ACTIVE',
                ));
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $pacResult = $statement->execute();
            if ($pacResult->count() > 0) {
                $productsAttributesLastValue = $pacResult->next()['productAttributeCombinationId'];
            } else {
                $data = array(
                    'attributeGroupId' => $attributeGroupId,
                    'productOrVariantId' => $productOrVariantId,
                    'attributeValue' => $result['viewAs'] == 'TEXT' ? '' : $attributeValue,
                    'isVariant' => $isVariant,
                    'groupType' => $result['viewAs'],
                    'status' => 'ACTIVE',
                    'created' => date('Y-m-d H:i:s')
                );
                $action = $this->_sql->insert();
                $action->values($data);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
                $productsAttributesLastValue = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
                //TODO: log action correctly -- currently we don't have access to a correct userID
                $action = array(
                    'data' => $data,
                    'condition' => array()
                );
                $this->_actionLogger->logCurrentAction(1, "Insert Product Attribute", json_encode($action), $this->_productsAttributesCombinationTable);
            }
        } else {
            $data = array(
                'attributeGroupId' => $attributeGroupId,
                'productOrVariantId' => $productOrVariantId,
                'attributeValue' => $result['viewAs'] == 'TEXT' ? '' : $attributeValue,
                'isVariant' => $isVariant,
                'groupType' => $result['viewAs'],
                'status' => 'ACTIVE',
                'created' => date('Y-m-d H:i:s')
            );
            $action = $this->_sql->insert();
            $action->values($data);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
            $productsAttributesLastValue = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
            //TODO: log action correctly -- currently we don't have access to a correct userID
            $action = array(
                'data' => $data,
                'condition' => array()
            );
            $this->_actionLogger->logCurrentAction(1, "Insert Product Attribute", json_encode($action), $this->_productsAttributesCombinationTable);
        }

        if ($result['viewAs'] == 'TEXT') {
            $this->_sql->setTable($this->_productsAttributesCombinationDefinitions);
            $data = array(
                'productAttributeCombinationId' => $productsAttributesLastValue,
                'attributeValue' => $attributeValue,
                'status' => 'ACTIVE',
                'created' => date('Y-m-d H:i:s')
            );

            if ($languageId !== false) {
                $data['languageId'] = $languageId;
                $action = $this->_sql->insert();
                $action->values($data);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
            } else {
                foreach ($this->_languages as $language) {
                    $data['languageId'] = $language;
                    $action = $this->_sql->insert();
                    $action->values($data);
                    $statement = $this->_sql->prepareStatementForSqlObject($action);
                    $statement->execute();
                }
            }
        }
    }
 
    /**
     * Retrieve attributes that have been assigned to products
     * 
     * @param $productId
     * @param $isVariant
     * @param $languageId
     * @param $namedArrayFlag - decides if the keys of the returned array will be the attributes names
     *      optimized for elastic-search
     * @throws \Exception
     * @return mixed
     */
    public function getAttributesAssignedToProduct($productId, $isVariant, $languageId = 1, $namedArrayFlag = false)
    {
        if ($namedArrayFlag) {
            // we want all the names to be returned at all times in the english language for elastic-search
            $languageId = 1;
        }
        $returnArray = array();
        $this->_sql->setTable(array('pact' => $this->_productsAttributesCombinationTable));
        $select = $this->_sql->select();
        $select->columns(array('attributeGroupId', 'attributeValue', 'productAttributeCombinationId'));
        $select->join(
            array(
                'agdt' => $this->_attributeGroupsDefinitionsTable
            ),
            'pact.attributeGroupId = agdt.attributeGroupId',
            array(
                'attributeGroupDefinitionId',
                'name',
            )
        );
        $select->join(
            array(
                'ag' => $this->_attributeGroupsTable
            ),
            'pact.attributeGroupId = ag.attributeGroupId',
            array('groupType','viewAs')
        );
        $select->where(array(
            'pact.productOrVariantId' => $productId,
            'pact.isVariant' => $isVariant,
            'agdt.languageId' => $languageId,
            'pact.status' => 'ACTIVE',
            'agdt.status' => 'ACTIVE'
        ));
        $select->where->greaterThan('ag.attributeGroupId', 4);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $key => $value) {
            if ($namedArrayFlag) {
                // return array format for elastic-search
                $attributeName = strtolower($value['name']);
                $attributeName = preg_replace('/[^0-9A-Za-z_]/', '_', $attributeName);
                $attributeName = preg_replace('/_+/', '_', $attributeName);
                switch ($value['viewAs']) {
                    case 'CHECKBOX':
                        $returnArray[$attributeName] = $value['attributeValue'];
                        break;
                    case 'TEXT':
                        $this->_sql->setTable($this->_productsAttributesCombinationDefinitions);
                        $select = $this->_sql->select()
                            ->where('languageId = '. $this->_currentLanguage)
                            ->where('productAttributeCombinationId = ' . $value['productAttributeCombinationId']);
                        $statement = $this->_sql->prepareStatementForSqlObject($select);
                        $newAttributeValue = $statement->execute()->next();

                        $returnArray[$attributeName] = $newAttributeValue['attributeValue'];
                        $this->_sql->setTable($this->_productsAttributesCombinationDefinitions);
                        break;
                    case 'SELECT':
                        $this->_sql->setTable(array('ad' => $this->_attributesDefinitionsTable));
                        $select = $this->_sql->select();
                        $select->columns(array('attributeValue'));
                        $select->where(array(
                            'ad.attributeId' => $value['attributeValue'],
                            'ad.languageId' => $languageId,
                            'ad.status' => 'ACTIVE',
                        ));
                        $statement = $this->_sql->prepareStatementForSqlObject($select);
                        $attributes = $statement->execute();
                        if ($attributes->count() > 0) {
                            $attribute = $attributes->next();
                            $returnArray[$attributeName] = $attribute['attributeValue'];
                        } else {
                            $returnArray[$attributeName] = false;
                        }
                        break;
                    default:
                        throw new \Exception("Invalid attribute viewAs parameter found!");
                }
            } else {
                // new attribute
                if ($value['viewAs'] == 'TEXT') {

                    // IF SOMEONE WILL REMOVE SPACE I WILL LOOK INTO THE COMMIT AND I WILL SLAIN THE ONE WHO DOES IT
                    $this->_sql->setTable($this->_productsAttributesCombinationDefinitions);
                    $select = $this->_sql->select()
                        ->where('languageId = '. $this->_currentLanguage)
                        ->where('productAttributeCombinationId = ' . $value['productAttributeCombinationId']);
                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $newAttributeValue = $statement->execute()->next();

                    $returnArray[strtolower($value['groupType']).'_'. $value['attributeGroupId']] = $newAttributeValue['attributeValue'];

                } else {
                    $returnArray[strtolower($value['groupType']).'_'. $value['attributeGroupId']] = $value['attributeValue'];
                }

            }
        }        
        return $returnArray;
    }

    public function getAllCombinationsForProduct($productId)
    {
        $returnArray = array();
        $this->_sql->setTable(array('pact' => $this->_productsAttributesCombinationTable));
        $select = $this->_sql->select()
            ->where(array(
                'pact.productOrVariantId' => $productId,
                'pact.isVariant' => 0,
                'pact.status' => 'ACTIVE'
            ));

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $result) {
            $this->_sql->setTable(array('pactd' => $this->_productsAttributesCombinationDefinitions));
            $select = $this->_sql->select()
                ->where(array(
                    'pactd.productAttributeCombinationId' => $result['productAttributeCombinationId'],
                ));
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $translationsResults = $statement->execute();

            if ($translationsResults->count() > 0) {
                $result['translations'] = array();
                foreach ($translationsResults as $translationsResult) {
                    $result['translations'][$translationsResult['languageId']] = $translationsResult['attributeValue'];
                }
            }
            $returnArray[] = $result;
        }
        return $returnArray;
    }

}
