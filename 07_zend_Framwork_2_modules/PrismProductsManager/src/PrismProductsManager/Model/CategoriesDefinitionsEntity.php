<?php

namespace PrismProductsManager\Model;

class CategoriesDefinitionsEntity
{

    protected $_categoryDefinitionsId;
    protected $_categoryId;
    protected $_languageId;
    protected $_versionId;
    protected $_versionDescription;
    protected $_categoryName;
    protected $_urlName;
    protected $_metaTitle;
    protected $_metaKeyword;
    protected $_metaDescription;
    protected $_shortDescription;
    protected $_longDescription;
    protected $_status;
    protected $_makeLive;
    protected $_created;
    protected $_modified;

    public function __construct()
    {
        //Set some default values
        $this->_makeLive = '0000-00-00 00:00:00';
        $this->_created = date("Y-m-d H:i:s");
        $this->_modified = date("Y-m-d H:i:s");
        $this->_languageId = 1;
        $this->_versionId = 1;
        $this->_versionDescription = '';
        $this->_urlName = '';
    }

    public function getCategoryDefinitionsId()
    {
        return $this->_categoryDefinitionsId;
    }

    public function getCategoryId()
    {
        return $this->_categoryId;
    }

    public function getLanguageId()
    {
        return $this->_languageId;
    }

    public function getVersionId()
    {
        return $this->_versionId;
    }

    public function getVersionDescription()
    {
        return $this->_versionDescription;
    }

    public function getCategoryName()
    {
        return $this->_categoryName;
    }

    public function getUrlName()
    {
        return $this->_urlName;
    }

    public function getmetaTitle()
    {
        return $this->_metaTitle;
    }

    public function getmetaKeyword()
    {
        return $this->_metaKeyword;
    }

    public function getmetaDescription()
    {
        return $this->_metaDescription;
    }

    public function getShortDescription()
    {
        return $this->_shortDescription;
    }

    public function getLongDescription()
    {
        return $this->_longDescription;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getMakeLive()
    {
        return $this->_makeLive;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setCategoryDefinitionsId($_categoryDefinitionsId)
    {
        $this->_categoryDefinitionsId = $_categoryDefinitionsId;
    }

    public function setCategoryId($_categoryId)
    {
        $this->_categoryId = $_categoryId;
    }

    public function setLanguageId($_languageId)
    {
        $this->_languageId = $_languageId;
    }

    public function setVersionId($_versionId)
    {
        $this->_versionId = $_versionId;
    }

    public function setVersionDescription($_versionDescription)
    {
        $this->_versionDescription = $_versionDescription;
    }

    public function setCategoryName($_categoryName)
    {
        $this->_categoryName = $_categoryName;
    }

    public function setUrlName($_urlName)
    {
        $this->_urlName = $_urlName;
    }

    public function setmetaTitle($_metaTitle)
    {
        $this->_metaTitle = $_metaTitle;
    }

    public function setmetaKeyword($_metaKeyword)
    {
        $this->_metaKeyword = $_metaKeyword;
    }

    public function setmetaDescription($_metaDescription)
    {
        $this->_metaDescription = $_metaDescription;
    }

    public function setShortDescription($_shortDescription)
    {
        $this->_shortDescription = $_shortDescription;
    }

    public function setLongDescription($_longDescription)
    {
        $this->_longDescription = $_longDescription;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setMakeLive($_makeLive)
    {
        $this->_makeLive = $_makeLive;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }


    /*
      protected $_tableName = 'CATEGORIES_Definitions';
      protected $_dbAdapter;
      protected $_sql;

      public function __construct()
      {
      $this->_dbAdapter = $dbAdapter;
      $this->_sql = new Sql($dbAdapter);
      $this->_sql->setTable($this->_tableName);
      }

      public function saveCategoryDefinitions(CategoriesEntity $category)
      {
      $hydrator = new ClassMethods(FALSE);
      $data = $hydrator->extract($category);

      try {

      $action = $this->_sql->insert();
      unset($data['categoryId']);
      $action->values($data);

      $statement = $this->_sql->prepareStatementForSqlObject($action);
      $result = $statement->execute();
      } catch (\Exception $e) {
      // var_dump($e);
      // log any error
      }

      return $result;
      }

      public function getCategoryByName($categoryName)
      {
      try {
      $select = $this->_sql->select();
      $select->where(array('categoryName' => $categoryName));

      $statement = $this->_sql->prepareStatementForSqlObject($select);
      $result = $statement->execute()->current();
      if (!$result) {
      return null;
      }
      } catch (\Exception $e) {
      var_dump($e);
      // log any error
      }
      }
     */
}
