<?php
namespace PrismProductsManager\Model;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class ProductsMerchantCategoriesEntity
{
    protected $_productMerchantcategoryId;
    
    protected $_parentId;
    
    protected $_status;
    
    protected $_created;
    
    protected $_modified;
    
    public function __construct() {
        
    }
    
    public function getProductMerchantcategoryId()
    {
        return $this->_productMerchantcategoryId;
    }

    public function getParentId()
    {
        return $this->_parentId;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setProductMerchantcategoryId($_productMerchantcategoryId)
    {
        $this->_productMerchantcategoryId = $_productMerchantcategoryId;
    }

    public function setParentId($_parentId)
    {
        $this->_parentId = $_parentId;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }


    
}
