<?php

namespace PrismProductsManager\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
//use Zend\Db\ResultSet\HydratingResultSet;
use PrismProductsManager\Model\ProductsMapper;

//use PrismProductsManager\Model\Entity\ProductsRelatedProductsEntity;

/**
 * This class is a sub class of ProductsMapper class.
 * It was created to reduce the length of the parent calss.
 *
 * It holds business logic for activating products.
 */
class ProductsMapperActivateProduct extends ProductsMapper
{

    protected $_sql;
    protected $_hydrator;
    protected $_dbAdapter;
    protected $_categoriesMapper;
    protected $_productsMapperVariants;
    protected $_productFolderMapper;

    public function __construct(Sql $sql, $hydrator, Adapter $dbAdapter, $productsMapperVariants, $categoriesMapper, $productFolderMapper)
    {
        $this->_sql = $sql;
        $this->_hydrator = $hydrator;
        $this->_dbAdapter = $dbAdapter;
        $this->_categoriesMapper = $categoriesMapper;
        $this->_productsMapperVariants = $productsMapperVariants;
        $this->_productFolderMapper = $productFolderMapper;
    }

    protected function validateAllMandatoryFieldsSetForProduct($productId)
    {
        //Check if product has prices
        //Check product basic information
        //Check product channels
        //Check product websites.
    }

    protected function _checkProductBasicInformation($productId)
    {

    }

    /**
     * Function to activate product
     *
     * @param $data
     * @param $languageId
     * @return mixed
     */
    protected function _activateProduct($data, $languageId = 1)
    {
        $productActivatedSuccessfully = array('success' => false);
        $productId = isset($data['productId']) ? $data['productId'] : 0;
        if ($productId) {
            $continue = true;
            //$productDetails = $this->getProductDetailsById($productId, 1);
            $productDetails = $this->getBasicProductsDetails(1, array($productId)) ;
            $productBasicDetails = isset($productDetails[0]) && count($productDetails[0]) > 0 ? $productDetails[0] : array();

            $errorMessage = '';

            //Product id 2 = Evoucher (skip some validations)
            if (isset($productBasicDetails['productTypeId'])) {
                if ($productBasicDetails['productTypeId'] != 3) {
                    //Check if product has prices
                    $productPrices = $this->getProductPrices($productId);
                    if (count($productPrices) < 1) {
                        $errorMessage .= '<li>No price has been setup for this product in <b>Basic Information</b> tab</li>';
                        $continue = false;
                    }

                    //Check if product has variants
                    $productVariants = $this->getVariants($productId);
                    if ($productVariants['dataCount'] < 1) {
                        //If there is no variant, check if the sku has been set at product level
                        if (isset($productBasicDetails['sku']) && trim($productBasicDetails['sku']) == '') {
                            $errorMessage .= '<li>You must setup at least one variant in <b>Variant Creation</b> tab OR fiil in sku field in <b>Basic Information</b> tab</li>';
                            $continue = false;
                        }
                    }

                    //Check if product belongs to at least one marketing category
                    $productMarketingCategories = $this->_categoriesMapper->getProductCategoriesRelationByProductId($productId, $languageId);
                    if (is_array($productMarketingCategories) && count($productMarketingCategories) < 1 && isset($productMarketingCategories['defaultCategory']) && $productMarketingCategories['defaultCategory'] != 0) {
                        $errorMessage .= '<li>Please select at least one default category in <b>Marketing Categories</b> tab</li>';
                        $continue = false;
                    }
                }
                //We want to return the product type Id
                $productActivatedSuccessfully['productTypeId'] = $productBasicDetails['productTypeId'];
            }

            //Check if the product has been deleted
            if (strtolower($productBasicDetails['status']) == 'deleted') {
                $errorMessage .= '<li>You cannot activate a product that has been deleted.</li>';
                $continue = false;
            }

            if (trim($productBasicDetails['productType']) == '') {
                $errorMessage .= '<li>Please select a product type in <b>Basic Information</b> tab.</li>';
                $continue = false;
            }

            if (trim($productBasicDetails['productName']) == '') {
                $errorMessage .= '<li>Please fill in the product name field in <b>Basic Information</b> tab.</li>';
                $continue = false;
            }

            if (trim($productBasicDetails['style']) == '') {
                $errorMessage .= '<li>Please fill in the style field in <b>Basic Information</b> tab.</li>';
                $continue = false;
            }

            if (trim($errorMessage) != '') {
                $errorMessage = '<ul>' . $errorMessage . '</ul>';
                $productActivatedSuccessfully['errorMessage'] = $errorMessage;
            }

            if ($continue === true) {
                $whereClause = array('productId' => $productId);
                $fieldsToUpdate = array('status' => 'ACTIVE');
                $table = $this->_tableName;
                $whereNoEqualTo = array(0 => array('status', 'DELETED'));
                //Activate product in products table
                $productActivated = $this->_update($fieldsToUpdate, $table, $whereClause, $whereNoEqualTo);
                if ($productActivated > 0) {
                    $table = $this->_productsDefinitionsTable;
                    //Update version description if it is set
                    if (isset($data['versionDescription'])) {
                        $fieldsToUpdate['versionDescription'] = $data['versionDescription'];
                    }
                    //Update version Id if it is set
                    if (isset($data['versionId'])) {
                        $fieldsToUpdate['versionId'] = $data['versionId'];
                    }
                    //Activate the product definitions table
                    $whereClause2 = array(
                        'productId' => $productId,
                        'productIdFrom'=>'PRODUCTS');
                    $whereNoEqualTo2 = array(
                        0 => array('status', 'DELETED'));

                    $productDefinitionsActivated = $this->_update($fieldsToUpdate, $table, $whereClause2, $whereNoEqualTo2);
                    $productVariantsActivated = $this->activateVariants($productId);
                    //if ($productDefinitionsActivated > 0 && $productVariantsActivated >= 0) {
                    if ($productVariantsActivated >= 0) {
                        $productActivatedSuccessfully['success'] = true;
                    }
                }
            }
        }
        return $productActivatedSuccessfully;
    }

    /**
     * Function to deactivate a product
     *
     * @param array $data
     * @return array
     */
    protected function _deactivateProduct($data)
    {
        $productDeactivatedSuccessfully = array('success' => false);
        // first deactivate the variants
        $productVariantsDeactivated = $this->deactivateVariants($data['productId']);
        if ($productVariantsDeactivated !== false) {
            $whereClause = array('productId' => $data['productId']);
            $fieldsToUpdate = array('status' => 'INACTIVE');
            $whereNoEqualTo = array(0 => array('status', 'DELETED'));
            $table = $this->_tableName;
            $productDeactivated = $this->_update($fieldsToUpdate, $table, $whereClause, $whereNoEqualTo);
            if ($productDeactivated > 0) {
                $table = $this->_productsDefinitionsTable;
                // Deactivate the product definitions table
                $productDefinitionsActivated = $this->_update($fieldsToUpdate, $table, $whereClause, $whereNoEqualTo);
                if ($productDefinitionsActivated > 0) {
                    $productDeactivatedSuccessfully['success'] = true;
                }
            }
        }
        return $productDeactivatedSuccessfully;
    }

}
