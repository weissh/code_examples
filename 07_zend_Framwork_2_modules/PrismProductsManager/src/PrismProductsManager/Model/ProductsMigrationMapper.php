<?php

namespace PrismProductsManager\Model;

use PrismMediaManager\Model\ProductFolderMapper;
use PrismSpectrumApiClient\Service\Customer;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Expression;
use Zend\Math\Rand;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\View\Model\JsonModel;

/**
 * Class ProductsMigrationMapper
 * @package PrismProductsManager\Model
 */
class ProductsMigrationMapper extends ProductsMapper
{
    /**
     * @var Adapter
     */
    protected $_dbAdapter;
    /**
     * @var Adapter
     */
    protected $_oldPMSAdapter;
    /**
     * @var Adapter
     */
    protected $_adminAdapter;
    /**
     * @var Sql
     */
    protected $_sqlPMS;
    /**
     * @var Sql
     */
    protected $_sqlOldPMS;
    /**
     * @var Sql
     */
    protected $_sqlAdmin;
    /**
     * @var ProductFolderMapper
     */
    protected $_productFolderMapper;

    /**
     * @var
     */
    protected $_languages;

    /**
     * @var array
     */
    protected $_existingColours = array('NOC', 'AUB', 'BEI', 'BLK', 'BLU', 'BRW', 'BUR', 'CAM', 'CHA', 'CLE', 'COG', 'CRE', 'DBR', 'DGR', 'FUO', 'GRE', 'GRY', 'KHA', 'LBL', 'MUL', 'NAT', 'NAV', 'OLI', 'ORA', 'PIN', 'PUR', 'RED', 'SAF', 'SAN', 'SIL', 'STO', 'TAN', 'TOR', 'WHT', 'CAS', 'MBR', 'MGY', 'MOR', 'NER', 'PYT', 'XXX', 'BLG', 'YEL', 'GAU', 'LGY', 'BRG', 'BLW', 'ROS', 'NAW', 'BLB', 'TOB', 'BRO', 'BLR', 'GLD', 'NVG', 'BRB', 'LBR', 'MUS', 'BBE', 'TOP', 'COB', 'PER', 'TAC', 'NAB', 'TAT', 'RUG', 'NAR', 'DBL', 'NAG', 'BLT', 'BRN', 'TAS', 'GRW', 'GBL', 'BLS', 'TAB', 'LIM', 'TBL', 'NBR', 'TUR', 'BRT', 'SNU', 'BTS', 'BRS', 'SBR', 'SPC', 'BSI', 'BL1', 'BL2', 'BL3', 'BAR', 'FOR', 'GRN', 'BLN', 'CHN', 'UNF', 'DGG', 'BLP', 'BBG', 'DTN', 'RBK', 'MID', 'MBW', 'BCF', 'BLM', 'RNY', 'BWI', 'BDG', 'COR', 'LON', 'VUL', 'WEL', 'PLM', 'DBE', 'RUS', 'BRY', 'BNC', 'PBL', 'LIL', 'RWD', 'INI', 'AQU', 'CAR', 'NBK', 'PBW', 'NTA', 'ANT', 'SCA', 'SWT');
    /**
     * @var array
     */
    protected $attributeMapping = array(
        '5' => 'NOEXPRESS',
        '9' => 'BACKORDERS',
        '10' => 'SUPPRESSFROMSALE',
        '11' => 'MINORDQTY',
        '13' => 'HEIGHT',
        '14' => 'WIDTH',
        '15' => 'ITEMWT',
        '17' => 'UKONLY',
        '147' => 'DESPHSE',
        '148' => 'LENGTH',
        '149' => 'COMMODITY',
        '150' => 'CNTRYORIG',
        '151' => 'HEAVYITEM',
        '152' => 'FREEPOST'
    );

    /**
     * @var array
     */
    protected $_womenStyles = array(
        'ANIALE', 'ANTOTA', 'BASTCO', 'CARDGL', 'CATEGL', 'CELILE', 'EMILPI', 'EMILSP',
        'EMILSU', 'EMILTA', 'FRANLE', 'GRAZPI', 'MALOLE', 'OMELTA', 'ROSASL', 'SIMOGL', 'SOPHMI', 'VALEDA'
    );

    /**
     * @var array
     */
    protected $womenShoesSizes = array(
        '030' => '68',
        '035' => '69',
        '040' => '70',
        '045' => '71',
        '050' => '72',
        '055' => '73',
        '060' => '73',
        '065' => '75',
        '070' => '76',
        '080' => '78',
        '090' => '80',
        '360' => '68',
        '370' => '70',
        '375' => '71',
        '380' => '72',
        '385' => '73',
        '390' => '74',
        '395' => '75',
        '400' => '76',
        '410' => '78',
        '420' => '80',
    );

    /**
     * @var array
     */
    protected $accessoriesSizeMapping = array(
        '012' => '7',
        '023' => '8',
        '034' => '9',
        '001' => '15',
    );
    /**
     * @var array
     */
    protected $clothesSizeMapping = array(
        '002' => '1',
        '003' => '2',
        '004' => '3',
        '005' => '4',
        '006' => '5',
        '007' => '6',
    );
    /**
     * @var array
     */
    protected $shoesSizeMapping = array(
        '030' => '15',
        '035' => '17',
        '040' => '19',
        '045' => '21',
        '050' => '23',
        '055' => '25',
        '060' => '27',
        '065' => '29',
        '070' => '31',
        '075' => '33',
        '080' => '35',
        '085' => '37',
        '090' => '39',
        '095' => '41',
        '100' => '43',
        '105' => '45',
        '110' => '47',
        '115' => '49',
        '120' => '51',
        '125' => '53',
        '130' => '55',
        '135' => '57',
        '140' => '59',
    );
    /**
     * @var array
     */
    protected $shirtsSizeMapping = array(
        '150' => '1',
        '155' => '2',
        '160' => '3',
        '165' => '4',
        '170' => '5',
    );

    /**
     * @var string
     */
    protected $_productVariantsTable = "PRODUCTS_R_ATTRIBUTES";
    /**
     * @var string
     */
    protected $_productsRAttributesTable = "PRODUCTS_ATTRIBUTES_Combination";
    /**
     * @var string
     */
    protected $_productsRAttributesTableDefinitions = "PRODUCTS_ATTRIBUTES_Combination_Definitions";

    private $productsMapper;
    /**
     * @var Customer
     */
    protected $_customerService;
    /**
     * @param Adapter $config
     * @param Adapter $dbAdapter
     * @param Adapter $oldPMSAdapter
     * @param Adapter $adminAdapter
     * @param ProductFolderMapper $productFolderMapper
     */
    public function __construct(
        $config,
        Adapter $dbAdapter,
        Adapter $oldPMSAdapter,
        Adapter $adminAdapter,
        Adapter $spectrumAdapter,
        ProductFolderMapper $productFolderMapper,
        Customer $customerService,
        ProductsMapper $productsMapper
    )
    {
        $this->_languages = $config['siteConfig']['languages']['site-languages'];
        $this->_websites = $config['siteConfig']['websites']['websites'];
        $this->_currencies = $config['siteConfig']['currencies']['site-currencies'];
        $this->_dbAdapter = $dbAdapter;
        $this->_adminAdapter = $adminAdapter;
        $this->_oldPMSAdapter = $oldPMSAdapter;
        $this->_spectrumAdapter = $spectrumAdapter;
        $this->_sqlPMS = new Sql($dbAdapter);
        $this->_sqlOldPMS = new Sql($oldPMSAdapter);
        $this->_sqlAdmin = new Sql($adminAdapter);
        $this->_sqlSpectrum = new Sql($spectrumAdapter);
        $this->_productFolderMapper = $productFolderMapper;
        $this->_customerService = $customerService;
        $this->productsMapper = $productsMapper;
    }
    /* ======================================================================== */
    /* START -- MIGRATE COLOURS */


    /**
     * @return bool
     */
    public function migrateSpectrumColour()
    {
        $this->_sqlSpectrum->setTable('PRODUCTS_Colours');
        $select = $this->_sqlSpectrum->select();
        $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
        $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=0;")->execute();
        $this->_dbAdapter->query("TRUNCATE COLOURS;")->execute();
        $this->_dbAdapter->query("TRUNCATE COLOURS_Definitions;")->execute();
        $this->_dbAdapter->query("TRUNCATE COLOURS_Groups;")->execute();
        $this->_dbAdapter->query("TRUNCATE COLOURS_Groups_Definitions;")->execute();
        $this->_dbAdapter->query("TRUNCATE COLOURS_R_GROUPS;")->execute();
        $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=1;")->execute();

        foreach ($results as $result) {
             try {
                 // we need to persist them all as groups in the database for each language
                 $this->_sqlPMS->setTable("COLOURS_Groups");
                 $insert = $this->_sqlPMS->insert()
                     ->values(array(
                         'groupCode' => $result['colour'],
                         'hasImage' => 0,
                         'hex' => '#000000',
                         'enabled' => 1
                     ));
                 $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                 $statement->execute();
                 $colourGroupId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

                 $this->_sqlPMS->setTable("COLOURS");
                 $insert = $this->_sqlPMS->insert()
                     ->values(array(
                         'colourCode' => $result['colour'],
                         'hex' => '#000000',
                     ));
                 $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                 $statement->execute();
                 $colourId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

                 foreach ($this->_languages as $languageId) {
                     $this->_sqlPMS->setTable("COLOURS_Groups_Definitions");
                     $insert = $this->_sqlPMS->insert()
                         ->values(array(
                             'colourGroupId' => $colourGroupId,
                             'languageId' => $languageId,
                             'name' => $result['description']
                         ));
                     $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                     $statement->execute();

                     $this->_sqlPMS->setTable("COLOURS_Definitions");
                     $insert = $this->_sqlPMS->insert()
                         ->values(array(
                             'colourId' => $colourId,
                             'languageId' => $languageId,
                             'colourName' => $result['description']
                         ));
                     $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                     $statement->execute();
                 }
                 $this->_sqlPMS->setTable("COLOURS_R_GROUPS");
                 $insert = $this->_sqlPMS->insert()
                     ->values(array(
                         'colourId' => $colourId,
                         'colourGroupId' => $colourGroupId,
                     ));
                 $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                 $statement->execute();
             } catch (\Exception $ex) {
                 var_dump($ex->getMessage());die;
             }

        }
        $this->_dbAdapter->getDriver()->getConnection()->commit();

        return true;
    }
    /**
     * Copy the PRODUCTS_Colours table from spectrum into the old CMS database first
     * @return bool$this->_currencies
     */
    public function migrateColours()
    {
        $this->_sqlOldPMS->setTable(array('P' => 'proddata'));
        $select = $this->_sqlOldPMS->select()
            ->columns(array('colour' => 'COLOR'))
            ->join(
                array('M' => 'migration_table'),
                "M.sku=P.FPRODNO",
                array()
            )
            ->join(
                array('PC' => 'PRODUCTS_Colours'),
                "PC.colour=P.COLOR",
                array('description'),
                Select::JOIN_LEFT
            )
            ->where('P.DELETED = 0')
            ->where('P.COLOR != "0NO"')
            ->group('COLOR');
        $statement = $this->_sqlOldPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
        if (empty($this->_existingColours)) {
            $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=0;")->execute();
            $this->_dbAdapter->query("TRUNCATE COLOURS;")->execute();
            $this->_dbAdapter->query("TRUNCATE COLOURS_Definitions;")->execute();
            $this->_dbAdapter->query("TRUNCATE COLOURS_Groups;")->execute();
            $this->_dbAdapter->query("TRUNCATE COLOURS_Groups_Definitions;")->execute();
            $this->_dbAdapter->query("TRUNCATE COLOURS_R_GROUPS;")->execute();
            $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=1;")->execute();
        }
        foreach ($results as $result) {
            if (!empty($this->_existingColours)) {
                if (in_array($result['colour'], $this->_existingColours)) {
                    continue;
                }
            }
            // we need to persist them all as groups in the database for each language
            $this->_sqlPMS->setTable("COLOURS_Groups");
            $insert = $this->_sqlPMS->insert()
                ->values(array(
                    'groupCode' => $result['colour'],
                    'hasImage' => 0,
                    'hex' => '#000000',
                    'enabled' => 1
                ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();
            $colourGroupId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

            $this->_sqlPMS->setTable("COLOURS");
            $insert = $this->_sqlPMS->insert()
                ->values(array(
                    'colourCode' => $result['colour'],
                    'hex' => '#000000',
                ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();
            $colourId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

            foreach ($this->_languages as $languageId) {
                $this->_sqlPMS->setTable("COLOURS_Groups_Definitions");
                $insert = $this->_sqlPMS->insert()
                    ->values(array(
                        'colourGroupId' => $colourGroupId,
                        'languageId' => $languageId,
                        'name' => $result['description']
                    ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();

                $this->_sqlPMS->setTable("COLOURS_Definitions");
                $insert = $this->_sqlPMS->insert()
                    ->values(array(
                        'colourId' => $colourId,
                        'languageId' => $languageId,
                        'colourName' => $result['description']
                    ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();
            }
            $this->_sqlPMS->setTable("COLOURS_R_GROUPS");
            $insert = $this->_sqlPMS->insert()
                ->values(array(
                    'colourId' => $colourId,
                    'colourGroupId' => $colourGroupId,
                ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();
        }
        $this->_dbAdapter->getDriver()->getConnection()->commit();

        return true;
    }

    /* END -- MIGRATE COLOURS */
    /* ======================================================================== */
    /* START -- MIGRATE MERCHANT CATEGORIES */

    /**
     * @return bool
     */
    public function migrateMerchantCategories()
    {
        $this->_sqlOldPMS->setTable('mercat5');
        $select = $this->_sqlOldPMS->select()
            ->order("CATID ASC");
        $statement = $this->_sqlOldPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $finalArray = array();
        foreach ($results as $result) {
            if (isset($finalArray[$result['PRDCAT']])) {
                $finalArray[$result['PRDCAT']][] = array(
                    'catSubId' => $result['SUBID'],
                    'catSubName' => $result['PRDCATSUB'],
                );
            } else {
                $finalArray[$result['PRDCAT']] = array(
                    array(
                        'catSubId' => $result['SUBID'],
                        'catSubName' => $result['PRDCATSUB'],
                    )
                );
            }
        }
        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
        $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=0;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Merchant_Categories;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Merchant_Categories_Definitions;")->execute();
        $this->_dbAdapter->query("TRUNCATE SKU_Rules;")->execute();
        $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=1;")->execute();
        foreach ($finalArray as $mainCategoryName => $subCategoryArray) {
            // insert a new main category
            $this->_sqlPMS->setTable("PRODUCTS_Merchant_Categories");
            $insert = $this->_sqlPMS->insert()
                ->values(array(
                    'parentId' => 0,
                    'status' => 'ACTIVE',
                ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();
            $merchantCategoryId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

            $this->_sqlPMS->setTable("SKU_Rules");
            $insert = $this->_sqlPMS->insert()
                ->values(array(
                    'merchantCategoryId' => $merchantCategoryId,
                    'merchantAttributeGroupId' => 1,
                    'position' => 1,
                ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();

            $this->_sqlPMS->setTable("SKU_Rules");
            $insert = $this->_sqlPMS->insert()
                ->values(array(
                    'merchantCategoryId' => $merchantCategoryId,
                    'merchantAttributeGroupId' => 2,
                    'position' => 2,
                ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();

            foreach ($this->_languages as $languageId) {
                $this->_sqlPMS->setTable("PRODUCTS_Merchant_Categories_Definitions");
                $insert = $this->_sqlPMS->insert()
                    ->values(array(
                        'productMerchantCategoryId' => $merchantCategoryId,
                        'languageId' => $languageId,
                        'categoryName' => $mainCategoryName,
                        'shortDescription' => $mainCategoryName,
                        'fullDescription' => $mainCategoryName
                    ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();
            }

            foreach ($subCategoryArray as $subCategory) {
                // insert a new main category
                $this->_sqlPMS->setTable("PRODUCTS_Merchant_Categories");
                $insert = $this->_sqlPMS->insert()
                    ->values(array(
                        'parentId' => $merchantCategoryId,
                        'status' => 'ACTIVE',
                        'oldCategoryId' => $subCategory['catSubId'],
                    ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();
                $subMerchantCategoryId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

                $this->_sqlPMS->setTable("SKU_Rules");
                $insert = $this->_sqlPMS->insert()
                    ->values(array(
                        'merchantCategoryId' => $subMerchantCategoryId,
                        'merchantAttributeGroupId' => 1,
                        'position' => 1,
                    ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();
                $insert = $this->_sqlPMS->insert()
                    ->values(array(
                        'merchantCategoryId' => $subMerchantCategoryId,
                        'merchantAttributeGroupId' => 2,
                        'position' => 2,
                    ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();

                foreach ($this->_languages as $languageId) {
                    $this->_sqlPMS->setTable("PRODUCTS_Merchant_Categories_Definitions");
                    $insert = $this->_sqlPMS->insert()
                        ->values(array(
                            'productMerchantCategoryId' => $subMerchantCategoryId,
                            'languageId' => $languageId,
                            'categoryName' => $subCategory['catSubName'],
                            'shortDescription' => $subCategory['catSubName'],
                            'fullDescription' => $subCategory['catSubName']
                        ));
                    $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                    $statement->execute();
                }
            }
        }
        $this->_dbAdapter->getDriver()->getConnection()->commit();

        return true;
    }

    /* END -- MIGRATE MERCHANT CATEGORIES */
    /* ======================================================================== */
    /* START -- MIGRATE MARKETING CATEGORIES */

    /**
     * @return bool
     */
    public function migrateMarketingCategories()
    {
        $categoriesTree = $this->fetchMarketingCategoriesRecursively();

        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
        $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=0;")->execute();
        $this->_dbAdapter->query("TRUNCATE CATEGORIES;")->execute();
        $this->_dbAdapter->query("TRUNCATE CATEGORIES_R_CLIENTS_Websites;")->execute();
        $this->_dbAdapter->query("TRUNCATE CATEGORIES_Definitions;")->execute();
        $this->_dbAdapter->query("TRUNCATE CATEGORIES_R_ATTRIBUTES;")->execute();
        $this->_dbAdapter->query("TRUNCATE CATEGORIES_Media;")->execute();
        $this->_dbAdapter->query("TRUNCATE CATEGORIES_Media_External;")->execute();
        $this->_dbAdapter->query("TRUNCATE CATEGORIES_Tags;")->execute();
        $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=1;")->execute();

            // insert the home category
            $this->insertMarketingCategory();
        $parentId = 1;
        $treeDepth = 1;
        foreach ($categoriesTree as $categoryArray) {
            $this->insertMarketingCategoriesRecursively($categoryArray, $parentId, $treeDepth);
        }
        $this->_dbAdapter->getDriver()->getConnection()->commit();
        $this->changeUrlForCategories();
        return true;
    }

    /**
     * @param int $parentCategoryId
     * @return array
     */
    private function fetchMarketingCategoriesRecursively($parentCategoryId = 0)
    {
        $this->_sqlOldPMS->setTable('webcat_category');
        $select = $this->_sqlOldPMS->select()
            ->where("PARENT_ID = ".$parentCategoryId)
            ->order("PARENT_ID ASC");
        $statement = $this->_sqlOldPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $finalArray = array();
        if ($results->count() == 0) {
            return $finalArray;
        } else {
            foreach ($results as $result) {
                $finalArray[$result['ID']] = array(
                    'oldId' => $result['ID'],
                    'name' => $result['NAME'],
                    'children' => $this->fetchMarketingCategoriesRecursively($result['ID'])
                );
            }
        }
        return $finalArray;
    }

    /**
     * @param $categoryArray
     * @param $parentId
     * @param $treeDepth
     */
    private function insertMarketingCategoriesRecursively($categoryArray, $parentId, $treeDepth)
    {
        $newCategoryId = $this->insertMarketingCategory($categoryArray['name'], $parentId, $treeDepth, $categoryArray['oldId']);

        if (!empty($categoryArray['children'])) {
            $treeDepth++;
            foreach ($categoryArray['children'] as $childCategoriesArray) {
                $this->insertMarketingCategoriesRecursively($childCategoriesArray, $newCategoryId, $treeDepth);
            }
        }
    }

    /**
     * @param string $categoryName
     * @param int $parentId
     * @param int $treeDepth
     * @param int $oldCategoryId
     * @return int
     */
    private function insertMarketingCategory($categoryName = 'Home',$parentId = 1, $treeDepth = 0, $oldCategoryId = 0)
    {
        $this->_sqlPMS->setTable("CATEGORIES");
        $insert = $this->_sqlPMS->insert()
            ->values(array(
                'categoryParentId' => $parentId,
                'treeDepth' => $treeDepth,
                'position' => 1,
                'status' => 'ACTIVE',
                'oldCategoryId' => $oldCategoryId,
            ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
        $statement->execute();
        $merchantCategoryId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

        $urlName = $this->getUrlName($categoryName);
        foreach ($this->_languages as $languageId) {
            $this->_sqlPMS->setTable("CATEGORIES_Definitions");
            $insert = $this->_sqlPMS->insert()
                ->values(array(
                    'categoryId' => $merchantCategoryId,
                    'languageId' => $languageId,
                    'versionId' => 1,
                    'versionDescription' => $categoryName,
                    'categoryName' => $categoryName,
                    'urlName' => $urlName,
                    'metaTitle' => $categoryName,
                    'metaDescription' => $categoryName,
                    'metaKeyword' => $categoryName,
                    'shortDescription' => $categoryName,
                    'longDescription' => $categoryName,
                    'status' => 'ACTIVE',
                    'locked' => '0',
                ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();
        }

        foreach ($this->_websites as $websiteId => $websiteName) {
            $this->_sqlPMS->setTable("CATEGORIES_R_CLIENTS_Websites");
            $insert = $this->_sqlPMS->insert()
                ->values(array(
                    'categoryId' => $merchantCategoryId,
                    'websiteId' => $websiteId
                ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();
        }

        return $merchantCategoryId;
    }

    /* END -- MIGRATE MARKETING CATEGORIES */
    /* ======================================================================== */
    /* START -- MIGRATE USERS */

    /**
     * @return bool
     */
    public function migrateUsersForOS()
    {
        $statement = $this->_oldPMSAdapter->query("SELECT * FROM (SELECT u.*, c.customerID AS customerID FROM users AS u INNER JOIN  CUSTOMER_Emails c ON u.username=c.email  ORDER BY account ASC, customerID DESC)T1 GROUP BY username ORDER BY account ASC");
        $results = $statement->execute();

        $this->_adminAdapter->getDriver()->getConnection()->beginTransaction();
        $this->_adminAdapter->query("SET FOREIGN_KEY_CHECKS=0;")->execute();
        $this->_adminAdapter->query("TRUNCATE WEBSITE_USERS;")->execute();
        $this->_adminAdapter->query("TRUNCATE WEBSITE_USERS_Addresses;")->execute();
        $this->_adminAdapter->query("TRUNCATE WEBSITE_USERS_R_CLIENTS_Websites;")->execute();
        $this->_adminAdapter->query("SET FOREIGN_KEY_CHECKS=1;")->execute();

        foreach ($results as $result) {
            if (empty($result['customerID'])) {
                continue;
            }
            // create the user
            $this->_sqlAdmin->setTable("WEBSITE_USERS");
            $insert = $this->_sqlAdmin->insert()
                ->values(array(
                    'spectrumId' => $result['customerID'],
                    'userTypeId' => 3,
                    'firstName' => $result['fname'],
                    'lastName' => $result['sname'],
                    'userName' => $result['username'],
                    'emailAddress' => $result['username'],
                    'passwordSalt' => '',
                    'password' => '',
                    'status' => 'ACTIVE',
                    'created' => $result['created'],
                    'oldPassword' => $result['password'],
                ));
            $statement = $this->_sqlAdmin->prepareStatementForSqlObject($insert);
            $statement->execute();
            $userId = $this->_adminAdapter->getDriver()->getConnection()->getLastGeneratedValue();

            foreach ($this->_websites as $websiteId => $websiteName) {
                $this->_sqlPMS->setTable("WEBSITE_USERS_R_CLIENTS_Websites");
                $insert = $this->_sqlPMS->insert()
                    ->values(array(
                        'userId' => $userId,
                        'websiteId' => $websiteId,
                        'status' => 'ACTIVE',
                        'created' => date('Y-m-d H:i:s'),
                    ));
                $statement = $this->_sqlAdmin->prepareStatementForSqlObject($insert);
                $statement->execute();
            }
        }
        $this->_adminAdapter->getDriver()->getConnection()->commit();
        return true;
    }

    /**
     * @return bool
     */
    public function migrateUsersForNauticalia()
    {
        ini_set('max_execution_time', 0);
        $statement = $this->_oldPMSAdapter->query("SELECT * FROM users ORDER by account ASC");
        $results = $statement->execute();

        $this->_adminAdapter->getDriver()->getConnection()->beginTransaction();
        $this->_adminAdapter->query("SET FOREIGN_KEY_CHECKS=0;")->execute();
        $this->_adminAdapter->query("TRUNCATE WEBSITE_USERS;")->execute();
        $this->_adminAdapter->query("TRUNCATE WEBSITE_USERS_Addresses;")->execute();
        $this->_adminAdapter->query("TRUNCATE WEBSITE_USERS_R_CLIENTS_Websites;")->execute();
        $this->_adminAdapter->query("SET FOREIGN_KEY_CHECKS=1;")->execute();

        $header = 'Title,First Name,Last Name,Company,Username,How did you hear about us,DOB,Phone,Mobile,Fax,Address1,Address2,Town,County,Postcode,Country,Created';
        file_put_contents('/tmp/not_inserted_record', $header);
        $notInserted = 0 ;
        foreach ($results as $result) {
            $spectrumId = 0;
            if (!empty($result['username']) && filter_var($result['username'], FILTER_VALIDATE_EMAIL)) {
                try {
                    $statement = $this->_spectrumAdapter->query('SELECT * FROM CUSTOMER_Emails WHERE email = "'.$result["username"].'";');
                    $spectrumResults = $statement->execute();
                    if ($spectrumResults->count() > 0) {
                        $spectrumId = $spectrumResults->next()['customerID'];
                    }
                    // create the user
                    $this->_sqlAdmin->setTable("WEBSITE_USERS");
                    $insert = $this->_sqlAdmin->insert()
                        ->values(array(
                            'spectrumId' => $spectrumId,
                            'userTypeId' => 3,
                            'firstName' => $result['fname'],
                            'lastName' => $result['sname'],
                            'userName' => $result['username'],
                            'emailAddress' => $result['username'],
                            'passwordSalt' => '',
                            'password' => '',
                            'status' => 'ACTIVE',
                            'created' => $result['created'],
                            'oldPassword' => $result['password'],
                        ));
                    $statement = $this->_sqlAdmin->prepareStatementForSqlObject($insert);
                    $statement->execute();
                    $userId = $this->_adminAdapter->getDriver()->getConnection()->getLastGeneratedValue();

                    $this->_sqlAdmin->setTable("WEBSITE_USERS_Addresses");
                    $insert = $this->_sqlAdmin->insert()
                        ->values(array(
                            'userId' => $userId,
                            'addressType' => 'BILLING',
                            'status' => 'CURRENT',
                            'addressLine1' => $result['add1'],
                            'addressLine2' => $result['add2'],
                            'city' => $result['town'],
                            'county' => $result['county'],
                            'postcode' => $result['postcode'],
                            'countryCode' => '',
                            'title' => $result['title'],
                            'phone' => $result['phone'],
                        ));
                    $statement = $this->_sqlAdmin->prepareStatementForSqlObject($insert);
                    $statement->execute();

                    foreach ($this->_websites as $websiteId => $websiteName) {
                        $this->_sqlPMS->setTable("WEBSITE_USERS_R_CLIENTS_Websites");
                        $insert = $this->_sqlPMS->insert()
                            ->values(array(
                                'userId' => $userId,
                                'websiteId' => $websiteId,
                                'status' => 'ACTIVE',
                                'created' => date('Y-m-d H:i:s'),
                            ));
                        $statement = $this->_sqlAdmin->prepareStatementForSqlObject($insert);
                        $statement->execute();
                    }
                } catch (\Exception $ex) {
                    $notInsertedEntry = (!empty($result['title']) ? $result['title'] : '-')  . ',' .
                                        (!empty($result['fname']) ? $result['fname'] : '-'). ',' .
                                        (!empty($result['sname']) ? $result['sname'] : '-'). ',' .
                                        (!empty($result['company']) ? $result['company'] : '-'). ',' .
                                        (!empty($result['username']) ? $result['username'] : '-'). ',' .
                                        (!empty($result['howhear']) ? $result['howhear'] : '-'). ',' .
                                        (!empty($result['dob']) ? $result['dob'] : '-'). ',' .
                                        (!empty($result['phone']) ? $result['phone'] : '-'). ',' .
                                        (!empty($result['mobile']) ? $result['mobile'] : '-'). ',' .
                                        (!empty($result['fax']) ? $result['fax'] : '-'). ',' .
                                        (!empty($result['add1']) ? $result['add1'] : '-'). ',' .
                                        (!empty($result['add2']) ? $result['add2'] : '-'). ',' .
                                        (!empty($result['town']) ? $result['town'] : '-'). ',' .
                                        (!empty($result['county']) ? $result['county'] : '-'). ',' .
                                        (!empty($result['postcode']) ? $result['postcode'] : '-'). ',' .
                                        (!empty($result['country']) ? $result['country'] : '-'). ',' .
                                        (!empty($result['created']) ? $result['created'] : '-');

                    file_put_contents('/tmp/not_inserted_record', $notInsertedEntry . PHP_EOL, FILE_APPEND);

//                    $notInserted++;
//                    file_put_contents('/tmp/not_inserted_record', 'Not Inserted So Far: ' . $notInserted . PHP_EOL . PHP_EOL . PHP_EOL, FILE_APPEND);
                    continue;
                }
            } else {
                $notInsertedEntry = (!empty($result['title']) ? $result['title'] : '-')  . ',' .
                                    (!empty($result['fname']) ? $result['fname'] : '-'). ',' .
                                    (!empty($result['sname']) ? $result['sname'] : '-'). ',' .
                                    (!empty($result['company']) ? $result['company'] : '-'). ',' .
                                    (!empty($result['username']) ? $result['username'] : '-'). ',' .
                                    (!empty($result['howhear']) ? $result['howhear'] : '-'). ',' .
                                    (!empty($result['dob']) ? $result['dob'] : '-'). ',' .
                                    (!empty($result['phone']) ? $result['phone'] : '-'). ',' .
                                    (!empty($result['mobile']) ? $result['mobile'] : '-'). ',' .
                                    (!empty($result['fax']) ? $result['fax'] : '-'). ',' .
                                    (!empty($result['add1']) ? $result['add1'] : '-'). ',' .
                                    (!empty($result['add2']) ? $result['add2'] : '-'). ',' .
                                    (!empty($result['town']) ? $result['town'] : '-'). ',' .
                                    (!empty($result['county']) ? $result['county'] : '-'). ',' .
                                    (!empty($result['postcode']) ? $result['postcode'] : '-'). ',' .
                                    (!empty($result['country']) ? $result['country'] : '-'). ',' .
                                    (!empty($result['created']) ? $result['created'] : '-');

                file_put_contents('/tmp/not_inserted_record', $notInsertedEntry . PHP_EOL, FILE_APPEND);
//                $notInserted++;
//                file_put_contents('/tmp/not_inserted_record', 'Not Inserted So Far: ' . $notInserted . PHP_EOL . PHP_EOL . PHP_EOL, FILE_APPEND);
            }
        }

        $this->_adminAdapter->getDriver()->getConnection()->commit();
        return true;
    }

    /* END -- MIGRATE USERS */
    /* ======================================================================== */
    /* START -- MIGRATE PRODUCTS */

    /**
     * @return array
     * @throws \Exception
     */
    public function migrateProducts()
    {
        $finalServiceResponse = array('messages' => array());
        ini_set('max_execution_time', 1000);
        $this->_sqlOldPMS->setTable(array('P' => 'proddata'));
        $select = $this->_sqlOldPMS->select()
            ->columns(array(
                'variantCount' => new \Zend\Db\Sql\Expression('COUNT(PRODNO)'),
                'prodNo' => 'FPRODNO',
                'style' => 'PRODNO',
                'colour' => 'COLOR'
            ))
            ->join(
                array('M' => 'migration_table'),
                "M.sku=P.FPRODNO",
                array()
            )
            ->where('P.DELETED = 0')
            ->group('P.PRODNO')
            ->order("P.ID ASC");
        $statement = $this->_sqlOldPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
        $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=0;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_ATTRIBUTES_Combination;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_ATTRIBUTES_Combination_Definitions;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Barcodes;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Cross_Border;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Definitions;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Locations;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Media;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Media_External;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Mis_Spells;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Prices;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_R_ATTRIBUTES;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_R_ATTRIBUTES_R_SKU_Rules;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_R_CATEGORIES;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_R_CHANNELS;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_R_CLIENTS_Websites;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_R_PRODUCTS;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_R_SPECTRUM;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_R_SUPPLIERS;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_R_TAXES;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Tabs;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Tags;")->execute();
        $this->_dbAdapter->query("TRUNCATE MEDIA_Products;")->execute();
        $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=1;")->execute();

        foreach ($results as $result) {
            // check if product is a single level product
            if ($result['variantCount'] == 1) {
                // single level product
                $this->_sqlOldPMS->setTable(array('p' => 'proddata'));
                $select = $this->_sqlOldPMS->select()
                    ->where(array(
                        'p.DELETED = 0',
                        'p.PRODNO = "' . $result['style'] . '"'
                    ))
                    ->join(
                        array('M' => 'migration_table'),
                        "M.sku=p.FPRODNO",
                        array()
                    )
                    ->group("p.PRODNO")
                    ->order("ID ASC");
                $statement = $this->_sqlOldPMS->prepareStatementForSqlObject($select);
                $productVariation = $statement->execute()->next();

                // need to check if the product has only one variant or it is truly a single level product
                switch ($result['colour']) {
                    case '0NO':
                    case 'NOC':
                    case 'XXX':
                        // it is a single level product
                        $prodStatus = ($productVariation['ENABLED'] == 0) ? 'INACTIVE' : 'ACTIVE';
                        $mainProductId = $this->insertProductDetails($productVariation, $prodStatus);
                        $this->insertProductPrices($productVariation, $mainProductId);
                        $this->insertProductCategories($productVariation, $mainProductId, 0, false);
                        $this->insertSpectrumProduct($productVariation, $mainProductId);
                        break;
                    default:
                        // it is a product with only one variant
                        $mainProductId = $this->insertProductDetails($productVariation);
                        $this->insertProductPrices($productVariation, $mainProductId);
                        $this->insertProductCategories($productVariation, $mainProductId, 0, false);
                        $defaultProduct = 1;
                        // do the actions for variant
                        $variantId = $this->insertVariant($productVariation, $mainProductId, $defaultProduct);
                        $this->insertProductPrices($productVariation, $variantId, 1);
                        $this->insertSkuRules($productVariation, $variantId);
                        $this->insertSpectrumProduct($productVariation, $variantId, 1);
                        break;
                }
            } else {
                // it is a product that has variants
                // need to get all variants and run the actions for each of them
                $this->_sqlOldPMS->setTable(array('p' => 'proddata'));
                $select = $this->_sqlOldPMS->select()
                    ->where(array(
                        'p.DELETED = 0',
                        'p.PRODNO = "' . $result['style'] . '"'
                    ))
                    ->join(
                        array('M' => 'migration_table'),
                        "M.sku=p.FPRODNO",
                        array()
                    )
                    ->order("ID ASC");
                $statement = $this->_sqlOldPMS->prepareStatementForSqlObject($select);
                $productVariations = $statement->execute();

                $count = 1;
                $mainProductId = false;
                $categoriesArray = array();
                $firstVariantCategoriesArray = array();
                $variantIds = array();
                foreach ($productVariations as $productVariation) {
                    if ($count == 1) {
                        // actions for main product
                        $mainProductId = $this->insertProductDetails($productVariation);
                        $this->insertProductPrices($productVariation, $mainProductId);
                        $this->insertProductCategories($productVariation, $mainProductId, 0, false);
                        $defaultProduct = 1;
                        $count++;
                    } else {
                        $defaultProduct = 0;
                    }
                    // do the actions for each variant
                    $variantId = $this->insertVariant($productVariation, $mainProductId, $defaultProduct);
                    $variantIds[] = $variantId;
                    $categoryIdsArray = $this->insertProductCategories($productVariation, $variantId, 1, true);
                    if (empty($categoriesArray)) {
                        $categoriesArray = $categoryIdsArray;
                        $firstVariantCategoriesArray = $categoryIdsArray;
                    } else {
                        $categoriesArray = array_intersect($categoriesArray, $categoryIdsArray);
                    }
                    $this->insertProductPrices($productVariation, $variantId, 1);
                    $this->insertSkuRules($productVariation, $variantId);
                    $this->insertSpectrumProduct($productVariation, $variantId, 1);
                }

                if (empty($categoriesArray)) {
                    // we need to fetch the highest entry in $firstVariantCategoriesArray and insert it as a default one
                    if (empty($firstVariantCategoriesArray)) {
                        $finalServiceResponse['messages'][] = "Could not identify the default category for the style: ".$result['style'];
                        $this->_dbAdapter->getDriver()->getConnection()->rollback();
                        return $finalServiceResponse;
                    }
                    $defaultCategoryId = max($firstVariantCategoriesArray);
                } else {
                    // we need to mark the highest entry in the intersected array as the default category
                    $defaultCategoryId = max($categoriesArray);
                }
                $this->setDefaultCategoryIdForVariantsAndProduct($mainProductId, $variantIds, $defaultCategoryId);
            }
        }

        $this->_sqlPMS->setTable(array('p' => 'PRODUCTS'));
        $select = $this->_sqlPMS->select()
            ->order("productId ASC");
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $products = $statement->execute();

        $this->_sqlPMS->setTable(array('c' => 'CHANNELS'));
        $select = $this->_sqlPMS->select()
            ->where('status = "ACTIVE"');
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $channelsResult = $statement->execute();
        $channels = array();
        foreach ($channelsResult as $channelResult) {
            $channels[] = $channelResult['channelId'];
        }

        foreach ($products as $product) {
            foreach ($channels as $channelId) {
                $this->_sqlPMS->setTable("PRODUCTS_R_CHANNELS");
                $insert = $this->_sqlPMS->insert()
                    ->values(array(
                        'productId' => $product['productId'],
                        'channelId' => $channelId,
                        'status' => 'ACTIVE',
                        'created' => date('Y-m-d H:i:s'),
                    ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();
            }
            foreach ($this->_websites as $websiteId => $websiteName) {
                $this->_sqlPMS->setTable("PRODUCTS_R_CLIENTS_Websites");
                $insert = $this->_sqlPMS->insert()
                    ->values(array(
                        'productIdFrom' => 'PRODUCTS',
                        'productId' => $product['productId'],
                        'websiteId' => $websiteId,
                    ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();
            }
        }

        $this->populateBarcodesTable();
        $this->_dbAdapter->getDriver()->getConnection()->commit();
        return $finalServiceResponse;
    }

    /**
     * @param $productDetails
     * @param string $status
     * @return int
     */
    private function insertProductDetails($productDetails, $status = 'INACTIVE')
    {
        $oldMerchantCategoryId = $productDetails['PSUBID'];
        $this->_sqlPMS->setTable('PRODUCTS_Merchant_Categories');
        $select = $this->_sqlPMS->select()
            ->where(array(
                'oldCategoryId = '.$oldMerchantCategoryId
            ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $newMerchantCategoryId = $statement->execute()->next()['productMerchantCategoryId'];

        $this->_sqlOldPMS->setTable('consolid_info');
        $select = $this->_sqlOldPMS->select()
            ->where("consolid LIKE '".$productDetails['PRODNO']."%'");
        $statement = $this->_sqlOldPMS->prepareStatementForSqlObject($select);
        $oldProductDetails = $statement->execute()->next();

        $this->_sqlPMS->setTable("PRODUCTS");
        $insert = $this->_sqlPMS->insert()
            ->values(array(
                'manufacturerId' => 1,
                'productTypeId' => 1,
                'productMerchantCategoryId' => $newMerchantCategoryId,
                'taxId' => 0,
                'ean13' => $productDetails['BARCODE'],
                'style' => $productDetails['PRODNO'],
                'sku' => $productDetails['FPRODNO'],
                'supplierReference' => $productDetails['SUPACNO'],
                'weight' => $productDetails['ITEMWT'],
                'allowBackOrder' => $productDetails['BACKORDERS'],
                'customizable' => isset($productDetails['PERSSTYLE']) ? 1 : 0,
                'status' => $status,
            ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
        $statement->execute();
        $productID = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

        $productName = $this->processProductName($oldProductDetails['grpdescrip']);
        $urlName = $this->getUrlName($productName);
        foreach ($this->_languages as $languageId) {
            $productDetailsArray = array(
                'productOrVariantId' => $productID,
                'productName' => $productName,
                'urlName' => $urlName,
                'shortDescription' => $productDetails['REPDESC'],
                'longDescription' => $oldProductDetails['copytext_top'],
            );
            $this->insertProductOrVariantDefinitions($productDetailsArray, $languageId);
        }
        return $productID;
    }

    /**
     * @param $productDetailsArray
     * @param $languageId
     * @param int $isVariant
     */
    private function insertProductOrVariantDefinitions($productDetailsArray, $languageId, $isVariant = 0)
    {
        if (empty($productDetailsArray['productName'])) {
            $productDetailsArray['productName'] = $productDetailsArray['shortDescription'];
        }
        if (empty($productDetailsArray['longDescription'])) {
            $productDetailsArray['longDescription'] = $productDetailsArray['shortDescription'];
        }
        if (empty($productDetailsArray['urlName'])) {
            $productDetailsArray['urlName'] = $productDetailsArray['shortDescription'];
        }
        $this->_sqlPMS->setTable("PRODUCTS_Definitions");
        $insert = $this->_sqlPMS->insert()
            ->values(array(
                'productId' => $productDetailsArray['productOrVariantId'],
                'productIdFrom' => ($isVariant == 0) ? 'PRODUCTS' : 'PRODUCTS_R_ATTRIBUTES',
                'languageId' => $languageId,
                'versionId' => 1,
                'shortProductName' => $productDetailsArray['shortDescription'],
                'productName' => $productDetailsArray['productName'],
                'urlName' => $productDetailsArray['urlName'],
                'metaTitle' => $productDetailsArray['productName'],
                'metaDescription' => $productDetailsArray['productName'],
                'metaKeyword' => str_replace(' ', ',', $productDetailsArray['productName']),
                'shortDescription' => $productDetailsArray['shortDescription'],
                'longDescription' => $productDetailsArray['longDescription'],
                'makeLive' => 1,
                'status' => 'ACTIVE',
                'created' => date('Y-m-d H:i:s'),
            ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
        $statement->execute();
    }

    /**
     * @param $mainProductId
     * @param $variantIds
     * @param $defaultCategoryId
     */
    private function setDefaultCategoryIdForVariantsAndProduct($mainProductId, $variantIds, $defaultCategoryId)
    {
        $this->_sqlPMS->setTable("PRODUCTS_R_CATEGORIES");
        $select = $this->_sqlPMS->select()
            ->where("categoryId = '$defaultCategoryId'")
            ->where("productIdFrom = 'PRODUCTS'")
            ->where("productId = '$mainProductId'");
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() > 0) {
            // we have this category for this product, we just need to update the default flag
            $productCategoryId = $results->next()['productCategoryId'];
            $update = $this->_sqlPMS->update();
            $update->table('PRODUCTS_R_CATEGORIES');
            $update->set(array('isDefault' => 1));
            $update->where(array(
                'productCategoryId' => $productCategoryId,
            ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($update);
            $statement->execute();
        } else {
            // we don;t have this category for this product, we will need to insert is as default
            $insert = $this->_sqlPMS->insert()
                ->values(array(
                    'productId' => $mainProductId,
                    'productIdFrom' => 'PRODUCTS',
                    'priority' => 1,
                    'categoryId' => $defaultCategoryId,
                    'isDefault' => 1,
                    'status' => 'ACTIVE',
                    'created' => date('Y-m-d H:i:s'),
                ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();
        }

        foreach ($variantIds as $variantId) {
            $this->_sqlPMS->setTable("PRODUCTS_R_CATEGORIES");
            $select = $this->_sqlPMS->select()
                ->where("categoryId = '$defaultCategoryId'")
                ->where("productIdFrom = 'PRODUCTS_R_ATTRIBUTES'")
                ->where("productId = '$variantId'");
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            if ($results->count() > 0) {
                // we have this category for this product, we just need to update the default flag
                $productCategoryId = $results->next()['productCategoryId'];
                $update = $this->_sqlPMS->update();
                $update->table('PRODUCTS_R_CATEGORIES');
                $update->set(array('isDefault' => 1));
                $update->where(array(
                    'productCategoryId' => $productCategoryId,
                ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($update);
                $statement->execute();
            } else {
                // we don;t have this category for this product, we will need to insert is as default
                $insert = $this->_sqlPMS->insert()
                    ->values(array(
                        'productId' => $variantId,
                        'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                        'priority' => 1,
                        'categoryId' => $defaultCategoryId,
                        'isDefault' => 1,
                        'status' => 'ACTIVE',
                        'created' => date('Y-m-d H:i:s'),
                    ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();
            }
        }
    }

    /**
     * @param $productName
     * @return mixed|string
     */
    private function processProductName($productName)
    {
        $productName = strtolower(preg_replace('/[\n|\n\r]+/', '', $productName));
        $this->_sqlPMS->setTable('COLOURS_Groups_Definitions');
        $select = $this->_sqlPMS->select()
            ->where(array(
                'languageId = 1'
            ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $coloursNames = $statement->execute();
        foreach ($coloursNames as $coloursName) {
            $productName = str_replace(strtolower($coloursName['name']), '', $productName);
        }

        $productName = ucwords(preg_replace('/\s+/', ' ', $productName));
        return $productName;
    }

    /**
     * @param $nameToConvert
     * @return string
     */
    private function getUrlName($nameToConvert)
    {
        return strtolower(trim(preg_replace('/-+/','-', preg_replace('/[^A-Za-z0-9]/', '-', $nameToConvert)), '-'));
    }

    /**
     * @param $productVariation
     * @param $productOrVariantId
     * @param int $isVariant
     */
    private function insertProductPrices($productVariation, $productOrVariantId, $isVariant = 0)
    {
        $nowPrice = 0.00;
        $wasPrice = 0.00;
        $costPrice = 0.00;
        $tradingCostPrice = 0.00;
        $wholesalePrice = 0.00;
        $freightPrice = 0.00;
        $designPrice = 0.00;
        $dutyPrice = 0.00;
        $packagingPrice = 0.00;

        foreach ($this->_currencies as $currencyId) {
            switch ($currencyId) {
                case 1:
                    $nowPrice = !empty($productVariation['SCOST']) ? $productVariation['SCOST'] : 0.00;
                    $wasPrice = !empty($productVariation['OLDSCOST']) ? $productVariation['OLDSCOST'] : 0.00;
                    $costPrice = !empty($productVariation['CCOST']) ? $productVariation['CCOST'] : 0.00;
                    $tradingCostPrice = !empty($productVariation['TCCOST']) ? $productVariation['TCCOST'] : 0.00;
                    $wholesalePrice = !empty($productVariation['TCCOST']) ? $productVariation['TCCOST'] : 0.00;
                    $freightPrice = !empty($productVariation['FREIGHT']) ? $productVariation['FREIGHT'] : 0.00;
                    $designPrice = !empty($productVariation['DESIGN']) ? $productVariation['DESIGN'] : 0.00;
                    $dutyPrice = !empty($productVariation['DUTY']) ? $productVariation['DUTY'] : 0.00;
                    $packagingPrice = !empty($productVariation['PACKAGING']) ? $productVariation['PACKAGING'] : 0.00;
                    break;
                case 2:
                    $nowPrice = !empty($productVariation['SCOST2']) ? $productVariation['SCOST2'] : 0.00;
                    $wasPrice = !empty($productVariation['OLDSCOST2']) ? $productVariation['OLDSCOST2'] : 0.00;
                    $costPrice = !empty($productVariation['CCOST']) ? $productVariation['CCOST'] : 0.00;
                    $tradingCostPrice = !empty($productVariation['TCCOST']) ? $productVariation['TCCOST'] : 0.00;
                    $wholesalePrice = !empty($productVariation['TCCOST']) ? $productVariation['TCCOST'] : 0.00;
                    $freightPrice = !empty($productVariation['FREIGHT']) ? $productVariation['FREIGHT'] : 0.00;
                    $designPrice = !empty($productVariation['DESIGN']) ? $productVariation['DESIGN'] : 0.00;
                    $dutyPrice = !empty($productVariation['DUTY']) ? $productVariation['DUTY'] : 0.00;
                    $packagingPrice = !empty($productVariation['PACKAGING']) ? $productVariation['PACKAGING'] : 0.00;
                    break;
                case 3:
                    $nowPrice = !empty($productVariation['SCOST3']) ? $productVariation['SCOST3'] : 0.00;
                    $wasPrice = !empty($productVariation['OLDSCOST3']) ? $productVariation['OLDSCOST3'] : 0.00;
                    $costPrice = !empty($productVariation['CCOST']) ? $productVariation['CCOST'] : 0.00;
                    $tradingCostPrice = !empty($productVariation['TCCOST']) ? $productVariation['TCCOST'] : 0.00;
                    $wholesalePrice = !empty($productVariation['TCCOST']) ? $productVariation['TCCOST'] : 0.00;
                    $freightPrice = !empty($productVariation['FREIGHT']) ? $productVariation['FREIGHT'] : 0.00;
                    $designPrice = !empty($productVariation['DESIGN']) ? $productVariation['DESIGN'] : 0.00;
                    $dutyPrice = !empty($productVariation['DUTY']) ? $productVariation['DUTY'] : 0.00;
                    $packagingPrice = !empty($productVariation['PACKAGING']) ? $productVariation['PACKAGING'] : 0.00;
                    break;
            }
            $this->_sqlPMS->setTable("PRODUCTS_Prices");
            $insert = $this->_sqlPMS->insert()
                ->values(array(
                    'productIdFrom' => ($isVariant == 0) ? 'PRODUCTS' : 'PRODUCTS_R_ATTRIBUTES',
                    'productOrVariantId' => $productOrVariantId,
                    'regionId' => 1,
                    'currencyId' => $currencyId,
                    'price' => $productVariation['SCOST'],
                    'nowPrice' => $nowPrice,
                    'wasPrice' => $wasPrice,
                    'costPrice' => $costPrice,
                    'tradingCostPrice' => $tradingCostPrice,
                    'wholesalePrice' => $wholesalePrice,
                    'freightPrice' => $freightPrice,
                    'dutyPrice' => $dutyPrice,
                    'designPrice' => $designPrice,
                    'packagingPrice' => $packagingPrice,
                    'status' => 'ACTIVE',
                    'created' => date('Y-m-d H:i:s'),
                ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();
        }
    }

    /**
     * @param $productVariation
     * @param $productOrVariantId
     * @param int $isVariant
     * @param bool $postponeDefaultCategorySetup
     * @return array
     */
    private function insertProductCategories($productVariation, $productOrVariantId, $isVariant = 0, $postponeDefaultCategorySetup = false)
    {
        $this->_sqlOldPMS->setTable('webcat');
        $select = $this->_sqlOldPMS->select()
            ->where("PRODUCTID = '".$productVariation['ID']."'");
        $statement = $this->_sqlOldPMS->prepareStatementForSqlObject($select);
        $responses = $statement->execute();

        $count = 1;
        $categoryIdsArray = array();
        if ($responses->count() == 0) {
            $this->_sqlPMS->setTable("PRODUCTS_R_CATEGORIES");
            $insert = $this->_sqlPMS->insert()
                ->values(array(
                    'productId' => $productOrVariantId,
                    'productIdFrom' => ($isVariant == 0) ? 'PRODUCTS' : 'PRODUCTS_R_ATTRIBUTES',
                    'priority' => 1,
                    'categoryId' => 2,
                    'isDefault' => $postponeDefaultCategorySetup ? 0 : 1,
                    'status' => 'ACTIVE',
                    'created' => date('Y-m-d H:i:s'),
                ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();
            $categoryIdsArray[] = 2;
        } else {
            foreach ($responses as $response) {
                if ($count == $responses->count()) {
                    $default = !$postponeDefaultCategorySetup ? 0 : 1;
                } else {
                    $default = 0;
                }
                // get new category id from old categoryId
                $this->_sqlPMS->setTable('CATEGORIES');
                $select = $this->_sqlPMS->select()
                    ->where(array(
                        'oldCategoryId = "' . $response['CATEGORY'] . '"'
                    ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
                $categoryId = $statement->execute()->next()['categoryId'];
                $categoryIdsArray[] = $categoryId;
                $this->_sqlPMS->setTable("PRODUCTS_R_CATEGORIES");
                $insert = $this->_sqlPMS->insert()
                    ->values(array(
                        'productId' => $productOrVariantId,
                        'productIdFrom' => ($isVariant == 0) ? 'PRODUCTS' : 'PRODUCTS_R_ATTRIBUTES',
                        'priority' => 1,
                        'categoryId' => $categoryId,
                        'isDefault' => $default,
                        'status' => 'ACTIVE',
                        'created' => date('Y-m-d H:i:s'),
                    ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();
                $count++;
            }
        }
        return $categoryIdsArray;
    }

    /**
     * @param $productVariation
     * @param $mainProductId
     * @param $defaultProduct
     * @return int
     */
    private function insertVariant($productVariation, $mainProductId, $defaultProduct)
    {
        $variantStatus = ($productVariation['ENABLED'] == 1) ? 'ACTIVE' : 'INACTIVE';
        $this->_sqlPMS->setTable("PRODUCTS_R_ATTRIBUTES");
        $insert = $this->_sqlPMS->insert()
            ->values(array(
                'productId' => $mainProductId,
                'manufacturerId' => 1,
                'ean13' => $productVariation['BARCODE'],
                'sku' => $productVariation['FPRODNO'],
                'supplierReference' => $productVariation['SUPACNO'],
                'status' => $variantStatus,
                'isDefaultProduct' => $defaultProduct,
                'created' => date('Y-m-d H:i:s'),
            ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
        $statement->execute();
        $variantID = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

        $this->_sqlOldPMS->setTable('consolid_info');
        $select = $this->_sqlOldPMS->select()
            ->where("consolid LIKE '".$productVariation['PRODNO']."%'");
        $statement = $this->_sqlOldPMS->prepareStatementForSqlObject($select);
        $oldProductDetails = $statement->execute()->next();

        $productName = $this->processProductName($oldProductDetails['grpdescrip']);
        $urlName = $this->getUrlName($productName);
        foreach ($this->_languages as $languageId) {
            $productDetailsArray = array(
                'productOrVariantId' => $variantID,
                'productName' => $productName,
                'urlName' => $urlName,
                'shortDescription' => $productVariation['REPDESC'],
                'longDescription' => $oldProductDetails['copytext_top'],
            );
            $this->insertProductOrVariantDefinitions($productDetailsArray, $languageId, 1);
        }
        if ($variantStatus == 'ACTIVE') {
            // activate the main product
            $update = $this->_sqlPMS->update();
            $update->table('PRODUCTS');
            $update->set(array('status' => 'ACTIVE'));
            $update->where(array(
                'productId' => $mainProductId,
            ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($update);
            $statement->execute();
        }
        return $variantID;
    }

    /**
     * @param $productVariation
     * @param $variantId
     * @throws \Exception
     */
    private function insertSkuRules($productVariation, $variantId) {
        // will always have colour so bind it to the product
        $this->_sqlPMS->setTable(array('cg' => 'COLOURS_Groups'));
        $select = $this->_sqlPMS->select()
            ->where(array(
                "groupCode = '$productVariation[COLOR]'"
            ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $colourGroupId = $statement->execute()->next()['colourGroupId'];

        if ($colourGroupId === null) {
            throw new \Exception('Could not find color: '. $productVariation['COLOR']);
        }

        $this->_sqlPMS->setTable("PRODUCTS_R_ATTRIBUTES_R_SKU_Rules");
        $insert = $this->_sqlPMS->insert()
            ->values(array(
                'productAttributeId' => $variantId,
                'skuRuleId' => $colourGroupId,
                'skuRuleTable' => 'COLOURS_Groups',
                'created' => date('Y-m-d H:i:s'),
            ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
        $statement->execute();

        if (in_array($productVariation['PRODNO'], $this->_womenStyles)) {
            // accessories sizes
            $this->_sqlPMS->setTable("PRODUCTS_R_ATTRIBUTES_R_SKU_Rules");
            $insert = $this->_sqlPMS->insert()
                ->values(array(
                    'productAttributeId' => $variantId,
                    'skuRuleId' => $this->womenShoesSizes[trim($productVariation["SIZE"])],
                    'skuRuleTable' => 'SIZES_Shoes',
                    'created' => date('Y-m-d H:i:s'),
                ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();
        } else {
            //will need to determine what size table to use
            switch (trim($productVariation['SIZE'])) {
                case '012':
                case '023':
                case '034':
                case '001':
                    // accessories sizes
                    $this->_sqlPMS->setTable("PRODUCTS_R_ATTRIBUTES_R_SKU_Rules");
                    $insert = $this->_sqlPMS->insert()
                        ->values(array(
                            'productAttributeId' => $variantId,
                            'skuRuleId' => $this->accessoriesSizeMapping[trim($productVariation["SIZE"])],
                            'skuRuleTable' => 'SIZES_Accessories',
                            'created' => date('Y-m-d H:i:s'),
                        ));
                    $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                    $statement->execute();
                    break;
                case '030':
                case '035':
                case '040':
                case '045':
                case '050':
                case '055':
                case '060':
                case '065':
                case '070':
                case '075':
                case '080':
                case '085':
                case '090':
                case '095':
                case '100':
                case '105':
                case '110':
                case '115':
                case '120':
                case '125':
                case '130':
                case '135':
                case '140':
                    // shoes sizes
                    $this->_sqlPMS->setTable("PRODUCTS_R_ATTRIBUTES_R_SKU_Rules");
                    $insert = $this->_sqlPMS->insert()
                        ->values(array(
                            'productAttributeId' => $variantId,
                            'skuRuleId' => $this->shoesSizeMapping[trim($productVariation["SIZE"])],
                            'skuRuleTable' => 'SIZES_Shoes',
                            'created' => date('Y-m-d H:i:s'),
                        ));
                    $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                    $statement->execute();
                    break;
                case '002':
                case '003':
                case '004':
                case '005':
                case '006':
                case '007':
                    // clothes sizes
                    $this->_sqlPMS->setTable("PRODUCTS_R_ATTRIBUTES_R_SKU_Rules");
                    $insert = $this->_sqlPMS->insert()
                        ->values(array(
                            'productAttributeId' => $variantId,
                            'skuRuleId' => $this->clothesSizeMapping[trim($productVariation["SIZE"])],
                            'skuRuleTable' => 'SIZES_Clothes',
                            'created' => date('Y-m-d H:i:s'),
                        ));
                    $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                    $statement->execute();
                    break;
                case '150':
                case '155':
                case '160':
                case '165':
                case '170':
                    // shirts sizes
                    $this->_sqlPMS->setTable("PRODUCTS_R_ATTRIBUTES_R_SKU_Rules");
                    $insert = $this->_sqlPMS->insert()
                        ->values(array(
                            'productAttributeId' => $variantId,
                            'skuRuleId' => $this->shirtsSizeMapping[trim($productVariation["SIZE"])],
                            'skuRuleTable' => 'SIZES_Shirts',
                            'created' => date('Y-m-d H:i:s'),
                        ));
                    $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                    $statement->execute();
                    break;
                default:
                    // do not set up a size for this product
                    break;
            }
        }
    }

    /**
     * @param $productVariation
     * @param $productOrVariantId
     * @param int $isVariant
     */
    private function insertSpectrumProduct($productVariation, $productOrVariantId, $isVariant = 0)
    {
        $this->_sqlPMS->setTable("PRODUCTS_R_SPECTRUM");
        $insert = $this->_sqlPMS->insert()
            ->values(array(
                'productIdFrom' => ($isVariant == 0) ? 'PRODUCTS' : 'PRODUCTS_R_ATTRIBUTES',
                'productId' => $productOrVariantId,
                'spectrumId' => $productVariation['SPECTRUMID'],
                'style' => $productVariation['PRODNO'],
                'created' => date('Y-m-d H:i:s'),
            ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
        $statement->execute();
    }

    /**
     *
     */
    private function populateBarcodesTable()
    {
        $query = "INSERT INTO PRODUCTS_Barcodes (`barcode`, `inUse`, `created`, `modified`)
            SELECT ean13, 1, NOW(), NOW() FROM
            (
                SELECT ean13 FROM PRODUCTS
                UNION
                SELECT ean13 FROM PRODUCTS_R_ATTRIBUTES
            )T1
            GROUP BY ean13";
        $statement = $this->_dbAdapter->query($query);
        $statement->execute();
    }

    /* END -- MIGRATE PRODUCTS */
    /* ======================================================================== */
    /* START -- MIGRATE ATTRIBUTES */

    /**
     * @return bool
     */
    public function migrateCoreAttributes()
    {
        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
        $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=0;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_ATTRIBUTES_Combination;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_ATTRIBUTES_Combination_Definitions;")->execute();
        $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=1;")->execute();
        // Get all the attributes
        $this->_sqlPMS->setTable(array("ag" => "ATTRIBUTES_Groups"));
        $select = $this->_sqlPMS->select()
            ->where("status='ACTIVE'")
            ->where("groupType='Core'")
            ->order("attributeGroupId ASC");
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $attributesCollection = array();
        foreach ($results as $result) {
            $attributesCollection[] = $result;
        }

        // For each product get the attributes values from old PMS and insert new records
        $this->_sqlPMS->setTable("PRODUCTS");
        $select = $this->_sqlPMS->select()
            ->order("productId ASC");
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $productsResults = $statement->execute();

        foreach ($productsResults as $productArray) {
            // this attribute is set in the old CMS
            $this->_sqlOldPMS->setTable("proddata");
            $select = $this->_sqlOldPMS->select();
            $select->where(array('FPRODNO' => $productArray['sku']));
            $statement = $this->_sqlOldPMS->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            if ($results->count() == 1) {
                $oldProductData = $results->next();
            } else {
                $oldProductData = false;
            }
            if ($oldProductData == false) {
                continue;
            }

            $this->addAttributes($productArray, $attributesCollection, $oldProductData);
        }

        // For each product variant get the attributes values from old PMS and insert new records
        $this->_sqlPMS->setTable("PRODUCTS_R_ATTRIBUTES");
        $select = $this->_sqlPMS->select()
            ->order("productAttributeId ASC");
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $variantsResults = $statement->execute();

        foreach ($variantsResults as $productArray) {
            // this attribute is set in the old CMS
            $this->_sqlOldPMS->setTable("proddata");
            $select = $this->_sqlOldPMS->select();
            $select->where(array('FPRODNO' => $productArray['sku']));
            $statement = $this->_sqlOldPMS->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            if ($results->count() == 1) {
                $oldProductData = $results->next();
            } else {
                $oldProductData = false;
            }
            if ($oldProductData == false) {
                continue;
            }
            $this->addAttributes($productArray, $attributesCollection, $oldProductData, 1);
        }
        $this->_dbAdapter->getDriver()->getConnection()->commit();
        return true;
    }

    /**
     * @param $productArray
     * @param $attributesCollection
     * @param $oldProductData
     * @param int $isVariant
     */
    private function addAttributes($productArray, $attributesCollection, $oldProductData, $isVariant = 0)
    {
        $productId = ($isVariant === 1) ? $productArray['productAttributeId'] : $productArray['productId'];
        foreach ($attributesCollection as $attributeArray) {
            $column = isset($this->attributeMapping[$attributeArray['attributeGroupId']]) ? $this->attributeMapping[$attributeArray['attributeGroupId']] : false;
            if ($column) {
                $finalValue = false;
                $type = '';
                if ($column == 'SUPPRESSFROMSALE') {
                    $finalValue = 0;
                    $type = 'CHECKBOX';
                }
                if (isset($oldProductData[$column])) {
                    switch ($column) {
                        case 'NOEXPRESS':
                            $finalValue = ($oldProductData[$column] == '0') ? 1 : 0;
                            $type = 'CHECKBOX';
                            break;
                        case 'BACKORDERS':
                        case 'SUPPRESSFROMSALE':
                        case 'UKONLY':
                        case 'HEAVYITEM':
                        case 'FREEPOST':
                            $finalValue = ($oldProductData[$column] == '0') ? 0 : 1;
                            $type = 'CHECKBOX';
                            break;
                        case 'MINORDQTY':
                        case 'HEIGHT':
                        case 'WIDTH':
                        case 'ITEMWT':
                        case 'DESPHSE':
                        case 'LENGTH':
                        case 'COMMODITY':
                            $finalValue = $oldProductData[$column];
                            $type = 'TEXT';
                            break;
                        case 'CNTRYORIG':
                            $countryCode = $oldProductData[$column];
                            $this->_sqlPMS->setTable("ATTRIBUTES_Definitions");
                            $select = $this->_sqlPMS->select()
                                ->where("status='ACTIVE'")
                                ->where("languageId='1'")
                                ->where("attributeValue='$countryCode'");
                            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
                            $results = $statement->execute();
                            if ($results->count() == 1) {
                                $res = $results->next();
                                $finalValue = $res['attributeId'];
                                $type = 'SELECT';
                            }
                            break;
                    }

                    if ($finalValue !== false) {
                        $this->_sqlPMS->setTable("PRODUCTS_ATTRIBUTES_Combination");
                        $insert = $this->_sqlPMS->insert()
                            ->values(array(
                                'attributeGroupId' => $attributeArray['attributeGroupId'],
                                'productOrVariantId' => $productId,
                                'isVariant' => $isVariant,
                                'attributeValue' => $finalValue,
                                'groupType' => $type,
                                'status' => 'ACTIVE',
                                'created' => date('Y-m-d H:i:s')
                            ));
                        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                        $statement->execute();
                        $attrId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

                        if ($type == 'TEXT') {
                            foreach ($this->_languages as $languageId) {
                                $this->_sqlPMS->setTable("PRODUCTS_ATTRIBUTES_Combination_Definitions");
                                $insert = $this->_sqlPMS->insert()
                                    ->values(array(
                                        'productAttributeCombinationId' => $attrId,
                                        'attributeValue' => $finalValue,
                                        'languageId' => $languageId,
                                        'status' => 'ACTIVE',
                                        'created' => date('Y-m-d H:i:s')
                                    ));
                                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                                $statement->execute();
                            }
                        }
                    }
                } else {
                    if ($finalValue !== false) {
                        $this->_sqlPMS->setTable("PRODUCTS_ATTRIBUTES_Combination");
                        $insert = $this->_sqlPMS->insert()
                            ->values(array(
                                'attributeGroupId' => $attributeArray['attributeGroupId'],
                                'productOrVariantId' => $productId,
                                'isVariant' => $isVariant,
                                'attributeValue' => $finalValue,
                                'groupType' => $type,
                                'status' => 'ACTIVE',
                                'created' => date('Y-m-d H:i:s')
                            ));
                        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                        $statement->execute();
                        $attrId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

                        if ($type == 'TEXT') {
                            foreach ($this->_languages as $languageId) {
                                $this->_sqlPMS->setTable("PRODUCTS_ATTRIBUTES_Combination_Definitions");
                                $insert = $this->_sqlPMS->insert()
                                    ->values(array(
                                        'productAttributeCombinationId' => $attrId,
                                        'attributeValue' => $finalValue,
                                        'languageId' => $languageId,
                                        'status' => 'ACTIVE',
                                        'created' => date('Y-m-d H:i:s')
                                    ));
                                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                                $statement->execute();
                            }
                        }
                    }
                }
            } else {
                // this attribute is not set in the old CMS
                $text = '';
                switch ($attributeArray['attributeGroupId']) {
                    case '145':
                        $text = 'FREE DELIVERY & RETURNS IN UK';
                        break;
                    case '153':
                        $text = 'FREE INTERNATIONAL DELIVERIES ON ORDERS OVER £100, $100';
                        break;
                }
                if ($text) {
                    $this->_sqlPMS->setTable("PRODUCTS_ATTRIBUTES_Combination");
                    $insert = $this->_sqlPMS->insert()
                        ->values(array(
                            'attributeGroupId' => $attributeArray['attributeGroupId'],
                            'productOrVariantId' => $productId,
                            'isVariant' => $isVariant,
                            'attributeValue' => $text,
                            'groupType' => 'TEXT',
                            'status' => 'ACTIVE',
                            'created' => date('Y-m-d H:i:s')
                        ));
                    $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                    $statement->execute();
                    $attrId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

                    foreach ($this->_languages as $languageId) {
                        $this->_sqlPMS->setTable("PRODUCTS_ATTRIBUTES_Combination_Definitions");
                        $insert = $this->_sqlPMS->insert()
                            ->values(array(
                                'productAttributeCombinationId' => $attrId,
                                'attributeValue' => $text,
                                'languageId' => $languageId,
                                'status' => 'ACTIVE',
                                'created' => date('Y-m-d H:i:s')
                            ));
                        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                        $statement->execute();
                    }
                }
            }
        }
    }
    /* END -- MIGRATE ATTRIBUTES */
    /**
     * Change url for current categories to have them unique
     */
    public function changeUrlForCategories()
    {
        $this->_sqlPMS->setTable($this->_categoriesTable);
        $select = $this->_sqlPMS->select()
            ->join(
                array('cd' => $this->_categoriesDefinitionsTable),
                $this->_categoriesTable . '.categoryId = cd.categoryId',
                array('*'),
                Select::JOIN_LEFT
            )
            ->join(
                array('self' => $this->_categoriesDefinitionsTable),
                $this->_categoriesTable . '.categoryParentId = self.categoryId',
                array('parentUrl' => 'urlName'),
                Select::JOIN_LEFT
            );
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $result) {
            if ($result['categoryParentId'] != '1' && $result['parentUrl'] != 'oliver-sweeney') {
                if (strpos($result['urlName'], $result['parentUrl']) === false) {
                    $newUrl = $result['parentUrl'] . '-' . $result['urlName'];
                    $data['urlName'] = $newUrl;
                    $update = $this->_sqlPMS->update();
                    $update->table($this->_categoriesDefinitionsTable);
                    $update->set($data);
                    $update->where(array(
                        'categoryDefinitionsId' => $result['categoryDefinitionsId'],
                    ));
                    $statement = $this->_sqlPMS->prepareStatementForSqlObject($update);
                    $statement->execute();
                }
            }
        }

        $this->_checkAndModifyDuplicatesUrls();
    }

    /**
     *
     */
    private function _checkAndModifyDuplicatesUrls()
    {
        $this->_sqlPMS->setTable($this->_categoriesDefinitionsTable);
        $select = $this->_sqlPMS->select()
            ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)'), 'urlName' => 'urlName'))
            ->group(array('urlName'))
            ->having(array('count > 1'));

        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $result) {
            $this->_sqlPMS->setTable($this->_categoriesDefinitionsTable);
            $select = $this->_sqlPMS->select()
                ->where('urlName = "'.$result['urlName'].'"');
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $sameUrls = $statement->execute();
            foreach ($sameUrls as $key => $url) {
                $data['urlName'] = $url['urlName'] . '-' . ($key+1);
                $update = $this->_sqlPMS->update();
                $update->table($this->_categoriesDefinitionsTable);
                $update->set($data);
                $update->where(array(
                    'categoryDefinitionsId' => $url['categoryDefinitionsId'],
                ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($update);
                $statement->execute();
            }
        }
    }

    /**
     * @return bool
     */
    public function copyCategoriesFromParents()
    {
        $statement = $this->_dbAdapter->query("
            	SELECT
                productId,
                productAttributeId
            FROM
                PRODUCTS_R_ATTRIBUTES
            WHERE
                productAttributeId NOT IN (
                    SELECT
                        productId
                    FROM
                        PRODUCTS_R_CATEGORIES
                    WHERE
                        productIdFrom = 'PRODUCTS_R_ATTRIBUTES'
                )
        ");
        $results = $statement->execute();

        foreach ($results as $result) {
            $this->_sqlPMS->setTable(array('pc' => 'PRODUCTS_R_CATEGORIES'));
            $select = $this->_sqlPMS->select()
                ->where("productIdFrom = 'PRODUCTS'")
                ->where("productId = '$result[productId]'");
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $productCategories = $statement->execute();

            foreach ($productCategories as $productCategory) {
                $statement = $this->_dbAdapter->query("
                    INSERT INTO PRODUCTS_R_CATEGORIES (`productId`, `categoryId`, `isDefault`, `productIdFrom`, `status`, `created`, `modified`)
                    VALUES ('$result[productAttributeId]','$productCategory[categoryId]','$productCategory[isDefault]','PRODUCTS_R_ATTRIBUTES'
                    ,'$productCategory[status]','$productCategory[created]','$productCategory[modified]');
                ");
                $statement->execute();
            }
        }
        return true;
    }

    /**
     * @return bool
     */
    public function updateAttributesForProducts()
    {
        $attributesArray = array(
            '9' => 'CHECKBOX',
            '10' => 'CHECKBOX',
            '11' => 'TEXT',
            '13' => 'TEXT',
            '14' => 'TEXT',
            '15' => 'TEXT',
            '17' => 'CHECKBOX',
            '145' => 'TEXT',
            '147' => 'TEXT',
            '148' => 'TEXT',
            '149' => 'TEXT',
            '150' => 'SELECT',
            '152' => 'CHECKBOX',
            '153' => 'TEXT'
        );
        $attributesArrayValues = array(
            '9' => '0',
            '10' => '0',
            '11' => '0',
            '13' => '0',
            '14' => '0',
            '15' => '0',
            '17' => '0',
            '145' => 'FREE DELIVERY & RETURNS IN UK',
            '147' => 'WGB',
            '148' => '1',
            '149' => '',
            '150' => '372',
            '152' => '0',
            '153' => 'FREE INTERNATIONAL DELIVERIES ON ORDERS OVER £100, $100'
        );
        /*$statement = $this->_dbAdapter->query("
        SELECT * FROM  PRODUCTS WHERE productId NOT IN
          (
            SELECT productOrVariantId FROM PRODUCTS_ATTRIBUTES_Combination WHERE isVariant=0
          )
        ");
        $results = $statement->execute();

        foreach ($results as $result) {
            $productId = $result['productId'];
            foreach ($attributesArray as $attributeId => $attributeType) {
                $this->_sqlPMS->setTable('PRODUCTS_ATTRIBUTES_Combination');
                $insert = $this->_sqlPMS->insert();
                $insert->values(array(
                    'attributeGroupId' => $attributeId,
                    'productOrVariantId' => $productId,
                    'attributeValue' => $attributesArrayValues[$attributeId],
                    'isVariant' => '0',
                    'groupType' => $attributeType,
                    'status' => 'ACTIVE',
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();

                $attrDefId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
                if ($attributeType == 'TEXT') {
                    $this->_sqlPMS->setTable('PRODUCTS_ATTRIBUTES_Combination_Definitions');
                    $insert = $this->_sqlPMS->insert();
                    $insert->values(array(
                        'productAttributeCombinationId' => $attrDefId,
                        'attributeValue' => $attributesArrayValues[$attributeId],
                        'languageId' => 1,
                        'status' => 'ACTIVE',
                        'created' => date('Y-m-d H:i:s'),
                        'modified' => date('Y-m-d H:i:s')
                    ));
                    $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                    $statement->execute();
                }
            }
        }*/

        /*$statement = $this->_dbAdapter->query("
          SELECT * FROM PRODUCTS_R_ATTRIBUTES WHERE productAttributeId NOT IN
          (
            SELECT productOrVariantId FROM PRODUCTS_ATTRIBUTES_Combination WHERE isVariant=1
          )
        ");
        $results = $statement->execute();

        foreach ($results as $result) {
            $productId = $result['productAttributeId'];
            foreach ($attributesArray as $attributeId => $attributeType) {
                $this->_sqlPMS->setTable('PRODUCTS_ATTRIBUTES_Combination');
                $insert = $this->_sqlPMS->insert();
                $insert->values(array(
                    'attributeGroupId' => $attributeId,
                    'productOrVariantId' => $productId,
                    'attributeValue' => $attributesArrayValues[$attributeId],
                    'isVariant' => '1',
                    'groupType' => $attributeType,
                    'status' => 'ACTIVE',
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();

                $attrDefId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
                if ($attributeType == 'TEXT') {
                    $this->_sqlPMS->setTable('PRODUCTS_ATTRIBUTES_Combination_Definitions');
                    $insert = $this->_sqlPMS->insert();
                    $insert->values(array(
                        'productAttributeCombinationId' => $attrDefId,
                        'attributeValue' => $attributesArrayValues[$attributeId],
                        'languageId' => 1,
                        'status' => 'ACTIVE',
                        'created' => date('Y-m-d H:i:s'),
                        'modified' => date('Y-m-d H:i:s')
                    ));
                    $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                    $statement->execute();
                }
            }
        }*/

        $statement = $this->_dbAdapter->query("SELECT * FROM CATEGORIES WHERE categoryId != 1");
        $results = $statement->execute();

        foreach ($results as $result) {
            foreach ($attributesArray as $attributeId => $attributeType) {
                $this->_sqlPMS->setTable('CATEGORIES_R_ATTRIBUTES');
                $insert = $this->_sqlPMS->insert();
                $insert->values(array(
                    'categoryId' => $result['categoryId'],
                    'attributeGroupId' => $attributeId,
                    'status' => 'ACTIVE',
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();
            }
        }
        return true;
    }

    /**
     * @return bool
     */
    public function updateMerchantAttributesForProducts()
    {
        $products = $this->_getAllBeltsProductsByName();
        foreach ($products as $product) {
            $this->_updateVariantSizesForProduct($product['productId']);
        }
        return true;
    }

    /**
     * @param $productId
     * @return bool
     */
    private function _updateVariantSizesForProduct($productId)
    {
        $this->_sqlPMS->setTable('PRODUCTS_R_ATTRIBUTES');
        $select = $this->_sqlPMS->select();
        $select->where('productId = ' . $productId);
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $variants = $statement->execute();
        foreach ($variants as $variant) {
            $this->_updateSizeForVariant($variant['productAttributeId']);
        }

        return true;
    }

    /**
     * @param $variantId
     * @return bool
     */
    private function _updateSizeForVariant($variantId)
    {
        $data = array('skuRuleTable' => 'SIZES_Belts');
        $update = $this->_sqlPMS->update();
        $update->table('PRODUCTS_R_ATTRIBUTES_R_SKU_Rules');
        $update->set($data);
        $update->where('productAttributeId = ' . $variantId);
        $update->where('skuRuleTable != "COLOURS_Groups"');
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($update);
        $result = $statement->execute();
        if ($result->getAffectedRows() > 0) {
            return true;
        }
        return false;
    }
    /**
     * @return array
     */
    private function _getAllBeltsProductsByName()
    {
        $this->_sqlPMS->setTable('PRODUCTS_Definitions');
        $select = $this->_sqlPMS->select();
        $select->where
            ->nest
            ->like('PRODUCTS_Definitions.shortProductName', "%belt%")
            ->or
            ->like('PRODUCTS_Definitions.productName', "%belt%")
            ->or
            ->like('PRODUCTS_Definitions.urlName', "%belt%")
            ->unnest;
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $returnArray = array();
        foreach ($results as $result) {
            $returnArray[] = $result;
        }
        return $returnArray;
    }
    /**
     * @return bool
     */
    public function updateMerchantCategorySku()
    {
        $attributeGroupId = $this->_getBeltSizeAttributesGroup();

        $this->_sqlPMS->setTable('PRODUCTS_Merchant_Categories');
        $select = $this->_sqlPMS->select()
            ->join(
                array('pmcd' => 'PRODUCTS_Merchant_Categories_Definitions'),
                'pmcd.productMerchantCategoryId = PRODUCTS_Merchant_Categories.productMerchantCategoryId',
                array('*'),
                Select::JOIN_LEFT
            );
        $select->where
            ->nest
            ->like('pmcd.categoryName', "%belt%")
            ->or
            ->like('pmcd.shortDescription', "%belt%")
            ->or
            ->like('pmcd.fullDescription', "%belt%")
            ->unnest;
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $return = array();
        foreach ($results as $result) {
            $return[] = $this->_changeSkuRuleForMerchantCategoryId($result['productMerchantCategoryId'], $attributeGroupId);
        }
        return json_encode($return);
    }

    /**
     * @param $merchantCategoryId
     * @param $attributeGroupId
     * @return bool
     */
    private function _changeSkuRuleForMerchantCategoryId($merchantCategoryId, $attributeGroupId)
    {
        $data = array('merchantAttributeGroupId' => $attributeGroupId);
        $update = $this->_sqlPMS->update();
        $update->table('SKU_Rules');
        $update->set($data);
        $update->where(array(
            'merchantCategoryId' => $merchantCategoryId,
            'position' => '2',
        ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($update);
        $result = $statement->execute();
        if ($result->getAffectedRows() > 0) {
            return true;
        }
        return false;
    }
    /**
     * @return mixed
     */
    private function _getBeltSizeAttributesGroup()
    {
        $this->_sqlPMS->setTable('ATTRIBUTES_Groups');
        $select = $this->_sqlPMS->select()
            ->join(
                array('agd' => 'ATTRIBUTES_Group_Definitions'),
                'agd.attributeGroupId = ATTRIBUTES_Groups.attributeGroupId',
                array('*'),
                Select::JOIN_LEFT
            )
            ->where('ATTRIBUTES_Groups.sourceTable = "SIZES_Belts"');
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();
        return $result['attributeGroupId'];
    }

    /**
     *
     */
    public function fixCategoriesForSingleVariantsProducts()
    {
        $select = $this->_sqlPMS->select('PRODUCTS');
        $select->where(array(
            'productId' => array(6, 20, 21, 33, 35, 41, 46, 48, 51, 52, 54, 55, 57, 65, 67, 72, 73, 74, 77, 100, 101, 119, 125, 127, 128, 130, 133, 134, 136, 137, 166, 167, 175, 177, 186, 187, 197, 204, 208, 209, 210, 211, 214, 215, 247, 288, 289, 291, 293, 294, 295, 301, 302, 303, 308, 309, 313, 332, 382, 383, 384, 385, 388, 405, 409, 470, 522, 523, 524, 526, 531, 532, 538, 539, 540, 541, 542, 544, 545, 546, 547, 548, 549, 555, 565, 566, 567, 568, 569, 570, 573, 574, 577, 597, 620, 633, 634, 635, 668, 669, 670, 726, 727, 782, 783, 784, 785, 819, 820, 844, 845, 846, 847, 848, 849, 850, 851, 852, 853, 970, 979)
        ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $result) {
            $parentCategories = array();
            $select12 = $this->_sqlPMS->select('PRODUCTS_R_CATEGORIES');
            $select12->where(array(
                'productId' => $result['productId'],
                'productIdFrom' => 'PRODUCTS'
            ));
            $select12->order('categoryId ASC');
            $statement2 = $this->_sqlPMS->prepareStatementForSqlObject($select12);
            $pCategories = $statement2->execute();

            foreach ($pCategories as $pCategory) {
                $parentCategories[] = $pCategory['categoryId'];
            }

            $select1 = $this->_sqlPMS->select('PRODUCTS_R_ATTRIBUTES');
            $select1->where('productId = "'.$result['productId'].'"');
            $statement1 = $this->_sqlPMS->prepareStatementForSqlObject($select1);
            $variants = $statement1->execute();

            foreach ($variants as $variant) {
                $deleteAction = $this->_sqlPMS->delete('PRODUCTS_R_CATEGORIES')->where(
                    array(
                        'productId' => $variant['productAttributeId'],
                        'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES'
                    )
                );
                $statement2 = $this->_sqlPMS->prepareStatementForSqlObject($deleteAction);
                $statement2->execute();

                foreach ($parentCategories as $pCategoryId) {
                    $data = array(
                        'productId'=> $variant['productAttributeId'],
                        'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                        'categoryId' => $pCategoryId,
                        'priority' => 1,
                        'isDefault' => 0,
                        'status' => 'ACTIVE'
                    );
                    $action = $this->_sqlPMS->insert('PRODUCTS_R_CATEGORIES');
                    $action->values($data);
                    $statement123 = $this->_sqlPMS->prepareStatementForSqlObject($action);
                    $statement123->execute();
                }

            }
        }
    }

    /**
     *
     */
    public function fixDefaultCategory()
    {
        // remove duplicates categoies first
        $q1 = "SELECT productIdFrom,productId, categoryId, count(*) as total FROM PRODUCTS_R_CATEGORIES GROUP BY productIdFrom,productId, categoryId HAVING total > 1";
        $duplicateCategories = $this->_dbAdapter->query($q1)->execute();

        foreach ($duplicateCategories as $catRow) {
            $q2 = "SELECT productCategoryId, productIdFrom,productId, categoryId FROM PRODUCTS_R_CATEGORIES WHERE productIdFrom='{$catRow['productIdFrom']}' AND productId='{$catRow['productId']}' AND categoryId='{$catRow['categoryId']}'";
            $duplicateCategoriesArray = $this->_dbAdapter->query($q2)->execute();

            // spare the first one, delete the others
            $status = 'ACTIVE';
            foreach ($duplicateCategoriesArray as $duplicateCategory) {
                if ($status == 'DELETED') {
                    $query = "DELETE FROM `PRODUCTS_R_CATEGORIES` WHERE `productCategoryId` = '{$duplicateCategory['productCategoryId']}'";
                    $this->_dbAdapter->query($query)->execute();
                    $status = 'DELETED';
                } else {
                    $query = "UPDATE `PRODUCTS_R_CATEGORIES` SET status='{$status}' WHERE `productCategoryId` = '{$duplicateCategory['productCategoryId']}'";
                    $this->_dbAdapter->query($query)->execute();
                    $status = 'DELETED';
                }
            }
        }

        $select = $this->_sqlPMS->select('PRODUCTS');
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $result) {
            $parentCategories = array();
            $variantsIds = array();

            $select1 = $this->_sqlPMS->select('PRODUCTS_R_ATTRIBUTES');
            $select1->where('productId = "'.$result['productId'].'"');
            $statement1 = $this->_sqlPMS->prepareStatementForSqlObject($select1);
            $variants = $statement1->execute();

            // get the parent categories
            $select2 = $this->_sqlPMS->select('PRODUCTS_R_CATEGORIES');
            $select2->where(array(
                'productId' => $result['productId'],
                'productIdFrom' => 'PRODUCTS',
                'status' => 'ACTIVE'
            ));
            $select2->order('categoryId ASC');
            $statement2 = $this->_sqlPMS->prepareStatementForSqlObject($select2);
            $pCategories = $statement2->execute();

            foreach ($pCategories as $pCategory) {
                $parentCategories[] = $pCategory['categoryId'];
            }

            if (empty($parentCategories)) {
                $select2 = $this->_sqlPMS->select('PRODUCTS_R_CATEGORIES');
                $select2->where(array(
                    'productId' => $result['productId'],
                    'productIdFrom' => 'PRODUCTS'
                ));
                $select2->limit(1);
                $statement2 = $this->_sqlPMS->prepareStatementForSqlObject($select2);
                $pCategories = $statement2->execute();

                if ($pCategories->count() == 0) {
                    echo '--- product '.$result['productId']. ' doesn\'t have any categories!<br />';
                    continue;
                }

                $catId = $pCategories->next()['categoryId'];
                $query = "UPDATE `PRODUCTS_R_CATEGORIES` SET status='ACTIVE' WHERE `productId` = '{$result['productId']}' AND `productIdFrom` = 'PRODUCTS' AND `categoryId` = '{$catId}'";
                $this->_dbAdapter->query($query)->execute();

                $parentCategories[] = $catId;
            }

            $commonElements = $parentCategories;
            foreach ($variants as $variant) {
                $variantsIds[] = $variant['productAttributeId'];
                $select4 = $this->_sqlPMS->select('PRODUCTS_R_CATEGORIES');
                $select4->where(array(
                    'productId' => $variant['productAttributeId'],
                    'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                    'status' => 'ACTIVE'
                ));
                $select4->order('categoryId ASC');
                $statement4 = $this->_sqlPMS->prepareStatementForSqlObject($select4);
                $vCategories = $statement4->execute();

                //let's see if we can find the categoryId
                $variantCatArray = array();
                foreach ($vCategories as $vCategory) {
                    $variantCatArray[] = $vCategory['categoryId'];
                }
                $commonElements = array_intersect($commonElements, $variantCatArray);
            }

            // after we exit this loop we will have all the common categories
            // let's try and make the default one the one with the hightst ID
            if (empty($commonElements)) {
                // this is wrong, we must flag this product so we can have a look at it later
                // in this case make take the category with the lowest id from the main product and add it to the variants
                if (!isset($parentCategories[0])) {
                    echo '---'. $result['productId']. " dosen't have any active categories".var_export($parentCategories, true).'<br />';
                    continue;
                }
                $firstCategoryId = $parentCategories[0];
                $query = "UPDATE `PRODUCTS_R_CATEGORIES` SET `isDefault` = '1' WHERE `productId` = '{$result['productId']}' AND `productIdFrom` = 'PRODUCTS' AND `categoryId` = '{$firstCategoryId}'";
                $this->_dbAdapter->query($query)->execute();

                // update the variants
                foreach ($variantsIds as $variantsId) {
                    $select21 = $this->_sqlPMS->select('PRODUCTS_R_CATEGORIES');
                    $select21->where(array(
                        'productId' => $variantsId,
                        'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                        'categoryId' => $firstCategoryId
                    ));
                    $select21->limit(1);
                    $statement21 = $this->_sqlPMS->prepareStatementForSqlObject($select21);
                    $variantCategory = $statement21->execute();

                    if ($variantCategory->count() == 1) {
                        // update existing line
                        $query = "UPDATE `PRODUCTS_R_CATEGORIES` SET `status` = 'ACTIVE', `isDefault` = '1' WHERE `productId` = '{$variantsId}' AND `productIdFrom` = 'PRODUCTS_R_ATTRIBUTES' AND `categoryId` = '{$firstCategoryId}'";
                        $this->_dbAdapter->query($query)->execute();
                    } else {
                        // insert new line
                        $query = "INSERT INTO `PRODUCTS_R_CATEGORIES`
                        (`productId`, `categoryId`, `isDefault`, `priority`, `productIdFrom`, `status`, `created`, `modified`)
                        VALUES
                        ('{$variantsId}', '{$firstCategoryId}', '1', '20', 'PRODUCTS_R_ATTRIBUTES', 'ACTIVE', NOW(), NOW());";
                        $this->_dbAdapter->query($query)->execute();
                    }
                }
            } else {
                rsort($commonElements);
                $defaultCatId = $commonElements[0];

                // update the main product first
                $query = "UPDATE `PRODUCTS_R_CATEGORIES` SET `isDefault` = '1' WHERE `productId` = '{$result['productId']}' AND `productIdFrom` = 'PRODUCTS' AND `categoryId` = '{$defaultCatId}'";
                $this->_dbAdapter->query($query)->execute();

                // update the variants
                foreach ($variantsIds as $variantsId) {
                    $query1 = "UPDATE `PRODUCTS_R_CATEGORIES` SET `isDefault` = '1' WHERE `productId` = '{$variantsId}' AND `productIdFrom` = 'PRODUCTS_R_ATTRIBUTES' AND `categoryId` = '{$defaultCatId}'";
                    $this->_dbAdapter->query($query1)->execute();
                }
            }
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function fixImages()
    {
        $select = $this->_sqlPMS->select('PRODUCTS_R_ATTRIBUTES');
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $result) {
            $folderContents = $this->_productFolderMapper->getProductFolderByProductAndVariant($result['productId'], $result['productAttributeId']);
            $mediaFolderImages = $this->_productFolderMapper->getProductImagesFromMediaFolderId($folderContents['mediaFolderId']);

            $folderImagesArray = array();
            foreach ($mediaFolderImages as $imageObject) {
                $folderImagesArray[] = $imageObject->ref;
            }

            // now we check if our default image is one of the images in the mediafolder
            if (!empty($folderImagesArray)) {
                if (!in_array($folderContents['defaultImageId'], $folderImagesArray)) {
                    // replace the existing default images
                    $defaultImageId = $folderImagesArray[0];
                    $this->_productFolderMapper->setProductSpecialImage('default', $folderContents['id'], $defaultImageId);
                    $this->_productFolderMapper->setProductSpecialImage('menu', $folderContents['id'], $defaultImageId);
                    $this->_productFolderMapper->setProductSpecialImage('hover', $folderContents['id'], $defaultImageId);
                    $this->_productFolderMapper->setProductSpecialImage('basket', $folderContents['id'], $defaultImageId);
                }
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function fixProductsUrls()
    {
        $query = "SELECT count(*) as total, urlName FROM PRODUCTS_Definitions WHERE productIdFrom='PRODUCTS' GROUP BY  urlName HAVING total > 1";
        $results = $this->_dbAdapter->query($query)->execute();

        foreach ($results as $result) {
            $query1 = "SELECT productId, urlName FROM PRODUCTS_Definitions WHERE productIdFrom='PRODUCTS' AND urlName='{$result['urlName']}'";
            $products = $this->_dbAdapter->query($query1)->execute();

            $i = 1;
            foreach ($products as $product) {
                // update the url
                $urlName = $product['urlName'].$i;
                $query3 = "UPDATE PRODUCTS_Definitions SET urlName = '{$urlName}' WHERE productIdFrom='PRODUCTS' AND productId='{$product['productId']}'";
                $this->_dbAdapter->query($query3)->execute();

                // get all variants for this product
                $query4 = "SELECT productAttributeId FROM PRODUCTS_R_ATTRIBUTES WHERE productId='{$product['productId']}'";
                $variants = $this->_dbAdapter->query($query4)->execute();

                foreach ($variants as $variant) {
                    $query5 = "UPDATE PRODUCTS_Definitions SET urlName = '{$urlName}' WHERE productIdFrom='PRODUCTS_R_ATTRIBUTES' AND productId='{$variant['productAttributeId']}'";
                    $this->_dbAdapter->query($query5)->execute();
                }
                $i++;
            }
        }

        return true;
    }

    /**
     *
     */
    public function getProductsWithoutImages()
    {
        $consolidInfo = array();
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo '"Product Name","Product Consolid", "Product Status"'."\n";
        $query = "SELECT T1.productId, SUBSTR(T1.sku, 1, 9) as consolid, T1.`status`, PD.productName FROM
            (SELECT P.* FROM PRODUCTS P LEFT JOIN PRODUCTS_R_ATTRIBUTES PA ON P.productId=PA.productId WHERE PA.productId IS NULL)T1
            INNER JOIN PRODUCTS_Definitions PD ON T1.productId = PD.productId AND PD.productIdFrom='PRODUCTS'";
        $standaloneProducts = $this->_dbAdapter->query($query)->execute();

        foreach ($standaloneProducts as $product) {
            if (!in_array($product['consolid'], $consolidInfo)) {
                // check if it has been already processed
                // check if it has a corespondent in the media table
                $mediaQuery = "SELECT mediaFolderId FROM MEDIA_Products WHERE productId='{$product['productId']}' AND variantId=0";
                $mediaFolderResult = $this->_dbAdapter->query($mediaQuery)->execute();

                if ($mediaFolderResult->count() > 0) {
                    // it has a media folder. let's check it
                    $mediaFolderId = $mediaFolderResult->next()['mediaFolderId'];
                    $mediaFolderImages = $this->_productFolderMapper->getProductImagesFromMediaFolderId($mediaFolderId);

                    if (count($mediaFolderImages) == 0) {
                        // we don't have any image for this product
                        echo '"'.$product['productName'].'","'.$product['consolid'].'","'.$product['status'].'"'."\n";
                    }
                    $consolidInfo[] = $product['consolid'];
                } else {
                    // no media folder so no images. add it to the list
                    // output the product
                    $consolidInfo[] = $product['consolid'];
                    echo '"'.$product['productName'].'","'.$product['consolid'].'","'.$product['status'].'"'."\n";
                }
            }
        }

        $query = "SELECT T1.productAttributeId, T1.productId, SUBSTR(T1.sku, 1, 9) as consolid, T1.`status`, PD.productName FROM
            (SELECT PA.* FROM PRODUCTS P INNER JOIN PRODUCTS_R_ATTRIBUTES PA ON P.productId=PA.productId)T1
            INNER JOIN PRODUCTS_Definitions PD ON T1.productAttributeId = PD.productId AND PD.productIdFrom='PRODUCTS_R_ATTRIBUTES'";
        $variantsProducts = $this->_dbAdapter->query($query)->execute();

        foreach ($variantsProducts as $variant) {
            if (!in_array($variant['consolid'], $consolidInfo)) {
                // check if it has been already processed
                // check if it has a corespondent in the media table
                $mediaQuery = "SELECT mediaFolderId FROM MEDIA_Products WHERE productId='{$variant['productId']}' AND variantId='{$variant['productAttributeId']}'";
                $mediaFolderResult = $this->_dbAdapter->query($mediaQuery)->execute();

                if ($mediaFolderResult->count() > 0) {
                    // it has a media folder. let's check it
                    $mediaFolderId = $mediaFolderResult->next()['mediaFolderId'];
                    $mediaFolderImages = $this->_productFolderMapper->getProductImagesFromMediaFolderId($mediaFolderId);

                    if (count($mediaFolderImages) == 0) {
                        // we don't have any image for this product
                        echo '"'.$variant['productName'].'","'.$variant['consolid'].'","'.$variant['status'].'"'."\n";
                    }
                    $consolidInfo[] = $variant['consolid'];
                } else {
                    // no media folder so no images. add it to the list
                    // output the product
                    $consolidInfo[] = $variant['consolid'];
                    echo '"'.$variant['productName'].'","'.$variant['consolid'].'","'.$variant['status'].'"'."\n";
                }
            }
        }
    }

    /**
     *
     */
    public function importCareInformation()
    {
        $select = $this->_sqlPMS->select('PRODUCTS');
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $product) {
            $this->_sqlOldPMS->setTable(array('P' => 'proddata'));
            $select123 = $this->_sqlOldPMS->select()
                ->columns(array('STYLETYPE'))
                ->where('P.FPRODNO = "'.$product['sku'].'"')
                ->limit(1);
            $statement123 = $this->_sqlOldPMS->prepareStatementForSqlObject($select123);
            $styleType = $statement123->execute()->next()['STYLETYPE'];

            // now we need to select the care information accordingly
            $this->_sqlOldPMS->setTable(array('C' => 'component'));
            $select1234 = $this->_sqlOldPMS->select()
                ->where('C.component_code = "CAREDATA-'.$styleType.'"');
            $statement1234 = $this->_sqlOldPMS->prepareStatementForSqlObject($select1234);
            $oldCareInfoIdResult = $statement1234->execute();

            if ($oldCareInfoIdResult->count() == 1) {
                $oldCareInfoId = $oldCareInfoIdResult->next()['id'];

                $select12345 = $this->_sqlPMS->select('TABS')->where("oldTabId='{$oldCareInfoId}'")->limit(1);
                $statement12345 = $this->_sqlPMS->prepareStatementForSqlObject($select12345);
                $tabIdResult = $statement12345->execute();
                if ($tabIdResult->count() == 1) {
                    $tabId = $tabIdResult->next()['tabId'];
                    $query123456 = "INSERT INTO `PRODUCTS_Tabs`
                     (`productId`, `tabId`, `tabGroupId`, `priority`, `status`, `created`, `modified`)
                     VALUES
                     ('{$product['productId']}', '{$tabId}', 1, 1, 'ACTIVE', NOW(), NOW());";
                    $this->_dbAdapter->query($query123456)->execute();
                } else {
                    echo '-- Old Tab Id '.$oldCareInfoId.'not found in current database<br />';
                }
            } else {
                echo '-- Product '.$product['productId'].' had no old care information<br />';
            }
        }
    }

    /**
     *
     */
    public function productStatuses()
    {
        $select = $this->_sqlPMS->select('PRODUCTS');
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $product) {
            // check if this product has variants

            $select1 = $this->_sqlPMS->select('PRODUCTS_R_ATTRIBUTES');
            $select1->where("productId = '{$product['productId']}'");
            $statement1 = $this->_sqlPMS->prepareStatementForSqlObject($select1);
            $results1 = $statement1->execute();

            if ($results1->count() > 0) {
                // it has variants
                $mainStatus = 'INACTIVE';
                foreach ($results1 as $variant) {
                    $select12 = $this->_sqlOldPMS->select('proddata')
                        ->where("FPRODNO = '{$variant['sku']}'")
                        ->limit(1);
                    $statement12 = $this->_sqlOldPMS->prepareStatementForSqlObject($select12);
                    $results12 = $statement12->execute()->next();

                    $status = ($results12['ENABLED'] == 0) ? 'INACTIVE' : 'ACTIVE';

                    $query = "UPDATE `PRODUCTS_R_ATTRIBUTES` SET `status` = '{$status}' WHERE `productAttributeId` = '{$variant['productAttributeId']}'";
                    $this->_dbAdapter->query($query)->execute();

                    if ($status == 'ACTIVE') {
                        $mainStatus = 'ACTIVE';
                    }
                }
                $query = "UPDATE `PRODUCTS` SET `status` = '{$mainStatus}' WHERE `productId` = '{$product['productId']}'";
                $this->_dbAdapter->query($query)->execute();
            } else {
                // it dosen't have variants, just update the status
                $select12 = $this->_sqlOldPMS->select('proddata')
                    ->where("FPRODNO = '{$product['sku']}'")
                    ->limit(1);
                $statement12 = $this->_sqlOldPMS->prepareStatementForSqlObject($select12);
                $results12 = $statement12->execute()->next();

                $status = ($results12['ENABLED'] == 0) ? 'INACTIVE' : 'ACTIVE';

                $query = "UPDATE `PRODUCTS` SET `status` = '{$status}' WHERE `productId` = '{$product['productId']}'";
                $this->_dbAdapter->query($query)->execute();
            }
        }
    }

    /**
     *
     */
    public function migrateNewMarketingAttributes()
    {
        $attributes = array(
            'brand' => 165,
            'material' => 166,
            'fastening' => 167,
            'soleMaterial' => 169,
            'style' => 170,
        );

        $brandList = array(
            '96 Hrs',
            'Compass',
            'Core',
            'Custom Grade',
            'Good Year',
            'Heritage',
            'Jaguar by OS',
            'Oliver Sweeney',
            'Oliver Sweeney X Swim-Ology',
            'Olympic',
            'OS X MM',
            'OS X Proudlock',
            'Sale Buy',
            'Sweeney London',
            'Top Tier',
            'Travel',
            'Tweed'
        );

        $materialList = array(
            'Batisfera',
            'Delave suede',
            'Princess',
            'Golf',
            'Suede',
            'Pass',
            'Mill',
            'Bleached suede',
            'Monster',
            'Siamesis Wallet',
            '100% Cotton',
            'Wash',
            'Stingray',
            'Carli Cordovan High Shine',
            'Colt',
            'Mask',
            'High End Pass',
            'Margarita',
            'Patent',
            'Nubuk',
            'Crust',
            'Wool Mix',
            'Calf',
            'Airone',
            'Lizard Calf',
            'Paesante',
            'Deer/Calf',
            'Deer',
            'Aquila',
            'Oxide',
            'Tamponato',
            'Cordovan',
            'Corteccia',
            'Polyamide Mix',
            'Kudu Reverse',
            'Dakar',
            'Iguana',
            'Cotton',
            'Oregon',
            'Shearling',
            'Merino wool',
            'Crocodile',
            'Boa managua',
            'Suede / Stingray',
            'Python / Suede',
            'Siamesis Key Chain',
            'Noblesse',
            'Harold',
            'Diana',
            'Carli Brush Off',
            'Brush Off',
            'Moriolo',
            'Moriolo/Suede',
            'Polaris Horse',
            'Carli Diana Black',
            'Scotch Grain',
            'Shiardo',
            'Carli Brushed Grey 140',
            'Carli Brushed 133 Red',
            'Carli Diana',
            'Sciarda Bronx',
            'Polaris Buttero',
            'Hummer/Bronx',
            'Leather',
            'Leather/Wool',
            'Wool',
            'Marino Wool',
            'Wool Blend',
            'Cashmere',
            'Deer Skin',
            'Lamb',
            'Eel/Calf',
            'Sting/Pat/Calf',
            'Ostrich',
            'Pony',
            'SIOUX/ MINERVA',
            'Rugby',
            'Tejus',
            'Croc Print',
            'Minerva',
            'Velvet',
            'Specchio',
            'Old Japan',
            'Carly Cordovan',
            'Aniline leather',
            'Rub Off',
            'Abrasivato',
            'Golf/Thule Lizard',
            'Polyamide',
            'Snake Print',
            'Dupuy Calf',
            'Saffiano',
            'Leather/Canvas',
            'Hi-Shine',
            'Buffalo',
            'Plastibert',
            'Croc/Tweed',
            'Saffiano/Wrangler',
            'Silk',
            'Sandwich Leather',
            'Carbon Fibre',
            'Tumbled Sheep',
            'Polyester',
            'Felt',
            'Wrangler',
            'Twill',
            'Lamb Nappa',
            'Wood',
            'Leather/Suede',
            'Oxhorn',
            'Oxhorn/Badger Hair',
            'Prime Antik',
            'Country Grain',
            'Denim/Country Grain',
            'Canvas',
            'Linen',
            'Straw',
            'Fire Hose',
            'Cracked Leather',
            'Camel',
            'Steads Leather',
            'Caviar Grain',
            'Poplin',
            'Brushed Cotton',
            'Waxed Cotton',
            'Madras',
            'Coated Cotton',
            'Nylon',
            'Denim',
            'Fabric',
            'Varenne',
            'Leather/Denim'
        );

        $soleMaterialList = array(
            'None selected',
            'Leather',
            'Latex',
            'Rubber',
            'Vibram',
            'Thumb Print Sole',
            'Crepe',
            'Thumb Print',
            'Commando',
            'Resin'
        );

        $fasteningList = array(
            'None selected',
            'Lace',
            'Slip',
            'Zip'
        );

        $styleList = array(
            'None selected',
            'Formal',
            'Casual'
        );

        foreach ($attributes as $attributeName => $attributeGroupId) {


            // set up the attributes
            /*$listArray = ${$attributeName.'List'};

            foreach ($listArray as $attributeName) {
                $query = "INSERT INTO `ATTRIBUTES`
                (`attributeGroupId`, `status`, `created`, `modified`) VALUES
                ('{$attributeGroupId}', 'ACTIVE', NOW(), NOW());";
                $this->_dbAdapter->query($query)->execute();
                $attributeId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
                $query = "INSERT INTO `ATTRIBUTES_Definitions`
                (`languageId`, `attributeId`, `attributeValue`, `status`, `created`, `modified`)
                VALUES ('1', '{$attributeId}', '{$attributeName}', 'ACTIVE', NOW(), NOW())";
                $this->_dbAdapter->query($query)->execute();
            }*/

            // bind the parent group to all categories
            /*$query = "SELECT * FROM CATEGORIES";
            $results = $this->_dbAdapter->query($query)->execute();

            foreach ($results as $result) {
                $insertQuery = "INSERT INTO `CATEGORIES_R_ATTRIBUTES` (`categoryId`, `attributeGroupId`, `status`, `created`, `modified`)
                VALUES
                ('{$result['categoryId']}', '{$attributeGroupId}', 'ACTIVE', NOW(), NOW());";
                $this->_dbAdapter->query($insertQuery)->execute();
            }*/
        }
    }

    /**
     *
     */
    public function bindNewMarketingAttributes()
    {
        $attributesToInspect = array(15, 23, 14,  22,  28,  31,  43,  54,  58,  118,  128, 13, 30, 42, 168,  132,  104,  101,  100,  102,  139,  11,  125,  105,  107,  143,  116,  26,  103,  106,  119, 10,  16,  18,  19,  20,  21,  24,  25,  27,  29,  32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  44,  45,  46,  47,  48,  49,  50,  51,  52,  53,  55,  56,  57,  59,  60,  61,  62,  63,  64,  65,  66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,  78,  79,  80,  81,  82,  83,  84,  85,  86,  87,  88,  89,  90,  91,  92,  93,  94,  95,  96,  97,  98,  99,  108,  109,  110,  111,  112,  113,  114,  115,  117,  120,  121,  122,  123,  124,  126,  127,  129,  130,  131,  133,  134,  135,  136,  137,  138,  140,  141,  142,  144,  145,  146,  147,  148,  150,  151,  152,  153,  154,  155,  156,  157,  158,  159,  160,  161,  162,  163,  164,  165,  166,  167,  169,  170,  172,  173,  174,  175,  176);

        $statement = $this->_oldPMSAdapter->query("SELECT W.PRODNO, W.CATEGORY, WC.NAME FROM webtags W INNER JOIN webtags_category WC ON W.CATEGORY=WC.ID");
        $results = $statement->execute();

        foreach ($results as $result) {
            if (in_array($result['CATEGORY'], $attributesToInspect)) {
                // check if this sku exists in the database
                // first at product level
                // CHECK IF WE HAVE A CORESPONDENT IN THE CURRENT DATABASE FOR THIS PRODUCT
                if ($result['NAME'] == 'Leather') {
                    $attributeId = 568;
                    $attributeGroupId = 169;
                } else {
                    $query = "SELECT A.attributeId, A.attributeGroupId FROM ATTRIBUTES A INNER JOIN ATTRIBUTES_Definitions AD ON A.attributeId=AD.attributeId AND AD.attributeValue='{$result['NAME']}'";
                    $attributesDetails = $this->_dbAdapter->query($query)->execute();
                    if ($attributesDetails->count() != 1) {
                        echo '-- no values found for attribute '.$result['NAME'].'<br />';
                        continue;
                    } else {
                        $attributeDetails = $attributesDetails->next();
                        $attributeId = $attributeDetails['attributeId'];
                        $attributeGroupId = $attributeDetails['attributeGroupId'];;
                    }
                }

                $query1 = "SELECT productId FROM PRODUCTS WHERE sku='{$result['PRODNO']}'";
                $products = $this->_dbAdapter->query($query1)->execute();

                if ($products->count() == 1) {
                    // update this product
                    $product = $products->next();
                    $insertQuery = "INSERT INTO `PRODUCTS_ATTRIBUTES_Combination`
                        (`attributeGroupId`, `productOrVariantId`, `attributeValue`, `isVariant`, `groupType`, `status`, `created`, `modified`)
                        VALUES
                        ('{$attributeGroupId}', '{$product['productId']}', '{$attributeId}', '0', 'SELECT', 'ACTIVE', NOW(), NOW());
                    ";
                    $this->_dbAdapter->query($insertQuery)->execute();
                }

                $query2 = "SELECT productAttributeId FROM PRODUCTS_R_ATTRIBUTES WHERE sku='{$result['PRODNO']}'";
                $variants = $this->_dbAdapter->query($query2)->execute();

                if ($variants->count() == 1) {
                    // update this product
                    $variant = $variants->next();
                    $insertQuery = "INSERT INTO `PRODUCTS_ATTRIBUTES_Combination`
                        (`attributeGroupId`, `productOrVariantId`, `attributeValue`, `isVariant`, `groupType`, `status`, `created`, `modified`)
                        VALUES
                        ('{$attributeGroupId}', '{$variant['productAttributeId']}', '{$attributeId}', '1', 'SELECT', 'ACTIVE', NOW(), NOW());
                    ";
                    $this->_dbAdapter->query($insertQuery)->execute();
                }
            } else {
                continue;
            }
        }
    }

    /**
     *
     */
    public function moveAttributesFromProductToVariants()
    {
        $this->_sqlPMS->setTable($this->_productVariantsTable);
        $select = $this->_sqlPMS->select();
        $select->join(
            array('pac' => $this->_productsRAttributesTable), //Set join table and alias
            new Expression("{$this->_productVariantsTable}.productAttributeId = pac.productOrVariantId AND isVariant = 1"),
            array('*'),
            Select::JOIN_LEFT
        );
        $select->where('pac.productAttributeCombinationId IS NULL');
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $productsVariants = array();
        echo '<pre>';
        foreach ($result as $res) {
            $productsVariants[] = $res;
            $attributesForProduct = $this->getAllCombinationsForProduct($res['productId']);
            foreach ($attributesForProduct as $attribute) {
                echo 'productId = ' . $res['productId'] . '<br>';
                echo 'variantId = ' . $res['productAttributeId'] . '<br>';
                //var_dump($attribute);

                $arrayToInsert = array(
                    'attributeGroupId' => $attribute['attributeGroupId'],
                    'productOrVariantId' => $res['productAttributeId'],
                    'attributeValue' => $attribute['attributeValue'],
                    'isVariant' => '1',
                    'groupType' => $attribute['groupType'],
                    'status' => 'ACTIVE'
                );
                $this->_sqlPMS->setTable($this->_productsRAttributesTable);
                $action = $this->_sqlPMS->insert();
                $action->values($arrayToInsert);
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($action);
                $statement->execute();
            }
        }
    }

    /**
     * @param $productId
     * @return array
     */
    public function getAllCombinationsForProduct($productId)
    {
        $returnArray = array();
        $this->_sqlPMS->setTable(array('pact' => $this->_productsRAttributesTable));
        $select = $this->_sqlPMS->select()
            ->where(array(
                'pact.productOrVariantId' => $productId,
                'pact.isVariant' => 0,
                'pact.status' => 'ACTIVE'
            ));

        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $result) {
            $this->_sqlPMS->setTable(array('pactd' => $this->_productsRAttributesTableDefinitions));
            $select = $this->_sqlPMS->select()
                ->where(array(
                    'pactd.productAttributeCombinationId' => $result['productAttributeCombinationId'],
                ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $translationsResults = $statement->execute();

            if ($translationsResults->count() > 0) {
                $result['translations'] = array();
                foreach ($translationsResults as $translationsResult) {
                    $result['translations'][$translationsResult['languageId']] = $translationsResult['attributeValue'];
                }
            }
            $returnArray[] = $result;
        }
        return $returnArray;
    }

    /**
     *
     */
    public function populateProductsTmp()
    {
        set_time_limit(0);

        $this->populateTMPTable();
        $this->_productsTMPTable = 'products_TMP';
        $this->_sqlPMS->setTable(array('pact' => $this->_productsTMPTable));
        $select = $this->_sqlPMS->select();
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $result) {
            switch ($result['productIdFrom']) {
                case 'PRODUCTS_R_ATTRIBUTES':
                    // PMS DATA
                    $categoriesPMS = $this->getProductInfoForVariantPMS($result);
                    $rangePMS = $this->getAttributeInfoForProductOrVariantPMS($result, 1, 165);
                    $supplierPMS = $this->getSupplierForVariant($result, 1, 171);
                    $descriptionPMS = $this->getDescriptionForProductOrVariantPMS($result);
                    $pricesPMS = $this->getPricesForProductOrVariantPMS($result);
                    $colourPMS = $this->getColourForVariantPMS($result);

                    // SPECTRUM DATA
                    $categoriesSpectrum = $this->getProductInfoFromSPECTRUM($result);
                    $rangeSPECTRUM = $this->getAttributeInfoForProductOrVariantSPECTRUM($result, 40);
                    $supplierSPECTRUM = $this->getAttributeInfoForProductOrVariantSPECTRUM($result, 14);
                    $descriptionSPECTRUM = $this->getDescriptionForProductOrVariantSPECTRUM($result);
                    $costPriceSPECTRUM = $this->getAttributeInfoForProductOrVariantSPECTRUM($result, 11);
                    $colourSPECTRUM = $this->getColourForVariantSPECTRUM($result);
                    $pricesSPECTRUM = $this->getPricesForProductSPECTRUM($result);
                    break;
                case 'PRODUCTS':
                    // PMS DATA
                    $categoriesPMS = $this->getProductInfoForProductPMS($result);
                    $rangePMS = $this->getAttributeInfoForProductOrVariantPMS($result, 0, 165);
                    $supplierPMS = $this->getSupplierForProduct($result['productId']);
                    $descriptionPMS = $this->getDescriptionForProductOrVariantPMS($result);
                    $pricesPMS = $this->getPricesForProductOrVariantPMS($result);
                    $colourPMS = '-';

                    // SPECTRUM DATA
                    $categoriesSpectrum = $this->getProductInfoFromSPECTRUM($result);
                    $rangeSPECTRUM = $this->getAttributeInfoForProductOrVariantSPECTRUM($result, 40);
                    $supplierSPECTRUM = $this->getAttributeInfoForProductOrVariantSPECTRUM($result, 14);
                    $descriptionSPECTRUM = $this->getDescriptionForProductOrVariantSPECTRUM($result);
                    $costPriceSPECTRUM = $this->getAttributeInfoForProductOrVariantSPECTRUM($result, 11);
                    $colourSPECTRUM = $this->getColourForVariantSPECTRUM($result);
                    $pricesSPECTRUM = $this->getPricesForProductSPECTRUM($result);
                    break;
            }
            $updateArray = array(
                'categoryPMS' => $categoriesPMS['main'],
                'subcategoryPMS' => $categoriesPMS['sub'],
                'rangePMS' => $rangePMS,
                'supplierPMS' => $supplierPMS,
                'descriptionPMS' => $descriptionPMS,
                'wasPricePMS' => $pricesPMS['wasPrice'],
                'nowPricePMS' => $pricesPMS['nowPrice'],
                'costPricePMS' => $pricesPMS['costPrice'],
                'colourPMS' => $colourPMS,

                'categorySPECTRUM' => $categoriesSpectrum['main'],
                'subcategorySPECTRUM' => $categoriesSpectrum['sub'],
                'rangeSPECTRUM' => $rangeSPECTRUM,
                'supplierSPECTRUM' => $supplierSPECTRUM,
                'descriptionSPECTRUM' => $descriptionSPECTRUM,
                'wasPriceSPECTRUM' => $pricesSPECTRUM['wasPrice'],
                'nowPriceSPECTRUM' => $pricesSPECTRUM['nowPrice'],
                'costPriceSPECTRUM' => $costPriceSPECTRUM,
                'colourSPECTRUM' => $colourSPECTRUM
            );

            $update = $this->_sqlPMS->update();
            $update->table('products_TMP');
            $update->set($updateArray);
            $update->where(array(
                'productsTmpId' => $result['productsTmpId'],
            ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($update);
            $statement->execute();
        }

        $this->_sqlPMS->setTable('products_TMP');
        $select = $this->_sqlPMS->select();
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $rows = $statement->execute();
        $this->downloadExcelReport($rows);
    }

    /**
     * @param $data
     * @return string
     */
    public function getSupplierForVariant ($data)
    {
        $this->_sqlPMS->setTable('PRODUCTS_R_ATTRIBUTES');
        $select = $this->_sqlPMS->select()->where('productAttributeId = ' . $data['productId']);
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $variant = $statement->execute()->next();

        $supplier = $this->getSupplierForProduct($variant['productId']);

        return $supplier;
    }

    /**
     * @param $productId
     * @return string
     */
    public function getSupplierForProduct ($productId)
    {
        $this->_sqlPMS->setTable('PRODUCTS');
        $select = $this->_sqlPMS->select()->where('productId = ' . $productId);
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $product = $statement->execute()->next();

        return !empty($product) ? $product['supplierReference'] : '-';
    }

    /**
     *
     */
    public function populateTMPTable()
    {
        $this->_dbAdapter->query("TRUNCATE products_TMP;")->execute();
        $SQL = 'INSERT INTO products_TMP
                SELECT * FROM
                (
                SELECT 0 as productsTmpID,
                T1.sku,
                ps.spectrumId,
                "PRODUCTS" as productIdFrom,
                T1.productId,
                NULL as categoryPMS,
                NULL as categorySPECTRUM,
                NULL as subcategoryPMS,
                NULL as subcategorySPECTRUM,
                NULL as rangePMS,
                NULL as rangeSPECTRUM,
                NULL as colourPMS,
                NULL as colourSPECTRUM,
                NULL as descriptionPMS,
                NULL as descriptionSPECTRUM,
                NULL as supplierPMS,
                NULL as supplierSPECTRUM,
                NULL as wasPricePMS,
                NULL as wasPriceSPECTRUM,
                NULL as nowPricePMS,
                NULL as nowPriceSPECTRUM,
                NULL as costPricePMS,
                NULL as costPriceSPECTRUM
                FROM
                (SELECT p.sku,p.productId FROM PRODUCTS p
                LEFT JOIN PRODUCTS_R_ATTRIBUTES pa ON p.productId = pa.productId
                WHERE pa.productId IS NULL ) T1
                INNER JOIN
                PRODUCTS_R_SPECTRUM ps ON T1.productId = ps.productId AND ps.productIdFrom = "PRODUCTS"
                UNION
                SELECT 0 as productsTmpID,
                pra.sku,
                prs.spectrumId,
                "PRODUCTS_R_ATTRIBUTES" as productIdFrom,
                pra.productAttributeId as productId,
                NULL as categoryPMS,
                NULL as categorySPECTRUM,
                NULL as subcategoryPMS,
                NULL as subcategorySPECTRUM,
                NULL as rangePMS,
                NULL as rangeSPECTRUM,
                NULL as colourPMS,
                NULL as colourSPECTRUM,
                NULL as descriptionPMS,
                NULL as descriptionSPECTRUM,
                NULL as supplierPMS,
                NULL as supplierSPECTRUM,
                NULL as wasPricePMS,
                NULL as wasPriceSPECTRUM,
                NULL as nowPricePMS,
                NULL as nowPriceSPECTRUM,
                NULL as costPricePMS,
                NULL as costPriceSPECTRUM
                FROM PRODUCTS_R_ATTRIBUTES pra
                INNER JOIN PRODUCTS_R_SPECTRUM prs ON pra.productAttributeId = prs.productId AND prs.productIdFrom = "PRODUCTS_R_ATTRIBUTES" ) as main;';
        $this->_dbAdapter->query($SQL)->execute();
    }

    /**
     * @param $rows
     * @throws \Exception
     */
    public function downloadExcelReport ($rows)
    {
        ini_set('memory_limit', '-1');
        $excelReader = new \PHPExcel();
        $excelReader->getProperties()->setCreator('Prism DM');
        $excelReader->getProperties()->setTitle('PMS Data <-> Spectrum Data');
        $excelReader->getProperties()->setSubject('PMS Data <-> Spectrum Data');
        $excelReader->getProperties()->setDescription('PMS Data <-> Spectrum Data');

        $i = 0;
        $excelReader->createSheet($i);
        $excelReader->getActiveSheetIndex($i);
        $excelReader->setActiveSheetIndex($i);

        $excelReader->getActiveSheet()->setCellValue('A1', 'SKU');
        $excelReader->getActiveSheet()->setCellValue('B1', 'SPECTRUM ID');
        $excelReader->getActiveSheet()->setCellValue('C1', 'PRODUCT OR VARIANT');
        $excelReader->getActiveSheet()->setCellValue('D1', 'PRODUCT ID PMS');
        $excelReader->getActiveSheet()->setCellValue('E1', 'CATEGORY PMS');
        $excelReader->getActiveSheet()->setCellValue('F1', 'CATEGORY SPECTRUM');
        $excelReader->getActiveSheet()->setCellValue('G1', 'SUBCATEGORY PMS');
        $excelReader->getActiveSheet()->setCellValue('H1', 'SUBCATEGORY SPECTRUM');
        $excelReader->getActiveSheet()->setCellValue('I1', 'RANGE PMS');
        $excelReader->getActiveSheet()->setCellValue('J1', 'RANGE SPECTRUM');
        $excelReader->getActiveSheet()->setCellValue('K1', 'COLOUR CODE PMS');
        $excelReader->getActiveSheet()->setCellValue('L1', 'COLOUR CODE SPECTRUM');
        $excelReader->getActiveSheet()->setCellValue('M1', 'DESCRIPTION PMS');
        $excelReader->getActiveSheet()->setCellValue('N1', 'DESCRIPTION SPECTRUM');
        $excelReader->getActiveSheet()->setCellValue('O1', 'SUPPLIER PMS');
        $excelReader->getActiveSheet()->setCellValue('P1', 'SUPPLIER SPECTRUM');
        $excelReader->getActiveSheet()->setCellValue('Q1', 'WAS PRICE PMS');
        $excelReader->getActiveSheet()->setCellValue('R1', 'WAS PRICE SPECTRUM');
        $excelReader->getActiveSheet()->setCellValue('S1', 'NOW PRICE PMS');
        $excelReader->getActiveSheet()->setCellValue('T1', 'NOW PRICE SPECTRUM');
        $excelReader->getActiveSheet()->setCellValue('U1', 'COST PRICE PMS');
        $excelReader->getActiveSheet()->setCellValue('V1', 'COST PRICE SPECTRUM');
        $excelReader->getActiveSheet()->setCellValue('W1', 'VALIDATION FLAG');

        $rowNumber = 2;
        foreach ($rows as $row) {
            $flag = $this->checkIfDataIsAllright($row);
            $excelReader->getActiveSheet()->setCellValue('A'.$rowNumber, $row['sku']);
            $excelReader->getActiveSheet()->setCellValue('B'.$rowNumber, $row['spectrumId']);
            $excelReader->getActiveSheet()->setCellValue('C'.$rowNumber, $row['productIdFrom']);
            $excelReader->getActiveSheet()->setCellValue('D'.$rowNumber, $row['productId']);
            $excelReader->getActiveSheet()->setCellValue('E'.$rowNumber, $row['categoryPMS']);
            $excelReader->getActiveSheet()->setCellValue('F'.$rowNumber, $row['categorySPECTRUM']);
            $excelReader->getActiveSheet()->setCellValue('G'.$rowNumber, $row['subcategoryPMS']);
            $excelReader->getActiveSheet()->setCellValue('H'.$rowNumber, $row['subcategorySPECTRUM']);
            $excelReader->getActiveSheet()->setCellValue('I'.$rowNumber, $row['rangePMS']);
            $excelReader->getActiveSheet()->setCellValue('J'.$rowNumber, $row['rangeSPECTRUM']);
            $excelReader->getActiveSheet()->setCellValue('K'.$rowNumber, $row['colourPMS']);
            $excelReader->getActiveSheet()->setCellValue('L'.$rowNumber, $row['colourSPECTRUM']);
            $excelReader->getActiveSheet()->setCellValue('M'.$rowNumber, $row['descriptionPMS']);
            $excelReader->getActiveSheet()->setCellValue('N'.$rowNumber, $row['descriptionSPECTRUM']);
            $excelReader->getActiveSheet()->setCellValue('O'.$rowNumber, $row['supplierPMS']);
            $excelReader->getActiveSheet()->setCellValue('P'.$rowNumber, $row['supplierSPECTRUM']);
            $excelReader->getActiveSheet()->setCellValue('Q'.$rowNumber, $row['wasPricePMS']);
            $excelReader->getActiveSheet()->setCellValue('R'.$rowNumber, $row['wasPriceSPECTRUM']);
            $excelReader->getActiveSheet()->setCellValue('S'.$rowNumber, $row['nowPricePMS']);
            $excelReader->getActiveSheet()->setCellValue('T'.$rowNumber, $row['nowPriceSPECTRUM']);
            $excelReader->getActiveSheet()->setCellValue('U'.$rowNumber, $row['costPricePMS']);
            $excelReader->getActiveSheet()->setCellValue('V'.$rowNumber, $row['costPriceSPECTRUM']);
            $excelReader->getActiveSheet()->setCellValue('W'.$rowNumber, $flag ? 1 : 2);
            if (!$flag) {
                $excelReader->getActiveSheet()->getStyle('W'.$rowNumber)->getFill()
                    ->applyFromArray(array('type' => \PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array('rgb' => 'F28A8C')
                ));
            }
            $rowNumber++;
        }

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="pms-spectrum-report.csv"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($excelReader, 'CSV');
        $objWriter->save('php://output');
        exit;
    }

    /**
     * @param $row
     * @return bool
     */
    public function checkIfDataIsAllright($row)
    {
        $flag = true;

        if ($row['categoryPMS'] != $row['categorySPECTRUM']) {
            $flag = false;
        }
        if ($row['subcategoryPMS'] != $row['subcategorySPECTRUM']) {
            $flag = false;
        }
        if ($row['rangePMS'] != $row['rangeSPECTRUM']) {
            $flag = false;
        }
        if ($row['colourPMS'] != $row['colourSPECTRUM']) {
            $flag = false;
        }
//        if ($row['descriptionPMS'] != $row['descriptionSPECTRUM']) {
//            $flag = false;
//        }
        if ($row['supplierPMS'] != $row['supplierSPECTRUM']) {
            $flag = false;
        }
        if ($row['wasPricePMS'] != $row['wasPriceSPECTRUM']) {
            $flag = false;
        }
        if ($row['nowPricePMS'] != $row['nowPriceSPECTRUM']) {
            $flag = false;
        }
        if ($row['costPricePMS'] != $row['costPriceSPECTRUM']) {
            $flag = false;
        }
        return $flag;
    }

    /**
     * @param $result
     * @return array
     */
    public function getPricesForProductSPECTRUM($result)
    {
        $this->_sqlSpectrum->setTable("PRODUCTS_PriceList");
        $select = $this->_sqlSpectrum->select()
            ->where('productID = ' . $result['spectrumId'])
            ->where('pricelist = 1')
            ->where('currency = "GBP"');
        $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
        $results = $statement->execute()->next();
        $prices = array(
            'wasPrice' => !empty($results['wasPrice']) ? $results['wasPrice'] : '-',
            'nowPrice' => !empty($results['price']) ? $results['price'] : '-'
        );
        return $prices;
    }

    /**
     * @param $result
     * @return string
     */
    public function getColourForVariantSPECTRUM($result)
    {
        $this->_sqlSpectrum->setTable("PRODUCTS");
        $select = $this->_sqlSpectrum->select()
            ->where('ID = ' . $result['spectrumId']);
        $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
        $results = $statement->execute()->next();

        return !empty($results['colour']) ? $results['colour']: '-';
    }

    /**
     * @param $result
     * @return array
     */
    public function getPricesForProductOrVariantSPECTRUM($result)
    {
        $this->_sqlPMS->setTable(array('pp' => 'PRODUCTS_PriceList'));
        $select = $this->_sqlPMS->select();
        $select->where(array('productIdFrom' => $result['productIdFrom']));
        $select->where(array('productOrVariantId' => $result['productId']));
        $select->where(array('regionId' => 1));
        $select->where(array('currencyId' => 1));

        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $productPrices = $statement->execute()->next();
        $prices = array(
            'nowPrice' => !empty($productPrices['nowPrice']) ? $productPrices['nowPrice'] : 0,
            'wasPrice' => !empty($productPrices['wasPrice']) ? $productPrices['wasPrice'] : 0,
            'costPrice' => !empty($productPrices['costPrice']) ? $productPrices['costPrice'] : 0
        );
        return $prices;
    }

    /**
     * @param $result
     * @return string
     */
    public function getDescriptionForProductOrVariantSPECTRUM($result)
    {
        $this->_sqlSpectrum->setTable("PRODUCTS");
        $select = $this->_sqlSpectrum->select()
            ->where('ID = ' . $result['spectrumId']);
        $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
        $results = $statement->execute()->next();

        return !empty($results['description']) ? $results['description']: '-';
    }

    /**
     * @param $result
     * @param $attributeId
     * @return string
     */
    public function getAttributeInfoForProductOrVariantSPECTRUM($result, $attributeId)
    {
        $this->_sqlSpectrum->setTable("PRODUCTS_Attributes");
        $select = $this->_sqlSpectrum->select()
            ->where('attributeID = ' . $attributeId)
            ->where('productId = ' . $result['spectrumId']);
        $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
        $results = $statement->execute()->next();

        return !empty($results['value']) ? $results['value'] : '-';
    }

    /**
     * @param $result
     * @return mixed
     */
    public function getProductInfoFromSPECTRUM ($result)
    {
        $this->_sqlSpectrum->setTable("PRODUCTS_Attributes");
        $select = $this->_sqlSpectrum->select()
            ->where(new Expression('attributeID = 31 OR attributeID = 32'))
            ->where('productId = ' . $result['spectrumId']);
        $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $categorySpectrum['main'] = '-';
        $categorySpectrum['sub'] = '-';
        foreach ($results as $result) {
            if ($result['attributeID'] == '31') {
                $categorySpectrum['main'] = $result['value'];
            }
            if ($result['attributeID'] == '32') {
                $categorySpectrum['sub'] = $result['value'];
            }
        }

        return $categorySpectrum;
    }

    /**
     * @param $result
     * @return string
     */
    public function getColourForVariantPMS ($result)
    {
        $this->_sqlPMS->setTable(array('prk' => 'PRODUCTS_R_ATTRIBUTES_R_SKU_Rules'));
        $select = $this->_sqlPMS->select();
        $select->where(array('productAttributeId' => $result['productId']));
        $select->where(array('skuRuleTable' => 'COLOURS_Groups'));

        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $colours = $statement->execute()->next();

        $this->_coloursGroupsTable = 'COLOURS_Groups';
        $this->_coloursGroupsDefinitionsTable = 'COLOURS_Groups_Definitions';
        $this->_coloursGroupsRelationTable = 'COLOURS_R_GROUPS';
        $this->_coloursDefinitionsTable = 'COLOURS_Definitions';

        $this->_sqlPMS->setTable(array('CG' => $this->_coloursGroupsTable));
        $select = $this->_sqlPMS->select();
        $select->columns(array(
            'hex',
            'groupCode',
            'hasImage',
            'imageId'
        ));
        $select->join(
            array('CGD' => $this->_coloursGroupsDefinitionsTable),
            'CG.colourGroupId = CGD.colourGroupId',
            array(
                'colourGroupId',
                'languageId',
                'groupName' => 'name'
            )
        );
        $select->join(
            array(
                'CRG' => $this->_coloursGroupsRelationTable
            ),
            'CRG.colourGroupId=CGD.colourGroupId'
        );
        $select->join(
            array(
                'CD' => $this->_coloursDefinitionsTable
            ),
            'CD.colourId=CRG.colourId AND CGD.languageId=CD.languageId',
            array(
                'colourName'
            )
        );
        $select->where('CGD.colourGroupId="'.$colours['skuRuleId'].'"');
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $coloursForLanguagesResult = $statement->execute();

        foreach ($coloursForLanguagesResult as $colourRow) {
            if (!isset($finalColorArray[$colourRow['languageId']])) {
                $finalColorArray[$colourRow['languageId']] = array(
                    'colour_group_code' => $colourRow['groupCode'],
                    'colour' => $colourRow['groupName'],
                    'colourGroups' => array(
                        $colourRow['colourName']
                    )
                );
                if ($colourRow['hasImage'] == 1) {
                    $finalColorArray[$colourRow['languageId']]['colourImageId'] = $colourRow['imageId'];
                    $finalColorArray[$colourRow['languageId']]['colourHex'] = '';
                } else {
                    $finalColorArray[$colourRow['languageId']]['colourImageId'] = '';
                    $finalColorArray[$colourRow['languageId']]['colourHex'] = $colourRow['hex'];
                }
            } else {
                $finalColorArray[$colourRow['languageId']]['colourGroups'][] = $colourRow['colourName'];
            }
        }
        return !empty($finalColorArray[1]['colour_group_code']) ? $finalColorArray[1]['colour_group_code'] : '-';
    }

    /**
     * @param $result
     * @return array
     */
    public function getPricesForProductOrVariantPMS ($result) {
        $this->_sqlPMS->setTable(array('pp' => 'PRODUCTS_Prices'));
        $select = $this->_sqlPMS->select();
        $select->where(array('productIdFrom' => $result['productIdFrom']));
        $select->where(array('productOrVariantId' => $result['productId']));
//        $select->where(array('regionId' => 1));
        $select->where(array('currencyId' => 1));
        $select->where(array('status' => "ACTIVE"));


        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $productPrices = $statement->execute()->next();
        $prices = array(
            'nowPrice' => !empty($productPrices['nowPrice']) ? $productPrices['nowPrice'] : 0,
            'wasPrice' => !empty($productPrices['wasPrice']) ? $productPrices['wasPrice'] : 0,
            'costPrice' => !empty($productPrices['costPrice']) ? $productPrices['costPrice'] : 0
        );
        return $prices;
    }

    /**
     * @param $result
     * @return string
     */
    public function getDescriptionForProductOrVariantPMS ($result)
    {
        $this->_sqlPMS->setTable(array('pact' => 'PRODUCTS_Definitions'));
        $select = $this->_sqlPMS->select();
        $select->where(array('productIdFrom' => $result['productIdFrom']));
        $select->where(array('productId' => $result['productId']));

        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $productDetails = $statement->execute()->next();

        return !empty($productDetails['shortProductName']) ? $productDetails['shortProductName'] : '-';
    }

    /**
     * @param $result
     * @param $isVariant
     * @param $attributeId
     * @return string
     */
    public function getAttributeInfoForProductOrVariantPMS ($result, $isVariant, $attributeId)
    {
        $this->_sqlPMS->setTable(array('pac' => 'PRODUCTS_ATTRIBUTES_Combination'));
        $select = $this->_sqlPMS->select();

        $select->join(
            array('ad' => 'ATTRIBUTES_Definitions'),
            new Expression("pac.attributeValue = ad.attributeId AND ad.languageId = 1"),
            array('attributeValue')
        );

        $select->where(array('productOrVariantId' => $result['productId'], 'isVariant' => $isVariant, 'attributeGroupId' => $attributeId ));

        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();

        return !empty($result['attributeValue']) ? $result['attributeValue'] : '-';

    }

    /**
     * @param $result
     * @return mixed
     */
    public function getProductInfoForVariantPMS ($result)
    {

        $this->_sqlPMS->setTable(array('pact' => 'PRODUCTS_R_ATTRIBUTES'));
        $select = $this->_sqlPMS->select();
        $select->where(array('sku' => $result['sku']));

        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $variant = $statement->execute()->next();

        $merchantCategories = $this->getMerchantCategoriesForProductPMS($variant['productId']);

        return $merchantCategories;
    }

    /**
     * @param $result
     * @return mixed
     */
    public function getProductInfoForProductPMS ($result)
    {
        $merchantCategories = $this->getMerchantCategoriesForProductPMS($result['productId']);
        return $merchantCategories;
    }

    /**
     * @param $productId
     * @return mixed
     */
    public function getMerchantCategoriesForProductPMS ($productId)
    {
        $this->_sqlPMS->setTable(array('p' => 'PRODUCTS'));
        $select = $this->_sqlPMS->select();
        $select->join(
            array('md' => 'PRODUCTS_Merchant_Categories_Definitions'),
            new Expression("p.productMerchantCategoryId = md.productMerchantCategoryId AND md.languageId = 1"),
            array('categoryName')
        );
        $select->join(
            array('m' => 'PRODUCTS_Merchant_Categories'),
            new Expression("p.productMerchantCategoryId = m.productMerchantCategoryId AND md.languageId = 1"),
            array('parentId')
        );
        $select->where(array('p.productId' => $productId));

        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $productData = $statement->execute()->next();

        $categories['sub'] = !empty($productData['categoryName']) ? $productData['categoryName'] : '-';

        if (!empty( $productData['parentId'])) {
            $this->_sqlPMS->setTable('PRODUCTS_Merchant_Categories_Definitions');
            $select = $this->_sqlPMS->select()
                ->where(array('productMerchantCategoryId = ' . $productData['parentId']));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $result = $statement->execute()->next();
        }

        $categories['main'] = !empty($result['categoryName']) ?$result['categoryName'] : '-';

        return $categories;
    }

    /**
     * @return array
     */
    public function updateSpectrumWithUnprocessedUsers()
    {
        $unProcessedUsers = $this->_getUnprocessedUsers();
        $updateFlag = array();
        foreach ($unProcessedUsers as $newUser) {
            $response = $this->_updateSpectrum($newUser);
            if (isset($response->status) && $response->status == 200) {
                $spectrumId = !empty($response->id) ? $response->id : 0;
                $updateFlag[$newUser['details']['userId']] = $this->_updatePmsDBWithSpectrumId($newUser, $spectrumId);
            }
        }

        return $updateFlag;
    }

    /**
     * @param $newUser
     * @param $spectrumId
     * @return bool
     */
    private function _updatePmsDBWithSpectrumId($newUser, $spectrumId)
    {
        $update = $this->_sqlAdmin->update();
        $update->table('WEBSITE_USERS');
        $updateData = array('spectrumId' => $spectrumId);
        if ($newUser['details']['passwordSalt'] == 'EMAIL_SIGNUP') {
            $updateData['passwordSalt'] = Rand::getString('16');
        }
        $update->set($updateData);
        $update->where(array(
            'userId' => $newUser['details']['userId'],
        ));
        $statement = $this->_sqlAdmin->prepareStatementForSqlObject($update);
        $result = $statement->execute();
        if ($result->getAffectedRows() > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $newUser
     * @return JsonModel
     */
    private function _updateSpectrum($newUser)
    {
        $newUser = $this->_checkIfUserIsEmailSignup($newUser);
        $newUserSendArray = array(
            'external_customer_ref1' => $newUser['details']['userId'],
            'date_of_birth' => isset($newUser['details']['date_of_birth']) ? $newUser['details']['date_of_birth'] : '0000-00-00',
            'title' => isset($newUser['addresses'][0]['title']) ? $newUser['addresses'][0]['title'] : 'Mr.',
            'forename' => isset($newUser['details']['firstName']) ? $newUser['details']['firstName'] : '',
            'surname' => isset($newUser['details']['lastName']) ? $newUser['details']['lastName'] : '',
            'address1' => isset($newUser['addresses'][0]['addressLine1']) ?$newUser['addresses'][0]['addressLine1'] : '-',
            'address2'=> isset($newUser['addresses'][0]['addressLine2']) ?$newUser['addresses'][0]['addressLine1'] : '-',
            'address3'=> isset($newUser['addresses'][0]['addressLine3']) ?$newUser['addresses'][0]['addressLine3'] : '-',
            'city' => isset($newUser['addresses'][0]['city']) ?$newUser['addresses'][0]['city'] : '-',
            'postcode' => isset($newUser['addresses'][0]['postcode']) ?$newUser['addresses'][0]['postcode'] : 'GU140GT',
            'country_code' => isset($newUser['addresses'][0]['countryCode']) ?$newUser['addresses'][0]['countryCode'] : 'GB',
            'phone_number'=> isset($newUser['addresses'][0]['phone']) ?$newUser['addresses'][0]['phone'] : '',
            'email_address' => isset($newUser['details']['emailAddress']) ? $newUser['details']['emailAddress'] : '',
            'customer_reference'=> '',
            'gender' => '',
            'company'=> '',
            'region' => '',
            'client_marketing_o_k' => isset($newUser['addresses'][0]['marketingOk']) ? $newUser['addresses'][0]['marketingOk'] : 0,
            'external_marketing_o_k' => isset($newUser['addresses'][0]['externalMarketingOk']) ? $newUser['addresses'][0]['externalMarketingOk'] : 0,
            'address_type' => '',
            'email_client_contact_o_k'=> isset($newUser['addresses'][0]['marketingOk']) ? $newUser['addresses'][0]['marketingOk'] : 0,
            'email_client_marketing_o_k'=> isset($newUser['addresses'][0]['marketingOk']) ? $newUser['addresses'][0]['marketingOk'] : 0,
            'email_external_marketing_o_k' => isset($newUser['addresses'][0]['externalMarketingOk']) ? $newUser['addresses'][0]['externalMarketingOk'] : 0,
            'phone_number_type' => '',
            'phone_client_contact_o_k' => isset($newUser['addresses'][0]['marketingOk']) ? $newUser['addresses'][0]['marketingOk'] : 0,
            'phone_client_marketing_o_k' => isset($newUser['addresses'][0]['marketingOk']) ? $newUser['addresses'][0]['marketingOk'] : 0
        );
        $response = $this->_customerService->createNewCustomer($newUserSendArray);
        return $response;
    }

    /**
     * @param $newUser
     * @return mixed
     */
    private function _checkIfUserIsEmailSignup($newUser)
    {
        if ($newUser['details']['passwordSalt'] == 'EMAIL_SIGNUP' || $newUser['details']['password'] == 'EMAIL_SIGNUP') {
            $newUser['addresses'][0]['marketingOk'] = 1;
            $newUser['addresses'][0]['externalMarketingOk'] = 1;
            $newUser['addresses'][0]['addressLine1'] = 'EMAIL_SIGNUP';
            $newUser['addresses'][0]['addressLine2'] = 'EMAIL_SIGNUP';
            $newUser['addresses'][0]['addressLine3'] = 'EMAIL_SIGNUP';
            $newUser['addresses'][0]['city'] = 'EMAIL_SIGNUP';

        }
        return $newUser;
    }

    /**
     * @return array
     */
    public function _getUnprocessedUsers()
    {
        $this->_sqlAdmin->setTable('WEBSITE_USERS');
        $select = $this->_sqlAdmin->select()
            ->where('WEBSITE_USERS.spectrumId = 0 OR WEBSITE_USERS.spectrumId IS NULL');
        $statement = $this->_sqlAdmin->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $unProcessedUsers = array();
        foreach ($results as $result) {
            $this->_sqlAdmin->setTable('WEBSITE_USERS_Addresses');
            $select = $this->_sqlAdmin->select()
                ->where('WEBSITE_USERS_Addresses.userId = ' . $result['userId']);
            $statement = $this->_sqlAdmin->prepareStatementForSqlObject($select);
            $addresses = $statement->execute();
            $userAddreses = array();
            foreach ($addresses as $addresse) {
                $userAddreses[] = $addresse;
            }
            $user = array('details' => $result, 'addresses' => $userAddreses);

            $unProcessedUsers[] = $user;
        }
        return $unProcessedUsers;
    }


    /**
     * @return bool
     */
    public function populateUsersTmp()
    {
        set_time_limit(0);

        $this->populateTMPTableUsers();
        $this->_sqlAdmin->setTable(array('wutmp' => 'WEBSITE_USERS_TMP'));
        $select = $this->_sqlAdmin->select();
        $statement = $this->_sqlAdmin->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $this->downloadExcelReportForUsers($results);
        return true;
    }

    /**
     * @param $rows
     * @throws \Exception
     */
    public function downloadExcelReportForUsers($rows)
    {
        ini_set('memory_limit', '-1');
        $excelReader = new \PHPExcel();
        $excelReader->getProperties()->setCreator('Prism DM');
        $excelReader->getProperties()->setTitle('PMS Data <-> Spectrum Data');
        $excelReader->getProperties()->setSubject('PMS Data <-> Spectrum Data');
        $excelReader->getProperties()->setDescription('PMS Data <-> Spectrum Data');

        $i = 0;
        $excelReader->createSheet($i);
        $excelReader->getActiveSheetIndex($i);
        $excelReader->setActiveSheetIndex($i);

        $excelReader->getActiveSheet()->setCellValue('A1', 'userIdPMS');
        $excelReader->getActiveSheet()->setCellValue('B1', 'userIdSPECTRUM');
        $excelReader->getActiveSheet()->setCellValue('C1', 'firstNamePMS');
        $excelReader->getActiveSheet()->setCellValue('D1', 'firstNameSPECTRUM');
        $excelReader->getActiveSheet()->setCellValue('E1', 'lastNamePMS');
        $excelReader->getActiveSheet()->setCellValue('F1', 'lastNameSPECTRUM');
        $excelReader->getActiveSheet()->setCellValue('G1', 'usernamePMS');
        $excelReader->getActiveSheet()->setCellValue('H1', 'emailAddressPMS');
        $excelReader->getActiveSheet()->setCellValue('I1', 'emailAddressSPECTRUM');
        $excelReader->getActiveSheet()->setCellValue('J1', 'statusPMS');
        $excelReader->getActiveSheet()->setCellValue('K1', 'statusSPECTRUM');
        $excelReader->getActiveSheet()->setCellValue('L1', 'VALIDATION FLAG');

        $rowNumber = 2;
        foreach ($rows as $row) {
            $flag = $this->checkIfDataIsAllrightForUsers($row);
            $excelReader->getActiveSheet()->setCellValue('A'.$rowNumber, $row['userIdPMS']);
            $excelReader->getActiveSheet()->setCellValue('B'.$rowNumber, $row['userIdSPECTRUM']);
            $excelReader->getActiveSheet()->setCellValue('C'.$rowNumber, $row['firstNamePMS']);
            $excelReader->getActiveSheet()->setCellValue('D'.$rowNumber, $row['firstNameSPECTRUM']);
            $excelReader->getActiveSheet()->setCellValue('E'.$rowNumber, $row['lastNamePMS']);
            $excelReader->getActiveSheet()->setCellValue('F'.$rowNumber, $row['lastNameSPECTRUM']);
            $excelReader->getActiveSheet()->setCellValue('G'.$rowNumber, $row['usernamePMS']);
            $excelReader->getActiveSheet()->setCellValue('H'.$rowNumber, $row['emailAddressPMS']);
            $excelReader->getActiveSheet()->setCellValue('I'.$rowNumber, $row['emailAddressSPECTRUM']);
            $excelReader->getActiveSheet()->setCellValue('J'.$rowNumber, $row['statusPMS']);
            $excelReader->getActiveSheet()->setCellValue('K'.$rowNumber, $row['statusSPECTRUM']);
            $excelReader->getActiveSheet()->setCellValue('L'.$rowNumber,  $flag ? 1 : 2);
            if (!$flag) {
                $excelReader->getActiveSheet()->getStyle('W'.$rowNumber)->getFill()
                    ->applyFromArray(array('type' => \PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array('rgb' => 'F28A8C')
                    ));
            }
            $rowNumber++;
        }

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="pms-spectrum-report.csv"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($excelReader, 'CSV');
        $objWriter->save('php://output');
        exit;
    }

    /**
     * @param $row
     * @return bool
     */
    public function checkIfDataIsAllrightForUsers($row)
    {
        $flag = true;

        if (strtolower($row['firstNamePMS']) != strtolower($row['firstNameSPECTRUM'])) {
            $flag = false;
        }
        if (strtolower($row['lastNamePMS']) != strtolower($row['lastNameSPECTRUM'])) {
            $flag = false;
        }
        if (strtolower($row['usernamePMS']) != strtolower($row['emailAddressPMS'])) {
            $flag = false;
        }
        if (strtolower($row['emailAddressPMS']) != strtolower($row['emailAddressSPECTRUM'])) {
            $flag = false;
        }
        return $flag;
    }
    /**
     *
     */
    private function populateTMPTableUsers()
    {
        $this->_adminAdapter->getDriver()->getConnection()->beginTransaction();

        $this->_adminAdapter->query("TRUNCATE WEBSITE_USERS_TMP;")->execute();

        $this->_sqlAdmin->setTable('WEBSITE_USERS');
        $select = $this->_sqlAdmin->select();
        $statement = $this->_sqlAdmin->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $result) {
            // UPDATE DATA FROM PMS/SPECTRUM
            $this->_updateUsersTMPWithData($result);

        }

        $this->_adminAdapter->getDriver()->getConnection()->commit();
    }


    /**
     * @param $customerData
     */
    private function _getMainEmailInfoFromSpectrum($customerData)
    {
        $this->_sqlSpectrum->setTable('CUSTOMER_Emails');
        $select = $this->_sqlSpectrum->select()
            ->where('ID = '. $customerData['primaryEmailID']);
        $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();
        return $result;
    }

    /**
     * @param $customerData
     */
    private function _getMainAddressInfoFromSpectrum($customerData)
    {
        $this->_sqlSpectrum->setTable('CUSTOMER_Addresses');
        $select = $this->_sqlSpectrum->select()
            ->where('ID = '. $customerData['primaryAddressID']);
        $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();
        return $result;

    }

    /**
     * @param $data
     */
    private function _getMainCustomerInfoFromSpectrum($data)
    {
        $this->_sqlSpectrum->setTable('CUSTOMER');
        $select = $this->_sqlSpectrum->select()
            ->where('ID = ' . $data['spectrumId']);
        $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
        $mainCustomerInfo = $statement->execute()->next();
        return $mainCustomerInfo;
    }

    /**
     * @param $data
     * @param bool $dieFlag
     */
    private function debug($data, $dieFlag = false)
    {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        if ($dieFlag) {
            die;
        }
    }
    /**
     * @param $data
     */
    private function _updateUsersTMPWithData($data)
    {
        $this->_sqlAdmin->setTable("WEBSITE_USERS_TMP");

        $customerData = $this->_getMainCustomerInfoFromSpectrum($data);
        $addressPrimary = $this->_getMainAddressInfoFromSpectrum($customerData);
        $emailPrimary = $this->_getMainEmailInfoFromSpectrum($customerData);

        $insertData = array(
            'userIdPMS' => $data['userId'],
            'firstNamePMS' => $data['firstName'],
            'lastNamePMS' => $data['lastName'],
            'usernamePMS' => $data['userName'],
            'emailAddressPMS' => $data['emailAddress'],
            'statusPMS' => $data['status'],
            'userIdSPECTRUM' => $data['spectrumId'],
            'firstNameSPECTRUM' => $addressPrimary['forename'],
            'lastNameSPECTRUM' => $addressPrimary['surname'],
            'emailAddressSPECTRUM' => $emailPrimary['email']
        );

        $insert = $this->_sqlAdmin->insert()
            ->values($insertData);
        $statement = $this->_sqlAdmin->prepareStatementForSqlObject($insert);
        $statement->execute();
    }

    /**
     *
     */
    public function importUsersFromSpectrum()
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        $usersFromSpectrum = $this->_getUsersFromSpectrum();
        $usersToAddToPMS = $this->_removesExistingPmsUsers($usersFromSpectrum);

        $errors = $this->_insertNewUsersToPMS($usersToAddToPMS);

        $this->debug($errors);die;
    }

    /**
     * @param $usersToAddToPMS
     * @return array
     */
    private function _insertNewUsersToPMS($usersToAddToPMS)
    {
        $this->_sqlAdmin->setTable('WEBSITE_USERS');
        $error = array();

        foreach ($usersToAddToPMS as $user) {
            try {
                if (empty($user['ID']) || empty($user['email'])) {
                    $error[] = $user;
                    continue;
                }
                $pwdGenerator = new \PrismAuth\Adapter\Generator\BcryptPasswordGenerator();

                $password = array(
                    'password' => $pwdGenerator->generatePassword(Rand::getString('16')),
                    'passwordSalt' => Rand::getString('16'),
                );

                $insertData = array(
                    'spectrumID' => $user['ID'],
                    'userTypeId' => 3,
                    'firstName' => $user['forename'],
                    'lastName' => $user['surname'],
                    'userName' => $user['email'],
                    'emailAddress' => $user['email'],
                    'passwordSalt' => $password['passwordSalt'],
                    'password' => $password['password'],
                    'status' => 'ACTIVE',
                    'oldPassword' => '',
                    'resetedPassword' => 0
                );

                $insert = $this->_sqlAdmin->insert()
                    ->values($insertData);
                $statement = $this->_sqlAdmin->prepareStatementForSqlObject($insert);
                $statement->execute();
            } catch (\Exception $e) {
               continue;
            }

        }

        return $error;
    }

    /**
     * @param $usersFromSpectrum
     * @return mixed
     */
    private function _removesExistingPmsUsers($usersFromSpectrum)
    {
        $this->_sqlAdmin->setTable('WEBSITE_USERS');

        foreach ($usersFromSpectrum as $key => $spectrumUser) {
            $select = $this->_sqlAdmin->select()
                ->where('emailAddress = "' . $spectrumUser['email'] . '"');
            $statement = $this->_sqlAdmin->prepareStatementForSqlObject($select);
            $result = $statement->execute();

            if ($result->count() > 0) {
                unset($usersFromSpectrum[$key]);
            }
        }
        return $usersFromSpectrum;
    }

    /**
     * @return array
     */
    private function _getUsersFromSpectrum()
    {
        $this->_sqlSpectrum->setTable('CUSTOMER');
        $select = $this->_sqlSpectrum->select();
        $select->join(
            array('ce' => 'CUSTOMER_Emails'),
            new Expression("ce.ID = CUSTOMER.primaryEmailID"),
            array('email'),
            Select::JOIN_LEFT
        );
        $select->join(
            array('ca' => 'CUSTOMER_Addresses'),
            new Expression("ca.ID = CUSTOMER.primaryAddressID"),
            array('forename', 'surname'),
            Select::JOIN_LEFT
        );
        $select->order('CUSTOMER.ID ASC');
        $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $toImportToWeb = array();

        foreach ($results as $result) {
            if (!empty($result['primaryEmailID'])) {

                $ordersToSave = array();
                $this->_sqlSpectrum->setTable('ORDER_Headers');
                $select = $this->_sqlSpectrum->select()
                    ->where('channel = "WEB"')
                    ->where('customerID = ' . $result['ID']);
                $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
                $orders = $statement->execute();
                foreach ($orders as $order) {
                    $ordersToSave[] = $order;
                }
                if ($orders->count() > 0) {
                    $result['orders'] = $ordersToSave;
                    $toImportToWeb[] = $result;
                }

            }
        }
        return $toImportToWeb;
    }

    /**
     * @return mixed
     */
    public function syncSuppliers()
    {
        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();

        $this->_sqlPMS->setTable(array('v' => 'vsupply'));
        $select = $this->_sqlPMS->select();

        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $suppliers = $statement->execute();

        foreach ($suppliers as $supplierData) {
            $supplierData['CONTACT'] = str_replace("'","\\'", $supplierData['CONTACT']);
            $supplierData['COMPANY'] = str_replace("'","\\'", $supplierData['COMPANY']);
            $supplierData['ADD1'] = str_replace("'","\\'", $supplierData['ADD1']);
            $supplierData['ADD2'] = str_replace("'","\\'", $supplierData['ADD2']);
            $supplierData['ADD3'] = str_replace("'","\\'", $supplierData['ADD3']);
            $supplierData['ADD4'] = str_replace("'","\\'", $supplierData['ADD4']);
            $this->_sqlPMS->setTable(array('s' => 'SUPPLIERS'));
            $select1 = $this->_sqlPMS->select();
            $select1->where(array('s.code' => $supplierData['SUPACNO']));
            $statement1 = $this->_sqlPMS->prepareStatementForSqlObject($select1);
            $existingSuppliers = $statement1->execute();

            if ($existingSuppliers->count() == 1) {
                $existingSupplierData = $existingSuppliers->next();
                $supplierId = $existingSupplierData['supplierId'];
                // update this entry
                $query = "UPDATE `SUPPLIERS` SET
                  `merchandizeController`='{$supplierData['MC']}',
                  `merchandizeInitials`='{$supplierData['INITIALS']}',
                  `merchandizeControllerEmail`='{$supplierData['MCEMAIL']}',
                  `merchandizeControllerType`='{$supplierData['MCTYPE']}',
                  `contact`='{$supplierData['CONTACT']}',
                  `company`='{$supplierData['COMPANY']}',
                  `address1`='{$supplierData['ADD1']}',
                  `address2`='{$supplierData['ADD2']}',
                  `address3`='',
                  `city`='{$supplierData['ADD2']}',
                  `postcode`='{$supplierData['POSTCODE']}',
                  `county`='{$supplierData['ADD4']}',
                  `country`='{$supplierData['COUNTRY']}',
                  `phone`='{$supplierData['TELEPHONE']}',
                  `fax`='{$supplierData['FAX']}',
                  `email`='{$supplierData['EMAIL']}',
                  `status`='".($supplierData['ACTIVE'] =='T' ? 'ACTIVE' : 'INACTIVE')."',
                  `modified`='NOW()'
                WHERE (`supplierId`='{$supplierId}');";
                $this->_dbAdapter->query($query)->execute();

                $this->_sqlPMS->setTable(array('sd' => 'SUPPLIERS_Definitions'));
                foreach($this->_languages as $languageId) {
                    // check if the entry exists

                    $select2 = $this->_sqlPMS->select();
                    $select2->where(array(
                        'sd.supplierId' => $supplierId,
                        'sd.languageId' => $languageId
                    ));
                    $statement2 = $this->_sqlPMS->prepareStatementForSqlObject($select2);
                    $existingSuppliersDefinitions = $statement2->execute();

                    if ($existingSuppliersDefinitions->count() == 1) {
                        // update existing
                        $updQuery = "UPDATE `SUPPLIERS_Definitions` SET
                          `supplierName`='{$supplierData['COMPANY']}',
                          `description`='{$supplierData['COMPANY']}'
                        WHERE `supplierID`='{$supplierId}' AND languageId='{$languageId}';";
                        $this->_dbAdapter->query($updQuery)->execute();
                    } else {
                        // insert new
                        $insQuery = "INSERT INTO `SUPPLIERS_Definitions`
                          (`supplierId`, `languageId`, `supplierName`, `description`)
                          VALUES
                          ('$supplierId', '$languageId', '{$supplierData['COMPANY']}', '{$supplierData['COMPANY']}');";
                        $this->_dbAdapter->query($insQuery)->execute();
                    }
                }
            } else {
                // insert new entry
                $insQuery = "
                    INSERT INTO SUPPLIERS
                    (`merchandizeController`, `merchandizeInitials`, `merchandizeControllerEmail`, `merchandizeControllerType`, `contact`, `code`, `company`, `address1`, `address2`, `address3`, `city`, `postcode`, `county`, `country`, `phone`, `fax`, `email`, `status`, `modified`)
                    VALUES
                    ('{$supplierData['MC']}', '{$supplierData['INITIALS']}', '{$supplierData['MCEMAIL']}', '{$supplierData['MCTYPE']}', '{$supplierData['CONTACT']}', '{$supplierData['SUPACNO']}', '{$supplierData['COMPANY']}', '{$supplierData['ADD1']}', '{$supplierData['ADD2']}', '', '{$supplierData['ADD3']}', '{$supplierData['POSTCODE']}', '{$supplierData['ADD4']}', '{$supplierData['COUNTRY']}', '{$supplierData['TELEPHONE']}', '{$supplierData['FAX']}', '{$supplierData['EMAIL']}', '".($supplierData['ACTIVE'] =='T' ? 'ACTIVE' : 'INACTIVE')."', 'NOW()');
                ";
                $this->_dbAdapter->query($insQuery)->execute();
                $supplierId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
                foreach($this->_languages as $languageId) {
                    // insert new
                    $insQuery = "INSERT INTO `SUPPLIERS_Definitions`
                          (`supplierId`, `languageId`, `supplierName`, `description`)
                          VALUES
                          ('$supplierId', '$languageId', '{$supplierData['COMPANY']}', '{$supplierData['COMPANY']}');";
                    $this->_dbAdapter->query($insQuery)->execute();
                }
            }
        }

        $this->_dbAdapter->getDriver()->getConnection()->commit();
    }

    /**
     * @param $result
     * @return string
     */
    public function getSizeForVariantPMS($result)
    {
        $size = '-';
        $this->_sqlPMS->setTable(array('prk' => 'PRODUCTS_R_ATTRIBUTES_R_SKU_Rules'));
        $select = $this->_sqlPMS->select();
        $select->where(array('productAttributeId' => $result['productId']));
        $select->where('skuRuleTable != "COLOURS_Groups"');

        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $sizes = $statement->execute()->next();

        if (!empty($sizes['skuRuleTable'])) {
            $this->_sqlPMS->setTable(array('tb' => $sizes['skuRuleTable']));
            $select = $this->_sqlPMS->select()
                ->join(
                    array('sku' => 'SIZE_SKU_Converter'),
                    "tb.uk = sku.sizeAlphabetic",
                    array('*')
                );
            $select->where('tb.sizeId = ' . $sizes['skuRuleId']);
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $sizeResult = $statement->execute()->next();

            if (!empty($sizeResult['skuCode'])) {
                $size = $sizeResult['skuCode'];
            }
        }
        return $size;
    }

    public function importRelatedProductsForLCM()
    {
        try {
            $this->_sqlPMS->setTable('lcm_temp_import');
            $select = $this->_sqlPMS->select();
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            foreach ($results as $result) {
                /** Get variant id */
                $this->_sqlPMS->setTable('PRODUCTS_R_ATTRIBUTES');
                $select = $this->_sqlPMS->select()
                    ->where('sku = "' . $result['sku'] . '"')
                    ->limit(1);
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
                $variant = $statement->execute()->next();

                if (!empty($variant['productAttributeId'])) {
                    $variantId = $variant['productAttributeId'];
                    $productId = $variant['productId'];

                    $this->_sqlPMS->setTable('PRODUCTS_Common_Attributes');
                    $deleteAction = $this->_sqlPMS->delete()->where(array(
                        'productOrVariantId' => $variantId,
                        'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                        'commonAttributeId' => 28
                    ));
                    $statement2 = $this->_sqlPMS->prepareStatementForSqlObject($deleteAction);
                    $statement2->execute();

                    $this->_sqlPMS->setTable('PRODUCTS_Common_Attributes');
                    $deleteAction = $this->_sqlPMS->delete()->where(array(
                        'productOrVariantId' => $productId,
                        'productIdFrom' => 'PRODUCTS',
                        'commonAttributeId' => 28
                    ));
                    $statement2 = $this->_sqlPMS->prepareStatementForSqlObject($deleteAction);
                    $statement2->execute();

                    $relatedSkus = explode(',', $result['relatedProducts']);

                    foreach($relatedSkus as $related) {
                        $related = trim($related);
                        /** Insert */
                        $data = array(
                            'productOrVariantId' => $variantId,
                            'productIdFrom' => "PRODUCTS_R_ATTRIBUTES",
                            'commonAttributeId' => 28,
                            'commonAttributeValue' => $related
                        );
                        $this->_sqlPMS->setTable("PRODUCTS_Common_Attributes");
                        $insert = $this->_sqlPMS->insert()
                            ->values($data);
                        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                        $statement->execute();

                        $data = array(
                            'productOrVariantId' => $productId,
                            'productIdFrom' => "PRODUCTS",
                            'commonAttributeId' => 28,
                            'commonAttributeValue' => $related
                        );
                        $this->_sqlPMS->setTable("PRODUCTS_Common_Attributes");
                        $insert = $this->_sqlPMS->insert()
                            ->values($data);
                        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                        $statement->execute();
                    }

                }

                $a = ' ';

            }

        } catch (\Exception $ex) {


        }
    }

    public function importProductsForLCM()
    {
        try {
            $awardsMapping = $this->getAwardsMapping();

            /** Retrieve all products from TMP table */
            $this->_sqlPMS->setTable('lcm_temp_import');
            $select = $this->_sqlPMS->select();
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $products = $statement->execute();

            $finalProductsArray = array();
            foreach ($products as $product) {

                $awardAttribute = array();

                foreach ($awardsMapping as $key => $award) {
                    if (!empty($product[$key])) {
                        $awardAttribute[] = $award;
                    }
                }

                $relatedProducts = explode(',', $product['relatedProducts']);

                $webcategories = array();
                if (!empty($product['webcategories'])) {
                    $webcategories = explode(',', $product['webcategories']);
                    foreach ($webcategories as &$category) {
                        $category = trim($category);
                    }
                }

                $tmpProductArray = array(
                    'barcode' => $product['barcode'],
                    'style' => $product['styleCode'],
                    'colour' => array(
                        'colourCode' => $product['colour']
                    ),
                    'size' => array(
                        'sizeCode' => $product['size']
                    ),
                    'sku' => $product['sku'],
                    'productName' => $product['title'],
                    'active' => 0,
                    'prices' => array(
                        'GBP' => array(
                            'nowPrice' => $product['price'],
                            'wasPrice' => '0.00'
                        )
                    ),
                    'productShortDescription' => $product['shortDescription'],
                    'productLongDescription' => $product['longDescription'],
                    'relatedProducts' => $relatedProducts,
                    'attributes' => array(
                        'awards' => $awardAttribute,
                        'brand' => trim($product['brand']),
                    ),
                    'styleGroup' => array()
                );
                if (!empty($webcategories)) {
                    $tmpProductArray['attributes']['webCategories'] = $webcategories;
                }

                $this->productsMapper->createVariantFromDropship($tmpProductArray);
                $finalProductsArray[] = $tmpProductArray;
            }

            die('here');
        } catch (\Exception $ex) {
            var_dump($ex->getMessage());die;
        }
    }
    public function deleteCategoriesTemp()
    {

        try {
            $this->_sqlPMS->setTable('test');
            $select = $this->_sqlPMS->select();
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            foreach ($results as $row) {
                $sku  = $row['sku'];
                $category = $row['category'];

                $this->_sqlPMS->setTable('PRODUCTS_R_ATTRIBUTES');
                $select = $this->_sqlPMS->select()
                    ->where(array(
                        'sku' => $sku
                ))->limit(1);
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
                $product = $statement->execute()->next();
                if (empty($product)) {
                    continue;
                }
                $variantId = $product['productAttributeId'];
                $productId = $product['productId'];

                $this->_sqlPMS->setTable('PRODUCTS_Common_Attributes');
                $deleteAction = $this->_sqlPMS->delete()->where(array(
                    'productOrVariantId' => $variantId,
                    'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                    'commonAttributeId' => 35, // Attribute for web categories
                    'commonAttributeValue' => $category
                ));
                $statement2 = $this->_sqlPMS->prepareStatementForSqlObject($deleteAction);
                $statement2->execute();

                /** PRODUCTS */
                $this->_sqlPMS->setTable('PRODUCTS_Common_Attributes');
                $deleteAction = $this->_sqlPMS->delete()->where(array(
                    'productOrVariantId' => $productId,
                    'productIdFrom' => 'PRODUCTS',
                    'commonAttributeId' => 35, // Attribute for web categories
                    'commonAttributeValue' => $category
                ));
                $statement2 = $this->_sqlPMS->prepareStatementForSqlObject($deleteAction);
                $statement2->execute();

                /** Add entry to QUEUE  */
                $queueData = array(
                    'processed' => 0,
                    'processing' => 0,
                    'priority' => 0,
                    'queueEntryType' => 5,
                    'queueEntryId' => $variantId,
                    'queueEntryBody' => json_encode(array(
                        'optionCode' => $sku,
                        'sku' => $sku,
                        'style' => $sku
                    )),
                    'queueReason' => 3
                );

                $this->_sqlPMS->setTable("QUEUE_Manager");
                $insert = $this->_sqlPMS->insert()
                    ->values($queueData);
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();
            }
            die('done');
        } catch (\Exception $ex) {
            var_dump($ex->getMessage());die;
        }
    }

    public function insertCategoriesTemp()
    {
        try {
            $this->_sqlPMS->setTable('inport');
            $select = $this->_sqlPMS->select();
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            foreach ($results as $re) {
                $sku = $re['sku'];

                $categories = explode(',', $re['categories']);

                foreach ($categories as $category) {
                    $category = trim($category);
                    $query = "SELECT
                        cavd.*, cav.commonAttributeId
                    FROM
                        COMMON_ATTRIBUTES_VALUES_Definitions cavd
                    INNER JOIN COMMON_ATTRIBUTES_VALUES cav ON cavd.commonAttributeValueId = cav.commonAttributeValueId AND commonAttributeId = 35
                    WHERE
                    cavd.`value` = \"{$category}\"";
                    $statement = $this->_dbAdapter->query($query);
                    $results = $statement->execute();

                    if (count($results) !== 1) {
                        $a = '';
                        continue;
                    }
                    $category = $results->next();

                    $query = "SELECT * FROM PRODUCTS_R_ATTRIBUTES WHERE sku = '{$sku}'";
                    $statement = $this->_dbAdapter->query($query);
                    $variant = $statement->execute()->next();
                    if (!$variant) {
                        continue;
                    }
                    $query = "SELECT * FROM PRODUCTS WHERE style = '{$sku}'";
                    $statement = $this->_dbAdapter->query($query);
                    $product = $statement->execute()->next();
                    if (!$product) {
                        continue;
                    }

                    $this->_sqlPMS->setTable("PRODUCTS_Common_Attributes");
                    $insert = $this->_sqlPMS->insert()
                        ->values(array(
                            'productOrVariantId' => $variant['productAttributeId'],
                            'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                            'commonAttributeId' => $category['commonAttributeId'],
                            'commonAttributeValue' => $category['commonAttributeValueId']
                        ));
                    $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                    $statement->execute();

                    $this->_sqlPMS->setTable("PRODUCTS_Common_Attributes");
                    $insert = $this->_sqlPMS->insert()
                        ->values(array(
                            'productOrVariantId' => $product['productId'],
                            'productIdFrom' => 'PRODUCTS',
                            'commonAttributeId' => $category['commonAttributeId'],
                            'commonAttributeValue' => $category['commonAttributeValueId']
                        ));
                    $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                    $statement->execute();
                }
            }
        } catch (\Exception $ex) {
            return false;
        }

       return true;
    }
    /**
     *
     */
    public function migrateAttributesFromFeed()
    {
        $doc = new \DOMDocument();
        $doc->load('http://www.preciouslittleone.com/feed/ed_attlist.php');
        $attributes = $doc->getElementsByTagName("attribute");
        foreach ($attributes as $attribute) {
            $textContent = $attribute->textContent;
            $exploded = explode("\n\n", $textContent);

            $attributeName = trim($exploded[0]);
            $attributeValues = explode("\n", $exploded[1]);

            switch($attributeName) {
                case 'Categories':
                    $attributeNames = array('Dropship Category', 'Dropship Sub Category');
                    foreach ($attributeNames as $attributeName) {
                        $commonAttributeId = $this->createCommonAttribute($attributeName);
                        $this->createCommonAttributeValues($commonAttributeId, $attributeValues);
                    }
                    break;
                case 'Size':
                    $commonAttributeId = $this->createCommonAttribute($attributeName, true);
                    $this->createCommonAttributeValues($commonAttributeId, $attributeValues);
                    break;
                case 'Colour':
                    $this->createNewColours($attributeValues);
                    break;
                default:
                    $commonAttributeId = $this->createCommonAttribute($attributeName);
                    $this->createCommonAttributeValues($commonAttributeId, $attributeValues);
                    break;
            }
        }
        die('done');
    }

    /**
     * @param int $characters
     * @param int $length
     * @return string
     */
    public function generateRandomString($characters = null, $length = 3)
    {
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @param $colours
     */
    private function createNewColours($colours)
    {
        foreach ($colours as $colour) {
            try {


                $this->_sqlPMS->setTable('COLOURS_Groups_Definitions');
                $select = $this->_sqlPMS->select()
                    ->where(array('name' => $colour));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
                $count = $statement->execute()->count();
                if ($count == 0) {
                    $colourCode = strtoupper($this->generateRandomString($colour));
                    // we need to persist them all as groups in the database for each language
                    $this->_sqlPMS->setTable("COLOURS_Groups");
                    $insert = $this->_sqlPMS->insert()
                        ->values(array(
                            'groupCode' => $colourCode,
                            'hasImage' => 0,
                            'hex' => '#000000',
                            'enabled' => 1
                        ));
                    $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                    $statement->execute();
                    $colourGroupId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

                    $this->_sqlPMS->setTable("COLOURS");
                    $insert = $this->_sqlPMS->insert()
                        ->values(array(
                            'colourCode' => $colourCode,
                            'hex' => '#000000',
                        ));
                    $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                    $statement->execute();
                    $colourId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

                    foreach ($this->_languages as $languageId) {
                        $this->_sqlPMS->setTable("COLOURS_Groups_Definitions");
                        $insert = $this->_sqlPMS->insert()
                            ->values(array(
                                'colourGroupId' => $colourGroupId,
                                'languageId' => $languageId,
                                'name' => $colour
                            ));
                        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                        $statement->execute();

                        $this->_sqlPMS->setTable("COLOURS_Definitions");
                        $insert = $this->_sqlPMS->insert()
                            ->values(array(
                                'colourId' => $colourId,
                                'languageId' => $languageId,
                                'colourName' => $colour
                            ));
                        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                        $statement->execute();
                    }
                    $this->_sqlPMS->setTable("COLOURS_R_GROUPS");
                    $insert = $this->_sqlPMS->insert()
                        ->values(array(
                            'colourId' => $colourId,
                            'colourGroupId' => $colourGroupId,
                        ));
                    $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);

                    $statement->execute();

                }
            } catch (\Exception $ex) {
                var_dump($ex->getMessage());die;
            }
        }


    }

    /**
     * @param $commonAttributeId
     * @param $attributeValues
     */
    public function createCommonAttributeValues($commonAttributeId, $attributeValues)
    {
        $nr = 0;
        foreach ($attributeValues as $attributeValue) {

            $existingCommonAttributeValueId = $this->getCommonAttributeValue($commonAttributeId, $attributeValue);
            if (!$existingCommonAttributeValueId) {
                $attributeValue = trim($attributeValue);
                $this->createCommonAttributeValue($commonAttributeId, $attributeValue, $nr);
            }
            $nr++;
        }
    }

    /**
     * @param $commonAttributeId
     * @param $attributeValue
     * @param $nr
     */
    public function createCommonAttributeValue($commonAttributeId, $attributeValue, $nr)
    {
        try {
            $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();

            $commonAttributeData = array(
                'commonAttributeId' => $commonAttributeId,
                'enabled' => 1,
                'territoryId' => 1
            );
            $this->_sqlPMS->setTable('COMMON_ATTRIBUTES_VALUES');
            $insert = $this->_sqlPMS->insert()
                ->values($commonAttributeData);
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();
            $commonAttributeValueId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

            if (!$commonAttributeValueId) {
                throw new \Exception('No common attribute value Id');
            }

            $commonAttributeData = array(
                'commonAttributeValueId' => $commonAttributeValueId,
                'languageId' => 1,
                'value' => $attributeValue
            );
            $this->_sqlPMS->setTable('COMMON_ATTRIBUTES_VALUES_Definitions');
            $insert = $this->_sqlPMS->insert()
                ->values($commonAttributeData);
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();


            $commonAttributeData = array(
                'commonAttributeValueId' => $commonAttributeValueId,
                'commonAttributeValueOrder' => $nr,
                'pmsDisplay' => 1,
                'shopDisplay' => 1
            );
            $this->_sqlPMS->setTable('COMMON_ATTRIBUTES_VALUES_GUI_Representation');
            $insert = $this->_sqlPMS->insert()
                ->values($commonAttributeData);
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();

            $this->_dbAdapter->getDriver()->getConnection()->commit();
        } catch (\Exception $ex) {
            $this->_dbAdapter->getDriver()->getConnection()->rollback();
            var_dump($ex->getMessage());die;
        }
    }

    /**
     * @param $commonAttributeId
     * @param $attributeValue
     */
    public function getCommonAttributeValue($commonAttributeId, $attributeValue)
    {
        $this->_sqlPMS->setTable(array('cav' => 'COMMON_ATTRIBUTES_VALUES'));
        $select = $this->_sqlPMS->select()
            ->columns(array('commonAttributeValueId'))
            ->join(
                array('cavd' => 'COMMON_ATTRIBUTES_VALUES_Definitions'),
                "cavd.commonAttributeValueId = cav.commonAttributeValueId",
                array('*')
            )
            ->where(array(
                'cav.commonAttributeId' => $commonAttributeId,
                'cavd.value' => $attributeValue
            ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        return $statement->execute()->next();
    }

    /**
     * @param $attributeName
     * @param bool $isSize
     * @return int
     */
    public function createCommonAttribute($attributeName, $isSize = false)
    {
        $existingCommonAttribute = $this->getCommonAttributeByName($attributeName);

        if (!$existingCommonAttribute) {
            $commonAttributeId = $this->createNewCommonAttribute($attributeName, true);
        } else {
            $commonAttributeId = $existingCommonAttribute['commonAttributeId'];
        }
        return $commonAttributeId;
    }

    /**
     * @param $attributeName
     * @param bool $isSize
     * @return int
     */
    public function createNewCommonAttribute($attributeName, $isSize = false)
    {
        try {
            $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();

            $commonAttributeData = array(
                'enabled' => 1,
                'isSize' => $isSize
            );
            $this->_sqlPMS->setTable('COMMON_ATTRIBUTES');
            $insert = $this->_sqlPMS->insert()
                ->values($commonAttributeData);
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();
            $commonAttributeId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

            if (!$commonAttributeId) {
                throw new \Exception('No common attribute Id');
            }

            $commonAttributeData = array(
                'commonAttributeId' => $commonAttributeId,
                'languageId' => 1,
                'description' => $attributeName
            );
            $this->_sqlPMS->setTable('COMMON_ATTRIBUTES_Definitions');
            $insert = $this->_sqlPMS->insert()
                ->values($commonAttributeData);
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();

            $commonAttributeData = array(
                'commonAttributeId' => $commonAttributeId,
                'commonAttributesViewTypeId' => 3,
                'commonAttributesParentId' => $commonAttributeId,
                'pmsDisplay' => 1,
                'shopDisplay' => 1,

            );
            $this->_sqlPMS->setTable('COMMON_ATTRIBUTES_GUI_Representation');
            $insert = $this->_sqlPMS->insert()
                ->values($commonAttributeData);
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();


            $this->_dbAdapter->getDriver()->getConnection()->commit();

            return $commonAttributeId;
        } catch (\Exception $ex) {
            $this->_dbAdapter->getDriver()->getConnection()->rollback();
            var_dump($ex->getMessage());die;
        }

    }

    /**
     * @param $attributeName
     */
    public function getCommonAttributeByName($attributeName)
    {
        $this->_sqlPMS->setTable($this->commonAttributesDefinitionsTable);
        $select = $this->_sqlPMS->select()
            ->columns(array('commonAttributeId'))
            ->where(array('description' => $attributeName));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        return $statement->execute()->next();
    }


    /**
     *
     */
    public function migrateProductsFromSpectrum()
    {
        $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=0;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Barcodes;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Common_Attributes;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Definitions;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_Prices;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_R_ATTRIBUTES;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_R_ATTRIBUTES_R_SKU_Rules;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_R_CHANNELS;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_R_CLIENTS_Websites;")->execute();
        $this->_dbAdapter->query("TRUNCATE PRODUCTS_R_SPECTRUM;")->execute();
        $this->_dbAdapter->query("SET FOREIGN_KEY_CHECKS=1;")->execute();

        $this->_sqlSpectrum->setTable('PRODUCTS');
        $select = $this->_sqlSpectrum->select()->where(array('productType' => 'STANDARD'));

        $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
        $spectrumProducts = $statement->execute();

        $count = count($spectrumProducts);
        $nrCrt = 1;
        foreach ($spectrumProducts as $product) {
            try {
                echo 'Processing ' . $nrCrt . ' out of ' . $count . "\r";
                /** get product Attributes **/
                $this->_sqlSpectrum->setTable(array('pa' => 'PRODUCTS_Attributes'));
                $select = $this->_sqlSpectrum->select()
                    ->join(
                        array('pad' => 'PRODUCTS_AttributesDefinitions'),
                        "pad.ID = pa.ID",
                        array('*')
                    )
                    ->where(array('pa.productID' => $product['ID']));
                $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
                $attributes = $statement->execute();

                $productAttributes = array();
                foreach ($attributes as $attribute) {
                    switch ($attribute['ID']) {
                        case 1:
                            $productAttributes[7] = $attribute['value'];
                            break;
                        case 2:
                            $productAttributes[8] = $attribute['value'];
                            break;
                        case 3:
                            $productAttributes[9] = $attribute['value'];
                            break;
                        case 4:
                            $productAttributes[10] = $attribute['value'];
                            break;
                    }
                }
                $productAttributes[4] = $product['allowExpress'];
                $productAttributes[5] = $product['allowBackOrders'];
                $productAttributes[6] = $product['screeningQty'];

                $productData = array_merge($product, array('attributes' => $productAttributes));


                $this->_sqlSpectrum->setTable('PRODUCTS_PriceList');
                $select = $this->_sqlSpectrum->select()->where(array('pricelist' => '1', 'productID' => $productData['ID']));
                $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
                $prices = $statement->execute();

                foreach ($prices as $price) {
                    $productData['prices'][] = $price;
                }

                $this->createNewProduct($productData);
                $nrCrt++;
            } catch (\Exception $ex) {
                var_dump($product, $ex->getMessage());die;
            }
        }
    }

    /**
     * @param $productData
     */
    public function createNewProduct($productData)
    {
        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();

        try {
            /** Check if the style is created and create it if not */
            $styleId = $this->createStyleFromData($productData);

            $variantId = $this->createSKU($productData, $styleId);

        } catch (\Exception $ex) {
            var_dump($ex->getMessage());die;

        }

        $this->_dbAdapter->getDriver()->getConnection()->commit();
    }

    /**
     * @param $productData
     * @param $styleId
     */
    private function createSKU($productData, $styleId)
    {
        try {

            $skuData = array(
                'productId' => $styleId,
                'ean13' => $productData['barcode'],
                'sku' => $productData['productCode'],
                'status' => $productData['active'] == 1 ? 'ACTIVE' : 'INACTIVE'
            );
            $this->_sqlPMS->setTable('PRODUCTS_R_ATTRIBUTES');
            $insert = $this->_sqlPMS->insert()
                ->values($skuData);
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();
            $variantId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

            try {
                /** Use the barcode */
                $barcodeData = array(
                    'barcode' => $productData['barcode'],
                    'inUse' => 1
                );
                $this->_sqlPMS->setTable('PRODUCTS_Barcodes');
                $insert = $this->_sqlPMS->insert()
                    ->values($barcodeData);
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();
            } catch (\Exception $ex) {

                echo 'Duplicate Barcode for product ' . $productData['productCode'] .' : ' . $productData['barcode'] . "\r\n";
            }


            /** Create definition **/
            $definitionArray = array(
                'languageId' => 1,
                'versionId' => 1,
                'shortProductName' => $productData['description'],
                'productName'=> $productData['description'],
                'status' => 'ACTIVE',
                'productId' =>  $variantId,
                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES'
            );
            $this->createProductDefinitions($definitionArray);
//
            /** Create attributes **/
            foreach ($productData['attributes'] as $commonAttributeId => $attributeValue) {
                $commonAttrArray = array(
                    'productOrVariantId' => $variantId,
                    'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                    'commonAttributeId' => $commonAttributeId,
                    'commonAttributeValue' => $attributeValue
                );
                $this->insertProductsRCommonAttributes($commonAttrArray);
            }

            /** Create prices */
            foreach ($productData['prices'] as $price) {
                $territoryId = $this->getTerritoryByString($price['territory']);
                $currencyId = $this->getCurrencyByString($price['currency']);

                if ($territoryId AND $currencyId) {
                    $priceArray = array(
                        'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                        'productOrVariantId' => $variantId,
                        'regionId' => $territoryId,
                        'currencyId' => $currencyId,
                        'price' => $price['price'],
                        'nowPrice' => $price['price'],
                        'wasPrice' => $price['wasPrice']
                    );
                    $this->insertProductPrice($priceArray);
                }
            }

            /** Products R Spectrum */
            $dataArray = array(
                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                'productId' => $variantId,
                'spectrumId' => $productData['ID'],
                'style' => $productData['style'],
            );
            $this->_sqlPMS->setTable('PRODUCTS_R_SPECTRUM');
            $insert = $this->_sqlPMS->insert()
                ->values($dataArray);
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();

            /** Insert PRODUCTS_R_ATTRIBUTES_R_SKU_RULES */

            $explodedSku = explode('-', $productData['productCode']);
            if (!empty($explodedSku)) {
                unset($explodedSku[0]);
                unset($explodedSku[1]);

            }

                        $baseSize = '';
            foreach ($explodedSku as $sizePart) {
                $baseSize .= $sizePart . '-';
            }
            $skuCode = trim($baseSize, '-');

            $this->_sqlPMS->setTable('COMMON_ATTRIBUTES_VALUES');
            $select = $this->_sqlPMS->select()->where(array('commonAttributeValueSkuCode' => $skuCode));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $commonAttributeValue = $statement->execute()->next();

            $sizeValueId = $commonAttributeValue['commonAttributeValueId'];


            $this->_sqlPMS->setTable('COLOURS_Groups');
            $select = $this->_sqlPMS->select()->where(array('groupCode' => $productData['colour']));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $colour = $statement->execute()->next();

            $colourId = $colour['colourGroupId'];

            if (!empty($sizeValueId)) {
                $sizeSkuData = array(
                    'productAttributeId' => $variantId,
                    'skuRuleId' => $sizeValueId,
                    'skuRuleTable' => 'COMMON_ATTRIBUTES_VALUES',
                    'isCommonAttribute' => 1
                );
                $this->insertProductsRAttributesRSkuRules($sizeSkuData);
            }

            if (!empty($colourId)) {
                $colourSkuData = array(
                    'productAttributeId' => $variantId,
                    'skuRuleId' => $colourId,
                    'skuRuleTable' => 'COLOURS_Groups',
                    'isCommonAttribute' => 1
                );
                $this->insertProductsRAttributesRSkuRules($colourSkuData);
            }

        } catch (\Exception $ex) {
            var_dump($ex->getMessage());die;
        }
    }

    /**
     * @param $sizeSkuData
     */
    private function insertProductsRAttributesRSkuRules($sizeSkuData)
    {
        $this->_sqlPMS->setTable('PRODUCTS_R_ATTRIBUTES_R_SKU_Rules');
        $insert = $this->_sqlPMS->insert()
            ->values($sizeSkuData);
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
        $statement->execute();
    }

    /**
     * @param $productData
     * @return int
     */
    private function createStyleFromData($productData)
    {

        $this->_sqlPMS->setTable('PRODUCTS');
        $select = $this->_sqlPMS->select()->where(array('style' => $productData['style']));

        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $style = $statement->execute()->next();

        if (!$style) {
            /** no style is present - we need to create the style */

            $styleArray = array(
                'productTypeId' => 1,
                'taxId' => 0,
                'productMerchantCategoryId' => null,
                'ean13' => '',
                'style' => $productData['style'],
                'status' => 'ACTIVE',
            );
            $this->_sqlPMS->setTable('PRODUCTS');
            $insert = $this->_sqlPMS->insert()
                ->values($styleArray);
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
            $statement->execute();
            $styleId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

            /** Create definition **/
            $definitionArray = array(
                'languageId' => 1,
                'versionId' => 1,
                'shortProductName' => $productData['description'],
                'productName'=> $productData['description'],
                'status' => 'ACTIVE',
                'productId' =>  $styleId,
                'productIdFrom' => 'PRODUCTS'
            );
            $this->createProductDefinitions($definitionArray);

            /** Create attributes **/
            foreach ($productData['attributes'] as $commonAttributeId => $attributeValue) {
                $commonAttrArray = array(
                    'productOrVariantId' => $styleId,
                    'productIdFrom' => 'PRODUCTS',
                    'commonAttributeId' => $commonAttributeId,
                    'commonAttributeValue' => $attributeValue
                );
                $this->insertProductsRCommonAttributes($commonAttrArray);
            }

            /** Create prices */
            foreach ($productData['prices'] as $price) {
                $territoryId = $this->getTerritoryByString($price['territory']);
                $currencyId = $this->getCurrencyByString($price['currency']);

                if ($territoryId AND $currencyId) {
                    $priceArray = array(
                        'productIdFrom' => 'PRODUCTS',
                        'productOrVariantId' => $styleId,
                        'regionId' => $territoryId,
                        'currencyId' => $currencyId,
                        'price' => $price['price'],
                        'nowPrice' => $price['price'],
                        'wasPrice' => $price['wasPrice']
                    );
                    $this->insertProductPrice($priceArray);
                }
            }

            /** Add channels */
            for ($i=1; $i<4; $i++) {
                $this->_sqlPMS->setTable('PRODUCTS_R_CHANNELS');
                $insert = $this->_sqlPMS->insert()
                    ->values(array('productId' => $styleId, 'channelId' => $i, 'status' => 'ACTIVE'));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();
            }
            return $styleId;
        }

        return $style['productId'];
    }


    public function importCostPrice()
    {
        $this->_sqlPMS->setTable('cost_import');
        $select = $this->_sqlPMS->select();
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $result) {
            $this->_sqlPMS->setTable('PRODUCTS_R_ATTRIBUTES');
            $select = $this->_sqlPMS->select()->where(array('sku' => $result['sku']));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $variant = $statement->execute()->next();
            $costPrice = $result['cost'];
            if ($variant && $costPrice) {
                $variantId = $variant['productAttributeId'];

                $updateData = array('costPrice' => $costPrice);
                $update = $this->_sqlPMS->update();
                $update->table('PRODUCTS_Prices');
                $update->set($updateData);
                $update->where(array(
                    'productOrVariantId' => $variantId,
                    'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                    'regionId' => 1,
                    'currencyId' => 1
                ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($update);
                $statement->execute();
                /** Add entry to QUEUE  */
                $queueData = array(
                    'processed' => 0,
                    'processing' => 0,
                    'priority' => 0,
                    'queueEntryType' => 8,
                    'queueEntryId' => $variantId,
                    'queueEntryBody' => '{}',
                    'queueReason' => 3
                );

                $this->_sqlPMS->setTable("QUEUE_Manager");
                $insert = $this->_sqlPMS->insert()
                    ->values($queueData);
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();
            }

        }

        return true;
    }



    public function changeLongDescription()
    {
        $this->_sqlPMS->setTable('PRODUCTS_Definitions');
        $select = $this->_sqlPMS->select();
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $result) {
            $originalLongDescription = $result['longDescription'];
            $modifiedLongDescription = preg_replace("/\r\n|\r|\n/",'<br/>',$originalLongDescription);
            $updateData = array('longDescription' => $modifiedLongDescription);
            $update = $this->_sqlPMS->update();
            $update->table('PRODUCTS_Definitions');
            $update->set($updateData);
            $update->where(array(
                'productDefinitionsId' => $result['productDefinitionsId'],
            ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($update);
            $statement->execute();
        }
        return true;
    }
    public function importAdditionalInfoFromPLO()
    {
        $this->_sqlPMS->setTable('DROPSHIP_Products');
        $select = $this->_sqlPMS->select();
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $result) {
            $result['data'] = json_decode($result['data'], true);
            $technicalInformation = $result['data']['attributes']['techInformation'];
            $longDescription = $result['data']['productDescription'];

            $this->_sqlPMS->setTable('PRODUCTS_R_ATTRIBUTES');
            $select = $this->_sqlPMS->select()->where(array('sku' => $result['sku']))->limit(1);
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $variant = $statement->execute()->next();

            if (!empty($variant)) {
                $variantId = $variant['productAttributeId'];
                $productId = $variant['productId'];
                $technicalInformationId = 49;

                /** Delete variant technical information  */
                $this->_sqlPMS->setTable('PRODUCTS_Common_Attributes');
                $deleteAction = $this->_sqlPMS->delete()->where(
                    array(
                        'productOrVariantId' => $variantId,
                        'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                        'commonAttributeId' => $technicalInformationId
                    )
                );
                $statement2 = $this->_sqlPMS->prepareStatementForSqlObject($deleteAction);
                $statement2->execute();

                /** Delete product technical information  */
                $this->_sqlPMS->setTable('PRODUCTS_Common_Attributes');
                $deleteAction = $this->_sqlPMS->delete()->where(
                    array(
                        'productOrVariantId' => $productId,
                        'productIdFrom' => 'PRODUCTS',
                        'commonAttributeId' => $technicalInformationId
                    )
                );
                $statement2 = $this->_sqlPMS->prepareStatementForSqlObject($deleteAction);
                $statement2->execute();

                /** Insert variant technical information */
                $commonAttributesValuesArray = array(
                    'productOrVariantId' => $variantId,
                    'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                    'commonAttributeId' => $technicalInformationId,
                    'commonAttributeValue' => $technicalInformation
                );
                $this->_sqlPMS->setTable('PRODUCTS_Common_Attributes');
                $insert = $this->_sqlPMS->insert()
                    ->values($commonAttributesValuesArray);
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();

                /** Insert product technical information */
                $commonAttributesValuesArray = array(
                    'productOrVariantId' => $productId,
                    'productIdFrom' => 'PRODUCTS',
                    'commonAttributeId' => $technicalInformationId,
                    'commonAttributeValue' => $technicalInformation
                );
                $this->_sqlPMS->setTable('PRODUCTS_Common_Attributes');
                $insert = $this->_sqlPMS->insert()
                    ->values($commonAttributesValuesArray);
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();

                /** Update variant description */
                $updateData = array('longDescription' => $longDescription);
                $update = $this->_sqlPMS->update();
                $update->table('PRODUCTS_Definitions');
                $update->set($updateData);
                $update->where(array(
                    'productId' => $variantId,
                    'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES'
                ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($update);
                $statement->execute();
                /** Update product description */
                $updateData = array('longDescription' => $longDescription);
                $update = $this->_sqlPMS->update();
                $update->table('PRODUCTS_Definitions');
                $update->set($updateData);
                $update->where(array(
                    'productId' => $productId,
                    'productIdFrom' => 'PRODUCTS'
                ));
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($update);
                $statement->execute();
            }
        }

    }
    /**
     * @param $data
     */
    private function insertProductPrice($data)
    {
        $this->_sqlPMS->setTable('PRODUCTS_Prices');
        $insert = $this->_sqlPMS->insert()
            ->values($data);
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
        $statement->execute();
    }

    /**
     * @param $territoryIso
     * @return bool
     */
    private function getTerritoryByString($territoryIso)
    {
        $this->_sqlPMS->setTable('TERRITORIES');
        $select = $this->_sqlPMS->select()->where(array('iso2Code' => $territoryIso))->limit(1);
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $territory = $statement->execute()->next();
        if ($territory) {
            return $territory['territoryId'];
        }
        return false;
    }

    /**
     * @param $currencyIso
     * @return bool
     */
    private function getCurrencyByString($currencyIso)
    {
        $this->_sqlPMS->setTable('CURRENCY');
        $select = $this->_sqlPMS->select()->where(array('isoCode' => $currencyIso))->limit(1);
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $currency = $statement->execute()->next();
        if ($currency) {
            return $currency['currencyId'];
        }
        return false;
    }

    /**
     * @param $commonAttrArray
     */
    private function insertProductsRCommonAttributes($commonAttrArray)
    {
        $this->_sqlPMS->setTable('PRODUCTS_Common_Attributes');
        $insert = $this->_sqlPMS->insert()
            ->values($commonAttrArray);
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
        $statement->execute();
    }

    /**
     * @param $definitionArray
     */
    private function createProductDefinitions($definitionArray)
    {
        $this->_sqlPMS->setTable('PRODUCTS_Definitions');
        $insert = $this->_sqlPMS->insert()
            ->values($definitionArray);
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
        $statement->execute();
    }

    /**
     *
     */
    public function migrateSpectrumSizes()
    {
        /**
         * 1 => sizes numeric
         * 2 -> sizes alfa
         * 3 -> sizes calf
         */

        $this->_sqlSpectrum->setTable('PRODUCTS');
        $select = $this->_sqlSpectrum->select();
        $statement = $this->_sqlSpectrum->prepareStatementForSqlObject($select);
        $spectrumProducts = $statement->execute();

        foreach ($spectrumProducts as $product) {
            if ($product['productType'] == 'STANDARD') {
                $explodedSku = explode('-', $product['productCode']);
                if (!empty($explodedSku)) {
                    unset($explodedSku[0]);
                    unset($explodedSku[1]);

                }

                /** Product doesn't have a size */
                if (empty($explodedSku)) {
                    continue;
                }

                $baseSize = '';
                foreach ($explodedSku as $sizePart) {
                    $baseSize .= $sizePart . '-';
                }
                $skuCode = trim($baseSize, '-');
                $size = $product['size'];

                /** Check if skuCode is already added to size and ignore */
                $this->_sqlPMS->setTable('COMMON_ATTRIBUTES_VALUES');
                $select = $this->_sqlPMS->select()
                    ->where('commonAttributeValueSkuCode = "'.$skuCode.'"' );
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
                $count = $statement->execute()->count();
                if ($count > 0) {
                    continue;
                }

                /** Else insert the new size */
                if (preg_match("/^[0-9]+$/", $skuCode)) {
                    $commonAttributeId = 1;
                } elseif (preg_match("/^[a-z]+$/i", $skuCode)) {
                    $commonAttributeId = 2;
                } elseif (preg_match("/^.*\-.*+$/", $skuCode)) {
                    $commonAttributeId = 3;
                }

                if (!isset($commonAttributeId)) {
                    continue;
                }

                $commonAttributesValuesArray = array(
                    'commonAttributeId' => $commonAttributeId,
                    'enabled' => 1,
                    'commonAttributeValueSkuCode' => $skuCode,
                    'commonAttributeValueAbbrev' => '',
                    'commonAttributeValueBaseSize' => '',
                    'territoryId' => 1,
                );
                $this->_sqlPMS->setTable('COMMON_ATTRIBUTES_VALUES');
                $insert = $this->_sqlPMS->insert()
                    ->values($commonAttributesValuesArray);
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();
                $commonAttributeValueId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();


                $commoNAttributeDef = array(
                    'commonAttributeValueId' => $commonAttributeValueId,
                    'languageId' => 1,
                    'value' => $size,
                );
                $this->_sqlPMS->setTable('COMMON_ATTRIBUTES_VALUES_Definitions');
                $insert = $this->_sqlPMS->insert()
                    ->values($commoNAttributeDef);
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();

                $gui = array(
                    'commonAttributeValueId' => $commonAttributeValueId,
                    'commonAttributeValueOrder' => 1,
                    'pmsDisplay' => 1,
                    'shopDisplay' => 1,
                );
                $this->_sqlPMS->setTable('COMMON_ATTRIBUTES_VALUES_GUI_Representation');
                $insert = $this->_sqlPMS->insert()
                    ->values($gui);
                $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                $statement->execute();



            }
        }
    }

    /**
     *
     */
    public function productReport()
    {
        set_time_limit(-1);
        ini_set('memory_limit', '-1');
        $products = $this->_getAllProductsAndVariants();
        $array = array();
        foreach ($products as $result) {

            switch ($result['productIdFrom']) {
                case 'PRODUCTS_R_ATTRIBUTES':
                    // PMS DATA
                    $categoryPMS = $result['mainCat'];
                    $subcategoryPMS = $result['subCat'];
                    $rangePMS = $result['rangePMS'];
                    $supplierPMS = $result['supplierReference'];
                    $descriptionPMS = $result['shortProductName'];
                    $pricesPMS = $this->getPricesForProductOrVariantPMSForAllCurrencies($result);
                    $colourPMS = $this->getColourForVariantPMS($result);
                    $sizePMS = $this->getSizeForVariantPMS($result);
                    $styleType = $result['code'];

                    $supplierCode = $result['currencyCode'];
                    $supplierPrice = $result['supplierPrice'];

                    $countryOfOrigin = $result['countryOrigin'];
                    break;
                case 'PRODUCTS':
                    // PMS DATA
                    $categoryPMS = $result['mainCat'];
                    $subcategoryPMS = $result['subCat'];
                    $rangePMS = $result['rangePMS'];
                    $supplierPMS = $result['supplierReference'];
                    $descriptionPMS = $result['shortProductName'];
                    $pricesPMS = $this->getPricesForProductOrVariantPMSForAllCurrencies($result);
                    $colourPMS = '-';
                    $sizePMS = '-';
                    $styleType = $result['code'];
                    $countryOfOrigin = $result['countryOrigin'];
                    $supplierCode = $result['currencyCode'];
                    $supplierPrice = $result['supplierPrice'];

                    break;
            }
            $newArray = array(
                'categoryPMS' => $categoryPMS,
                'subcategoryPMS' => $subcategoryPMS,
                'rangePMS' => $rangePMS,
                'supplierPMS' => $supplierPMS,
                'descriptionPMS' => $descriptionPMS,
                'prices' => $pricesPMS,
                'colourPMS' => $colourPMS,
                'sizePMS' => $sizePMS,
                'styleType' => $styleType,
                'supplierPrice' => $supplierPrice,
                'supplierCode' => $supplierCode,
                'countryOfOrigin' => $countryOfOrigin
            );

            $array[] = array_merge($newArray, $result);

        }
        return $this->downloadExcelReportProData($array);
    }

    /**
     * @param $variantId
     * @return string
     */
    private function getStyleTypeForVariant($variantId)
    {
        $result = $this->getVariantIdFromProductId($variantId);
        return $this->getStyleTypeForProduct($result['productId']);
    }

    /**
     * @param $variantId
     */
    private function getVariantIdFromProductId($variantId)
    {
        $this->_sqlPMS->setTable('PRODUCTS_R_ATTRIBUTES');
        $select = $this->_sqlPMS->select()
            ->where('productAttributeId = ' . $variantId);
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();

        return $result;
    }

    /**
     * @param $productId
     * @return string
     */
    private function getStyleTypeForProduct($productId)
    {
        $this->_sqlPMS->setTable('PRODUCTS_Tabs');
        $select = $this->_sqlPMS->select()
            ->where('PRODUCTS_Tabs.productId = ' . $productId)
            ->join(
                array('td' => 'TABS_Definitions'),
                "PRODUCTS_Tabs.tabId = td.tabId",
                array('*')
            );
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();

        return !empty($result['code']) ? $result['code'] : '-';
    }

    /**
     * @param $result
     * @return array
     */
    private function getPricesForProductOrVariantPMSForAllCurrencies($result)
    {
        $currenyIDs = $this->_currencies;
        // Get maximum currency id value
        $maxCurrency =  max($currenyIDs);
        $this->_sqlPMS->setTable(array('pp' => 'PRODUCTS_Prices'));
        $select = $this->_sqlPMS->select();
        $select->where(array('productIdFrom' => $result['productIdFrom']));
        $select->where(array('productOrVariantId' => $result['productId']));
        $select->where(array('status' => "ACTIVE"));
        $select->where(array("currencyId <= $maxCurrency"));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $productPrices = $statement->execute();
        $pricesArray = array();

        // If currency values found, we add them to pricesArray
        if (count($productPrices) > 0) {
            foreach ($productPrices as $prices) {

                $pricesArray['scost' . $prices['currencyId']] = $prices['nowPrice'];
                $pricesArray['oldscost' . $prices['currencyId']] = $prices['wasPrice'];
                $pricesArray['ccost' . $prices['currencyId']] = $prices['costPrice'];
                $pricesArray['tccost' . $prices['currencyId']] = $prices['tradingCostPrice'];
                $pricesArray['freight' . $prices['currencyId']] = $prices['freightPrice'];
                $pricesArray['duty' . $prices['currencyId']] = $prices['dutyPrice'];
                $pricesArray['design' . $prices['currencyId']] = $prices['designPrice'];
                $pricesArray['packaging' . $prices['currencyId']] = $prices['packagingPrice'];

            }
            // We check if the array is fully filled, and if not we add default values
            foreach ($currenyIDs as $cId) {
                if (!array_key_exists("scost$cId", $pricesArray)) {
                    $pricesArray['scost' . $cId] = "0.00";
                    $pricesArray['oldscost' . $cId] = "0.00";
                    $pricesArray['ccost' . $cId] = "0.00";
                    $pricesArray['tccost' . $cId] = "0.00";
                    $pricesArray['freight' . $cId] = "0.00";
                    $pricesArray['duty' . $cId] = "0.00";
                    $pricesArray['design' . $cId] = "0.00";
                    $pricesArray['packaging' . $cId] = "0.00";
                }
            }
        } else {
            // Because in db Price values are not set to default,
            // Code breaks when value is null
            // This will fix the break
            foreach ($currenyIDs as $cId) {
                $pricesArray['scost' . $cId] = "0.00";
                $pricesArray['oldscost' . $cId] = "0.00";
                $pricesArray['ccost' . $cId] = "0.00";
                $pricesArray['tccost' . $cId] = "0.00";
                $pricesArray['freight' . $cId] = "0.00";
                $pricesArray['duty' . $cId] = "0.00";
                $pricesArray['design' . $cId] = "0.00";
                $pricesArray['packaging' . $cId] = "0.00";
            }
        }
        return $pricesArray;
    }

    /**
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    private function _getAllProductsAndVariants()
    {
        $sqlQuery = 'SELECT *
                        FROM
                         (
                        SELECT
                         T1.sku,
                         ps.spectrumId,
                         "PRODUCTS" AS productIdFrom,
                         T1.productId,
                         T1.status,
                         T1.style,
                         T1.ean13,
                         T1.productMerchantCategoryId,
                         T1.supplierReference,
                         T1.shortProductName,
                         T1.code,
                         T1.currencyCode,
                         T1.supplierPrice,
                         T1.countryOrigin,
                         T1.rangePMS,
                         T1.subCat,
                         T1.parentId,
                         T1.mainCat,
                         "-" as size
                        FROM
                         (
                         SELECT T0.*, IFNULL(pmcddd.categoryName, "-") as mainCat  FROM
                         (
                        SELECT
                         p.sku,p.productId,
                         p.`status`,
                         p.`style`,
                         p.ean13,
                         p.productMerchantCategoryId,
                         p.supplierReference,
                         pd.shortProductName,
                         IFNULL(td.code,"-") as code,
                         IFNULL(psp.currencyCode, "-") as currencyCode,
                         IFNULL(psp.price, "-") as supplierPrice,
                         IFNULL(ad.attributeValue, "-") as countryOrigin,
                         IFNULL(ad2.attributeValue, "-") as rangePMS,
                         IFNULL(pmcd.categoryName,"-") as subCat,
                         pmc.parentId
                        FROM PRODUCTS p
                        LEFT JOIN PRODUCTS_R_ATTRIBUTES pa ON p.productId = pa.productId
                        LEFT JOIN PRODUCTS_Definitions pd ON pd.productId = p.productId AND pd.productIdFrom = "PRODUCTS"
                        LEFT JOIN PRODUCTS_Tabs pt ON pt.productId = p.productId
                        LEFT JOIN TABS_Definitions td ON pt.tabId = td.tabId
                        LEFT JOIN PRODUCTS_Supplier_Price psp ON psp.productOrVariantId = p.productId AND psp.productIdFrom="PRODUCTS"
                        LEFT JOIN PRODUCTS_ATTRIBUTES_Combination pac ON pac.productOrVariantId = p.productId AND pac.isVariant = 0 AND pac.attributeGroupId = 150
                        LEFT JOIN ATTRIBUTES_Definitions ad ON ad.languageId = 1 AND pac.attributeValue = ad.attributeId
                        LEFT JOIN PRODUCTS_ATTRIBUTES_Combination pac2 ON pac2.productOrVariantId = p.productId AND pac2.isVariant = 0 AND pac2.attributeGroupId = 165
                        LEFT JOIN ATTRIBUTES_Definitions ad2 ON ad2.languageId = 1 AND pac2.attributeValue = ad2.attributeId
                        LEFT JOIN PRODUCTS_Merchant_Categories_Definitions pmcd ON pmcd.productMerchantCategoryId = p.productMerchantCategoryId AND pmcd.languageId = 1
                        LEFT JOIN PRODUCTS_Merchant_Categories pmc ON pmc.productMerchantCategoryId = p.productMerchantCategoryId
                        WHERE pa.productId IS NULL) T0
                        LEFT JOIN PRODUCTS_Merchant_Categories_Definitions pmcddd ON T0.parentId =  pmcddd.productMerchantCategoryId AND pmcddd.languageId = 1) T1
                        INNER JOIN
                         PRODUCTS_R_SPECTRUM ps ON T1.productId = ps.productId AND ps.productIdFrom = "PRODUCTS"

                          UNION
                          SELECT T9.* FROM (
                        SELECT T5.*, IFNULL(pmcddd.categoryName, "-") as mainCat, "-" as size FROM (
                        SELECT
                         pra.sku,
                         prs.spectrumId,
                         "PRODUCTS_R_ATTRIBUTES" AS productIdFrom,
                         pra.productAttributeId AS productId,
                         pra.status,
                         p.style,
                         pra.ean13,
                         p.productMerchantCategoryId,
                         p.supplierReference,
                         pd.shortProductName,
                         IFNULL(td.code,"-") as code,
                         IFNULL(psp.currencyCode, "-") as currencyCode,
                         IFNULL(psp.price, "-") as supplierPrice,
                         IFNULL(ad.attributeValue, "-") as countryOrigin,
                         IFNULL(ad2.attributeValue, "-") as rangePMS,
                         IFNULL(pmcd.categoryName,"-") as subCat,
                         pmc.parentId
                        FROM PRODUCTS_R_ATTRIBUTES pra
                        INNER JOIN PRODUCTS_R_SPECTRUM prs ON pra.productAttributeId = prs.productId AND prs.productIdFrom = "PRODUCTS_R_ATTRIBUTES"
                        LEFT JOIN PRODUCTS p ON pra.productId = p.productId
                        LEFT JOIN PRODUCTS_Definitions pd ON pd.productId = pra.productAttributeId AND pd.productIdFrom = "PRODUCTS_R_ATTRIBUTES"
                        LEFT JOIN PRODUCTS_Tabs pt ON pt.productId = p.productId
                        LEFT JOIN TABS_Definitions td ON pt.tabId = td.tabId
                        LEFT JOIN PRODUCTS_Supplier_Price psp ON psp.productOrVariantId = pra.productAttributeId AND psp.productIdFrom="PRODUCTS_R_ATTRIBUTES"
                        LEFT JOIN PRODUCTS_ATTRIBUTES_Combination pac ON pac.productOrVariantId = pra.productAttributeId AND pac.isVariant = 1 AND pac.attributeGroupId = 150
                        LEFT JOIN ATTRIBUTES_Definitions ad ON ad.languageId = 1 AND pac.attributeValue = ad.attributeId
                        LEFT JOIN PRODUCTS_ATTRIBUTES_Combination pac2 ON pac2.productOrVariantId = pra.productAttributeId AND pac2.isVariant = 1 AND pac2.attributeGroupId = 165
                        LEFT JOIN ATTRIBUTES_Definitions ad2 ON ad2.languageId = 1 AND pac2.attributeValue = ad2.attributeId
                        LEFT JOIN PRODUCTS_Merchant_Categories_Definitions pmcd ON pmcd.productMerchantCategoryId = p.productMerchantCategoryId AND pmcd.languageId = 1
                        LEFT JOIN PRODUCTS_Merchant_Categories pmc ON pmc.productMerchantCategoryId = p.productMerchantCategoryId) T5
                        LEFT JOIN PRODUCTS_Merchant_Categories_Definitions pmcddd ON T5.parentId =  pmcddd.productMerchantCategoryId AND pmcddd.languageId = 1) T9

                        ) AS main';
        if ($_SERVER['SERVER_NAME'] == 'sweeneypms.prism-test.co.uk') {
            $sqlQuery .= ' LIMIT 1000';
        }
        $sqlQuery .= ';';

        return $this->_dbAdapter->query($sqlQuery)->execute();
    }

    /**
     * @param $rows
     * @throws \Exception
     */
    public function downloadExcelReportProData($rows)
    {
        set_time_limit(-1);
        ini_set('memory_limit', '-1');
        $excelReader = new \PHPExcel();
        $excelReader->getProperties()->setCreator('Prism DM');
        $excelReader->getProperties()->setTitle('PMS Data <-> Spectrum Data');
        $excelReader->getProperties()->setSubject('PMS Data <-> Spectrum Data');
        $excelReader->getProperties()->setDescription('PMS Data <-> Spectrum Data');

        $i = 0;
        $excelReader->createSheet($i);
        $excelReader->getActiveSheetIndex($i);
        $excelReader->setActiveSheetIndex($i);

        $excelReader->getActiveSheet()->setCellValue('A1', 'FPRODNO');
        $excelReader->getActiveSheet()->setCellValue('B1', 'PRODNO');
        $excelReader->getActiveSheet()->setCellValue('C1', 'STATUS');
        $excelReader->getActiveSheet()->setCellValue('D1', 'SIZE');
        $excelReader->getActiveSheet()->setCellValue('E1', 'REPDESC');
        $excelReader->getActiveSheet()->setCellValue('F1', 'COLOUR');
        $excelReader->getActiveSheet()->setCellValue('G1', 'STYLETYPE');
        $excelReader->getActiveSheet()->setCellValue('H1', 'SCOST1');
        $excelReader->getActiveSheet()->setCellValue('I1', 'OLDSCOST1');
        $excelReader->getActiveSheet()->setCellValue('J1', 'SCOST2');
        $excelReader->getActiveSheet()->setCellValue('K1', 'OLDSCOST2');
        $excelReader->getActiveSheet()->setCellValue('L1', 'SCOST3');
        $excelReader->getActiveSheet()->setCellValue('M1', 'OLDSCOST3');
        $excelReader->getActiveSheet()->setCellValue('N1', 'CCOST');
        $excelReader->getActiveSheet()->setCellValue('O1', 'TCCOST');
        $excelReader->getActiveSheet()->setCellValue('P1', 'TRADECURR');
        $excelReader->getActiveSheet()->setCellValue('Q1', 'SUPACNO');
        $excelReader->getActiveSheet()->setCellValue('R1', 'FREIGHT');
        $excelReader->getActiveSheet()->setCellValue('S1', 'DUTY');
        $excelReader->getActiveSheet()->setCellValue('T1', 'DESIGN');
        $excelReader->getActiveSheet()->setCellValue('U1', 'PACKAGING');
        $excelReader->getActiveSheet()->setCellValue('V1', 'CNTRYORIG');
        $excelReader->getActiveSheet()->setCellValue('W1', 'BARCODE');


        $rowNumber = 2;
        foreach ($rows as $row) {
            $excelReader->getActiveSheet()->setCellValue('A'.$rowNumber, $row['sku']);
            $excelReader->getActiveSheet()->setCellValue('B'.$rowNumber, $row['style']);
            $excelReader->getActiveSheet()->setCellValue('C'.$rowNumber, $row['status']);
            $excelReader->getActiveSheet()->setCellValue('D'.$rowNumber, $row['sizePMS']);
            $excelReader->getActiveSheet()->setCellValue('E'.$rowNumber, $row['descriptionPMS']);
            $excelReader->getActiveSheet()->setCellValue('F'.$rowNumber, $row['colourPMS']);
            $excelReader->getActiveSheet()->setCellValue('G'.$rowNumber, $row['styleType']);
            $excelReader->getActiveSheet()->setCellValue('H'.$rowNumber, $row['prices']['scost1']);
            $excelReader->getActiveSheet()->setCellValue('I'.$rowNumber, $row['prices']['oldscost1']);
            $excelReader->getActiveSheet()->setCellValue('J'.$rowNumber, $row['prices']['scost2']);
            $excelReader->getActiveSheet()->setCellValue('K'.$rowNumber, $row['prices']['oldscost2']);
            $excelReader->getActiveSheet()->setCellValue('L'.$rowNumber, $row['prices']['scost3']);
            $excelReader->getActiveSheet()->setCellValue('M'.$rowNumber, $row['prices']['oldscost3']);
            $excelReader->getActiveSheet()->setCellValue('N'.$rowNumber, $row['prices']['ccost1']);
            $excelReader->getActiveSheet()->setCellValue('O'.$rowNumber, $row['supplierPrice']);
            $excelReader->getActiveSheet()->setCellValue('P'.$rowNumber, $row['supplierCode']);
            $excelReader->getActiveSheet()->setCellValue('Q'.$rowNumber, $row['supplierPMS']);
            $excelReader->getActiveSheet()->setCellValue('R'.$rowNumber, $row['prices']['freight1']);
            $excelReader->getActiveSheet()->setCellValue('S'.$rowNumber, $row['prices']['duty1']);
            $excelReader->getActiveSheet()->setCellValue('T'.$rowNumber, $row['prices']['design1']);
            $excelReader->getActiveSheet()->setCellValue('U'.$rowNumber, $row['prices']['packaging1']);
            $excelReader->getActiveSheet()->setCellValue('V'.$rowNumber, $row['countryOfOrigin']);
            $excelReader->getActiveSheet()->setCellValue('W'.$rowNumber, $row['ean13']);


            $rowNumber++;
        }

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="proddata-report.csv"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($excelReader, 'CSV');
        $objWriter->save('php://output');
        exit;
    }

    /**
     *
     */
    public function changeScreeningQuantity()
    {

        $productsAndVariants = $this->_getAllProductsAndVariants();
        foreach ($productsAndVariants as $productOrVariant) {
            // Skip empty entries
            if (empty($productOrVariant['productMerchantCategoryId'])) {
                continue;
            }
            // If the product is deleted skip the entry
            if ($productOrVariant['status'] == 'DELETED') {
                continue;
            }

            // Set up new screening quantity
            $specialCategory = array(3,11,4,15,18,16,5,9,6,10,17,7,8,12,13,14,19);

            if (in_array($productOrVariant['productMerchantCategoryId'], $specialCategory)) {
                $newScreening = 0;
            } else {
                $newScreening = 2;
            }

            $currentScreeningQuantity = $this->_getCurrentScreeningQuantity($productOrVariant);

            // If false we need to insert the attribute
            // If true we need to update the old attribute
            if (!$currentScreeningQuantity) {

                $this->_insertNewScreeningAttributeValue($newScreening, $currentScreeningQuantity, $productOrVariant);


            } else {
                // If the current screen quantity is the same with the new one skip the entry
                if ($currentScreeningQuantity['attributeValue'] == $newScreening) {
                    continue;
                }
                $this->_updateScreeningAttributeValue($newScreening, $currentScreeningQuantity);
            }

        }
    }

    /**
     * @param $newScreening
     * @param $currentScreeningQuantity
     * @param $productOrVariant
     */
    private function _insertNewScreeningAttributeValue($newScreening, $currentScreeningQuantity, $productOrVariant)
    {
        if (!empty($productOrVariant['productIdFrom']) && $productOrVariant['productIdFrom'] == 'PRODUCTS') {
            $isVariant = 0;
        } else {
            $isVariant = 1;
        }

        $this->_dbAdapter->getDriver()->getConnection()->beginTransaction();
        $this->_sqlPMS->setTable("PRODUCTS_ATTRIBUTES_Combination");
        $insertData = array(
            'attributeGroupId' => '11',
            'productOrVariantId' => $productOrVariant['productId'],
            'attributeValue' => '',
            'isVariant' => $isVariant,
            'groupType' => 'TEXT',
            'status' => 'ACTIVE'
        );

        $insert = $this->_sqlPMS->insert()
            ->values($insertData);
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
        $statement->execute();
        $productAttributeCombinationId = $this->_dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

        if (empty($productAttributeCombinationId)) {
            $this->_dbAdapter->getDriver()->getConnection()->rollback();
        }

        $this->_sqlPMS->setTable("PRODUCTS_ATTRIBUTES_Combination_Definitions");
        $insert = $this->_sqlPMS->insert()
            ->values(array(
                'productAttributeCombinationId' => $productAttributeCombinationId,
                'attributeValue' => $newScreening,
                'languageId' => '1',
                'status' => 'ACTIVE'
            ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
        $statement->execute();


        $this->_dbAdapter->getDriver()->getConnection()->commit();
    }

    /**
     * @param $newScreening
     * @param $currentScreeningQuantity
     */
    private function _updateScreeningAttributeValue($newScreening, $currentScreeningQuantity)
    {
        $updateData = array('attributeValue' => $newScreening);
        $update = $this->_sqlPMS->update();
        $update->table('PRODUCTS_ATTRIBUTES_Combination_Definitions');
        $update->set($updateData);
        $update->where(array(
            'productAttributesCombinationDefinitionId' => $currentScreeningQuantity['productAttributesCombinationDefinitionId'],
        ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     * @param $productOrVariant
     * @return bool|void
     */
    public function _getCurrentScreeningQuantity($productOrVariant)
    {
        if (!empty($productOrVariant['productIdFrom']) && $productOrVariant['productIdFrom'] == 'PRODUCTS') {
            $isVariant = 0;
        } else {
            $isVariant = 1;
        }
        $this->_sqlPMS->setTable('PRODUCTS_ATTRIBUTES_Combination');
        $select = $this->_sqlPMS->select()
            ->join(
                array('PACd' => 'PRODUCTS_ATTRIBUTES_Combination_Definitions'),
                "PACd.productAttributeCombinationId = PRODUCTS_ATTRIBUTES_Combination.productAttributeCombinationId",
                array('*'),
                Select::JOIN_LEFT
            )
            ->where('PRODUCTS_ATTRIBUTES_Combination.productOrVariantId = ' . $productOrVariant['productId'])
            ->where('PRODUCTS_ATTRIBUTES_Combination.isVariant = ' . $isVariant)
            ->where('PRODUCTS_ATTRIBUTES_Combination.attributeGroupId = 11');


        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute()->next();

        return !empty($results) ? $results : false;
    }

    /**
     *
     */
    public function copySupplierPriceFromProductToVariants()
    {
        $this->_sqlPMS->setTable('PRODUCTS_Supplier_Price');
        $select = $this->_sqlPMS->select()
            ->where('productIdFrom = "PRODUCTS"');
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $result) {

            $this->_sqlPMS->setTable($this->_productsVariantsRelationshipTable);
            $select = $this->_sqlPMS->select()
                ->columns(array('productAttributeId'))
                ->where('productId = ' . $result['productOrVariantId']);
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
            $variants = $statement->execute();

            foreach ($variants as $variant) {
                $this->_saveSupplierPriceForVariants(
                    array('productSupplierCurrency' => $result['currencyCode'], 'supplierPrice' => $result['price']),
                    array('variantId' => $variant['productAttributeId']),
                    $this->_sqlPMS
                );
            }

        }
    }
    /**
     *  THIS FUNCTION IS TO CALCULATE NEW ACCOUNT NR FOR TNP - SPECTRUM AND GATEWAY
     *  Make the new account
     */
    public function tnpAccount()
    {

        $this->_sqlPMS->setTable('TNP_TEST');
        $select = $this->_sqlPMS->select();
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $result) {

            $_fullCustomerNumber =  substr($result['OLDACCOUNT'], 1);
            $_fullCustomerNumber =  substr_replace($_fullCustomerNumber, "", -1);

            $_fullCustomerNumber = '7' . $_fullCustomerNumber;
            $_fullCustomerNumberLength = strlen($_fullCustomerNumber);
            $_tempTotalValue = 0;
            for ($_x=0;$_x<$_fullCustomerNumberLength;$_x++) {
                $_tempValue = ($_x % 2 == 0 ? (intval(substr($_fullCustomerNumber,$_x,1)) * 2) : intval(substr($_fullCustomerNumber,$_x,1)));
                $_tempTotalValue = $_tempTotalValue + ($_tempValue > 9 ? ($_tempValue % 10) + 1 : ($_tempValue % 10));
            }
            $_checkDigit = ((100 - $_tempTotalValue) % 10);
            $_accountNumber = $_fullCustomerNumber . strval($_checkDigit);
            $newIban = $this->getIBAN($_accountNumber);

            $update = $this->_sqlPMS->update();
            $update->table('TNP_TEST');
            $update->set(array('NEWACCOUNT' => $_accountNumber, 'NEWIBAN' => $newIban));
            $update->where(array(
                'ID' => $result['ID'],
            ));
            $statement = $this->_sqlPMS->prepareStatementForSqlObject($update);
            $statement->execute();
        }
    }

    /**
     * Convert Trust and Pay account ID to its IBAN equivalent
     *
     */
    function getIBAN($tpaccountid, $tpbank_code = '47840080') {
        return (($tpaccountid)?'DE' . str_pad((98 - bcmod((62 * (1 + bcmod($tpbank_code, 97)) + 27 * bcmod($tpaccountid, 97)), 97)), 2, '0', STR_PAD_LEFT) . $tpbank_code . $tpaccountid:'');
    }
    public function getAwardsMapping()
    {
        return array(
            'award_1' => 'LITTLE LONDON AWARDS BEST CHANGING BAG',
            'award_2' => 'Prima Baby Award (Gold) 2014',
            'award_3' => 'Mother & Baby 2016 Best Product for a new born -Bronze',
            'award_4' => 'Mother & Baby 2016',
            'award_5' => 'Best Baby and Toddler gear Mummi Awards 2015',
            'award_6' => '*Practical Parenting and Pregnancy - New Product to Market Gold 2010/2011',
            'award_7' => '*Practical Parenting and Pregnancy – Innovative Product under £10 Gold 2010/2011',
            'award_8' => '*Practical Parenting and Pregnancy – Toilet Training Silver 2010/201',
            'award_9' => '*Practical Pre School – Potty Training – Silver 2010',
            'award_10' => '*Junior Design Awards 2016 - Most Innovative Parenting Product',
            'award_11' => 'Prima Baby & Pregnant Award 2015 - Bronze',
            'award_12' => 'Mother & Baby Award 2015  - Silver',
            'award_13' => 'Junior Design Award 2015- Shortlisted',
            'award_14' => 'Junior Design Award 2015 - Winner',
            'award_15' => 'Banta Best Saftey Product 2016',
            'award_17' => 'WHICH Best Buy 2015',
            'award_18' => 'Loved by Parents 2015',
            'award_19' => 'Baby & Toddler Gear 2015',
            'award_20' => 'National Parenting Products, IRL 2015',
            'award_21' => 'The Independent "Indy Best" 2015',
            'award_22' => 'Loved By Mums - Best Baby & Toddler Gear 2015 - Silver',
            'award_23' => 'Prima Baby & Pregnancy Awards, Gold, "Baby Carrier" catergory, UK, 2016',
            'award_24' => 'Junior Design Awards, Winner, Best Infant Carrier, UK, 2015',
            'award_25' => 'Little London Awards, Winner, UK, 2015',
            'award_26' => 'Loved By Parents, Winner, Winner in the "Best Baby Carrier/Back Carrier"-category. UK, 2014',
            'award_27' => 'Mother & Baby Gold Best Stroller 2015',
            'award_28' => 'Loved by Parents 2015',
            'award_29' => 'Best Baby & Toddler 2015',
            'award_30' => 'Loved By Parents Gold 2015',
            'award_31' => 'Mother & Baby Best Travel System 2016',
            'award_32' => 'Loved by Parents Travel System 2015',
            'award_33' => '"Best travel cot 2015" by Junior magazine',
            'award_34' => 'Prima Baby & Pregnancy Awards, Silver "travel cot" category, UK, 2016',
            'award_35' => 'Gold Award - Prima Baby Cot Bed 2016',
            'award_36' => 'Winner of Mother & Baby Gold 2015',
            'award_37' => "BabyCentre Mums Picks 2016",
            'award_38' => 'Loved by Parents awards in "Best Nursery Bedding" winner',
            'award_39' => 'Winner of GOLD Mother & Baby Award 2016 for Best Sleep Product',
            'award_40' => 'Awarded 5 Stars as Best Value Cot Mattress by Mother & Baby Tried & Tested',
            'award_41' => 'Prima Baby & Pregnancy Awards, Bronze, "Bouncer" category, 2016',
            'award_42' => 'Loved by Parents: Winner in the "Best Bouncy Chair"-category, United Kingdom, 2014',
            'award_43' => 'Mother & Baby Bronze 2016',
            'award_44' => 'Loved by Parents Silver 2015',
            'award_45' => 'Prima Baby Bronze 2015',
            'award_46' => 'Awarded Mumsnet Best 2015',
            'award_47' => 'Awarded Mumsnet Best 2015',
            'award_48' => 'Best Baby and Toddler Gear Gold 2015',
            'award_49' => 'National Parenting Products, IRL 2015',
            'award_50' => '*Practical Pre-school Gold award 2014',
            'award_51' => '*Right Start Best Baby Essentials Highly Commended 2014',
            'award_52' => 'Right Start Best Baby Essentials Gold 2014',
            'award_53' => 'LBP Best Baby Bath Seat 2014 Gold',
            'award_54' => 'LBP Best Innovative Bath Product Gold',
            'award_55' => 'Junior Design Awards Innovative Parent Product 2014',
            'award_56' => 'Mummi Award 2014 Best Baby & Toddler Gear',
            'award_57' => '*Loved by Parents Best Pushchair Accessory award winner 2014',
            'award_58' => '*Mums Love Award winner 2015',
            'award_59' => '*Highly Commended Gift of the Year 2016',
            'award_60' => '*Best Baby & Toddler Gear 2015 in the Mumii Awards',
            'award_61' => 'Prima Baby - Sleepwear - Gold - 2016',
            'award_62' => 'Loved by Parents - Best Sleep Solution - Gold 2014',
            'award_63' => 'Mother & Baby - Best Sleeptime Product - Gold - 2014',
            'award_64' => "Prima Baby - Couldn't live without - Gold - Grobag",
            'award_65' => 'Prima Baby Best Nursery Accessory - SIlver - 2014',
            'award_66' => 'Best Baby and Toddler Gear GOLD 2013',
            'award_67' => 'Practical Parenting Awards SILVER',
            'award_68' => 'UKMUMS top choice',
            'award_69' => 'What to buy for your baby STAR BUY',
        );
    }
    public function lechameaub2cTmpTable()
    {
        $this->_sqlPMS->setTable('tmp_com');
        $select = $this->_sqlPMS->select();
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        /** Common attributes mapping */
        $mapping = array(
            'styleDesc' => 28,
            'comodity' => 26,
            'country' => 27,
            'weigth' => 10
        );
        foreach ($results as $result) {
            foreach ($mapping as $key => $commonAttributeId) {
                if (isset($result[$key])) {
                    switch ($key) {
                        case 'weigth':
                            $valueForDb = strtoupper($result[$key]);
                            if (strpos($valueForDb, 'KG') !== false) {
                                $valueForDb = str_replace('KG', '', $valueForDb);
                                /** Transform value to grams */
                                $valueForDb =  (float)$valueForDb * 1000;
                            }
                            break;
                        default:
                            $valueForDb = $result[$key];
                            break;
                    }
                    /** Get products from DB */
                    $variants = $this->getAllVariantsForStyle($result['sku']);
                    foreach ($variants as $variant) {
                        $this->deleteProductCommonAttribute($commonAttributeId, $variant['productAttributeId']);
                        $this->addCommonAttributeToProductOrVariant($variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES', $commonAttributeId, $valueForDb);
                        /** Add entry to QUEUE  */
                        $queueData = array(
                            'processed' => 0,
                            'processing' => 0,
                            'priority' => 1,
                            'queueEntryType' => 8,
                            'queueEntryId' => $variant['productAttributeId'],
                            'queueEntryBody' => json_encode(array()),
                            'queueReason' => 3
                        );

                        $this->_sqlPMS->setTable("QUEUE_Manager");
                        $insert = $this->_sqlPMS->insert()
                            ->values($queueData);
                        $statement = $this->_sqlPMS->prepareStatementForSqlObject($insert);
                        $statement->execute();
                    }
                }
            }
        }
        return true;
    }

    public function getAllVariantsForStyle($sku)
    {
        $this->_sqlPMS->setTable('PRODUCTS_R_ATTRIBUTES');
        $select = $this->_sqlPMS->select()
            ->where('sku LIKE "'.$sku.'%"');
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function deleteProductCommonAttribute($commonAttributeId, $productOrVariantId, $productIdFrom = 'PRODUCTS_R_ATTRIBUTES')
    {

        try {
            /**
             * Delete all attribute values for $commonAttributeId and $ValueId
             */
            $whereClause = array(
                'commonAttributeId' => $commonAttributeId,
                'productOrVariantId' => $productOrVariantId,
                'productIdFrom' => $productIdFrom
            );
            $this->_sqlPMS->setTable('PRODUCTS_Common_Attributes');
            $action = $this->_sqlPMS->delete()->where($whereClause);
            $this->_sqlPMS->prepareStatementForSqlObject($action)->execute();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function addCommonAttributeToProductOrVariant($productOrVariantId, $productIdFrom, $commonAttributeId, $commonAttributeValue, $checkSelect = true)
    {
        /**
         * We need to insert new entry for the common attributes
         */
        $this->_sqlPMS->setTable('PRODUCTS_Common_Attributes');
        $action = $this->_sqlPMS->insert();
        $action->values(array(
            'productOrVariantId' => $productOrVariantId,
            'productIdFrom' => $productIdFrom,
            'commonAttributeId' => $commonAttributeId,
            'commonAttributeValue' => $commonAttributeValue
        ));
        $statement = $this->_sqlPMS->prepareStatementForSqlObject($action);
        $statement->execute();

    }
}
