<?php

namespace PrismProductsManager\Model;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
/**
 * Class ProductsDefinitionsEntity
 * @package PrismProductsManager\Model
 */
class ProductsDefinitionsEntity
{

    /**
     * @var
     */
    protected $_productDefinitionsId;

    /**
     * @var
     */
    protected $_productId;

    /**
     * @var
     */
    protected $_languageId;

    /**
     * @var int
     */
    protected $_versionId = 1;

    /**
     * @var string
     */
    protected $_versionDescription = '';

    /**
     * @var
     */
    protected $_shortProductName;

    /**
     * @var
     */
    protected $_productName;

    /**
     * @var string
     */
    protected $_urlName = '';

    /**
     * @var string
     */
    protected $_metaTitle = '';

    /**
     * @var string
     */
    protected $_metaDescription = '';

    /**
     * @var string
     */
    protected $_metaKeyword = '';

    /**
     * @var string
     */
    protected $_shortDescription = '';

    /**
     * @var string
     */
    protected $_longDescription = '';

    /**
     * @var string
     */
    protected $_misSpells = '';

    /**
     * @var string
     */
    protected $_tags = '';

    /**
     * @var string
     */
    protected $_status;

    /**
     * @var
     */
    protected $_makeLive;

    /**
     * @var bool|string
     */
    protected $_created;

    /**
     * @var bool|string
     */
    protected $_modified;

    /**
     *
     */
    public function __construct()
    {
        $this->_created = date("Y-m-d H:i:s");
        $this->_modified = date("Y-m-d H:i:s");
        $this->_status = 'INACTIVE';
    }

    /**
     * @return string
     */
    public function getMisSpells()
    {
        return $this->_misSpells;
    }

    /**
     * @param string $misSpells
     */
    public function setMisSpells($misSpells)
    {
        $this->_misSpells = $misSpells;
    }

    /**
     * @return string
     */
    public function getTags()
    {
        return $this->_tags;
    }

    /**
     * @param string $tags
     */
    public function setTags($tags)
    {
        $this->_tags = $tags;
    }

    /**
     * @return mixed
     */
    public function getShortProductName()
    {
        return $this->_shortProductName;
    }

    /**
     * @param $_shortProductName
     */
    public function setShortProductName($_shortProductName)
    {
        $this->_shortProductName = $_shortProductName;
    }

    /**
     * @return mixed
     */
    public function getProductDefinitionsId()
    {
        return $this->_productDefinitionsId;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->_productId;
    }

    /**
     * @return mixed
     */
    public function getLanguageId()
    {
        return $this->_languageId;
    }

    /**
     * @return int
     */
    public function getVersionId()
    {
        return $this->_versionId;
    }

    /**
     * @return string
     */
    public function getVersionDescription()
    {
        return $this->_versionDescription;
    }

    /**
     * @return mixed
     */
    public function getProductName()
    {
        return $this->_productName;
    }

    /**
     * @return string
     */
    public function getUrlName()
    {
        return $this->_urlName;
    }

    /**
     * @return string
     */
    public function getMetaTitle()
    {
        return $this->_metaTitle;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->_metaDescription;
    }

    /**
     * @return string
     */
    public function getMetaKeyword()
    {
        return $this->_metaKeyword;
    }

    /**
     * @return string
     */
    public function getShortDescription()
    {
        return $this->_shortDescription;
    }

    /**
     * @return string
     */
    public function getLongDescription()
    {
        return $this->_longDescription;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->_status;
    }

    /**
     * @return mixed
     */
    public function getMakeLive()
    {
        return $this->_makeLive;
    }

    /**
     * @return bool|string
     */
    public function getCreated()
    {
        return $this->_created;
    }

    /**
     * @return bool|string
     */
    public function getModified()
    {
        return $this->_modified;
    }

    /**
     * @param $_productDefinitionsId
     */
    public function setProductDefinitionsId($_productDefinitionsId)
    {
        $this->_productDefinitionsId = $_productDefinitionsId;
    }

    /**
     * @param $_productId
     */
    public function setProductId($_productId)
    {
        $this->_productId = $_productId;
    }

    /**
     * @param $_languageId
     */
    public function setLanguageId($_languageId)
    {
        $this->_languageId = $_languageId;
    }

    /**
     * @param $_versionId
     */
    public function setVersionId($_versionId)
    {
        $this->_versionId = $_versionId;
    }

    /**
     * @param $_versionDescription
     */
    public function setVersionDescription($_versionDescription)
    {
        $this->_versionDescription = $_versionDescription;
    }

    /**
     * @param $_productName
     */
    public function setProductName($_productName)
    {
        $this->_productName = $_productName;
    }

    /**
     * @param $_urlName
     */
    public function setUrlName($_urlName)
    {
        $this->_urlName = $_urlName;
    }

    /**
     * @param $_metaTitle
     */
    public function setMetaTitle($_metaTitle)
    {
        $this->_metaTitle = $_metaTitle;
    }

    /**
     * @param $_metaDescription
     */
    public function setMetaDescription($_metaDescription)
    {
        $this->_metaDescription = $_metaDescription;
    }

    /**
     * @param $_metaKeyword
     */
    public function setMetaKeyword($_metaKeyword)
    {
        $this->_metaKeyword = $_metaKeyword;
    }

    /**
     * @param $_shortDescription
     */
    public function setShortDescription($_shortDescription)
    {
        $this->_shortDescription = $_shortDescription;
    }

    /**
     * @param $_longDescription
     */
    public function setLongDescription($_longDescription)
    {
        $this->_longDescription = $_longDescription;
    }

    /**
     * @param $_status
     */
    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    /**
     * @param $_makeLive
     */
    public function setMakeLive($_makeLive)
    {
        $this->_makeLive = $_makeLive;
    }

    /**
     * @param $_created
     */
    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    /**
     * @param $_modified
     */
    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }


    
}
