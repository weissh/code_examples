<?php
namespace PrismProductsManager\Model;


class ColourDefinitionsEntity
{

    protected $_colourDefinitionId;

    protected $_colourId;

    protected $_languageId;

    protected $_colourName;

    protected $_colourDescription;

    public function __construct()
    {

    }

    /**
     * @param mixed $colourDefinitionId
     */
    public function setColourDefinitionId($colourDefinitionId)
    {
        $this->_colourDefinitionId = $colourDefinitionId;
    }

    /**
     * @return mixed
     */
    public function getColourDefinitionId()
    {
        return $this->_colourDefinitionId;
    }

    /**
     * @param mixed $colourDescription
     */
    public function setColourDescription($colourDescription)
    {
        $this->_colourDescription = $colourDescription;
    }

    /**
     * @return mixed
     */
    public function getColourDescription()
    {
        return $this->_colourDescription;
    }

    /**
     * @param mixed $colourId
     */
    public function setColourId($colourId)
    {
        $this->_colourId = $colourId;
    }

    /**
     * @return mixed
     */
    public function getColourId()
    {
        return $this->_colourId;
    }

    /**
     * @param mixed $colourName
     */
    public function setColourName($colourName)
    {
        $this->_colourName = $colourName;
    }

    /**
     * @return mixed
     */
    public function getColourName()
    {
        return $this->_colourName;
    }

    /**
     * @param mixed $languageId
     */
    public function setLanguageId($languageId)
    {
        $this->_languageId = $languageId;
    }

    /**
     * @return mixed
     */
    public function getLanguageId()
    {
        return $this->_languageId;
    }

}
