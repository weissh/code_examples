<?php

namespace PrismProductsManager\Model;

use PrismMediaManager\Model\ProductFolderMapper;
use PrismSpectrumApiClient\Service\Customer;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Expression;
use Zend\Math\Rand;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\View\Model\JsonModel;

/**
 * Class NauticaliaProductsMigrationMapper
 * @package PrismProductsManager\Model
 */
class NauticaliaProductsMigrationMapper extends ProductsMapper
{
    /**
     * @var Adapter
     */
    protected $dbAdapter;
    /**
     * @var Adapter
     */
    protected $oldPMSAdapter;
    /**
     * @var Adapter
     */
    protected $adminAdapter;
    /**
     * @var Sql
     */
    protected $sqlPMS;
    /**
     * @var Sql
     */
    protected $sqlOldPMS;
    /**
     * @var Sql
     */
    protected $sqlAdmin;
    /**
     * @var ProductFolderMapper
     */
    protected $productFolderMapper;

    /**
     * @var
     */
    protected $languages;

    /**
     * @var string
     */
    protected $productVariantsTable = "PRODUCTS_R_ATTRIBUTES";
    /**
     * @var string
     */
    protected $productTable = "PRODUCTS";
    /**
     * @var string
     */
    protected $productsRAttributesTable = "PRODUCTS_ATTRIBUTES_Combination";
    /**
     * @var string
     */
    protected $productsRAttributesTableDefinitions = "PRODUCTS_ATTRIBUTES_Combination_Definitions";

    /**
     * @var string COMMON ATTRIBUTES TABLES
     */
    protected $commonAttributesTable = 'COMMON_ATTRIBUTES';
    /**
     * @var string
     */
    protected $commonAttributesDefinitionsTable = 'COMMON_ATTRIBUTES_Definitions';
    /**
     * @var string
     */
    protected $commonAttributesGUIRepresentationTable = 'COMMON_ATTRIBUTES_GUI_Representation';
    /**
     * @var string
     */
    protected $commonAttributesValuesTable = 'COMMON_ATTRIBUTES_VALUES';
    /**
     * @var string
     */
    protected $commonAttributesValuesDefinitionsTable = 'COMMON_ATTRIBUTES_VALUES_Definitions';
    /**
     * @var string
     */
    protected $commonAttributesValuesEquivalenceTable = 'COMMON_ATTRIBUTES_VALUES_Equivalence';
    /**
     * @var string
     */
    protected $commonAttributesValuesGUIRepresentationTable = 'COMMON_ATTRIBUTES_VALUES_GUI_Representation';
    /**
     * @var string
     */
    protected $commonAttributesViewTypeTable = 'COMMON_ATTRIBUTES_View_Type';
    /********************************************* *****************************************************/

    protected $productsTableOldPMS = "products";

    /**
     * @var string
     */
    protected $_attributesValuesTable = 'ATTRIBUTES';
    /**
     * @var string
     */
    protected $_attributesValuesDefinitionsTable = 'ATTRIBUTES_Definitions';
    /**
     * @var string
     */
    protected $_attributesExcelCore = "ATTRIBUTES_Excel_Core_IMPORT";
    /**
     * @var string
     */
    protected $_attributesExcelMarketing = "ATTRIBUTES_Excel_Marketing_IMPORT";

    /**
     * @var array
     */
    protected $marketingAttributes = array();
    /**
     * @var array
     */
    protected $coreAttributes = array();

    /**
     * @var Customer
     */
    protected $_customerService;
    /**
     * @param Adapter $config
     * @param Adapter $dbAdapter
     * @param Adapter $oldPMSAdapter
     * @param Adapter $adminAdapter
     * @param ProductFolderMapper $productFolderMapper
     */
    public function __construct(
        $config,
        Adapter $dbAdapter,
        Adapter $oldPMSAdapter,
        Adapter $adminAdapter,
        Adapter $spectrumAdapter,
        ProductFolderMapper $productFolderMapper,
        Customer $customerService)
    {
        $this->languages = $config['siteConfig']['languages']['site-languages'];
        $this->websites = $config['siteConfig']['websites']['websites'];
        $this->currencies = $config['siteConfig']['currencies']['site-currencies'];
        $this->dbAdapter = $dbAdapter;
        $this->adminAdapter = $adminAdapter;
        $this->oldPMSAdapter = $oldPMSAdapter;
        $this->spectrumAdapter = $spectrumAdapter;
        $this->sqlPMS = new Sql($dbAdapter);
        $this->sqlOldPMS = new Sql($oldPMSAdapter);
        $this->sqlAdmin = new Sql($adminAdapter);
        $this->sqlSpectrum = new Sql($spectrumAdapter);
        $this->productFolderMapper = $productFolderMapper;
        $this->customerService = $customerService;
        $this->sqlOldPMS->setTable($this->productsTableOldPMS);

    }
    public function migrateRecipients()
    {
        $this->populateSkuFromOldSystem();
        $this->sqlPMS->setTable('ATTRIBUTES_Recipients');
        $select = $this->sqlPMS->select();
        $statement = $this->sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $result) {
            $variant = $this->retrieveVariantBasedOnSku($result['oldSku']);
            if (!empty($variant)) {
                $this->sqlPMS->setTable('PRODUCTS_ATTRIBUTES_Combination');
                $action = $this->sqlPMS->insert();

                if ($result['recipient'] == 'KIDS') {
                    $attributeValue = '589';
                } elseif ($result['recipient'] == 'MEN') {
                    $attributeValue = '587';
                } else {
                    $attributeValue = '588';
                }
                $insertData = array(
                    'attributeGroupId' => '185',
                    'productOrVariantId' => $variant['productAttributeId'],
                    'attributeValue' => $attributeValue,
                    'groupType' => 'SELECT',
                    'isVariant' => '1',
                    'status' => 'ACTIVE',
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                );
                $action->values($insertData);
                $statement = $this->sqlPMS->prepareStatementForSqlObject($action);
                $statement->execute();
            }
        }
        die('done');
    }
    public function populateSkuFromOldSystem()
    {
        $this->sqlPMS->setTable('ATTRIBUTES_Recipients');
        $select = $this->sqlPMS->select();
        $statement = $this->sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $result) {

            $this->sqlOldPMS->setTable('products');
            $select = $this->sqlOldPMS->select()
                ->where('id = ' . $result['oldProductId']);
            $statement = $this->sqlOldPMS->prepareStatementForSqlObject($select);
            $product = $statement->execute()->next();

            $update = $this->sqlPMS->update();
            $update->table('ATTRIBUTES_Recipients');
            $update->set(array('oldSku' => $product['fprodno']));
            $update->where('id = ' . $result['id']);
            $statement = $this->sqlPMS->prepareStatementForSqlObject($update);
            $statement->execute();
        }
    }
    /**
     *
     */
    public function prepareForAttributesMigration()
    {
        try {
            $this->dbAdapter->getDriver()->getConnection()->beginTransaction();
            /** Map the attributes into class variable */
            $this->mapMarketingAttributes();
            $this->mapCoreAttributes();

            /** Clear Data from database */
            $this->emptyAttributesFromProducts();
            $this->emptyAttributesValues();

            /** Insert New Attributes Values and update the class variable */
            $this->insertNewAttributesValues();

            /** Link the Core attributes to products and marketing categories */
            $this->addCoreAttributesToProducts();

            /** Link the Marketing attributes to products and marketing categories */
            $this->addMarketingAttributesToProducts();

            $this->dbAdapter->getDriver()->getConnection()->commit();
            die('here');
        } catch (\Exception $ex) {
            $this->dbAdapter->getDriver()->getConnection()->rollback();
            //var_dump($ex->getMessage());die;
        }
    }
    public function migrateCollectionVariantNames()
    {
        $allVariants = $this->getAllCollectionVariants();
        foreach ($allVariants as $variant) {
            $this->sqlOldPMS->setTable('products');
            $select = $this->sqlOldPMS->select()
                ->where('fprodno = "' . $variant['sku'] . '"');
            $statement = $this->sqlOldPMS->prepareStatementForSqlObject($select);
            $oldProduct = $statement->execute()->next();

            if (!empty($oldProduct['variant'])) {
                $update = $this->sqlPMS->update();
                $update->table('PRODUCTS_Definitions');
                $update->set(array('productName' => $oldProduct['variant']));
                $update->where('productId = ' . $variant['productAttributeId']);
                $update->where('productIdFrom = "PRODUCTS_R_ATTRIBUTES"');

                $statement = $this->sqlPMS->prepareStatementForSqlObject($update);
                $statement->execute();
            }
        }
    }
    private function getAllCollectionVariants()
    {
        $this->sqlPMS->setTable(array('pra' => 'PRODUCTS_R_ATTRIBUTES'));
        $select = $this->sqlPMS->select()
            ->join(
                array('p' => 'PRODUCTS'),
                new Expression(
                    'p.productId = pra.productId'
                ),
                array(
                    'productTypeId'
                ),
                Select::JOIN_LEFT
            )
            ->where('p.productTypeId = 4');
        $statement = $this->sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    /**
     *
     */
    private function addCoreAttributesToProducts()
    {
        foreach ($this->coreAttributes as $coreAttribute) {
            if ($coreAttribute['attributeGroupType'] == 'CHECKBOX') {
                $this->addCheckboxAttributeToProducts($coreAttribute, 'core');
            }
        }
    }
    /**
     *
     */
    private function addMarketingAttributesToProducts()
    {
        foreach ($this->marketingAttributes as $marketingAttribute) {
            switch ($marketingAttribute['attributeGroupType']) {
                case 'CHECKBOX':
                    $this->addCheckboxAttributeToProducts($marketingAttribute, 'marketing');
                    break;
                case 'SELECT':
                    $this->addSelectAttributeToProducts($marketingAttribute, 'marketing');
                    break;
            }

        }
    }

    /**
     * @param $attribute
     * @param $type
     */
    private function addSelectAttributeToProducts($attribute, $type)
    {
        /** Retrieve all products that have this attribute */
        $products = $this->getAllProductsAvailableForAttribute($attribute, $type);
        if (!empty($products)) {
            foreach ($products as $product) {

                $variant = $this->retrieveVariantBasedOnSku($product['oldSku']);

                if (!empty($variant)) {
                    /**  Check if the product has this attribute group assigned to it */
                    if (!$this->checkIfProductOrVariantHasAttributeAssigned($variant['productId'], 0, $attribute)) {
                        /**  If the main product for this variant doesn't have the attribute then */
                        /**  Retrieve all marketing categories for the main product */
                        $marketingCategories = $this->getAllMarketingCategoriesForProductOrVariant($variant['productId'], 0);
                        if (!empty($marketingCategories)) {
                            foreach ($marketingCategories as $marketingCategory) {
                                if (!$this->checkIfAttributeIsAddedToMarketingCategory($attribute, $marketingCategory)) {
                                    /**  and if it is not, add the attribute to them */
                                    $this->addAttributeToMarketingCategory($attribute, $marketingCategory);
                                }
                            }
                        }
                        /** Add attribute to the main product level */
                        $this->addSelectAttributeToProductOrVariant($attribute, $variant['productId'], 0, $product);
                    }

                    /**  Check if the variant has this attribute group assigned to it */
                    if (!$this->checkIfProductOrVariantHasAttributeAssigned($variant['productAttributeId'], 1, $attribute)) {
                        /**  If the variant doesn't have the attribute then */
                        /**  Retrieve all marketing categories for the variant */
                        $marketingCategories = $this->getAllMarketingCategoriesForProductOrVariant($variant['productAttributeId'], 1);
                        if (!empty($marketingCategories)) {
                            foreach ($marketingCategories as $marketingCategory) {
                                if (!$this->checkIfAttributeIsAddedToMarketingCategory($attribute, $marketingCategory)) {
                                    /**  and if it is not, add the attribute to them */
                                    $this->addAttributeToMarketingCategory($attribute, $marketingCategory);
                                }
                            }
                        }
                        /** Add attribute to the main product level */
                        $this->addSelectAttributeToProductOrVariant($attribute, $variant['productAttributeId'], 1, $product);
                    }
                }
            }
        }
    }

    /**
     * @param $attribute
     * @param $variantOrProductId
     * @param $isVariant
     * @param $product
     */
    private function addSelectAttributeToProductOrVariant($attribute, $variantOrProductId, $isVariant, $product)
    {
        $attributeSelectId = null;
        foreach ($attribute['options'] as $option) {
            if ($option['value'] == $product[$attribute['columnExport']]) {
                $attributeSelectId = $option['attributeId'];
                break;
            }
        }
        if (!empty($attributeSelectId)) {
            $this->sqlPMS->setTable('PRODUCTS_ATTRIBUTES_Combination');
            $action = $this->sqlPMS->insert();
            $insertData = array(
                'attributeGroupId' => $attribute['attributeGroupId'],
                'productOrVariantId' => $variantOrProductId,
                'attributeValue' => $attributeSelectId,
                'groupType' => $attribute['attributeGroupType'],
                'isVariant' => $isVariant,
                'status' => 'ACTIVE',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            );
            $action->values($insertData);
            $statement = $this->sqlPMS->prepareStatementForSqlObject($action);
            $statement->execute();
        }

    }

    /**
     * @param $attribute
     * @param $type
     */
    private function addCheckboxAttributeToProducts($attribute, $type)
    {
        /** Retrieve all products that have this attribute */
        $products = $this->getAllProductsAvailableForAttribute($attribute, $type);
        if (!empty($products)) {
            foreach ($products as $product) {
                /** Retrieve the variant entry based on the sku */
                $variant = $this->retrieveVariantBasedOnSku($product['oldSku']);

                if (!empty($variant)) {
                    /**  Check if the product has this attribute group assigned to it */
                    if (!$this->checkIfProductOrVariantHasAttributeAssigned($variant['productId'], 0, $attribute)) {
                        /**  If the main product for this variant doesn't have the attribute then */
                        /**  Retrieve all marketing categories for the main product */
                        $marketingCategories = $this->getAllMarketingCategoriesForProductOrVariant($variant['productId'], 0);
                        if (!empty($marketingCategories)) {
                            foreach ($marketingCategories as $marketingCategory) {
                                /** Check if the attribute is added to the category */
                                if (!$this->checkIfAttributeIsAddedToMarketingCategory($attribute, $marketingCategory)) {
                                    /**  and if it is not, add the attribute to them */
                                    $this->addAttributeToMarketingCategory($attribute, $marketingCategory);
                                }
                            }
                        }
                        /** Add attribute to the main product level */
                        $this->addAttributeToProductOrVariant($attribute, $variant['productId'], 0);
                    }
                    /**  Check if the variant has this attribute group assigned to it */
                    if (!$this->checkIfProductOrVariantHasAttributeAssigned($variant['productAttributeId'], 1, $attribute)) {
                        /**  If the variant doesn't have the attribute then */
                        /**  Retrieve all marketing categories for the variant */
                        $marketingCategories = $this->getAllMarketingCategoriesForProductOrVariant($variant['productAttributeId'], 1);
                        if (!empty($marketingCategories)) {
                            foreach ($marketingCategories as $marketingCategory) {
                                /** Check if the attribute is added to the category */
                                if (!$this->checkIfAttributeIsAddedToMarketingCategory($attribute, $marketingCategory)) {
                                    /**  and if it is not, add the attribute to them */
                                    $this->addAttributeToMarketingCategory($attribute, $marketingCategory);
                                }
                            }
                        }
                        /** Add attribute to the variant level */
                        $this->addAttributeToProductOrVariant($attribute, $variant['productAttributeId'], 1);
                    }
                }
            }
        }
    }
    /**
     * @param $attribute
     * @param $productOrVariantId
     * @param $isVariant
     */
    private function addAttributeToProductOrVariant($attribute, $productOrVariantId, $isVariant)
    {
        $this->sqlPMS->setTable('PRODUCTS_ATTRIBUTES_Combination');
        $action = $this->sqlPMS->insert();
        $insertData = array(
            'attributeGroupId' => $attribute['attributeGroupId'],
            'productOrVariantId' => $productOrVariantId,
            'attributeValue' => '1',
            'groupType' => $attribute['attributeGroupType'],
            'isVariant' => $isVariant,
            'status' => 'ACTIVE',
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        );
        $action->values($insertData);
        $statement = $this->sqlPMS->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     * @param $coreAttribute
     * @param $marketingCategory
     */
    private function addAttributeToMarketingCategory($coreAttribute, $marketingCategory)
    {
        $this->sqlPMS->setTable('CATEGORIES_R_ATTRIBUTES');
        $action = $this->sqlPMS->insert();
        $action->values(array(
            'categoryId' => $marketingCategory['categoryId'],
            'attributeGroupId' => $coreAttribute['attributeGroupId'],
            'status' => 'ACTIVE',
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        ));
        $statement = $this->sqlPMS->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     * @param $attribute
     * @param $marketingCategory
     * @return bool
     */
    private function checkIfAttributeIsAddedToMarketingCategory($attribute, $marketingCategory)
    {
        $alreadyAdded = false;
        $this->sqlPMS->setTable('CATEGORIES_R_ATTRIBUTES');
        $select = $this->sqlPMS->select()
            ->where('categoryId = ' . $marketingCategory['categoryId'])
            ->where('attributeGroupId = ' . $attribute['attributeGroupId'])
            ->where('status = "ACTIVE"');
        $statement = $this->sqlPMS->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() > 0) {
            $alreadyAdded = true;
        }
        return $alreadyAdded;
    }

    /**
     * @param $productOrVariantId
     * @param $isVariant
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    private function getAllMarketingCategoriesForProductOrVariant($productOrVariantId, $isVariant)
    {
        if ($isVariant == 1) {
            $table = 'PRODUCTS_R_ATTRIBUTES';
        } else {
            $table = 'PRODUCTS';
        }
        $this->sqlPMS->setTable('PRODUCTS_R_CATEGORIES');
        $select = $this->sqlPMS->select()
            ->where('productIdFrom = "' . $table . '"')
            ->where('status = "ACTIVE"')
            ->where('productId = ' . $productOrVariantId);
        $statement = $this->sqlPMS->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     * @param $productOrVariantId
     * @param $isVariant
     * @param $attribute
     * @return bool
     */
    private function checkIfProductOrVariantHasAttributeAssigned($productOrVariantId, $isVariant, $attribute)
    {

        $hasAttribute = false;
        $this->sqlPMS->setTable($this->_productsAttributesCombinationTable);
        $select = $this->sqlPMS->select()
            ->where('status = "ACTIVE"')
            ->where('attributeGroupId = ' . $attribute['attributeGroupId'])
            ->where('isVariant = ' . $isVariant)
            ->where('productOrVariantId = ' . $productOrVariantId);

        $statement = $this->sqlPMS->prepareStatementForSqlObject($select);
        $count = $statement->execute()->count();
        if ($count > 0) {
            $hasAttribute = true;
        }
        return $hasAttribute;
    }

    /**
     * @param $sku
     */
    private function retrieveVariantBasedOnSku($sku)
    {
        $this->sqlPMS->setTable($this->productVariantsTable);
        $select = $this->sqlPMS->select()->where('sku = "' . $sku . '"');
        $statement = $this->sqlPMS->prepareStatementForSqlObject($select);
        return $statement->execute()->next();
    }




    /**
     * @param $attribute
     * @param $attributeType
     * @return array
     */
    private function getAllProductsAvailableForAttribute($attribute, $attributeType)
    {
        switch ($attributeType) {
            case 'core':
                $this->sqlPMS->setTable($this->_attributesExcelCore);
                break;
            case 'marketing':
                $this->sqlPMS->setTable($this->_attributesExcelMarketing);
                break;
        }
        $select = $this->sqlPMS->select();
        switch ($attribute['attributeGroupType']) {
            case 'CHECKBOX':
                $select->where($attribute['columnExport'] . ' = 1');
                break;
            case 'SELECT':
                $select->where($attribute['columnExport'] . ' != ""');
                break;
        }
        $statement = $this->sqlPMS->prepareStatementForSqlObject($select);

        $results = $statement->execute();

        $productsArray = array();
        foreach ($results as $product) {
            $productsArray[] = $product;
        }
       return $productsArray;
    }
    /**
     *
     */
    private function insertNewAttributesValues()
    {
        /** Insert New Marketing Attributes Values */
        foreach ($this->marketingAttributes as $key => &$marketingAttribute) {
            $this->insertNewValuesForAttributeGroup($marketingAttribute, $key, 'marketingAttributes');
        }
        /** Insert New Core Attributes Values */
        foreach ($this->coreAttributes as $key => $coreAttribute) {
            $this->insertNewValuesForAttributeGroup($coreAttribute, $key, 'coreAttributes');
        }
    }

    /**
     * @param $attributeGroup
     * @param $key
     * @param $typeOfAttribute
     */
    private function InsertNewValuesForAttributeGroup(&$attributeGroup, $key, $typeOfAttribute)
    {
        switch ($attributeGroup['attributeGroupType']) {
            case 'CHECKBOX':
                $this->insertNewValuesForCheckboxAttributeGroup($attributeGroup, $typeOfAttribute);
                break;
            case 'SELECT':
                $this->insertNewValuesForSelectAttributeGroup($attributeGroup, $typeOfAttribute);
                break;
        }
    }

    /**
     * @param $attributeGroup
     * @param $typeOfAttribute
     */
    private function insertNewValuesForCheckboxAttributeGroup($attributeGroup, $typeOfAttribute)
    {
        /** Insert data into Attributes Table */
        $this->sqlPMS->setTable($this->_attributesValuesTable);
        $action = $this->sqlPMS->insert();
        $action->values(array(
            'attributeGroupId' => $attributeGroup['attributeGroupId'],
            'status' => 'ACTIVE',
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        ));
        $statement = $this->sqlPMS->prepareStatementForSqlObject($action);
        $statement->execute();

        $attributeValueId = $this->dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
        /** Insert Data in Attributes Definitions */
        $this->sqlPMS->setTable($this->_attributesValuesDefinitionsTable);
        $action = $this->sqlPMS->insert();
        $action->values(array(
            'languageId' => 1, // No need for other languages
            'attributeId' => $attributeValueId,
            'attributeValue' => str_replace('_',' ', $attributeGroup['columnExport']),
            'status' => "ACTIVE",
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        ));
        $statement = $this->sqlPMS->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     * Select the values for the select types attributes from the data import tables
     * Insert the new values for each attribute group in the database
     * and modify the class variables $typeOfAttribute to hold the information in the options key
     * @param $attributeGroup
     * @param $typeOfAttribute
     */
    private function insertNewValuesForSelectAttributeGroup(&$attributeGroup, $typeOfAttribute)
    {
        $possibleAttributesValues = $this->getAllPossibleAttributesValues($attributeGroup, $typeOfAttribute);

        foreach ($possibleAttributesValues as $possibleAttributeValue) {
            /** Insert Data into ATTRIBUTES Table */
            $this->sqlPMS->setTable($this->_attributesValuesTable);
            $attributesValuesData = array(
                'attributeGroupId' => $attributeGroup['attributeGroupId'],
                'status' => 'ACTIVE',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            );
            $action = $this->sqlPMS->insert();
            $action->values($attributesValuesData);
            $statement = $this->sqlPMS->prepareStatementForSqlObject($action);
            $statement->execute();

            $attributeValueId = $this->dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();

            /** Insert Data into ATTRIBUTES_Definitions Table */
            $this->sqlPMS->setTable($this->_attributesValuesDefinitionsTable);
            $attributesValuesDefinitionsData = array(
                'languageId' => 1,
                'attributeId' => $attributeValueId,
                'attributeValue' => $possibleAttributeValue,
                'status' => 'ACTIVE',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            );
            $action = $this->sqlPMS->insert();
            $action->values($attributesValuesDefinitionsData );
            $statement = $this->sqlPMS->prepareStatementForSqlObject($action);
            $statement->execute();

            /** Update the class variable passed by reference with the value inserted */
            $attributeGroup['options'][] = array('attributeId' => $attributeValueId, 'value' => $possibleAttributeValue);
        }
    }

    /**
     * @param $attributeGroup
     * @param $typeOfAttribute
     * @return array
     */
    private function getAllPossibleAttributesValues($attributeGroup, $typeOfAttribute)
    {
        switch ($typeOfAttribute) {
            case 'marketingAttributes':
                $this->sqlPMS->setTable($this->_attributesExcelMarketing);
                break;
            case 'coreAttributes':
                $this->sqlPMS->setTable($this->_attributesExcelCore);
                break;
        }
        $select = $this->sqlPMS->select();
        $statement = $this->sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $possibleAttributeValues = array();
        foreach ($results as $result) {
            if (!isset($possibleAttributeValues[$result[$attributeGroup['columnExport']]]) && !empty($result[$attributeGroup['columnExport']])) {
                $possibleAttributeValues[$result[$attributeGroup['columnExport']]] = $result[$attributeGroup['columnExport']];
            }
        }

        return $possibleAttributeValues;
    }
    /**
     *
     */
    private function emptyAttributesValues()
    {
        /** Clear Marketing Attributes Values */
        foreach ($this->marketingAttributes as $marketingAttribute) {
            /** Select all values for a attribute Group */
            $this->removeAttributeValueForGroupId($marketingAttribute['attributeGroupId']);
        }
        /** Clear Core Attributes Values */
        foreach ($this->coreAttributes as $coreAttribute) {
            /** Select all values for a attribute Group */
            $this->removeAttributeValueForGroupId($coreAttribute['attributeGroupId']);
        }
    }

    /**
     * @param $attributeGroupId
     */
    private function removeAttributeValueForGroupId($attributeGroupId)
    {
        $this->sqlPMS->setTable($this->_attributesValuesTable);
        $select = $this->sqlPMS->select()->where('attributeGroupId = ' . $attributeGroupId);
        $attributeValues = $this->sqlPMS->prepareStatementForSqlObject($select)->execute();
        foreach ($attributeValues as $attributeValue) {
            /** Remove Attribute Value Definition */
            $update = $this->sqlPMS->update();
            $update->table($this->_attributesValuesDefinitionsTable);
            $update->set(array('STATUS' => 'DELETED'));
            $update->where('attributeId = ' . $attributeValue['attributeId']);
            $statement = $this->sqlPMS->prepareStatementForSqlObject($update);
            $statement->execute();
            /** Remove Attribute Value */
            $update = $this->sqlPMS->update();
            $update->table($this->_attributesValuesTable);
            $update->set(array('STATUS' => 'DELETED'));
            $update->where('attributeId = ' . $attributeValue['attributeId']);
            $statement = $this->sqlPMS->prepareStatementForSqlObject($update);
            $statement->execute();
        }
    }

    /**
     *
     */
    private function emptyAttributesFromProducts()
    {
        /** Clear Marketing Attributes From Products */
        foreach ($this->marketingAttributes as $marketingAttribute) {
            $this->removeAttributeFromProducts($marketingAttribute['attributeGroupId']);
        }
        /** Clear Core Attributes From Products */
        foreach ($this->coreAttributes as $coreAttribute) {
            $this->removeAttributeFromProducts($coreAttribute['attributeGroupId']);
        }
    }

    /**
     * @param $attributeGroupId
     */
    private function removeAttributeFromProducts($attributeGroupId)
    {
        $update = $this->sqlPMS->update();
        $update->table($this->productsRAttributesTable);
        $update->set(array('STATUS' => 'DELETED'));
        $update->where('attributeGroupId = ' . $attributeGroupId);
        $statement = $this->sqlPMS->prepareStatementForSqlObject($update);
        $statement->execute();
    }

    /**
     *
     */
    private function mapCoreAttributes()
    {
        $this->coreAttributes = array (
            'Suppress_from_GPD' => array(
                'columnExport' => 'Suppress_from_GPD',
                'attributeGroupId' =>  '204',
                'attributeGroupType' => 'CHECKBOX',
            ),
            'Made_in_UK' => array(
                'columnExport' => 'Made_in_UK',
                'attributeGroupId' =>  '205',
                'attributeGroupType' => 'CHECKBOX',
            ),
            'Personalised' => array(
                'columnExport' => 'Personalised',
                'attributeGroupId' =>  '187',
                'attributeGroupType' => 'CHECKBOX',
            ),
            'Over_18' => array(
                'columnExport' => 'Over_18',
                'attributeGroupId' =>  '172',
                'attributeGroupType' => 'CHECKBOX',
            ),
            '7_10_Day_Delivery' => array(
                'columnExport' => '7_10_Day_Delivery',
                'attributeGroupId' =>  '206',
                'attributeGroupType' => 'CHECKBOX',
            ),
            '21_Day_Delivery' => array(
                'columnExport' => '21_Day_Delivery',
                'attributeGroupId' =>  '207',
                'attributeGroupType' => 'CHECKBOX',
            ),
            'No_Overseas_Delivery' => array(
                'columnExport' => 'No_Overseas_Delivery',
                'attributeGroupId' =>  '208',
                'attributeGroupType' => 'CHECKBOX',
            ),
            'Gift_Boxed' => array(
                'columnExport' => 'Gift_Boxed',
                'attributeGroupId' =>  '209',
                'attributeGroupType' => 'CHECKBOX',
            ),
        );
    }

    /**
     *
     */
    private function mapMarketingAttributes()
    {
        $this->marketingAttributes = array(
            'Brand' => array(
                'columnExport' => 'Brand',
                'attributeGroupId' =>  '165',
                'attributeGroupType' => 'SELECT',
                'options' => array()
            ),
            'Material' => array(
                'columnExport' => 'Material',
                'attributeGroupId' => '192',
                'attributeGroupType' => 'SELECT',
                'options' => array()
            ),
            'Gender' => array(
                'columnExport' => 'Gender',
                'attributeGroupId' => '193',
                'attributeGroupType' => 'SELECT',
                'options' => array()
            ),
            'Age_Group' => array(
                'columnExport' => 'Age_Group',
                'attributeGroupId' => '194',
                'attributeGroupType' => 'SELECT',
                'options' => array()
            ),
            'Waterproof' => array(
                'columnExport' => 'Waterproof',
                'attributeGroupId' => '195',
                'attributeGroupType' => 'CHECKBOX',
            ),
            'Windproof' => array(
                'columnExport' => 'Windproof',
                'attributeGroupId' => '196',
                'attributeGroupType' => 'CHECKBOX',
            ),
            'UV_Protection' => array(
                'columnExport' => 'UV_Protection',
                'attributeGroupId' => '197',
                'attributeGroupType' => 'CHECKBOX',
            ),
            'Hand_Made' => array(
                'columnExport' => 'Hand_Made',
                'attributeGroupId' => '198',
                'attributeGroupType' => 'CHECKBOX',
            ),
            'Machine_Washable' => array(
                'columnExport' => 'Machine_Washable',
                'attributeGroupId' => '199',
                'attributeGroupType' => 'CHECKBOX',
            ),
            'Exclusive_to_Nauticalia' => array(
                'columnExport' => 'Exclusive_to_Nauticalia',
                'attributeGroupId' => '200',
                'attributeGroupType' => 'CHECKBOX',
            ),
            'Limited_Edition' => array(
                'columnExport' => 'Limited_Edition',
                'attributeGroupId' => '201',
                'attributeGroupType' => 'CHECKBOX',
            ),
            'Free_Standing' => array(
                'columnExport' => 'Free_Standing',
                'attributeGroupId' => '202',
                'attributeGroupType' => 'CHECKBOX',
            ),
            'Wall_Mounted' => array(
                'columnExport' => 'Wall_Mounted',
                'attributeGroupId' => '203',
                'attributeGroupType' => 'CHECKBOX',
            )
        );
    }

    /**
     *
     */
    public function copyOverSkuFromOldPMS()
    {
        $this->copyOverSkuFromOldPmsFor($this->_attributesExcelCore);
        $this->copyOverSkuFromOldPmsFor($this->_attributesExcelMarketing);
    }

    /**
     * @param $attributesTable
     */
    private function copyOverSkuFromOldPmsFor($attributesTable)
    {
        $this->sqlPMS->setTable($attributesTable);
        $select = $this->sqlPMS->select();
        $select->where('oldSku IS NULL');
        $statement = $this->sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $this->sqlOldPMS->setTable($this->productsTableOldPMS);
        foreach ($results as $result) {
            $select = $this->sqlOldPMS->select()
                ->where('id = ' . $result['oldProductId']);
            $statement = $this->sqlOldPMS->prepareStatementForSqlObject($select);
            $oldProduct = $statement->execute()->next();

            $update = $this->sqlPMS->update();
            $update->table($attributesTable);
            $update->set(array('oldSku' => $oldProduct['fprodno']));
            $update->where("id = " . $result['id']);
            $statement = $this->sqlPMS->prepareStatementForSqlObject($update);
            $statement->execute();
        }
    }

    /**
     * @param $from
     * @param $to
     * @return array
     */
    public function getAllPMSVariants($from, $to)
    {
        $this->sqlPMS->setTable(array('pra' => $this->productVariantsTable));
        $select = $this->sqlPMS->select();

        $select->join(
            array('pd' => $this->_productsDefinitionsTable),
            new Expression(
                'pra.productAttributeId = pd.productId AND pd.productIdFrom = "PRODUCTS_R_ATTRIBUTES"'
            ),
            array(
                'variantShortProductName' => 'shortProductName',
                'variantProductName' => 'productName',
                'variantShortDescription' => 'shortDescription',
                'variantLongDescription' => 'longDescription',
                'productDefinitionsId' => 'productDefinitionsId'
            ),
            Select::JOIN_LEFT
        );

        $select->join(
            array('pd2' => $this->_productsDefinitionsTable),
            new Expression(
                'pra.productId = pd2.productId AND pd2.productIdFrom = "PRODUCTS"'
            ),
            array(
                'productShortProductName' => 'shortProductName',
                'productProductName' => 'productName',
                'productShortDescription' => 'shortDescription',
                'productLongDescription' => 'longDescription'
            ),
            Select::JOIN_LEFT
        );

        $select->where('productAttributeId <= ' . $to);
        $select->where('productAttributeId > ' . $from);


        $statement = $this->sqlPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $returnArray = array();
        foreach ($results as $res){
            $returnArray[$res['productId']][] = $res;
        }

        return $returnArray;
    }

    /**
     * @param $pmsProducts
     */
    public function transferDetailsToVariant($pmsProducts)
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $this->dbAdapter->getDriver()->getConnection()->beginTransaction();
        $newDefinitions = 0;
        $updatedDefinitions = 0;
        try {
            foreach ($pmsProducts as $products) {
                foreach ($products as $variant) {
                    $dataToUpdate = array();
                    if (
                        !empty($variant['productShortProductName']) &&
                        $variant['productShortProductName'] !== $variant['variantShortProductName']
                    )
                    {
                        $dataToUpdate['shortProductName'] = $variant['productShortProductName'];
                    }
                    if (
                        !empty($variant['productProductName']) &&
                        $variant['productProductName'] !== $variant['variantProductName']
                    ) {
                        $dataToUpdate['productName'] = $variant['productProductName'];
                    }
                    if (
                        !empty($variant['productShortDescription']) &&
                        $variant['productShortDescription'] !== $variant['variantShortDescription']
                    ) {
                        $dataToUpdate['shortDescription'] = $variant['productShortDescription'];
                    }
                    if (
                        !empty($variant['productLongDescription']) &&
                        $variant['productLongDescription'] !== $variant['variantLongDescription']
                    ) {
                        $dataToUpdate['longDescription'] = $variant['productLongDescription'];
                    }
                    if (!empty($dataToUpdate)) {
                        if (!empty($variant['productDefinitionsId'])) {
                            // Update definitions
                            $update = $this->sqlPMS->update();
                            $update->table($this->_productsDefinitionsTable);
                            $update->set($dataToUpdate);
                            $update->where("productDefinitionsId = " . $variant['productDefinitionsId']);

                            $statement = $this->sqlPMS->prepareStatementForSqlObject($update);
                            $statement->execute();
                            $updatedDefinitions++;
                        } else {
                            // Insert new definition

                            $productDetails = $this->getProductDetailsById($variant['productId'], 1);
                            if (!empty($productDetails['productDefinitionsId'])) {
                                $this->sqlPMS->setTable($this->_productsDefinitionsTable);
                                unset($productDetails['productDefinitionsId']);
                                $productDetails['productIdFrom'] = 'PRODUCTS_R_ATTRIBUTES';
                                $productDetails['productId'] = $variant['productAttributeId'];

                                $action = $this->sqlPMS->insert();
                                $action->values($productDetails);
                                $statement = $this->sqlPMS->prepareStatementForSqlObject($action);
                                $statement->execute();
                                $newDefinitions++;
                            } else {
                                //var_dump($variant);
                                throw new \Exception('product does not have a definitions');
                            }
                        }
                    }
                }
            }
            echo ' new definitions '  . $newDefinitions . '; updated definitions ' . $updatedDefinitions;
            $this->dbAdapter->getDriver()->getConnection()->commit();
            die;
        } catch (\Exception $ex) {
            $this->dbAdapter->getDriver()->getConnection()->rollback();
            //echo '<pre>';
            //var_dump($variant);die;
            throw $ex;
        }
    }

    /**
     * @param $productId
     * @param $languageId
     */
    public function getProductDetailsById($productId, $languageId)
    {
        $this->sqlPMS->setTable($this->_productsDefinitionsTable);
        $select = $this->sqlPMS->select()
            ->where('productId = ' . $productId)
            ->where('languageId = ' . $languageId)
            ->where('productIdFrom = "PRODUCTS"');
        $statement = $this->sqlPMS->prepareStatementForSqlObject($select);
        return $statement->execute()->next();
    }

    /**
     * @param null $limit
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getAllProducts($limit = null)
    {
        $select = $this->sqlOldPMS->select();
        if (!is_null($limit) && is_numeric($limit)) {
            $select->limit($limit);
        }
        $select->order('id DESC');
        $statement = $this->sqlOldPMS->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * @param $oldProducts
     * @param string $column
     * @return array
     */
    public function groupProductsByColumn($oldProducts, $column = 'style')
    {
        $returnArray = array();
        foreach ($oldProducts as $oldProduct) {
            $returnArray[$oldProduct['style']][] = $oldProduct;
        }
        return $returnArray;
    }

    /**
     * @param $oldProduct
     * @return string
     */
    public function buildObjectForDatabase($oldProduct)
    {
        $productsMainDetailsFlag = false;
        /**
         * Parsing all variants of the same style
         */
        foreach ($oldProduct as $variant) {
            /**
             * Build individual objects needed for the whole product object and variant object
             */
            $attributesObject = $this->buildAttributesObject($variant);
            $merchantCategoryObject = $this->buildMerchantCategoryObject($variant);
            $marketingCategoryObject = $this->buildMarketingCategoryObject($variant);
            $pricesObject = $this->buildPricesObject($variant);
            /**
             * Selecting from the first variant the main product holder information and build the object for it
             */
            if (!$productsMainDetailsFlag) {
                $mainProductHolderDetails = $this->buildMainObjectFromDetails($variant, 'PRODUCTS');
                $productsMainDetailsFlag = true;
            }

        }
        return 'test';
    }

    /**
     * @param $variant
     */
    public function buildPricesObject($variant)
    {

    }

    /**
     * @param $variant
     * @return null
     */
    public function buildAttributesObject($variant)
    {
        return null;
    }

    /**
     * @param $variant
     * @return null
     */
    public function buildMerchantCategoryObject($variant)
    {
        return null;
    }

    /**
     * @param $variant
     * @return null
     */
    public function buildMarketingCategoryObject($variant)
    {
        return null;
    }

    /**
     * @param $variant
     * @param $table
     * @return array
     */
    public function buildMainObjectFromDetails($variant, $table)
    {
        /**
         *  Builds the mapping for the main PRODUCTS table or for the
         *  variants table PRODUCTS_R_ATTRIBUTES
         */
        $returnMainObject = array();
        switch ($table) {
            case 'PRODUCTS':
                $returnMainObject = array(
                    'table' => $table,
                    'mainDetails' => array(
                        'manufacturerId' => $this->getManufacturerIdFromDetails($variant),
                        'productTypeId' => $this->getProductTypeIdFromDetails($variant),
                        'productMerchantCategoryId' => $this->getProductMerchantCategoryIdFromDetails($variant),
                        'taxId' => $this->getTaxIdFromDetails($variant),
                        'ean13' => $this->getBarcodeFromDetails($variant),
                        'ecoTax' => $this->getEcoTaxFromDetails($variant),
                        'quantity' => $this->getQUantityFromDetails($variant),
                        'style' => $this->getStyleFromDetails($variant),
                        'sku' => $this->getSkuFromDetails($variant),
                        'supplierReference' => $this->getSupplierReferenceFromDetails($variant),
                        'status' => $this->getStatusFromDetails($variant),
                        'created' => $this->getCreatedDateFromDetails($variant),
                        'modified' => $this->getModifiedDateFromDetails($variant)
                    )
                );
                break;
            case 'PRODUCTS_R_ATTRIBUTES':
                break;
        }
        return $returnMainObject;
    }

    /**
     * @param $variant
     * @return null
     */
    public function getModifiedDateFromDetails($variant)
    {
        return null;
    }

    /**
     * @param $variant
     * @return null
     */
    public function getCreatedDateFromDetails($variant)
    {
        return null;
    }

    /**
     * @param $variant
     * @return null
     */
    public function getStatusFromDetails($variant)
    {
        return null;
    }

    /**
     * @param $variant
     * @return null
     */
    public function getSupplierReferenceFromDetails($variant)
    {
        return null;
    }

    /**
     * @param $variant
     * @return null
     */
    public function getSkuFromDetails($variant)
    {
        return null;
    }

    /**
     * @param $variant
     * @return null
     */
    public function getStyleFromDetails($variant)
    {
        return null;
    }

    /**
     * @param $variant
     * @return int
     */
    public function getQuantityFromDetails($variant)
    {
        return 1;
    }

    /**
     * @param $variant
     * @return int
     */
    public function getEcoTaxFromDetails($variant)
    {
        return 1;
    }

    /**
     * @param $variant
     * @return null
     */
    public function getBarcodeFromDetails($variant)
    {
        return null;
    }

    /**
     * @param $variant
     * @return int
     */
    public function getTaxIdFromDetails($variant)
    {
        return 1;
    }

    /**
     * @param $variant
     * @return null
     */
    public function getManufacturerIdFromDetails($variant)
    {
        /**
         * Could not find a manufacturer in the old pms
         * Returning null
         */
        return null;
    }

    /**
     * @param $variant
     * @return int
     */
    public function getProductMerchantCategoryIdFromDetails($variant)
    {
        return 1;
    }

    /**
     * @param $variant
     * @return int
     */
    public function getProductTypeIdFromDetails($variant)
    {
        /**
         * Temporary
         */
        return 1;
    }

    /**
     * @param $oldProductObject
     * @param $oldProduct
     * @return bool
     */
    public function integrityCheckObject($oldProductObject, $oldProduct)
    {
        return true;
    }

    /**
     * @param $oldProductObject
     * @return bool
     */
    public function saveObjectInDatabase($oldProductObject)
    {
        return true;
    }

    /**
     *
     */
    public function emptyDatabase()
    {
//        $query = "TRUNCATE TABLE ";
//        $this->dbAdapter->query("SET FOREIGN_KEY_CHECKS = 0;")->execute();
//        /**
//         * EMPTY COMMON ATTRIBUTES TABLES
//         */
//        $this->dbAdapter->query($query . $this->commonAttributesValuesGUIRepresentationTable)->execute();
//        $this->dbAdapter->query($query . $this->commonAttributesGUIRepresentationTable)->execute();
//        $this->dbAdapter->query($query . $this->commonAttributesValuesEquivalenceTable)->execute();
//        $this->dbAdapter->query($query . $this->commonAttributesValuesDefinitionsTable)->execute();
//        $this->dbAdapter->query($query . $this->commonAttributesValuesTable)->execute();
//        $this->dbAdapter->query($query . $this->commonAttributesDefinitionsTable)->execute();
//        $this->dbAdapter->query($query . $this->commonAttributesTable)->execute();
//        /******************************************** **********************************************/
//
//        $this->dbAdapter->query("SET FOREIGN_KEY_CHECKS = 1;")->execute();
    }
}