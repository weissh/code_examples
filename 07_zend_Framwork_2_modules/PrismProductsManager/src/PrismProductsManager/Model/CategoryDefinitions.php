<?php

namespace PrismProductsManager\Model;

class CategoryDefinitions
{
    public $categoryDefinitionId;
    public $categoryId;
    public $languageId;
    public $versionId;
    public $versionDescription;
    public $categoryName;
    public $urlName;
    public $metaTitle;
    public $metaDescription;
    public $metaKeyword;
    public $shortDescription;
    public $longDescription;
    public $status;
    public $makeLive;
    public $created;
    public $modified;
    public $locked;

    public function exchangeArray($data)
    {
        $this->categoryDefinitionId     = (isset($data['categoryDefinitionId'])) ? $data['categoryDefinitionId'] : null;
        $this->categoryId     = (isset($data['categoryId'])) ? $data['categoryId'] : null;
        $this->languageId     = (isset($data['languageId'])) ? $data['languageId'] : null;
        $this->versionId     = (isset($data['versionId'])) ? $data['versionId'] : null;
        $this->versionDescription     = (isset($data['versionDescription'])) ? $data['versionDescription'] : null;
        $this->categoryName     = (isset($data['categoryName'])) ? $data['categoryName'] : null;
        $this->urlName     = (isset($data['urlName'])) ? $data['urlName'] : null;
        $this->metaTitle     = (isset($data['metaTitle'])) ? $data['metaTitle'] : null;
        $this->metaDescription     = (isset($data['metaDescription'])) ? $data['metaDescription'] : null;
        $this->metaKeyword     = (isset($data['metaKeyword'])) ? $data['metaKeyword'] : null;
        $this->shortDescription     = (isset($data['shortDescription'])) ? $data['shortDescription'] : null;
        $this->longDescription     = (isset($data['longDescription'])) ? $data['longDescription'] : null;
        $this->status     = (isset($data['status'])) ? $data['status'] : null;
        $this->makeLive     = (isset($data['makeLive'])) ? $data['makeLive'] : null;
        $this->created     = (isset($data['created'])) ? $data['created'] : null;
        $this->modified     = (isset($data['modified'])) ? $data['modified'] : null;
        $this->locked     = (isset($data['locked'])) ? $data['locked'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
