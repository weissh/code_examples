<?php
namespace PrismProductsManager\Model;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class CurrencyEntity
{
    protected $_currencyId;
    protected $_name;
    protected $_isDefault;
    protected $_isoCode;
    protected $_htmlCode;
    protected $_blank;
    protected $_format;
    protected $_decimals;
    protected $_conversionRate;
    protected $_status;
    
    public function getCurrencyId()
    {
        return $this->_currencyId;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getIsDefault()
    {
        return $this->_isDefault;
    }

    public function getIsoCode()
    {
        return $this->_isoCode;
    }

    public function getHtmlCode()
    {
        return $this->_htmlCode;
    }

    public function getBlank()
    {
        return $this->_blank;
    }

    public function getFormat()
    {
        return $this->_format;
    }

    public function getDecimals()
    {
        return $this->_decimals;
    }

    public function getConversionRate()
    {
        return $this->_conversionRate;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function setCurrencyId($_currencyId)
    {
        $this->_currencyId = $_currencyId;
    }

    public function setName($_name)
    {
        $this->_name = $_name;
    }

    public function setIsDefault($_isDefault)
    {
        $this->_isDefault = $_isDefault;
    }

    public function setIsoCode($_isoCode)
    {
        $this->_isoCode = $_isoCode;
    }

    public function setHtmlCode($_htmlCode)
    {
        $this->_htmlCode = $_htmlCode;
    }

    public function setBlank($_blank)
    {
        $this->_blank = $_blank;
    }

    public function setFormat($_format)
    {
        $this->_format = $_format;
    }

    public function setDecimals($_decimals)
    {
        $this->_decimals = $_decimals;
    }

    public function setConversionRate($_conversionRate)
    {
        $this->_conversionRate = $_conversionRate;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }


}

