<?php

namespace PrismProductsManager\Model\Entity;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class TaxesGroupsDefinitionsEntity
{
    
    protected $_taxGroupDefinitionId;
    protected $_languageId;
    protected $_taxGroupId;
    protected $_name;
    protected $_description;
    protected $_status;
    protected $_created;
    protected $_modified;
       
    
    public function __construct()
    {
        $currentDatetime = new \Zend\Db\Sql\Expression("NOW()");
        
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
        $this->_status = 'ACTIVE';
    }
    
    public function getTaxGroupDefinitionId()
    {
        return $this->_taxGroupDefinitionId;
    }

    public function getLanguageId()
    {
        return $this->_languageId;
    }

    public function getTaxGroupId()
    {
        return $this->_taxGroupId;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getDescription()
    {
        return $this->_description;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setTaxGroupDefinitionId($_taxGroupDefinitionId)
    {
        $this->_taxGroupDefinitionId = $_taxGroupDefinitionId;
    }

    public function setLanguageId($_languageId)
    {
        $this->_languageId = $_languageId;
    }

    public function setTaxGroupId($_taxGroupId)
    {
        $this->_taxGroupId = $_taxGroupId;
    }

    public function setName($_name)
    {
        $this->_name = $_name;
    }

    public function setDescription($_description)
    {
        $this->_description = $_description;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }
    
}
   