<?php

namespace PrismProductsManager\Model\Entity;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class TaxesGroupsEntity
{
    
    protected $_taxGroupId;
    protected $_status;
    protected $_created;
    protected $_modified;
       
    
    public function __construct()
    {
        $currentDatetime = new \Zend\Db\Sql\Expression("NOW()");
        
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
        $this->_status = 'ACTIVE';
    }
    
    public function getTaxGroupId()
    {
        return $this->_taxGroupId;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setTaxGroupId($_taxGroupId)
    {
        $this->_taxGroupId = $_taxGroupId;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }


}
   