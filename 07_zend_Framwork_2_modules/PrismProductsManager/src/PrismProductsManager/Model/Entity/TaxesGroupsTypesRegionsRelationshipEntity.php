<?php

namespace PrismProductsManager\Model\Entity;

/**
 * This class holds relationship between the following tables
 * 1. Tax groups
 * 2. Tax types
 * 3. Tax regions
 * 
 * @author emmanuel.edegbo
 *        
 */
class TaxesGroupsTypesRegionsRelationshipEntity
{
    
    protected $_taxGroupTypeRegionId;
    protected $_taxGroupId;
    protected $_taxTypeId;
    protected $_taxRegionId;
    protected $_value;
    protected $_priority;
    protected $_status;
    protected $_created;
    protected $_modified;
       
    
    public function __construct()
    {
        $currentDatetime = new \Zend\Db\Sql\Expression("NOW()");
        
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
        $this->_status = 'ACTIVE';
    }
    
    public function getTaxGroupTypeRegionId()
    {
        return $this->_taxGroupTypeRegionId;
    }

    public function getTaxGroupId()
    {
        return $this->_taxGroupId;
    }

    public function getTaxTypeId()
    {
        return $this->_taxTypeId;
    }

    public function getTaxRegionId()
    {
        return $this->_taxRegionId;
    }

    public function getValue()
    {
        return $this->_value;
    }

    public function getPriority()
    {
        return $this->_priority;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setTaxGroupTypeRegionId($_taxGroupTypeRegionId)
    {
        $this->_taxGroupTypeRegionId = $_taxGroupTypeRegionId;
    }

    public function setTaxGroupId($_taxGroupId)
    {
        $this->_taxGroupId = $_taxGroupId;
    }

    public function setTaxTypeId($_taxTypeId)
    {
        $this->_taxTypeId = $_taxTypeId;
    }

    public function setTaxRegionId($_taxRegionId)
    {
        $this->_taxRegionId = $_taxRegionId;
    }

    public function setValue($_value)
    {
        $this->_value = $_value;
    }

    public function setPriority($_priority)
    {
        $this->_priority = $_priority;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }
    
}
