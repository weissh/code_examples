<?php

namespace PrismProductsManager\Model\Entity;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class ProductsRelatedProductsEntity
{
    
    protected $_productProductId; //The primary key of the table
    protected $_productIdFrom;
    protected $_productId;
    protected $_relatedProductId;
    protected $_status;
    protected $_created;
    protected $_modified;
           
    public function __construct()
    {
        $currentDatetime = new \Zend\Db\Sql\Expression("NOW()");
        
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
        $this->_status = 'ACTIVE';
    }
    
    public function getProductProductId()
    {
        return $this->_productProductId;
    }

    public function getProductIdFrom()
    {
        return $this->_productIdFrom;
    }

    public function getProductId()
    {
        return $this->_productId;
    }

    public function getRelatedProductId()
    {
        return $this->_relatedProductId;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setProductProductId($_productProductId)
    {
        $this->_productProductId = $_productProductId;
    }

    public function setProductIdFrom($_productIdFrom)
    {
        $this->_productIdFrom = $_productIdFrom;
    }

    public function setProductId($_productId)
    {
        $this->_productId = $_productId;
    }

    public function setRelatedProductId($_relatedProductId)
    {
        $this->_relatedProductId = $_relatedProductId;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }


}
