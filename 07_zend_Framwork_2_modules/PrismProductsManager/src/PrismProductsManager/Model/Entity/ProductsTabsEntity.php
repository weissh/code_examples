<?php

namespace PrismProductsManager\Model\Entity;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class ProductsTabsEntity
{
    protected $_productId;
    protected $_tabId;
    protected $_priority;
    protected $_status;
    protected $_created;
    protected $_modified;    
    protected $_tabGroupId;

    public function __construct()
    {
        $currentDatetime = new \Zend\Db\Sql\Expression("NOW()");
        
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
        $this->_status = 'ACTIVE';
        $this->_priority = 0;
    }
    
    public function getTabGroupId()
    {
        return $this->_tabGroupId;
    }

    public function setTabGroupId($_tabGroupId)
    {
        $this->_tabGroupId = $_tabGroupId;
    }
        
    public function getProductId()
    {
        return $this->_productId;
    }

    public function getTabId()
    {
        return $this->_tabId;
    }

    public function getPriority()
    {
        return $this->_priority;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setProductId($_productId)
    {
        $this->_productId = $_productId;
    }

    public function setTabId($_tabId)
    {
        $this->_tabId = $_tabId;
    }

    public function setPriority($_priority)
    {
        $this->_priority = $_priority;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }


    
}
