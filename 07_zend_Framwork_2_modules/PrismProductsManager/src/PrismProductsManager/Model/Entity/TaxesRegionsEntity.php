<?php

namespace PrismProductsManager\Model\Entity;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class TaxesRegionsEntity
{
    
    protected $_taxRegionId;
    protected $_taxRegion;
    protected $_status;
    protected $_created;
    protected $_modified;
       
    
    public function __construct()
    {
        $currentDatetime = new \Zend\Db\Sql\Expression("NOW()");
        
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
        $this->_status = 'ACTIVE';
    }
    
    public function getTaxRegionId()
    {
        return $this->_taxRegionId;
    }

    public function getTaxRegion()
    {
        return $this->_taxRegion;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setTaxRegionId($_taxRegionId)
    {
        $this->_taxRegionId = $_taxRegionId;
    }

    public function setTaxRegion($_taxRegion)
    {
        $this->_taxRegion = $_taxRegion;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }


}
   