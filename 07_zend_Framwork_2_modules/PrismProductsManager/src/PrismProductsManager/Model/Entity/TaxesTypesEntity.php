<?php

namespace PrismProductsManager\Model\Entity;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class TaxesTypesEntity
{
    
    protected $_taxTypeId;
    protected $_calculateFrom;
    protected $_status;
    protected $_created;
    protected $_modified;
           
    public function __construct()
    {
        $currentDatetime = new \Zend\Db\Sql\Expression("NOW()");
        
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
        $this->_status = 'ACTIVE';
    }
    
    public function getTaxTypeId()
    {
        return $this->_taxTypeId;
    }

    public function getCalculateFrom()
    {
        return $this->_calculateFrom;
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setTaxTypeId($_taxTypeId)
    {
        $this->_taxTypeId = $_taxTypeId;
    }

    public function setCalculateFrom($_calculateFrom)
    {
        $this->_calculateFrom = $_calculateFrom;
    }

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }


}
    