<?php

namespace PrismProductsManager\Model;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class ProductsAttributesSkuRulesEntity
{
    
    protected $_productAttributeSkuRuleId;
    protected $_productAttributeId;
    protected $_skuRuleId;
    protected $_skuRuleTable;
    protected $_created;
    protected $_modified;

    public function __construct()
    {
        $currentDatetime = new \Zend\Db\Sql\Expression("NOW()");
        
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
        
    }
    
    public function getProductAttributeSkuRuleId()
    {
        return $this->_productAttributeSkuRuleId;
    }

    public function getProductAttributeId()
    {
        return $this->_productAttributeId;
    }

    public function getSkuRuleId()
    {
        return $this->_skuRuleId;
    }

    public function getSkuRuleTable()
    {
        return $this->_skuRuleTable;
    }

    public function getCreated()
    {
        return $this->_created;
    }

    public function getModified()
    {
        return $this->_modified;
    }

    public function setProductAttributeSkuRuleId($_productAttributeSkuRuleId)
    {
        $this->_productAttributeSkuRuleId = $_productAttributeSkuRuleId;
    }

    public function setProductAttributeId($_productAttributeId)
    {
        $this->_productAttributeId = $_productAttributeId;
    }

    public function setSkuRuleId($_skuRuleId)
    {
        $this->_skuRuleId = $_skuRuleId;
    }

    public function setSkuRuleTable($_skuRuleTable)
    {
        $this->_skuRuleTable = $_skuRuleTable;
    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }


}