<?php
namespace PrismProductsManager\Model;

use Doctrine\Common\Collections\ArrayCollection;
use PrismProductsManager\Entity\CommonAttributes;
use PrismProductsManager\Entity\CommonAttributesValues;
use PrismProductsManager\Entity\HistoricalValue;
use PrismQueueManager\Service\QueueService;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Ddl\Column\Datetime;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\EventManager\EventManagerInterface;

/**
 * Class CommonAttributesMapper
 *
 * @package PrismProductsManager\Model
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class CommonAttributesMapper
{

    /**
     *
     * @var Sql
     */
    protected $sqlObject;

    /**
     *
     * @var Adapter
     */
    protected $dbAdapter;

    /**
     *
     * @var
     *
     */
    protected $languages;

    /**
     *
     * @var string
     */
    protected $commonAttributesTable = 'COMMON_ATTRIBUTES';

    /**
     *
     * @var string
     */
    protected $commonAttributesDefinitionsTable = 'COMMON_ATTRIBUTES_Definitions';

    /**
     *
     * @var string
     */
    protected $commonAttributesGUIRepresentationTable = 'COMMON_ATTRIBUTES_GUI_Representation';

    /**
     *
     * @var string
     */
    protected $commonAttributesValuesTable = 'COMMON_ATTRIBUTES_VALUES';

    /**
     *
     * @var string
     */
    protected $commonAttributesValuesDefinitionsTable = 'COMMON_ATTRIBUTES_VALUES_Definitions';

    /**
     *
     * @var string
     */
    protected $commonAttributesValuesEquivalenceTable = 'COMMON_ATTRIBUTES_VALUES_Equivalence';

    /**
     *
     * @var string
     */
    protected $commonAttributesValuesGUIRepresentationTable = 'COMMON_ATTRIBUTES_VALUES_GUI_Representation';

    protected $commonAttributesViewType = 'COMMON_ATTRIBUTES_View_Type';

    /**
     *
     * @var string
     */
    protected $productsCommonAttributesTable = 'PRODUCTS_Common_Attributes';

    /**
     * @var string
     */
    protected $commonAttributesSizePropertyRelationsTable = 'COMMON_ATTRIBUTES_Size_Property_Relations';

    /**
     * @var string
     */
    protected $commonAttributesValuesSizePropertyRelationsTable = 'COMMON_ATTRIBUTES_VALUES_Size_Property_Relations';

    /** @var QueueService $queueService */
    private $queueService;

    /** @var  $clientConfig array */
    private $clientConfig;

    /**
     * CommonAttributesMapper constructor.
     * @param Adapter $dbAdapter
     * @param $clientConfig
     * @param $language
     * @param QueueService $queueService
     */
    public function __construct(Adapter $dbAdapter, $clientConfig, $language, QueueService $queueService)
    {
        $this->sqlObject = new Sql($dbAdapter);
        $this->languages = $language['site-languages'];
        $this->currentLanguage = $language['current-language'];
        $this->dbAdapter = $dbAdapter;
        $this->queueService = $queueService;
        $this->clientConfig = $clientConfig;
    }

    /**
     *
     * @param null $search
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getAllCommonAttributes($search = null)
    {
        $this->sqlObject->setTable(array(
            'ca' => $this->commonAttributesTable
        ));
        $select = $this->sqlObject->select();
        $select->join(array(
            'cad' => $this->commonAttributesDefinitionsTable
        ), new Expression('ca.commonAttributeId = cad.commonAttributeId
                AND cad.languageId = ' . $this->currentLanguage), array(
            '*'
        ), Select::JOIN_LEFT);

        if (!is_null($search)) {
            $select->where->like('cad.description', '%' . $search . '%');
        }

        $statement = $this->sqlObject->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $commonAttributes = array();
        $this->sqlObject->setTable($this->commonAttributesValuesTable);
        foreach ($results as $result) {
            $select = $this->sqlObject->select()->where('commonAttributeId = ' . $result['commonAttributeId']);
            $statement = $this->sqlObject->prepareStatementForSqlObject($select);
            $valueCount = $statement->execute()->count();
            $result['valueCount'] = $valueCount;
            $commonAttributes[] = $result;
        }
        return $commonAttributes;
    }

    public function getCommonAttributeViewType($commonAttributeId)
    {
        $this->sqlObject->setTable(array(
            'cagr' => $this->commonAttributesGUIRepresentationTable
        ));
        $select = $this->sqlObject->select()
            ->columns(array(
                'commonAttributesGuiRepresentationId'
            ))
            ->join(array(
                'cavt' => $this->commonAttributesViewType
            ), new Expression('cagr.commonAttributesViewTypeId = cavt.commonAttributeViewTypeId'), array(
                'commonAttributeViewType'
            ), Select::JOIN_LEFT)
            ->join(array(
                'cavd' => $this->commonAttributesDefinitionsTable
            ), new Expression('cagr.commonAttributeId = cavd.commonAttributeId'), array(
                'description'
            ), Select::JOIN_LEFT)
            ->where('cagr.commonAttributeId = ' . $commonAttributeId);
        $statement = $this->sqlObject->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();
        return $result;
    }

    /**
     *
     * @param
     *            $commonAttributeId
     * @param null $search
     * @return array
     */
    public function getAllCommonAttributesValues($commonAttributeId, $search = null)
    {
        $this->sqlObject->setTable(array(
            'cav' => $this->commonAttributesValuesTable
        ));
        $select = $this->sqlObject->select();
        $select->join(array(
            'cavd' => $this->commonAttributesValuesDefinitionsTable
        ), new Expression('cav.commonAttributeValueId = cavd.commonAttributeValueId
                AND cavd.languageId = ' . $this->currentLanguage), array(
            '*'
        ), Select::JOIN_LEFT);
        $select->where('cav.commonAttributeId = ' . $commonAttributeId);
        if (!is_null($search)) {
            $select->where->like('cavd.value', '%' . $search . '%');
        }

        $statement = $this->sqlObject->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $commonAttributesValues = array();
        foreach ($results as $result) {
            $commonAttributesValues[] = $result;
        }

        return $commonAttributesValues;
    }

    /**
     *
     * @param
     *            $data
     * @return bool/commonAttributeId
     */
    public function addNewCommonAttribute($data)
    {
        /**
         * Begin mysql transaction
         */
        $this->dbAdapter->getDriver()
            ->getConnection()
            ->beginTransaction();
        try {
            /**
             * Insert data into $this->commonAttributesTable
             */
            $commonAttributeId = $this->insertDataIntoCommonAttributesTable($data);
            /**
             * Insert data into $this->commonAttributesDefinitionsTable
             */
            $this->insertDataIntoCommonAttributesDefinitionsTable($data, $commonAttributeId);
            /**
             * Insert data into $this->commonAttributesGUIRepresentationTable
             */
            $this->insertDataIntoCommonAttributesGUIRepresentationTable($data, $commonAttributeId);
            /**
             * Commit mysql transaction to database
             */
            $this->dbAdapter->getDriver()
                ->getConnection()
                ->commit();
            /**
             * Return common attribute id
             */
            return $commonAttributeId;
        } catch (\Exception $ex) {
            /**
             * In case of fatal error rollback mysql transaction and return false
             */
            $this->dbAdapter->getDriver()
                ->getConnection()
                ->rollback();
            return false;
        }
    }

    /**
     *
     * @param
     *            $data
     * @param
     *            $commonAttributeId
     */
    public function insertDataIntoCommonAttributesGUIRepresentationTable($data, $commonAttributeId)
    {
        /**
         * Build data for the $this->commonAttributesDefinitionsTable without languages
         */
        $commonAttributesGUIRepresentationData = array(
            'commonAttributeId' => $commonAttributeId,
            'commonAttributesViewTypeId' => $data['commonAttributesViewTypeId'],
            'commonAttributesParentId' => $commonAttributeId,
            'pmsDisplay' => 1,
            'shopDisplay' => 0,
            'modifiedAt' => date('Y-m-d'),
            'createdAt' => date('Y-m-d')
        );
        /**
         * Loop through the existing languages and build a entry for each language
         */
        $this->sqlObject->setTable($this->commonAttributesGUIRepresentationTable);
        $action = $this->sqlObject->insert();
        $action->values($commonAttributesGUIRepresentationData);
        $statement = $this->sqlObject->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     *
     * @param
     *            $data
     * @param
     *            $commonAttributeId
     * @return bool
     */
    public function addNewCommonAttributeValue($data, $commonAttributeId)
    {
        /**
         * Begin mysql transaction
         */
        $this->dbAdapter->getDriver()
            ->getConnection()
            ->beginTransaction();
        try {
            /**
             * Insert data into $this->commonAttributesValuesTable
             */
            $commonAttributeValueId = $this->insertDataIntoCommonAttributesValuesTable($data, $commonAttributeId);
            /**
             * Insert data into $this->commonAttributesValuesDefinitionsTable
             */
            $this->insertDataIntoCommonAttributesDefinitionsValuesTable($data, $commonAttributeValueId);
            /**
             * Insert data into $this->commonAttributesValuesGUIRepresentationTable
             */
            $this->insertDataIntoCommonAttributesValuesGUIRepresentationTable($data, $commonAttributeValueId);
            /**
             * Commit mysql transaction to database
             */
            $this->dbAdapter->getDriver()
                ->getConnection()
                ->commit();
        } catch (\Exception $ex) {
            /**
             * In case of fatal error rollback mysql transaction and return false
             */
            $this->dbAdapter->getDriver()
                ->getConnection()
                ->rollback();
            return false;
        }
    }

    /**
     *
     * @param
     *            $data
     * @param
     *            $commonAttributeValueId
     */
    public function insertDataIntoCommonAttributesValuesGUIRepresentationTable($data, $commonAttributeValueId)
    {
        $commonAttributesValuesGUIRepresentationTableData = array(
            'commonAttributeValueId' => $commonAttributeValueId,
            'commonAttributeValueOrder' => $data['commonAttributeValueOrder'],
            'pmsDisplay' => 0,
            'shopDisplay' => 1,
            'createdAt' => date('Y-m-d'),
            'modifiedAt' => date('Y-m-d')
        );
        $this->sqlObject->setTable($this->commonAttributesValuesGUIRepresentationTable);
        $action = $this->sqlObject->insert();
        $action->values($commonAttributesValuesGUIRepresentationTableData);
        $statement = $this->sqlObject->prepareStatementForSqlObject($action);
        $statement->execute();
    }

    /**
     *
     * @param
     *            $data
     * @param
     *            $commonAttributeId
     * @return int
     */
    public function insertDataIntoCommonAttributesValuesTable($data, $commonAttributeId)
    {
        /**
         * Build Data for the $this->commonAttributesTable; Values are hardcoded - TODO: Need to change for LTS
         */
        $commonAttributesValuesTableData = array(
            'commonAttributeId' => $commonAttributeId,
            'enabled' => 1,
            'commonAttributeValueSkuCode' => $data['commonAttributeValueSkuCode'],
            'territoryId' => 1,
            'modifiedAt' => date('Y-m-d'),
            'createdAt' => date('Y-m-d')
        );
        /**
         * Insert data in $this->commonAttributesTable
         */
        $this->sqlObject->setTable($this->commonAttributesValuesTable);
        $action = $this->sqlObject->insert();
        $action->values($commonAttributesValuesTableData);
        $statement = $this->sqlObject->prepareStatementForSqlObject($action);
        $statement->execute();
        return $this->dbAdapter->getDriver()
            ->getConnection()
            ->getLastGeneratedValue();
    }

    /**
     *
     * @param
     *            $data
     * @param
     *            $commonAttributeValueId
     */
    public function insertDataIntoCommonAttributesDefinitionsValuesTable($data, $commonAttributeValueId)
    {
        /**
         * Build data for the $this->commonAttributesDefinitionsTable without languages
         */
        $commonAttributesDefinitionsValuesTableData = array(
            'commonAttributeValueId' => $commonAttributeValueId,
            'value' => $data['value'],
            'modifiedAt' => date('Y-m-d'),
            'createdAt' => date('Y-m-d')
        );
        /**
         * Loop through the existing languages and build a entry for each language
         */
        $this->sqlObject->setTable($this->commonAttributesValuesDefinitionsTable);
        foreach ($this->languages as $language) {
            $commonAttributesDefinitionsValuesTableData['languageId'] = $language;
            $action = $this->sqlObject->insert();
            $action->values($commonAttributesDefinitionsValuesTableData);
            $statement = $this->sqlObject->prepareStatementForSqlObject($action);
            $statement->execute();
        }
    }

    /**
     * Method needs to be accessed locally and within a mysql transaction
     * See method addNewCommonAttribute for example
     *
     * @param
     *            $data
     * @param
     *            $commonAttributeId
     */
    private function insertDataIntoCommonAttributesDefinitionsTable($data, $commonAttributeId)
    {
        /**
         * Build data for the $this->commonAttributesDefinitionsTable without languages
         */
        $commonAttributesDefinitionsTableData = array(
            'commonAttributeId' => $commonAttributeId,
            'description' => $data['description'],
            'modifiedAt' => date('Y-m-d'),
            'createdAt' => date('Y-m-d')
        );
        /**
         * Loop through the existing languages and build a entry for each language
         */
        $this->sqlObject->setTable($this->commonAttributesDefinitionsTable);
        foreach ($this->languages as $language) {
            $commonAttributesDefinitionsTableData['languageId'] = $language;
            $action = $this->sqlObject->insert();
            $action->values($commonAttributesDefinitionsTableData);
            $statement = $this->sqlObject->prepareStatementForSqlObject($action);
            $statement->execute();
        }
    }

    /**
     * Method needs to be accessed locally and within a mysql transaction
     * See method addNewCommonAttribute for example
     *
     * @param
     *            $data
     * @return int / commonAttributeId
     */
    private function insertDataIntoCommonAttributesTable($data)
    {
        /**
         * Build Data for the $this->commonAttributesTable; Values are hardcoded - TODO: Need to change for LTS
         */
        $isColour = 0;
        $isSize = 0;
        if (isset($data['isColour'])) {
            $isColour = 1;
        }
        if (isset($data['isSize'])) {
            $isSize = 1;
        }
        $commonAttributesTableData = array(
            'usedForFiltering' => 0,
            'usedForMerchantCategories' => 1,
            'isColour' => $isColour,
            'isSize' => $isSize,
            'enabled' => 1,
            'modifiedAt' => date('Y-m-d'),
            'createdAt' => date('Y-m-d')
        );
        /**
         * Insert data in $this->commonAttributesTable
         */
        $this->sqlObject->setTable($this->commonAttributesTable);
        $action = $this->sqlObject->insert();
        $action->values($commonAttributesTableData);
        $statement = $this->sqlObject->prepareStatementForSqlObject($action);
        $statement->execute();
        return $this->dbAdapter->getDriver()
            ->getConnection()
            ->getLastGeneratedValue();
    }

    /**
     *
     * @param
     *            $commonAttributeValueId
     */
    public function getCommonAttributeValueByCommonAttributeId($commonAttributeValueId)
    {
        $this->sqlObject->setTable(array(
            'cav' => $this->commonAttributesValuesTable
        ));
        $select = $this->sqlObject->select();
        $select->join(array(
            'cavd' => $this->commonAttributesValuesDefinitionsTable
        ), new Expression('cav.commonAttributeValueId = cavd.commonAttributeValueId
                AND cavd.languageId = ' . $this->currentLanguage), array(
            'value'
        ), Select::JOIN_LEFT);
        $select->join(array(
            'cavg' => $this->commonAttributesValuesGUIRepresentationTable
        ), new Expression('cav.commonAttributeValueId = cavg.commonAttributeValueId'), array(
            'commonAttributeValueOrder'
        ), Select::JOIN_LEFT);
        $select->where('cav.commonAttributeValueId = ' . $commonAttributeValueId);

        $statement = $this->sqlObject->prepareStatementForSqlObject($select);
        return $statement->execute()->next();
    }

    /**
     *
     * @param
     *            $data
     * @param
     *            $commonAttributeId
     * @return bool
     */
    public function editCommonAttributeValue($data, $commonAttributeId)
    {
        /**
         * Begin mysql transaction
         */
        $this->dbAdapter->getDriver()
            ->getConnection()
            ->beginTransaction();
        try {
            /**
             * Update $this->commonAttributesValuesTable
             */
            $dataUpdate = array(
                'commonAttributeValueSkuCode' => $data['commonAttributeValueSkuCode']
            );
            $update = $this->sqlObject->update();
            $update->table($this->commonAttributesValuesTable);
            $update->set($dataUpdate);
            $update->where('commonAttributeValueId = ' . $data['commonAttributeValueId']);
            $statement = $this->sqlObject->prepareStatementForSqlObject($update);
            $statement->execute();

            /**
             * Update $this->commonAttributesValuesDefinitionsTable
             */
            $dataUpdate = array(
                'value' => $data['value']
            );
            $update = $this->sqlObject->update();
            $update->table($this->commonAttributesValuesDefinitionsTable);
            $update->set($dataUpdate);
            $update->where('commonAttributeValueId = ' . $data['commonAttributeValueId']);
            $statement = $this->sqlObject->prepareStatementForSqlObject($update);
            $statement->execute();

            /**
             * Check if the order needs to be updated or inserted there a new row
             */
            $this->sqlObject->setTable($this->commonAttributesValuesGUIRepresentationTable);
            $select = $this->sqlObject->select()->where('commonAttributeValueId = ' . $data['commonAttributeValueId']);
            $statement = $this->sqlObject->prepareStatementForSqlObject($select);
            $valueGUICount = $statement->execute()->count();

            if ($valueGUICount > 0) {
                /**
                 * Update $this->commonAttributesValuesGUIRepresentationTable
                 */
                $dataUpdate = array(
                    'commonAttributeValueOrder' => $data['commonAttributeValueOrder']
                );
                $update = $this->sqlObject->update();
                $update->table($this->commonAttributesValuesGUIRepresentationTable);
                $update->set($dataUpdate);
                $update->where('commonAttributeValueId = ' . $data['commonAttributeValueId']);
                $statement = $this->sqlObject->prepareStatementForSqlObject($update);
                $statement->execute();
            } else {
                /**
                 * Insert into $this->commonAttributesValuesGUIRepresentationTable
                 */
                $commonAttributesValuesGUIRepresentationData = array(
                    'commonAttributeValueId' => $data['commonAttributeValueId'],
                    'commonAttributeValueOrder' => $data['commonAttributeValueOrder'],
                    'pmsDisplay' => 0,
                    'shopDisplay' => 1,
                    'modifiedAt' => date('Y-m-d'),
                    'createdAt' => date('Y-m-d')
                );
                /**
                 * Insert data in $this->commonAttributesTable
                 */
                $this->sqlObject->setTable($this->commonAttributesValuesGUIRepresentationTable);
                $action = $this->sqlObject->insert();
                $action->values($commonAttributesValuesGUIRepresentationData);
                $statement = $this->sqlObject->prepareStatementForSqlObject($action);
                $statement->execute();
            }
            /**
             * Update $this->commonAttributesValuesGUIRepresentationTable
             */
            $dataUpdate = array(
                'commonAttributeValueOrder' => $data['commonAttributeValueOrder']
            );
            $update = $this->sqlObject->update();
            $update->table($this->commonAttributesValuesGUIRepresentationTable);
            $update->set($dataUpdate);
            $update->where('commonAttributeValueId = ' . $data['commonAttributeValueId']);
            $statement = $this->sqlObject->prepareStatementForSqlObject($update);
            $statement->execute();

            /**
             * Commit mysql transaction to database
             */
            $this->dbAdapter->getDriver()
                ->getConnection()
                ->commit();
        } catch (\Exception $ex) {
            /**
             * In case of fatal error rollback mysql transaction and return false
             */
            $this->dbAdapter->getDriver()
                ->getConnection()
                ->rollback();
            return false;
        }
    }

    /**
     *
     * @param
     *            $attributeValueId
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getCommonAttributeValueById($attributeValueId)
    {
        $this->sqlObject->setTable(array(
            'cav' => $this->commonAttributesValuesTable
        ));
        $select = $this->sqlObject->select();
        $select->join(array(
            'cavd' => $this->commonAttributesValuesDefinitionsTable
        ), new Expression('cavd.commonAttributeValueId = cav.commonAttributeValueId'), array(
            'languageId',
            'value'
        ), Select::JOIN_LEFT);
        $select->join(array(
            'cad' => $this->commonAttributesDefinitionsTable
        ), new Expression('cav.commonAttributeId = cad.commonAttributeId'), array(
            'description'
        ), Select::JOIN_LEFT);
        $select->join(array(
            'cavg' => $this->commonAttributesValuesGUIRepresentationTable
        ), new Expression('cavg.commonAttributeValueId = cav.commonAttributeValueId'), array(
            'commonAttributeValueOrder'
        ), Select::JOIN_LEFT);
        $select->where('cav.commonAttributeValueId = ' . $attributeValueId);
        $statement = $this->sqlObject->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    /**
     *
     * @param
     *            $attributeId
     * @param
     *            $productOrVariantId
     * @param
     *            $productIdFrom
     * @return array
     */
    public function getAttributeValueForProduct($attributeId, $productOrVariantId, $productIdFrom)
    {
        $attributesValues = '';
        $this->sqlObject->setTable($this->productsCommonAttributesTable);
        $select = $this->sqlObject->select()->where(array(
            'commonAttributeId' => $attributeId,
            'productOrVariantId' => $productOrVariantId,
            'productIdFrom' => $productIdFrom
        ));
        $statement = $this->sqlObject->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!empty($results)) {
            if (count($results) > 1) {
                foreach ($results as $result) {
                    $attributesValues .= $result['commonAttributeValue'] . ', ';
                }
                return $attributesValues;
            } else {
                $attributeValue = $results->next();
                if (!empty($attributeValue)) {
                    return $attributeValue['commonAttributeValue'];
                }
            }
        }
    }

    /**
     *
     * @param int $commonAttributeId
     * @param string $variantsIds
     * @param
     *            array | string $commonAttributeIdValue
     * @param string $commonAttributeIdDataType
     */
    public function isSameValueCrossOptions($commonAttributeId, $variantsIds, $commonAttributeIdValue, $commonAttributeIdDataType)
    {
        $result = false;
        switch ($commonAttributeIdDataType) {
            case 'CHECKBOX':
            case 'SELECT':
            case 'TEXT':
            case 'DATEPICKER':
            case 'TEXTAREA':
            case 'DATEPICKER - DATE ONLY':
                // $commonAttributeId got same value corss all option (variants)
                $result = $this->isSameValueCrossOptionsQueryResult($commonAttributeId, $variantsIds, $commonAttributeIdValue);
                break;
            case 'MULTISELECT':
                // do loop throw data
                $result = $this->isSameValueCrossOptionsQueryResult($commonAttributeId, $variantsIds, $commonAttributeIdValue, true);
                break;
        }
        return $result;
    }

    protected function isSameValueCrossOptionsQueryResult($commonAttributeId, $variantsIds, $commonAttributeIdValue, $valueIsArray = false)
    {
        $isSame = false;
        try {
            $query = "SELECT commonAttributeValue FROM $this->productsCommonAttributesTable 
            where 
            	productOrVariantId IN($variantsIds) 
            	AND productIdFrom = 'PRODUCTS_R_ATTRIBUTES'	
            	AND commonAttributeId = '$commonAttributeId'
            group by commonAttributeValue;
            ";

            $result = $this->dbAdapter->query($query)->execute();

            if ($valueIsArray == false) {
                $data = $result->current();
                if ($result->count() == 1 || $data['commonAttributeValue'] == $commonAttributeIdValue) {
                    $isSame = true;
                }
            }

            if ($valueIsArray == true) {
                $data = array();
                while ($result->valid()) {
                    $data[] = $result->current()['commonAttributeValue'];
                    $result->next();
                }
                // compare arrays (multiselct values)
                if (is_array($commonAttributeIdValue) && is_array($data)
                    && array_diff($commonAttributeIdValue, $data) === array_diff($data, $commonAttributeIdValue)
                ) {
                    $isSame = true;
                }
            }

        } catch (\Exception $ex) {
            return false;
        }
        return $isSame;
    }

    public function updateCommonAttbuteValueOnStyleLevel($productOrVariantId, $commonAttributeId, $commonAttributeIdValue, $productIdFrom)
    {
        try {
            // delete the record first
            $this->deleteCommonAttributeStyleLevel($productOrVariantId, $commonAttributeId, $productIdFrom);

            if (is_array($commonAttributeIdValue)) {
                foreach ($commonAttributeIdValue as $value) {
                    // inset record
                    $this->insertCommonAttributeStyleLevel($productOrVariantId, $commonAttributeId, $value, $productIdFrom);
                }
            } else {
                // inset record
                $this->insertCommonAttributeStyleLevel($productOrVariantId, $commonAttributeId, $commonAttributeIdValue, $productIdFrom);
            }
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function insertNewAttributeValueForProduct($productOrVariantId, $commonAttributeId, $commonAttributeIdValue, $productIdFrom)
    {
        try {
            // inset record
            $this->insertCommonAttributeStyleLevel($productOrVariantId, $commonAttributeId, $commonAttributeIdValue, $productIdFrom);
        } catch (\Exception $ex) {
            return false;
        }
    }

    protected function deleteCommonAttributeStyleLevel($productOrVariantId, $commonAttributeId, $productIdFrom)
    {
        $isDeleted = false;
        try {
            $query = "DELETE FROM $this->productsCommonAttributesTable  
                        WHERE 
                        productOrVariantId = '$productOrVariantId'
                        AND productIdFrom = '$productIdFrom'
                        AND commonAttributeId = '$commonAttributeId'";

            $this->dbAdapter->query($query)->execute();
            $isDeleted = true;
        } catch (\Exception $ex) {
            return $isDeleted;
        }
        return $isDeleted;
    }

    protected function insertCommonAttributeStyleLevel($productOrVariantId, $commonAttributeId, $commonAttributeIdValue, $productIdFrom)
    {
        $isInserted = false;
        $now = date('Y-m-d H:i:s');
        try {
            $query = "INSERT INTO $this->productsCommonAttributesTable 
            (`productOrVariantId`, `productIdFrom`, `commonAttributeId`, `commonAttributeValue`, `created`) 
            VALUES ('$productOrVariantId', '$productIdFrom', '$commonAttributeId', '$commonAttributeIdValue', '$now')";

            $this->dbAdapter->query($query)->execute();
            $isInserted = true;
        } catch (\Exception $ex) {
            return $isInserted;
        }
        return $isInserted;
    }

    /**
     * Get all related common attributes set as size property
     * @param int $commonAttributeId
     * @return array
     */
    public function getCommonAttributesSizePropertyRelations($commonAttributeId)
    {
        $this->sqlObject->setTable($this->commonAttributesSizePropertyRelationsTable);

        $select = $this->sqlObject->select();
        $select->where(array('commonAttributeSizePropertyParentId' => $commonAttributeId));

        $statement = $this->sqlObject->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $returnArray = [];
        foreach ($results as $value) {
            $returnArray[] = $value['commonAttributeSizePropertyChildId'];
        }

        return $returnArray;
    }

    /**
     * Create common attribute relation with common attribute set as size property
     * @param $commonAttributeId
     * @param array $commonAttributeSizeProperty
     * @return bool
     */
    public function createCommonAttributeSizePropertyRelation($commonAttributeId, $commonAttributeSizeProperty)
    {
        if (is_array($commonAttributeSizeProperty) && !empty($commonAttributeSizeProperty)) {
            try {
                for ($i = 0; $i < count($commonAttributeSizeProperty); $i++) {
                    $sizePropertyCommonAttributeId = $commonAttributeSizeProperty[$i];
                    $insertData = [
                        'commonAttributeSizePropertyParentId' => $commonAttributeId,
                        'commonAttributeSizePropertyChildId' => $sizePropertyCommonAttributeId,
                    ];
                    $this->sqlObject->setTable($this->commonAttributesSizePropertyRelationsTable);
                    $action = $this->sqlObject->insert();
                    $action->values($insertData);
                    $statement = $this->sqlObject->prepareStatementForSqlObject($action);
                    $statement->execute();
                }
                return true;
            } catch (\Exception $ex) {
                return false;

            }
        }
        return true;
    }

    /**
     * Save realtion between values of common attributes and values of common attribute set as size property size
     * @param int $commonAttributeId
     * @param int array $commonAttributeValueId
     * @param $data
     * @return bool
     */
    public function createCommonAttributeValueSizePropertyRelation($commonAttributeId, $commonAttributeValueId, $data)
    {
        if ($data['sizePropertyCommonAttributeCount'] > 0) {
            try {
                for ($i = 0; $i < $data['sizePropertyCommonAttributeCount']; $i++) {
                    $commonAttributesSizePropertyRelationsId = $this->getCommonAttributesSizePropertyRelationsId($commonAttributeId, $data['sizePropertyCommonAttributeId_' . $i]);
                    if ($commonAttributesSizePropertyRelationsId !== null) {
                        $insertData = [
                            'commonAttributesSizePropertyRelationsId' => $commonAttributesSizePropertyRelationsId,
                            'commonAttributeValuesSizePropertyParentId' => $commonAttributeValueId,
                            'commonAttributeValuesSizePropertyChildId' => $data['sizePropertyCommonAttribute_' . $i]
                        ];
                        $this->sqlObject->setTable($this->commonAttributesValuesSizePropertyRelationsTable);
                        $action = $this->sqlObject->insert();
                        $action->values($insertData);
                        $statement = $this->sqlObject->prepareStatementForSqlObject($action);
                        $statement->execute();
                    }
                }
                return true;
            } catch (\Exception $ex) {
                return false;

            }
        }
        return true;
    }

    /**
     * Edit CommonAttributeValueSizePropertyRelation
     * @param int $commonAttributeId
     * @param int $commonAttributeValueId
     * @param array $data
     * @return bool
     */
    public function editCommonAttributeValueSizePropertyRelation($commonAttributeId, $commonAttributeValueId, $data)
    {
        if ($data['sizePropertyCommonAttributeCount'] > 0) {
            try {
                for ($i = 0; $i < $data['sizePropertyCommonAttributeCount']; $i++) {
                    $commonAttributesSizePropertyRelationsId = $this->getCommonAttributesSizePropertyRelationsId($commonAttributeId, $data['sizePropertyCommonAttributeId_' . $i]);

                    if ($commonAttributesSizePropertyRelationsId !== null) {
                        $isCommonAttributesValuesSizePropertyRelationsExist = $this->isCommonAttributesValuesSizePropertyRelationsExist($commonAttributeValueId, $commonAttributesSizePropertyRelationsId);
                        if ($isCommonAttributesValuesSizePropertyRelationsExist) {
                            $dataUpdate = [
                                'commonAttributeValuesSizePropertyChildId' => $data['sizePropertyCommonAttribute_' . $i]
                            ];

                            $update = $this->sqlObject->update();
                            $update->table($this->commonAttributesValuesSizePropertyRelationsTable);
                            $update->set($dataUpdate);
                            $update->where('commonAttributeValuesSizePropertyParentId = ' . $commonAttributeValueId);
                            $update->where('commonAttributesSizePropertyRelationsId = ' . $commonAttributesSizePropertyRelationsId);
                            $statement = $this->sqlObject->prepareStatementForSqlObject($update);
                            $statement->execute();
                        } else {
                            $insertData = [
                                'commonAttributesSizePropertyRelationsId' => $commonAttributesSizePropertyRelationsId,
                                'commonAttributeValuesSizePropertyParentId' => $commonAttributeValueId,
                                'commonAttributeValuesSizePropertyChildId' => $data['sizePropertyCommonAttribute_' . $i]
                            ];
                            $this->sqlObject->setTable($this->commonAttributesValuesSizePropertyRelationsTable);
                            $action = $this->sqlObject->insert();
                            $action->values($insertData);
                            $statement = $this->sqlObject->prepareStatementForSqlObject($action);
                            $statement->execute();
                        }

                    }
                    // sync all product too spectrum that use the value common attribute
                }
                return true;
            } catch (\Exception $ex) {
                return false;

            }
        }
        return true;
    }

    /**
     * Is Common Attributes Values Size Property Relations Exist
     * @param int $commonAttributeValuesSizePropertyParentId
     * @param int $commonAttributesSizePropertyRelationsId
     * @return bool
     */
    public function isCommonAttributesValuesSizePropertyRelationsExist($commonAttributeValuesSizePropertyParentId, $commonAttributesSizePropertyRelationsId)
    {
        try {
            $this->sqlObject->setTable($this->commonAttributesValuesSizePropertyRelationsTable);

            $select = $this->sqlObject->select();
            $select->where('commonAttributeValuesSizePropertyParentId = ' . $commonAttributeValuesSizePropertyParentId);
            $select->where('commonAttributesSizePropertyRelationsId = ' . $commonAttributesSizePropertyRelationsId);
            $statement = $this->sqlObject->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            if ($results->count() == 1) {
                return true;
            }

        } catch (\Exception $ex) {
            return false;
        }
        return false;
    }

    /**
     * Delete CommonAttributeValueSizePropertyRelation
     * @param int $commonAttributeValueParentId
     * @param int $commonAttributeValueChildId
     * @return bool
     */
    public function deleteCommonAttributeValueSizePropertyRelation($commonAttributeValueParentId, $commonAttributeValueChildId)
    {
        try {
            $this->sqlObject->setTable($this->commonAttributesValuesSizePropertyRelationsTable);
            $action = $this->sqlObject->delete();
            $action->where("commonAttributeValuesSizePropertyParentId = $commonAttributeValueParentId");
            $action->where("commonAttributeValuesSizePropertyChildId = $commonAttributeValueChildId");
            $statement = $this->sqlObject->prepareStatementForSqlObject($action);
            $statement->execute();
            return true;
        } catch (\Exception $ex) {
            return false;

        }
        return false;
    }

    /**
     * Get CommonAttributesSizePropertyRelationsId
     * @param int $commonAttributeId
     * @param int $sizePropertyCommonAttributeId
     * @return void null | int
     */
    public function getCommonAttributesSizePropertyRelationsId($commonAttributeId, $sizePropertyCommonAttributeId)
    {
        $this->sqlObject->setTable($this->commonAttributesSizePropertyRelationsTable);

        $select = $this->sqlObject->select();
        $select->where(array('commonAttributeSizePropertyParentId' => $commonAttributeId));
        $select->where(array('commonAttributeSizePropertyChildId' => $sizePropertyCommonAttributeId));

        $statement = $this->sqlObject->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() == 1) {
            $result = $results->next();
            return $result['commonAttributesSizePropertyRelationsId'];
        }
        return null;

    }

    /**
     * Delete common attribute relation with common attribute set as size property
     * Please note if common attribute was deleted form from then records realted to this common attribute will be automatically
     * deleted (Foreign key cascade)
     * @param $commonAttributeId
     * @param array $commonAttributeSizeProperty
     * @return bool
     */
    public function deleteCommonAttributeSizePropertyRelation($commonAttributeId, $commonAttributeSizeProperty)
    {
        if (is_array($commonAttributeSizeProperty) && !empty($commonAttributeSizeProperty)) {
            try {
                for ($i = 0; $i < count($commonAttributeSizeProperty); $i++) {
                    $sizePropertyCommonAttributeId = $commonAttributeSizeProperty[$i];

                    $this->sqlObject->setTable($this->commonAttributesSizePropertyRelationsTable);
                    $action = $this->sqlObject->delete();
                    $action->where("commonAttributeSizePropertyParentId = $commonAttributeId");
                    $action->where("commonAttributeSizePropertyChildId = $sizePropertyCommonAttributeId");
                    $statement = $this->sqlObject->prepareStatementForSqlObject($action);
                    $statement->execute();
                }
                return true;
            } catch (\Exception $ex) {
                return false;

            }
        } else {
            try {
                $this->sqlObject->setTable($this->commonAttributesSizePropertyRelationsTable);
                $action = $this->sqlObject->delete();
                $action->where("commonAttributeSizePropertyParentId = $commonAttributeId");
                $statement = $this->sqlObject->prepareStatementForSqlObject($action);
                $statement->execute();
                return true;
            } catch (\Exception $ex) {
                return false;
            }
        }
    }

    /**
     * @param int $commonAttributeValueId
     * @param int $commonAttributeId
     * @param int $commonAttributeSizePropertyId
     * @return null
     */
    public function getCommonAttributesSizePropertyValues($commonAttributeValueId, $commonAttributeId, $commonAttributeSizePropertyId)
    {
        $this->sqlObject->setTable([
            'cav' => $this->commonAttributesValuesSizePropertyRelationsTable
        ]);
        $select = $this->sqlObject->select();

        $select->join(['ca' => $this->commonAttributesSizePropertyRelationsTable],
            "cav.commonAttributesSizePropertyRelationsId = ca.commonAttributesSizePropertyRelationsId");

        $select->where([
            "cav.commonAttributeValuesSizePropertyParentId" => $commonAttributeValueId,
            "ca.commonAttributeSizePropertyParentId" => $commonAttributeId,
            "ca.commonAttributeSizePropertyChildId" => $commonAttributeSizePropertyId
        ]);
        $statement = $this->sqlObject->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results->count() == 1) {
            $result = $results->next();
            return $result['commonAttributeValuesSizePropertyChildId'];
        }
        return null;
    }

    /**
     * @param int $commonAttributeValueParentId
     * @return array
     */
    public function getCommonAttributeValueSizePropertyIdsByParnetValueId($commonAttributeValueParentId)
    {
        $this->sqlObject->setTable($this->commonAttributesValuesSizePropertyRelationsTable);

        $select = $this->sqlObject->select();
        $select->where(array('commonAttributeValuesSizePropertyParentId' => $commonAttributeValueParentId));

        $statement = $this->sqlObject->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $returnArray = [];
        foreach ($results as $value) {
            $returnArray[] = $value['commonAttributeValuesSizePropertyChildId'];
        }

        return $returnArray;
    }

    /**
     * @param $commonAttributeId
     * @param $fromCommonAttributeValueId
     * @param $toCommonAttributeValueId
     * @param CommonAttributesValues $toEntity
     * @return ArrayCollection|bool
     */
    public function updateAllProductsCommonAttributeValues(
        $commonAttributeId,
        $fromCommonAttributeValueId,
        $toCommonAttributeValueId,
        CommonAttributesValues $toEntity)
    {
        try {
            $this->dbAdapter->getDriver()
                ->getConnection()
                ->beginTransaction();

            $historicalEntriesCollection = new ArrayCollection();

            $fromDateEntity = $toEntity->getCommonAttributesValuesAutoFromDate();
            $fromDate = null;
            if (!is_null($fromDateEntity)) {
                $fromDate = $fromDateEntity->getFromDate();
            }

            $this->sqlObject->setTable(array(
                'pca' => $this->productsCommonAttributesTable
            ));
            $select = $this->sqlObject->select();
            $select->join(array(
                'pra' => 'PRODUCTS_R_ATTRIBUTES'
            ),
                new Expression('pca.productOrVariantId = pra.productAttributeId'),
                ['productAttributeId', 'productId'],
                Select::JOIN_INNER);
            $select->where('pca.commonAttributeId = ' . $commonAttributeId);
            $select->where('pca.commonAttributeValue = ' . $fromCommonAttributeValueId);

            $statement = $this->sqlObject->prepareStatementForSqlObject($select);

            $selectedResults = $statement->execute();

            foreach ($selectedResults as $selectedResult) {

                $update = $this->sqlObject->update();
                $update->table($this->productsCommonAttributesTable);
                $update->set([
                    'commonAttributeValue' => $toCommonAttributeValueId
                ]);
                $update->where('productsCommonAttributesId = ' . $selectedResult['productsCommonAttributesId']);
                $statement = $this->sqlObject->prepareStatementForSqlObject($update);
                $statement->execute();

                $productIdFrom = $selectedResult['productIdFrom'] ? $selectedResult['productIdFrom'] : false;
                $historicalEntry = new HistoricalValue();
                $optionCode = '';
                switch ($productIdFrom) {
                    case 'PRODUCTS_R_ATTRIBUTES':
                        $historicalEntry->setStyle(false);
                        $optionCode = $this->getOptionCodeForVariantId($selectedResult['productOrVariantId']);
                        break;
                    case 'PRODUCTS':
                        $historicalEntry->setStyle(true);
                        break;
                }
                $commonAttribute = new CommonAttributes();
                $commonAttribute->setCommonattributeid($selectedResult['commonAttributeId']);

                $historicalEntry->setProductOrVariantId($selectedResult['productOrVariantId']);
                $historicalEntry->setUserId(null);
                $historicalEntry->setCommonAttribute($commonAttribute);
                $historicalEntry->setAttributeName('');
                $historicalEntry->setValue($toCommonAttributeValueId);
                $historicalEntry->setFromDate($fromDate);
                $historicalEntry->setUpdateType(HistoricalValue::UPDATE_TYPE_Auto_Date);
                $historicalEntry->setModified(new \DateTime());
                $historicalEntry->setCreated(new \DateTime());
                $historicalEntry->setModifiedBy(null);
                $historicalEntry->setOptionCode($optionCode);
                $historicalEntriesCollection->add($historicalEntry);
            }

            /**
             * TODO: Add $selectedResults to queue for SIP and Spectrum
             * TODO: When LTS will ask for it - not included in CCR LTS385 !
             */
            $this->dbAdapter->getDriver()
                ->getConnection()
                ->commit();

            return $historicalEntriesCollection;
        } catch (\Exception $ex) {
            $this->dbAdapter->getDriver()
                ->getConnection()
                ->rollback();
            return false;
        }
    }

    /**
     *
     * @param
     *            $variantId
     * @return array
     */
    public function getColourForVariantId($variantId)
    {
        $this->sqlObject->setTable(array(
            'prarsr' => 'PRODUCTS_R_ATTRIBUTES_R_SKU_Rules'
        ));
        $select = $this->sqlObject->select()
            ->where('prarsr.productAttributeId = ' . $variantId)
            ->where('prarsr.skuRuleTable = "COLOURS_Groups"')
            ->join(array(
                'cg' =>'COLOURS_Groups'
            ), new Expression('cg.colourGroupId = prarsr.skuRuleId'), array(
                '*'
            ), Select::JOIN_LEFT)
            ->join(array(
                'cd' => 'COLOURS_Groups_Definitions'
            ), new Expression('cd.colourGroupId = cg.colourGroupId'), array(
                '*'
            ), Select::JOIN_LEFT)
            ->where('cd.languageId = 1');
        $select->limit(1);
        $statement = $this->sqlObject->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();

        return is_array($result) ? $result : array();
    }

    /**
     *
     * @param
     *            $variantId
     * @return mixed
     */
    public function getProductIdFromVariantId($variantId)
    {
        $this->sqlObject->setTable(array(
            'prv' => 'PRODUCTS_R_ATTRIBUTES'
        ));
        $select = $this->sqlObject->select();
        $select->columns(array(
            'productId'
        ));
        $select->where('productAttributeId = "' . $variantId . '"');
        $statement = $this->sqlObject->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() == 1) {
            return $results->next()['productId'];
        } else {
            return false;
        }
    }

    /**
     * Retrieve product information
     *
     * @param
     *            $productId
     * @return array
     */
    public function getMinimumProductsDetails($productId, $fullDetails = false)
    {
        $this->sqlObject->setTable(array(
            'p' => 'PRODUCTS'
        ));
        $select = $this->sqlObject->select();
        if ($fullDetails) {
            $select->join(array(
                'ptd' => 'PRODUCTS_Types_Definitions'
            ), "p.productTypeId = ptd.productTypeId", array(
                'name'
            ));
            $select->columns(array(
                '*'
            ));
        } else {
            $select->columns(array(
                'productId',
                'status',
                'ean13',
                'style'
            ));
        }

        $select->where(array(
            'p.productId' => $productId
        ));
        $select->limit(1);
        $statement = $this->sqlObject->prepareStatementForSqlObject($select);
        $product = $statement->execute()->next();
        return $product;
    }

    /**
     *
     * @param
     *            $variantId
     * @return string
     */
    public function getOptionCodeForVariantId($variantId)
    {
        $colourUsed = $this->getColourForVariantId($variantId);
        $productId = $this->getProductIdFromVariantId($variantId);
        $minimumDetails = $this->getMinimumProductsDetails($productId);

        $productStyle = '';
        $colourCode = '';

        if (isset($minimumDetails['style'])) {
            $productStyle = $minimumDetails['style'];
        }

        if (isset($colourUsed['groupCode'])) {
            $colourCode = $colourUsed['groupCode'];
        }
        $separator = '';
        if (!empty($this->clientConfig['settings']['skuGenerationSeparator'])) {
            $separator = $this->clientConfig['settings']['skuGenerationSeparator'];
        }

        return $productStyle . $separator .  $colourCode;
    }

}