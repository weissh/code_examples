<?php

namespace PrismProductsManager\Model;

/**
 *
 * @author emmanuel.edegbo
 *        
 */
class ProductsEntity
{
    
    protected $_productId;
    
    protected $_productMerchantCategoryId = null;
    
    protected $_productTypeId;

    protected  $_manufacturerId;
    
    protected $_ean13;
    
    protected $_ecoTax = 0.00;
    
    protected $_quantity = 0;
    
    protected $_sku;

    protected $_style;

    /**
     * @param mixed $style
     */
    public function setStyle($style)
    {
        $this->_style = $style;
    }

    /**
     * @return mixed
     */
    public function getStyle()
    {
        return $this->_style;
    }

    protected $_supplierreference;
        
    protected $_status = 'INACTIVE';
    
    protected $_created;
    
    protected $_modified;

    public function __construct()
    {
        $currentDatetime = new \Zend\Db\Sql\Expression("NOW()");
        
        $this->_created = $currentDatetime;
        $this->_modified = $currentDatetime;
    }
    
    public function getproductId()
    {
        return $this->_productId;
    }
    
    public function getProductMerchantCategoryId()
    {
        return $this->_productMerchantCategoryId;
    }

    public function setProductMerchantCategoryId($_productMerchantCategoryId)
    {
        $this->_productMerchantCategoryId = $_productMerchantCategoryId;
    }
    
    public function getProductTypeId()
    {
        return $this->_productTypeId;
    }

    public function setProductTypeId($_productTypeId)
    {
        $this->_productTypeId = $_productTypeId;
    }
    
    public function getManufacturerId()
    {
        return $this->_manufacturerId;
    }

    public function getEan13()
    {
        return $this->_ean13;
    }

    public function getecoTax()
    {
        return $this->_ecoTax;
    }

    public function getquantity()
    {
        return $this->_quantity;
    }

    public function getsku()
    {
        return $this->_sku;
    }

    public function getsupplierreference()
    {
        return $this->_supplierreference;
    }

    /*public function getweight()
    {
        return $this->_weight;
    }

    public function getonSale()
    {
        return $this->_onSale;
    }

    public function getoutOfStock()
    {
        return $this->_outOfStock;
    }

    public function getallowBackOrder()
    {
        return $this->_allowBackOrder;
    }

    public function getquantityDiscount()
    {
        return $this->_quantityDiscount;
    }

    public function getcustomizable()
    {
        return $this->_customizable;
    }

    public function getuploadableFiles()
    {
        return $this->_uploadableFiles;
    }

    public function getapplyPostage()
    {
        return $this->_applyPostage;
    }*/

    public function getstatus()
    {
        return $this->_status;
    }

//    public function getindex()
//    {
//        return $this->_index;
//    }

    public function getcreated()
    {
        return $this->_created;
    }

    public function getmodified()
    {
        return $this->_modified;
    }

    public function setProductId($_productId)
    {
        $this->_productId = $_productId;
    }

    public function setManufacturerId($_manufacturerId)
    {
        $this->_manufacturerId = $_manufacturerId;
    }

    public function setEan13($_ean13)
    {
        $this->_ean13 = $_ean13;
    }

    public function setEcoTax($_ecoTax)
    {
        $this->_ecoTax = $_ecoTax;
    }

    public function setQuantity($_quantity)
    {
        $this->_quantity = $_quantity;
    }

    public function setSku($_sku)
    {
        $this->_sku = $_sku;
    }

    public function setSupplierreference($_supplierreference)
    {
        $this->_supplierreference = $_supplierreference;
    }

//    public function setWeight($_weight)
//    {
//        $this->_weight = $_weight;
//    }

    /*public function setOnSale($_onSale)
    {
        $this->_onSale = $_onSale;
    }

    public function setOutOfStock($_outOfStock)
    {
        $this->_outOfStock = $_outOfStock;
    }

    public function setAllowBackOrder($_allowBackOrder)
    {
        $this->_allowBackOrder = $_allowBackOrder;
    }

    public function setQuantityDiscount($_quantityDiscount)
    {
        $this->_quantityDiscount = $_quantityDiscount;
    }

    public function setCustomizable($_customizable)
    {
        $this->_customizable = $_customizable;
    }

    public function setUploadableFiles($_uploadableFiles)
    {
        $this->_uploadableFiles = $_uploadableFiles;
    }

    public function setApplyPostage($_applyPostage)
    {
        $this->_applyPostage = $_applyPostage;
    }*/

    public function setStatus($_status)
    {
        $this->_status = $_status;
    }

//    public function setIndex($_index)
//    {
//        $this->_index = $_index;
//    }

    public function setCreated($_created)
    {
        $this->_created = $_created;
    }

    public function setModified($_modified)
    {
        $this->_modified = $_modified;
    }


    
}
