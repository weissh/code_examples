<?php

namespace PrismProductsManager\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

//use Zend\Db\ResultSet\HydratingResultSet;
use PrismProductsManager\Model\ProductsMapper;
//use PrismProductsManager\Model\ProductsMapperVariants;

/**
 * This class is a sub class of ProductsMapper class. 
 * It was created to reduce the length of the parent calss.
 * 
 * It holds business logic for searching for products.
 */

class ProductsMapperSearchProducts extends ProductsMapper
{

    protected $_sql;
    protected $_hydrator;
    protected $_dbAdapter;
    protected $_productsMapperVariants;

    public function __construct(
            Sql $sql
            ,$hydrator 
            ,Adapter $dbAdapter 
            ,$productsMapperVariants
            )
    {
        $this->_sql = $sql;
        $this->_hydrator = $hydrator;
        $this->_dbAdapter = $dbAdapter;
        $this->_productsMapperVariants = $productsMapperVariants;
    }
    
    /**
     * Search product by product name
     * 
     * @param string $search
     * @param int $languageId
     * @return array
     */
    protected function _searchByProductName($search, $languageId = 1)
    {
        
        $returnArray = array();
            
        try {

            $this->_sql->setTable(array('p' => $this->_tableName));
            $select = $this->_sql->select();
            $select->columns(array('productId'));
            $select->where(array(
                    'pd.languageId' => $languageId,
                ));
            $select->where
                ->nest
                ->like('pd.productName', '%' . $search . '%')
                ->or
                ->like('pd.shortProductName', '%' . $search . '%')
                ->unnest;
            $select->where
                ->nest
                ->like('p.status', 'ACTIVE')
                ->or
                ->like('p.status', 'INACTIVE')
                ->unnest;
            $select->where
                ->nest
                ->like('pd.status', 'ACTIVE')
                ->or
                ->like('pd.status', 'INACTIVE')
                ->unnest;
            $select->join(
                array('pd' => $this->_productsDefinitionsTable), //Set join table and alias
                'p.productId = pd.productId', 
                array('productDefinitionsId', 'productName'),
                Select::JOIN_LEFT
            );
            
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
         
            $position = 0;
            $searchResultArray = array();
            foreach ($results as $key => $value) {
                                
                $basicDetails = $value;
                $variants = $this->_productsMapperVariants->getProductVariantsByProductID($value['productId']);
                
                $searchResultArray['basicDetails'] = $basicDetails;
                $searchResultArray['variants'] = $variants;
                $searchResultArray['variantCount'] = count($variants);
                
                $position++;
            }
            
            $returnArray['dataCount'] = $position;
            $returnArray['data'] = $searchResultArray;
            
        } catch (\Exception $ex) {
            
            //var_dump($ex);
        }

        return $returnArray;
    }



    public function _searchBySku($search, $languageId = 1)
    {

        $returnArray = array();
        $productsTable = 'PRODUCTS';
        $variantsTable = 'PRODUCTS_R_ATTRIBUTES';
        $productsDefinitionsTable = "PRODUCTS_Definitions";


        try {

            $this->_sql->setTable(array('pd' => $productsDefinitionsTable));

            // First sql
            $sql1 = $this->_sql->select();
            $sql1->columns(array(
                        'shortProductName',
                        'productName',
                        'productIdFrom',


            ));
            $sql1->where(array(
                'pd.languageId' => $languageId,
            ));
            $sql1->where
                ->nest
                ->like('p.sku',  $search . '%')
                ->unnest;
            $sql1->where
                ->nest
                ->like('pd.productIdFrom', 'PRODUCTS')
                ->unnest;
            $sql1->where
                ->nest
                ->like('p.status', 'ACTIVE')
                ->unnest;
            $sql1->where
                ->nest
                ->like('pd.status', 'ACTIVE')
                ->unnest;
            $sql1->join(
                array('p' => $productsTable), //Set join table and alias
                'p.productId = pd.productId',
                array(
                    'productId',
                    'sku',
                    'ean13',
                    'productAttributeId' => new Expression(' "0" ')
                ),
                Select::JOIN_LEFT
            );

            // Second sql
            $sql2 = $this->_sql->select();
            $sql2->columns(array(
                'shortProductName',
                'productName',
                'productIdFrom'
            ));
            $sql2->where(array(
                'pd.languageId' => $languageId,
            ));

            $sql2->where
                ->nest
                ->like('pr.sku', $search . '%')
                ->unnest;
            $sql2->where
                ->nest
                ->like('pd.productIdFrom', 'PRODUCTS_R_ATTRIBUTES')
                ->unnest;
            $sql2->where
                ->nest
                ->like('pr.status', 'ACTIVE')
                ->unnest;
            $sql2->where
                ->nest
                ->like('pd.status', 'ACTIVE')
                ->unnest;
            $sql2->join(
                array('pr' => $variantsTable), //Set join table and alias
                'pr.productAttributeId = pd.productId',
                array(
                    'productId',
                    'sku',
                    'ean13',
                    'productAttributeId'
                ),
                Select::JOIN_LEFT
            );

            $select = $sql1->combine ( $sql2 );
            $statement = $this->_sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            $position = 0;
            $searchResultArray = array();

            //Add all results to array
            foreach($results as $key =>$value){
                $searchResultArray[] = $value;
            }

            // Collect all product Ids from variant products
            // This will be used later to decide
            // if we need to add the products or only variants
            $variantIdsList = array();
            foreach ($searchResultArray as $key => $value) {
                if ($value['productIdFrom'] == 'PRODUCTS_R_ATTRIBUTES'){
                    if(!in_array($value['productId'],$variantIdsList)){
                        array_push($variantIdsList,$value['productId']);
                    }
                }
            }

            // Check if product is from products table,
            // If so check if product has variants,
            // If not add it to return array,
            // Else add all variants
            foreach($searchResultArray as $key => $value){
                if($value['productIdFrom'] == 'PRODUCTS') {
                    if(!in_array($value['productId'],$variantIdsList)){
                        $returnArray[$key] = $value;
                    }
                } else {
                    $returnArray[$key] = $value;
                }
            }


        } catch (Exception $ex) {
            //var_dump($ex);
        }

        return $returnArray;

    }
}
