<?php

namespace PrismProductsManager\Model;

use Zend\Db\Adapter\Adapter;
use PrismProductsManager\Model\CategoriesEntity;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Expression;

class CategoriesDefinitionsMapper
{

    protected $_tableName = 'CATEGORIES_Definitions';
    protected $_dbAdapter;
    protected $_sql;

    public function __construct(Adapter $dbAdapter)
    {
        $this->_dbAdapter = $dbAdapter;
        $this->_sql = new Sql($dbAdapter);
        $this->_sql->setTable($this->_tableName);
    }

    public function saveCategoryDefinitions(CategoriesEntity $category)
    {
        $hydrator = new ClassMethods(false);
        $data = $hydrator->extract($category);

        $result = false;
        try {

            $action = $this->_sql->insert();
            unset($data['categoryId']);
            $action->values($data);

            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $result = $statement->execute();
        } catch (\Exception $e) {
            // var_dump($e);
            // log any error
        }

        return $result;
    }

}
