<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Territories
 *
 * @ORM\Table(name="TERRITORIES", indexes={@ORM\Index(name="status", columns={"status"}), @ORM\Index(name="iso2Code", columns={"iso2Code"})})
 * @ORM\Entity
 */
class Territories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="territoryId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $territoryid;

    /**
     * @var string
     *
     * @ORM\Column(name="iso2Code", type="string", length=2, nullable=false)
     */
    private $iso2code;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @param int $territoryid
     */
    public function setTerritoryid($territoryid)
    {
        $this->territoryid = $territoryid;
    }



    /**
     * Get territoryid
     *
     * @return integer 
     */
    public function getTerritoryid()
    {
        return $this->territoryid;
    }

    /**
     * Set iso2code
     *
     * @param string $iso2code
     * @return Territories
     */
    public function setIso2code($iso2code)
    {
        $this->iso2code = $iso2code;

        return $this;
    }

    /**
     * Get iso2code
     *
     * @return string 
     */
    public function getIso2code()
    {
        return $this->iso2code;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Territories
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Territories
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Territories
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Territories
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
