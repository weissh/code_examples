<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputFieldRModules
 *
 * @ORM\Table(name="INPUT_Field_R_Modules", uniqueConstraints={@ORM\UniqueConstraint(name="Index 2", columns={"inputFieldId"})}, indexes={@ORM\Index(name="Index 3", columns={"inputModuleId"})})
 * @ORM\Entity
 */
class InputFieldRModules
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputFieldRModuleId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputfieldrmoduleid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \PrismProductsManager\Entity\InputFields
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputFields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputFieldId", referencedColumnName="inputFieldId")
     * })
     */
    private $inputfieldid;

    /**
     * @var \PrismProductsManager\Entity\InputModules
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputModules")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputModuleId", referencedColumnName="inputModuleId")
     * })
     */
    private $inputmoduleid;



    /**
     * Get inputfieldrmoduleid
     *
     * @return integer 
     */
    public function getInputfieldrmoduleid()
    {
        return $this->inputfieldrmoduleid;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputFieldRModules
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputFieldRModules
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set inputfieldid
     *
     * @param \PrismProductsManager\Entity\InputFields $inputfieldid
     * @return InputFieldRModules
     */
    public function setInputfieldid(\PrismProductsManager\Entity\InputFields $inputfieldid = null)
    {
        $this->inputfieldid = $inputfieldid;

        return $this;
    }

    /**
     * Get inputfieldid
     *
     * @return \PrismProductsManager\Entity\InputFields 
     */
    public function getInputfieldid()
    {
        return $this->inputfieldid;
    }

    /**
     * Set inputmoduleid
     *
     * @param \PrismProductsManager\Entity\InputModules $inputmoduleid
     * @return InputFieldRModules
     */
    public function setInputmoduleid(\PrismProductsManager\Entity\InputModules $inputmoduleid = null)
    {
        $this->inputmoduleid = $inputmoduleid;

        return $this;
    }

    /**
     * Get inputmoduleid
     *
     * @return \PrismProductsManager\Entity\InputModules 
     */
    public function getInputmoduleid()
    {
        return $this->inputmoduleid;
    }
}
