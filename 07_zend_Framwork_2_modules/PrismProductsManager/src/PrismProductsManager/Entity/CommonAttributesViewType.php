<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributesViewType
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES_View_Type")
 * @ORM\Entity
 */
class CommonAttributesViewType
{
    const VIEW_TYPE_CHECKBOX = 'CHECKBOX';
    const VIEW_TYPE_SELECT = 'SELECT';
    const VIEW_TYPE_TEXT = 'TEXT';
    const VIEW_TYPE_MULTISELECT = 'MULTISELECT';
    const VIEW_TYPE_DATEPICKER = 'DATEPICKER';
    const VIEW_TYPE_TEXTAREA = 'TEXTAREA';
    const VIEW_TYPE_DATEPICKER_DATE_ONLY = 'DATEPICKER - DATE ONLY';

    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributeViewTypeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commonattributeviewtypeid;

    /**
     * @var string
     *
     * @ORM\Column(name="commonAttributeViewType", type="string", length=35, nullable=false)
     */
    private $commonattributeviewtype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;



    /**
     * Get commonattributeviewtypeid
     *
     * @return integer 
     */
    public function getCommonattributeviewtypeid()
    {
        return $this->commonattributeviewtypeid;
    }

    /**
     * Set commonattributeviewtype
     *
     * @param string $commonattributeviewtype
     * @return CommonAttributesViewType
     */
    public function setCommonattributeviewtype($commonattributeviewtype)
    {
        $this->commonattributeviewtype = $commonattributeviewtype;

        return $this;
    }

    /**
     * Get commonattributeviewtype
     *
     * @return string 
     */
    public function getCommonattributeviewtype()
    {
        return $this->commonattributeviewtype;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return CommonAttributesViewType
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return CommonAttributesViewType
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }
}
