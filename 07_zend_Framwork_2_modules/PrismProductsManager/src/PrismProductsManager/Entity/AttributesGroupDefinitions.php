<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AttributesGroupDefinitions
 *
 * @ORM\Table(name="ATTRIBUTES_Group_Definitions", indexes={@ORM\Index(name="Index 2", columns={"languageId"}), @ORM\Index(name="Index 3", columns={"name"}), @ORM\Index(name="attributeGroupId", columns={"attributeGroupId"})})
 * @ORM\Entity
 */
class AttributesGroupDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="attributeGroupDefinitionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $attributegroupdefinitionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="publicName", type="string", length=64, nullable=false)
     */
    private $publicname;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\AttributesGroups
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\AttributesGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="attributeGroupId", referencedColumnName="attributeGroupId")
     * })
     */
    private $attributegroupid;



    /**
     * Get attributegroupdefinitionid
     *
     * @return integer 
     */
    public function getAttributegroupdefinitionid()
    {
        return $this->attributegroupdefinitionid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return AttributesGroupDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AttributesGroupDefinitions
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set publicname
     *
     * @param string $publicname
     * @return AttributesGroupDefinitions
     */
    public function setPublicname($publicname)
    {
        $this->publicname = $publicname;

        return $this;
    }

    /**
     * Get publicname
     *
     * @return string 
     */
    public function getPublicname()
    {
        return $this->publicname;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return AttributesGroupDefinitions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return AttributesGroupDefinitions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return AttributesGroupDefinitions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set attributegroupid
     *
     * @param \PrismProductsManager\Entity\AttributesGroups $attributegroupid
     * @return AttributesGroupDefinitions
     */
    public function setAttributegroupid(\PrismProductsManager\Entity\AttributesGroups $attributegroupid = null)
    {
        $this->attributegroupid = $attributegroupid;

        return $this;
    }

    /**
     * Get attributegroupid
     *
     * @return \PrismProductsManager\Entity\AttributesGroups 
     */
    public function getAttributegroupid()
    {
        return $this->attributegroupid;
    }
}
