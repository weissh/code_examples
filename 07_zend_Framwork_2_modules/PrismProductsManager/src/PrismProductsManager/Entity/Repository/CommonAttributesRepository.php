<?php

namespace PrismProductsManager\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class CommonAttributesRepository
 * @package PrismProductsManager\Entity\Repository
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class CommonAttributesRepository extends EntityRepository
{
    /**
     * @param int $limit
     * @param int $offset
     * @param int $currentLanguage
     * @param string $search
     * @return Paginator
     */
    public function getCommonAttributes($limit = 10, $offset = 0, $currentLanguage = 1, $search = '', $attributeGroupId = null)
    {
        $query =  $this->createQueryBuilder('ca')
            ->select('ca, cad, cagr')
            ->leftJoin('ca.commonAttributeDefinitions', 'cad')
            ->leftJoin('ca.commonAttributeGuiRepresentation', 'cagr')
            ->leftJoin('ca.commonAttributesValues', 'cav')
            ->where('cad.languageid = ' . $currentLanguage);

        if (!is_null($attributeGroupId)) {
            $query->andWhere('cav.commonAttributeId = :commonAttributeId')
                ->setParameter('commonAttributeId', $attributeGroupId);
        }
        /** If search exists then add the parameter into the query */
        if (!empty($search)) {
            $query->andWhere('cad.description LIKE :search')
                ->setParameter('search', '%' . $search . '%');
        }
        
        if ($limit != 0) {
            $query->setMaxResults($limit)->setFirstResult($offset);
        }
        
        return new Paginator($query->getQuery());
    }

    public function getAllCommonAttributes($currentLanguage = 1)
    {
        $query =  $this->createQueryBuilder('ca')
            ->select('ca, cad, cagr')
            ->leftJoin('ca.commonAttributeDefinitions', 'cad')
            ->leftJoin('ca.commonAttributeGuiRepresentation', 'cagr')
            ->leftJoin('ca.commonAttributesValues', 'cav')
            ->where('cad.languageid = ' . $currentLanguage)
            ->andwhere("ca.EditableOnLevel = 'ALL' OR ca.EditableOnLevel = 'OPTION'");

        return new Paginator($query->getQuery());
    }

    public function getAllSizePropertyCommonAttributes($currentLanguage = 1)
    {
        $query =  $this->createQueryBuilder('ca')
            ->select('ca, cad, cagr')
            ->leftJoin('ca.commonAttributeDefinitions', 'cad')
            ->leftJoin('ca.commonAttributeGuiRepresentation', 'cagr')
            ->leftJoin('ca.commonAttributesValues', 'cav')
            ->where("ca.isSizeProperty = '1'")
            ->andwhere('cad.languageid = ' . $currentLanguage)
            ->andwhere("ca.EditableOnLevel = 'ALL' OR ca.EditableOnLevel = 'OPTION'");

        return new Paginator($query->getQuery());
    }


    public function getCascadeCommonAttributes($currentLanguage = 1)
    {
        $query =  $this->createQueryBuilder('ca')
            ->select('ca, cad, cagr')
            ->leftJoin('ca.commonAttributeDefinitions', 'cad')
            ->leftJoin('ca.commonAttributeGuiRepresentation', 'cagr')
            ->leftJoin('ca.commonAttributesValues', 'cav')
            ->where('cad.languageid = ' . $currentLanguage)
            ->andwhere("ca.EditableOnLevel = 'ALL' OR ca.EditableOnLevel = 'STYLE'");

        return new Paginator($query->getQuery());
    }
    
    public function getAttributeType($process_key)
    {
        $query =  $this->createQueryBuilder('ca')
            ->select('ca, cad, cagr, cvt')
            ->leftJoin('ca.commonAttributeDefinitions', 'cad')
            ->leftJoin('ca.commonAttributeGuiRepresentation', 'cagr')
            ->leftJoin('cagr.commonattributesviewtypeid', 'cvt')
            ->where('cad.description = :description')->setParameters(['description' => $process_key]);

        return $query->getQuery()->getArrayResult();
    }
}