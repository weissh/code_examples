<?php

namespace PrismProductsManager\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Zend\Db\Sql\Expression;

/**
 * Class CommonAttributesValuesRepository
 *
 * @package PrismProductsManager\Entity\Repository
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class CommonAttributesValuesRepository extends EntityRepository
{
    /**
     * @param $commonAttributeId
     * @param $currentLanguageId
     * @param $search
     *
     * @return array
     */
    public function getCommonAttributesValues($commonAttributeId = null, $currentLanguageId, $search = null)
    {
        $query = $this->createQueryBuilder('cav')
            ->select('cav, cavd, cavgr')
            ->leftJoin('cav.commonAttributeValuesDefinitions', 'cavd')
            ->leftJoin('cav.commonAttributeValuesGuiRepresentation', 'cavgr')
            ->where('cavd.languageid = ' . $currentLanguageId);


        /** If $commonAttributeId exists then add the parameter into the query */
        if (!empty($commonAttributeId)) {
            $query->andWhere('cav.commonattributeid = ' . $commonAttributeId);
        }

        if (!empty($search)) {
            $query->andWhere('cavd.value LIKE :search')
                ->setParameter('search', '%' . $search . '%');
        }
        $query->orderBy('cavgr.commonattributevalueorder', 'ASC');

        return $query->getQuery()->getResult();
    }

    /**
     * @param           $afterOrder
     * @param           $commonAttributeId
     * @param           $currentLanguageId
     * @param bool|true $afterOrderFlag
     *
     * @return array
     */
    public function getCommonAttributesOrderHigherThan($afterOrder, $commonAttributeId, $currentLanguageId, $afterOrderFlag = true)
    {
        $query = $this->createQueryBuilder('cav')
            ->select('cav, cavd, cavgr')
            ->leftJoin('cav.commonAttributeValuesDefinitions', 'cavd')
            ->leftJoin('cav.commonAttributeValuesGuiRepresentation', 'cavgr')
            ->where('cavd.languageid = ' . $currentLanguageId)
            ->andWhere('cav.commonattributeid = ' . $commonAttributeId)
            ->orderBy('cavgr.commonattributevalueorder', 'ASC');

        if ($afterOrderFlag) {
            $query->andWhere('cavgr.commonattributevalueorder > ' . $afterOrder);
        }

        return $query->getQuery()->getResult();
    }

    /**
     * @param string $commonAttributeId
     *
     * @return int
     */
    public function getMaxOrder($commonAttributeId)
    {
        $sql = '
            SELECT max(cavgr.commonAttributeValueOrder) AS max_order
            FROM COMMON_ATTRIBUTES_VALUES_GUI_Representation cavgr
              JOIN COMMON_ATTRIBUTES_VALUES cav ON cavgr.commonAttributeValueId = cav.commonAttributeValueId
            WHERE cav.commonAttributeId = :commonAttributeId;
        ';
        $statement = $this->getEntityManager()->getConnection()->prepare($sql);
        $statement->bindParam('commonAttributeId', $commonAttributeId);
        $statement->execute();

        return (int)$statement->fetch()['max_order'];
    }
}
