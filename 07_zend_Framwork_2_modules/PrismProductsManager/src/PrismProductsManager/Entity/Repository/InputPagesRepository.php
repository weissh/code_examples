<?php


namespace PrismProductsManager\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class InputPagesRepository extends EntityRepository
{
    public function findOneByPageurl($pageUrl)
    {
        return $this->createQueryBuilder('ip')
            ->select('ip, ifp, if')
            ->innerJoin('ip.inputFieldRPages', 'ifp')
            ->innerJoin('ifp.inputfieldid', 'if')
            ->where('ip.pageurl = :pageurl')
            ->setParameter('pageurl', $pageUrl)
            ->getQuery()->getSingleResult();
    }

    public function getAllPagesInputs($pageUrl = null)
    {
        if (!is_null($pageUrl)) {

            $return = $this->createQueryBuilder('ip')
                ->select('ip, ifp, ifs, if')
                ->leftJoin('ip.inputFieldRPages', 'ifp')
                ->leftJoin('ifp.inputfieldid', 'if')
                ->leftJoin('ifp.inputFieldsetsInputfieldsetid', 'ifs')
                ->where('ifp.enabled = 1')
                ->andWhere('ip.pageurl = :pageUrl')
                ->setParameter('pageUrl', $pageUrl)
                ->addOrderBy('ifs.fieldSetOrder', 'ASC')
                ->addOrderBy('ifp.order', 'ASC')
                ->getQuery()->getResult();
        } else {
            $return = $this->createQueryBuilder('ip')
                ->select('ip, ifp, ifs, if')
                ->leftJoin('ip.inputFieldRPages', 'ifp')
                ->leftJoin('ifp.inputfieldid', 'if')
                ->leftJoin('ifp.inputFieldsetsInputfieldsetid', 'ifs')
                ->where('ifp.enabled = 1')
                ->addOrderBy('ifs.fieldSetOrder', 'ASC')
                ->addOrderBy('ifp.order', 'ASC')
                ->getQuery()->getResult();
        }

        return $return;
    }
}