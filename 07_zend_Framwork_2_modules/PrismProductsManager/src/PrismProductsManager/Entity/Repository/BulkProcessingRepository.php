<?php

namespace PrismProductsManager\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class BulkProcessingRepository extends EntityRepository
{
    /**
     * @param $keyword
     * @param $process_type
     * @param $status
     * @param int $page
     * @param bool $selectCount
     * @return array | int
     */
    public function getBulkProcessingByProcessType(
        $keyword,
        $process_type,
        $status,
        $page = 1,
        $selectCount = true
    ) {

        $offset = ($page == 0) ? 0 : ($page - 1) * 100;

        $query =  $this->createQueryBuilder('bp');

        if ($selectCount === true) {
            $query->select('COUNT(bp)');
        } else {
            $query->select('bp')->orderBy('bp.bulkProcessingId', 'DESC');
        }

        $keyword = trim($keyword);
        if ($keyword != '') {
            
            $query->andWhere(
                'bp.processedResponse LIKE :keyword 
                    OR 
                bp.styleoroption LIKE :keyword
                    OR
                bp.processValue LIKE :keyword
                    OR 
                bp.processKey LIKE :keyword'
            )->setParameter('keyword', '%' . $keyword . '%');
            
        }
        
        if ($process_type) {
            $query->andWhere('bp.processType = :process_type')
                  ->setParameter('process_type', $process_type);
        }
        
        if ($status === 'PROCESSED') {
            $query->andWhere('bp.processed = 1');
            $query->andWhere("bp.processedResponse = 'SUCCESS'");             
        }
        if ($status === 'IN_PROCESS') {
            $query->andWhere('bp.processing = 1');
            $query->andWhere('bp.processed = 0');
            $currentDate = date('Y-m-d H:i:s');
            $query->andWhere("bp.schedule > '$currentDate'");             
        }    
        if ($status === 'PROCESSED_WITH_ERROR') {
            $currentDate = date('Y-m-d H:i:s');
            $query->andWhere("bp.schedule <= '$currentDate'");
            $query->andWhere('bp.processedResponse IS NOT NULL');        
            $query->andWhere("bp.processedResponse != 'SUCCESS'");
        }
        if ($status === 'NOT_PROCESSED') {
            $query->andWhere('bp.processing = 0');
            $query->andWhere('bp.processed = 0');
        }

        if ($selectCount === false) {
            $query->setMaxResults('100')->setFirstResult($offset);
            $result = $query->getQuery()->getResult();
            return $result;
        } else {
            $result = $query->getQuery()->getSingleResult();
            if (!empty($result)) {
                $key = key($result);
                if (isset($result[$key])) {
                    return $result[$key];
                }
            }
            return 0;
        }

    }
    
    /**
     * @return array
     */
    public function getErrorReport()
    {
        $currentDate = date('Y-m-d H:i:s');
        $last24hours = date_sub(new \DateTime($currentDate), date_interval_create_from_date_string('1 days'));
        $last24hours = $last24hours->format('Y-m-d H:i:s'); // needs re-call for php5.6
        
        $query =  $this->createQueryBuilder('bp')
                       ->select('bp')
                       ->orderBy('bp.bulkProcessingId', 'DESC');
        
        $query->andWhere('bp.processed = 1');
        $query->andWhere("bp.processedResponse != 'SUCCESS'");
        $query->andWhere("bp.schedule >= '$last24hours'");

        return $query->getQuery()->getArrayResult();
    }
}