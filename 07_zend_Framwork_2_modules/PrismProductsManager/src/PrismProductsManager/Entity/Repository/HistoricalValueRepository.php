<?php

namespace PrismProductsManager\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class HistoricalValueRepository extends EntityRepository
{
    public function retrieveResults($offset, $limit)
    {
        $query = $this->createQueryBuilder('h')
            ->select('h')
            ->leftJoin('cav.commonAttributeValuesDefinitions', 'cavd')
            ->leftJoin('cav.commonAttributeValuesGuiRepresentation', 'cavgr')
            ->where('cavd.languageid = ' . $currentLanguageId);

        $query->orderBy('cavgr.commonattributevalueorder', 'ASC');

        return $query->getQuery()->getResult();
    }
}