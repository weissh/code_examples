<?php

namespace PrismProductsManager\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class CommonAttributesRepository
 * @package PrismProductsManager\Entity\Repository
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class SchedulesRepository extends EntityRepository
{
    /**
     * @param $uniqueIdentifier
     * @return bool
     */
    public function lockScheduleEventsForProcessing($uniqueIdentifier)
    {
        try {
            return $this->createQueryBuilder('s')
                ->update('PrismProductsManager\Entity\Schedules', 's')
                ->set('s.processing', '?1' )
                ->setParameter(1, $uniqueIdentifier)
                ->where('s.scheduleStartDate <= CURRENT_TIMESTAMP()')
                ->andWhere('s.processing IS NULL')
                ->getQuery()->execute();
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $productOrVariantId
     * @param $productIdFrom
     * @param $priceTable
     * @param $priceColumn
     * @return array|bool
     */
    public function getPriceSchedulesForProduct($productOrVariantId, $productIdFrom, $priceTable, $priceColumn)
    {
        try {
            return $this->createQueryBuilder('s')
                ->select('s, sp')
                ->innerJoin('s.schedulePrices', 'sp')
                ->where('s.productOrVariantId = :productOrVariantId')
                ->andWhere('s.productIdFrom = :productIdFrom')
                ->andWhere('sp.priceTable = :priceTable')
                ->andWhere('sp.priceColumn = :priceColumn')
                ->setParameter('productOrVariantId', $productOrVariantId)
                ->setParameter('productIdFrom', $productIdFrom)
                ->setParameter('priceTable', $priceTable)
                ->setParameter('priceColumn', $priceColumn)
                ->getQuery()->execute();
        } catch (\Exception $ex) {
            return false;
        }
    }
    public function getPriceSchedulesForWasPriceUpToDate($date, $productOrVariantId, $productIdFrom, $territoryId, $currencyId)
    {
        return $this->createQueryBuilder('s')
            ->select('s, sp')
            ->innerJoin('s.schedulePrices', 'sp')
            ->where('s.productOrVariantId = :productOrVariantId')
            ->andWhere('s.productIdFrom = :productIdFrom')
            ->andWhere('sp.priceTable = :priceTable')
            ->andWhere('sp.priceColumn = :priceColumn')
            ->andWhere('s.scheduleStartDate <= :date')
            ->andWhere('sp.currencyId = :currencyId')
            ->andWhere('sp.territoryId = :territoryId')
            ->setParameter('productOrVariantId', $productOrVariantId)
            ->setParameter('productIdFrom', $productIdFrom)
            ->setParameter('priceTable', 'PRODUCTS_Prices')
            ->setParameter('priceColumn', 'wasPrice')
            ->setParameter('currencyId', $currencyId)
            ->setParameter('territoryId', $territoryId)
            ->setParameter('date', $date)
            ->orderBy('s.scheduleStartDate', 'DESC')
            ->setMaxResults(1)
            ->getQuery()->execute();
    }
    /**
     * @param $uniqueIdentifier
     * @return bool|mixed
     */
    public function unlockRecords($uniqueIdentifier)
    {
        try {
            return $this->createQueryBuilder('s')
                ->update('PrismProductsManager\Entity\Schedules', 's')
                ->set('s.processing', '?1' )
                ->where('s.processing = :uniqueIdentifier')
                ->setParameter(1, NULL)
                ->setParameter('uniqueIdentifier', $uniqueIdentifier)
                ->getQuery()->execute();
        } catch (\Exception $ex) {
            return false;
        }
    }
}