<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QueueManagerBulkRules
 *
 * @ORM\Table(name="QUEUE_Manager_Bulk_Rules", indexes={@ORM\Index(name="queueEntryType", columns={"spectrumSync"}), @ORM\Index(name="processed", columns={"scriptAllowedFrom", "scriptAllowedTo"}), @ORM\Index(name="queueProcessId", columns={"batchNumber"})})
 * @ORM\Entity
 */
class QueueManagerBulkRules
{
    /**
     * @var integer
     *
     * @ORM\Column(name="queueBulkRuleId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $queuebulkruleid;

    /**
     * @var string
     *
     * @ORM\Column(name="scriptAllowedFrom", type="string", length=50, nullable=false)
     */
    private $scriptallowedfrom;

    /**
     * @var string
     *
     * @ORM\Column(name="scriptAllowedTo", type="string", length=50, nullable=false)
     */
    private $scriptallowedto;

    /**
     * @var integer
     *
     * @ORM\Column(name="batchNumber", type="integer", nullable=false)
     */
    private $batchnumber;

    /**
     * @var boolean
     *
     * @ORM\Column(name="spectrumSync", type="boolean", nullable=false)
     */
    private $spectrumsync;

    /**
     * @var boolean
     *
     * @ORM\Column(name="elasticsearchSync", type="boolean", nullable=false)
     */
    private $elasticsearchsync;

    /**
     * @var integer
     *
     * @ORM\Column(name="allowNewInsertLastModified", type="integer", nullable=false)
     */
    private $allownewinsertlastmodified;

    /**
     * @var integer
     *
     * @ORM\Column(name="lockedScriptEmailAlert", type="integer", nullable=false)
     */
    private $lockedscriptemailalert;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get queuebulkruleid
     *
     * @return integer 
     */
    public function getQueuebulkruleid()
    {
        return $this->queuebulkruleid;
    }

    /**
     * Set scriptallowedfrom
     *
     * @param string $scriptallowedfrom
     * @return QueueManagerBulkRules
     */
    public function setScriptallowedfrom($scriptallowedfrom)
    {
        $this->scriptallowedfrom = $scriptallowedfrom;

        return $this;
    }

    /**
     * Get scriptallowedfrom
     *
     * @return string 
     */
    public function getScriptallowedfrom()
    {
        return $this->scriptallowedfrom;
    }

    /**
     * Set scriptallowedto
     *
     * @param string $scriptallowedto
     * @return QueueManagerBulkRules
     */
    public function setScriptallowedto($scriptallowedto)
    {
        $this->scriptallowedto = $scriptallowedto;

        return $this;
    }

    /**
     * Get scriptallowedto
     *
     * @return string 
     */
    public function getScriptallowedto()
    {
        return $this->scriptallowedto;
    }

    /**
     * Set batchnumber
     *
     * @param integer $batchnumber
     * @return QueueManagerBulkRules
     */
    public function setBatchnumber($batchnumber)
    {
        $this->batchnumber = $batchnumber;

        return $this;
    }

    /**
     * Get batchnumber
     *
     * @return integer 
     */
    public function getBatchnumber()
    {
        return $this->batchnumber;
    }

    /**
     * Set spectrumsync
     *
     * @param boolean $spectrumsync
     * @return QueueManagerBulkRules
     */
    public function setSpectrumsync($spectrumsync)
    {
        $this->spectrumsync = $spectrumsync;

        return $this;
    }

    /**
     * Get spectrumsync
     *
     * @return boolean 
     */
    public function getSpectrumsync()
    {
        return $this->spectrumsync;
    }

    /**
     * Set elasticsearchsync
     *
     * @param boolean $elasticsearchsync
     * @return QueueManagerBulkRules
     */
    public function setElasticsearchsync($elasticsearchsync)
    {
        $this->elasticsearchsync = $elasticsearchsync;

        return $this;
    }

    /**
     * Get elasticsearchsync
     *
     * @return boolean 
     */
    public function getElasticsearchsync()
    {
        return $this->elasticsearchsync;
    }

    /**
     * Set allownewinsertlastmodified
     *
     * @param integer $allownewinsertlastmodified
     * @return QueueManagerBulkRules
     */
    public function setAllownewinsertlastmodified($allownewinsertlastmodified)
    {
        $this->allownewinsertlastmodified = $allownewinsertlastmodified;

        return $this;
    }

    /**
     * Get allownewinsertlastmodified
     *
     * @return integer 
     */
    public function getAllownewinsertlastmodified()
    {
        return $this->allownewinsertlastmodified;
    }

    /**
     * Set lockedscriptemailalert
     *
     * @param integer $lockedscriptemailalert
     * @return QueueManagerBulkRules
     */
    public function setLockedscriptemailalert($lockedscriptemailalert)
    {
        $this->lockedscriptemailalert = $lockedscriptemailalert;

        return $this;
    }

    /**
     * Get lockedscriptemailalert
     *
     * @return integer 
     */
    public function getLockedscriptemailalert()
    {
        return $this->lockedscriptemailalert;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return QueueManagerBulkRules
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
