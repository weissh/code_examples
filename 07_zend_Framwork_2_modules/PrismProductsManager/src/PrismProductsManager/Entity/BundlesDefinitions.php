<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BundlesDefinitions
 *
 * @ORM\Table(name="BUNDLES_Definitions", indexes={@ORM\Index(name="Index 2", columns={"bundleId"}), @ORM\Index(name="Index 3", columns={"languageId"})})
 * @ORM\Entity
 */
class BundlesDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bundleDefinitionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bundledefinitionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="shortBundleName", type="string", length=80, nullable=false)
     */
    private $shortbundlename;

    /**
     * @var string
     *
     * @ORM\Column(name="bundleName", type="string", length=255, nullable=false)
     */
    private $bundlename;

    /**
     * @var string
     *
     * @ORM\Column(name="urlName", type="string", length=250, nullable=false)
     */
    private $urlname;

    /**
     * @var string
     *
     * @ORM\Column(name="metaTitle", type="string", length=250, nullable=false)
     */
    private $metatitle;

    /**
     * @var string
     *
     * @ORM\Column(name="metaDescription", type="string", length=250, nullable=false)
     */
    private $metadescription;

    /**
     * @var string
     *
     * @ORM\Column(name="metaKeyword", type="string", length=250, nullable=false)
     */
    private $metakeyword;

    /**
     * @var string
     *
     * @ORM\Column(name="shortDescription", type="string", length=250, nullable=false)
     */
    private $shortdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="longDescription", type="text", nullable=false)
     */
    private $longdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="liveFromDate", type="datetime", nullable=true)
     */
    private $livefromdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="liveUntilDate", type="datetime", nullable=true)
     */
    private $liveuntildate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \PrismProductsManager\Entity\Bundles
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Bundles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bundleId", referencedColumnName="bundleId")
     * })
     */
    private $bundleid;



    /**
     * Get bundledefinitionid
     *
     * @return integer 
     */
    public function getBundledefinitionid()
    {
        return $this->bundledefinitionid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return BundlesDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set shortbundlename
     *
     * @param string $shortbundlename
     * @return BundlesDefinitions
     */
    public function setShortbundlename($shortbundlename)
    {
        $this->shortbundlename = $shortbundlename;

        return $this;
    }

    /**
     * Get shortbundlename
     *
     * @return string 
     */
    public function getShortbundlename()
    {
        return $this->shortbundlename;
    }

    /**
     * Set bundlename
     *
     * @param string $bundlename
     * @return BundlesDefinitions
     */
    public function setBundlename($bundlename)
    {
        $this->bundlename = $bundlename;

        return $this;
    }

    /**
     * Get bundlename
     *
     * @return string 
     */
    public function getBundlename()
    {
        return $this->bundlename;
    }

    /**
     * Set urlname
     *
     * @param string $urlname
     * @return BundlesDefinitions
     */
    public function setUrlname($urlname)
    {
        $this->urlname = $urlname;

        return $this;
    }

    /**
     * Get urlname
     *
     * @return string 
     */
    public function getUrlname()
    {
        return $this->urlname;
    }

    /**
     * Set metatitle
     *
     * @param string $metatitle
     * @return BundlesDefinitions
     */
    public function setMetatitle($metatitle)
    {
        $this->metatitle = $metatitle;

        return $this;
    }

    /**
     * Get metatitle
     *
     * @return string 
     */
    public function getMetatitle()
    {
        return $this->metatitle;
    }

    /**
     * Set metadescription
     *
     * @param string $metadescription
     * @return BundlesDefinitions
     */
    public function setMetadescription($metadescription)
    {
        $this->metadescription = $metadescription;

        return $this;
    }

    /**
     * Get metadescription
     *
     * @return string 
     */
    public function getMetadescription()
    {
        return $this->metadescription;
    }

    /**
     * Set metakeyword
     *
     * @param string $metakeyword
     * @return BundlesDefinitions
     */
    public function setMetakeyword($metakeyword)
    {
        $this->metakeyword = $metakeyword;

        return $this;
    }

    /**
     * Get metakeyword
     *
     * @return string 
     */
    public function getMetakeyword()
    {
        return $this->metakeyword;
    }

    /**
     * Set shortdescription
     *
     * @param string $shortdescription
     * @return BundlesDefinitions
     */
    public function setShortdescription($shortdescription)
    {
        $this->shortdescription = $shortdescription;

        return $this;
    }

    /**
     * Get shortdescription
     *
     * @return string 
     */
    public function getShortdescription()
    {
        return $this->shortdescription;
    }

    /**
     * Set longdescription
     *
     * @param string $longdescription
     * @return BundlesDefinitions
     */
    public function setLongdescription($longdescription)
    {
        $this->longdescription = $longdescription;

        return $this;
    }

    /**
     * Get longdescription
     *
     * @return string 
     */
    public function getLongdescription()
    {
        return $this->longdescription;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return BundlesDefinitions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set livefromdate
     *
     * @param \DateTime $livefromdate
     * @return BundlesDefinitions
     */
    public function setLivefromdate($livefromdate)
    {
        $this->livefromdate = $livefromdate;

        return $this;
    }

    /**
     * Get livefromdate
     *
     * @return \DateTime 
     */
    public function getLivefromdate()
    {
        return $this->livefromdate;
    }

    /**
     * Set liveuntildate
     *
     * @param \DateTime $liveuntildate
     * @return BundlesDefinitions
     */
    public function setLiveuntildate($liveuntildate)
    {
        $this->liveuntildate = $liveuntildate;

        return $this;
    }

    /**
     * Get liveuntildate
     *
     * @return \DateTime 
     */
    public function getLiveuntildate()
    {
        return $this->liveuntildate;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return BundlesDefinitions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return BundlesDefinitions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set bundleid
     *
     * @param \PrismProductsManager\Entity\Bundles $bundleid
     * @return BundlesDefinitions
     */
    public function setBundleid(\PrismProductsManager\Entity\Bundles $bundleid = null)
    {
        $this->bundleid = $bundleid;

        return $this;
    }

    /**
     * Get bundleid
     *
     * @return \PrismProductsManager\Entity\Bundles 
     */
    public function getBundleid()
    {
        return $this->bundleid;
    }
}
