<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tabs
 *
 * @ORM\Table(name="TABS", indexes={@ORM\Index(name="status", columns={"status"}), @ORM\Index(name="FK_TABS_TABS_Groups", columns={"tabGroupId"})})
 * @ORM\Entity
 */
class Tabs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tabId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tabid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var integer
     *
     * @ORM\Column(name="oldTabId", type="integer", nullable=false)
     */
    private $oldtabid;

    /**
     * @var \PrismProductsManager\Entity\TabsGroups
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\TabsGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tabGroupId", referencedColumnName="tabGroupId")
     * })
     */
    private $tabgroupid;



    /**
     * Get tabid
     *
     * @return integer 
     */
    public function getTabid()
    {
        return $this->tabid;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Tabs
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Tabs
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Tabs
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set oldtabid
     *
     * @param integer $oldtabid
     * @return Tabs
     */
    public function setOldtabid($oldtabid)
    {
        $this->oldtabid = $oldtabid;

        return $this;
    }

    /**
     * Get oldtabid
     *
     * @return integer 
     */
    public function getOldtabid()
    {
        return $this->oldtabid;
    }

    /**
     * Set tabgroupid
     *
     * @param \PrismProductsManager\Entity\TabsGroups $tabgroupid
     * @return Tabs
     */
    public function setTabgroupid(\PrismProductsManager\Entity\TabsGroups $tabgroupid = null)
    {
        $this->tabgroupid = $tabgroupid;

        return $this;
    }

    /**
     * Get tabgroupid
     *
     * @return \PrismProductsManager\Entity\TabsGroups 
     */
    public function getTabgroupid()
    {
        return $this->tabgroupid;
    }
}
