<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImageMigration
 *
 * @ORM\Table(name="IMAGE_MIGRATION", uniqueConstraints={@ORM\UniqueConstraint(name="Index 1", columns={"oldVariantId"}), @ORM\UniqueConstraint(name="Index 2", columns={"variantId"})})
 * @ORM\Entity
 */
class ImageMigration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="convertionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $convertionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="oldVariantId", type="integer", nullable=true)
     */
    private $oldvariantid;

    /**
     * @var string
     *
     * @ORM\Column(name="oldStyle", type="string", length=50, nullable=true)
     */
    private $oldstyle;

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=50, nullable=true)
     */
    private $sku;

    /**
     * @var string
     *
     * @ORM\Column(name="consolid", type="string", length=50, nullable=true)
     */
    private $consolid;

    /**
     * @var integer
     *
     * @ORM\Column(name="productId", type="integer", nullable=true)
     */
    private $productid;

    /**
     * @var integer
     *
     * @ORM\Column(name="variantId", type="integer", nullable=true)
     */
    private $variantid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;



    /**
     * Get convertionid
     *
     * @return integer 
     */
    public function getConvertionid()
    {
        return $this->convertionid;
    }

    /**
     * Set oldvariantid
     *
     * @param integer $oldvariantid
     * @return ImageMigration
     */
    public function setOldvariantid($oldvariantid)
    {
        $this->oldvariantid = $oldvariantid;

        return $this;
    }

    /**
     * Get oldvariantid
     *
     * @return integer 
     */
    public function getOldvariantid()
    {
        return $this->oldvariantid;
    }

    /**
     * Set oldstyle
     *
     * @param string $oldstyle
     * @return ImageMigration
     */
    public function setOldstyle($oldstyle)
    {
        $this->oldstyle = $oldstyle;

        return $this;
    }

    /**
     * Get oldstyle
     *
     * @return string 
     */
    public function getOldstyle()
    {
        return $this->oldstyle;
    }

    /**
     * Set sku
     *
     * @param string $sku
     * @return ImageMigration
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string 
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set consolid
     *
     * @param string $consolid
     * @return ImageMigration
     */
    public function setConsolid($consolid)
    {
        $this->consolid = $consolid;

        return $this;
    }

    /**
     * Get consolid
     *
     * @return string 
     */
    public function getConsolid()
    {
        return $this->consolid;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return ImageMigration
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set variantid
     *
     * @param integer $variantid
     * @return ImageMigration
     */
    public function setVariantid($variantid)
    {
        $this->variantid = $variantid;

        return $this;
    }

    /**
     * Get variantid
     *
     * @return integer 
     */
    public function getVariantid()
    {
        return $this->variantid;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return ImageMigration
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ImageMigration
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }
}
