<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputFieldsets
 *
 * @ORM\Table(name="INPUT_Fieldsets")
 * @ORM\Entity
 */
class InputFieldsets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputFieldsetId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputfieldsetid;

    /**
     * @var string
     *
     * @ORM\Column(name="inputFieldSetName", type="string", length=70, nullable=false)
     */
    private $inputfieldsetname;

    /**
     * @var string
     *
     * @ORM\Column(name="fieldSetOrder", type="integer", length=70, nullable=true)
     */
    private $fieldSetOrder;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     * Get inputfieldsetid
     *
     * @return integer 
     */
    public function getInputfieldsetid()
    {
        return $this->inputfieldsetid;
    }

    /**
     * Set inputfieldsetname
     *
     * @param string $inputfieldsetname
     * @return InputFieldsets
     */
    public function setInputfieldsetname($inputfieldsetname)
    {
        $this->inputfieldsetname = $inputfieldsetname;

        return $this;
    }

    /**
     * Get inputfieldsetname
     *
     * @return string 
     */
    public function getInputfieldsetname()
    {
        return $this->inputfieldsetname;
    }

    /**
     * Set $fieldSetOrder
     *
     * @param string $fieldSetOrder
     * @return InputFieldsets
     */
    public function setFieldSetOrder($fielsetorder)
    {
        $this->fieldSetOrder = $fielsetorder;

        return $this;
    }

    /**
     * Get fieldSetOrder
     *
     * @return string
     */
    public function getFieldSetOrder()
    {
        return $this->fieldSetOrder;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputFieldsets
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputFieldsets
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }
}
