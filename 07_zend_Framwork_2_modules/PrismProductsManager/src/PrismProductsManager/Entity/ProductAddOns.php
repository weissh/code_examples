<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductAddOns
 *
 * @ORM\Table(name="PRODUCT_Add_Ons")
 * @ORM\Entity
 */
class ProductAddOns
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="productId", type="integer", nullable=false)
     */
    private $productid;

    /**
     * @var integer
     *
     * @ORM\Column(name="productVariantId", type="integer", nullable=true)
     */
    private $productvariantid;

    /**
     * @var string
     *
     * @ORM\Column(name="productOrVariant", type="string", length=45, nullable=false)
     */
    private $productorvariant;

    /**
     * @var integer
     *
     * @ORM\Column(name="productAddOnId", type="integer", nullable=false)
     */
    private $productaddonid;

    /**
     * @var string
     *
     * @ORM\Column(name="addOnProductOrVariant", type="string", length=45, nullable=false)
     */
    private $addonproductorvariant;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return ProductAddOns
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set productvariantid
     *
     * @param integer $productvariantid
     * @return ProductAddOns
     */
    public function setProductvariantid($productvariantid)
    {
        $this->productvariantid = $productvariantid;

        return $this;
    }

    /**
     * Get productvariantid
     *
     * @return integer 
     */
    public function getProductvariantid()
    {
        return $this->productvariantid;
    }

    /**
     * Set productorvariant
     *
     * @param string $productorvariant
     * @return ProductAddOns
     */
    public function setProductorvariant($productorvariant)
    {
        $this->productorvariant = $productorvariant;

        return $this;
    }

    /**
     * Get productorvariant
     *
     * @return string 
     */
    public function getProductorvariant()
    {
        return $this->productorvariant;
    }

    /**
     * Set productaddonid
     *
     * @param integer $productaddonid
     * @return ProductAddOns
     */
    public function setProductaddonid($productaddonid)
    {
        $this->productaddonid = $productaddonid;

        return $this;
    }

    /**
     * Get productaddonid
     *
     * @return integer 
     */
    public function getProductaddonid()
    {
        return $this->productaddonid;
    }

    /**
     * Set addonproductorvariant
     *
     * @param string $addonproductorvariant
     * @return ProductAddOns
     */
    public function setAddonproductorvariant($addonproductorvariant)
    {
        $this->addonproductorvariant = $addonproductorvariant;

        return $this;
    }

    /**
     * Get addonproductorvariant
     *
     * @return string 
     */
    public function getAddonproductorvariant()
    {
        return $this->addonproductorvariant;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductAddOns
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductAddOns
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
