<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ElasticsearchIndexqueue
 *
 * @ORM\Table(name="ELASTICSEARCH_IndexQueue", indexes={@ORM\Index(name="productId", columns={"indexType"})})
 * @ORM\Entity
 */
class ElasticsearchIndexqueue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="indexType", type="integer", nullable=false)
     */
    private $indextype;

    /**
     * @var integer
     *
     * @ORM\Column(name="elasticSearchId", type="integer", nullable=false)
     */
    private $elasticsearchid;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", nullable=false)
     */
    private $reason;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set indextype
     *
     * @param integer $indextype
     * @return ElasticsearchIndexqueue
     */
    public function setIndextype($indextype)
    {
        $this->indextype = $indextype;

        return $this;
    }

    /**
     * Get indextype
     *
     * @return integer 
     */
    public function getIndextype()
    {
        return $this->indextype;
    }

    /**
     * Set elasticsearchid
     *
     * @param integer $elasticsearchid
     * @return ElasticsearchIndexqueue
     */
    public function setElasticsearchid($elasticsearchid)
    {
        $this->elasticsearchid = $elasticsearchid;

        return $this;
    }

    /**
     * Get elasticsearchid
     *
     * @return integer 
     */
    public function getElasticsearchid()
    {
        return $this->elasticsearchid;
    }

    /**
     * Set reason
     *
     * @param string $reason
     * @return ElasticsearchIndexqueue
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string 
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return ElasticsearchIndexqueue
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }
}
