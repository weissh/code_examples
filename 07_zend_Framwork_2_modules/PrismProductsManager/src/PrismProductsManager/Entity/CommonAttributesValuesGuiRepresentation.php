<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributesValuesGuiRepresentation
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES_VALUES_GUI_Representation", indexes={@ORM\Index(name="Index 3", columns={"shopDisplay", "pmsDisplay"}), @ORM\Index(name="COMMON_ATTRIBUTES_VALUES_GUI_Representation_ibfk_1", columns={"commonAttributeValueId"})})
 * @ORM\Entity
 */
class CommonAttributesValuesGuiRepresentation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributesValuesGuiRepresentationId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commonattributesvaluesguirepresentationid;

    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributeValueOrder", type="integer", nullable=false)
     */
    private $commonattributevalueorder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pmsDisplay", type="boolean", nullable=false)
     */
    private $pmsdisplay;

    /**
     * @var boolean
     *
     * @ORM\Column(name="shopDisplay", type="boolean", nullable=false)
     */
    private $shopdisplay;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDefault", type="boolean", nullable=false)
     */
    private $isDefault;

    /**
     * @var string
     *
     * @ORM\Column(name="page", type="string", length=120, nullable=false)
     */
    private $page;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     * @var integer
     *
     * @ORM\Column(name="modifiedBy", type="integer", nullable=true)
     */
    private $modifiedby;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributesValues
     *
     * @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesValues" ,inversedBy="commonAttributeValueId")
     * @ORM\JoinColumn(name="commonAttributeValueId", referencedColumnName="commonAttributeValueId")
     */
    private $commonAttributeValueId;

    /**
     * Get commonattributesvaluesguirepresentationid
     *
     * @return integer 
     */
    public function getCommonattributesvaluesguirepresentationid()
    {
        return $this->commonattributesvaluesguirepresentationid;
    }

    /**
     * Set commonattributevalueorder
     *
     * @param integer $commonattributevalueorder
     * @return CommonAttributesValuesGuiRepresentation
     */
    public function setCommonattributevalueorder($commonattributevalueorder)
    {
        $this->commonattributevalueorder = $commonattributevalueorder;

        return $this;
    }

    /**
     * Get commonattributevalueorder
     *
     * @return integer 
     */
    public function getCommonattributevalueorder()
    {
        return $this->commonattributevalueorder;
    }

    /**
     * Set pmsdisplay
     *
     * @param boolean $pmsdisplay
     * @return CommonAttributesValuesGuiRepresentation
     */
    public function setPmsdisplay($pmsdisplay)
    {
        $this->pmsdisplay = $pmsdisplay;

        return $this;
    }

    /**
     * Get pmsdisplay
     *
     * @return boolean 
     */
    public function getPmsdisplay()
    {
        return $this->pmsdisplay;
    }

    /**
     * Set shopdisplay
     *
     * @param boolean $shopdisplay
     * @return CommonAttributesValuesGuiRepresentation
     */
    public function setShopdisplay($shopdisplay)
    {
        $this->shopdisplay = $shopdisplay;

        return $this;
    }

    /**
     * Get shopdisplay
     *
     * @return boolean 
     */
    public function getShopdisplay()
    {
        return $this->shopdisplay;
    }

    /**
     * @return boolean
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * @param boolean $isDefault
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;
    }

    /**
     * Set page
     *
     * @param string $page
     * @return CommonAttributesValuesGuiRepresentation
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return string 
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return CommonAttributesValuesGuiRepresentation
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return CommonAttributesValuesGuiRepresentation
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set modifiedby
     *
     * @param integer $modifiedby
     * @return CommonAttributesValuesGuiRepresentation
     */
    public function setModifiedby($modifiedby)
    {
        $this->modifiedby = $modifiedby;

        return $this;
    }

    /**
     * Get modifiedby
     *
     * @return integer 
     */
    public function getModifiedby()
    {
        return $this->modifiedby;
    }

    /**
     * Set commonAttributeValueId
     *
     * @param \PrismProductsManager\Entity\CommonAttributesValues $commonAttributeValueId
     * @return CommonAttributesValuesGuiRepresentation
     */
    public function setcommonAttributeValueId(\PrismProductsManager\Entity\CommonAttributesValues $commonAttributeValueId = null)
    {
        $this->commonAttributeValueId = $commonAttributeValueId;

        return $this;
    }

    /**
     * Get commonAttributeValueId
     *
     * @return \PrismProductsManager\Entity\CommonAttributesValues 
     */
    public function getcommonAttributeValueId()
    {
        return $this->commonAttributeValueId;
    }
}
