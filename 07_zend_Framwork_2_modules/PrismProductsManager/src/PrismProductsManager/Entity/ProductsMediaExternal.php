<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsMediaExternal
 *
 * @ORM\Table(name="PRODUCTS_Media_External", indexes={@ORM\Index(name="FK_PRODUCTS_Media_External_PRODUCTS_Media", columns={"productsMediaId"})})
 * @ORM\Entity
 */
class ProductsMediaExternal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="externalMediaId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $externalmediaid;

    /**
     * @var string
     *
     * @ORM\Column(name="fileName", type="string", length=50, nullable=false)
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=150, nullable=false)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\ProductsMedia
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\ProductsMedia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="productsMediaId", referencedColumnName="productsMediaId")
     * })
     */
    private $productsmediaid;



    /**
     * Get externalmediaid
     *
     * @return integer 
     */
    public function getExternalmediaid()
    {
        return $this->externalmediaid;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return ProductsMediaExternal
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return ProductsMediaExternal
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProductsMediaExternal
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsMediaExternal
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsMediaExternal
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set productsmediaid
     *
     * @param \PrismProductsManager\Entity\ProductsMedia $productsmediaid
     * @return ProductsMediaExternal
     */
    public function setProductsmediaid(\PrismProductsManager\Entity\ProductsMedia $productsmediaid = null)
    {
        $this->productsmediaid = $productsmediaid;

        return $this;
    }

    /**
     * Get productsmediaid
     *
     * @return \PrismProductsManager\Entity\ProductsMedia 
     */
    public function getProductsmediaid()
    {
        return $this->productsmediaid;
    }
}
