<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Taxes
 *
 * @ORM\Table(name="TAXES", indexes={@ORM\Index(name="status", columns={"status"})})
 * @ORM\Entity
 */
class Taxes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="taxId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $taxid;

    /**
     * @var integer
     *
     * @ORM\Column(name="taxType", type="integer", nullable=false)
     */
    private $taxtype;

    /**
     * @var string
     *
     * @ORM\Column(name="taxTerritory", type="string", length=4, nullable=false)
     */
    private $taxterritory;

    /**
     * @var float
     *
     * @ORM\Column(name="taxValue", type="float", precision=10, scale=2, nullable=false)
     */
    private $taxvalue;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get taxid
     *
     * @return integer 
     */
    public function getTaxid()
    {
        return $this->taxid;
    }

    /**
     * Set taxtype
     *
     * @param integer $taxtype
     * @return Taxes
     */
    public function setTaxtype($taxtype)
    {
        $this->taxtype = $taxtype;

        return $this;
    }

    /**
     * Get taxtype
     *
     * @return integer 
     */
    public function getTaxtype()
    {
        return $this->taxtype;
    }

    /**
     * Set taxterritory
     *
     * @param string $taxterritory
     * @return Taxes
     */
    public function setTaxterritory($taxterritory)
    {
        $this->taxterritory = $taxterritory;

        return $this;
    }

    /**
     * Get taxterritory
     *
     * @return string 
     */
    public function getTaxterritory()
    {
        return $this->taxterritory;
    }

    /**
     * Set taxvalue
     *
     * @param float $taxvalue
     * @return Taxes
     */
    public function setTaxvalue($taxvalue)
    {
        $this->taxvalue = $taxvalue;

        return $this;
    }

    /**
     * Get taxvalue
     *
     * @return float 
     */
    public function getTaxvalue()
    {
        return $this->taxvalue;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Taxes
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Taxes
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Taxes
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
