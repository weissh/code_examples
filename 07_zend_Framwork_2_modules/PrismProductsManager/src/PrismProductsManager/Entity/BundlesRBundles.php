<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BundlesRBundles
 *
 * @ORM\Table(name="BUNDLES_R_BUNDLES", indexes={@ORM\Index(name="Index 2", columns={"bundleId"}), @ORM\Index(name="Index 3", columns={"relatedBundleId"})})
 * @ORM\Entity
 */
class BundlesRBundles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bundleRBundleId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bundlerbundleid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\Bundles
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Bundles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bundleId", referencedColumnName="bundleId")
     * })
     */
    private $bundleid;

    /**
     * @var \PrismProductsManager\Entity\Bundles
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Bundles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="relatedBundleId", referencedColumnName="bundleId")
     * })
     */
    private $relatedbundleid;



    /**
     * Get bundlerbundleid
     *
     * @return integer 
     */
    public function getBundlerbundleid()
    {
        return $this->bundlerbundleid;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return BundlesRBundles
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return BundlesRBundles
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return BundlesRBundles
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set bundleid
     *
     * @param \PrismProductsManager\Entity\Bundles $bundleid
     * @return BundlesRBundles
     */
    public function setBundleid(\PrismProductsManager\Entity\Bundles $bundleid = null)
    {
        $this->bundleid = $bundleid;

        return $this;
    }

    /**
     * Get bundleid
     *
     * @return \PrismProductsManager\Entity\Bundles 
     */
    public function getBundleid()
    {
        return $this->bundleid;
    }

    /**
     * Set relatedbundleid
     *
     * @param \PrismProductsManager\Entity\Bundles $relatedbundleid
     * @return BundlesRBundles
     */
    public function setRelatedbundleid(\PrismProductsManager\Entity\Bundles $relatedbundleid = null)
    {
        $this->relatedbundleid = $relatedbundleid;

        return $this;
    }

    /**
     * Get relatedbundleid
     *
     * @return \PrismProductsManager\Entity\Bundles 
     */
    public function getRelatedbundleid()
    {
        return $this->relatedbundleid;
    }
}
