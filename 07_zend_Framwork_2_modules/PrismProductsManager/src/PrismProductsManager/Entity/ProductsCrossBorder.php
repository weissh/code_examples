<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsCrossBorder
 *
 * @ORM\Table(name="PRODUCTS_Cross_Border")
 * @ORM\Entity
 */
class ProductsCrossBorder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="productIdFrom", type="string", nullable=false)
     */
    private $productidfrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="productId", type="integer", nullable=false)
     */
    private $productid;

    /**
     * @var string
     *
     * @ORM\Column(name="crossBorderString", type="string", length=255, nullable=false)
     */
    private $crossborderstring;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    private $enabled;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productidfrom
     *
     * @param string $productidfrom
     * @return ProductsCrossBorder
     */
    public function setProductidfrom($productidfrom)
    {
        $this->productidfrom = $productidfrom;

        return $this;
    }

    /**
     * Get productidfrom
     *
     * @return string 
     */
    public function getProductidfrom()
    {
        return $this->productidfrom;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return ProductsCrossBorder
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set crossborderstring
     *
     * @param string $crossborderstring
     * @return ProductsCrossBorder
     */
    public function setCrossborderstring($crossborderstring)
    {
        $this->crossborderstring = $crossborderstring;

        return $this;
    }

    /**
     * Get crossborderstring
     *
     * @return string 
     */
    public function getCrossborderstring()
    {
        return $this->crossborderstring;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return ProductsCrossBorder
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsCrossBorder
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }
}
