<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TabsGroupsDefinitions
 *
 * @ORM\Table(name="TABS_Groups_Definitions", indexes={@ORM\Index(name="FK_TABS_Groups_Definitions_TABS_Groups", columns={"tabGroupId"})})
 * @ORM\Entity
 */
class TabsGroupsDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tabGroupDefinitionsId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tabgroupdefinitionsid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var integer
     *
     * @ORM\Column(name="versionId", type="integer", nullable=false)
     */
    private $versionid;

    /**
     * @var string
     *
     * @ORM\Column(name="versionDescription", type="string", length=255, nullable=false)
     */
    private $versiondescription;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\TabsGroups
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\TabsGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tabGroupId", referencedColumnName="tabGroupId")
     * })
     */
    private $tabgroupid;



    /**
     * Get tabgroupdefinitionsid
     *
     * @return integer 
     */
    public function getTabgroupdefinitionsid()
    {
        return $this->tabgroupdefinitionsid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return TabsGroupsDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set versionid
     *
     * @param integer $versionid
     * @return TabsGroupsDefinitions
     */
    public function setVersionid($versionid)
    {
        $this->versionid = $versionid;

        return $this;
    }

    /**
     * Get versionid
     *
     * @return integer 
     */
    public function getVersionid()
    {
        return $this->versionid;
    }

    /**
     * Set versiondescription
     *
     * @param string $versiondescription
     * @return TabsGroupsDefinitions
     */
    public function setVersiondescription($versiondescription)
    {
        $this->versiondescription = $versiondescription;

        return $this;
    }

    /**
     * Get versiondescription
     *
     * @return string 
     */
    public function getVersiondescription()
    {
        return $this->versiondescription;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TabsGroupsDefinitions
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TabsGroupsDefinitions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return TabsGroupsDefinitions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return TabsGroupsDefinitions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set tabgroupid
     *
     * @param \PrismProductsManager\Entity\TabsGroups $tabgroupid
     * @return TabsGroupsDefinitions
     */
    public function setTabgroupid(\PrismProductsManager\Entity\TabsGroups $tabgroupid = null)
    {
        $this->tabgroupid = $tabgroupid;

        return $this;
    }

    /**
     * Get tabgroupid
     *
     * @return \PrismProductsManager\Entity\TabsGroups 
     */
    public function getTabgroupid()
    {
        return $this->tabgroupid;
    }
}
