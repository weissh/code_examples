<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributesGuiRepresentation
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES_GUI_Representation", uniqueConstraints={@ORM\UniqueConstraint(name="Index 2", columns={"commonAttributeId", "commonAttributesViewTypeId"})}, indexes={@ORM\Index(name="FK_COMMON_ATTRIBUTES_GUI_Rep_COM_ATTR_View_Type", columns={"commonAttributesViewTypeId", "commonAttributeId"}), @ORM\Index(name="FK_COMMON_ATTRIBUTES_GUI_Representation_COMMON_ATTRIBUTES_2", columns={"commonAttributesParentId"}), @ORM\Index(name="IDX_3B8FE2533972F46", columns={"commonAttributeId"}), @ORM\Index(name="IDX_3B8FE2590B2575", columns={"commonAttributesViewTypeId"})})
     * @ORM\Entity
 */
class CommonAttributesGuiRepresentation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributesGuiRepresentationId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commonattributesguirepresentationid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pmsDisplay", type="boolean", nullable=false)
     */
    private $pmsdisplay;

    /**
     * @var boolean
     *
     * @ORM\Column(name="shopDisplay", type="boolean", nullable=false)
     */
    private $shopdisplay;

    /**
     * @var string
     *
     * @ORM\Column(name="page", type="string", length=120, nullable=true)
     */
    private $page;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     * @var integer
     *
     * @ORM\Column(name="modifiedBy", type="integer", nullable=true)
     */
    private $modifiedby;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributes", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="commonAttributesParentId", referencedColumnName="commonAttributeId")
     * })
     */
    private $commonattributesparentid;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributes
     *
     * @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributes" , inversedBy="commonAttributeId")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="commonAttributeId", referencedColumnName="commonAttributeId")
     * })
     */
    private $commonattributeid;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributesViewType
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesViewType", inversedBy="commonattributeid")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="commonAttributesViewTypeId", referencedColumnName="commonAttributeViewTypeId")
     * })
     */
    private $commonattributesviewtypeid;



    /**
     * Get commonattributesguirepresentationid
     *
     * @return integer 
     */
    public function getCommonattributesguirepresentationid()
    {
        return $this->commonattributesguirepresentationid;
    }

    /**
     * Set pmsdisplay
     *
     * @param boolean $pmsdisplay
     * @return CommonAttributesGuiRepresentation
     */
    public function setPmsdisplay($pmsdisplay)
    {
        $this->pmsdisplay = $pmsdisplay;

        return $this;
    }

    /**
     * Get pmsdisplay
     *
     * @return boolean 
     */
    public function getPmsdisplay()
    {
        return $this->pmsdisplay;
    }

    /**
     * Set shopdisplay
     *
     * @param boolean $shopdisplay
     * @return CommonAttributesGuiRepresentation
     */
    public function setShopdisplay($shopdisplay)
    {
        $this->shopdisplay = $shopdisplay;

        return $this;
    }

    /**
     * Get shopdisplay
     *
     * @return boolean 
     */
    public function getShopdisplay()
    {
        return $this->shopdisplay;
    }

    /**
     * Set page
     *
     * @param string $page
     * @return CommonAttributesGuiRepresentation
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return string 
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return CommonAttributesGuiRepresentation
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return CommonAttributesGuiRepresentation
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set modifiedby
     *
     * @param integer $modifiedby
     * @return CommonAttributesGuiRepresentation
     */
    public function setModifiedby($modifiedby)
    {
        $this->modifiedby = $modifiedby;

        return $this;
    }

    /**
     * Get modifiedby
     *
     * @return integer 
     */
    public function getModifiedby()
    {
        return $this->modifiedby;
    }

    /**
     * Set commonattributesparentid
     *
     * @param \PrismProductsManager\Entity\CommonAttributes $commonattributesparentid
     * @return CommonAttributesGuiRepresentation
     */
    public function setCommonattributesparentid(\PrismProductsManager\Entity\CommonAttributes $commonattributesparentid = null)
    {
        $this->commonattributesparentid = $commonattributesparentid;

        return $this;
    }

    /**
     * Get commonattributesparentid
     *
     * @return \PrismProductsManager\Entity\CommonAttributes 
     */
    public function getCommonattributesparentid()
    {
        return $this->commonattributesparentid;
    }

    /**
     * Set commonattributeid
     *
     * @param \PrismProductsManager\Entity\CommonAttributes $commonattributeid
     * @return CommonAttributesGuiRepresentation
     */
    public function setCommonattributeid(\PrismProductsManager\Entity\CommonAttributes $commonattributeid = null)
    {
        $this->commonattributeid = $commonattributeid;

        return $this;
    }

    /**
     * Get commonattributeid
     *
     * @return \PrismProductsManager\Entity\CommonAttributes 
     */
    public function getCommonattributeid()
    {
        return $this->commonattributeid;
    }

    /**
     * Set commonattributesviewtypeid
     *
     * @param \PrismProductsManager\Entity\CommonAttributesViewType $commonattributesviewtypeid
     * @return CommonAttributesGuiRepresentation
     */
    public function setCommonattributesviewtypeid(\PrismProductsManager\Entity\CommonAttributesViewType $commonattributesviewtypeid = null)
    {
        $this->commonattributesviewtypeid = $commonattributesviewtypeid;

        return $this;
    }

    /**
     * Get commonattributesviewtypeid
     *
     * @return \PrismProductsManager\Entity\CommonAttributesViewType 
     */
    public function getCommonattributesviewtypeid()
    {
        return $this->commonattributesviewtypeid;
    }
}
