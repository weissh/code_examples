<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Schedules
 *
 * @ORM\Table(name="SCHEDULES", indexes={@ORM\Index(name="FK_SCHEDULES_SCHEDULE_TYPES", columns={"scheduleTypeId"}), @ORM\Index(name="Index 3", columns={"productOrVariantId", "productIdFrom"}), @ORM\Index(name="Index 4", columns={"processing"})})
 * @ORM\Entity(repositoryClass="PrismProductsManager\Entity\Repository\SchedulesRepository")
 */
class Schedules
{
    /**
     * @var integer
     *
     * @ORM\Column(name="scheduleId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $scheduleId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="scheduleStartDate", type="datetime", nullable=true)
     */
    private $scheduleStartDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="productOrVariantId", type="integer", nullable=false)
     */
    private $productOrVariantId;

    /**
     * @var string
     *
     * @ORM\Column(name="productIdFrom", type="string", length=75, nullable=false)
     */
    private $productIdFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="scheduledBy", type="integer", nullable=false)
     */
    private $scheduledBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="processing", type="string", length=150, nullable=true)
     */
    private $processing;

    /**
     * @var integer
     *
     * @ORM\Column(name="numberOfTries", type="smallint", nullable=false)
     */
    private $numberOfTries;

    /**
     * @var string
     *
     * @ORM\Column(name="response", type="text", nullable=true)
     */
    private $response;

    /**
     * @var \PrismProductsManager\Entity\ScheduleTypes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\ScheduleTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="scheduleTypeId", referencedColumnName="scheduleTypeId")
     * })
     */
    private $scheduleTypeId;

    /**
     * @var \PrismProductsManager\Entity\SchedulePrices
     *
     * @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\SchedulePrices" , mappedBy="scheduleId", cascade={"persist"})
     */
    private $schedulePrices;

    /**
     * @var \PrismProductsManager\Entity\ScheduleAttributes
     *
     * @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\ScheduleAttributes" , mappedBy="scheduleId", cascade={"persist"})
     */
    private $scheduleAttributes;

    /**
     * @return SchedulePrices
     */
    public function getSchedulePrices()
    {
        return $this->schedulePrices;
    }

    /**
     * @param SchedulePrices $schedulePrices
     */
    public function setSchedulePrices($schedulePrices)
    {
        $this->schedulePrices = $schedulePrices;
    }

    /**
     * @return ScheduleAttributes
     */
    public function getScheduleAttributes()
    {
        return $this->scheduleAttributes;
    }

    /**
     * @param ScheduleAttributes $scheduleAttributes
     */
    public function setScheduleAttributes($scheduleAttributes)
    {
        $this->scheduleAttributes = $scheduleAttributes;
    }

    /**
     * @return \DateTime
     */
    public function getScheduleStartDate()
    {
        return $this->scheduleStartDate;
    }

    /**
     * @param \DateTime $scheduleStartDate
     */
    public function setScheduleStartDate($scheduleStartDate)
    {
        $this->scheduleStartDate = $scheduleStartDate;
    }

    /**
     * @return int
     */
    public function getProductOrVariantId()
    {
        return $this->productOrVariantId;
    }

    /**
     * @param int $$productOrVariantId
     */
    public function setproductOrVariantId($productOrVariantId)
    {
        $this->productOrVariantId = $productOrVariantId;
    }

    /**
     * @return string
     */
    public function getProductIdFrom()
    {
        return $this->productIdFrom;
    }

    /**
     * @param string $productIdFrom
     */
    public function setProductIdFrom($productIdFrom)
    {
        $this->productIdFrom = $productIdFrom;
    }

    /**
     * @return int
     */
    public function getScheduledBy()
    {
        return $this->scheduledBy;
    }

    /**
     * @param int $scheduledBy
     */
    public function setScheduledBy($scheduledBy)
    {
        $this->scheduledBy = $scheduledBy;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return boolean
     */
    public function isProcessing()
    {
        return $this->processing;
    }

    /**
     * @param boolean $processing
     */
    public function setProcessing($processing)
    {
        $this->processing = $processing;
    }

    /**
     * @return int
     */
    public function getNumberOfTries()
    {
        return $this->numberOfTries;
    }

    /**
     * @param int $numberOfTries
     */
    public function setNumberOfTries($numberOfTries)
    {
        $this->numberOfTries = $numberOfTries;
    }

    /**
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param string $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * @return ScheduleTypes
     */
    public function getScheduleTypeId()
    {
        return $this->scheduleTypeId;
    }

    /**
     * @param ScheduleTypes $scheduleTypeId
     */
    public function setScheduleTypeId($scheduleTypeId)
    {
        $this->scheduleTypeId = $scheduleTypeId;
    }

    /**
     * @return int
     */
    public function getScheduleId()
    {
        return $this->scheduleId;
    }
}