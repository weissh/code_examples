<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BundlesRProductsRPrices
 *
 * @ORM\Table(name="BUNDLES_R_PRODUCTS_R_PRICES", indexes={@ORM\Index(name="Index 2", columns={"bundlesRProductsId"}), @ORM\Index(name="Index 3", columns={"currencyId"})})
 * @ORM\Entity
 */
class BundlesRProductsRPrices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bundlesRProductsRPricesId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bundlesrproductsrpricesid;

    /**
     * @var string
     *
     * @ORM\Column(name="priceValue", type="decimal", precision=8, scale=2, nullable=true)
     */
    private $pricevalue;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \PrismProductsManager\Entity\BundlesRProducts
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\BundlesRProducts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bundlesRProductsId", referencedColumnName="bundleRProductId")
     * })
     */
    private $bundlesrproductsid;

    /**
     * @var \PrismProductsManager\Entity\Currency
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Currency")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="currencyId", referencedColumnName="currencyId")
     * })
     */
    private $currencyid;



    /**
     * Get bundlesrproductsrpricesid
     *
     * @return integer 
     */
    public function getBundlesrproductsrpricesid()
    {
        return $this->bundlesrproductsrpricesid;
    }

    /**
     * Set pricevalue
     *
     * @param string $pricevalue
     * @return BundlesRProductsRPrices
     */
    public function setPricevalue($pricevalue)
    {
        $this->pricevalue = $pricevalue;

        return $this;
    }

    /**
     * Get pricevalue
     *
     * @return string 
     */
    public function getPricevalue()
    {
        return $this->pricevalue;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return BundlesRProductsRPrices
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return BundlesRProductsRPrices
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return BundlesRProductsRPrices
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set bundlesrproductsid
     *
     * @param \PrismProductsManager\Entity\BundlesRProducts $bundlesrproductsid
     * @return BundlesRProductsRPrices
     */
    public function setBundlesrproductsid(\PrismProductsManager\Entity\BundlesRProducts $bundlesrproductsid = null)
    {
        $this->bundlesrproductsid = $bundlesrproductsid;

        return $this;
    }

    /**
     * Get bundlesrproductsid
     *
     * @return \PrismProductsManager\Entity\BundlesRProducts 
     */
    public function getBundlesrproductsid()
    {
        return $this->bundlesrproductsid;
    }

    /**
     * Set currencyid
     *
     * @param \PrismProductsManager\Entity\Currency $currencyid
     * @return BundlesRProductsRPrices
     */
    public function setCurrencyid(\PrismProductsManager\Entity\Currency $currencyid = null)
    {
        $this->currencyid = $currencyid;

        return $this;
    }

    /**
     * Get currencyid
     *
     * @return \PrismProductsManager\Entity\Currency 
     */
    public function getCurrencyid()
    {
        return $this->currencyid;
    }
}
