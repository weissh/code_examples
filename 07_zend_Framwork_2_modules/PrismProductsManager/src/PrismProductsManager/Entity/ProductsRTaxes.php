<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsRTaxes
 *
 * @ORM\Table(name="PRODUCTS_R_TAXES", indexes={@ORM\Index(name="productId", columns={"productOrVariantId"}), @ORM\Index(name="taxId", columns={"taxId"})})
 * @ORM\Entity
 */
class ProductsRTaxes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productTaxId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $producttaxid;

    /**
     * @var integer
     *
     * @ORM\Column(name="productOrVariantId", type="integer", nullable=false)
     */
    private $productorvariantid;

    /**
     * @var integer
     *
     * @ORM\Column(name="taxId", type="integer", nullable=false)
     */
    private $taxid;

    /**
     * @var integer
     *
     * @ORM\Column(name="isVariant", type="smallint", nullable=false)
     */
    private $isvariant;



    /**
     * Get producttaxid
     *
     * @return integer 
     */
    public function getProducttaxid()
    {
        return $this->producttaxid;
    }

    /**
     * Set productorvariantid
     *
     * @param integer $productorvariantid
     * @return ProductsRTaxes
     */
    public function setProductorvariantid($productorvariantid)
    {
        $this->productorvariantid = $productorvariantid;

        return $this;
    }

    /**
     * Get productorvariantid
     *
     * @return integer 
     */
    public function getProductorvariantid()
    {
        return $this->productorvariantid;
    }

    /**
     * Set taxid
     *
     * @param integer $taxid
     * @return ProductsRTaxes
     */
    public function setTaxid($taxid)
    {
        $this->taxid = $taxid;

        return $this;
    }

    /**
     * Get taxid
     *
     * @return integer 
     */
    public function getTaxid()
    {
        return $this->taxid;
    }

    /**
     * Set isvariant
     *
     * @param integer $isvariant
     * @return ProductsRTaxes
     */
    public function setIsvariant($isvariant)
    {
        $this->isvariant = $isvariant;

        return $this;
    }

    /**
     * Get isvariant
     *
     * @return integer 
     */
    public function getIsvariant()
    {
        return $this->isvariant;
    }
}
