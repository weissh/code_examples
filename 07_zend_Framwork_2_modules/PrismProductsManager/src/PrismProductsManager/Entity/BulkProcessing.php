<?php

namespace PrismProductsManager\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * BulkProcessing
 *
 * @ORM\Table(name="BULK_PROCESSING")
 * @ORM\Entity(repositoryClass="PrismProductsManager\Entity\Repository\BulkProcessingRepository")
 */
class BulkProcessing
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bulk_processing_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bulkProcessingId;

    /**
     * @var string
     *
     * @ORM\Column(name="process_type", type="string", columnDefinition="ENUM('COMMON_CASCADE','PRICE_CASCADE','COMMON_ALL','PRICE_ALL')")
     */
    private $processType;

    /**
     * @var string
     *
     * @ORM\Column(name="orginal_file_name", type="string", length=245, nullable=false)
     */
    private $orginalFileName;

    /**
     * @var string
     *
     * @ORM\Column(name="styleOrOption", type="string", length=245, nullable=false)
     */
    private $styleoroption;

    /**
     * @var integer
     *
     * @ORM\Column(name="processing", type="integer", nullable=false)
     */
    private $processing = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="processed", type="integer", nullable=false)
     */
    private $processed = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="schedule", type="datetime", nullable=false)
     */
    private $schedule;

    /**
     * @var string
     *
     * @ORM\Column(name="processed_response", type="text", length=65535, nullable=true)
     */
    private $processedResponse;

    /**
     * @var string
     *
     * @ORM\Column(name="process_key", type="string", length=150, nullable=false)
     */
    private $processKey;

    /**
     * @var string
     *
     * @ORM\Column(name="process_value", type="text", length=65535, nullable=false)
     */
    private $processValue;

    /**
     * @var smallint
     *
     * @ORM\Column(name="sip_spectrum_notification", type="smallint", nullable=false)
     */
    private $sipSpectrumNotification;

    /**
     * @var string
     *
     * @ORM\Column(name="submitted_by", type="string", length=150, nullable=false)
     */
    private $submittedBy;

    /**
     * @var smallint
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="changed_file_name", type="string", length=245, nullable=false)
     */
    private $changedFileName;




    /**
     * Get bulkProcessingId
     *
     * @return integer
     */
    public function getBulkProcessingId()
    {
        return $this->bulkProcessingId;
    }

    /**
     * Set processType
     *
     * @param string $processType
     *
     * @return BulkProcessing
     */
    public function setProcessType($processType)
    {
        $this->processType = $processType;

        return $this;
    }

    /**
     * Get processType
     *
     * @return string
     */
    public function getProcessType()
    {
        return $this->processType;
    }

    /**
     * Set orginalFileName
     *
     * @param string $orginalFileName
     *
     * @return BulkProcessing
     */
    public function setOrginalFileName($orginalFileName)
    {
        $this->orginalFileName = $orginalFileName;

        return $this;
    }

    /**
     * Get orginalFileName
     *
     * @return string
     */
    public function getOrginalFileName()
    {
        return $this->orginalFileName;
    }

    /**
     * Set changedFileName
     *
     * @param string $changedFileName
     *
     * @return BulkProcessing
     */
    public function setChangedFileName($changedFileName)
    {
        $this->changedFileName = $changedFileName;

        return $this;
    }

    /**
     * Get changedFileName
     *
     * @return string
     */
    public function getChangedFileName()
    {
        return $this->changedFileName;
    }

    /**
     * Set styleoroption
     *
     * @param string $styleoroption
     *
     * @return BulkProcessing
     */
    public function setStyleoroption($styleoroption)
    {
        $this->styleoroption = $styleoroption;

        return $this;
    }

    /**
     * Get styleoroption
     *
     * @return string
     */
    public function getStyleoroption()
    {
        return $this->styleoroption;
    }

    /**
     * Set processing
     *
     * @param integer $processing
     *
     * @return BulkProcessing
     */
    public function setProcessing($processing)
    {
        $this->processing = $processing;

        return $this;
    }

    /**
     * Get processing
     *
     * @return integer
     */
    public function getProcessing()
    {
        return $this->processing;
    }

    /**
     * Set processed
     *
     * @param integer $processed
     *
     * @return BulkProcessing
     */
    public function setProcessed($processed)
    {
        $this->processed = $processed;

        return $this;
    }

    /**
     * Get processed
     *
     * @return integer
     */
    public function getProcessed()
    {
        return $this->processed;
    }

    /**
     * Set schedule
     *
     * @param \DateTime $schedule
     *
     * @return BulkProcessing
     */
    public function setSchedule($schedule)
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Get schedule
     *
     * @return \DateTime
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * Set processedResponse
     *
     * @param string $processedResponse
     *
     * @return BulkProcessing
     */
    public function setProcessedResponse($processedResponse)
    {
        $this->processedResponse = $processedResponse;

        return $this;
    }

    /**
     * Get processedResponse
     *
     * @return string
     */
    public function getProcessedResponse()
    {
        return $this->processedResponse;
    }

    /**
     * Set processKey
     *
     * @param string $processKey
     *
     * @return BulkProcessing
     */
    public function setProcessKey($processKey)
    {
        $this->processKey = $processKey;

        return $this;
    }

    /**
     * Get processKey
     *
     * @return string
     */
    public function getProcessKey()
    {
        return $this->processKey;
    }

    /**
     * Set processValue
     *
     * @param string $processValue
     *
     * @return BulkProcessing
     */
    public function setProcessValue($processValue)
    {
        $this->processValue = $processValue;

        return $this;
    }

    /**
     * Get processValue
     *
     * @return string
     */
    public function getProcessValue()
    {
        return $this->processValue;
    }

    /**
     * Set submittedBy
     *
     * @param string $submittedBy
     *
     * @return BulkProcessing
     */
    public function setSubmittedBy($submittedBy)
    {
        $this->submittedBy = $submittedBy;

        return $this;
    }

    /**
     * Get submittedBy
     *
     * @return string
     */
    public function getSubmittedBy()
    {
        return $this->submittedBy;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return BulkProcessing
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return BulkProcessing
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @return smallint
     */
    public function getSipSpectrumNotification()
    {
        return $this->sipSpectrumNotification;
    }

    /**
     * @param smallint $sipSpectrumNotification
     */
    public function setSipSpectrumNotification($sipSpectrumNotification)
    {
        $this->sipSpectrumNotification = $sipSpectrumNotification;
    }

    /**
     * @return smallint
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param smallint $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

}
