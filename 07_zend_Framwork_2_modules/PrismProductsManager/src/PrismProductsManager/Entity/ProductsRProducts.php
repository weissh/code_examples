<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsRProducts
 *
 * @ORM\Table(name="PRODUCTS_R_PRODUCTS", indexes={@ORM\Index(name="Index 2", columns={"productId", "productIdFrom"}), @ORM\Index(name="Index 3", columns={"relatedProductId", "relatedProductIdFrom"})})
 * @ORM\Entity
 */
class ProductsRProducts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productProductId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productproductid;

    /**
     * @var string
     *
     * @ORM\Column(name="productIdFrom", type="string", length=25, nullable=false)
     */
    private $productidfrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="productId", type="integer", nullable=false)
     */
    private $productid;

    /**
     * @var string
     *
     * @ORM\Column(name="relatedProductIdFrom", type="string", length=25, nullable=false)
     */
    private $relatedproductidfrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="relatedProductId", type="integer", nullable=false)
     */
    private $relatedproductid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get productproductid
     *
     * @return integer 
     */
    public function getProductproductid()
    {
        return $this->productproductid;
    }

    /**
     * Set productidfrom
     *
     * @param string $productidfrom
     * @return ProductsRProducts
     */
    public function setProductidfrom($productidfrom)
    {
        $this->productidfrom = $productidfrom;

        return $this;
    }

    /**
     * Get productidfrom
     *
     * @return string 
     */
    public function getProductidfrom()
    {
        return $this->productidfrom;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return ProductsRProducts
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set relatedproductidfrom
     *
     * @param string $relatedproductidfrom
     * @return ProductsRProducts
     */
    public function setRelatedproductidfrom($relatedproductidfrom)
    {
        $this->relatedproductidfrom = $relatedproductidfrom;

        return $this;
    }

    /**
     * Get relatedproductidfrom
     *
     * @return string 
     */
    public function getRelatedproductidfrom()
    {
        return $this->relatedproductidfrom;
    }

    /**
     * Set relatedproductid
     *
     * @param integer $relatedproductid
     * @return ProductsRProducts
     */
    public function setRelatedproductid($relatedproductid)
    {
        $this->relatedproductid = $relatedproductid;

        return $this;
    }

    /**
     * Get relatedproductid
     *
     * @return integer 
     */
    public function getRelatedproductid()
    {
        return $this->relatedproductid;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProductsRProducts
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsRProducts
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsRProducts
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
