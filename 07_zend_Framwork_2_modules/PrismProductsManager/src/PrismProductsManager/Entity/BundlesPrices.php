<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BundlesPrices
 *
 * @ORM\Table(name="BUNDLES_Prices", indexes={@ORM\Index(name="Index 2", columns={"bundleId"}), @ORM\Index(name="Index 3", columns={"currencyId"})})
 * @ORM\Entity
 */
class BundlesPrices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bundlePriceId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bundlepriceid;

    /**
     * @var string
     *
     * @ORM\Column(name="priceValue", type="decimal", precision=8, scale=2, nullable=true)
     */
    private $pricevalue;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \PrismProductsManager\Entity\Bundles
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Bundles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bundleId", referencedColumnName="bundleId")
     * })
     */
    private $bundleid;

    /**
     * @var \PrismProductsManager\Entity\Currency
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Currency")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="currencyId", referencedColumnName="currencyId")
     * })
     */
    private $currencyid;



    /**
     * Get bundlepriceid
     *
     * @return integer 
     */
    public function getBundlepriceid()
    {
        return $this->bundlepriceid;
    }

    /**
     * Set pricevalue
     *
     * @param string $pricevalue
     * @return BundlesPrices
     */
    public function setPricevalue($pricevalue)
    {
        $this->pricevalue = $pricevalue;

        return $this;
    }

    /**
     * Get pricevalue
     *
     * @return string 
     */
    public function getPricevalue()
    {
        return $this->pricevalue;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return BundlesPrices
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return BundlesPrices
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return BundlesPrices
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set bundleid
     *
     * @param \PrismProductsManager\Entity\Bundles $bundleid
     * @return BundlesPrices
     */
    public function setBundleid(\PrismProductsManager\Entity\Bundles $bundleid = null)
    {
        $this->bundleid = $bundleid;

        return $this;
    }

    /**
     * Get bundleid
     *
     * @return \PrismProductsManager\Entity\Bundles 
     */
    public function getBundleid()
    {
        return $this->bundleid;
    }

    /**
     * Set currencyid
     *
     * @param \PrismProductsManager\Entity\Currency $currencyid
     * @return BundlesPrices
     */
    public function setCurrencyid(\PrismProductsManager\Entity\Currency $currencyid = null)
    {
        $this->currencyid = $currencyid;

        return $this;
    }

    /**
     * Get currencyid
     *
     * @return \PrismProductsManager\Entity\Currency 
     */
    public function getCurrencyid()
    {
        return $this->currencyid;
    }
}
