<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputFilters
 *
 * @ORM\Table(name="INPUT_Filters")
 * @ORM\Entity
 */
class InputFilters
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputFilterId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputfilterid;

    /**
     * @var string
     *
     * @ORM\Column(name="inputFilterType", type="string", length=50, nullable=false)
     */
    private $inputfiltertype;



    /**
     * Get inputfilterid
     *
     * @return integer 
     */
    public function getInputfilterid()
    {
        return $this->inputfilterid;
    }

    /**
     * Set inputfiltertype
     *
     * @param string $inputfiltertype
     * @return InputFilters
     */
    public function setInputfiltertype($inputfiltertype)
    {
        $this->inputfiltertype = $inputfiltertype;

        return $this;
    }

    /**
     * Get inputfiltertype
     *
     * @return string 
     */
    public function getInputfiltertype()
    {
        return $this->inputfiltertype;
    }
}
