<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SizesShirtsInfo
 *
 * @ORM\Table(name="SIZES_Shirts_Info")
 * @ORM\Entity
 */
class SizesShirtsInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="sizeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sizeid;

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=8, nullable=true)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="chestInch", type="string", length=8, nullable=true)
     */
    private $chestinch;

    /**
     * @var string
     *
     * @ORM\Column(name="chestCm", type="string", length=8, nullable=true)
     */
    private $chestcm;

    /**
     * @var string
     *
     * @ORM\Column(name="eur", type="string", length=8, nullable=true)
     */
    private $eur;

    /**
     * @var boolean
     *
     * @ORM\Column(name="priority", type="boolean", nullable=false)
     */
    private $priority;



    /**
     * Get sizeid
     *
     * @return integer 
     */
    public function getSizeid()
    {
        return $this->sizeid;
    }

    /**
     * Set size
     *
     * @param string $size
     * @return SizesShirtsInfo
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set chestinch
     *
     * @param string $chestinch
     * @return SizesShirtsInfo
     */
    public function setChestinch($chestinch)
    {
        $this->chestinch = $chestinch;

        return $this;
    }

    /**
     * Get chestinch
     *
     * @return string 
     */
    public function getChestinch()
    {
        return $this->chestinch;
    }

    /**
     * Set chestcm
     *
     * @param string $chestcm
     * @return SizesShirtsInfo
     */
    public function setChestcm($chestcm)
    {
        $this->chestcm = $chestcm;

        return $this;
    }

    /**
     * Get chestcm
     *
     * @return string 
     */
    public function getChestcm()
    {
        return $this->chestcm;
    }

    /**
     * Set eur
     *
     * @param string $eur
     * @return SizesShirtsInfo
     */
    public function setEur($eur)
    {
        $this->eur = $eur;

        return $this;
    }

    /**
     * Get eur
     *
     * @return string 
     */
    public function getEur()
    {
        return $this->eur;
    }

    /**
     * Set priority
     *
     * @param boolean $priority
     * @return SizesShirtsInfo
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return boolean 
     */
    public function getPriority()
    {
        return $this->priority;
    }
}
