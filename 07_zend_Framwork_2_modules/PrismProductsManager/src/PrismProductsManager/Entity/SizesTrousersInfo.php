<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SizesTrousersInfo
 *
 * @ORM\Table(name="SIZES_Trousers_Info", uniqueConstraints={@ORM\UniqueConstraint(name="Index 2", columns={"size"})}, indexes={@ORM\Index(name="FK_SIZE_Trousers_Info_SIZE_Trousers", columns={"sizeId"})})
 * @ORM\Entity
 */
class SizesTrousersInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="sizeTrouserInfoId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sizetrouserinfoid;

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=15, nullable=true)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="waistInch", type="string", length=15, nullable=true)
     */
    private $waistinch;

    /**
     * @var string
     *
     * @ORM\Column(name="legInch", type="string", length=15, nullable=true)
     */
    private $leginch;

    /**
     * @var string
     *
     * @ORM\Column(name="waistCm", type="string", length=15, nullable=true)
     */
    private $waistcm;

    /**
     * @var string
     *
     * @ORM\Column(name="legCm", type="string", length=15, nullable=true)
     */
    private $legcm;

    /**
     * @var \PrismProductsManager\Entity\SizesTrousers
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\SizesTrousers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sizeId", referencedColumnName="sizeId")
     * })
     */
    private $sizeid;



    /**
     * Get sizetrouserinfoid
     *
     * @return integer 
     */
    public function getSizetrouserinfoid()
    {
        return $this->sizetrouserinfoid;
    }

    /**
     * Set size
     *
     * @param string $size
     * @return SizesTrousersInfo
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set waistinch
     *
     * @param string $waistinch
     * @return SizesTrousersInfo
     */
    public function setWaistinch($waistinch)
    {
        $this->waistinch = $waistinch;

        return $this;
    }

    /**
     * Get waistinch
     *
     * @return string 
     */
    public function getWaistinch()
    {
        return $this->waistinch;
    }

    /**
     * Set leginch
     *
     * @param string $leginch
     * @return SizesTrousersInfo
     */
    public function setLeginch($leginch)
    {
        $this->leginch = $leginch;

        return $this;
    }

    /**
     * Get leginch
     *
     * @return string 
     */
    public function getLeginch()
    {
        return $this->leginch;
    }

    /**
     * Set waistcm
     *
     * @param string $waistcm
     * @return SizesTrousersInfo
     */
    public function setWaistcm($waistcm)
    {
        $this->waistcm = $waistcm;

        return $this;
    }

    /**
     * Get waistcm
     *
     * @return string 
     */
    public function getWaistcm()
    {
        return $this->waistcm;
    }

    /**
     * Set legcm
     *
     * @param string $legcm
     * @return SizesTrousersInfo
     */
    public function setLegcm($legcm)
    {
        $this->legcm = $legcm;

        return $this;
    }

    /**
     * Get legcm
     *
     * @return string 
     */
    public function getLegcm()
    {
        return $this->legcm;
    }

    /**
     * Set sizeid
     *
     * @param \PrismProductsManager\Entity\SizesTrousers $sizeid
     * @return SizesTrousersInfo
     */
    public function setSizeid(\PrismProductsManager\Entity\SizesTrousers $sizeid = null)
    {
        $this->sizeid = $sizeid;

        return $this;
    }

    /**
     * Get sizeid
     *
     * @return \PrismProductsManager\Entity\SizesTrousers 
     */
    public function getSizeid()
    {
        return $this->sizeid;
    }
}
