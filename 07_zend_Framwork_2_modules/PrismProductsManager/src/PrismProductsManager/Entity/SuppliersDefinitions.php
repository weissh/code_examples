<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SuppliersDefinitions
 *
 * @ORM\Table(name="SUPPLIERS_Definitions", indexes={@ORM\Index(name="FK_SUPPLIERS_Definitions_SUPPLIERS", columns={"supplierId"})})
 * @ORM\Entity
 */
class SuppliersDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="suppliersDefinitionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $suppliersdefinitionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="supplierId", type="integer", nullable=false)
     */
    private $supplierid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="supplierName", type="string", length=50, nullable=false)
     */
    private $suppliername;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;



    /**
     * Get suppliersdefinitionid
     *
     * @return integer 
     */
    public function getSuppliersdefinitionid()
    {
        return $this->suppliersdefinitionid;
    }

    /**
     * Set supplierid
     *
     * @param integer $supplierid
     * @return SuppliersDefinitions
     */
    public function setSupplierid($supplierid)
    {
        $this->supplierid = $supplierid;

        return $this;
    }

    /**
     * Get supplierid
     *
     * @return integer 
     */
    public function getSupplierid()
    {
        return $this->supplierid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return SuppliersDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set suppliername
     *
     * @param string $suppliername
     * @return SuppliersDefinitions
     */
    public function setSuppliername($suppliername)
    {
        $this->suppliername = $suppliername;

        return $this;
    }

    /**
     * Get suppliername
     *
     * @return string 
     */
    public function getSuppliername()
    {
        return $this->suppliername;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return SuppliersDefinitions
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
