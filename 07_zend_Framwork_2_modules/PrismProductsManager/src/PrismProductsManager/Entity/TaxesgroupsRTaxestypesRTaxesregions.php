<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaxesgroupsRTaxestypesRTaxesregions
 *
 * @ORM\Table(name="TaxesGroups_R_TaxesTypes_R_TaxesRegions", uniqueConstraints={@ORM\UniqueConstraint(name="taxGroupId_taxTypeId_taxRegionId_status", columns={"taxGroupId", "taxTypeId", "taxRegionId", "status"})}, indexes={@ORM\Index(name="taxGroupId", columns={"taxGroupId"}), @ORM\Index(name="taxTypeId", columns={"taxTypeId"}), @ORM\Index(name="taxRegionId", columns={"taxRegionId"}), @ORM\Index(name="status", columns={"status"})})
 * @ORM\Entity
 */
class TaxesgroupsRTaxestypesRTaxesregions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="TaxGroupTypeRegionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $taxgrouptyperegionid;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float", precision=10, scale=0, nullable=false)
     */
    private $value;

    /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="integer", nullable=false)
     */
    private $priority;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\TaxesGroups
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\TaxesGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="taxGroupId", referencedColumnName="taxGroupId")
     * })
     */
    private $taxgroupid;

    /**
     * @var \PrismProductsManager\Entity\TaxesRegions
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\TaxesRegions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="taxRegionId", referencedColumnName="taxRegionId")
     * })
     */
    private $taxregionid;

    /**
     * @var \PrismProductsManager\Entity\TaxesTypes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\TaxesTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="taxTypeId", referencedColumnName="taxTypeId")
     * })
     */
    private $taxtypeid;



    /**
     * Get taxgrouptyperegionid
     *
     * @return integer 
     */
    public function getTaxgrouptyperegionid()
    {
        return $this->taxgrouptyperegionid;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return TaxesgroupsRTaxestypesRTaxesregions
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     * @return TaxesgroupsRTaxestypesRTaxesregions
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TaxesgroupsRTaxestypesRTaxesregions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return TaxesgroupsRTaxestypesRTaxesregions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return TaxesgroupsRTaxestypesRTaxesregions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set taxgroupid
     *
     * @param \PrismProductsManager\Entity\TaxesGroups $taxgroupid
     * @return TaxesgroupsRTaxestypesRTaxesregions
     */
    public function setTaxgroupid(\PrismProductsManager\Entity\TaxesGroups $taxgroupid = null)
    {
        $this->taxgroupid = $taxgroupid;

        return $this;
    }

    /**
     * Get taxgroupid
     *
     * @return \PrismProductsManager\Entity\TaxesGroups 
     */
    public function getTaxgroupid()
    {
        return $this->taxgroupid;
    }

    /**
     * Set taxregionid
     *
     * @param \PrismProductsManager\Entity\TaxesRegions $taxregionid
     * @return TaxesgroupsRTaxestypesRTaxesregions
     */
    public function setTaxregionid(\PrismProductsManager\Entity\TaxesRegions $taxregionid = null)
    {
        $this->taxregionid = $taxregionid;

        return $this;
    }

    /**
     * Get taxregionid
     *
     * @return \PrismProductsManager\Entity\TaxesRegions 
     */
    public function getTaxregionid()
    {
        return $this->taxregionid;
    }

    /**
     * Set taxtypeid
     *
     * @param \PrismProductsManager\Entity\TaxesTypes $taxtypeid
     * @return TaxesgroupsRTaxestypesRTaxesregions
     */
    public function setTaxtypeid(\PrismProductsManager\Entity\TaxesTypes $taxtypeid = null)
    {
        $this->taxtypeid = $taxtypeid;

        return $this;
    }

    /**
     * Get taxtypeid
     *
     * @return \PrismProductsManager\Entity\TaxesTypes 
     */
    public function getTaxtypeid()
    {
        return $this->taxtypeid;
    }
}
