<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CountriesRTerritories
 *
 * @ORM\Table(name="COUNTRIES_R_TERRITORIES", indexes={@ORM\Index(name="countryId", columns={"iso2CountryCode"}), @ORM\Index(name="territoryId", columns={"iso2TerritoryCode"})})
 * @ORM\Entity
 */
class CountriesRTerritories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="countryTerritoryId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $countryterritoryid;

    /**
     * @var string
     *
     * @ORM\Column(name="iso2CountryCode", type="string", length=2, nullable=false)
     */
    private $iso2countrycode;

    /**
     * @var string
     *
     * @ORM\Column(name="iso2TerritoryCode", type="string", length=2, nullable=false)
     */
    private $iso2territorycode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get countryterritoryid
     *
     * @return integer 
     */
    public function getCountryterritoryid()
    {
        return $this->countryterritoryid;
    }

    /**
     * Set iso2countrycode
     *
     * @param string $iso2countrycode
     * @return CountriesRTerritories
     */
    public function setIso2countrycode($iso2countrycode)
    {
        $this->iso2countrycode = $iso2countrycode;

        return $this;
    }

    /**
     * Get iso2countrycode
     *
     * @return string 
     */
    public function getIso2countrycode()
    {
        return $this->iso2countrycode;
    }

    /**
     * Set iso2territorycode
     *
     * @param string $iso2territorycode
     * @return CountriesRTerritories
     */
    public function setIso2territorycode($iso2territorycode)
    {
        $this->iso2territorycode = $iso2territorycode;

        return $this;
    }

    /**
     * Get iso2territorycode
     *
     * @return string 
     */
    public function getIso2territorycode()
    {
        return $this->iso2territorycode;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return CountriesRTerritories
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return CountriesRTerritories
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
