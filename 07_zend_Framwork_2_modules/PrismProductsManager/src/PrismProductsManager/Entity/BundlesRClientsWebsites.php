<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BundlesRClientsWebsites
 *
 * @ORM\Table(name="BUNDLES_R_CLIENTS_Websites", indexes={@ORM\Index(name="Index 2", columns={"websiteId"}), @ORM\Index(name="Index 3", columns={"bundleId"})})
 * @ORM\Entity
 */
class BundlesRClientsWebsites
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bundleRWebsiteId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bundlerwebsiteid;

    /**
     * @var integer
     *
     * @ORM\Column(name="websiteId", type="integer", nullable=false)
     */
    private $websiteid;

    /**
     * @var \PrismProductsManager\Entity\Bundles
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Bundles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bundleId", referencedColumnName="bundleId")
     * })
     */
    private $bundleid;



    /**
     * Get bundlerwebsiteid
     *
     * @return integer 
     */
    public function getBundlerwebsiteid()
    {
        return $this->bundlerwebsiteid;
    }

    /**
     * Set websiteid
     *
     * @param integer $websiteid
     * @return BundlesRClientsWebsites
     */
    public function setWebsiteid($websiteid)
    {
        $this->websiteid = $websiteid;

        return $this;
    }

    /**
     * Get websiteid
     *
     * @return integer 
     */
    public function getWebsiteid()
    {
        return $this->websiteid;
    }

    /**
     * Set bundleid
     *
     * @param \PrismProductsManager\Entity\Bundles $bundleid
     * @return BundlesRClientsWebsites
     */
    public function setBundleid(\PrismProductsManager\Entity\Bundles $bundleid = null)
    {
        $this->bundleid = $bundleid;

        return $this;
    }

    /**
     * Get bundleid
     *
     * @return \PrismProductsManager\Entity\Bundles 
     */
    public function getBundleid()
    {
        return $this->bundleid;
    }
}
