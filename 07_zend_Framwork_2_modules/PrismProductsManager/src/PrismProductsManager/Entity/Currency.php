<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Currency
 *
 * @ORM\Table(name="CURRENCY")
 * @ORM\Entity
 */
class Currency
{
    /**
     * @var integer
     *
     * @ORM\Column(name="currencyId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $currencyid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDefault", type="boolean", nullable=false)
     */
    private $isdefault;

    /**
     * @var string
     *
     * @ORM\Column(name="isoCode", type="string", length=3, nullable=false)
     */
    private $isocode;

    /**
     * @var string
     *
     * @ORM\Column(name="htmlCode", type="string", length=7, nullable=false)
     */
    private $htmlcode;

    /**
     * @var string
     *
     * @ORM\Column(name="sign", type="string", length=8, nullable=false)
     */
    private $sign;

    /**
     * @var boolean
     *
     * @ORM\Column(name="blank", type="boolean", nullable=false)
     */
    private $blank;

    /**
     * @var boolean
     *
     * @ORM\Column(name="format", type="boolean", nullable=false)
     */
    private $format;

    /**
     * @var boolean
     *
     * @ORM\Column(name="decimals", type="boolean", nullable=false)
     */
    private $decimals;

    /**
     * @var string
     *
     * @ORM\Column(name="conversionRate", type="decimal", precision=13, scale=6, nullable=false)
     */
    private $conversionrate;

    /**
     * @var string
     *
     * @ORM\Column(name="plusFixed", type="decimal", precision=13, scale=6, nullable=true)
     */
    private $plusfixed;

    /**
     * @var string
     *
     * @ORM\Column(name="threshold", type="decimal", precision=13, scale=0, nullable=true)
     */
    private $threshold;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;



    /**
     * Get currencyid
     *
     * @return integer 
     */
    public function getCurrencyid()
    {
        return $this->currencyid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Currency
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isdefault
     *
     * @param boolean $isdefault
     * @return Currency
     */
    public function setIsdefault($isdefault)
    {
        $this->isdefault = $isdefault;

        return $this;
    }

    /**
     * Get isdefault
     *
     * @return boolean 
     */
    public function getIsdefault()
    {
        return $this->isdefault;
    }

    /**
     * Set isocode
     *
     * @param string $isocode
     * @return Currency
     */
    public function setIsocode($isocode)
    {
        $this->isocode = $isocode;

        return $this;
    }

    /**
     * Get isocode
     *
     * @return string 
     */
    public function getIsocode()
    {
        return $this->isocode;
    }

    /**
     * Set htmlcode
     *
     * @param string $htmlcode
     * @return Currency
     */
    public function setHtmlcode($htmlcode)
    {
        $this->htmlcode = $htmlcode;

        return $this;
    }

    /**
     * Get htmlcode
     *
     * @return string 
     */
    public function getHtmlcode()
    {
        return $this->htmlcode;
    }

    /**
     * Set sign
     *
     * @param string $sign
     * @return Currency
     */
    public function setSign($sign)
    {
        $this->sign = $sign;

        return $this;
    }

    /**
     * Get sign
     *
     * @return string 
     */
    public function getSign()
    {
        return $this->sign;
    }

    /**
     * Set blank
     *
     * @param boolean $blank
     * @return Currency
     */
    public function setBlank($blank)
    {
        $this->blank = $blank;

        return $this;
    }

    /**
     * Get blank
     *
     * @return boolean 
     */
    public function getBlank()
    {
        return $this->blank;
    }

    /**
     * Set format
     *
     * @param boolean $format
     * @return Currency
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return boolean 
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set decimals
     *
     * @param boolean $decimals
     * @return Currency
     */
    public function setDecimals($decimals)
    {
        $this->decimals = $decimals;

        return $this;
    }

    /**
     * Get decimals
     *
     * @return boolean 
     */
    public function getDecimals()
    {
        return $this->decimals;
    }

    /**
     * Set conversionrate
     *
     * @param string $conversionrate
     * @return Currency
     */
    public function setConversionrate($conversionrate)
    {
        $this->conversionrate = $conversionrate;

        return $this;
    }

    /**
     * Get conversionrate
     *
     * @return string 
     */
    public function getConversionrate()
    {
        return $this->conversionrate;
    }

    /**
     * Set plusfixed
     *
     * @param string $plusfixed
     * @return Currency
     */
    public function setPlusfixed($plusfixed)
    {
        $this->plusfixed = $plusfixed;

        return $this;
    }

    /**
     * Get plusfixed
     *
     * @return string 
     */
    public function getPlusfixed()
    {
        return $this->plusfixed;
    }

    /**
     * Set threshold
     *
     * @param string $threshold
     * @return Currency
     */
    public function setThreshold($threshold)
    {
        $this->threshold = $threshold;

        return $this;
    }

    /**
     * Get threshold
     *
     * @return string 
     */
    public function getThreshold()
    {
        return $this->threshold;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Currency
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }
}
