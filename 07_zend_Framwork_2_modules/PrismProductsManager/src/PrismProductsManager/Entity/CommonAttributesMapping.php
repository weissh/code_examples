<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributesMapping
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES_Mapping", indexes={@ORM\Index(name="FK_COMMON_ATTRIBUTES_Mapping_COMMON_ATTRIBUTES", columns={"commonAttributeId"})})
 * @ORM\Entity
 */
class CommonAttributesMapping
{
    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributeMappingId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commonAttributeMappingId;

    /**
     * @var string
     *
     * @ORM\Column(name="commonAttributeMapping", type="string", length=250, nullable=true)
     */
    private $commonAttributeMapping;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="commonAttributeId", referencedColumnName="commonAttributeId")
     * })
     */
    private $commonAttributeId;

    /**
     * @return string
     */
    public function getCommonAttributeMapping()
    {
        return $this->commonAttributeMapping;
    }

    /**
     * @param string $commonAttributeMapping
     */
    public function setCommonAttributeMapping($commonAttributeMapping)
    {
        $this->commonAttributeMapping = $commonAttributeMapping;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return CommonAttributes
     */
    public function getCommonAttributeId()
    {
        return $this->commonAttributeId;
    }

    /**
     * @param CommonAttributes $commonAttributeId
     */
    public function setCommonAttributeId($commonAttributeId)
    {
        $this->commonAttributeId = $commonAttributeId;
    }

    /**
     * @return int
     */
    public function getCommonAttributeMappingId()
    {
        return $this->commonAttributeMappingId;
    }



}
