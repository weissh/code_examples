<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ScheduleTypes
 *
 * @ORM\Table(name="SCHEDULE_TYPES")
 * @ORM\Entity
 */
class ScheduleTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="scheduleTypeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $scheduleTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="scheduleType", type="string", length=75, nullable=false)
     */
    private $scheduleType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedAt;

    /**
     * @return string
     */
    public function getScheduleType()
    {
        return $this->scheduleType;
    }

    /**
     * @param string $scheduleType
     */
    public function setScheduleType($scheduleType)
    {
        $this->scheduleType = $scheduleType;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @return int
     */
    public function getScheduleTypeId()
    {
        return $this->scheduleTypeId;
    }
}