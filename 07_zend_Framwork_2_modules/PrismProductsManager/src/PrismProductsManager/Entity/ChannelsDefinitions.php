<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ChannelsDefinitions
 *
 * @ORM\Table(name="CHANNELS_Definitions", indexes={@ORM\Index(name="languageId", columns={"languageId"}), @ORM\Index(name="name", columns={"name"}), @ORM\Index(name="languageId_name", columns={"languageId", "name", "status"}), @ORM\Index(name="status", columns={"status"}), @ORM\Index(name="FK_CHANNELS_Definitions_CHANNELS", columns={"channelId"})})
 * @ORM\Entity
 */
class ChannelsDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="channelDefinitionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $channeldefinitionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=20, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\Channels
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Channels")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="channelId", referencedColumnName="channelId")
     * })
     */
    private $channelid;



    /**
     * Get channeldefinitionid
     *
     * @return integer 
     */
    public function getChanneldefinitionid()
    {
        return $this->channeldefinitionid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return ChannelsDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ChannelsDefinitions
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ChannelsDefinitions
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ChannelsDefinitions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ChannelsDefinitions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ChannelsDefinitions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set channelid
     *
     * @param \PrismProductsManager\Entity\Channels $channelid
     * @return ChannelsDefinitions
     */
    public function setChannelid(\PrismProductsManager\Entity\Channels $channelid = null)
    {
        $this->channelid = $channelid;

        return $this;
    }

    /**
     * Get channelid
     *
     * @return \PrismProductsManager\Entity\Channels 
     */
    public function getChannelid()
    {
        return $this->channelid;
    }
}
