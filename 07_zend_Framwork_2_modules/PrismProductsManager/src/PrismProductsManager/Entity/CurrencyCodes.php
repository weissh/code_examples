<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CurrencyCodes
 *
 * @ORM\Table(name="CURRENCY_Codes")
 * @ORM\Entity
 */
class CurrencyCodes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="currencyCodeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $currencycodeid;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=75, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="currencyName", type="string", length=75, nullable=true)
     */
    private $currencyname;

    /**
     * @var string
     *
     * @ORM\Column(name="alphabeticCode", type="string", length=10, nullable=true)
     */
    private $alphabeticcode;

    /**
     * @var string
     *
     * @ORM\Column(name="numericCode", type="string", length=10, nullable=true)
     */
    private $numericcode;

    /**
     * @var string
     *
     * @ORM\Column(name="minorUnit", type="string", length=10, nullable=true)
     */
    private $minorunit;

    /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="integer", nullable=true)
     */
    private $priority;



    /**
     * Get currencycodeid
     *
     * @return integer 
     */
    public function getCurrencycodeid()
    {
        return $this->currencycodeid;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return CurrencyCodes
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set currencyname
     *
     * @param string $currencyname
     * @return CurrencyCodes
     */
    public function setCurrencyname($currencyname)
    {
        $this->currencyname = $currencyname;

        return $this;
    }

    /**
     * Get currencyname
     *
     * @return string 
     */
    public function getCurrencyname()
    {
        return $this->currencyname;
    }

    /**
     * Set alphabeticcode
     *
     * @param string $alphabeticcode
     * @return CurrencyCodes
     */
    public function setAlphabeticcode($alphabeticcode)
    {
        $this->alphabeticcode = $alphabeticcode;

        return $this;
    }

    /**
     * Get alphabeticcode
     *
     * @return string 
     */
    public function getAlphabeticcode()
    {
        return $this->alphabeticcode;
    }

    /**
     * Set numericcode
     *
     * @param string $numericcode
     * @return CurrencyCodes
     */
    public function setNumericcode($numericcode)
    {
        $this->numericcode = $numericcode;

        return $this;
    }

    /**
     * Get numericcode
     *
     * @return string 
     */
    public function getNumericcode()
    {
        return $this->numericcode;
    }

    /**
     * Set minorunit
     *
     * @param string $minorunit
     * @return CurrencyCodes
     */
    public function setMinorunit($minorunit)
    {
        $this->minorunit = $minorunit;

        return $this;
    }

    /**
     * Get minorunit
     *
     * @return string 
     */
    public function getMinorunit()
    {
        return $this->minorunit;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     * @return CurrencyCodes
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer 
     */
    public function getPriority()
    {
        return $this->priority;
    }
}
