<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributesValuesChain
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES_VALUES_Chain", indexes={@ORM\Index(name="FK_COMMON_ATTRIBUTES_VALUES_Chain_COMMON_ATTRIBUTES_VALUES", columns={"sourceValueId"}), @ORM\Index(name="FK_COMMON_ATTRIBUTES_VALUES_Chain_COMMON_ATTRIBUTES_VALUES_2", columns={"destinationValueId"})})
 * @ORM\Entity
 */
class CommonAttributesValuesChain
{
    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributesValuesChainId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commonattributesvalueschainid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     * @var integer
     *
     * @ORM\Column(name="modifiedBy", type="integer", nullable=false)
     */
    private $modifiedby;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributesValues
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesValues")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sourceValueId", referencedColumnName="commonAttributeValueId")
     * })
     */
    private $sourcevalueid;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributesValues
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesValues")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="destinationValueId", referencedColumnName="commonAttributeValueId")
     * })
     */
    private $destinationvalueid;

    /**
     * Get commonattributesvalueschainid
     *
     * @return integer 
     */
    public function getCommonattributesvalueschainid()
    {
        return $this->commonattributesvalueschainid;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return CommonAttributesValuesChain
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return CommonAttributesValuesChain
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set modifiedby
     *
     * @param integer $modifiedby
     * @return CommonAttributesValuesChain
     */
    public function setModifiedby($modifiedby)
    {
        $this->modifiedby = $modifiedby;

        return $this;
    }

    /**
     * Get modifiedby
     *
     * @return integer 
     */
    public function getModifiedby()
    {
        return $this->modifiedby;
    }

    /**
     * Set sourcevalueid
     *
     * @param \PrismProductsManager\Entity\CommonAttributesValues $sourcevalueid
     * @return CommonAttributesValuesChain
     */
    public function setSourcevalueid(\PrismProductsManager\Entity\CommonAttributesValues $sourcevalueid = null)
    {
        $this->sourcevalueid = $sourcevalueid;

        return $this;
    }

    /**
     * Get sourcevalueid
     *
     * @return \PrismProductsManager\Entity\CommonAttributesValues 
     */
    public function getSourcevalueid()
    {
        return $this->sourcevalueid;
    }

    /**
     * Set destinationvalueid
     *
     * @param \PrismProductsManager\Entity\CommonAttributesValues $destinationvalueid
     * @return CommonAttributesValuesChain
     */
    public function setDestinationvalueid(\PrismProductsManager\Entity\CommonAttributesValues $destinationvalueid = null)
    {
        $this->destinationvalueid = $destinationvalueid;

        return $this;
    }

    /**
     * Get destinationvalueid
     *
     * @return \PrismProductsManager\Entity\CommonAttributesValues 
     */
    public function getDestinationvalueid()
    {
        return $this->destinationvalueid;
    }
}
