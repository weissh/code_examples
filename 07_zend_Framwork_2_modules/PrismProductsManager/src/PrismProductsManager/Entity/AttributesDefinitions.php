<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AttributesDefinitions
 *
 * @ORM\Table(name="ATTRIBUTES_Definitions", indexes={@ORM\Index(name="Index 2", columns={"languageId"}), @ORM\Index(name="Index 3", columns={"attributeId"}), @ORM\Index(name="attributeValue", columns={"attributeValue"})})
 * @ORM\Entity
 */
class AttributesDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="attributesDefinitionsId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $attributesdefinitionsid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="attributeValue", type="string", length=150, nullable=false)
     */
    private $attributevalue;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\Attributes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Attributes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="attributeId", referencedColumnName="attributeId")
     * })
     */
    private $attributeid;



    /**
     * Get attributesdefinitionsid
     *
     * @return integer 
     */
    public function getAttributesdefinitionsid()
    {
        return $this->attributesdefinitionsid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return AttributesDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set attributevalue
     *
     * @param string $attributevalue
     * @return AttributesDefinitions
     */
    public function setAttributevalue($attributevalue)
    {
        $this->attributevalue = $attributevalue;

        return $this;
    }

    /**
     * Get attributevalue
     *
     * @return string 
     */
    public function getAttributevalue()
    {
        return $this->attributevalue;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return AttributesDefinitions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return AttributesDefinitions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return AttributesDefinitions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set attributeid
     *
     * @param \PrismProductsManager\Entity\Attributes $attributeid
     * @return AttributesDefinitions
     */
    public function setAttributeid(\PrismProductsManager\Entity\Attributes $attributeid = null)
    {
        $this->attributeid = $attributeid;

        return $this;
    }

    /**
     * Get attributeid
     *
     * @return \PrismProductsManager\Entity\Attributes 
     */
    public function getAttributeid()
    {
        return $this->attributeid;
    }
}
