<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributesValuesEquivalence
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES_VALUES_Equivalence", uniqueConstraints={@ORM\UniqueConstraint(name="Index 3", columns={"uniqueGroupId", "commonAttributeValueId"})}, indexes={@ORM\Index(name="COMMON_ATTRIBUTES_VALUES_Equivalence_ibfk_1", columns={"commonAttributeValueId"})})
 * @ORM\Entity
 */
class CommonAttributesValuesEquivalence
{
    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributesValuesEquivalenceId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commonattributesvaluesequivalenceid;

    /**
     * @var integer
     *
     * @ORM\Column(name="uniqueGroupId", type="integer", nullable=false)
     *  @ORM\OneToMany(targetEntity="PrismProductsManager\Entity\CommonAttributesValuesEquivalence",mappedBy="uniqueGroupId")
     */
    private $uniquegroupid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     * @var integer
     *
     * @ORM\Column(name="modifiedBy", type="integer", nullable=false)
     */
    private $modifiedby;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributesValues
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesValues" , inversedBy="commonAttributeValueId")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="commonAttributeValueId", referencedColumnName="commonAttributeValueId")
     * })
     */
    private $commonAttributeValueId;


    /**
     * Get commonattributesvaluesequivalenceid
     *
     * @return integer 
     */
    public function getCommonattributesvaluesequivalenceid()
    {
        return $this->commonattributesvaluesequivalenceid;
    }

    /**
     * Set uniquegroupid
     *
     * @param integer $uniquegroupid
     * @return CommonAttributesValuesEquivalence
     */
    public function setUniquegroupid($uniquegroupid)
    {
        $this->uniquegroupid = $uniquegroupid;

        return $this;
    }

    /**
     * Get uniquegroupid
     *
     * @return integer 
     */
    public function getUniquegroupid()
    {
        return $this->uniquegroupid;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return CommonAttributesValuesEquivalence
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return CommonAttributesValuesEquivalence
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set modifiedby
     *
     * @param integer $modifiedby
     * @return CommonAttributesValuesEquivalence
     */
    public function setModifiedby($modifiedby)
    {
        $this->modifiedby = $modifiedby;

        return $this;
    }

    /**
     * Get modifiedby
     *
     * @return integer 
     */
    public function getModifiedby()
    {
        return $this->modifiedby;
    }

    /**
     * Set commonAttributeValueId
     *
     * @param \PrismProductsManager\Entity\CommonAttributesValues $commonAttributeValueId
     * @return CommonAttributesValuesEquivalence
     */
    public function setcommonAttributeValueId(\PrismProductsManager\Entity\CommonAttributesValues $commonAttributeValueId = null)
    {
        $this->commonAttributeValueId = $commonAttributeValueId;

        return $this;
    }

    /**
     * Get commonAttributeValueId
     *
     * @return \PrismProductsManager\Entity\CommonAttributesValues 
     */
    public function getcommonAttributeValueId()
    {
        return $this->commonAttributeValueId;
    }
}
