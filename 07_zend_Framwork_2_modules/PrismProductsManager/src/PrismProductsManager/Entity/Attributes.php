<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Attributes
 *
 * @ORM\Table(name="ATTRIBUTES", indexes={@ORM\Index(name="attributeGroupId", columns={"attributeGroupId"})})
 * @ORM\Entity
 */
class Attributes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="attributeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $attributeid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;


    /**
     * @var \PrismProductsManager\Entity\AttributesGroups
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\AttributesGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="attributeGroupId", referencedColumnName="attributeGroupId")
     * })
     */
    private $attributegroupid;


    /**
     *
     *  @var \PrismProductsManager\Entity\AttributesDefinitions
     *
     *  @ORM\OneToMany(targetEntity="PrismProductsManager\Entity\AttributesDefinitions", mappedBy="attributeid")
     */
    private $attributeDefinitions;


    /**
     * Get attributeid
     *
     * @return integer 
     */
    public function getAttributeid()
    {
        return $this->attributeid;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Attributes
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Attributes
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Attributes
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set attributegroupid
     *
     * @param \PrismProductsManager\Entity\AttributesGroups $attributegroupid
     * @return Attributes
     */
    public function setAttributegroupid(\PrismProductsManager\Entity\AttributesGroups $attributegroupid = null)
    {
        $this->attributegroupid = $attributegroupid;

        return $this;
    }

    /**
     * Get attributegroupid
     *
     * @return \PrismProductsManager\Entity\AttributesGroups 
     */
    public function getAttributegroupid()
    {
        return $this->attributegroupid;
    }

    /**
     * Get attributeDefinition
     *
     * @return \PrismProductsManager\Entity\AttributesDefinitions
     */
    public function getAttributeDefinition()
    {
        return $this->attributeDefinitions;
    }


}
