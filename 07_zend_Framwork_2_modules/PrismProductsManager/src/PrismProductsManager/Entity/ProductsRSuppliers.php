<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsRSuppliers
 *
 * @ORM\Table(name="PRODUCTS_R_SUPPLIERS", indexes={@ORM\Index(name="FK__SUPPLIERS", columns={"supplierId"}), @ORM\Index(name="Index 3", columns={"productId"})})
 * @ORM\Entity
 */
class ProductsRSuppliers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productSupplierId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productsupplierid;

    /**
     * @var string
     *
     * @ORM\Column(name="productIdFrom", type="string", nullable=false)
     */
    private $productidfrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="productId", type="integer", nullable=false)
     */
    private $productid;

    /**
     * @var \PrismProductsManager\Entity\Suppliers
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Suppliers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="supplierId", referencedColumnName="supplierId")
     * })
     */
    private $supplierid;



    /**
     * Get productsupplierid
     *
     * @return integer 
     */
    public function getProductsupplierid()
    {
        return $this->productsupplierid;
    }

    /**
     * Set productidfrom
     *
     * @param string $productidfrom
     * @return ProductsRSuppliers
     */
    public function setProductidfrom($productidfrom)
    {
        $this->productidfrom = $productidfrom;

        return $this;
    }

    /**
     * Get productidfrom
     *
     * @return string 
     */
    public function getProductidfrom()
    {
        return $this->productidfrom;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return ProductsRSuppliers
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set supplierid
     *
     * @param \PrismProductsManager\Entity\Suppliers $supplierid
     * @return ProductsRSuppliers
     */
    public function setSupplierid(\PrismProductsManager\Entity\Suppliers $supplierid = null)
    {
        $this->supplierid = $supplierid;

        return $this;
    }

    /**
     * Get supplierid
     *
     * @return \PrismProductsManager\Entity\Suppliers
     */
    public function getSupplierid()
    {
        return $this->supplierid;
    }
}
