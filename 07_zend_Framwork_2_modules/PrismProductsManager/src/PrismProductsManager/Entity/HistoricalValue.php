<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HISTORICAL_Values
 *
 * @ORM\Table(name="HISTORICAL_Values", indexes={@ORM\Index(name="commonAttributeId", columns={"commonAttributeId"}), @ORM\Index(name="productOrVariantId", columns={"productOrVariantId", "commonAttributeId"})})
 * @ORM\Entity(repositoryClass="PrismProductsManager\Entity\Repository\HistoricalValueRepository")
 */
class HistoricalValue
{
    const UPDATE_TYPE_Interface = 1;
    const UPDATE_TYPE_Bulk = 2;
    const UPDATE_TYPE_Scheduled = 3;
    const UPDATE_TYPE_Auto_Date = 4;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $historicalValueId;

    /**
     * @var integer
     *
     * @ORM\Column(name="productOrVariantId", type="integer", nullable=false)
     */
    private $productOrVariantId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="style", type="boolean", nullable=false)
     */
    private $style;

    /**
     * @var string
     *
     * @ORM\Column(name="optionCode", type="string", length=20, nullable=false)
     */
    private $optionCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="userId", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributes
     *
     * @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributes", mappedBy="commonattributeid", cascade={"persist"})
     * @ORM\JoinColumn(name="commonAttributeId", referencedColumnName="commonAttributeId")
     */
    private $commonAttribute = null;

    /**
     * @var string
     *
     * @ORM\Column(name="attributeName", type="string", length=255, nullable=true)
     */
    private $attributeName;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=false)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fromDate", type="datetime", nullable=false)
     */
    private $fromDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="updateType", type="integer", nullable=false)
     */
    private $updateType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var integer
     *
     * @ORM\Column(name="modifiedBy", type="integer", nullable=true)
     */
    private $modifiedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @return int
     */
    public function getHistoricalValueId()
    {
        return $this->historicalValueId;
    }

    /**
     * @param int $historicalValueId
     */
    public function setHistoricalValueId($historicalValueId)
    {
        $this->historicalValueId = $historicalValueId;
    }

    /**
     * @return int
     */
    public function getProductOrVariantId()
    {
        return $this->productOrVariantId;
    }

    /**
     * @param int $productOrVariantId
     */
    public function setProductOrVariantId($productOrVariantId)
    {
        $this->productOrVariantId = $productOrVariantId;
    }

    /**
     * @return boolean
     */
    public function isStyle()
    {
        return $this->style;
    }

    /**
     * @param boolean $style
     */
    public function setStyle($style)
    {
        $this->style = $style;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return CommonAttributes
     */
    public function getCommonAttribute()
    {
        return $this->commonAttribute;
    }

    /**
     * @param CommonAttributes $commonAttribute
     */
    public function setCommonAttribute(CommonAttributes $commonAttribute)
    {
        $this->commonAttribute = $commonAttribute;
    }

    /**
     * @return string
     */
    public function getAttributeName()
    {
        return $this->attributeName;
    }

    /**
     * @param string $attributeName
     */
    public function setAttributeName($attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return \DateTime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * @param \DateTime $fromDate
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;
    }

    /**
     * @return int
     */
    public function getUpdateType()
    {
        return $this->updateType;
    }

    /**
     * @param int $updateType
     */
    public function setUpdateType($updateType)
    {
        $this->updateType = $updateType;
    }

    /**
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     */
    public function setModified(\DateTime $modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return int
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * @param int $modifiedBy
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getOptionCode()
    {
        return $this->optionCode;
    }

    /**
     * @param string $optionCode
     */
    public function setOptionCode($optionCode)
    {
        $this->optionCode = $optionCode;
    }

    /**
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param boolean $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

}
