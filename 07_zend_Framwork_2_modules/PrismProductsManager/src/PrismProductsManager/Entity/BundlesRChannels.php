<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BundlesRChannels
 *
 * @ORM\Table(name="BUNDLES_R_CHANNELS", indexes={@ORM\Index(name="Index 2", columns={"bundleId"}), @ORM\Index(name="Index 3", columns={"channelId"})})
 * @ORM\Entity
 */
class BundlesRChannels
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bundleRChannelId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bundlerchannelid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\Bundles
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Bundles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bundleId", referencedColumnName="bundleId")
     * })
     */
    private $bundleid;

    /**
     * @var \PrismProductsManager\Entity\Channels
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Channels")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="channelId", referencedColumnName="channelId")
     * })
     */
    private $channelid;



    /**
     * Get bundlerchannelid
     *
     * @return integer 
     */
    public function getBundlerchannelid()
    {
        return $this->bundlerchannelid;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return BundlesRChannels
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return BundlesRChannels
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return BundlesRChannels
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set bundleid
     *
     * @param \PrismProductsManager\Entity\Bundles $bundleid
     * @return BundlesRChannels
     */
    public function setBundleid(\PrismProductsManager\Entity\Bundles $bundleid = null)
    {
        $this->bundleid = $bundleid;

        return $this;
    }

    /**
     * Get bundleid
     *
     * @return \PrismProductsManager\Entity\Bundles 
     */
    public function getBundleid()
    {
        return $this->bundleid;
    }

    /**
     * Set channelid
     *
     * @param \PrismProductsManager\Entity\Channels $channelid
     * @return BundlesRChannels
     */
    public function setChannelid(\PrismProductsManager\Entity\Channels $channelid = null)
    {
        $this->channelid = $channelid;

        return $this;
    }

    /**
     * Get channelid
     *
     * @return \PrismProductsManager\Entity\Channels 
     */
    public function getChannelid()
    {
        return $this->channelid;
    }
}
