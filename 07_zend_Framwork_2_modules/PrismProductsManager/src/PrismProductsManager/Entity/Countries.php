<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Countries
 *
 * @ORM\Table(name="COUNTRIES", indexes={@ORM\Index(name="isocode2", columns={"isoCode2"}), @ORM\Index(name="isocode3", columns={"isoCode3"}), @ORM\Index(name="isonum3", columns={"isoNum3"})})
 * @ORM\Entity
 */
class Countries
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="isoCode2", type="string", length=2, nullable=false)
     */
    private $isocode2;

    /**
     * @var string
     *
     * @ORM\Column(name="isoCode3", type="string", length=3, nullable=false)
     */
    private $isocode3;

    /**
     * @var string
     *
     * @ORM\Column(name="isoNum3", type="string", length=3, nullable=false)
     */
    private $isonum3;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=50, nullable=false)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="countryLong", type="string", length=100, nullable=false)
     */
    private $countrylong;

    /**
     * @var string
     *
     * @ORM\Column(name="countryRemark", type="text", nullable=false)
     */
    private $countryremark;

    /**
     * @var string
     *
     * @ORM\Column(name="postcodeRegEx", type="string", length=150, nullable=false)
     */
    private $postcoderegex;

    /**
     * @var boolean
     *
     * @ORM\Column(name="postcodeCheck", type="boolean", nullable=false)
     */
    private $postcodecheck;

    /**
     * @var boolean
     *
     * @ORM\Column(name="postcodeBlank", type="boolean", nullable=false)
     */
    private $postcodeblank;

    /**
     * @var boolean
     *
     * @ORM\Column(name="taxZone", type="boolean", nullable=false)
     */
    private $taxzone;

    /**
     * @var string
     *
     * @ORM\Column(name="taxRegion", type="string", length=2, nullable=false)
     */
    private $taxregion;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isocode2
     *
     * @param string $isocode2
     * @return Countries
     */
    public function setIsocode2($isocode2)
    {
        $this->isocode2 = $isocode2;

        return $this;
    }

    /**
     * Get isocode2
     *
     * @return string 
     */
    public function getIsocode2()
    {
        return $this->isocode2;
    }

    /**
     * Set isocode3
     *
     * @param string $isocode3
     * @return Countries
     */
    public function setIsocode3($isocode3)
    {
        $this->isocode3 = $isocode3;

        return $this;
    }

    /**
     * Get isocode3
     *
     * @return string 
     */
    public function getIsocode3()
    {
        return $this->isocode3;
    }

    /**
     * Set isonum3
     *
     * @param string $isonum3
     * @return Countries
     */
    public function setIsonum3($isonum3)
    {
        $this->isonum3 = $isonum3;

        return $this;
    }

    /**
     * Get isonum3
     *
     * @return string 
     */
    public function getIsonum3()
    {
        return $this->isonum3;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Countries
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set countrylong
     *
     * @param string $countrylong
     * @return Countries
     */
    public function setCountrylong($countrylong)
    {
        $this->countrylong = $countrylong;

        return $this;
    }

    /**
     * Get countrylong
     *
     * @return string 
     */
    public function getCountrylong()
    {
        return $this->countrylong;
    }

    /**
     * Set countryremark
     *
     * @param string $countryremark
     * @return Countries
     */
    public function setCountryremark($countryremark)
    {
        $this->countryremark = $countryremark;

        return $this;
    }

    /**
     * Get countryremark
     *
     * @return string 
     */
    public function getCountryremark()
    {
        return $this->countryremark;
    }

    /**
     * Set postcoderegex
     *
     * @param string $postcoderegex
     * @return Countries
     */
    public function setPostcoderegex($postcoderegex)
    {
        $this->postcoderegex = $postcoderegex;

        return $this;
    }

    /**
     * Get postcoderegex
     *
     * @return string 
     */
    public function getPostcoderegex()
    {
        return $this->postcoderegex;
    }

    /**
     * Set postcodecheck
     *
     * @param boolean $postcodecheck
     * @return Countries
     */
    public function setPostcodecheck($postcodecheck)
    {
        $this->postcodecheck = $postcodecheck;

        return $this;
    }

    /**
     * Get postcodecheck
     *
     * @return boolean 
     */
    public function getPostcodecheck()
    {
        return $this->postcodecheck;
    }

    /**
     * Set postcodeblank
     *
     * @param boolean $postcodeblank
     * @return Countries
     */
    public function setPostcodeblank($postcodeblank)
    {
        $this->postcodeblank = $postcodeblank;

        return $this;
    }

    /**
     * Get postcodeblank
     *
     * @return boolean 
     */
    public function getPostcodeblank()
    {
        return $this->postcodeblank;
    }

    /**
     * Set taxzone
     *
     * @param boolean $taxzone
     * @return Countries
     */
    public function setTaxzone($taxzone)
    {
        $this->taxzone = $taxzone;

        return $this;
    }

    /**
     * Get taxzone
     *
     * @return boolean 
     */
    public function getTaxzone()
    {
        return $this->taxzone;
    }

    /**
     * Set taxregion
     *
     * @param string $taxregion
     * @return Countries
     */
    public function setTaxregion($taxregion)
    {
        $this->taxregion = $taxregion;

        return $this;
    }

    /**
     * Get taxregion
     *
     * @return string 
     */
    public function getTaxregion()
    {
        return $this->taxregion;
    }
}
