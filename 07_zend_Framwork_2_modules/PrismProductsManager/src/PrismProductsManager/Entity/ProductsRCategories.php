<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsRCategories
 *
 * @ORM\Table(name="PRODUCTS_R_CATEGORIES", indexes={@ORM\Index(name="FK_PRODUCTS_R_CATEGORIES_PRODUCTS", columns={"productId"}), @ORM\Index(name="FK_PRODUCTS_R_CATEGORIES_CATEGORIES", columns={"categoryId"})})
 * @ORM\Entity
 */
class ProductsRCategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productCategoryId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productcategoryid;

    /**
     * @var integer
     *
     * @ORM\Column(name="productId", type="integer", nullable=false)
     */
    private $productid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDefault", type="boolean", nullable=false)
     */
    private $isdefault;

    /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="integer", nullable=false)
     */
    private $priority;

    /**
     * @var string
     *
     * @ORM\Column(name="productIdFrom", type="string", nullable=false)
     */
    private $productidfrom;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\Categories
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Categories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryId", referencedColumnName="categoryId")
     * })
     */
    private $categoryid;



    /**
     * Get productcategoryid
     *
     * @return integer 
     */
    public function getProductcategoryid()
    {
        return $this->productcategoryid;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return ProductsRCategories
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set isdefault
     *
     * @param boolean $isdefault
     * @return ProductsRCategories
     */
    public function setIsdefault($isdefault)
    {
        $this->isdefault = $isdefault;

        return $this;
    }

    /**
     * Get isdefault
     *
     * @return boolean 
     */
    public function getIsdefault()
    {
        return $this->isdefault;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     * @return ProductsRCategories
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set productidfrom
     *
     * @param string $productidfrom
     * @return ProductsRCategories
     */
    public function setProductidfrom($productidfrom)
    {
        $this->productidfrom = $productidfrom;

        return $this;
    }

    /**
     * Get productidfrom
     *
     * @return string 
     */
    public function getProductidfrom()
    {
        return $this->productidfrom;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProductsRCategories
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsRCategories
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsRCategories
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set categoryid
     *
     * @param \PrismProductsManager\Entity\Categories $categoryid
     * @return ProductsRCategories
     */
    public function setCategoryid(\PrismProductsManager\Entity\Categories $categoryid = null)
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    /**
     * Get categoryid
     *
     * @return \PrismProductsManager\Entity\Categories 
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }
}
