<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Promotions
 *
 * @ORM\Table(name="PROMOTIONS")
 * @ORM\Entity
 */
class Promotions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="prmotionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $prmotionid;

    /**
     * @var string
     *
     * @ORM\Column(name="promotionName", type="string", length=50, nullable=false)
     */
    private $promotionname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get prmotionid
     *
     * @return integer 
     */
    public function getPrmotionid()
    {
        return $this->prmotionid;
    }

    /**
     * Set promotionname
     *
     * @param string $promotionname
     * @return Promotions
     */
    public function setPromotionname($promotionname)
    {
        $this->promotionname = $promotionname;

        return $this;
    }

    /**
     * Get promotionname
     *
     * @return string 
     */
    public function getPromotionname()
    {
        return $this->promotionname;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Promotions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Promotions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
