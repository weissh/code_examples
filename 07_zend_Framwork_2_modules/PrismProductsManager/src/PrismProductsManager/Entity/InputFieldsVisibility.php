<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputFieldsVisibility
 *
 * @ORM\Table(name="INPUT_Fields_Visibility", indexes={@ORM\Index(name="Index 2", columns={"inputFieldId"}), @ORM\Index(name="Index 3", columns={"commonAttributeValueId"})})
 * @ORM\Entity
 */
class InputFieldsVisibility
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputFieldVisibilityId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputfieldvisibilityid;

    /**
     *
     *  @var \PrismProductsManager\Entity\CommonAttributes
     *
     *  @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributes")
     *  @ORM\JoinColumn (name="commonAttributeId" , referencedColumnName="commonAttributeId")
     */
    private $commonAttribute;

    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributeValueId", type="integer", nullable=false)
     */
    private $commonattributevalueid;

    /**
     * @var \PrismProductsManager\Entity\InputFields
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputFields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputFieldId", referencedColumnName="inputFieldId")
     * })
     */
    private $inputfieldid;

    /**
     * @return CommonAttributes
     */
    public function getCommonAttribute()
    {
        return $this->commonAttribute;
    }

    /**
     * @param CommonAttributes $commonAttribute
     */
    public function setCommonAttribute($commonAttribute)
    {
        $this->commonAttribute = $commonAttribute;
    }


    /**
     * Get inputfieldvisibilityid
     *
     * @return integer 
     */
    public function getInputfieldvisibilityid()
    {
        return $this->inputfieldvisibilityid;
    }

    /**
     * Set commonattributevalueid
     *
     * @param integer $commonattributevalueid
     * @return InputFieldsVisibility
     */
    public function setCommonattributevalueid($commonattributevalueid)
    {
        $this->commonattributevalueid = $commonattributevalueid;

        return $this;
    }

    /**
     * Get commonattributevalueid
     *
     * @return integer 
     */
    public function getCommonattributevalueid()
    {
        return $this->commonattributevalueid;
    }

    /**
     * Set inputfieldid
     *
     * @param \PrismProductsManager\Entity\InputFields $inputfieldid
     * @return InputFieldsVisibility
     */
    public function setInputfieldid(\PrismProductsManager\Entity\InputFields $inputfieldid = null)
    {
        $this->inputfieldid = $inputfieldid;

        return $this;
    }

    /**
     * Get inputfieldid
     *
     * @return \PrismProductsManager\Entity\InputFields 
     */
    public function getInputfieldid()
    {
        return $this->inputfieldid;
    }
}
