<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HistorySummary
 *
 * @ORM\Table(name="HISTORY_SUMMARY")
 * @ORM\Entity
 */
class HistorySummary
{
    /**
     * @var string
     *
     * @ORM\Column(name="attribute", type="string", length=255, nullable=false)
     */
    private $attribute = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="optionOrStyleCode", type="string", length=20, nullable=false)
     */
    private $optionOrStyleCode = '';

    /**
     * @var boolean
     *
     * @ORM\Column(name="isStyle", type="boolean", nullable=false)
     */
    private $isStyle = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="userName", type="string", length=75, nullable=true)
     */
    private $username;

    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributeId", type="integer", nullable=true)
     */
    private $commonAttributeId;

    /**
     * @var string
     *
     * @ORM\Column(name="attributeValue", type="text", nullable=true)
     */
    private $attributeValue;

    /**
     * @var string
     *
     * @ORM\Column(name="attributeValueHash", type="string", length=32, nullable=false)
     */
    private $attributeValueHash = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fromDate", type="string", length=20, nullable=false)
     */
    private $fromDate = '0000-00-00 00:00:00';

    /**
     * @var boolean
     *
     * @ORM\Column(name="updateType", type="integer", nullable=false)
     */
    private $updateType = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=75, nullable=true)
     */
    private $modifiedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=75, nullable=true)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedTrunc", type="string", length=21, nullable=false)
     */
    private $modifiedTrunc = '';

    /**
     * @var string
     *
     * @ORM\Column(name="createdTrunc", type="string", length=21, nullable=false)
     */
    private $createdTrunc = '';



    /**
     * Set attribute
     *
     * @param string $attribute
     *
     * @return HistorySummary
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute
     *
     * @return string
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return HistorySummary
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set optionOrStyleCode
     *
     * @param string $optionOrStyleCode
     *
     * @return HistorySummary
     */
    public function setOptionOrStyleCode($optionOrStyleCode)
    {
        $this->optionOrStyleCode = $optionOrStyleCode;

        return $this;
    }

    /**
     * Get optionOrStyleCode
     *
     * @return string
     */
    public function getOptionOrStyleCode()
    {
        return $this->optionOrStyleCode;
    }

    /**
     * Set isStyle
     *
     * @param boolean $isStyle
     *
     * @return HistorySummary
     */
    public function setIsStyle($isStyle)
    {
        $this->isStyle = $isStyle;

        return $this;
    }

    /**
     * Get isStyle
     *
     * @return boolean
     */
    public function getIsStyle()
    {
        return $this->isStyle;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return HistorySummary
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set commonAttributeId
     *
     * @param integer $commonAttributeId
     *
     * @return HistorySummary
     */
    public function setCommonAttributeId($commonAttributeId)
    {
        $this->commonAttributeId = $commonAttributeId;

        return $this;
    }

    /**
     * Get commonAttributeId
     *
     * @return integer
     */
    public function getCommonAttributeId()
    {
        return $this->commonAttributeId;
    }

    /**
     * Set attributeValue
     *
     * @param string $attributeValue
     *
     * @return HistorySummary
     */
    public function setAttributeValue($attributeValue)
    {
        $this->attributeValue = $attributeValue;

        return $this;
    }

    /**
     * Get attributeValue
     *
     * @return string
     */
    public function getAttributeValue()
    {
        return $this->attributeValue;
    }

    /**
     * Set attributeValueHash
     *
     * @param string $attributeValueHash
     *
     * @return HistorySummary
     */
    public function setAttributeValueHash($attributeValueHash)
    {
        $this->attributeValueHash = $attributeValueHash;

        return $this;
    }

    /**
     * Get attributeValueHash
     *
     * @return string
     */
    public function getAttributeValueHash()
    {
        return $this->attributeValueHash;
    }

    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     *
     * @return HistorySummary
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set updateType
     *
     * @param integer $updateType
     *
     * @return HistorySummary
     */
    public function setUpdateType($updateType)
    {
        $this->updateType = $updateType;

        return $this;
    }

    /**
     * Get updateType
     *
     * @return integer
     */
    public function getUpdateType()
    {
        return $this->updateType;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return HistorySummary
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     *
     * @return HistorySummary
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return HistorySummary
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     *
     * @return HistorySummary
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedTrunc
     *
     * @param string $modifiedTrunc
     *
     * @return HistorySummary
     */
    public function setModifiedTrunc($modifiedTrunc)
    {
        $this->modifiedTrunc = $modifiedTrunc;

        return $this;
    }

    /**
     * Get modifiedTrunc
     *
     * @return string
     */
    public function getModifiedTrunc()
    {
        return $this->modifiedTrunc;
    }

    /**
     * Set createdTrunc
     *
     * @param string $createdTrunc
     *
     * @return HistorySummary
     */
    public function setCreatedTrunc($createdTrunc)
    {
        $this->createdTrunc = $createdTrunc;

        return $this;
    }

    /**
     * Get createdTrunc
     *
     * @return string
     */
    public function getCreatedTrunc()
    {
        return $this->createdTrunc;
    }
}
