<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ColoursDefinitions
 *
 * @ORM\Table(name="COLOURS_Definitions", indexes={@ORM\Index(name="colourDescription", columns={"colourName"}), @ORM\Index(name="languageId", columns={"languageId"}), @ORM\Index(name="colourId", columns={"colourId"})})
 * @ORM\Entity
 */
class ColoursDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="colourDefinitionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $colourdefinitionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="colourName", type="string", length=45, nullable=false)
     */
    private $colourname;

    /**
     * @var \PrismProductsManager\Entity\Colours
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Colours")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="colourId", referencedColumnName="colourId")
     * })
     */
    private $colourid;



    /**
     * Get colourdefinitionid
     *
     * @return integer 
     */
    public function getColourdefinitionid()
    {
        return $this->colourdefinitionid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return ColoursDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set colourname
     *
     * @param string $colourname
     * @return ColoursDefinitions
     */
    public function setColourname($colourname)
    {
        $this->colourname = $colourname;

        return $this;
    }

    /**
     * Get colourname
     *
     * @return string 
     */
    public function getColourname()
    {
        return $this->colourname;
    }

    /**
     * Set colourid
     *
     * @param \PrismProductsManager\Entity\Colours $colourid
     * @return ColoursDefinitions
     */
    public function setColourid(\PrismProductsManager\Entity\Colours $colourid = null)
    {
        $this->colourid = $colourid;

        return $this;
    }

    /**
     * Get colourid
     *
     * @return \PrismProductsManager\Entity\Colours 
     */
    public function getColourid()
    {
        return $this->colourid;
    }
}
