<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputFieldRTablefields
 *
 * @ORM\Table(name="INPUT_Field_R_Tablefields", indexes={@ORM\Index(name="Index 2", columns={"inputTableFieldId"}), @ORM\Index(name="Index 3", columns={"inputFieldId"})})
 * @ORM\Entity
 */
class InputFieldRTablefields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputFieldRTablefieldId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputfieldrtablefieldid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \PrismProductsManager\Entity\InputTablefields
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputTablefields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputTableFieldId", referencedColumnName="inputTablefieldId")
     * })
     */
    private $inputtablefieldid;

    /**
     * @var \PrismProductsManager\Entity\InputFields
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputFields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputFieldId", referencedColumnName="inputFieldId")
     * })
     */
    private $inputfieldid;



    /**
     * Get inputfieldrtablefieldid
     *
     * @return integer 
     */
    public function getInputfieldrtablefieldid()
    {
        return $this->inputfieldrtablefieldid;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputFieldRTablefields
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputFieldRTablefields
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set inputtablefieldid
     *
     * @param \PrismProductsManager\Entity\InputTablefields $inputtablefieldid
     * @return InputFieldRTablefields
     */
    public function setInputtablefieldid(\PrismProductsManager\Entity\InputTablefields $inputtablefieldid = null)
    {
        $this->inputtablefieldid = $inputtablefieldid;

        return $this;
    }

    /**
     * Get inputtablefieldid
     *
     * @return \PrismProductsManager\Entity\InputTablefields 
     */
    public function getInputtablefieldid()
    {
        return $this->inputtablefieldid;
    }

    /**
     * Set inputfieldid
     *
     * @param \PrismProductsManager\Entity\InputFields $inputfieldid
     * @return InputFieldRTablefields
     */
    public function setInputfieldid(\PrismProductsManager\Entity\InputFields $inputfieldid = null)
    {
        $this->inputfieldid = $inputfieldid;

        return $this;
    }

    /**
     * Get inputfieldid
     *
     * @return \PrismProductsManager\Entity\InputFields 
     */
    public function getInputfieldid()
    {
        return $this->inputfieldid;
    }
}
