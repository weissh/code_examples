<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SizesTrousers
 *
 * @ORM\Table(name="SIZES_Trousers")
 * @ORM\Entity
 */
class SizesTrousers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="sizeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sizeid;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="uk", type="string", length=15, nullable=false)
     */
    private $uk;

    /**
     * @var string
     *
     * @ORM\Column(name="us", type="string", length=15, nullable=false)
     */
    private $us;

    /**
     * @var string
     *
     * @ORM\Column(name="eur", type="string", length=15, nullable=false)
     */
    private $eur;

    /**
     * @var boolean
     *
     * @ORM\Column(name="priority", type="boolean", nullable=false)
     */
    private $priority;



    /**
     * Get sizeid
     *
     * @return integer 
     */
    public function getSizeid()
    {
        return $this->sizeid;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return SizesTrousers
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set uk
     *
     * @param string $uk
     * @return SizesTrousers
     */
    public function setUk($uk)
    {
        $this->uk = $uk;

        return $this;
    }

    /**
     * Get uk
     *
     * @return string 
     */
    public function getUk()
    {
        return $this->uk;
    }

    /**
     * Set us
     *
     * @param string $us
     * @return SizesTrousers
     */
    public function setUs($us)
    {
        $this->us = $us;

        return $this;
    }

    /**
     * Get us
     *
     * @return string 
     */
    public function getUs()
    {
        return $this->us;
    }

    /**
     * Set eur
     *
     * @param string $eur
     * @return SizesTrousers
     */
    public function setEur($eur)
    {
        $this->eur = $eur;

        return $this;
    }

    /**
     * Get eur
     *
     * @return string 
     */
    public function getEur()
    {
        return $this->eur;
    }

    /**
     * Set priority
     *
     * @param boolean $priority
     * @return SizesTrousers
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return boolean 
     */
    public function getPriority()
    {
        return $this->priority;
    }
}
