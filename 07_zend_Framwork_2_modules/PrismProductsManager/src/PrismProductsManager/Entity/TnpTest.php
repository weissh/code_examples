<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TnpTest
 *
 * @ORM\Table(name="TNP_TEST")
 * @ORM\Entity
 */
class TnpTest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="SPECTRUMGATEWAYID", type="integer", nullable=false)
     */
    private $spectrumgatewayid;

    /**
     * @var string
     *
     * @ORM\Column(name="OLDACCOUNT", type="string", length=150, nullable=true)
     */
    private $oldaccount;

    /**
     * @var string
     *
     * @ORM\Column(name="NEWACCOUNT", type="string", length=150, nullable=true)
     */
    private $newaccount;

    /**
     * @var string
     *
     * @ORM\Column(name="OLDIBAN", type="string", length=255, nullable=false)
     */
    private $oldiban;

    /**
     * @var string
     *
     * @ORM\Column(name="NEWIBAN", type="string", length=255, nullable=false)
     */
    private $newiban;

    /**
     * @var string
     *
     * @ORM\Column(name="CUSTOMER", type="string", length=255, nullable=false)
     */
    private $customer;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set spectrumgatewayid
     *
     * @param integer $spectrumgatewayid
     * @return TnpTest
     */
    public function setSpectrumgatewayid($spectrumgatewayid)
    {
        $this->spectrumgatewayid = $spectrumgatewayid;

        return $this;
    }

    /**
     * Get spectrumgatewayid
     *
     * @return integer 
     */
    public function getSpectrumgatewayid()
    {
        return $this->spectrumgatewayid;
    }

    /**
     * Set oldaccount
     *
     * @param string $oldaccount
     * @return TnpTest
     */
    public function setOldaccount($oldaccount)
    {
        $this->oldaccount = $oldaccount;

        return $this;
    }

    /**
     * Get oldaccount
     *
     * @return string 
     */
    public function getOldaccount()
    {
        return $this->oldaccount;
    }

    /**
     * Set newaccount
     *
     * @param string $newaccount
     * @return TnpTest
     */
    public function setNewaccount($newaccount)
    {
        $this->newaccount = $newaccount;

        return $this;
    }

    /**
     * Get newaccount
     *
     * @return string 
     */
    public function getNewaccount()
    {
        return $this->newaccount;
    }

    /**
     * Set oldiban
     *
     * @param string $oldiban
     * @return TnpTest
     */
    public function setOldiban($oldiban)
    {
        $this->oldiban = $oldiban;

        return $this;
    }

    /**
     * Get oldiban
     *
     * @return string 
     */
    public function getOldiban()
    {
        return $this->oldiban;
    }

    /**
     * Set newiban
     *
     * @param string $newiban
     * @return TnpTest
     */
    public function setNewiban($newiban)
    {
        $this->newiban = $newiban;

        return $this;
    }

    /**
     * Get newiban
     *
     * @return string 
     */
    public function getNewiban()
    {
        return $this->newiban;
    }

    /**
     * Set customer
     *
     * @param string $customer
     * @return TnpTest
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return string 
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
