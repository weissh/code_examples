<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PromotionRules
 *
 * @ORM\Table(name="PROMOTION_Rules")
 * @ORM\Entity
 */
class PromotionRules
{
    /**
     * @var integer
     *
     * @ORM\Column(name="promotionRuleId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $promotionruleid;

    /**
     * @var integer
     *
     * @ORM\Column(name="promotionId", type="integer", nullable=true)
     */
    private $promotionid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;



    /**
     * Get promotionruleid
     *
     * @return integer 
     */
    public function getPromotionruleid()
    {
        return $this->promotionruleid;
    }

    /**
     * Set promotionid
     *
     * @param integer $promotionid
     * @return PromotionRules
     */
    public function setPromotionid($promotionid)
    {
        $this->promotionid = $promotionid;

        return $this;
    }

    /**
     * Get promotionid
     *
     * @return integer 
     */
    public function getPromotionid()
    {
        return $this->promotionid;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return PromotionRules
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
