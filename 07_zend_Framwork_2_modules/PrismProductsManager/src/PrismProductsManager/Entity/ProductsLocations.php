<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsLocations
 *
 * @ORM\Table(name="PRODUCTS_Locations", indexes={@ORM\Index(name="FK_PRODUCTS_Locations_PRODUCTS", columns={"productId"}), @ORM\Index(name="FK_PRODUCTS_Locations_PRODUCTS_R_ATTRIBUTES", columns={"productAttributeId"})})
 * @ORM\Entity
 */
class ProductsLocations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="locationId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $locationid;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=50, nullable=false)
     */
    private $location;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\Products
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Products")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="productId", referencedColumnName="productId")
     * })
     */
    private $productid;

    /**
     * @var \PrismProductsManager\Entity\ProductsRAttributes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\ProductsRAttributes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="productAttributeId", referencedColumnName="productAttributeId")
     * })
     */
    private $productattributeid;



    /**
     * Get locationid
     *
     * @return integer 
     */
    public function getLocationid()
    {
        return $this->locationid;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return ProductsLocations
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return ProductsLocations
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsLocations
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set productid
     *
     * @param \PrismProductsManager\Entity\Products $productid
     * @return ProductsLocations
     */
    public function setProductid(\PrismProductsManager\Entity\Products $productid = null)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return \PrismProductsManager\Entity\Products 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set productattributeid
     *
     * @param \PrismProductsManager\Entity\ProductsRAttributes $productattributeid
     * @return ProductsLocations
     */
    public function setProductattributeid(\PrismProductsManager\Entity\ProductsRAttributes $productattributeid = null)
    {
        $this->productattributeid = $productattributeid;

        return $this;
    }

    /**
     * Get productattributeid
     *
     * @return \PrismProductsManager\Entity\ProductsRAttributes 
     */
    public function getProductattributeid()
    {
        return $this->productattributeid;
    }
}
