<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputFieldRFieldsets
 *
 * @ORM\Table(name="INPUT_Field_R_Fieldsets", indexes={@ORM\Index(name="Index 2", columns={"inputFieldId"}), @ORM\Index(name="Index 3", columns={"inputFieldsetId"})})
 * @ORM\Entity
 */
class InputFieldRFieldsets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputFieldRFieldsetId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputfieldrfieldsetid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \PrismProductsManager\Entity\InputFields
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputFields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputFieldId", referencedColumnName="inputFieldId")
     * })
     */
    private $inputfieldid;

    /**
     * @var \PrismProductsManager\Entity\InputFieldsets
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputFieldsets")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputFieldsetId", referencedColumnName="inputFieldsetId")
     * })
     */
    private $inputfieldsetid;



    /**
     * Get inputfieldrfieldsetid
     *
     * @return integer 
     */
    public function getInputfieldrfieldsetid()
    {
        return $this->inputfieldrfieldsetid;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputFieldRFieldsets
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputFieldRFieldsets
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set inputfieldid
     *
     * @param \PrismProductsManager\Entity\InputFields $inputfieldid
     * @return InputFieldRFieldsets
     */
    public function setInputfieldid(\PrismProductsManager\Entity\InputFields $inputfieldid = null)
    {
        $this->inputfieldid = $inputfieldid;

        return $this;
    }

    /**
     * Get inputfieldid
     *
     * @return \PrismProductsManager\Entity\InputFields 
     */
    public function getInputfieldid()
    {
        return $this->inputfieldid;
    }

    /**
     * Set inputfieldsetid
     *
     * @param \PrismProductsManager\Entity\InputFieldsets $inputfieldsetid
     * @return InputFieldRFieldsets
     */
    public function setInputfieldsetid(\PrismProductsManager\Entity\InputFieldsets $inputfieldsetid = null)
    {
        $this->inputfieldsetid = $inputfieldsetid;

        return $this;
    }

    /**
     * Get inputfieldsetid
     *
     * @return \PrismProductsManager\Entity\InputFieldsets 
     */
    public function getInputfieldsetid()
    {
        return $this->inputfieldsetid;
    }
}
