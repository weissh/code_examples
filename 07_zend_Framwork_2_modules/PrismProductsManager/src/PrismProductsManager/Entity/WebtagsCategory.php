<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WebtagsCategory
 *
 * @ORM\Table(name="webtags_category", indexes={@ORM\Index(name="parpri", columns={"PARENT_ID", "PRIORITY"}), @ORM\Index(name="setup_id", columns={"setup_id"}), @ORM\Index(name="parname", columns={"PARENT_ID", "NAME"}), @ORM\Index(name="name", columns={"NAME"})})
 * @ORM\Entity
 */
class WebtagsCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NAME", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="PARENT_ID", type="bigint", nullable=false)
     */
    private $parentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="PRIORITY", type="bigint", nullable=false)
     */
    private $priority;

    /**
     * @var boolean
     *
     * @ORM\Column(name="LOCKED", type="boolean", nullable=false)
     */
    private $locked;

    /**
     * @var boolean
     *
     * @ORM\Column(name="LIMITED", type="boolean", nullable=false)
     */
    private $limited;

    /**
     * @var string
     *
     * @ORM\Column(name="setup_id", type="string", length=250, nullable=false)
     */
    private $setupId;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return WebtagsCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     * @return WebtagsCategory
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     * @return WebtagsCategory
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     * @return WebtagsCategory
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean 
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Set limited
     *
     * @param boolean $limited
     * @return WebtagsCategory
     */
    public function setLimited($limited)
    {
        $this->limited = $limited;

        return $this;
    }

    /**
     * Get limited
     *
     * @return boolean 
     */
    public function getLimited()
    {
        return $this->limited;
    }

    /**
     * Set setupId
     *
     * @param string $setupId
     * @return WebtagsCategory
     */
    public function setSetupId($setupId)
    {
        $this->setupId = $setupId;

        return $this;
    }

    /**
     * Get setupId
     *
     * @return string 
     */
    public function getSetupId()
    {
        return $this->setupId;
    }
}
