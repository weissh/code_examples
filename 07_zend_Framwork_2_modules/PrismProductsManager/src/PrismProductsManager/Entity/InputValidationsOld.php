<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputValidationsOld
 *
 * @ORM\Table(name="INPUT_Validations_old")
 * @ORM\Entity
 */
class InputValidationsOld
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputValidationId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputvalidationid;

    /**
     * @var string
     *
     * @ORM\Column(name="inputValidationType", type="string", length=50, nullable=false)
     */
    private $inputvalidationtype;

    /**
     * @var string
     *
     * @ORM\Column(name="pattern", type="string", length=250, nullable=true)
     */
    private $pattern;

    /**
     * @var integer
     *
     * @ORM\Column(name="inputValidationMinValue", type="integer", nullable=true)
     */
    private $inputvalidationminvalue;

    /**
     * @var integer
     *
     * @ORM\Column(name="inputValidationMaxValue", type="integer", nullable=true)
     */
    private $inputvalidationmaxvalue;

    /**
     * @var integer
     *
     * @ORM\Column(name="inputValidationUniqueRecord", type="integer", nullable=false)
     */
    private $inputvalidationuniquerecord;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;



    /**
     * Get inputvalidationid
     *
     * @return integer 
     */
    public function getInputvalidationid()
    {
        return $this->inputvalidationid;
    }

    /**
     * Set inputvalidationtype
     *
     * @param string $inputvalidationtype
     * @return InputValidationsOld
     */
    public function setInputvalidationtype($inputvalidationtype)
    {
        $this->inputvalidationtype = $inputvalidationtype;

        return $this;
    }

    /**
     * Get inputvalidationtype
     *
     * @return string 
     */
    public function getInputvalidationtype()
    {
        return $this->inputvalidationtype;
    }

    /**
     * Set pattern
     *
     * @param string $pattern
     * @return InputValidationsOld
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;

        return $this;
    }

    /**
     * Get pattern
     *
     * @return string 
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * Set inputvalidationminvalue
     *
     * @param integer $inputvalidationminvalue
     * @return InputValidationsOld
     */
    public function setInputvalidationminvalue($inputvalidationminvalue)
    {
        $this->inputvalidationminvalue = $inputvalidationminvalue;

        return $this;
    }

    /**
     * Get inputvalidationminvalue
     *
     * @return integer 
     */
    public function getInputvalidationminvalue()
    {
        return $this->inputvalidationminvalue;
    }

    /**
     * Set inputvalidationmaxvalue
     *
     * @param integer $inputvalidationmaxvalue
     * @return InputValidationsOld
     */
    public function setInputvalidationmaxvalue($inputvalidationmaxvalue)
    {
        $this->inputvalidationmaxvalue = $inputvalidationmaxvalue;

        return $this;
    }

    /**
     * Get inputvalidationmaxvalue
     *
     * @return integer 
     */
    public function getInputvalidationmaxvalue()
    {
        return $this->inputvalidationmaxvalue;
    }

    /**
     * Set inputvalidationuniquerecord
     *
     * @param integer $inputvalidationuniquerecord
     * @return InputValidationsOld
     */
    public function setInputvalidationuniquerecord($inputvalidationuniquerecord)
    {
        $this->inputvalidationuniquerecord = $inputvalidationuniquerecord;

        return $this;
    }

    /**
     * Get inputvalidationuniquerecord
     *
     * @return integer 
     */
    public function getInputvalidationuniquerecord()
    {
        return $this->inputvalidationuniquerecord;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputValidationsOld
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputValidationsOld
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }
}
