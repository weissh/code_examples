<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputTablefields
 *
 * @ORM\Table(name="INPUT_Tablefields")
 * @ORM\Entity
 */
class InputTablefields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputTablefieldId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputtablefieldid;

    /**
     * @var string
     *
     * @ORM\Column(name="table", type="string", length=120, nullable=true)
     */
    private $table;

    /**
     * @var string
     *
     * @ORM\Column(name="column", type="string", length=120, nullable=true)
     */
    private $column;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=80, nullable=true)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;



    /**
     * Get inputtablefieldid
     *
     * @return integer 
     */
    public function getInputtablefieldid()
    {
        return $this->inputtablefieldid;
    }

    /**
     * Set table
     *
     * @param string $table
     * @return InputTablefields
     */
    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }

    /**
     * Get table
     *
     * @return string 
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Set column
     *
     * @param string $column
     * @return InputTablefields
     */
    public function setColumn($column)
    {
        $this->column = $column;

        return $this;
    }

    /**
     * Get column
     *
     * @return string 
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return InputTablefields
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputTablefields
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputTablefields
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }
}
