<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriesMediaExternal
 *
 * @ORM\Table(name="CATEGORIES_Media_External", indexes={@ORM\Index(name="FK_CATEGORIES_Media_External_CATEGORIES_Media", columns={"categoryMediaId"})})
 * @ORM\Entity
 */
class CategoriesMediaExternal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="externalMediaId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $externalmediaid;

    /**
     * @var string
     *
     * @ORM\Column(name="fileName", type="string", length=50, nullable=false)
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=150, nullable=false)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\CategoriesMedia
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CategoriesMedia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryMediaId", referencedColumnName="categoryMediaId")
     * })
     */
    private $categorymediaid;



    /**
     * Get externalmediaid
     *
     * @return integer 
     */
    public function getExternalmediaid()
    {
        return $this->externalmediaid;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return CategoriesMediaExternal
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return CategoriesMediaExternal
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return CategoriesMediaExternal
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return CategoriesMediaExternal
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return CategoriesMediaExternal
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set categorymediaid
     *
     * @param \PrismProductsManager\Entity\CategoriesMedia $categorymediaid
     * @return CategoriesMediaExternal
     */
    public function setCategorymediaid(\PrismProductsManager\Entity\CategoriesMedia $categorymediaid = null)
    {
        $this->categorymediaid = $categorymediaid;

        return $this;
    }

    /**
     * Get categorymediaid
     *
     * @return \PrismProductsManager\Entity\CategoriesMedia 
     */
    public function getCategorymediaid()
    {
        return $this->categorymediaid;
    }
}
