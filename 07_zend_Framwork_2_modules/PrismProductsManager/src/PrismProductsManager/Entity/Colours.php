<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Colours
 *
 * @ORM\Table(name="COLOURS", uniqueConstraints={@ORM\UniqueConstraint(name="colourId", columns={"colourId"}), @ORM\UniqueConstraint(name="colourCode", columns={"colourCode"})})
 * @ORM\Entity
 */
class Colours
{
    /**
     * @var integer
     *
     * @ORM\Column(name="colourId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $colourid;

    /**
     * @var string
     *
     * @ORM\Column(name="colourCode", type="string", length=5, nullable=false)
     */
    private $colourcode;

    /**
     * @var string
     *
     * @ORM\Column(name="hex", type="string", length=7, nullable=false)
     */
    private $hex;



    /**
     * Get colourid
     *
     * @return integer 
     */
    public function getColourid()
    {
        return $this->colourid;
    }

    /**
     * Set colourcode
     *
     * @param string $colourcode
     * @return Colours
     */
    public function setColourcode($colourcode)
    {
        $this->colourcode = $colourcode;

        return $this;
    }

    /**
     * Get colourcode
     *
     * @return string 
     */
    public function getColourcode()
    {
        return $this->colourcode;
    }

    /**
     * Set hex
     *
     * @param string $hex
     * @return Colours
     */
    public function setHex($hex)
    {
        $this->hex = $hex;

        return $this;
    }

    /**
     * Get hex
     *
     * @return string 
     */
    public function getHex()
    {
        return $this->hex;
    }
}
