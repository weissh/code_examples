<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriesRClientsWebsites
 *
 * @ORM\Table(name="CATEGORIES_R_CLIENTS_Websites", indexes={@ORM\Index(name="Index 2", columns={"websiteId"}), @ORM\Index(name="FK_CATEGORIES_R_CLIENTS_Websites_CATEGORIES", columns={"categoryId"})})
 * @ORM\Entity
 */
class CategoriesRClientsWebsites
{
    /**
     * @var integer
     *
     * @ORM\Column(name="categoryWebsiteId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $categorywebsiteid;

    /**
     * @var integer
     *
     * @ORM\Column(name="websiteId", type="integer", nullable=false)
     */
    private $websiteid;

    /**
     * @var \PrismProductsManager\Entity\Categories
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Categories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryId", referencedColumnName="categoryId")
     * })
     */
    private $categoryid;



    /**
     * Get categorywebsiteid
     *
     * @return integer 
     */
    public function getCategorywebsiteid()
    {
        return $this->categorywebsiteid;
    }

    /**
     * Set websiteid
     *
     * @param integer $websiteid
     * @return CategoriesRClientsWebsites
     */
    public function setWebsiteid($websiteid)
    {
        $this->websiteid = $websiteid;

        return $this;
    }

    /**
     * Get websiteid
     *
     * @return integer 
     */
    public function getWebsiteid()
    {
        return $this->websiteid;
    }

    /**
     * Set categoryid
     *
     * @param \PrismProductsManager\Entity\Categories $categoryid
     * @return CategoriesRClientsWebsites
     */
    public function setCategoryid(\PrismProductsManager\Entity\Categories $categoryid = null)
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    /**
     * Get categoryid
     *
     * @return \PrismProductsManager\Entity\Categories 
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }
}
