<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Suppliers
 *
 * @ORM\Table(name="SUPPLIERS")
 * @ORM\Entity
 */
class Suppliers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="supplierId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $supplierid;

    /**
     * @var string
     *
     * @ORM\Column(name="merchandizeController", type="string", length=30, nullable=false)
     */
    private $merchandizecontroller;

    /**
     * @var string
     *
     * @ORM\Column(name="merchandizeInitials", type="string", length=2, nullable=false)
     */
    private $merchandizeinitials;

    /**
     * @var string
     *
     * @ORM\Column(name="merchandizeControllerEmail", type="string", length=50, nullable=false)
     */
    private $merchandizecontrolleremail;

    /**
     * @var string
     *
     * @ORM\Column(name="merchandizeControllerType", type="string", length=1, nullable=false)
     */
    private $merchandizecontrollertype;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=30, nullable=false)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=6, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=45, nullable=false)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="address1", type="string", length=45, nullable=false)
     */
    private $address1;

    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=45, nullable=false)
     */
    private $address2;

    /**
     * @var string
     *
     * @ORM\Column(name="address3", type="string", length=45, nullable=true)
     */
    private $address3;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=45, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=8, nullable=false)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="county", type="string", length=45, nullable=false)
     */
    private $county;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=45, nullable=false)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=45, nullable=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=45, nullable=false)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=150, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get supplierid
     *
     * @return integer 
     */
    public function getSupplierid()
    {
        return $this->supplierid;
    }

    /**
     * Set merchandizecontroller
     *
     * @param string $merchandizecontroller
     * @return Suppliers
     */
    public function setMerchandizecontroller($merchandizecontroller)
    {
        $this->merchandizecontroller = $merchandizecontroller;

        return $this;
    }

    /**
     * Get merchandizecontroller
     *
     * @return string 
     */
    public function getMerchandizecontroller()
    {
        return $this->merchandizecontroller;
    }

    /**
     * Set merchandizeinitials
     *
     * @param string $merchandizeinitials
     * @return Suppliers
     */
    public function setMerchandizeinitials($merchandizeinitials)
    {
        $this->merchandizeinitials = $merchandizeinitials;

        return $this;
    }

    /**
     * Get merchandizeinitials
     *
     * @return string 
     */
    public function getMerchandizeinitials()
    {
        return $this->merchandizeinitials;
    }

    /**
     * Set merchandizecontrolleremail
     *
     * @param string $merchandizecontrolleremail
     * @return Suppliers
     */
    public function setMerchandizecontrolleremail($merchandizecontrolleremail)
    {
        $this->merchandizecontrolleremail = $merchandizecontrolleremail;

        return $this;
    }

    /**
     * Get merchandizecontrolleremail
     *
     * @return string 
     */
    public function getMerchandizecontrolleremail()
    {
        return $this->merchandizecontrolleremail;
    }

    /**
     * Set merchandizecontrollertype
     *
     * @param string $merchandizecontrollertype
     * @return Suppliers
     */
    public function setMerchandizecontrollertype($merchandizecontrollertype)
    {
        $this->merchandizecontrollertype = $merchandizecontrollertype;

        return $this;
    }

    /**
     * Get merchandizecontrollertype
     *
     * @return string 
     */
    public function getMerchandizecontrollertype()
    {
        return $this->merchandizecontrollertype;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return Suppliers
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Suppliers
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return Suppliers
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set address1
     *
     * @param string $address1
     * @return Suppliers
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string 
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     * @return Suppliers
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string 
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set address3
     *
     * @param string $address3
     * @return Suppliers
     */
    public function setAddress3($address3)
    {
        $this->address3 = $address3;

        return $this;
    }

    /**
     * Get address3
     *
     * @return string 
     */
    public function getAddress3()
    {
        return $this->address3;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Suppliers
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Suppliers
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set county
     *
     * @param string $county
     * @return Suppliers
     */
    public function setCounty($county)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * Get county
     *
     * @return string 
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Suppliers
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Suppliers
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Suppliers
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Suppliers
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Suppliers
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Suppliers
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
