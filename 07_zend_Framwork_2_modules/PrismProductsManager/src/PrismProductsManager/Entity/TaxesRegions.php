<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaxesRegions
 *
 * @ORM\Table(name="TAXES_Regions", indexes={@ORM\Index(name="taxRegion", columns={"taxRegion"}), @ORM\Index(name="status", columns={"status"})})
 * @ORM\Entity
 */
class TaxesRegions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="taxRegionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $taxregionid;

    /**
     * @var string
     *
     * @ORM\Column(name="taxRegion", type="string", length=2, nullable=false)
     */
    private $taxregion;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get taxregionid
     *
     * @return integer 
     */
    public function getTaxregionid()
    {
        return $this->taxregionid;
    }

    /**
     * Set taxregion
     *
     * @param string $taxregion
     * @return TaxesRegions
     */
    public function setTaxregion($taxregion)
    {
        $this->taxregion = $taxregion;

        return $this;
    }

    /**
     * Get taxregion
     *
     * @return string 
     */
    public function getTaxregion()
    {
        return $this->taxregion;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TaxesRegions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return TaxesRegions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return TaxesRegions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
