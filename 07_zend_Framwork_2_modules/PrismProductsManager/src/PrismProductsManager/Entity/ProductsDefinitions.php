<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsDefinitions
 *
 * @ORM\Table(name="PRODUCTS_Definitions", indexes={@ORM\Index(name="FK_PRODUCTS_Definitions_PRODUCTS", columns={"productId"})})
 * @ORM\Entity
 */
class ProductsDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productDefinitionsId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productdefinitionsid;

    /**
     * @var integer
     *
     * @ORM\Column(name="productId", type="integer", nullable=false)
     */
    private $productid;

    /**
     * @var string
     *
     * @ORM\Column(name="productIdFrom", type="string", nullable=false)
     */
    private $productidfrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var integer
     *
     * @ORM\Column(name="versionId", type="integer", nullable=false)
     */
    private $versionid;

    /**
     * @var string
     *
     * @ORM\Column(name="versionDescription", type="string", length=255, nullable=false)
     */
    private $versiondescription;

    /**
     * @var string
     *
     * @ORM\Column(name="shortProductName", type="string", length=180, nullable=false)
     */
    private $shortproductname;

    /**
     * @var string
     *
     * @ORM\Column(name="productName", type="string", length=250, nullable=false)
     */
    private $productname;

    /**
     * @var string
     *
     * @ORM\Column(name="urlName", type="string", length=250, nullable=false)
     */
    private $urlname;

    /**
     * @var string
     *
     * @ORM\Column(name="metaTitle", type="string", length=250, nullable=false)
     */
    private $metatitle;

    /**
     * @var string
     *
     * @ORM\Column(name="metaDescription", type="string", length=250, nullable=false)
     */
    private $metadescription;

    /**
     * @var string
     *
     * @ORM\Column(name="metaKeyword", type="string", length=250, nullable=false)
     */
    private $metakeyword;

    /**
     * @var string
     *
     * @ORM\Column(name="shortDescription", type="string", length=250, nullable=false)
     */
    private $shortdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="longDescription", type="text", nullable=false)
     */
    private $longdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="misSpells", type="string", length=200, nullable=false)
     */
    private $misspells;

    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=200, nullable=false)
     */
    private $tags;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="makeLive", type="datetime", nullable=true)
     */
    private $makelive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;



    /**
     * Get productdefinitionsid
     *
     * @return integer 
     */
    public function getProductdefinitionsid()
    {
        return $this->productdefinitionsid;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return ProductsDefinitions
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set productidfrom
     *
     * @param string $productidfrom
     * @return ProductsDefinitions
     */
    public function setProductidfrom($productidfrom)
    {
        $this->productidfrom = $productidfrom;

        return $this;
    }

    /**
     * Get productidfrom
     *
     * @return string 
     */
    public function getProductidfrom()
    {
        return $this->productidfrom;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return ProductsDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set versionid
     *
     * @param integer $versionid
     * @return ProductsDefinitions
     */
    public function setVersionid($versionid)
    {
        $this->versionid = $versionid;

        return $this;
    }

    /**
     * Get versionid
     *
     * @return integer 
     */
    public function getVersionid()
    {
        return $this->versionid;
    }

    /**
     * Set versiondescription
     *
     * @param string $versiondescription
     * @return ProductsDefinitions
     */
    public function setVersiondescription($versiondescription)
    {
        $this->versiondescription = $versiondescription;

        return $this;
    }

    /**
     * Get versiondescription
     *
     * @return string 
     */
    public function getVersiondescription()
    {
        return $this->versiondescription;
    }

    /**
     * Set shortproductname
     *
     * @param string $shortproductname
     * @return ProductsDefinitions
     */
    public function setShortproductname($shortproductname)
    {
        $this->shortproductname = $shortproductname;

        return $this;
    }

    /**
     * Get shortproductname
     *
     * @return string 
     */
    public function getShortproductname()
    {
        return $this->shortproductname;
    }

    /**
     * Set productname
     *
     * @param string $productname
     * @return ProductsDefinitions
     */
    public function setProductname($productname)
    {
        $this->productname = $productname;

        return $this;
    }

    /**
     * Get productname
     *
     * @return string 
     */
    public function getProductname()
    {
        return $this->productname;
    }

    /**
     * Set urlname
     *
     * @param string $urlname
     * @return ProductsDefinitions
     */
    public function setUrlname($urlname)
    {
        $this->urlname = $urlname;

        return $this;
    }

    /**
     * Get urlname
     *
     * @return string 
     */
    public function getUrlname()
    {
        return $this->urlname;
    }

    /**
     * Set metatitle
     *
     * @param string $metatitle
     * @return ProductsDefinitions
     */
    public function setMetatitle($metatitle)
    {
        $this->metatitle = $metatitle;

        return $this;
    }

    /**
     * Get metatitle
     *
     * @return string 
     */
    public function getMetatitle()
    {
        return $this->metatitle;
    }

    /**
     * Set metadescription
     *
     * @param string $metadescription
     * @return ProductsDefinitions
     */
    public function setMetadescription($metadescription)
    {
        $this->metadescription = $metadescription;

        return $this;
    }

    /**
     * Get metadescription
     *
     * @return string 
     */
    public function getMetadescription()
    {
        return $this->metadescription;
    }

    /**
     * Set metakeyword
     *
     * @param string $metakeyword
     * @return ProductsDefinitions
     */
    public function setMetakeyword($metakeyword)
    {
        $this->metakeyword = $metakeyword;

        return $this;
    }

    /**
     * Get metakeyword
     *
     * @return string 
     */
    public function getMetakeyword()
    {
        return $this->metakeyword;
    }

    /**
     * Set shortdescription
     *
     * @param string $shortdescription
     * @return ProductsDefinitions
     */
    public function setShortdescription($shortdescription)
    {
        $this->shortdescription = $shortdescription;

        return $this;
    }

    /**
     * Get shortdescription
     *
     * @return string 
     */
    public function getShortdescription()
    {
        return $this->shortdescription;
    }

    /**
     * Set longdescription
     *
     * @param string $longdescription
     * @return ProductsDefinitions
     */
    public function setLongdescription($longdescription)
    {
        $this->longdescription = $longdescription;

        return $this;
    }

    /**
     * Get longdescription
     *
     * @return string 
     */
    public function getLongdescription()
    {
        return $this->longdescription;
    }

    /**
     * Set misspells
     *
     * @param string $misspells
     * @return ProductsDefinitions
     */
    public function setMisspells($misspells)
    {
        $this->misspells = $misspells;

        return $this;
    }

    /**
     * Get misspells
     *
     * @return string 
     */
    public function getMisspells()
    {
        return $this->misspells;
    }

    /**
     * Set tags
     *
     * @param string $tags
     * @return ProductsDefinitions
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return string 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProductsDefinitions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set makelive
     *
     * @param \DateTime $makelive
     * @return ProductsDefinitions
     */
    public function setMakelive($makelive)
    {
        $this->makelive = $makelive;

        return $this;
    }

    /**
     * Get makelive
     *
     * @return \DateTime 
     */
    public function getMakelive()
    {
        return $this->makelive;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsDefinitions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsDefinitions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
