<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ColoursGroupsDefinitions
 *
 * @ORM\Table(name="COLOURS_Groups_Definitions", indexes={@ORM\Index(name="languageId", columns={"languageId"}), @ORM\Index(name="name", columns={"name"}), @ORM\Index(name="colourGroupId", columns={"colourGroupId"})})
 * @ORM\Entity
 */
class ColoursGroupsDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="colourGroupDefinitionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $colourgroupdefinitionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var \PrismProductsManager\Entity\ColoursGroups
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\ColoursGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="colourGroupId", referencedColumnName="colourGroupId")
     * })
     */
    private $colourgroupid;



    /**
     * Get colourgroupdefinitionid
     *
     * @return integer 
     */
    public function getColourgroupdefinitionid()
    {
        return $this->colourgroupdefinitionid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return ColoursGroupsDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ColoursGroupsDefinitions
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set colourgroupid
     *
     * @param \PrismProductsManager\Entity\ColoursGroups $colourgroupid
     * @return ColoursGroupsDefinitions
     */
    public function setColourgroupid(\PrismProductsManager\Entity\ColoursGroups $colourgroupid = null)
    {
        $this->colourgroupid = $colourgroupid;

        return $this;
    }

    /**
     * Get colourgroupid
     *
     * @return \PrismProductsManager\Entity\ColoursGroups 
     */
    public function getColourgroupid()
    {
        return $this->colourgroupid;
    }
}
