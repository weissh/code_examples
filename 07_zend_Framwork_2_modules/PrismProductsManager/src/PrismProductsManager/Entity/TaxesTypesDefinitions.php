<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaxesTypesDefinitions
 *
 * @ORM\Table(name="TAXES_Types_Definitions", indexes={@ORM\Index(name="languageId", columns={"languageId"}), @ORM\Index(name="name", columns={"name"}), @ORM\Index(name="languageId_name", columns={"languageId", "name"}), @ORM\Index(name="taxTypeId", columns={"taxTypeId"})})
 * @ORM\Entity
 */
class TaxesTypesDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="taxTypeDefinitionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $taxtypedefinitionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\TaxesTypes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\TaxesTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="taxTypeId", referencedColumnName="taxTypeId")
     * })
     */
    private $taxtypeid;



    /**
     * Get taxtypedefinitionid
     *
     * @return integer 
     */
    public function getTaxtypedefinitionid()
    {
        return $this->taxtypedefinitionid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return TaxesTypesDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TaxesTypesDefinitions
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TaxesTypesDefinitions
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TaxesTypesDefinitions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return TaxesTypesDefinitions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return TaxesTypesDefinitions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set taxtypeid
     *
     * @param \PrismProductsManager\Entity\TaxesTypes $taxtypeid
     * @return TaxesTypesDefinitions
     */
    public function setTaxtypeid(\PrismProductsManager\Entity\TaxesTypes $taxtypeid = null)
    {
        $this->taxtypeid = $taxtypeid;

        return $this;
    }

    /**
     * Get taxtypeid
     *
     * @return \PrismProductsManager\Entity\TaxesTypes 
     */
    public function getTaxtypeid()
    {
        return $this->taxtypeid;
    }
}
