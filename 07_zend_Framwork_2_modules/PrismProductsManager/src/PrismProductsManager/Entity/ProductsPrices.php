<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsPrices
 *
 * @ORM\Table(name="PRODUCTS_Prices", indexes={@ORM\Index(name="FK_PRODUCTS_Prices_PRODUCTS", columns={"productIdFrom"}), @ORM\Index(name="regionId", columns={"regionId"}), @ORM\Index(name="currencyId", columns={"currencyId"}), @ORM\Index(name="productAttributeId", columns={"productOrVariantId"})})
 * @ORM\Entity
 */
class ProductsPrices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productPriceId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productpriceid;

    /**
     * @var string
     *
     * @ORM\Column(name="productIdFrom", type="string", nullable=true)
     */
    private $productidfrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="productOrVariantId", type="integer", nullable=true)
     */
    private $productorvariantid;

    /**
     * @var integer
     *
     * @ORM\Column(name="regionId", type="integer", nullable=false)
     */
    private $regionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="currencyId", type="integer", nullable=false)
     */
    private $currencyid;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="offerPrice", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $offerprice;

    /**
     * @var string
     *
     * @ORM\Column(name="nowPrice", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $nowprice;

    /**
     * @var string
     *
     * @ORM\Column(name="wasPrice", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $wasprice;

    /**
     * @var string
     *
     * @ORM\Column(name="costPrice", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $costprice;

    /**
     * @var string
     *
     * @ORM\Column(name="tradingCostPrice", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $tradingcostprice;

    /**
     * @var string
     *
     * @ORM\Column(name="wholesalePrice", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $wholesaleprice;

    /**
     * @var string
     *
     * @ORM\Column(name="freightPrice", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $freightprice;

    /**
     * @var string
     *
     * @ORM\Column(name="dutyPrice", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $dutyprice;

    /**
     * @var string
     *
     * @ORM\Column(name="designPrice", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $designprice;

    /**
     * @var string
     *
     * @ORM\Column(name="packagingPrice", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $packagingprice;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get productpriceid
     *
     * @return integer 
     */
    public function getProductpriceid()
    {
        return $this->productpriceid;
    }

    /**
     * Set productidfrom
     *
     * @param string $productidfrom
     * @return ProductsPrices
     */
    public function setProductidfrom($productidfrom)
    {
        $this->productidfrom = $productidfrom;

        return $this;
    }

    /**
     * Get productidfrom
     *
     * @return string 
     */
    public function getProductidfrom()
    {
        return $this->productidfrom;
    }

    /**
     * Set productorvariantid
     *
     * @param integer $productorvariantid
     * @return ProductsPrices
     */
    public function setProductorvariantid($productorvariantid)
    {
        $this->productorvariantid = $productorvariantid;

        return $this;
    }

    /**
     * Get productorvariantid
     *
     * @return integer 
     */
    public function getProductorvariantid()
    {
        return $this->productorvariantid;
    }

    /**
     * Set regionid
     *
     * @param integer $regionid
     * @return ProductsPrices
     */
    public function setRegionid($regionid)
    {
        $this->regionid = $regionid;

        return $this;
    }

    /**
     * Get regionid
     *
     * @return integer 
     */
    public function getRegionid()
    {
        return $this->regionid;
    }

    /**
     * Set currencyid
     *
     * @param integer $currencyid
     * @return ProductsPrices
     */
    public function setCurrencyid($currencyid)
    {
        $this->currencyid = $currencyid;

        return $this;
    }

    /**
     * Get currencyid
     *
     * @return integer 
     */
    public function getCurrencyid()
    {
        return $this->currencyid;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return ProductsPrices
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set offerprice
     *
     * @param string $offerprice
     * @return ProductsPrices
     */
    public function setOfferprice($offerprice)
    {
        $this->offerprice = $offerprice;

        return $this;
    }

    /**
     * Get offerprice
     *
     * @return string 
     */
    public function getOfferprice()
    {
        return $this->offerprice;
    }

    /**
     * Set nowprice
     *
     * @param string $nowprice
     * @return ProductsPrices
     */
    public function setNowprice($nowprice)
    {
        $this->nowprice = $nowprice;

        return $this;
    }

    /**
     * Get nowprice
     *
     * @return string 
     */
    public function getNowprice()
    {
        return $this->nowprice;
    }

    /**
     * Set wasprice
     *
     * @param string $wasprice
     * @return ProductsPrices
     */
    public function setWasprice($wasprice)
    {
        $this->wasprice = $wasprice;

        return $this;
    }

    /**
     * Get wasprice
     *
     * @return string 
     */
    public function getWasprice()
    {
        return $this->wasprice;
    }

    /**
     * Set costprice
     *
     * @param string $costprice
     * @return ProductsPrices
     */
    public function setCostprice($costprice)
    {
        $this->costprice = $costprice;

        return $this;
    }

    /**
     * Get costprice
     *
     * @return string 
     */
    public function getCostprice()
    {
        return $this->costprice;
    }

    /**
     * Set tradingcostprice
     *
     * @param string $tradingcostprice
     * @return ProductsPrices
     */
    public function setTradingcostprice($tradingcostprice)
    {
        $this->tradingcostprice = $tradingcostprice;

        return $this;
    }

    /**
     * Get tradingcostprice
     *
     * @return string 
     */
    public function getTradingcostprice()
    {
        return $this->tradingcostprice;
    }

    /**
     * Set wholesaleprice
     *
     * @param string $wholesaleprice
     * @return ProductsPrices
     */
    public function setWholesaleprice($wholesaleprice)
    {
        $this->wholesaleprice = $wholesaleprice;

        return $this;
    }

    /**
     * Get wholesaleprice
     *
     * @return string 
     */
    public function getWholesaleprice()
    {
        return $this->wholesaleprice;
    }

    /**
     * Set freightprice
     *
     * @param string $freightprice
     * @return ProductsPrices
     */
    public function setFreightprice($freightprice)
    {
        $this->freightprice = $freightprice;

        return $this;
    }

    /**
     * Get freightprice
     *
     * @return string 
     */
    public function getFreightprice()
    {
        return $this->freightprice;
    }

    /**
     * Set dutyprice
     *
     * @param string $dutyprice
     * @return ProductsPrices
     */
    public function setDutyprice($dutyprice)
    {
        $this->dutyprice = $dutyprice;

        return $this;
    }

    /**
     * Get dutyprice
     *
     * @return string 
     */
    public function getDutyprice()
    {
        return $this->dutyprice;
    }

    /**
     * Set designprice
     *
     * @param string $designprice
     * @return ProductsPrices
     */
    public function setDesignprice($designprice)
    {
        $this->designprice = $designprice;

        return $this;
    }

    /**
     * Get designprice
     *
     * @return string 
     */
    public function getDesignprice()
    {
        return $this->designprice;
    }

    /**
     * Set packagingprice
     *
     * @param string $packagingprice
     * @return ProductsPrices
     */
    public function setPackagingprice($packagingprice)
    {
        $this->packagingprice = $packagingprice;

        return $this;
    }

    /**
     * Get packagingprice
     *
     * @return string 
     */
    public function getPackagingprice()
    {
        return $this->packagingprice;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProductsPrices
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsPrices
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsPrices
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
