<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MediaMissing
 *
 * @ORM\Table(name="Media_Missing")
 * @ORM\Entity
 */
class MediaMissing
{
    /**
     * @var integer
     *
     * @ORM\Column(name="mediaMissingId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $mediamissingid;

    /**
     * @var integer
     *
     * @ORM\Column(name="mediaFolderId", type="integer", nullable=false)
     */
    private $mediafolderid;

    /**
     * @var integer
     *
     * @ORM\Column(name="variantFolderId", type="integer", nullable=false)
     */
    private $variantfolderid;



    /**
     * Get mediamissingid
     *
     * @return integer 
     */
    public function getMediamissingid()
    {
        return $this->mediamissingid;
    }

    /**
     * Set mediafolderid
     *
     * @param integer $mediafolderid
     * @return MediaMissing
     */
    public function setMediafolderid($mediafolderid)
    {
        $this->mediafolderid = $mediafolderid;

        return $this;
    }

    /**
     * Get mediafolderid
     *
     * @return integer 
     */
    public function getMediafolderid()
    {
        return $this->mediafolderid;
    }

    /**
     * Set variantfolderid
     *
     * @param integer $variantfolderid
     * @return MediaMissing
     */
    public function setVariantfolderid($variantfolderid)
    {
        $this->variantfolderid = $variantfolderid;

        return $this;
    }

    /**
     * Get variantfolderid
     *
     * @return integer 
     */
    public function getVariantfolderid()
    {
        return $this->variantfolderid;
    }
}
