<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsTypesDefinitions
 *
 * @ORM\Table(name="PRODUCTS_Types_Definitions", indexes={@ORM\Index(name="languageId", columns={"languageId"}), @ORM\Index(name="name", columns={"name"}), @ORM\Index(name="productTypeId", columns={"productTypeId"}), @ORM\Index(name="languageId_productTypeId_name", columns={"languageId", "productTypeId", "name"})})
 * @ORM\Entity
 */
class ProductsTypesDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productTypeDefinitionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $producttypedefinitionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=20, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\ProductsTypes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\ProductsTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="productTypeId", referencedColumnName="productTypeId")
     * })
     */
    private $producttypeid;



    /**
     * Get producttypedefinitionid
     *
     * @return integer 
     */
    public function getProducttypedefinitionid()
    {
        return $this->producttypedefinitionid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return ProductsTypesDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProductsTypesDefinitions
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ProductsTypesDefinitions
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsTypesDefinitions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsTypesDefinitions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set producttypeid
     *
     * @param \PrismProductsManager\Entity\ProductsTypes $producttypeid
     * @return ProductsTypesDefinitions
     */
    public function setProducttypeid(\PrismProductsManager\Entity\ProductsTypes $producttypeid = null)
    {
        $this->producttypeid = $producttypeid;

        return $this;
    }

    /**
     * Get producttypeid
     *
     * @return \PrismProductsManager\Entity\ProductsTypes 
     */
    public function getProducttypeid()
    {
        return $this->producttypeid;
    }
}
