<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributesSpectrumMappingOverride
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES_Spectrum_Mapping_Override", indexes={@ORM\Index(name="commonAttributeSpectrumMappingId", columns={"commonAttributeSpectrumMappingId"}), @ORM\Index(name="commonAttributeId", columns={"commonAttributeId"}), @ORM\Index(name="enabledForCommonAttributeId", columns={"enabledForCommonAttributeId"}), @ORM\Index(name="enabledForCommonAttributeValueId", columns={"enabledForCommonAttributeValueId"})})
 * @ORM\Entity
 */
class CommonAttributesSpectrumMappingOverride
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     *  @var \PrismProductsManager\Entity\CommonAttributesSpectrumMapping
     *
     *  @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesSpectrumMapping", inversedBy="commonAttributeSpectrumMappingId", cascade={"persist"})
     *  @ORM\JoinColumns({
     *    @ORM\JoinColumn(name="commonAttributeSpectrumMappingId", referencedColumnName="commonAttributeSpectrumMappingId")
     *  })
     */
    private $commonAttributeSpectrumMapping;

    /**
     *
     *  @var \PrismProductsManager\Entity\CommonAttributes
     *
     *  @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributes", inversedBy="commonattributeid", cascade={"persist"})
     *  @ORM\JoinColumns({
     *    @ORM\JoinColumn(name="commonAttributeId", referencedColumnName="commonAttributeId")
     *  })
     */
    private $commonAttribute;

    /**
     *
     *  @var \PrismProductsManager\Entity\CommonAttributes
     *
     *  @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributes", inversedBy="commonattributeid", cascade={"persist"})
     *  @ORM\JoinColumns({
     *    @ORM\JoinColumn(name="enabledForCommonAttributeId", referencedColumnName="commonAttributeId")
     *  })
     */
    private $enabledForCommonAttribute;

    /**
     *
     *  @var \PrismProductsManager\Entity\CommonAttributesValues
     *
     *  @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesValues", inversedBy="commonAttributeValueId", cascade={"persist"})
     *  @ORM\JoinColumns({
     *    @ORM\JoinColumn(name="enabledForCommonAttributeValueId", referencedColumnName="commonAttributeValueId")
     *  })
     */
    private $enabledForCommonAttributeValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return CommonAttributesSpectrumMapping
     */
    public function getCommonAttributeSpectrumMapping()
    {
        return $this->commonAttributeSpectrumMapping;
    }

    /**
     * @param CommonAttributesSpectrumMapping $commonAttributeSpectrumMapping
     */
    public function setCommonAttributeSpectrumMapping(CommonAttributesSpectrumMapping $commonAttributeSpectrumMapping)
    {
        $this->commonAttributeSpectrumMapping = $commonAttributeSpectrumMapping;
    }

    /**
     * @return CommonAttributes
     */
    public function getCommonAttribute()
    {
        return $this->commonAttribute;
    }

    /**
     * @param CommonAttributes $commonAttribute
     */
    public function setCommonAttribute(CommonAttributes $commonAttribute)
    {
        $this->commonAttribute = $commonAttribute;
    }

    /**
     * @return CommonAttributes
     */
    public function getEnabledForCommonAttribute()
    {
        return $this->enabledForCommonAttribute;
    }

    /**
     * @param CommonAttributes $enabledForCommonAttribute
     */
    public function setEnabledForCommonAttribute(CommonAttributes $enabledForCommonAttribute)
    {
        $this->enabledForCommonAttribute = $enabledForCommonAttribute;
    }

    /**
     * @return CommonAttributesValues
     */
    public function getEnabledForCommonAttributeValue()
    {
        return $this->enabledForCommonAttributeValue;
    }

    /**
     * @param CommonAttributesValues $enabledForCommonAttributeValue
     */
    public function setEnabledForCommonAttributeValue(CommonAttributesValues $enabledForCommonAttributeValue)
    {
        $this->enabledForCommonAttributeValue = $enabledForCommonAttributeValue;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

}