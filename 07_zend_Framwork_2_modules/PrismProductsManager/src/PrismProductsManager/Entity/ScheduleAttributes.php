<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ScheduleAttributes
 *
 * @ORM\Table(name="SCHEDULE_ATTRIBUTES", indexes={@ORM\Index(name="FK_SCHEDULE_ATTRIBUTES_SCHEDULES", columns={"scheduleId"})})
 * @ORM\Entity
 */
class ScheduleAttributes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="scheduleAttributeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $scheduleAttributeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="attributeId", type="integer", nullable=false)
     */
    private $attributeId;

    /**
     * @var string
     *
     * @ORM\Column(name="attributeValue", type="string", length=75, nullable=true)
     */
    private $attributeValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \PrismProductsManager\Entity\Schedules
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Schedules")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="scheduleId", referencedColumnName="scheduleId")
     * })
     */
    private $scheduleId;

    /**
     * @return int
     */
    public function getScheduleAttributeId()
    {
        return $this->scheduleAttributeId;
    }

    /**
     * @return int
     */
    public function getAttributeId()
    {
        return $this->attributeId;
    }

    /**
     * @param int $attributeId
     */
    public function setAttributeId($attributeId)
    {
        $this->attributeId = $attributeId;
    }

    /**
     * @return string
     */
    public function getAttributeValue()
    {
        return $this->attributeValue;
    }

    /**
     * @param string $attributeValue
     */
    public function setAttributeValue($attributeValue)
    {
        $this->attributeValue = $attributeValue;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return Schedules
     */
    public function getScheduleId()
    {
        return $this->scheduleId;
    }

    /**
     * @param Schedules $scheduleId
     */
    public function setScheduleId($scheduleId)
    {
        $this->scheduleId = $scheduleId;
    }



}
