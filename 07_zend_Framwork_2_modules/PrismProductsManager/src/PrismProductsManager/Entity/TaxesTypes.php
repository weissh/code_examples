<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaxesTypes
 *
 * @ORM\Table(name="TAXES_Types", indexes={@ORM\Index(name="status", columns={"status"})})
 * @ORM\Entity
 */
class TaxesTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="taxTypeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $taxtypeid;

    /**
     * @var integer
     *
     * @ORM\Column(name="calculateFrom", type="integer", nullable=false)
     */
    private $calculatefrom;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get taxtypeid
     *
     * @return integer 
     */
    public function getTaxtypeid()
    {
        return $this->taxtypeid;
    }

    /**
     * Set calculatefrom
     *
     * @param integer $calculatefrom
     * @return TaxesTypes
     */
    public function setCalculatefrom($calculatefrom)
    {
        $this->calculatefrom = $calculatefrom;

        return $this;
    }

    /**
     * Get calculatefrom
     *
     * @return integer 
     */
    public function getCalculatefrom()
    {
        return $this->calculatefrom;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return TaxesTypes
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TaxesTypes
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return TaxesTypes
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return TaxesTypes
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
