<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SizesSwimshortsInfo
 *
 * @ORM\Table(name="SIZES_SwimShorts_Info", indexes={@ORM\Index(name="Index 2", columns={"size"})})
 * @ORM\Entity
 */
class SizesSwimshortsInfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="sizeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sizeid;

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=10, nullable=false)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="waistInch", type="string", length=11, nullable=false)
     */
    private $waistinch;

    /**
     * @var string
     *
     * @ORM\Column(name="waistCm", type="string", length=11, nullable=false)
     */
    private $waistcm;



    /**
     * Get sizeid
     *
     * @return integer 
     */
    public function getSizeid()
    {
        return $this->sizeid;
    }

    /**
     * Set size
     *
     * @param string $size
     * @return SizesSwimshortsInfo
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set waistinch
     *
     * @param string $waistinch
     * @return SizesSwimshortsInfo
     */
    public function setWaistinch($waistinch)
    {
        $this->waistinch = $waistinch;

        return $this;
    }

    /**
     * Get waistinch
     *
     * @return string 
     */
    public function getWaistinch()
    {
        return $this->waistinch;
    }

    /**
     * Set waistcm
     *
     * @param string $waistcm
     * @return SizesSwimshortsInfo
     */
    public function setWaistcm($waistcm)
    {
        $this->waistcm = $waistcm;

        return $this;
    }

    /**
     * Get waistcm
     *
     * @return string 
     */
    public function getWaistcm()
    {
        return $this->waistcm;
    }
}
