<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsRWorkflows
 *
 * @ORM\Table(name="PRODUCTS_R_WORKFLOWS", indexes={@ORM\Index(name="FK_WORKFLOWS_WORKFLOWS", columns={"workflowId"}), @ORM\Index(name="Index 2", columns={"productOrVariantId", "productIdFrom"})})
 * @ORM\Entity
 */
class ProductsRWorkflows
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productRWorkflowId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productrworkflowid;

    /**
     * @var string
     *
     * @ORM\Column(name="productIdFrom", type="string", length=75, nullable=false)
     */
    private $productidfrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="productOrVariantId", type="integer", nullable=false)
     */
    private $productorvariantid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     * @var \PrismProductsManager\Entity\Workflows
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Workflows")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="workflowId", referencedColumnName="workflowId")
     * })
     */
    private $workflowid;



    /**
     * Get productrworkflowid
     *
     * @return integer 
     */
    public function getProductrworkflowid()
    {
        return $this->productrworkflowid;
    }

    /**
     * Set productidfrom
     *
     * @param string $productidfrom
     * @return ProductsRWorkflows
     */
    public function setProductidfrom($productidfrom)
    {
        $this->productidfrom = $productidfrom;

        return $this;
    }

    /**
     * Get productidfrom
     *
     * @return string 
     */
    public function getProductidfrom()
    {
        return $this->productidfrom;
    }

    /**
     * Set productorvariantid
     *
     * @param integer $productorvariantid
     * @return ProductsRWorkflows
     */
    public function setProductorvariantid($productorvariantid)
    {
        $this->productorvariantid = $productorvariantid;

        return $this;
    }

    /**
     * Get productorvariantid
     *
     * @return integer 
     */
    public function getProductorvariantid()
    {
        return $this->productorvariantid;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return ProductsRWorkflows
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return ProductsRWorkflows
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set workflowid
     *
     * @param \PrismProductsManager\Entity\Workflows $workflowid
     * @return ProductsRWorkflows
     */
    public function setWorkflowid(\PrismProductsManager\Entity\Workflows $workflowid = null)
    {
        $this->workflowid = $workflowid;

        return $this;
    }

    /**
     * Get workflowid
     *
     * @return \PrismProductsManager\Entity\Workflows 
     */
    public function getWorkflowid()
    {
        return $this->workflowid;
    }
}
