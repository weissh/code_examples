<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AttributesGroups
 *
 * @ORM\Table(name="ATTRIBUTES_Groups", uniqueConstraints={@ORM\UniqueConstraint(name="attributeGroupId", columns={"attributeGroupId"})}, indexes={@ORM\Index(name="groupParentId", columns={"groupParentId"}), @ORM\Index(name="isMarketingGroup", columns={"groupType"})})
 * @ORM\Entity
 */
class AttributesGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="attributeGroupId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $attributegroupid;

    /**
     * @var integer
     *
     * @ORM\Column(name="groupParentId", type="integer", nullable=false)
     */
    private $groupparentid;

    /**
     * @var string
     *
     * @ORM\Column(name="groupCode", type="string", length=5, nullable=false)
     */
    private $groupcode;

    /**
     * @var string
     *
     * @ORM\Column(name="sourceTable", type="string", length=100, nullable=false)
     */
    private $sourcetable;

    /**
     * @var string
     *
     * @ORM\Column(name="groupType", type="string", nullable=false)
     */
    private $grouptype;

    /**
     * @var string
     *
     * @ORM\Column(name="viewAs", type="string", nullable=false)
     */
    private $viewas;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usedForFiltering", type="boolean", nullable=false)
     */
    private $usedforfiltering;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usedForGrouping", type="boolean", nullable=false)
     */
    private $usedforgrouping;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="priority", type="boolean", nullable=false)
     */
    private $priority;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     *
     *  @var \PrismProductsManager\Entity\Attributes
     *
     *  @ORM\OneToMany(targetEntity="PrismProductsManager\Entity\Attributes", mappedBy="attributegroupid")
     */
    private $attributes;

    /**
     *
     *  @var \PrismProductsManager\Entity\AttributesGroupDefinitions
     *
     *  @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\AttributesGroupDefinitions", mappedBy="attributegroupid")
     */
    private $attributeGroupDefinition;


    /**
     * @return AttributesGroupDefinitions
     */
    public function getAttributeGroupDefinition()
    {
        return $this->attributeGroupDefinition;
    }

    /**
     * @param AttributesGroupDefinitions $attributeGroupDefinition
     */
    public function setAttributeGroupDefinition($attributeGroupDefinition)
    {
        $this->attributeGroupDefinition = $attributeGroupDefinition;
    }


    /**
     * Get attributegroupid
     *
     * @return integer 
     */
    public function getAttributegroupid()
    {
        return $this->attributegroupid;
    }

    /**
     * Set groupparentid
     *
     * @param integer $groupparentid
     * @return AttributesGroups
     */
    public function setGroupparentid($groupparentid)
    {
        $this->groupparentid = $groupparentid;

        return $this;
    }

    /**
     * Get groupparentid
     *
     * @return integer 
     */
    public function getGroupparentid()
    {
        return $this->groupparentid;
    }

    /**
     * Set groupcode
     *
     * @param string $groupcode
     * @return AttributesGroups
     */
    public function setGroupcode($groupcode)
    {
        $this->groupcode = $groupcode;

        return $this;
    }

    /**
     * Get groupcode
     *
     * @return string 
     */
    public function getGroupcode()
    {
        return $this->groupcode;
    }

    /**
     * Set sourcetable
     *
     * @param string $sourcetable
     * @return AttributesGroups
     */
    public function setSourcetable($sourcetable)
    {
        $this->sourcetable = $sourcetable;

        return $this;
    }

    /**
     * Get sourcetable
     *
     * @return string 
     */
    public function getSourcetable()
    {
        return $this->sourcetable;
    }

    /**
     * Set grouptype
     *
     * @param string $grouptype
     * @return AttributesGroups
     */
    public function setGrouptype($grouptype)
    {
        $this->grouptype = $grouptype;

        return $this;
    }

    /**
     * Get grouptype
     *
     * @return string 
     */
    public function getGrouptype()
    {
        return $this->grouptype;
    }

    /**
     * Set viewas
     *
     * @param string $viewas
     * @return AttributesGroups
     */
    public function setViewas($viewas)
    {
        $this->viewas = $viewas;

        return $this;
    }

    /**
     * Get viewas
     *
     * @return string 
     */
    public function getViewas()
    {
        return $this->viewas;
    }

    /**
     * Set usedforfiltering
     *
     * @param boolean $usedforfiltering
     * @return AttributesGroups
     */
    public function setUsedforfiltering($usedforfiltering)
    {
        $this->usedforfiltering = $usedforfiltering;

        return $this;
    }

    /**
     * Get usedforfiltering
     *
     * @return boolean 
     */
    public function getUsedforfiltering()
    {
        return $this->usedforfiltering;
    }

    /**
     * Set usedforgrouping
     *
     * @param boolean $usedforgrouping
     * @return AttributesGroups
     */
    public function setUsedforgrouping($usedforgrouping)
    {
        $this->usedforgrouping = $usedforgrouping;

        return $this;
    }

    /**
     * Get usedforgrouping
     *
     * @return boolean 
     */
    public function getUsedforgrouping()
    {
        return $this->usedforgrouping;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return AttributesGroups
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set priority
     *
     * @param boolean $priority
     * @return AttributesGroups
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return boolean 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return AttributesGroups
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return AttributesGroups
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Get Attributes
     *
     * @return \PrismProductsManager\Entity\Attributes
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Get Attributes
     *
     * @return \PrismProductsManager\Entity\AttributesGroupDefinitions
     */
    public function getAttributeDefinition()
    {
        return $this->attributeGroupDefinition;
    }

}
