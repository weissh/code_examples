<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductTabs
 *
 * @ORM\Table(name="PRODUCT_Tabs", indexes={@ORM\Index(name="Index 2", columns={"productId"})})
 * @ORM\Entity
 */
class ProductTabs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productTabId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $producttabid;

    /**
     * @var string
     *
     * @ORM\Column(name="productTabTitle", type="string", length=50, nullable=false)
     */
    private $producttabtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="productTabContent", type="text", nullable=false)
     */
    private $producttabcontent;

    /**
     * @var string
     *
     * @ORM\Column(name="productIdFrom", type="string", length=50, nullable=false)
     */
    private $productidfrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="productId", type="integer", nullable=false)
     */
    private $productid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;



    /**
     * Get producttabid
     *
     * @return integer 
     */
    public function getProducttabid()
    {
        return $this->producttabid;
    }

    /**
     * Set producttabtitle
     *
     * @param string $producttabtitle
     * @return ProductTabs
     */
    public function setProducttabtitle($producttabtitle)
    {
        $this->producttabtitle = $producttabtitle;

        return $this;
    }

    /**
     * Get producttabtitle
     *
     * @return string 
     */
    public function getProducttabtitle()
    {
        return $this->producttabtitle;
    }

    /**
     * Set producttabcontent
     *
     * @param string $producttabcontent
     * @return ProductTabs
     */
    public function setProducttabcontent($producttabcontent)
    {
        $this->producttabcontent = $producttabcontent;

        return $this;
    }

    /**
     * Get producttabcontent
     *
     * @return string 
     */
    public function getProducttabcontent()
    {
        return $this->producttabcontent;
    }

    /**
     * Set productidfrom
     *
     * @param string $productidfrom
     * @return ProductTabs
     */
    public function setProductidfrom($productidfrom)
    {
        $this->productidfrom = $productidfrom;

        return $this;
    }

    /**
     * Get productidfrom
     *
     * @return string 
     */
    public function getProductidfrom()
    {
        return $this->productidfrom;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return ProductTabs
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProductTabs
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductTabs
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductTabs
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }
}
