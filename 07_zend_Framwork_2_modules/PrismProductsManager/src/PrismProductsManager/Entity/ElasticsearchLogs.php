<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ElasticsearchLogs
 *
 * @ORM\Table(name="ELASTICSEARCH_Logs", uniqueConstraints={@ORM\UniqueConstraint(name="productId", columns={"logId"})}, indexes={@ORM\Index(name="indexType", columns={"indexType"})})
 * @ORM\Entity
 */
class ElasticsearchLogs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="logId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $logid;

    /**
     * @var integer
     *
     * @ORM\Column(name="indexType", type="integer", nullable=false)
     */
    private $indextype;

    /**
     * @var integer
     *
     * @ORM\Column(name="processId", type="integer", nullable=false)
     */
    private $processid;

    /**
     * @var string
     *
     * @ORM\Column(name="lastStatusUpdate", type="text", nullable=false)
     */
    private $laststatusupdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;



    /**
     * Get logid
     *
     * @return integer 
     */
    public function getLogid()
    {
        return $this->logid;
    }

    /**
     * Set indextype
     *
     * @param integer $indextype
     * @return ElasticsearchLogs
     */
    public function setIndextype($indextype)
    {
        $this->indextype = $indextype;

        return $this;
    }

    /**
     * Get indextype
     *
     * @return integer 
     */
    public function getIndextype()
    {
        return $this->indextype;
    }

    /**
     * Set processid
     *
     * @param integer $processid
     * @return ElasticsearchLogs
     */
    public function setProcessid($processid)
    {
        $this->processid = $processid;

        return $this;
    }

    /**
     * Get processid
     *
     * @return integer 
     */
    public function getProcessid()
    {
        return $this->processid;
    }

    /**
     * Set laststatusupdate
     *
     * @param string $laststatusupdate
     * @return ElasticsearchLogs
     */
    public function setLaststatusupdate($laststatusupdate)
    {
        $this->laststatusupdate = $laststatusupdate;

        return $this;
    }

    /**
     * Get laststatusupdate
     *
     * @return string 
     */
    public function getLaststatusupdate()
    {
        return $this->laststatusupdate;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return ElasticsearchLogs
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }
}
