<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputPages
 *
 * @ORM\Table(name="INPUT_Pages")
 * @ORM\Entity(repositoryClass="PrismProductsManager\Entity\Repository\InputPagesRepository")
 */
class InputPages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputPageId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputpageid;

    /**
     * @var string
     *
     * @ORM\Column(name="pageName", type="string", length=70, nullable=false)
     */
    private $pagename;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allowNewFields", type="boolean", nullable=false)
     */
    private $allownewfields;

    /**
     * @var string
     *
     * @ORM\Column(name="pageUrl", type="string", length=70, nullable=false)
     */
    private $pageurl;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \pageOrder
     *
     * @ORM\Column(name="pageOrder", type="integer", nullable=true)
     */
    private $pageOrder;


    /**
     *
     *  @var \PrismProductsManager\Entity\InputFieldRPages
     *
     *  @ORM\OneToMany(targetEntity="PrismProductsManager\Entity\InputFieldRPages", mappedBy="inputpageid")
     */
    private $inputFieldRPages;

    /**
     * @return boolean
     */
    public function isAllownewfields()
    {
        return $this->allownewfields;
    }

    /**
     * @param boolean $allownewfields
     */
    public function setAllownewfields($allownewfields)
    {
        $this->allownewfields = $allownewfields;
    }



    /**
     * Get inputpageid
     *
     * @return integer
     */
    public function getInputpageid()
    {
        return $this->inputpageid;
    }

    /**
     * Set pagename
     *
     * @param string $pagename
     * @return InputPages
     */
    public function setPagename($pagename)
    {
        $this->pagename = $pagename;

        return $this;
    }

    /**
     * Get pagename
     *
     * @return string
     */
    public function getPagename()
    {
        return $this->pagename;
    }

    /**
     * Set pageurl
     *
     * @param string $pageurl
     * @return InputPages
     */
    public function setPageurl($pageurl)
    {
        $this->pageurl = $pageurl;

        return $this;
    }

    /**
     * Get pageurl
     *
     * @return string
     */
    public function getPageurl()
    {
        return $this->pageurl;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputPages
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Set pageOrder
     * @param  $pageorder
     * @return InputPages
     */
    public function setPageOrder($pageorder)
    {
        $this->pageOrder = $pageorder;

        return $this;
    }
    /**
     * Set pageOrder
     * @return pageOrder
     */
    public function getPageOrder()
    {
        return $this->pageOrder;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputPages
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Get createdat
     *
     * @return \PrismProductsManager\Entity\InputFieldRPages
     */
    public function getInputFieldRPages()
    {
        return $this->inputFieldRPages;
    }


}
