<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputValidations
 *
 * @ORM\Table(name="INPUT_Validations", indexes={@ORM\Index(name="Index 2", columns={"inputValidationType"}), @ORM\Index(name="Index 3", columns={"inputFieldId"})})
 * @ORM\Entity
 */
class InputValidations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputValidationId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputvalidationid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="required", type="boolean", nullable=false)
     */
    private $required;

    /**
     * @var string
     *
     * @ORM\Column(name="pattern", type="string", length=250, nullable=true)
     */
    private $pattern;

    /**
     * @var integer
     *
     * @ORM\Column(name="inputValidationMinValue", type="integer", nullable=true)
     */
    private $inputvalidationminvalue;

    /**
     * @var integer
     *
     * @ORM\Column(name="inputValidationMaxValue", type="integer", nullable=true)
     */
    private $inputvalidationmaxvalue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \PrismProductsManager\Entity\InputFields
     *
     * @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\InputFields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputFieldId", referencedColumnName="inputFieldId")
     * })
     */
    private $inputfieldid;

    /**
     * @var \PrismProductsManager\Entity\InputValidationTypes
     *
     * @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\InputValidationTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputValidationType", referencedColumnName="inputValidationTypeId")
     * })
     */
    private $inputvalidationtype;

    /**
     * @return boolean
     */
    public function isRequired()
    {
        return $this->required;
    }

    /**
     * @param boolean $required
     */
    public function setRequired($required)
    {
        $this->required = $required;
    }




    /**
     * Get inputvalidationid
     *
     * @return integer 
     */
    public function getInputvalidationid()
    {
        return $this->inputvalidationid;
    }

    /**
     * Set pattern
     *
     * @param string $pattern
     * @return InputValidations
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;

        return $this;
    }

    /**
     * Get pattern
     *
     * @return string 
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * Set inputvalidationminvalue
     *
     * @param integer $inputvalidationminvalue
     * @return InputValidations
     */
    public function setInputvalidationminvalue($inputvalidationminvalue)
    {
        $this->inputvalidationminvalue = $inputvalidationminvalue;

        return $this;
    }

    /**
     * Get inputvalidationminvalue
     *
     * @return integer 
     */
    public function getInputvalidationminvalue()
    {
        return $this->inputvalidationminvalue;
    }

    /**
     * Set inputvalidationmaxvalue
     *
     * @param integer $inputvalidationmaxvalue
     * @return InputValidations
     */
    public function setInputvalidationmaxvalue($inputvalidationmaxvalue)
    {
        $this->inputvalidationmaxvalue = $inputvalidationmaxvalue;

        return $this;
    }

    /**
     * Get inputvalidationmaxvalue
     *
     * @return integer 
     */
    public function getInputvalidationmaxvalue()
    {
        return $this->inputvalidationmaxvalue;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputValidations
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputValidations
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set inputfieldid
     *
     * @param \PrismProductsManager\Entity\InputFields $inputfieldid
     * @return InputValidations
     */
    public function setInputfieldid(\PrismProductsManager\Entity\InputFields $inputfieldid = null)
    {
        $this->inputfieldid = $inputfieldid;

        return $this;
    }

    /**
     * Get inputfieldid
     *
     * @return \PrismProductsManager\Entity\InputFields 
     */
    public function getInputfieldid()
    {
        return $this->inputfieldid;
    }

    /**
     * Set inputvalidationtype
     *
     * @param \PrismProductsManager\Entity\InputValidationTypes $inputvalidationtype
     * @return InputValidations
     */
    public function setInputvalidationtype(\PrismProductsManager\Entity\InputValidationTypes $inputvalidationtype = null)
    {
        $this->inputvalidationtype = $inputvalidationtype;

        return $this;
    }

    /**
     * Get inputvalidationtype
     *
     * @return \PrismProductsManager\Entity\InputValidationTypes 
     */
    public function getInputvalidationtype()
    {
        return $this->inputvalidationtype;
    }
}
