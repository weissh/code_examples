<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsRClientsWebsites
 *
 * @ORM\Table(name="PRODUCTS_R_CLIENTS_Websites", indexes={@ORM\Index(name="Index 2", columns={"websiteId"}), @ORM\Index(name="Index 3", columns={"productId"})})
 * @ORM\Entity
 */
class ProductsRClientsWebsites
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productWebsiteId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productwebsiteid;

    /**
     * @var string
     *
     * @ORM\Column(name="productIdFrom", type="string", nullable=false)
     */
    private $productidfrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="productId", type="integer", nullable=false)
     */
    private $productid;

    /**
     * @var integer
     *
     * @ORM\Column(name="websiteId", type="integer", nullable=false)
     */
    private $websiteid;

    /**
     * @var integer
     *
     * @ORM\Column(name="elasticSearchIndexId", type="integer", nullable=false)
     */
    private $elasticsearchindexid;



    /**
     * Get productwebsiteid
     *
     * @return integer 
     */
    public function getProductwebsiteid()
    {
        return $this->productwebsiteid;
    }

    /**
     * Set productidfrom
     *
     * @param string $productidfrom
     * @return ProductsRClientsWebsites
     */
    public function setProductidfrom($productidfrom)
    {
        $this->productidfrom = $productidfrom;

        return $this;
    }

    /**
     * Get productidfrom
     *
     * @return string 
     */
    public function getProductidfrom()
    {
        return $this->productidfrom;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return ProductsRClientsWebsites
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set websiteid
     *
     * @param integer $websiteid
     * @return ProductsRClientsWebsites
     */
    public function setWebsiteid($websiteid)
    {
        $this->websiteid = $websiteid;

        return $this;
    }

    /**
     * Get websiteid
     *
     * @return integer 
     */
    public function getWebsiteid()
    {
        return $this->websiteid;
    }

    /**
     * Set elasticsearchindexid
     *
     * @param integer $elasticsearchindexid
     * @return ProductsRClientsWebsites
     */
    public function setElasticsearchindexid($elasticsearchindexid)
    {
        $this->elasticsearchindexid = $elasticsearchindexid;

        return $this;
    }

    /**
     * Get elasticsearchindexid
     *
     * @return integer 
     */
    public function getElasticsearchindexid()
    {
        return $this->elasticsearchindexid;
    }
}
