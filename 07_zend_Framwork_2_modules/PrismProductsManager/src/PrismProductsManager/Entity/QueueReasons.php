<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QueueReasons
 *
 * @ORM\Table(name="QUEUE_Reasons")
 * @ORM\Entity
 */
class QueueReasons
{
    /**
     * @var integer
     *
     * @ORM\Column(name="queueReason", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $queuereason;

    /**
     * @var string
     *
     * @ORM\Column(name="queueReasonDefinition", type="string", length=255, nullable=false)
     */
    private $queuereasondefinition;



    /**
     * Get queuereason
     *
     * @return integer 
     */
    public function getQueuereason()
    {
        return $this->queuereason;
    }

    /**
     * Set queuereasondefinition
     *
     * @param string $queuereasondefinition
     * @return QueueReasons
     */
    public function setQueuereasondefinition($queuereasondefinition)
    {
        $this->queuereasondefinition = $queuereasondefinition;

        return $this;
    }

    /**
     * Get queuereasondefinition
     *
     * @return string 
     */
    public function getQueuereasondefinition()
    {
        return $this->queuereasondefinition;
    }
}
