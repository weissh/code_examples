<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputValidationOptions
 *
 * @ORM\Table(name="INPUT_Validation_Options", indexes={@ORM\Index(name="Index 2", columns={"inputValidationId"}), @ORM\Index(name="Index 3", columns={"inputValidationParent"})})
 * @ORM\Entity
 */
class InputValidationOptions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputValidationOptionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputvalidationoptionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="inputValidationId", type="integer", nullable=true)
     */
    private $inputvalidationid;

    /**
     * @var integer
     *
     * @ORM\Column(name="inputValidationParent", type="integer", nullable=true)
     */
    private $inputvalidationparent;

    /**
     * @var string
     *
     * @ORM\Column(name="inputValidationOptionKey", type="string", length=250, nullable=true)
     */
    private $inputvalidationoptionkey;

    /**
     * @var string
     *
     * @ORM\Column(name="inputValidationOptionValue", type="string", length=250, nullable=true)
     */
    private $inputvalidationoptionvalue;

    /**
     * @var string
     *
     * @ORM\Column(name="inputValidationOptionType", type="string", length=50, nullable=true)
     */
    private $inputvalidationoptiontype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;



    /**
     * Get inputvalidationoptionid
     *
     * @return integer 
     */
    public function getInputvalidationoptionid()
    {
        return $this->inputvalidationoptionid;
    }

    /**
     * Set inputvalidationid
     *
     * @param integer $inputvalidationid
     * @return InputValidationOptions
     */
    public function setInputvalidationid($inputvalidationid)
    {
        $this->inputvalidationid = $inputvalidationid;

        return $this;
    }

    /**
     * Get inputvalidationid
     *
     * @return integer 
     */
    public function getInputvalidationid()
    {
        return $this->inputvalidationid;
    }

    /**
     * Set inputvalidationparent
     *
     * @param integer $inputvalidationparent
     * @return InputValidationOptions
     */
    public function setInputvalidationparent($inputvalidationparent)
    {
        $this->inputvalidationparent = $inputvalidationparent;

        return $this;
    }

    /**
     * Get inputvalidationparent
     *
     * @return integer 
     */
    public function getInputvalidationparent()
    {
        return $this->inputvalidationparent;
    }

    /**
     * Set inputvalidationoptionkey
     *
     * @param string $inputvalidationoptionkey
     * @return InputValidationOptions
     */
    public function setInputvalidationoptionkey($inputvalidationoptionkey)
    {
        $this->inputvalidationoptionkey = $inputvalidationoptionkey;

        return $this;
    }

    /**
     * Get inputvalidationoptionkey
     *
     * @return string 
     */
    public function getInputvalidationoptionkey()
    {
        return $this->inputvalidationoptionkey;
    }

    /**
     * Set inputvalidationoptionvalue
     *
     * @param string $inputvalidationoptionvalue
     * @return InputValidationOptions
     */
    public function setInputvalidationoptionvalue($inputvalidationoptionvalue)
    {
        $this->inputvalidationoptionvalue = $inputvalidationoptionvalue;

        return $this;
    }

    /**
     * Get inputvalidationoptionvalue
     *
     * @return string 
     */
    public function getInputvalidationoptionvalue()
    {
        return $this->inputvalidationoptionvalue;
    }

    /**
     * Set inputvalidationoptiontype
     *
     * @param string $inputvalidationoptiontype
     * @return InputValidationOptions
     */
    public function setInputvalidationoptiontype($inputvalidationoptiontype)
    {
        $this->inputvalidationoptiontype = $inputvalidationoptiontype;

        return $this;
    }

    /**
     * Get inputvalidationoptiontype
     *
     * @return string 
     */
    public function getInputvalidationoptiontype()
    {
        return $this->inputvalidationoptiontype;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputValidationOptions
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputValidationOptions
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }
}
