<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SizeSkuConverter
 *
 * @ORM\Table(name="SIZE_SKU_Converter", uniqueConstraints={@ORM\UniqueConstraint(name="sizeAlphabet_skuCode", columns={"sizeAlphabetic", "skuCode"})})
 * @ORM\Entity
 */
class SizeSkuConverter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sizeAlphabetic", type="string", length=50, nullable=false)
     */
    private $sizealphabetic;

    /**
     * @var string
     *
     * @ORM\Column(name="skuCode", type="string", length=50, nullable=false)
     */
    private $skucode;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sizealphabetic
     *
     * @param string $sizealphabetic
     * @return SizeSkuConverter
     */
    public function setSizealphabetic($sizealphabetic)
    {
        $this->sizealphabetic = $sizealphabetic;

        return $this;
    }

    /**
     * Get sizealphabetic
     *
     * @return string 
     */
    public function getSizealphabetic()
    {
        return $this->sizealphabetic;
    }

    /**
     * Set skucode
     *
     * @param string $skucode
     * @return SizeSkuConverter
     */
    public function setSkucode($skucode)
    {
        $this->skucode = $skucode;

        return $this;
    }

    /**
     * Get skucode
     *
     * @return string 
     */
    public function getSkucode()
    {
        return $this->skucode;
    }
}
