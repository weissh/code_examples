<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributesValuesOverrides
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES_VALUES_Overrides", indexes={@ORM\Index(name="FK_COMMON_ATTRIBUTES_VALUES_Overrides_COMMON_ATTRIBUTES_VALUES", columns={"commonAttributeValueId"}), @ORM\Index(name="FK_COMMON_ATTRIBUTES_VALUES_Overrides_TERRITORIES", columns={"territoryId"})})
 * @ORM\Entity
 */
class CommonAttributesValuesOverrides
{
    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributeValueOverrideId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commonattributevalueoverrideid;

    /**
     * @var string
     *
     * @ORM\Column(name="commonAttributeValueSkuCode", type="string", length=10, nullable=false)
     */
    private $commonattributevalueskucode;

    /**
     * @var string
     *
     * @ORM\Column(name="commonAttributeValue", type="string", length=75, nullable=false)
     */
    private $commonattributevalue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \PrismProductsManager\Entity\Territories
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Territories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="territoryId", referencedColumnName="territoryId")
     * })
     */
    private $territoryid;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributesValues
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesValues")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="commonAttributeValueId", referencedColumnName="commonAttributeValueId")
     * })
     */
    private $commonattributevalueid;



    /**
     * Get commonattributevalueoverrideid
     *
     * @return integer 
     */
    public function getCommonattributevalueoverrideid()
    {
        return $this->commonattributevalueoverrideid;
    }

    /**
     * Set commonattributevalueskucode
     *
     * @param string $commonattributevalueskucode
     * @return CommonAttributesValuesOverrides
     */
    public function setCommonattributevalueskucode($commonattributevalueskucode)
    {
        $this->commonattributevalueskucode = $commonattributevalueskucode;

        return $this;
    }

    /**
     * Get commonattributevalueskucode
     *
     * @return string 
     */
    public function getCommonattributevalueskucode()
    {
        return $this->commonattributevalueskucode;
    }

    /**
     * Set commonattributevalue
     *
     * @param string $commonattributevalue
     * @return CommonAttributesValuesOverrides
     */
    public function setCommonattributevalue($commonattributevalue)
    {
        $this->commonattributevalue = $commonattributevalue;

        return $this;
    }

    /**
     * Get commonattributevalue
     *
     * @return string 
     */
    public function getCommonattributevalue()
    {
        return $this->commonattributevalue;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return CommonAttributesValuesOverrides
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return CommonAttributesValuesOverrides
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set territoryid
     *
     * @param \PrismProductsManager\Entity\Territories $territoryid
     * @return CommonAttributesValuesOverrides
     */
    public function setTerritoryid(\PrismProductsManager\Entity\Territories $territoryid = null)
    {
        $this->territoryid = $territoryid;

        return $this;
    }

    /**
     * Get territoryid
     *
     * @return \PrismProductsManager\Entity\Territories 
     */
    public function getTerritoryid()
    {
        return $this->territoryid;
    }

    /**
     * Set commonattributevalueid
     *
     * @param \PrismProductsManager\Entity\CommonAttributesValues $commonattributevalueid
     * @return CommonAttributesValuesOverrides
     */
    public function setCommonattributevalueid(\PrismProductsManager\Entity\CommonAttributesValues $commonattributevalueid = null)
    {
        $this->commonattributevalueid = $commonattributevalueid;

        return $this;
    }

    /**
     * Get commonattributevalueid
     *
     * @return \PrismProductsManager\Entity\CommonAttributesValues 
     */
    public function getCommonattributevalueid()
    {
        return $this->commonattributevalueid;
    }
}
