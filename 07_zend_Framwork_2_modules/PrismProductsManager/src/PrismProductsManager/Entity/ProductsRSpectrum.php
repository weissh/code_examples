<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsRSpectrum
 *
 * @ORM\Table(name="PRODUCTS_R_SPECTRUM", indexes={@ORM\Index(name="spectrumId", columns={"spectrumId"}), @ORM\Index(name="productId", columns={"productId"}), @ORM\Index(name="style", columns={"style"})})
 * @ORM\Entity
 */
class ProductsRSpectrum
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productSpectrumId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productspectrumid;

    /**
     * @var string
     *
     * @ORM\Column(name="productIdFrom", type="string", nullable=false)
     */
    private $productidfrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="productId", type="integer", nullable=false)
     */
    private $productid;

    /**
     * @var integer
     *
     * @ORM\Column(name="spectrumId", type="integer", nullable=false)
     */
    private $spectrumid;

    /**
     * @var string
     *
     * @ORM\Column(name="style", type="string", length=30, nullable=false)
     */
    private $style;

    /**
     * @var string
     *
     * @ORM\Column(name="crossBorder", type="string", length=255, nullable=true)
     */
    private $crossborder;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get productspectrumid
     *
     * @return integer 
     */
    public function getProductspectrumid()
    {
        return $this->productspectrumid;
    }

    /**
     * Set productidfrom
     *
     * @param string $productidfrom
     * @return ProductsRSpectrum
     */
    public function setProductidfrom($productidfrom)
    {
        $this->productidfrom = $productidfrom;

        return $this;
    }

    /**
     * Get productidfrom
     *
     * @return string 
     */
    public function getProductidfrom()
    {
        return $this->productidfrom;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return ProductsRSpectrum
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set spectrumid
     *
     * @param integer $spectrumid
     * @return ProductsRSpectrum
     */
    public function setSpectrumid($spectrumid)
    {
        $this->spectrumid = $spectrumid;

        return $this;
    }

    /**
     * Get spectrumid
     *
     * @return integer 
     */
    public function getSpectrumid()
    {
        return $this->spectrumid;
    }

    /**
     * Set style
     *
     * @param string $style
     * @return ProductsRSpectrum
     */
    public function setStyle($style)
    {
        $this->style = $style;

        return $this;
    }

    /**
     * Get style
     *
     * @return string 
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * Set crossborder
     *
     * @param string $crossborder
     * @return ProductsRSpectrum
     */
    public function setCrossborder($crossborder)
    {
        $this->crossborder = $crossborder;

        return $this;
    }

    /**
     * Get crossborder
     *
     * @return string 
     */
    public function getCrossborder()
    {
        return $this->crossborder;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsRSpectrum
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsRSpectrum
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
