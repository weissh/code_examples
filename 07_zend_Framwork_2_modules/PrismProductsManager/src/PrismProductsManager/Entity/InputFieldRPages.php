<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputFieldRPages
 *
 * @ORM\Table(name="INPUT_Field_R_Pages", indexes={@ORM\Index(name="Index 2", columns={"inputPageId"}), @ORM\Index(name="Index 3", columns={"inputFieldId"}), @ORM\Index(name="fk_INPUT_Field_R_Pages_INPUT_Fieldsets1_idx", columns={"INPUT_Fieldsets_inputFieldsetId"})})
 * @ORM\Entity
 */
class InputFieldRPages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputFieldRPageId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputfieldrpageid;

    /**
     * @var integer
     *
     * @ORM\Column(name="`order`", type="integer", nullable=false)
     */
    private $order;

    /**
     * @var integer
     *
     * @ORM\Column(name="enabled", type="integer", nullable=false)
     */
    private $enabled;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \PrismProductsManager\Entity\InputFields
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputFields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputFieldId", referencedColumnName="inputFieldId")
     * })
     */
    private $inputfieldid;

    /**
     * @var \PrismProductsManager\Entity\InputPages
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputPages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputPageId", referencedColumnName="inputPageId")
     * })
     */
    private $inputpageid;

    /**
     * @var \PrismProductsManager\Entity\InputFieldsets
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputFieldsets")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="INPUT_Fieldsets_inputFieldsetId", referencedColumnName="inputFieldsetId")
     * })
     */
    private $inputFieldsetsInputfieldsetid;



    /**
     * Get inputfieldrpageid
     *
     * @return integer 
     */
    public function getInputfieldrpageid()
    {
        return $this->inputfieldrpageid;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return InputFieldRPages
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set enabled
     *
     * @param integer $enabled
     * @return InputFieldRPages
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return integer
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputFieldRPages
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputFieldRPages
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set inputfieldid
     *
     * @param \PrismProductsManager\Entity\InputFields $inputfieldid
     * @return InputFieldRPages
     */
    public function setInputfieldid(\PrismProductsManager\Entity\InputFields $inputfieldid = null)
    {
        $this->inputfieldid = $inputfieldid;

        return $this;
    }

    /**
     * Get inputfieldid
     *
     * @return \PrismProductsManager\Entity\InputFields 
     */
    public function getInputfieldid()
    {
        return $this->inputfieldid;
    }

    /**
     * Set inputpageid
     *
     * @param \PrismProductsManager\Entity\InputPages $inputpageid
     * @return InputFieldRPages
     */
    public function setInputpageid(\PrismProductsManager\Entity\InputPages $inputpageid = null)
    {
        $this->inputpageid = $inputpageid;

        return $this;
    }

    /**
     * Get inputpageid
     *
     * @return \PrismProductsManager\Entity\InputPages 
     */
    public function getInputpageid()
    {
        return $this->inputpageid;
    }

    /**
     * Set inputFieldsetsInputfieldsetid
     *
     * @param \PrismProductsManager\Entity\InputFieldsets $inputFieldsetsInputfieldsetid
     * @return InputFieldRPages
     */
    public function setInputFieldsetsInputfieldsetid(\PrismProductsManager\Entity\InputFieldsets $inputFieldsetsInputfieldsetid = null)
    {
        $this->inputFieldsetsInputfieldsetid = $inputFieldsetsInputfieldsetid;

        return $this;
    }

    /**
     * Get inputFieldsetsInputfieldsetid
     *
     * @return \PrismProductsManager\Entity\InputFieldsets 
     */
    public function getInputFieldsetsInputfieldsetid()
    {
        return $this->inputFieldsetsInputfieldsetid;
    }
}
