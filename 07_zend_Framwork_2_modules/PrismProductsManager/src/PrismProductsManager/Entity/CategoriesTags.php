<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriesTags
 *
 * @ORM\Table(name="CATEGORIES_Tags", indexes={@ORM\Index(name="FK_CATEGORIES_Tags_CATEGORIES_Definitions", columns={"categoriesDefinitionsId"})})
 * @ORM\Entity
 */
class CategoriesTags
{
    /**
     * @var integer
     *
     * @ORM\Column(name="categoriesTagId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $categoriestagid;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=50, nullable=false)
     */
    private $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\CategoriesDefinitions
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CategoriesDefinitions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoriesDefinitionsId", referencedColumnName="categoryDefinitionsId")
     * })
     */
    private $categoriesdefinitionsid;



    /**
     * Get categoriestagid
     *
     * @return integer 
     */
    public function getCategoriestagid()
    {
        return $this->categoriestagid;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return CategoriesTags
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return CategoriesTags
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return CategoriesTags
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return CategoriesTags
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set categoriesdefinitionsid
     *
     * @param \PrismProductsManager\Entity\CategoriesDefinitions $categoriesdefinitionsid
     * @return CategoriesTags
     */
    public function setCategoriesdefinitionsid(\PrismProductsManager\Entity\CategoriesDefinitions $categoriesdefinitionsid = null)
    {
        $this->categoriesdefinitionsid = $categoriesdefinitionsid;

        return $this;
    }

    /**
     * Get categoriesdefinitionsid
     *
     * @return \PrismProductsManager\Entity\CategoriesDefinitions 
     */
    public function getCategoriesdefinitionsid()
    {
        return $this->categoriesdefinitionsid;
    }
}
