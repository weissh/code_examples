<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsAttributesCombination
 *
 * @ORM\Table(name="PRODUCTS_ATTRIBUTES_Combination", indexes={@ORM\Index(name="productId", columns={"productOrVariantId"}), @ORM\Index(name="attributeId", columns={"attributeGroupId"}), @ORM\Index(name="status", columns={"status"})})
 * @ORM\Entity
 */
class ProductsAttributesCombination
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productAttributeCombinationId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productattributecombinationid;

    /**
     * @var integer
     *
     * @ORM\Column(name="attributeGroupId", type="integer", nullable=false)
     */
    private $attributegroupid;

    /**
     * @var integer
     *
     * @ORM\Column(name="productOrVariantId", type="integer", nullable=false)
     */
    private $productorvariantid;

    /**
     * @var string
     *
     * @ORM\Column(name="attributeValue", type="string", length=100, nullable=false)
     */
    private $attributevalue;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isVariant", type="boolean", nullable=false)
     */
    private $isvariant;

    /**
     * @var string
     *
     * @ORM\Column(name="groupType", type="string", length=255, nullable=true)
     */
    private $grouptype;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get productattributecombinationid
     *
     * @return integer 
     */
    public function getProductattributecombinationid()
    {
        return $this->productattributecombinationid;
    }

    /**
     * Set attributegroupid
     *
     * @param integer $attributegroupid
     * @return ProductsAttributesCombination
     */
    public function setAttributegroupid($attributegroupid)
    {
        $this->attributegroupid = $attributegroupid;

        return $this;
    }

    /**
     * Get attributegroupid
     *
     * @return integer 
     */
    public function getAttributegroupid()
    {
        return $this->attributegroupid;
    }

    /**
     * Set productorvariantid
     *
     * @param integer $productorvariantid
     * @return ProductsAttributesCombination
     */
    public function setProductorvariantid($productorvariantid)
    {
        $this->productorvariantid = $productorvariantid;

        return $this;
    }

    /**
     * Get productorvariantid
     *
     * @return integer 
     */
    public function getProductorvariantid()
    {
        return $this->productorvariantid;
    }

    /**
     * Set attributevalue
     *
     * @param string $attributevalue
     * @return ProductsAttributesCombination
     */
    public function setAttributevalue($attributevalue)
    {
        $this->attributevalue = $attributevalue;

        return $this;
    }

    /**
     * Get attributevalue
     *
     * @return string 
     */
    public function getAttributevalue()
    {
        return $this->attributevalue;
    }

    /**
     * Set isvariant
     *
     * @param boolean $isvariant
     * @return ProductsAttributesCombination
     */
    public function setIsvariant($isvariant)
    {
        $this->isvariant = $isvariant;

        return $this;
    }

    /**
     * Get isvariant
     *
     * @return boolean 
     */
    public function getIsvariant()
    {
        return $this->isvariant;
    }

    /**
     * Set grouptype
     *
     * @param string $grouptype
     * @return ProductsAttributesCombination
     */
    public function setGrouptype($grouptype)
    {
        $this->grouptype = $grouptype;

        return $this;
    }

    /**
     * Get grouptype
     *
     * @return string 
     */
    public function getGrouptype()
    {
        return $this->grouptype;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProductsAttributesCombination
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsAttributesCombination
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsAttributesCombination
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
