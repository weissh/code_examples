<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributesDefinitions
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES_Definitions", indexes={@ORM\Index(name="Index 2", columns={"languageId"}), @ORM\Index(name="FK__COMMON_ATTRIBUTES", columns={"commonAttributeId"})})
 * @ORM\Entity
 */
class CommonAttributesDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributeDefinitionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commonattributedefinitionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=true)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=75, nullable=false)
     */
    private $description;


    /**
     * @var string
     *
     * @ORM\Column(name="commonAttributeDataElement", type="string", nullable=true)
     */
    private $commonAttributeDataElement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     * @var integer
     *
     * @ORM\Column(name="modifiedBy", type="integer", nullable=false)
     */
    private $modifiedby;

    /**
     * @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributes", mappedBy="commonAttributeId", cascade={"persist"})
     * @ORM\JoinColumn(name="commonAttributeId", referencedColumnName="commonAttributeId")
     **/
    private $commonattributeid;

    /**
     * @return string
     */
    public function getCommonAttributeDataElement()
    {
        return $this->commonAttributeDataElement;
    }

    /**
     * @param string $commonAttributeDataElement
     */
    public function setCommonAttributeDataElement($commonAttributeDataElement)
    {
        $this->commonAttributeDataElement = $commonAttributeDataElement;
    }

    /**
     * Get commonattributedefinitionid
     *
     * @return integer 
     */
    public function getCommonattributedefinitionid()
    {
        return $this->commonattributedefinitionid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return CommonAttributesDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return CommonAttributesDefinitions
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return CommonAttributesDefinitions
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return CommonAttributesDefinitions
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set modifiedby
     *
     * @param integer $modifiedby
     * @return CommonAttributesDefinitions
     */
    public function setModifiedby($modifiedby)
    {
        $this->modifiedby = $modifiedby;

        return $this;
    }

    /**
     * Get modifiedby
     *
     * @return integer 
     */
    public function getModifiedby()
    {
        return $this->modifiedby;
    }

    /**
     * Set commonattributeid
     *
     * @param \PrismProductsManager\Entity\CommonAttributes $commonattributeid
     * @return CommonAttributesDefinitions
     */
    public function setCommonattributeid(\PrismProductsManager\Entity\CommonAttributes $commonattributeid = null)
    {
        $this->commonattributeid = $commonattributeid;

        return $this;
    }

    /**
     * Get commonattributeid
     *
     * @return \PrismProductsManager\Entity\CommonAttributes 
     */
    public function getCommonattributeid()
    {
        return $this->commonattributeid;
    }
}
