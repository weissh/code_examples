<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputFieldRAttributes
 *
 * @ORM\Table(name="INPUT_Field_R_Attributes", indexes={@ORM\Index(name="Index 3", columns={"attributeId"})})
 * @ORM\Entity
 */
class InputFieldRAttributes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputFieldRAttributeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputfieldrattributeid;

    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributeId", type="integer", nullable=false)
     */
    private $commonAttributeId;

    /**
     * @var string
     *
     * @ORM\Column(name="attributeTable", type="string", length=70, nullable=false)
     */
    private $attributetable;

    /**
     *
     *  @var \PrismProductsManager\Entity\AttributesGroups
     *
     *  @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\AttributesGroups")
     *  @ORM\JoinColumn (name="attributeGroupId" , referencedColumnName="attributeGroupId")
     */
    private $attributeGroupId;

    /**
     *
     *  @var \PrismProductsManager\Entity\CommonAttributes
     *
     *  @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributes")
     *  @ORM\JoinColumn (name="commonAttributeId" , referencedColumnName="commonAttributeId")
     */
    private $commonAttributes;

    /**
     * @param AttributesGroups $attributeGroupId
     */
    public function setAttributeGroupId($attributeGroupId)
    {
        $this->attributeGroupId = $attributeGroupId;
    }

    /**
     * @param CommonAttributes $commonAttributes
     */
    public function setCommonAttributes($commonAttributes)
    {
        $this->commonAttributes = $commonAttributes;
    }

    /**
     * @return int
     */
    public function getCommonAttributeId()
    {
        return $this->commonAttributeId;
    }

    /**
     * @param int $commonAttributeId
     */
    public function setCommonAttributeId($commonAttributeId)
    {
        $this->commonAttributeId = $commonAttributeId;
    }



    /**
     * @return AttributesGroups
     */
    public function getAttributeGroupId()
    {
        return $this->attributeGroupId;
    }


    /**
     * @return CommonAttributes
     */
    public function getCommonAttributes()
    {
        return $this->commonAttributes;
    }


    /**
     * Get inputfieldrattributeid
     *
     * @return integer
     */
    public function getInputfieldrattributeid()
    {
        return $this->inputfieldrattributeid;
    }

    /**
     * Set attributetable
     *
     * @param string $attributetable
     * @return InputFieldRAttributes
     */
    public function setAttributetable($attributetable)
    {
        $this->attributetable = $attributetable;

        return $this;
    }

    /**
     * Get attributetable
     *
     * @return string
     */
    public function getAttributetable()
    {
        return $this->attributetable;
    }

    /**
     * Set attributeid
     *
     * @param integer $attributeid
     * @return InputFieldRAttributes
     */
    public function setAttributeid($attributeid)
    {
        $this->attributeid = $attributeid;

        return $this;
    }

    /**
     * Get attributeid
     *
     * @return integer
     */
    public function getAttributeid()
    {
        return $this->attributeid;
    }

}
