<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriesRAttributes
 *
 * @ORM\Table(name="CATEGORIES_R_ATTRIBUTES", indexes={@ORM\Index(name="FK_CATEGORIES_R_ATTRIBUTES_CATEGORIES", columns={"categoryId"}), @ORM\Index(name="FK_CATEGORIES_R_ATTRIBUTES_ATTRIBUTES_Groups", columns={"attributeGroupId"})})
 * @ORM\Entity
 */
class CategoriesRAttributes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="categoryRAttributeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $categoryrattributeid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\AttributesGroups
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\AttributesGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="attributeGroupId", referencedColumnName="attributeGroupId")
     * })
     */
    private $attributegroupid;

    /**
     * @var \PrismProductsManager\Entity\Categories
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Categories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryId", referencedColumnName="categoryId")
     * })
     */
    private $categoryid;



    /**
     * Get categoryrattributeid
     *
     * @return integer 
     */
    public function getCategoryrattributeid()
    {
        return $this->categoryrattributeid;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return CategoriesRAttributes
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return CategoriesRAttributes
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return CategoriesRAttributes
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set attributegroupid
     *
     * @param \PrismProductsManager\Entity\AttributesGroups $attributegroupid
     * @return CategoriesRAttributes
     */
    public function setAttributegroupid(\PrismProductsManager\Entity\AttributesGroups $attributegroupid = null)
    {
        $this->attributegroupid = $attributegroupid;

        return $this;
    }

    /**
     * Get attributegroupid
     *
     * @return \PrismProductsManager\Entity\AttributesGroups 
     */
    public function getAttributegroupid()
    {
        return $this->attributegroupid;
    }

    /**
     * Set categoryid
     *
     * @param \PrismProductsManager\Entity\Categories $categoryid
     * @return CategoriesRAttributes
     */
    public function setCategoryid(\PrismProductsManager\Entity\Categories $categoryid = null)
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    /**
     * Get categoryid
     *
     * @return \PrismProductsManager\Entity\Categories 
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }
}
