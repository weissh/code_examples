<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsMerchantCategoriesDefinitions
 *
 * @ORM\Table(name="PRODUCTS_Merchant_Categories_Definitions", indexes={@ORM\Index(name="productsMerchantCategoriesId", columns={"productMerchantCategoryId"}), @ORM\Index(name="languageId", columns={"languageId"}), @ORM\Index(name="categoryName", columns={"categoryName"})})
 * @ORM\Entity
 */
class ProductsMerchantCategoriesDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productMerchantCategoryDefinitionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productmerchantcategorydefinitionid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="languageId", type="boolean", nullable=false)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="categoryName", type="string", length=30, nullable=false)
     */
    private $categoryname;

    /**
     * @var string
     *
     * @ORM\Column(name="shortDescription", type="string", length=150, nullable=true)
     */
    private $shortdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="fullDescription", type="text", nullable=true)
     */
    private $fulldescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\ProductsMerchantCategories
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\ProductsMerchantCategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="productMerchantCategoryId", referencedColumnName="productMerchantCategoryId")
     * })
     */
    private $productmerchantcategoryid;



    /**
     * Get productmerchantcategorydefinitionid
     *
     * @return integer 
     */
    public function getProductmerchantcategorydefinitionid()
    {
        return $this->productmerchantcategorydefinitionid;
    }

    /**
     * Set languageid
     *
     * @param boolean $languageid
     * @return ProductsMerchantCategoriesDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return boolean 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set categoryname
     *
     * @param string $categoryname
     * @return ProductsMerchantCategoriesDefinitions
     */
    public function setCategoryname($categoryname)
    {
        $this->categoryname = $categoryname;

        return $this;
    }

    /**
     * Get categoryname
     *
     * @return string 
     */
    public function getCategoryname()
    {
        return $this->categoryname;
    }

    /**
     * Set shortdescription
     *
     * @param string $shortdescription
     * @return ProductsMerchantCategoriesDefinitions
     */
    public function setShortdescription($shortdescription)
    {
        $this->shortdescription = $shortdescription;

        return $this;
    }

    /**
     * Get shortdescription
     *
     * @return string 
     */
    public function getShortdescription()
    {
        return $this->shortdescription;
    }

    /**
     * Set fulldescription
     *
     * @param string $fulldescription
     * @return ProductsMerchantCategoriesDefinitions
     */
    public function setFulldescription($fulldescription)
    {
        $this->fulldescription = $fulldescription;

        return $this;
    }

    /**
     * Get fulldescription
     *
     * @return string 
     */
    public function getFulldescription()
    {
        return $this->fulldescription;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsMerchantCategoriesDefinitions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsMerchantCategoriesDefinitions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set productmerchantcategoryid
     *
     * @param \PrismProductsManager\Entity\ProductsMerchantCategories $productmerchantcategoryid
     * @return ProductsMerchantCategoriesDefinitions
     */
    public function setProductmerchantcategoryid(\PrismProductsManager\Entity\ProductsMerchantCategories $productmerchantcategoryid = null)
    {
        $this->productmerchantcategoryid = $productmerchantcategoryid;

        return $this;
    }

    /**
     * Get productmerchantcategoryid
     *
     * @return \PrismProductsManager\Entity\ProductsMerchantCategories 
     */
    public function getProductmerchantcategoryid()
    {
        return $this->productmerchantcategoryid;
    }
}
