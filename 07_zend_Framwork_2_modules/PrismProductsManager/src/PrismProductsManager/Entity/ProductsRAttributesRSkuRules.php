<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsRAttributesRSkuRules
 *
 * @ORM\Table(name="PRODUCTS_R_ATTRIBUTES_R_SKU_Rules", indexes={@ORM\Index(name="Index 2", columns={"productAttributeId"})})
 * @ORM\Entity
 */
class ProductsRAttributesRSkuRules
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productAttributeSkuRuleId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productattributeskuruleid;

    /**
     * @var integer
     *
     * @ORM\Column(name="productAttributeId", type="integer", nullable=false)
     */
    private $productattributeid;

    /**
     * @var integer
     *
     * @ORM\Column(name="skuRuleId", type="integer", nullable=false)
     */
    private $skuruleid;

    /**
     * @var string
     *
     * @ORM\Column(name="skuRuleTable", type="string", length=70, nullable=false)
     */
    private $skuruletable;

    /**
     * @var integer
     *
     * @ORM\Column(name="isCommonAttribute", type="integer", nullable=false)
     */
    private $iscommonattribute;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get productattributeskuruleid
     *
     * @return integer 
     */
    public function getProductattributeskuruleid()
    {
        return $this->productattributeskuruleid;
    }

    /**
     * Set productattributeid
     *
     * @param integer $productattributeid
     * @return ProductsRAttributesRSkuRules
     */
    public function setProductattributeid($productattributeid)
    {
        $this->productattributeid = $productattributeid;

        return $this;
    }

    /**
     * Get productattributeid
     *
     * @return integer 
     */
    public function getProductattributeid()
    {
        return $this->productattributeid;
    }

    /**
     * Set skuruleid
     *
     * @param integer $skuruleid
     * @return ProductsRAttributesRSkuRules
     */
    public function setSkuruleid($skuruleid)
    {
        $this->skuruleid = $skuruleid;

        return $this;
    }

    /**
     * Get skuruleid
     *
     * @return integer 
     */
    public function getSkuruleid()
    {
        return $this->skuruleid;
    }

    /**
     * Set skuruletable
     *
     * @param string $skuruletable
     * @return ProductsRAttributesRSkuRules
     */
    public function setSkuruletable($skuruletable)
    {
        $this->skuruletable = $skuruletable;

        return $this;
    }

    /**
     * Get skuruletable
     *
     * @return string 
     */
    public function getSkuruletable()
    {
        return $this->skuruletable;
    }

    /**
     * Set iscommonattribute
     *
     * @param integer $iscommonattribute
     * @return ProductsRAttributesRSkuRules
     */
    public function setIscommonattribute($iscommonattribute)
    {
        $this->iscommonattribute = $iscommonattribute;

        return $this;
    }

    /**
     * Get iscommonattribute
     *
     * @return integer 
     */
    public function getIscommonattribute()
    {
        return $this->iscommonattribute;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsRAttributesRSkuRules
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsRAttributesRSkuRules
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
