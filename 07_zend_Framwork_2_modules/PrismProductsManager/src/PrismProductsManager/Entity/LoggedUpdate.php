<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LoggedUpdate
 *
 * @ORM\Table(name="logged_updates")
 * @ORM\Entity
 */
class LoggedUpdate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="userId", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="styleCode", type="string", nullable=true)
     */
    private $style;

    /**
     * @var string
     *
     * @ORM\Column(name="optionCode", type="string", nullable=true)
     */
    private $option;

    /**
     * @var string
     *
     * @ORM\Column(name="updateKey", type="string", nullable=false)
     */
    private $updateKey;

    /**
     * @var string
     *
     * @ORM\Column(name="updateValue", type="string", nullable=true)
     */
    private $updateValue;

    /**
     * @var string
     *
     * @ORM\Column(name="backtrace", type="string", nullable=true)
     */
    private $backtrace;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * @param string $style
     */
    public function setStyle($style)
    {
        $this->style = $style;
    }

    /**
     * @return string
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param string $option
     */
    public function setOption($option)
    {
        $this->option = $option;
    }

    /**
     * @return string
     */
    public function getUpdateKey()
    {
        return $this->updateKey;
    }

    /**
     * @param string $updateKey
     */
    public function setUpdateKey($updateKey)
    {
        $this->updateKey = $updateKey;
    }

    /**
     * @return string
     */
    public function getUpdateValue()
    {
        return $this->updateValue;
    }

    /**
     * @param string $updateValue
     */
    public function setUpdateValue($updateValue)
    {
        $this->updateValue = $updateValue;
    }

    /**
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getBacktrace()
    {
        return $this->backtrace;
    }

    /**
     * @param string $backtrace
     */
    public function setBacktrace($backtrace)
    {
        $this->backtrace = $backtrace;
    }

}
