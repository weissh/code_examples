<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vsupply
 *
 * @ORM\Table(name="vsupply", indexes={@ORM\Index(name="mcsupp", columns={"INITIALS", "SUPACNO"}), @ORM\Index(name="suppmc", columns={"SUPACNO", "INITIALS"})})
 * @ORM\Entity
 */
class Vsupply
{
    /**
     * @var string
     *
     * @ORM\Column(name="CLIENT", type="string", length=1, nullable=false)
     */
    private $client;

    /**
     * @var string
     *
     * @ORM\Column(name="SUPACNO", type="string", length=5, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $supacno;

    /**
     * @var string
     *
     * @ORM\Column(name="ACTIVE", type="string", length=1, nullable=false)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="VSTOCK", type="string", length=1, nullable=false)
     */
    private $vstock;

    /**
     * @var string
     *
     * @ORM\Column(name="MC", type="string", length=30, nullable=false)
     */
    private $mc;

    /**
     * @var string
     *
     * @ORM\Column(name="INITIALS", type="string", length=2, nullable=false)
     */
    private $initials;

    /**
     * @var string
     *
     * @ORM\Column(name="MCEMAIL", type="string", length=50, nullable=false)
     */
    private $mcemail;

    /**
     * @var string
     *
     * @ORM\Column(name="MCTYPE", type="string", length=1, nullable=false)
     */
    private $mctype;

    /**
     * @var string
     *
     * @ORM\Column(name="CONTACT", type="string", length=30, nullable=false)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="COMPANY", type="string", length=30, nullable=false)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="ADD1", type="string", length=30, nullable=false)
     */
    private $add1;

    /**
     * @var string
     *
     * @ORM\Column(name="ADD2", type="string", length=30, nullable=false)
     */
    private $add2;

    /**
     * @var string
     *
     * @ORM\Column(name="ADD3", type="string", length=30, nullable=false)
     */
    private $add3;

    /**
     * @var string
     *
     * @ORM\Column(name="ADD4", type="string", length=30, nullable=false)
     */
    private $add4;

    /**
     * @var string
     *
     * @ORM\Column(name="POSTCODE", type="string", length=8, nullable=false)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="COUNTRY", type="string", length=30, nullable=false)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="TELEPHONE", type="string", length=20, nullable=false)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="FAX", type="string", length=20, nullable=false)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="EMAIL", type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="EMAILTYPE", type="string", length=1, nullable=false)
     */
    private $emailtype;

    /**
     * @var string
     *
     * @ORM\Column(name="VATREG", type="string", length=14, nullable=false)
     */
    private $vatreg;

    /**
     * @var string
     *
     * @ORM\Column(name="CREDLIMIT", type="decimal", precision=9, scale=2, nullable=false)
     */
    private $credlimit;

    /**
     * @var integer
     *
     * @ORM\Column(name="TOTALREQS", type="integer", nullable=false)
     */
    private $totalreqs;

    /**
     * @var integer
     *
     * @ORM\Column(name="TOTALRETS", type="integer", nullable=false)
     */
    private $totalrets;

    /**
     * @var string
     *
     * @ORM\Column(name="MIN_VALUE", type="decimal", precision=7, scale=2, nullable=false)
     */
    private $minValue;

    /**
     * @var string
     *
     * @ORM\Column(name="ANAME", type="string", length=30, nullable=false)
     */
    private $aname;

    /**
     * @var string
     *
     * @ORM\Column(name="AADD1", type="string", length=30, nullable=false)
     */
    private $aadd1;

    /**
     * @var string
     *
     * @ORM\Column(name="AADD2", type="string", length=30, nullable=false)
     */
    private $aadd2;

    /**
     * @var string
     *
     * @ORM\Column(name="AADD3", type="string", length=30, nullable=false)
     */
    private $aadd3;

    /**
     * @var string
     *
     * @ORM\Column(name="AADD4", type="string", length=30, nullable=false)
     */
    private $aadd4;

    /**
     * @var string
     *
     * @ORM\Column(name="APOSTCODE", type="string", length=8, nullable=false)
     */
    private $apostcode;

    /**
     * @var string
     *
     * @ORM\Column(name="ACOUNTRY", type="string", length=30, nullable=false)
     */
    private $acountry;

    /**
     * @var string
     *
     * @ORM\Column(name="RNAME", type="string", length=30, nullable=false)
     */
    private $rname;

    /**
     * @var string
     *
     * @ORM\Column(name="RADD1", type="string", length=30, nullable=false)
     */
    private $radd1;

    /**
     * @var string
     *
     * @ORM\Column(name="RADD2", type="string", length=30, nullable=false)
     */
    private $radd2;

    /**
     * @var string
     *
     * @ORM\Column(name="RADD3", type="string", length=30, nullable=false)
     */
    private $radd3;

    /**
     * @var string
     *
     * @ORM\Column(name="RADD4", type="string", length=30, nullable=false)
     */
    private $radd4;

    /**
     * @var string
     *
     * @ORM\Column(name="RPOSTCODE", type="string", length=8, nullable=false)
     */
    private $rpostcode;

    /**
     * @var string
     *
     * @ORM\Column(name="RCOUNTRY", type="string", length=30, nullable=false)
     */
    private $rcountry;

    /**
     * @var string
     *
     * @ORM\Column(name="PRODSSETUP", type="string", length=1, nullable=false)
     */
    private $prodssetup;

    /**
     * @var integer
     *
     * @ORM\Column(name="REQTOS", type="integer", nullable=false)
     */
    private $reqtos;

    /**
     * @var integer
     *
     * @ORM\Column(name="REQD", type="integer", nullable=false)
     */
    private $reqd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="REQDUE", type="date", nullable=false)
     */
    private $reqdue;

    /**
     * @var string
     *
     * @ORM\Column(name="REQOVERDUE", type="string", length=1, nullable=false)
     */
    private $reqoverdue;

    /**
     * @var string
     *
     * @ORM\Column(name="EXCESS", type="string", length=1, nullable=false)
     */
    private $excess;

    /**
     * @var string
     *
     * @ORM\Column(name="HAYSTYPE", type="string", length=1, nullable=false)
     */
    private $haystype;

    /**
     * @var integer
     *
     * @ORM\Column(name="DELETED", type="integer", nullable=false)
     */
    private $deleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CREATED", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="CREATEDBY", type="bigint", nullable=false)
     */
    private $createdby;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MODIFIED", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var integer
     *
     * @ORM\Column(name="MODIFIEDBY", type="bigint", nullable=false)
     */
    private $modifiedby;

    /**
     * @var integer
     *
     * @ORM\Column(name="ERRORS", type="integer", nullable=false)
     */
    private $errors;

    /**
     * @var string
     *
     * @ORM\Column(name="SUPTERM", type="string", length=3, nullable=false)
     */
    private $supterm;

    /**
     * @var string
     *
     * @ORM\Column(name="SUPCURR", type="string", length=6, nullable=false)
     */
    private $supcurr;

    /**
     * @var string
     *
     * @ORM\Column(name="SHIPTERMS", type="string", length=3, nullable=false)
     */
    private $shipterms;

    /**
     * @var string
     *
     * @ORM\Column(name="SETTLEDAYS", type="string", length=4, nullable=false)
     */
    private $settledays;

    /**
     * @var string
     *
     * @ORM\Column(name="CNTRYORIG", type="string", length=3, nullable=false)
     */
    private $cntryorig;

    /**
     * @var string
     *
     * @ORM\Column(name="SETTLEDISC", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $settledisc;

    /**
     * @var string
     *
     * @ORM\Column(name="BUYERINITS", type="string", length=2, nullable=false)
     */
    private $buyerinits;



    /**
     * Set client
     *
     * @param string $client
     * @return Vsupply
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Get supacno
     *
     * @return string 
     */
    public function getSupacno()
    {
        return $this->supacno;
    }

    /**
     * Set active
     *
     * @param string $active
     * @return Vsupply
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return string 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set vstock
     *
     * @param string $vstock
     * @return Vsupply
     */
    public function setVstock($vstock)
    {
        $this->vstock = $vstock;

        return $this;
    }

    /**
     * Get vstock
     *
     * @return string 
     */
    public function getVstock()
    {
        return $this->vstock;
    }

    /**
     * Set mc
     *
     * @param string $mc
     * @return Vsupply
     */
    public function setMc($mc)
    {
        $this->mc = $mc;

        return $this;
    }

    /**
     * Get mc
     *
     * @return string 
     */
    public function getMc()
    {
        return $this->mc;
    }

    /**
     * Set initials
     *
     * @param string $initials
     * @return Vsupply
     */
    public function setInitials($initials)
    {
        $this->initials = $initials;

        return $this;
    }

    /**
     * Get initials
     *
     * @return string 
     */
    public function getInitials()
    {
        return $this->initials;
    }

    /**
     * Set mcemail
     *
     * @param string $mcemail
     * @return Vsupply
     */
    public function setMcemail($mcemail)
    {
        $this->mcemail = $mcemail;

        return $this;
    }

    /**
     * Get mcemail
     *
     * @return string 
     */
    public function getMcemail()
    {
        return $this->mcemail;
    }

    /**
     * Set mctype
     *
     * @param string $mctype
     * @return Vsupply
     */
    public function setMctype($mctype)
    {
        $this->mctype = $mctype;

        return $this;
    }

    /**
     * Get mctype
     *
     * @return string 
     */
    public function getMctype()
    {
        return $this->mctype;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return Vsupply
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return Vsupply
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set add1
     *
     * @param string $add1
     * @return Vsupply
     */
    public function setAdd1($add1)
    {
        $this->add1 = $add1;

        return $this;
    }

    /**
     * Get add1
     *
     * @return string 
     */
    public function getAdd1()
    {
        return $this->add1;
    }

    /**
     * Set add2
     *
     * @param string $add2
     * @return Vsupply
     */
    public function setAdd2($add2)
    {
        $this->add2 = $add2;

        return $this;
    }

    /**
     * Get add2
     *
     * @return string 
     */
    public function getAdd2()
    {
        return $this->add2;
    }

    /**
     * Set add3
     *
     * @param string $add3
     * @return Vsupply
     */
    public function setAdd3($add3)
    {
        $this->add3 = $add3;

        return $this;
    }

    /**
     * Get add3
     *
     * @return string 
     */
    public function getAdd3()
    {
        return $this->add3;
    }

    /**
     * Set add4
     *
     * @param string $add4
     * @return Vsupply
     */
    public function setAdd4($add4)
    {
        $this->add4 = $add4;

        return $this;
    }

    /**
     * Get add4
     *
     * @return string 
     */
    public function getAdd4()
    {
        return $this->add4;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Vsupply
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Vsupply
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Vsupply
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Vsupply
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Vsupply
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set emailtype
     *
     * @param string $emailtype
     * @return Vsupply
     */
    public function setEmailtype($emailtype)
    {
        $this->emailtype = $emailtype;

        return $this;
    }

    /**
     * Get emailtype
     *
     * @return string 
     */
    public function getEmailtype()
    {
        return $this->emailtype;
    }

    /**
     * Set vatreg
     *
     * @param string $vatreg
     * @return Vsupply
     */
    public function setVatreg($vatreg)
    {
        $this->vatreg = $vatreg;

        return $this;
    }

    /**
     * Get vatreg
     *
     * @return string 
     */
    public function getVatreg()
    {
        return $this->vatreg;
    }

    /**
     * Set credlimit
     *
     * @param string $credlimit
     * @return Vsupply
     */
    public function setCredlimit($credlimit)
    {
        $this->credlimit = $credlimit;

        return $this;
    }

    /**
     * Get credlimit
     *
     * @return string 
     */
    public function getCredlimit()
    {
        return $this->credlimit;
    }

    /**
     * Set totalreqs
     *
     * @param integer $totalreqs
     * @return Vsupply
     */
    public function setTotalreqs($totalreqs)
    {
        $this->totalreqs = $totalreqs;

        return $this;
    }

    /**
     * Get totalreqs
     *
     * @return integer 
     */
    public function getTotalreqs()
    {
        return $this->totalreqs;
    }

    /**
     * Set totalrets
     *
     * @param integer $totalrets
     * @return Vsupply
     */
    public function setTotalrets($totalrets)
    {
        $this->totalrets = $totalrets;

        return $this;
    }

    /**
     * Get totalrets
     *
     * @return integer 
     */
    public function getTotalrets()
    {
        return $this->totalrets;
    }

    /**
     * Set minValue
     *
     * @param string $minValue
     * @return Vsupply
     */
    public function setMinValue($minValue)
    {
        $this->minValue = $minValue;

        return $this;
    }

    /**
     * Get minValue
     *
     * @return string 
     */
    public function getMinValue()
    {
        return $this->minValue;
    }

    /**
     * Set aname
     *
     * @param string $aname
     * @return Vsupply
     */
    public function setAname($aname)
    {
        $this->aname = $aname;

        return $this;
    }

    /**
     * Get aname
     *
     * @return string 
     */
    public function getAname()
    {
        return $this->aname;
    }

    /**
     * Set aadd1
     *
     * @param string $aadd1
     * @return Vsupply
     */
    public function setAadd1($aadd1)
    {
        $this->aadd1 = $aadd1;

        return $this;
    }

    /**
     * Get aadd1
     *
     * @return string 
     */
    public function getAadd1()
    {
        return $this->aadd1;
    }

    /**
     * Set aadd2
     *
     * @param string $aadd2
     * @return Vsupply
     */
    public function setAadd2($aadd2)
    {
        $this->aadd2 = $aadd2;

        return $this;
    }

    /**
     * Get aadd2
     *
     * @return string 
     */
    public function getAadd2()
    {
        return $this->aadd2;
    }

    /**
     * Set aadd3
     *
     * @param string $aadd3
     * @return Vsupply
     */
    public function setAadd3($aadd3)
    {
        $this->aadd3 = $aadd3;

        return $this;
    }

    /**
     * Get aadd3
     *
     * @return string 
     */
    public function getAadd3()
    {
        return $this->aadd3;
    }

    /**
     * Set aadd4
     *
     * @param string $aadd4
     * @return Vsupply
     */
    public function setAadd4($aadd4)
    {
        $this->aadd4 = $aadd4;

        return $this;
    }

    /**
     * Get aadd4
     *
     * @return string 
     */
    public function getAadd4()
    {
        return $this->aadd4;
    }

    /**
     * Set apostcode
     *
     * @param string $apostcode
     * @return Vsupply
     */
    public function setApostcode($apostcode)
    {
        $this->apostcode = $apostcode;

        return $this;
    }

    /**
     * Get apostcode
     *
     * @return string 
     */
    public function getApostcode()
    {
        return $this->apostcode;
    }

    /**
     * Set acountry
     *
     * @param string $acountry
     * @return Vsupply
     */
    public function setAcountry($acountry)
    {
        $this->acountry = $acountry;

        return $this;
    }

    /**
     * Get acountry
     *
     * @return string 
     */
    public function getAcountry()
    {
        return $this->acountry;
    }

    /**
     * Set rname
     *
     * @param string $rname
     * @return Vsupply
     */
    public function setRname($rname)
    {
        $this->rname = $rname;

        return $this;
    }

    /**
     * Get rname
     *
     * @return string 
     */
    public function getRname()
    {
        return $this->rname;
    }

    /**
     * Set radd1
     *
     * @param string $radd1
     * @return Vsupply
     */
    public function setRadd1($radd1)
    {
        $this->radd1 = $radd1;

        return $this;
    }

    /**
     * Get radd1
     *
     * @return string 
     */
    public function getRadd1()
    {
        return $this->radd1;
    }

    /**
     * Set radd2
     *
     * @param string $radd2
     * @return Vsupply
     */
    public function setRadd2($radd2)
    {
        $this->radd2 = $radd2;

        return $this;
    }

    /**
     * Get radd2
     *
     * @return string 
     */
    public function getRadd2()
    {
        return $this->radd2;
    }

    /**
     * Set radd3
     *
     * @param string $radd3
     * @return Vsupply
     */
    public function setRadd3($radd3)
    {
        $this->radd3 = $radd3;

        return $this;
    }

    /**
     * Get radd3
     *
     * @return string 
     */
    public function getRadd3()
    {
        return $this->radd3;
    }

    /**
     * Set radd4
     *
     * @param string $radd4
     * @return Vsupply
     */
    public function setRadd4($radd4)
    {
        $this->radd4 = $radd4;

        return $this;
    }

    /**
     * Get radd4
     *
     * @return string 
     */
    public function getRadd4()
    {
        return $this->radd4;
    }

    /**
     * Set rpostcode
     *
     * @param string $rpostcode
     * @return Vsupply
     */
    public function setRpostcode($rpostcode)
    {
        $this->rpostcode = $rpostcode;

        return $this;
    }

    /**
     * Get rpostcode
     *
     * @return string 
     */
    public function getRpostcode()
    {
        return $this->rpostcode;
    }

    /**
     * Set rcountry
     *
     * @param string $rcountry
     * @return Vsupply
     */
    public function setRcountry($rcountry)
    {
        $this->rcountry = $rcountry;

        return $this;
    }

    /**
     * Get rcountry
     *
     * @return string 
     */
    public function getRcountry()
    {
        return $this->rcountry;
    }

    /**
     * Set prodssetup
     *
     * @param string $prodssetup
     * @return Vsupply
     */
    public function setProdssetup($prodssetup)
    {
        $this->prodssetup = $prodssetup;

        return $this;
    }

    /**
     * Get prodssetup
     *
     * @return string 
     */
    public function getProdssetup()
    {
        return $this->prodssetup;
    }

    /**
     * Set reqtos
     *
     * @param integer $reqtos
     * @return Vsupply
     */
    public function setReqtos($reqtos)
    {
        $this->reqtos = $reqtos;

        return $this;
    }

    /**
     * Get reqtos
     *
     * @return integer 
     */
    public function getReqtos()
    {
        return $this->reqtos;
    }

    /**
     * Set reqd
     *
     * @param integer $reqd
     * @return Vsupply
     */
    public function setReqd($reqd)
    {
        $this->reqd = $reqd;

        return $this;
    }

    /**
     * Get reqd
     *
     * @return integer 
     */
    public function getReqd()
    {
        return $this->reqd;
    }

    /**
     * Set reqdue
     *
     * @param \DateTime $reqdue
     * @return Vsupply
     */
    public function setReqdue($reqdue)
    {
        $this->reqdue = $reqdue;

        return $this;
    }

    /**
     * Get reqdue
     *
     * @return \DateTime 
     */
    public function getReqdue()
    {
        return $this->reqdue;
    }

    /**
     * Set reqoverdue
     *
     * @param string $reqoverdue
     * @return Vsupply
     */
    public function setReqoverdue($reqoverdue)
    {
        $this->reqoverdue = $reqoverdue;

        return $this;
    }

    /**
     * Get reqoverdue
     *
     * @return string 
     */
    public function getReqoverdue()
    {
        return $this->reqoverdue;
    }

    /**
     * Set excess
     *
     * @param string $excess
     * @return Vsupply
     */
    public function setExcess($excess)
    {
        $this->excess = $excess;

        return $this;
    }

    /**
     * Get excess
     *
     * @return string 
     */
    public function getExcess()
    {
        return $this->excess;
    }

    /**
     * Set haystype
     *
     * @param string $haystype
     * @return Vsupply
     */
    public function setHaystype($haystype)
    {
        $this->haystype = $haystype;

        return $this;
    }

    /**
     * Get haystype
     *
     * @return string 
     */
    public function getHaystype()
    {
        return $this->haystype;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     * @return Vsupply
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Vsupply
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set createdby
     *
     * @param integer $createdby
     * @return Vsupply
     */
    public function setCreatedby($createdby)
    {
        $this->createdby = $createdby;

        return $this;
    }

    /**
     * Get createdby
     *
     * @return integer 
     */
    public function getCreatedby()
    {
        return $this->createdby;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Vsupply
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set modifiedby
     *
     * @param integer $modifiedby
     * @return Vsupply
     */
    public function setModifiedby($modifiedby)
    {
        $this->modifiedby = $modifiedby;

        return $this;
    }

    /**
     * Get modifiedby
     *
     * @return integer 
     */
    public function getModifiedby()
    {
        return $this->modifiedby;
    }

    /**
     * Set errors
     *
     * @param integer $errors
     * @return Vsupply
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * Get errors
     *
     * @return integer 
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set supterm
     *
     * @param string $supterm
     * @return Vsupply
     */
    public function setSupterm($supterm)
    {
        $this->supterm = $supterm;

        return $this;
    }

    /**
     * Get supterm
     *
     * @return string 
     */
    public function getSupterm()
    {
        return $this->supterm;
    }

    /**
     * Set supcurr
     *
     * @param string $supcurr
     * @return Vsupply
     */
    public function setSupcurr($supcurr)
    {
        $this->supcurr = $supcurr;

        return $this;
    }

    /**
     * Get supcurr
     *
     * @return string 
     */
    public function getSupcurr()
    {
        return $this->supcurr;
    }

    /**
     * Set shipterms
     *
     * @param string $shipterms
     * @return Vsupply
     */
    public function setShipterms($shipterms)
    {
        $this->shipterms = $shipterms;

        return $this;
    }

    /**
     * Get shipterms
     *
     * @return string 
     */
    public function getShipterms()
    {
        return $this->shipterms;
    }

    /**
     * Set settledays
     *
     * @param string $settledays
     * @return Vsupply
     */
    public function setSettledays($settledays)
    {
        $this->settledays = $settledays;

        return $this;
    }

    /**
     * Get settledays
     *
     * @return string 
     */
    public function getSettledays()
    {
        return $this->settledays;
    }

    /**
     * Set cntryorig
     *
     * @param string $cntryorig
     * @return Vsupply
     */
    public function setCntryorig($cntryorig)
    {
        $this->cntryorig = $cntryorig;

        return $this;
    }

    /**
     * Get cntryorig
     *
     * @return string 
     */
    public function getCntryorig()
    {
        return $this->cntryorig;
    }

    /**
     * Set settledisc
     *
     * @param string $settledisc
     * @return Vsupply
     */
    public function setSettledisc($settledisc)
    {
        $this->settledisc = $settledisc;

        return $this;
    }

    /**
     * Get settledisc
     *
     * @return string 
     */
    public function getSettledisc()
    {
        return $this->settledisc;
    }

    /**
     * Set buyerinits
     *
     * @param string $buyerinits
     * @return Vsupply
     */
    public function setBuyerinits($buyerinits)
    {
        $this->buyerinits = $buyerinits;

        return $this;
    }

    /**
     * Get buyerinits
     *
     * @return string 
     */
    public function getBuyerinits()
    {
        return $this->buyerinits;
    }
}
