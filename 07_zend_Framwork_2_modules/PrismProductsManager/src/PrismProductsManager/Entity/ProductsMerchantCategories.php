<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsMerchantCategories
 *
 * @ORM\Table(name="PRODUCTS_Merchant_Categories", indexes={@ORM\Index(name="parentId", columns={"parentId"}), @ORM\Index(name="status", columns={"status"})})
 * @ORM\Entity
 */
class ProductsMerchantCategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productMerchantCategoryId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productmerchantcategoryid;

    /**
     * @var integer
     *
     * @ORM\Column(name="parentId", type="integer", nullable=false)
     */
    private $parentid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var integer
     *
     * @ORM\Column(name="oldCategoryId", type="integer", nullable=false)
     */
    private $oldcategoryid;



    /**
     * Get productmerchantcategoryid
     *
     * @return integer 
     */
    public function getProductmerchantcategoryid()
    {
        return $this->productmerchantcategoryid;
    }

    /**
     * Set parentid
     *
     * @param integer $parentid
     * @return ProductsMerchantCategories
     */
    public function setParentid($parentid)
    {
        $this->parentid = $parentid;

        return $this;
    }

    /**
     * Get parentid
     *
     * @return integer 
     */
    public function getParentid()
    {
        return $this->parentid;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProductsMerchantCategories
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsMerchantCategories
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsMerchantCategories
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set oldcategoryid
     *
     * @param integer $oldcategoryid
     * @return ProductsMerchantCategories
     */
    public function setOldcategoryid($oldcategoryid)
    {
        $this->oldcategoryid = $oldcategoryid;

        return $this;
    }

    /**
     * Get oldcategoryid
     *
     * @return integer 
     */
    public function getOldcategoryid()
    {
        return $this->oldcategoryid;
    }
}
