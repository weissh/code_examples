<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputFieldRValidations
 *
 * @ORM\Table(name="INPUT_Field_R_Validations", indexes={@ORM\Index(name="Index 2", columns={"inputValidationId"}), @ORM\Index(name="Index 3", columns={"inputFieldId"})})
 * @ORM\Entity
 */
class InputFieldRValidations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputFieldRValidationId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputfieldrvalidationid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \PrismProductsManager\Entity\InputValidations
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputValidations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputValidationId", referencedColumnName="inputValidationId")
     * })
     */
    private $inputvalidationid;

    /**
     * @var \PrismProductsManager\Entity\InputFields
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputFields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputFieldId", referencedColumnName="inputFieldId")
     * })
     */
    private $inputfieldid;



    /**
     * Get inputfieldrvalidationid
     *
     * @return integer 
     */
    public function getInputfieldrvalidationid()
    {
        return $this->inputfieldrvalidationid;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputFieldRValidations
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputFieldRValidations
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set inputvalidationid
     *
     * @param \PrismProductsManager\Entity\InputValidations $inputvalidationid
     * @return InputFieldRValidations
     */
    public function setInputvalidationid(\PrismProductsManager\Entity\InputValidations $inputvalidationid = null)
    {
        $this->inputvalidationid = $inputvalidationid;

        return $this;
    }

    /**
     * Get inputvalidationid
     *
     * @return \PrismProductsManager\Entity\InputValidations 
     */
    public function getInputvalidationid()
    {
        return $this->inputvalidationid;
    }

    /**
     * Set inputfieldid
     *
     * @param \PrismProductsManager\Entity\InputFields $inputfieldid
     * @return InputFieldRValidations
     */
    public function setInputfieldid(\PrismProductsManager\Entity\InputFields $inputfieldid = null)
    {
        $this->inputfieldid = $inputfieldid;

        return $this;
    }

    /**
     * Get inputfieldid
     *
     * @return \PrismProductsManager\Entity\InputFields 
     */
    public function getInputfieldid()
    {
        return $this->inputfieldid;
    }
}
