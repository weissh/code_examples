<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QueueEntryTypes
 *
 * @ORM\Table(name="QUEUE_Entry_Types")
 * @ORM\Entity
 */
class QueueEntryTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="queueEntryType", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $queueentrytype;

    /**
     * @var string
     *
     * @ORM\Column(name="queueEntryDefinition", type="string", length=255, nullable=false)
     */
    private $queueentrydefinition;



    /**
     * Get queueentrytype
     *
     * @return integer 
     */
    public function getQueueentrytype()
    {
        return $this->queueentrytype;
    }

    /**
     * Set queueentrydefinition
     *
     * @param string $queueentrydefinition
     * @return QueueEntryTypes
     */
    public function setQueueentrydefinition($queueentrydefinition)
    {
        $this->queueentrydefinition = $queueentrydefinition;

        return $this;
    }

    /**
     * Get queueentrydefinition
     *
     * @return string 
     */
    public function getQueueentrydefinition()
    {
        return $this->queueentrydefinition;
    }
}
