<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsAttributesCombinationDefinitions
 *
 * @ORM\Table(name="PRODUCTS_ATTRIBUTES_Combination_Definitions", indexes={@ORM\Index(name="status", columns={"status"}), @ORM\Index(name="Index 3", columns={"languageId"}), @ORM\Index(name="productAttributeCombinationId", columns={"productAttributeCombinationId"})})
 * @ORM\Entity
 */
class ProductsAttributesCombinationDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productAttributesCombinationDefinitionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productattributescombinationdefinitionid;

    /**
     * @var string
     *
     * @ORM\Column(name="attributeValue", type="string", length=100, nullable=false)
     */
    private $attributevalue;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\ProductsAttributesCombination
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\ProductsAttributesCombination")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="productAttributeCombinationId", referencedColumnName="productAttributeCombinationId")
     * })
     */
    private $productattributecombinationid;



    /**
     * Get productattributescombinationdefinitionid
     *
     * @return integer 
     */
    public function getProductattributescombinationdefinitionid()
    {
        return $this->productattributescombinationdefinitionid;
    }

    /**
     * Set attributevalue
     *
     * @param string $attributevalue
     * @return ProductsAttributesCombinationDefinitions
     */
    public function setAttributevalue($attributevalue)
    {
        $this->attributevalue = $attributevalue;

        return $this;
    }

    /**
     * Get attributevalue
     *
     * @return string 
     */
    public function getAttributevalue()
    {
        return $this->attributevalue;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return ProductsAttributesCombinationDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProductsAttributesCombinationDefinitions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsAttributesCombinationDefinitions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsAttributesCombinationDefinitions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set productattributecombinationid
     *
     * @param \PrismProductsManager\Entity\ProductsAttributesCombination $productattributecombinationid
     * @return ProductsAttributesCombinationDefinitions
     */
    public function setProductattributecombinationid(\PrismProductsManager\Entity\ProductsAttributesCombination $productattributecombinationid = null)
    {
        $this->productattributecombinationid = $productattributecombinationid;

        return $this;
    }

    /**
     * Get productattributecombinationid
     *
     * @return \PrismProductsManager\Entity\ProductsAttributesCombination 
     */
    public function getProductattributecombinationid()
    {
        return $this->productattributecombinationid;
    }
}
