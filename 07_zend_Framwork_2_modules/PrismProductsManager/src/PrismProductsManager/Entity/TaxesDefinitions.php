<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaxesDefinitions
 *
 * @ORM\Table(name="TAXES_Definitions", indexes={@ORM\Index(name="languageId", columns={"languageId"}), @ORM\Index(name="name", columns={"name"}), @ORM\Index(name="taxGroupId", columns={"taxId"}), @ORM\Index(name="languageId_taxGroupId_status", columns={"languageId", "taxId"})})
 * @ORM\Entity
 */
class TaxesDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="taxDefinitionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $taxdefinitionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var integer
     *
     * @ORM\Column(name="taxId", type="integer", nullable=false)
     */
    private $taxid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get taxdefinitionid
     *
     * @return integer 
     */
    public function getTaxdefinitionid()
    {
        return $this->taxdefinitionid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return TaxesDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set taxid
     *
     * @param integer $taxid
     * @return TaxesDefinitions
     */
    public function setTaxid($taxid)
    {
        $this->taxid = $taxid;

        return $this;
    }

    /**
     * Get taxid
     *
     * @return integer 
     */
    public function getTaxid()
    {
        return $this->taxid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TaxesDefinitions
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TaxesDefinitions
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return TaxesDefinitions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return TaxesDefinitions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
