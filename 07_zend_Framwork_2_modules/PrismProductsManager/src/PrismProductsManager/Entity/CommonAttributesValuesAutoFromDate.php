<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributesValuesAutoFromDate
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES_VALUES_Auto_From_Date", indexes={@ORM\Index(name="fromDateProcessed", columns={"fromDate", "processed"}), @ORM\Index(name="COMMON_ATTRIBUTES_VALUES_Auto_From_Date_ibfk_1", columns={"commonAttributeValueId"})})
 * @ORM\Entity
 */
class CommonAttributesValuesAutoFromDate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributeValueAutoFromDateId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commonAttributeValueAutoFromDateId;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributesValues
     *
     * @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesValues" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="commonAttributeValueId", referencedColumnName="commonAttributeValueId")
     * })
     */
    private $commonAttributeValueId;

    /**
     * @var bool $processed
     *
     * @ORM\Column(name="processed", type="boolean", nullable=false)
     */
    private $processed;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fromDate", type="datetime", nullable=true)
     */
    private $fromDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=true)
     */
    private $modifiedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @return int
     */
    public function getCommonAttributeValueAutoFromDateId()
    {
        return $this->commonAttributeValueAutoFromDateId;
    }

    /**
     * @param int $commonAttributeValueAutoFromDateId
     */
    public function setCommonAttributeValueAutoFromDateId($commonAttributeValueAutoFromDateId)
    {
        $this->commonAttributeValueAutoFromDateId = $commonAttributeValueAutoFromDateId;
    }

    /**
     * @return CommonAttributesValues
     */
    public function getCommonAttributeValueId()
    {
        return $this->commonAttributeValueId;
    }

    /**
     * @param CommonAttributesValues $commonAttributeValueId
     */
    public function setCommonAttributeValueId($commonAttributeValueId)
    {
        $this->commonAttributeValueId = $commonAttributeValueId;
    }

    /**
     * @return \DateTime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * @param \DateTime $fromDate
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return boolean
     */
    public function isProcessed()
    {
        return $this->processed;
    }

    /**
     * @param boolean $processed
     */
    public function setProcessed($processed)
    {
        $this->processed = $processed;
    }
}
