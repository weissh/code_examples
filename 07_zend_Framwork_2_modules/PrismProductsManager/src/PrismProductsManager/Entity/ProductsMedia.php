<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsMedia
 *
 * @ORM\Table(name="PRODUCTS_Media", indexes={@ORM\Index(name="mediaServerId", columns={"mediaServerId"}), @ORM\Index(name="mediaPriority", columns={"mediaPriority"})})
 * @ORM\Entity
 */
class ProductsMedia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productsMediaId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productsmediaid;

    /**
     * @var integer
     *
     * @ORM\Column(name="productsId", type="integer", nullable=false)
     */
    private $productsid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="mediaTypeId", type="boolean", nullable=false)
     */
    private $mediatypeid;

    /**
     * @var integer
     *
     * @ORM\Column(name="mediaServerId", type="integer", nullable=false)
     */
    private $mediaserverid;

    /**
     * @var integer
     *
     * @ORM\Column(name="mediaPriority", type="integer", nullable=false)
     */
    private $mediapriority;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDefaultMedia", type="boolean", nullable=false)
     */
    private $isdefaultmedia;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isBasketImage", type="boolean", nullable=false)
     */
    private $isbasketimage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isHoverImage", type="boolean", nullable=false)
     */
    private $ishoverimage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isExternal", type="boolean", nullable=false)
     */
    private $isexternal;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get productsmediaid
     *
     * @return integer 
     */
    public function getProductsmediaid()
    {
        return $this->productsmediaid;
    }

    /**
     * Set productsid
     *
     * @param integer $productsid
     * @return ProductsMedia
     */
    public function setProductsid($productsid)
    {
        $this->productsid = $productsid;

        return $this;
    }

    /**
     * Get productsid
     *
     * @return integer 
     */
    public function getProductsid()
    {
        return $this->productsid;
    }

    /**
     * Set mediatypeid
     *
     * @param boolean $mediatypeid
     * @return ProductsMedia
     */
    public function setMediatypeid($mediatypeid)
    {
        $this->mediatypeid = $mediatypeid;

        return $this;
    }

    /**
     * Get mediatypeid
     *
     * @return boolean 
     */
    public function getMediatypeid()
    {
        return $this->mediatypeid;
    }

    /**
     * Set mediaserverid
     *
     * @param integer $mediaserverid
     * @return ProductsMedia
     */
    public function setMediaserverid($mediaserverid)
    {
        $this->mediaserverid = $mediaserverid;

        return $this;
    }

    /**
     * Get mediaserverid
     *
     * @return integer 
     */
    public function getMediaserverid()
    {
        return $this->mediaserverid;
    }

    /**
     * Set mediapriority
     *
     * @param integer $mediapriority
     * @return ProductsMedia
     */
    public function setMediapriority($mediapriority)
    {
        $this->mediapriority = $mediapriority;

        return $this;
    }

    /**
     * Get mediapriority
     *
     * @return integer 
     */
    public function getMediapriority()
    {
        return $this->mediapriority;
    }

    /**
     * Set isdefaultmedia
     *
     * @param boolean $isdefaultmedia
     * @return ProductsMedia
     */
    public function setIsdefaultmedia($isdefaultmedia)
    {
        $this->isdefaultmedia = $isdefaultmedia;

        return $this;
    }

    /**
     * Get isdefaultmedia
     *
     * @return boolean 
     */
    public function getIsdefaultmedia()
    {
        return $this->isdefaultmedia;
    }

    /**
     * Set isbasketimage
     *
     * @param boolean $isbasketimage
     * @return ProductsMedia
     */
    public function setIsbasketimage($isbasketimage)
    {
        $this->isbasketimage = $isbasketimage;

        return $this;
    }

    /**
     * Get isbasketimage
     *
     * @return boolean 
     */
    public function getIsbasketimage()
    {
        return $this->isbasketimage;
    }

    /**
     * Set ishoverimage
     *
     * @param boolean $ishoverimage
     * @return ProductsMedia
     */
    public function setIshoverimage($ishoverimage)
    {
        $this->ishoverimage = $ishoverimage;

        return $this;
    }

    /**
     * Get ishoverimage
     *
     * @return boolean 
     */
    public function getIshoverimage()
    {
        return $this->ishoverimage;
    }

    /**
     * Set isexternal
     *
     * @param boolean $isexternal
     * @return ProductsMedia
     */
    public function setIsexternal($isexternal)
    {
        $this->isexternal = $isexternal;

        return $this;
    }

    /**
     * Get isexternal
     *
     * @return boolean 
     */
    public function getIsexternal()
    {
        return $this->isexternal;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProductsMedia
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsMedia
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsMedia
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
