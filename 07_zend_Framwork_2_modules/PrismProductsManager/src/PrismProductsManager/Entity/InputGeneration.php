<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputGeneration
 *
 * @ORM\Table(name="INPUT_Generation", indexes={@ORM\Index(name="FK_INPUT_Generation_INPUT_Fields", columns={"inputFieldId"})})
 * @ORM\Entity
 */
class InputGeneration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputOptionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputoptionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="inputOptionParentId", type="integer", nullable=false)
     */
    private $inputoptionparentid;

    /**
     * @var string
     *
     * @ORM\Column(name="inputOptionKey", type="string", length=250, nullable=true)
     */
    private $inputoptionkey;

    /**
     * @var string
     *
     * @ORM\Column(name="inputOptionValue", type="string", length=250, nullable=true)
     */
    private $inputoptionvalue;

    /**
     * @var string
     *
     * @ORM\Column(name="inputOptionType", type="string", length=100, nullable=true)
     */
    private $inputoptiontype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \PrismProductsManager\Entity\InputFields
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputFields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputFieldId", referencedColumnName="inputFieldId")
     * })
     */
    private $inputfieldid;



    /**
     * Get inputoptionid
     *
     * @return integer 
     */
    public function getInputoptionid()
    {
        return $this->inputoptionid;
    }

    /**
     * Set inputoptionparentid
     *
     * @param integer $inputoptionparentid
     * @return InputGeneration
     */
    public function setInputoptionparentid($inputoptionparentid)
    {
        $this->inputoptionparentid = $inputoptionparentid;

        return $this;
    }

    /**
     * Get inputoptionparentid
     *
     * @return integer 
     */
    public function getInputoptionparentid()
    {
        return $this->inputoptionparentid;
    }

    /**
     * Set inputoptionkey
     *
     * @param string $inputoptionkey
     * @return InputGeneration
     */
    public function setInputoptionkey($inputoptionkey)
    {
        $this->inputoptionkey = $inputoptionkey;

        return $this;
    }

    /**
     * Get inputoptionkey
     *
     * @return string 
     */
    public function getInputoptionkey()
    {
        return $this->inputoptionkey;
    }

    /**
     * Set inputoptionvalue
     *
     * @param string $inputoptionvalue
     * @return InputGeneration
     */
    public function setInputoptionvalue($inputoptionvalue)
    {
        $this->inputoptionvalue = $inputoptionvalue;

        return $this;
    }

    /**
     * Get inputoptionvalue
     *
     * @return string 
     */
    public function getInputoptionvalue()
    {
        return $this->inputoptionvalue;
    }

    /**
     * Set inputoptiontype
     *
     * @param string $inputoptiontype
     * @return InputGeneration
     */
    public function setInputoptiontype($inputoptiontype)
    {
        $this->inputoptiontype = $inputoptiontype;

        return $this;
    }

    /**
     * Get inputoptiontype
     *
     * @return string 
     */
    public function getInputoptiontype()
    {
        return $this->inputoptiontype;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputGeneration
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputGeneration
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set inputfieldid
     *
     * @param \PrismProductsManager\Entity\InputFields $inputfieldid
     * @return InputGeneration
     */
    public function setInputfieldid(\PrismProductsManager\Entity\InputFields $inputfieldid = null)
    {
        $this->inputfieldid = $inputfieldid;

        return $this;
    }

    /**
     * Get inputfieldid
     *
     * @return \PrismProductsManager\Entity\InputFields 
     */
    public function getInputfieldid()
    {
        return $this->inputfieldid;
    }
}
