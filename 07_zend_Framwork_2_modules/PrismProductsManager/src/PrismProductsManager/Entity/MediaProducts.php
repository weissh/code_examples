<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MediaProducts
 *
 * @ORM\Table(name="MEDIA_Products", uniqueConstraints={@ORM\UniqueConstraint(name="Index 4", columns={"productId", "variantId"})})
 * @ORM\Entity
 */
class MediaProducts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="productId", type="integer", nullable=false)
     */
    private $productid;

    /**
     * @var integer
     *
     * @ORM\Column(name="variantId", type="integer", nullable=false)
     */
    private $variantid;

    /**
     * @var integer
     *
     * @ORM\Column(name="mediaFolderId", type="integer", nullable=false)
     */
    private $mediafolderid;

    /**
     * @var integer
     *
     * @ORM\Column(name="defaultImageId", type="integer", nullable=true)
     */
    private $defaultimageid;

    /**
     * @var integer
     *
     * @ORM\Column(name="basketImageId", type="integer", nullable=true)
     */
    private $basketimageid;

    /**
     * @var integer
     *
     * @ORM\Column(name="menuImageId", type="integer", nullable=true)
     */
    private $menuimageid;

    /**
     * @var integer
     *
     * @ORM\Column(name="hoverImageId", type="integer", nullable=true)
     */
    private $hoverimageid;

    /**
     * @var integer
     *
     * @ORM\Column(name="overlayImageId", type="integer", nullable=true)
     */
    private $overlayimageid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return MediaProducts
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set variantid
     *
     * @param integer $variantid
     * @return MediaProducts
     */
    public function setVariantid($variantid)
    {
        $this->variantid = $variantid;

        return $this;
    }

    /**
     * Get variantid
     *
     * @return integer 
     */
    public function getVariantid()
    {
        return $this->variantid;
    }

    /**
     * Set mediafolderid
     *
     * @param integer $mediafolderid
     * @return MediaProducts
     */
    public function setMediafolderid($mediafolderid)
    {
        $this->mediafolderid = $mediafolderid;

        return $this;
    }

    /**
     * Get mediafolderid
     *
     * @return integer 
     */
    public function getMediafolderid()
    {
        return $this->mediafolderid;
    }

    /**
     * Set defaultimageid
     *
     * @param integer $defaultimageid
     * @return MediaProducts
     */
    public function setDefaultimageid($defaultimageid)
    {
        $this->defaultimageid = $defaultimageid;

        return $this;
    }

    /**
     * Get defaultimageid
     *
     * @return integer 
     */
    public function getDefaultimageid()
    {
        return $this->defaultimageid;
    }

    /**
     * Set basketimageid
     *
     * @param integer $basketimageid
     * @return MediaProducts
     */
    public function setBasketimageid($basketimageid)
    {
        $this->basketimageid = $basketimageid;

        return $this;
    }

    /**
     * Get basketimageid
     *
     * @return integer 
     */
    public function getBasketimageid()
    {
        return $this->basketimageid;
    }

    /**
     * Set menuimageid
     *
     * @param integer $menuimageid
     * @return MediaProducts
     */
    public function setMenuimageid($menuimageid)
    {
        $this->menuimageid = $menuimageid;

        return $this;
    }

    /**
     * Get menuimageid
     *
     * @return integer 
     */
    public function getMenuimageid()
    {
        return $this->menuimageid;
    }

    /**
     * Set hoverimageid
     *
     * @param integer $hoverimageid
     * @return MediaProducts
     */
    public function setHoverimageid($hoverimageid)
    {
        $this->hoverimageid = $hoverimageid;

        return $this;
    }

    /**
     * Get hoverimageid
     *
     * @return integer 
     */
    public function getHoverimageid()
    {
        return $this->hoverimageid;
    }

    /**
     * Set overlayimageid
     *
     * @param integer $overlayimageid
     * @return MediaProducts
     */
    public function setOverlayimageid($overlayimageid)
    {
        $this->overlayimageid = $overlayimageid;

        return $this;
    }

    /**
     * Get overlayimageid
     *
     * @return integer 
     */
    public function getOverlayimageid()
    {
        return $this->overlayimageid;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return MediaProducts
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return MediaProducts
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return MediaProducts
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
