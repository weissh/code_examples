<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ScheduleTrail
 *
 * @ORM\Table(name="SCHEDULE_TRAIL", indexes={@ORM\Index(name="FK_SCHEDULE_TRAIL_SCHEDULE_TYPES", columns={"scheduleTypeId"}), @ORM\Index(name="FK_SCHEDULE_TRAIL_QUEUE_Manager", columns={"queueManagerId"}), @ORM\Index(name="Index 4", columns={"productOrVariantId", "productIdFrom"})})
 * @ORM\Entity
 */
class ScheduleTrail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="scheduleTrailId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $scheduleTrailId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="scheduleStartDate", type="datetime", nullable=false)
     */
    private $scheduleStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="scheduleUpdatedDate", type="datetime", nullable=false)
     */
    private $scheduleUpdatedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="productOrVariantId", type="integer", nullable=false)
     */
    private $productOrVariantId;

    /**
     * @var integer
     *
     * @ORM\Column(name="productIdFrom", type="string", length=75, nullable=true)
     */
    private $productIdFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="updatedJsonBody", type="integer", nullable=false)
     */
    private $updatedJsonBody;

    /**
     * @var integer
     *
     * @ORM\Column(name="scheduledBy", type="integer", nullable=false)
     */
    private $scheduleBby;

    /**
     * @var boolean
     *
     * @ORM\Column(name="queueManagerId", type="string", length=50, nullable=true)
     */
    private $queueManagerId;

    /**
     * @var \PrismProductsManager\Entity\ScheduleTypes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\ScheduleTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="scheduleTypeId", referencedColumnName="scheduleTypeId")
     * })
     */
    private $scheduleTypeId;

    /**
     * @return \DateTime
     */
    public function getScheduleStartDate()
    {
        return $this->scheduleStartDate;
    }

    /**
     * @param \DateTime $scheduleStartDate
     */
    public function setScheduleStartDate($scheduleStartDate)
    {
        $this->scheduleStartDate = $scheduleStartDate;
    }

    /**
     * @return \DateTime
     */
    public function getScheduleUpdatedDate()
    {
        return $this->scheduleUpdatedDate;
    }

    /**
     * @param \DateTime $scheduleUpdatedDate
     */
    public function setScheduleUpdatedDate($scheduleUpdatedDate)
    {
        $this->scheduleUpdatedDate = $scheduleUpdatedDate;
    }

    /**
     * @return int
     */
    public function getProductOrVariantId()
    {
        return $this->productOrVariantId;
    }

    /**
     * @param int $productOrVariantId
     */
    public function setProductOrVariantId($productOrVariantId)
    {
        $this->productOrVariantId = $productOrVariantId;
    }

    /**
     * @return int
     */
    public function getProductIdFrom()
    {
        return $this->productIdFrom;
    }

    /**
     * @param int $productIdFrom
     */
    public function setProductIdFrom($productIdFrom)
    {
        $this->productIdFrom = $productIdFrom;
    }

    /**
     * @return int
     */
    public function getUpdatedJsonBody()
    {
        return $this->updatedJsonBody;
    }

    /**
     * @param int $updatedJsonBody
     */
    public function setUpdatedJsonBody($updatedJsonBody)
    {
        $this->updatedJsonBody = $updatedJsonBody;
    }

    /**
     * @return int
     */
    public function getScheduleBby()
    {
        return $this->scheduleBby;
    }

    /**
     * @param int $scheduleBby
     */
    public function setScheduleBby($scheduleBby)
    {
        $this->scheduleBby = $scheduleBby;
    }

    /**
     * @return QueueManager
     */
    public function getQueueManagerId()
    {
        return $this->queueManagerId;
    }

    /**
     * @param string $queueManagerId
     */
    public function setQueueManagerId($queueManagerId)
    {
        $this->queueManagerId = $queueManagerId;
    }

    /**
     * @return ScheduleTypes
     */
    public function getScheduleTypeId()
    {
        return $this->scheduleTypeId;
    }

    /**
     * @param ScheduleTypes $scheduleTypeId
     */
    public function setScheduleTypeId($scheduleTypeId)
    {
        $this->scheduleTypeId = $scheduleTypeId;
    }

    /**
     * @return int
     */
    public function getScheduleTrailId()
    {
        return $this->scheduleTrailId;
    }
}