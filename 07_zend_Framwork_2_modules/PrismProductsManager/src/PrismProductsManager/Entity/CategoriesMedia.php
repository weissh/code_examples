<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriesMedia
 *
 * @ORM\Table(name="CATEGORIES_Media", indexes={@ORM\Index(name="mediaPriority", columns={"mediaPriority"}), @ORM\Index(name="fk_key", columns={"categoryId"})})
 * @ORM\Entity
 */
class CategoriesMedia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="categoryMediaId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $categorymediaid;

    /**
     * @var string
     *
     * @ORM\Column(name="imageId", type="string", length=45, nullable=false)
     */
    private $imageid;

    /**
     * @var integer
     *
     * @ORM\Column(name="mediaPriority", type="integer", nullable=false)
     */
    private $mediapriority;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\Categories
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Categories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoryId", referencedColumnName="categoryId")
     * })
     */
    private $categoryid;



    /**
     * Get categorymediaid
     *
     * @return integer 
     */
    public function getCategorymediaid()
    {
        return $this->categorymediaid;
    }

    /**
     * Set imageid
     *
     * @param string $imageid
     * @return CategoriesMedia
     */
    public function setImageid($imageid)
    {
        $this->imageid = $imageid;

        return $this;
    }

    /**
     * Get imageid
     *
     * @return string 
     */
    public function getImageid()
    {
        return $this->imageid;
    }

    /**
     * Set mediapriority
     *
     * @param integer $mediapriority
     * @return CategoriesMedia
     */
    public function setMediapriority($mediapriority)
    {
        $this->mediapriority = $mediapriority;

        return $this;
    }

    /**
     * Get mediapriority
     *
     * @return integer 
     */
    public function getMediapriority()
    {
        return $this->mediapriority;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return CategoriesMedia
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return CategoriesMedia
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set categoryid
     *
     * @param \PrismProductsManager\Entity\Categories $categoryid
     * @return CategoriesMedia
     */
    public function setCategoryid(\PrismProductsManager\Entity\Categories $categoryid = null)
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    /**
     * Get categoryid
     *
     * @return \PrismProductsManager\Entity\Categories 
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }
}
