<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputModules
 *
 * @ORM\Table(name="INPUT_Modules")
 * @ORM\Entity
 */
class InputModules
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputModuleId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputmoduleid;

    /**
     * @var string
     *
     * @ORM\Column(name="inputModuleName", type="string", length=70, nullable=true)
     */
    private $inputmodulename;

    /**
     * @var string
     *
     * @ORM\Column(name="inputModuleRef", type="string", length=70, nullable=true)
     */
    private $inputmoduleref;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;



    /**
     * Get inputmoduleid
     *
     * @return integer 
     */
    public function getInputmoduleid()
    {
        return $this->inputmoduleid;
    }

    /**
     * Set inputmodulename
     *
     * @param string $inputmodulename
     * @return InputModules
     */
    public function setInputmodulename($inputmodulename)
    {
        $this->inputmodulename = $inputmodulename;

        return $this;
    }

    /**
     * Get inputmodulename
     *
     * @return string 
     */
    public function getInputmodulename()
    {
        return $this->inputmodulename;
    }

    /**
     * Set inputmoduleref
     *
     * @param string $inputmoduleref
     * @return InputModules
     */
    public function setInputmoduleref($inputmoduleref)
    {
        $this->inputmoduleref = $inputmoduleref;

        return $this;
    }

    /**
     * Get inputmoduleref
     *
     * @return string 
     */
    public function getInputmoduleref()
    {
        return $this->inputmoduleref;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputModules
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputModules
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }
}
