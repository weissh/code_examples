<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsBarcodes
 *
 * @ORM\Table(name="PRODUCTS_Barcodes", uniqueConstraints={@ORM\UniqueConstraint(name="barcode", columns={"barcode"})})
 * @ORM\Entity
 */
class ProductsBarcodes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="barcodeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $barcodeid;

    /**
     * @var string
     *
     * @ORM\Column(name="barcode", type="string", length=100, nullable=true)
     */
    private $barcode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="inUse", type="boolean", nullable=true)
     */
    private $inuse;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;



    /**
     * Get barcodeid
     *
     * @return integer 
     */
    public function getBarcodeid()
    {
        return $this->barcodeid;
    }

    /**
     * Set barcode
     *
     * @param string $barcode
     * @return ProductsBarcodes
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;

        return $this;
    }

    /**
     * Get barcode
     *
     * @return string 
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * Set inuse
     *
     * @param boolean $inuse
     * @return ProductsBarcodes
     */
    public function setInuse($inuse)
    {
        $this->inuse = $inuse;

        return $this;
    }

    /**
     * Get inuse
     *
     * @return boolean 
     */
    public function getInuse()
    {
        return $this->inuse;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsBarcodes
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsBarcodes
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
