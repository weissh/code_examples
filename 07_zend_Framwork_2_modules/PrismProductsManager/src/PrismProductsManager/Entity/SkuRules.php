<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SkuRules
 *
 * @ORM\Table(name="SKU_Rules", indexes={@ORM\Index(name="merchantCategoryId", columns={"merchantCategoryId"}), @ORM\Index(name="merchantAttributeGroupId", columns={"merchantAttributeGroupId"})})
 * @ORM\Entity
 */
class SkuRules
{
    /**
     * @var integer
     *
     * @ORM\Column(name="skuRuleId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $skuruleid;

    /**
     * @var integer
     *
     * @ORM\Column(name="merchantAttributeGroupId", type="integer", nullable=false)
     */
    private $merchantattributegroupid;

    /**
     * @var integer
     *
     * @ORM\Column(name="isCommonAttribute", type="integer", nullable=false)
     */
    private $iscommonattribute;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var \PrismProductsManager\Entity\ProductsMerchantCategories
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\ProductsMerchantCategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="merchantCategoryId", referencedColumnName="productMerchantCategoryId")
     * })
     */
    private $merchantcategoryid;



    /**
     * Get skuruleid
     *
     * @return integer 
     */
    public function getSkuruleid()
    {
        return $this->skuruleid;
    }

    /**
     * Set merchantattributegroupid
     *
     * @param integer $merchantattributegroupid
     * @return SkuRules
     */
    public function setMerchantattributegroupid($merchantattributegroupid)
    {
        $this->merchantattributegroupid = $merchantattributegroupid;

        return $this;
    }

    /**
     * Get merchantattributegroupid
     *
     * @return integer 
     */
    public function getMerchantattributegroupid()
    {
        return $this->merchantattributegroupid;
    }

    /**
     * Set iscommonattribute
     *
     * @param integer $iscommonattribute
     * @return SkuRules
     */
    public function setIscommonattribute($iscommonattribute)
    {
        $this->iscommonattribute = $iscommonattribute;

        return $this;
    }

    /**
     * Get iscommonattribute
     *
     * @return integer 
     */
    public function getIscommonattribute()
    {
        return $this->iscommonattribute;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return SkuRules
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set merchantcategoryid
     *
     * @param \PrismProductsManager\Entity\ProductsMerchantCategories $merchantcategoryid
     * @return SkuRules
     */
    public function setMerchantcategoryid(\PrismProductsManager\Entity\ProductsMerchantCategories $merchantcategoryid = null)
    {
        $this->merchantcategoryid = $merchantcategoryid;

        return $this;
    }

    /**
     * Get merchantcategoryid
     *
     * @return \PrismProductsManager\Entity\ProductsMerchantCategories 
     */
    public function getMerchantcategoryid()
    {
        return $this->merchantcategoryid;
    }
}
