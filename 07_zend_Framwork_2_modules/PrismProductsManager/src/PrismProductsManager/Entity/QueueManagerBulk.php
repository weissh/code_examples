<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QueueManagerBulk
 *
 * @ORM\Table(name="QUEUE_Manager_Bulk")
 * @ORM\Entity
 */
class QueueManagerBulk
{
    /**
     * @var integer
     *
     * @ORM\Column(name="queueBulkId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $queuebulkid;

    /**
     * @var string
     *
     * @ORM\Column(name="dateScriptRun", type="string", length=50, nullable=false)
     */
    private $datescriptrun;

    /**
     * @var integer
     *
     * @ORM\Column(name="productsCount", type="integer", nullable=false)
     */
    private $productscount;

    /**
     * @var integer
     *
     * @ORM\Column(name="fromProduct", type="integer", nullable=false)
     */
    private $fromproduct;

    /**
     * @var integer
     *
     * @ORM\Column(name="toProduct", type="integer", nullable=false)
     */
    private $toproduct;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get queuebulkid
     *
     * @return integer 
     */
    public function getQueuebulkid()
    {
        return $this->queuebulkid;
    }

    /**
     * Set datescriptrun
     *
     * @param string $datescriptrun
     * @return QueueManagerBulk
     */
    public function setDatescriptrun($datescriptrun)
    {
        $this->datescriptrun = $datescriptrun;

        return $this;
    }

    /**
     * Get datescriptrun
     *
     * @return string 
     */
    public function getDatescriptrun()
    {
        return $this->datescriptrun;
    }

    /**
     * Set productscount
     *
     * @param integer $productscount
     * @return QueueManagerBulk
     */
    public function setProductscount($productscount)
    {
        $this->productscount = $productscount;

        return $this;
    }

    /**
     * Get productscount
     *
     * @return integer 
     */
    public function getProductscount()
    {
        return $this->productscount;
    }

    /**
     * Set fromproduct
     *
     * @param integer $fromproduct
     * @return QueueManagerBulk
     */
    public function setFromproduct($fromproduct)
    {
        $this->fromproduct = $fromproduct;

        return $this;
    }

    /**
     * Get fromproduct
     *
     * @return integer 
     */
    public function getFromproduct()
    {
        return $this->fromproduct;
    }

    /**
     * Set toproduct
     *
     * @param integer $toproduct
     * @return QueueManagerBulk
     */
    public function setToproduct($toproduct)
    {
        $this->toproduct = $toproduct;

        return $this;
    }

    /**
     * Get toproduct
     *
     * @return integer 
     */
    public function getToproduct()
    {
        return $this->toproduct;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return QueueManagerBulk
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
