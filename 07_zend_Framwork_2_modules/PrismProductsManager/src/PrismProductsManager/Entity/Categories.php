<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categories
 *
 * @ORM\Table(name="CATEGORIES", indexes={@ORM\Index(name="categoryAndParentCategory", columns={"categoryParentId", "categoryId"})})
 * @ORM\Entity
 */
class Categories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="categoryId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $categoryid;

    /**
     * @var integer
     *
     * @ORM\Column(name="categoryParentId", type="integer", nullable=false)
     */
    private $categoryparentid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="treeDepth", type="boolean", nullable=false)
     */
    private $treedepth;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var integer
     *
     * @ORM\Column(name="oldCategoryId", type="integer", nullable=false)
     */
    private $oldcategoryid;



    /**
     * Get categoryid
     *
     * @return integer 
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }

    /**
     * Set categoryparentid
     *
     * @param integer $categoryparentid
     * @return Categories
     */
    public function setCategoryparentid($categoryparentid)
    {
        $this->categoryparentid = $categoryparentid;

        return $this;
    }

    /**
     * Get categoryparentid
     *
     * @return integer 
     */
    public function getCategoryparentid()
    {
        return $this->categoryparentid;
    }

    /**
     * Set treedepth
     *
     * @param boolean $treedepth
     * @return Categories
     */
    public function setTreedepth($treedepth)
    {
        $this->treedepth = $treedepth;

        return $this;
    }

    /**
     * Get treedepth
     *
     * @return boolean 
     */
    public function getTreedepth()
    {
        return $this->treedepth;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Categories
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Categories
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Categories
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Categories
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set oldcategoryid
     *
     * @param integer $oldcategoryid
     * @return Categories
     */
    public function setOldcategoryid($oldcategoryid)
    {
        $this->oldcategoryid = $oldcategoryid;

        return $this;
    }

    /**
     * Get oldcategoryid
     *
     * @return integer 
     */
    public function getOldcategoryid()
    {
        return $this->oldcategoryid;
    }
}
