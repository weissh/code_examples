<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Workflows
 *
 * @ORM\Table(name="WORKFLOWS", indexes={@ORM\Index(name="Index 2", columns={"workflowPriority"})})
 * @ORM\Entity
 */
class Workflows
{
    /**
     * @var integer
     *
     * @ORM\Column(name="workflowId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $workflowid;

    /**
     * @var string
     *
     * @ORM\Column(name="workflowDescription", type="string", length=100, nullable=false)
     */
    private $workflowdescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="workflowPriority", type="integer", nullable=false)
     */
    private $workflowpriority;

    /**
     * @var integer
     *
     * @ORM\Column(name="percentagePart", type="integer", nullable=false)
     */
    private $percentagepart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     * @var string
     *
     * @ORM\Column(name="hexCode", type="string", length=7, nullable=true)
     */
    private $hexcode;

    /**
     * @return string
     */
    public function getHexcode()
    {
        return $this->hexcode;
    }

    /**
     * @param string $hexcode
     */
    public function setHexcode($hexcode)
    {
        $this->hexcode = $hexcode;
    }


    /**
     * Get workflowid
     *
     * @return integer 
     */
    public function getWorkflowid()
    {
        return $this->workflowid;
    }

    /**
     * Set workflowdescription
     *
     * @param string $workflowdescription
     * @return Workflows
     */
    public function setWorkflowdescription($workflowdescription)
    {
        $this->workflowdescription = $workflowdescription;

        return $this;
    }

    /**
     * Get workflowdescription
     *
     * @return string 
     */
    public function getWorkflowdescription()
    {
        return $this->workflowdescription;
    }

    /**
     * Set workflowpriority
     *
     * @param integer $workflowpriority
     * @return Workflows
     */
    public function setWorkflowpriority($workflowpriority)
    {
        $this->workflowpriority = $workflowpriority;

        return $this;
    }

    /**
     * Get workflowpriority
     *
     * @return integer 
     */
    public function getWorkflowpriority()
    {
        return $this->workflowpriority;
    }

    /**
     * Set percentagepart
     *
     * @param integer $percentagepart
     * @return Workflows
     */
    public function setPercentagepart($percentagepart)
    {
        $this->percentagepart = $percentagepart;

        return $this;
    }

    /**
     * Get percentagepart
     *
     * @return integer 
     */
    public function getPercentagepart()
    {
        return $this->percentagepart;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return Workflows
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return Workflows
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }
}
