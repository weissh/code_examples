<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputFieldTypes
 *
 * @ORM\Table(name="INPUT_Field_Types")
 * @ORM\Entity
 */
class InputFieldTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputFieldTypeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputfieldtypeid;

    /**
     * @var string
     *
     * @ORM\Column(name="inputFieldTypeName", type="string", length=45, nullable=false)
     */
    private $inputfieldtypename;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_At", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_At", type="datetime", nullable=false)
     */
    private $modifiedAt;



    /**
     * Get inputfieldtypeid
     *
     * @return integer 
     */
    public function getInputfieldtypeid()
    {
        return $this->inputfieldtypeid;
    }

    /**
     * Set inputfieldtypename
     *
     * @param string $inputfieldtypename
     * @return InputFieldTypes
     */
    public function setInputfieldtypename($inputfieldtypename)
    {
        $this->inputfieldtypename = $inputfieldtypename;

        return $this;
    }

    /**
     * Get inputfieldtypename
     *
     * @return string 
     */
    public function getInputfieldtypename()
    {
        return $this->inputfieldtypename;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return InputFieldTypes
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return InputFieldTypes
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }
}
