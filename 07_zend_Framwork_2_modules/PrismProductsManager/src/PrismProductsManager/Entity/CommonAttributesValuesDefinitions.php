<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributesValuesDefinitions
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES_VALUES_Definitions", indexes={@ORM\Index(name="Index 3", columns={"languageId"}), @ORM\Index(name="COMMON_ATTRIBUTES_VALUES_Definitions_ibfk_1", columns={"commonAttributeValueId"})})
 * @ORM\Entity
 */
class CommonAttributesValuesDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributesValuesDefinitionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commonattributesvaluesdefinitionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=75, nullable=true)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=true)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributesValues
     *
     * @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesValues" , inversedBy="commonAttributeValueId")
     * @ORM\JoinColumn(name="commonAttributeValueId", referencedColumnName="commonAttributeValueId")
     */
    private $commonAttributeValueId;

    /**
     * Get commonattributesvaluesdefinitionid
     *
     * @return integer 
     */
    public function getCommonattributesvaluesdefinitionid()
    {
        return $this->commonattributesvaluesdefinitionid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return CommonAttributesValuesDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return CommonAttributesValuesDefinitions
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return CommonAttributesValuesDefinitions
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return CommonAttributesValuesDefinitions
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set commonAttributeValueId
     *
     * @param \PrismProductsManager\Entity\CommonAttributesValues $commonAttributeValueId
     * @return CommonAttributesValuesDefinitions
     */
    public function setcommonAttributeValueId(\PrismProductsManager\Entity\CommonAttributesValues $commonAttributeValueId = null)
    {
        $this->commonAttributeValueId = $commonAttributeValueId;

        return $this;
    }

    /**
     * Get commonAttributeValueId
     *
     * @return \PrismProductsManager\Entity\CommonAttributesValues 
     */
    public function getcommonAttributeValueId()
    {
        return $this->commonAttributeValueId;
    }
}
