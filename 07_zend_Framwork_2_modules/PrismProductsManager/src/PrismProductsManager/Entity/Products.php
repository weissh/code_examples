<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Products
 *
 * @ORM\Table(name="PRODUCTS", indexes={@ORM\Index(name="manufacturerId", columns={"manufacturerId"}), @ORM\Index(name="taxId", columns={"taxId"}), @ORM\Index(name="created", columns={"created"}), @ORM\Index(name="FK_PRODUCTS_PRODUCTS_Merchant_Categories", columns={"productMerchantCategoryId"}), @ORM\Index(name="FK_PRODUCTS_PRODUCTS_Types", columns={"productTypeId"})})
 * @ORM\Entity
 */
class Products
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productid;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturerId", type="integer", nullable=true)
     */
    private $manufacturerid;

    /**
     * @var integer
     *
     * @ORM\Column(name="taxId", type="integer", nullable=false)
     */
    private $taxid;

    /**
     * @var string
     *
     * @ORM\Column(name="ean13", type="string", length=30, nullable=true)
     */
    private $ean13;

    /**
     * @var string
     *
     * @ORM\Column(name="ecoTax", type="decimal", precision=17, scale=2, nullable=false)
     */
    private $ecotax;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="style", type="string", length=30, nullable=false)
     */
    private $style;

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=32, nullable=true)
     */
    private $sku;

    /**
     * @var string
     *
     * @ORM\Column(name="supplierReference", type="string", length=32, nullable=true)
     */
    private $supplierreference;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer", nullable=true)
     */
    private $weight;

    /**
     * @var integer
     *
     * @ORM\Column(name="onSale", type="integer", nullable=true)
     */
    private $onsale;

    /**
     * @var integer
     *
     * @ORM\Column(name="outOfStock", type="integer", nullable=true)
     */
    private $outofstock;

    /**
     * @var integer
     *
     * @ORM\Column(name="allowBackOrder", type="integer", nullable=true)
     */
    private $allowbackorder;

    /**
     * @var integer
     *
     * @ORM\Column(name="customizable", type="integer", nullable=true)
     */
    private $customizable;

    /**
     * @var integer
     *
     * @ORM\Column(name="uploadableFiles", type="integer", nullable=true)
     */
    private $uploadablefiles;

    /**
     * @var integer
     *
     * @ORM\Column(name="applyPostage", type="integer", nullable=true)
     */
    private $applypostage;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantityDiscount", type="integer", nullable=true)
     */
    private $quantitydiscount;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\ProductsMerchantCategories
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\ProductsMerchantCategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="productMerchantCategoryId", referencedColumnName="productMerchantCategoryId")
     * })
     */
    private $productmerchantcategoryid;

    /**
     * @var \PrismProductsManager\Entity\ProductsTypes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\ProductsTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="productTypeId", referencedColumnName="productTypeId")
     * })
     */
    private $producttypeid;



    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set manufacturerid
     *
     * @param integer $manufacturerid
     * @return Products
     */
    public function setManufacturerid($manufacturerid)
    {
        $this->manufacturerid = $manufacturerid;

        return $this;
    }

    /**
     * Get manufacturerid
     *
     * @return integer 
     */
    public function getManufacturerid()
    {
        return $this->manufacturerid;
    }

    /**
     * Set taxid
     *
     * @param integer $taxid
     * @return Products
     */
    public function setTaxid($taxid)
    {
        $this->taxid = $taxid;

        return $this;
    }

    /**
     * Get taxid
     *
     * @return integer 
     */
    public function getTaxid()
    {
        return $this->taxid;
    }

    /**
     * Set ean13
     *
     * @param string $ean13
     * @return Products
     */
    public function setEan13($ean13)
    {
        $this->ean13 = $ean13;

        return $this;
    }

    /**
     * Get ean13
     *
     * @return string 
     */
    public function getEan13()
    {
        return $this->ean13;
    }

    /**
     * Set ecotax
     *
     * @param string $ecotax
     * @return Products
     */
    public function setEcotax($ecotax)
    {
        $this->ecotax = $ecotax;

        return $this;
    }

    /**
     * Get ecotax
     *
     * @return string 
     */
    public function getEcotax()
    {
        return $this->ecotax;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Products
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set style
     *
     * @param string $style
     * @return Products
     */
    public function setStyle($style)
    {
        $this->style = $style;

        return $this;
    }

    /**
     * Get style
     *
     * @return string 
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * Set sku
     *
     * @param string $sku
     * @return Products
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string 
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set supplierreference
     *
     * @param string $supplierreference
     * @return Products
     */
    public function setSupplierreference($supplierreference)
    {
        $this->supplierreference = $supplierreference;

        return $this;
    }

    /**
     * Get supplierreference
     *
     * @return string 
     */
    public function getSupplierreference()
    {
        return $this->supplierreference;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     * @return Products
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set onsale
     *
     * @param integer $onsale
     * @return Products
     */
    public function setOnsale($onsale)
    {
        $this->onsale = $onsale;

        return $this;
    }

    /**
     * Get onsale
     *
     * @return integer 
     */
    public function getOnsale()
    {
        return $this->onsale;
    }

    /**
     * Set outofstock
     *
     * @param integer $outofstock
     * @return Products
     */
    public function setOutofstock($outofstock)
    {
        $this->outofstock = $outofstock;

        return $this;
    }

    /**
     * Get outofstock
     *
     * @return integer 
     */
    public function getOutofstock()
    {
        return $this->outofstock;
    }

    /**
     * Set allowbackorder
     *
     * @param integer $allowbackorder
     * @return Products
     */
    public function setAllowbackorder($allowbackorder)
    {
        $this->allowbackorder = $allowbackorder;

        return $this;
    }

    /**
     * Get allowbackorder
     *
     * @return integer 
     */
    public function getAllowbackorder()
    {
        return $this->allowbackorder;
    }

    /**
     * Set customizable
     *
     * @param integer $customizable
     * @return Products
     */
    public function setCustomizable($customizable)
    {
        $this->customizable = $customizable;

        return $this;
    }

    /**
     * Get customizable
     *
     * @return integer 
     */
    public function getCustomizable()
    {
        return $this->customizable;
    }

    /**
     * Set uploadablefiles
     *
     * @param integer $uploadablefiles
     * @return Products
     */
    public function setUploadablefiles($uploadablefiles)
    {
        $this->uploadablefiles = $uploadablefiles;

        return $this;
    }

    /**
     * Get uploadablefiles
     *
     * @return integer 
     */
    public function getUploadablefiles()
    {
        return $this->uploadablefiles;
    }

    /**
     * Set applypostage
     *
     * @param integer $applypostage
     * @return Products
     */
    public function setApplypostage($applypostage)
    {
        $this->applypostage = $applypostage;

        return $this;
    }

    /**
     * Get applypostage
     *
     * @return integer 
     */
    public function getApplypostage()
    {
        return $this->applypostage;
    }

    /**
     * Set quantitydiscount
     *
     * @param integer $quantitydiscount
     * @return Products
     */
    public function setQuantitydiscount($quantitydiscount)
    {
        $this->quantitydiscount = $quantitydiscount;

        return $this;
    }

    /**
     * Get quantitydiscount
     *
     * @return integer 
     */
    public function getQuantitydiscount()
    {
        return $this->quantitydiscount;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Products
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Products
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Products
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set productmerchantcategoryid
     *
     * @param \PrismProductsManager\Entity\ProductsMerchantCategories $productmerchantcategoryid
     * @return Products
     */
    public function setProductmerchantcategoryid(\PrismProductsManager\Entity\ProductsMerchantCategories $productmerchantcategoryid = null)
    {
        $this->productmerchantcategoryid = $productmerchantcategoryid;

        return $this;
    }

    /**
     * Get productmerchantcategoryid
     *
     * @return \PrismProductsManager\Entity\ProductsMerchantCategories 
     */
    public function getProductmerchantcategoryid()
    {
        return $this->productmerchantcategoryid;
    }

    /**
     * Set producttypeid
     *
     * @param \PrismProductsManager\Entity\ProductsTypes $producttypeid
     * @return Products
     */
    public function setProducttypeid(\PrismProductsManager\Entity\ProductsTypes $producttypeid = null)
    {
        $this->producttypeid = $producttypeid;

        return $this;
    }

    /**
     * Get producttypeid
     *
     * @return \PrismProductsManager\Entity\ProductsTypes 
     */
    public function getProducttypeid()
    {
        return $this->producttypeid;
    }
}
