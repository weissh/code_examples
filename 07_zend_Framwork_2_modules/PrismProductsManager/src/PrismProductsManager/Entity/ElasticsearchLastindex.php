<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ElasticsearchLastindex
 *
 * @ORM\Table(name="ELASTICSEARCH_LastIndex")
 * @ORM\Entity
 */
class ElasticsearchLastindex
{
    /**
     * @var integer
     *
     * @ORM\Column(name="indexType", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $indextype;

    /**
     * @var integer
     *
     * @ORM\Column(name="lastIndexTimestamp", type="integer", nullable=false)
     */
    private $lastindextimestamp;



    /**
     * Get indextype
     *
     * @return integer 
     */
    public function getIndextype()
    {
        return $this->indextype;
    }

    /**
     * Set lastindextimestamp
     *
     * @param integer $lastindextimestamp
     * @return ElasticsearchLastindex
     */
    public function setLastindextimestamp($lastindextimestamp)
    {
        $this->lastindextimestamp = $lastindextimestamp;

        return $this;
    }

    /**
     * Get lastindextimestamp
     *
     * @return integer 
     */
    public function getLastindextimestamp()
    {
        return $this->lastindextimestamp;
    }
}
