<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsTabs
 *
 * @ORM\Table(name="PRODUCTS_Tabs", uniqueConstraints={@ORM\UniqueConstraint(name="productId_tabId_tabGroupId", columns={"productId", "tabId", "tabGroupId"})}, indexes={@ORM\Index(name="priority", columns={"priority"}), @ORM\Index(name="IDX_EFF6C10236799605", columns={"productId"})})
 * @ORM\Entity
 */
class ProductsTabs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productTabId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $producttabid;

    /**
     * @var integer
     *
     * @ORM\Column(name="tabId", type="integer", nullable=false)
     */
    private $tabid;

    /**
     * @var integer
     *
     * @ORM\Column(name="tabGroupId", type="integer", nullable=false)
     */
    private $tabgroupid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="priority", type="boolean", nullable=false)
     */
    private $priority;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\Products
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Products")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="productId", referencedColumnName="productId")
     * })
     */
    private $productid;



    /**
     * Get producttabid
     *
     * @return integer 
     */
    public function getProducttabid()
    {
        return $this->producttabid;
    }

    /**
     * Set tabid
     *
     * @param integer $tabid
     * @return ProductsTabs
     */
    public function setTabid($tabid)
    {
        $this->tabid = $tabid;

        return $this;
    }

    /**
     * Get tabid
     *
     * @return integer 
     */
    public function getTabid()
    {
        return $this->tabid;
    }

    /**
     * Set tabgroupid
     *
     * @param integer $tabgroupid
     * @return ProductsTabs
     */
    public function setTabgroupid($tabgroupid)
    {
        $this->tabgroupid = $tabgroupid;

        return $this;
    }

    /**
     * Get tabgroupid
     *
     * @return integer 
     */
    public function getTabgroupid()
    {
        return $this->tabgroupid;
    }

    /**
     * Set priority
     *
     * @param boolean $priority
     * @return ProductsTabs
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return boolean 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProductsTabs
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsTabs
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsTabs
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set productid
     *
     * @param \PrismProductsManager\Entity\Products $productid
     * @return ProductsTabs
     */
    public function setProductid(\PrismProductsManager\Entity\Products $productid = null)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return \PrismProductsManager\Entity\Products 
     */
    public function getProductid()
    {
        return $this->productid;
    }
}
