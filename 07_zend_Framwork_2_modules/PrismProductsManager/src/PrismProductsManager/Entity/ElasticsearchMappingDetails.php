<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ElasticsearchMappingDetails
 *
 * @ORM\Table(name="ELASTICSEARCH_Mapping_Details")
 * @ORM\Entity
 */
class ElasticsearchMappingDetails
{
    /**
     * @var string
     *
     * @ORM\Column(name="elasticSearchType", type="string", nullable=false)
     */
    private $elasticsearchtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="fieldId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $fieldid;

    /**
     * @var integer
     *
     * @ORM\Column(name="fieldParent", type="integer", nullable=false)
     */
    private $fieldparent;

    /**
     * @var string
     *
     * @ORM\Column(name="fieldName", type="string", length=60, nullable=false)
     */
    private $fieldname;

    /**
     * @var string
     *
     * @ORM\Column(name="fieldType", type="string", nullable=false)
     */
    private $fieldtype;

    /**
     * @var string
     *
     * @ORM\Column(name="indexable", type="string", nullable=true)
     */
    private $indexable;

    /**
     * @var boolean
     *
     * @ORM\Column(name="includeInParent", type="boolean", nullable=false)
     */
    private $includeinparent;

    /**
     * @var integer
     *
     * @ORM\Column(name="searchPriority", type="integer", nullable=false)
     */
    private $searchpriority;

    /**
     * @var boolean
     *
     * @ORM\Column(name="autoCompleteEnabled", type="boolean", nullable=false)
     */
    private $autocompleteenabled;



    /**
     * Set elasticsearchtype
     *
     * @param string $elasticsearchtype
     * @return ElasticsearchMappingDetails
     */
    public function setElasticsearchtype($elasticsearchtype)
    {
        $this->elasticsearchtype = $elasticsearchtype;

        return $this;
    }

    /**
     * Get elasticsearchtype
     *
     * @return string 
     */
    public function getElasticsearchtype()
    {
        return $this->elasticsearchtype;
    }

    /**
     * Get fieldid
     *
     * @return integer 
     */
    public function getFieldid()
    {
        return $this->fieldid;
    }

    /**
     * Set fieldparent
     *
     * @param integer $fieldparent
     * @return ElasticsearchMappingDetails
     */
    public function setFieldparent($fieldparent)
    {
        $this->fieldparent = $fieldparent;

        return $this;
    }

    /**
     * Get fieldparent
     *
     * @return integer 
     */
    public function getFieldparent()
    {
        return $this->fieldparent;
    }

    /**
     * Set fieldname
     *
     * @param string $fieldname
     * @return ElasticsearchMappingDetails
     */
    public function setFieldname($fieldname)
    {
        $this->fieldname = $fieldname;

        return $this;
    }

    /**
     * Get fieldname
     *
     * @return string 
     */
    public function getFieldname()
    {
        return $this->fieldname;
    }

    /**
     * Set fieldtype
     *
     * @param string $fieldtype
     * @return ElasticsearchMappingDetails
     */
    public function setFieldtype($fieldtype)
    {
        $this->fieldtype = $fieldtype;

        return $this;
    }

    /**
     * Get fieldtype
     *
     * @return string 
     */
    public function getFieldtype()
    {
        return $this->fieldtype;
    }

    /**
     * Set indexable
     *
     * @param string $indexable
     * @return ElasticsearchMappingDetails
     */
    public function setIndexable($indexable)
    {
        $this->indexable = $indexable;

        return $this;
    }

    /**
     * Get indexable
     *
     * @return string 
     */
    public function getIndexable()
    {
        return $this->indexable;
    }

    /**
     * Set includeinparent
     *
     * @param boolean $includeinparent
     * @return ElasticsearchMappingDetails
     */
    public function setIncludeinparent($includeinparent)
    {
        $this->includeinparent = $includeinparent;

        return $this;
    }

    /**
     * Get includeinparent
     *
     * @return boolean 
     */
    public function getIncludeinparent()
    {
        return $this->includeinparent;
    }

    /**
     * Set searchpriority
     *
     * @param integer $searchpriority
     * @return ElasticsearchMappingDetails
     */
    public function setSearchpriority($searchpriority)
    {
        $this->searchpriority = $searchpriority;

        return $this;
    }

    /**
     * Get searchpriority
     *
     * @return integer 
     */
    public function getSearchpriority()
    {
        return $this->searchpriority;
    }

    /**
     * Set autocompleteenabled
     *
     * @param boolean $autocompleteenabled
     * @return ElasticsearchMappingDetails
     */
    public function setAutocompleteenabled($autocompleteenabled)
    {
        $this->autocompleteenabled = $autocompleteenabled;

        return $this;
    }

    /**
     * Get autocompleteenabled
     *
     * @return boolean 
     */
    public function getAutocompleteenabled()
    {
        return $this->autocompleteenabled;
    }
}
