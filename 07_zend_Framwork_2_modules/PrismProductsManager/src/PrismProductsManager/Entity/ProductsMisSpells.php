<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsMisSpells
 *
 * @ORM\Table(name="PRODUCTS_Mis_Spells", indexes={@ORM\Index(name="productId", columns={"productId"}), @ORM\Index(name="tagName", columns={"misSpell"})})
 * @ORM\Entity
 */
class ProductsMisSpells
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productMisSpellId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productmisspellid;

    /**
     * @var integer
     *
     * @ORM\Column(name="productId", type="integer", nullable=false)
     */
    private $productid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="misSpell", type="string", length=50, nullable=false)
     */
    private $misspell;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get productmisspellid
     *
     * @return integer 
     */
    public function getProductmisspellid()
    {
        return $this->productmisspellid;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return ProductsMisSpells
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return ProductsMisSpells
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set misspell
     *
     * @param string $misspell
     * @return ProductsMisSpells
     */
    public function setMisspell($misspell)
    {
        $this->misspell = $misspell;

        return $this;
    }

    /**
     * Get misspell
     *
     * @return string 
     */
    public function getMisspell()
    {
        return $this->misspell;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsMisSpells
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsMisSpells
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
