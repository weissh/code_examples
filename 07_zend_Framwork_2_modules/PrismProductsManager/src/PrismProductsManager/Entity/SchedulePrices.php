<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SchedulePrices
 *
 * @ORM\Table(name="SCHEDULE_PRICES", indexes={@ORM\Index(name="FK_SCHEDULE_PRICES_SCHEDULES", columns={"scheduleId"}), @ORM\Index(name="FK_SCHEDULE_PRICES_CURRENCY_2", columns={"currencyId"}), @ORM\Index(name="FK_SCHEDULE_PRICES_TERRITORIES", columns={"territoryId"})})
 * @ORM\Entity
 */
class SchedulePrices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="schedulePriceId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $schedulePriceId;

    /**
     * @var string
     *
     * @ORM\Column(name="priceTable", type="string", length=75, nullable=true)
     */
    private $priceTable;

    /**
     * @var string
     *
     * @ORM\Column(name="priceColumn", type="string", length=75, nullable=true)
     */
    private $priceColumn;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \PrismProductsManager\Entity\Territories
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Territories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="territoryId", referencedColumnName="territoryId")
     * })
     */
    private $territoryId;

    /**
     * @var \PrismProductsManager\Entity\Currency
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Currency")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="currencyId", referencedColumnName="currencyId")
     * })
     */
    private $currencyId;

    /**
     * @var \PrismProductsManager\Entity\Schedules
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Schedules")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="scheduleId", referencedColumnName="scheduleId")
     * })
     */
    private $scheduleId;

    /**
     * @return string
     */
    public function getPriceColumn()
    {
        return $this->priceColumn;
    }

    /**
     * @param string $priceColumn
     */
    public function setPriceColumn($priceColumn)
    {
        $this->priceColumn = $priceColumn;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return Territories
     */
    public function getTerritoryId()
    {
        return $this->territoryId;
    }

    /**
     * @param Territories $territoryId
     */
    public function setTerritoryId($territoryId)
    {
        $this->territoryId = $territoryId;
    }

    /**
     * @return Currency
     */
    public function getCurrencyId()
    {
        return $this->currencyId;
    }

    /**
     * @param Currency $currencyId
     */
    public function setCurrencyId($currencyId)
    {
        $this->currencyId = $currencyId;
    }

    /**
     * @return Schedules
     */
    public function getScheduleId()
    {
        return $this->scheduleId;
    }

    /**
     * @param Schedules $scheduleId
     */
    public function setScheduleId($scheduleId)
    {
        $this->scheduleId = $scheduleId;
    }

    /**
     * @return int
     */
    public function getSchedulePriceId()
    {
        return $this->schedulePriceId;
    }

    /**
     * @return string
     */
    public function getPriceTable()
    {
        return $this->priceTable;
    }

    /**
     * @param string $priceTable
     */
    public function setPriceTable($priceTable)
    {
        $this->priceTable = $priceTable;
    }
}