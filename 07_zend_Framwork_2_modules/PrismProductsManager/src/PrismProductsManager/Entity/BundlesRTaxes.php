<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BundlesRTaxes
 *
 * @ORM\Table(name="BUNDLES_R_TAXES", indexes={@ORM\Index(name="productId", columns={"bundleId"}), @ORM\Index(name="taxId", columns={"taxId"})})
 * @ORM\Entity
 */
class BundlesRTaxes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bundleTaxId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bundletaxid;

    /**
     * @var \PrismProductsManager\Entity\Bundles
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Bundles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bundleId", referencedColumnName="bundleId")
     * })
     */
    private $bundleid;

    /**
     * @var \PrismProductsManager\Entity\Taxes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Taxes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="taxId", referencedColumnName="taxId")
     * })
     */
    private $taxid;



    /**
     * Get bundletaxid
     *
     * @return integer 
     */
    public function getBundletaxid()
    {
        return $this->bundletaxid;
    }

    /**
     * Set bundleid
     *
     * @param \PrismProductsManager\Entity\Bundles $bundleid
     * @return BundlesRTaxes
     */
    public function setBundleid(\PrismProductsManager\Entity\Bundles $bundleid = null)
    {
        $this->bundleid = $bundleid;

        return $this;
    }

    /**
     * Get bundleid
     *
     * @return \PrismProductsManager\Entity\Bundles 
     */
    public function getBundleid()
    {
        return $this->bundleid;
    }

    /**
     * Set taxid
     *
     * @param \PrismProductsManager\Entity\Taxes $taxid
     * @return BundlesRTaxes
     */
    public function setTaxid(\PrismProductsManager\Entity\Taxes $taxid = null)
    {
        $this->taxid = $taxid;

        return $this;
    }

    /**
     * Get taxid
     *
     * @return \PrismProductsManager\Entity\Taxes 
     */
    public function getTaxid()
    {
        return $this->taxid;
    }
}
