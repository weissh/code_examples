<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributes
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES", indexes={@ORM\Index(name="Index 2", columns={"isColour", "isSize"}), @ORM\Index(name="Index 3", columns={"enabled"}), @ORM\Index(name="Index 4", columns={"usedForMerchantCategories", "usedForFiltering"})})
 * @ORM\Entity(repositoryClass="PrismProductsManager\Entity\Repository\CommonAttributesRepository")
 */
class CommonAttributes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commonattributeid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usedForFiltering", type="boolean", nullable=false)
     */
    private $usedforfiltering;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usedForMerchantCategories", type="boolean", nullable=false)
     */
    private $usedformerchantcategories;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isColour", type="boolean", nullable=false)
     */
    private $iscolour;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isSize", type="boolean", nullable=false)
     */
    private $issize;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isSizeProperty", type="boolean", nullable=false)
     */
    private $isSizeProperty;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isEditable", type="boolean", nullable=false)
     */
    private $isEditable;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tickedByDefault", type="boolean", nullable=false)
     */
    private $ticketByDefault;

    /**
     * @var boolean
     *
     * @ORM\Column(name="chained", type="boolean", nullable=false)
     */
    private $chained;
    /**
     * @var boolean
     *
     * @ORM\Column(name="period", type="boolean", nullable=false)
     */
    private $period;

    /**
     * @var boolean
     *
     * @ORM\Column(name="automaticFromDate", type="boolean", nullable=false)
     */
    private $automaticFromDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    private $enabled;


    /**
     * @var boolean
     *
     * @ORM\Column(name="keepHistory", type="boolean", nullable=false)
     */
    private $keepHistory;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var integer
     *
     * @ORM\Column(name="modifiedBy", type="integer", nullable=false)
     */
    private $modifiedby;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     *
     *  @var \PrismProductsManager\Entity\CommonAttributesDefinitions
     *
     *  @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesDefinitions", mappedBy="commonattributeid", cascade={"persist"})
     */
    private $commonAttributeDefinitions;

    /**
     *
     *  @var \PrismProductsManager\Entity\CommonAttributesGuiRepresentation
     *
     *  @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesGuiRepresentation", mappedBy="commonattributeid", cascade={"persist"})
     */
    private $commonAttributeGuiRepresentation;

    /**
     *
     *  @var \PrismProductsManager\Entity\CommonAttributesValues
     *
     *  @ORM\OneToMany(targetEntity="PrismProductsManager\Entity\CommonAttributesValues", mappedBy="commonattributeid", cascade={"persist"})
     */
    private $commonAttributesValues;

    /**
     *
     *  @var \PrismProductsManager\Entity\CommonAttributesMapping
     *
     *  @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesMapping", mappedBy="commonAttributeId", cascade={"persist"})
     */
    private $commonAttributeMapping;

    /**
     *
     *  @var \PrismProductsManager\Entity\CommonAttributesMapping
     *
     *  @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesSpectrumMapping", mappedBy="commonAttributeId", cascade={"persist"})
     */
    private $commonAttributeSpectrumMapping;

    /**
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('ALL', 'STYLE', 'OPTION', 'VARIANT')")
     */
    private $EditableOnLevel;


    /**
     * @return boolean
     */
    public function isTicketByDefault()
    {
        return $this->ticketByDefault;
    }

    /**
     * @param boolean $ticketByDefault
     */
    public function setTicketByDefault($ticketByDefault)
    {
        $this->ticketByDefault = $ticketByDefault;
    }



    /**
     * @return CommonAttributesMapping
     */
    public function getCommonAttributeSpectrumMapping()
    {
        return $this->commonAttributeSpectrumMapping;
    }

    /**
     * @param CommonAttributesMapping $commonAttributeSpectrumMapping
     */
    public function setCommonAttributeSpectrumMapping($commonAttributeSpectrumMapping)
    {
        $this->commonAttributeSpectrumMapping = $commonAttributeSpectrumMapping;
    }

    /**
     * @return boolean
     */
    public function isEditable()
    {
        return $this->isEditable;
    }

    /**
     * @param boolean $isEditable
     */
    public function setIsEditable($isEditable)
    {
        $this->isEditable = $isEditable;
    }


    /**
     * @return CommonAttributesMapping
     */
    public function getCommonAttributeMapping()
    {
        return $this->commonAttributeMapping;
    }

    /**
     * @param CommonAttributesMapping $commonAttributeMapping
     */
    public function setCommonAttributeMapping($commonAttributeMapping)
    {
        $this->commonAttributeMapping = $commonAttributeMapping;
    }


    /**
     * @return boolean
     */
    public function isPeriod()
    {
        return $this->period;
    }

    /**
     * @param boolean $period
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    }


    /**
     * @return boolean
     */
    public function isChained()
    {
        return $this->chained;
    }

    /**
     * @param boolean $chained
     */
    public function setChained($chained)
    {
        $this->chained = $chained;
    }


    /**
     * Get commonattributeid
     *
     * @return integer
     */
    public function getCommonattributeid()
    {
        return $this->commonattributeid;
    }

    /**
     * Get isEditable
     *
     * @return integer
     */
    public function getIsEditable()
    {
        return $this->isEditable;
    }

    /**
     * Set usedforfiltering
     *
     * @param boolean $usedforfiltering
     * @return CommonAttributes
     */
    public function setUsedforfiltering($usedforfiltering)
    {
        $this->usedforfiltering = $usedforfiltering;

        return $this;
    }

    /**
     * Get usedforfiltering
     *
     * @return boolean
     */
    public function getUsedforfiltering()
    {
        return $this->usedforfiltering;
    }

    /**
     * Set usedformerchantcategories
     *
     * @param boolean $usedformerchantcategories
     * @return CommonAttributes
     */
    public function setUsedformerchantcategories($usedformerchantcategories)
    {
        $this->usedformerchantcategories = $usedformerchantcategories;

        return $this;
    }

    /**
     * Get usedformerchantcategories
     *
     * @return boolean
     */
    public function getUsedformerchantcategories()
    {
        return $this->usedformerchantcategories;
    }

    /**
     * Set iscolour
     *
     * @param boolean $iscolour
     * @return CommonAttributes
     */
    public function setIscolour($iscolour)
    {
        $this->iscolour = $iscolour;

        return $this;
    }

    /**
     * Get iscolour
     *
     * @return boolean
     */
    public function getIscolour()
    {
        return $this->iscolour;
    }

    /**
     * Set issize
     *
     * @param boolean $issize
     * @return CommonAttributes
     */
    public function setIssize($issize)
    {
        $this->issize = $issize;

        return $this;
    }

    /**
     * Get issize
     *
     * @return boolean
     */
    public function getIssize()
    {
        return $this->issize;
    }

    /**
     * Set isSizeProperty
     *
     * @param boolean $isSizeProperty
     * @return CommonAttributes
     */
    public function setIsSizeProperty($isSizeProperty)
    {
        $this->isSizeProperty = $isSizeProperty;

        return $this;
    }

    /**
     * Get isSizeProperty
     *
     * @return boolean
     */
    public function getIsSizeProperty()
    {
        return $this->isSizeProperty;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return CommonAttributes
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return CommonAttributes
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set modifiedby
     *
     * @param integer $modifiedby
     * @return CommonAttributes
     */
    public function setModifiedby($modifiedby)
    {
        $this->modifiedby = $modifiedby;

        return $this;
    }

    /**
     * Get modifiedby
     *
     * @return integer
     */
    public function getModifiedby()
    {
        return $this->modifiedby;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return CommonAttributes
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Get commonAttributesDefinitions
     *
     * @return \PrismProductsManager\Entity\CommonAttributesDefinitions
     */
    public function getCommonAttributeDefinitions()
    {
        return $this->commonAttributeDefinitions;
    }

    /**
     * Get commonAttributesGuiRepresentation
     *
     * @return \PrismProductsManager\Entity\CommonAttributesGuiRepresentation
     */
    public function getCommonAttributeGuiRepresentation()
    {
        return $this->commonAttributeGuiRepresentation;
    }

    /**
     * Get commonAttributesValues
     *
     * @return \PrismProductsManager\Entity\CommonAttributesValues
     */
    public function getCommonAttributeValues()
    {
        return $this->commonAttributesValues;
    }

    /**
     * @return CommonAttributesValues
     */
    public function getCommonAttributesValues()
    {
        return $this->commonAttributesValues;
    }

    /**
     * @return string
     */
    public function getEditableOnLevel()
    {
        return $this->EditableOnLevel;
    }

    /**
     * @param CommonAttributesValues $commonAttributesValues
     */
    public function setCommonAttributesValues($commonAttributesValues)
    {
        $this->commonAttributesValues = $commonAttributesValues;
    }

    /**
     * @param int $commonattributeid
     */
    public function setCommonattributeid($commonattributeid)
    {
        $this->commonattributeid = $commonattributeid;
    }

    /**
     * @param CommonAttributesDefinitions $commonAttributeDefinitions
     */
    public function setCommonAttributeDefinitions($commonAttributeDefinitions)
    {
        $this->commonAttributeDefinitions = $commonAttributeDefinitions;
    }

    /**
     * @param CommonAttributesGuiRepresentation $commonAttributeGuiRepresentation
     */
    public function setCommonAttributeGuiRepresentation($commonAttributeGuiRepresentation)
    {
        $this->commonAttributeGuiRepresentation = $commonAttributeGuiRepresentation;
    }

    /**
     * @param string $editableOnLevel
     */
    public function setEditableOnLevel($editableOnLevel)
    {
        if (!in_array($editableOnLevel, ['ALL', 'STYLE', 'OPTION', 'VARIANT'])) {
            $this->EditableOnLevel =  'ALL';
        }

        $this->EditableOnLevel = $editableOnLevel;
    }

    /**
     * @return boolean
     */
    public function isAutomaticFromDate()
    {
        return $this->automaticFromDate;
    }

    /**
     * @param boolean $automaticFromDate
     */
    public function setAutomaticFromDate($automaticFromDate)
    {
        $this->automaticFromDate = $automaticFromDate;
    }

    /**
     * @return boolean
     */
    public function isKeepHistory()
    {
        return $this->keepHistory;
    }

    /**
     * @param boolean $keepHistory
     */
    public function setKeepHistory($keepHistory)
    {
        $this->keepHistory = $keepHistory;
    }

}
