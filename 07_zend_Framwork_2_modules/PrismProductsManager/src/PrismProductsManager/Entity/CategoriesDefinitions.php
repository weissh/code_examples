<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriesDefinitions
 *
 * @ORM\Table(name="CATEGORIES_Definitions")
 * @ORM\Entity
 */
class CategoriesDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="categoryDefinitionsId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $categorydefinitionsid;

    /**
     * @var integer
     *
     * @ORM\Column(name="categoryId", type="integer", nullable=false)
     */
    private $categoryid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var integer
     *
     * @ORM\Column(name="versionId", type="integer", nullable=false)
     */
    private $versionid;

    /**
     * @var string
     *
     * @ORM\Column(name="versionDescription", type="string", length=255, nullable=false)
     */
    private $versiondescription;

    /**
     * @var string
     *
     * @ORM\Column(name="categoryName", type="string", length=75, nullable=false)
     */
    private $categoryname;

    /**
     * @var string
     *
     * @ORM\Column(name="urlName", type="string", length=75, nullable=false)
     */
    private $urlname;

    /**
     * @var string
     *
     * @ORM\Column(name="metaTitle", type="string", length=75, nullable=false)
     */
    private $metatitle;

    /**
     * @var string
     *
     * @ORM\Column(name="metaDescription", type="string", length=160, nullable=false)
     */
    private $metadescription;

    /**
     * @var string
     *
     * @ORM\Column(name="metaKeyword", type="string", length=160, nullable=false)
     */
    private $metakeyword;

    /**
     * @var string
     *
     * @ORM\Column(name="shortDescription", type="string", length=30, nullable=false)
     */
    private $shortdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="longDescription", type="text", nullable=false)
     */
    private $longdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="makeLive", type="datetime", nullable=true)
     */
    private $makelive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean", nullable=false)
     */
    private $locked;



    /**
     * Get categorydefinitionsid
     *
     * @return integer 
     */
    public function getCategorydefinitionsid()
    {
        return $this->categorydefinitionsid;
    }

    /**
     * Set categoryid
     *
     * @param integer $categoryid
     * @return CategoriesDefinitions
     */
    public function setCategoryid($categoryid)
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    /**
     * Get categoryid
     *
     * @return integer 
     */
    public function getCategoryid()
    {
        return $this->categoryid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return CategoriesDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set versionid
     *
     * @param integer $versionid
     * @return CategoriesDefinitions
     */
    public function setVersionid($versionid)
    {
        $this->versionid = $versionid;

        return $this;
    }

    /**
     * Get versionid
     *
     * @return integer 
     */
    public function getVersionid()
    {
        return $this->versionid;
    }

    /**
     * Set versiondescription
     *
     * @param string $versiondescription
     * @return CategoriesDefinitions
     */
    public function setVersiondescription($versiondescription)
    {
        $this->versiondescription = $versiondescription;

        return $this;
    }

    /**
     * Get versiondescription
     *
     * @return string 
     */
    public function getVersiondescription()
    {
        return $this->versiondescription;
    }

    /**
     * Set categoryname
     *
     * @param string $categoryname
     * @return CategoriesDefinitions
     */
    public function setCategoryname($categoryname)
    {
        $this->categoryname = $categoryname;

        return $this;
    }

    /**
     * Get categoryname
     *
     * @return string 
     */
    public function getCategoryname()
    {
        return $this->categoryname;
    }

    /**
     * Set urlname
     *
     * @param string $urlname
     * @return CategoriesDefinitions
     */
    public function setUrlname($urlname)
    {
        $this->urlname = $urlname;

        return $this;
    }

    /**
     * Get urlname
     *
     * @return string 
     */
    public function getUrlname()
    {
        return $this->urlname;
    }

    /**
     * Set metatitle
     *
     * @param string $metatitle
     * @return CategoriesDefinitions
     */
    public function setMetatitle($metatitle)
    {
        $this->metatitle = $metatitle;

        return $this;
    }

    /**
     * Get metatitle
     *
     * @return string 
     */
    public function getMetatitle()
    {
        return $this->metatitle;
    }

    /**
     * Set metadescription
     *
     * @param string $metadescription
     * @return CategoriesDefinitions
     */
    public function setMetadescription($metadescription)
    {
        $this->metadescription = $metadescription;

        return $this;
    }

    /**
     * Get metadescription
     *
     * @return string 
     */
    public function getMetadescription()
    {
        return $this->metadescription;
    }

    /**
     * Set metakeyword
     *
     * @param string $metakeyword
     * @return CategoriesDefinitions
     */
    public function setMetakeyword($metakeyword)
    {
        $this->metakeyword = $metakeyword;

        return $this;
    }

    /**
     * Get metakeyword
     *
     * @return string 
     */
    public function getMetakeyword()
    {
        return $this->metakeyword;
    }

    /**
     * Set shortdescription
     *
     * @param string $shortdescription
     * @return CategoriesDefinitions
     */
    public function setShortdescription($shortdescription)
    {
        $this->shortdescription = $shortdescription;

        return $this;
    }

    /**
     * Get shortdescription
     *
     * @return string 
     */
    public function getShortdescription()
    {
        return $this->shortdescription;
    }

    /**
     * Set longdescription
     *
     * @param string $longdescription
     * @return CategoriesDefinitions
     */
    public function setLongdescription($longdescription)
    {
        $this->longdescription = $longdescription;

        return $this;
    }

    /**
     * Get longdescription
     *
     * @return string 
     */
    public function getLongdescription()
    {
        return $this->longdescription;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return CategoriesDefinitions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set makelive
     *
     * @param \DateTime $makelive
     * @return CategoriesDefinitions
     */
    public function setMakelive($makelive)
    {
        $this->makelive = $makelive;

        return $this;
    }

    /**
     * Get makelive
     *
     * @return \DateTime 
     */
    public function getMakelive()
    {
        return $this->makelive;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return CategoriesDefinitions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return CategoriesDefinitions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     * @return CategoriesDefinitions
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean 
     */
    public function getLocked()
    {
        return $this->locked;
    }
}
