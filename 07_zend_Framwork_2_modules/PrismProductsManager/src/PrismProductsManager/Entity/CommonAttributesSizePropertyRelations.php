<?php
/**
 * Created by PhpStorm.
 * User: haniw
 * Date: 08/05/2017
 * Time: 15:13
 */

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributesSizePropertyRelations
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES_Size_Property_Relations", indexes={@ORM\Index(name="commonAttributesSizePropertyRelationsid", columns={"commonAttributeSizePropertyParentId", "commonAttributeSizePropertyChildId"})})
 * @ORM\Entity
 */
class CommonAttributesSizePropertyRelations
{

    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributesSizePropertyRelationsId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commonAttributesSizePropertyRelationsId;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributes" , inversedBy="commonAttributeSizePropertyParentId")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="commonAttributeSizePropertyParentId", referencedColumnName="commonattributeid")
     * })
     */
    private $commonAttributeSizePropertyParentId;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributes" , inversedBy="commonAttributesSizePropertyRelations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="commonAttributeSizePropertyChildId", referencedColumnName="commonattributeid")
     * })
     */
    private $commonAttributeSizePropertyChildId;

    /**
     * @return int
     */
    public function getCommonAttributesSizePropertyRelationsId()
    {
        return $this->commonAttributesSizePropertyRelationsId;
    }

    /**
     * @param int $commonAttributesSizePropertyRelationsId
     */
    public function setCommonAttributesSizePropertyRelationsId($commonAttributesSizePropertyRelationsId)
    {
        $this->commonAttributesSizePropertyRelationsId = $commonAttributesSizePropertyRelationsId;
    }

    /**
     * @return CommonAttributes
     */
    public function getCommonAttributeSizePropertyParentId()
    {
        return $this->commonAttributeSizePropertyParentId;
    }

    /**
     * @param CommonAttributes $commonAttributeSizePropertyParentId
     */
    public function setCommonAttributeSizePropertyParentId($commonAttributeSizePropertyParentId)
    {
        $this->commonAttributeSizePropertyParentId = $commonAttributeSizePropertyParentId;
    }

    /**
     * @return CommonAttributes
     */
    public function getCommonAttributeSizePropertyChildId()
    {
        return $this->commonAttributeSizePropertyChildId;
    }

    /**
     * @param CommonAttributes $commonAttributeSizePropertyChildId
     */
    public function setCommonAttributeSizePropertyChildId($commonAttributeSizePropertyChildId)
    {
        $this->commonAttributeSizePropertyChildId = $commonAttributeSizePropertyChildId;
    }



}