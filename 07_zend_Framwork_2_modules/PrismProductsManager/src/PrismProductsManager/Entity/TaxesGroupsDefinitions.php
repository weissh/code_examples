<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TaxesGroupsDefinitions
 *
 * @ORM\Table(name="TAXES_Groups_Definitions", indexes={@ORM\Index(name="languageId", columns={"languageId"}), @ORM\Index(name="taxGroupId", columns={"taxGroupId"}), @ORM\Index(name="name", columns={"name"}), @ORM\Index(name="status", columns={"status"}), @ORM\Index(name="languageId_taxGroupId_status", columns={"languageId", "taxGroupId", "status"})})
 * @ORM\Entity
 */
class TaxesGroupsDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="taxGroupDefinitionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $taxgroupdefinitionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\TaxesGroups
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\TaxesGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="taxGroupId", referencedColumnName="taxGroupId")
     * })
     */
    private $taxgroupid;



    /**
     * Get taxgroupdefinitionid
     *
     * @return integer 
     */
    public function getTaxgroupdefinitionid()
    {
        return $this->taxgroupdefinitionid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return TaxesGroupsDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TaxesGroupsDefinitions
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return TaxesGroupsDefinitions
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TaxesGroupsDefinitions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return TaxesGroupsDefinitions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return TaxesGroupsDefinitions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set taxgroupid
     *
     * @param \PrismProductsManager\Entity\TaxesGroups $taxgroupid
     * @return TaxesGroupsDefinitions
     */
    public function setTaxgroupid(\PrismProductsManager\Entity\TaxesGroups $taxgroupid = null)
    {
        $this->taxgroupid = $taxgroupid;

        return $this;
    }

    /**
     * Get taxgroupid
     *
     * @return \PrismProductsManager\Entity\TaxesGroups 
     */
    public function getTaxgroupid()
    {
        return $this->taxgroupid;
    }
}
