<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ColoursRGroups
 *
 * @ORM\Table(name="COLOURS_R_GROUPS", indexes={@ORM\Index(name="colourGroupId", columns={"colourGroupId"}), @ORM\Index(name="colourId", columns={"colourId"})})
 * @ORM\Entity
 */
class ColoursRGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="coloursRGroupsId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $coloursrgroupsid;

    /**
     * @var \PrismProductsManager\Entity\ColoursGroups
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\ColoursGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="colourGroupId", referencedColumnName="colourGroupId")
     * })
     */
    private $colourgroupid;

    /**
     * @var \PrismProductsManager\Entity\Colours
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Colours")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="colourId", referencedColumnName="colourId")
     * })
     */
    private $colourid;



    /**
     * Get coloursrgroupsid
     *
     * @return integer 
     */
    public function getColoursrgroupsid()
    {
        return $this->coloursrgroupsid;
    }

    /**
     * Set colourgroupid
     *
     * @param \PrismProductsManager\Entity\ColoursGroups $colourgroupid
     * @return ColoursRGroups
     */
    public function setColourgroupid(\PrismProductsManager\Entity\ColoursGroups $colourgroupid = null)
    {
        $this->colourgroupid = $colourgroupid;

        return $this;
    }

    /**
     * Get colourgroupid
     *
     * @return \PrismProductsManager\Entity\ColoursGroups 
     */
    public function getColourgroupid()
    {
        return $this->colourgroupid;
    }

    /**
     * Set colourid
     *
     * @param \PrismProductsManager\Entity\Colours $colourid
     * @return ColoursRGroups
     */
    public function setColourid(\PrismProductsManager\Entity\Colours $colourid = null)
    {
        $this->colourid = $colourid;

        return $this;
    }

    /**
     * Get colourid
     *
     * @return \PrismProductsManager\Entity\Colours 
     */
    public function getColourid()
    {
        return $this->colourid;
    }
}
