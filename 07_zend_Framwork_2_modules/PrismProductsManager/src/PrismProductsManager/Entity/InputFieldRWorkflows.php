<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputFieldRWorkflows
 *
 * @ORM\Table(name="INPUT_Field_R_Workflows", indexes={@ORM\Index(name="FK__INPUT_Fields", columns={"inputFieldId"}), @ORM\Index(name="FK__WORKFLOWS", columns={"workflowId"})})
 * @ORM\Entity
 */
class InputFieldRWorkflows
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputFieldRWorkflowsId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputfieldrworkflowsid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     * @var \PrismProductsManager\Entity\InputFields
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputFields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputFieldId", referencedColumnName="inputFieldId")
     * })
     */
    private $inputfieldid;

    /**
     * @var \PrismProductsManager\Entity\Workflows
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Workflows")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="workflowId", referencedColumnName="workflowId")
     * })
     */
    private $workflowid;



    /**
     * Get inputfieldrworkflowsid
     *
     * @return integer 
     */
    public function getInputfieldrworkflowsid()
    {
        return $this->inputfieldrworkflowsid;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputFieldRWorkflows
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputFieldRWorkflows
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set inputfieldid
     *
     * @param \PrismProductsManager\Entity\InputFields $inputfieldid
     * @return InputFieldRWorkflows
     */
    public function setInputfieldid(\PrismProductsManager\Entity\InputFields $inputfieldid = null)
    {
        $this->inputfieldid = $inputfieldid;

        return $this;
    }

    /**
     * Get inputfieldid
     *
     * @return \PrismProductsManager\Entity\InputFields 
     */
    public function getInputfieldid()
    {
        return $this->inputfieldid;
    }

    /**
     * Set workflowid
     *
     * @param \PrismProductsManager\Entity\Workflows $workflowid
     * @return InputFieldRWorkflows
     */
    public function setWorkflowid(\PrismProductsManager\Entity\Workflows $workflowid = null)
    {
        $this->workflowid = $workflowid;

        return $this;
    }

    /**
     * Get workflowid
     *
     * @return \PrismProductsManager\Entity\Workflows 
     */
    public function getWorkflowid()
    {
        return $this->workflowid;
    }
}
