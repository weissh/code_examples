<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AttributesMerchants
 *
 * @ORM\Table(name="ATTRIBUTES_Merchants", indexes={@ORM\Index(name="attributeGroupId", columns={"attributeGroupId"})})
 * @ORM\Entity
 */
class AttributesMerchants
{
    /**
     * @var integer
     *
     * @ORM\Column(name="attributeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $attributeid;

    /**
     * @var integer
     *
     * @ORM\Column(name="merchantAttributeId", type="integer", nullable=false)
     */
    private $merchantattributeid;

    /**
     * @var \PrismProductsManager\Entity\AttributesGroups
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\AttributesGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="attributeGroupId", referencedColumnName="attributeGroupId")
     * })
     */
    private $attributegroupid;



    /**
     * Get attributeid
     *
     * @return integer 
     */
    public function getAttributeid()
    {
        return $this->attributeid;
    }

    /**
     * Set merchantattributeid
     *
     * @param integer $merchantattributeid
     * @return AttributesMerchants
     */
    public function setMerchantattributeid($merchantattributeid)
    {
        $this->merchantattributeid = $merchantattributeid;

        return $this;
    }

    /**
     * Get merchantattributeid
     *
     * @return integer 
     */
    public function getMerchantattributeid()
    {
        return $this->merchantattributeid;
    }

    /**
     * Set attributegroupid
     *
     * @param \PrismProductsManager\Entity\AttributesGroups $attributegroupid
     * @return AttributesMerchants
     */
    public function setAttributegroupid(\PrismProductsManager\Entity\AttributesGroups $attributegroupid = null)
    {
        $this->attributegroupid = $attributegroupid;

        return $this;
    }

    /**
     * Get attributegroupid
     *
     * @return \PrismProductsManager\Entity\AttributesGroups 
     */
    public function getAttributegroupid()
    {
        return $this->attributegroupid;
    }
}
