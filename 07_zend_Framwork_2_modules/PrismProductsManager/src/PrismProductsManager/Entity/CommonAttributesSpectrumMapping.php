<?php

namespace PrismProductsManager\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributesSpectrumMapping
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES_Spectrum_Mapping", indexes={@ORM\Index(name="FK_COMMON_ATTRIBUTES_Mapping_COMMON_ATTRIBUTES", columns={"commonAttributeId"})})
 * @ORM\Entity
 */
class CommonAttributesSpectrumMapping
{
    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributeSpectrumMappingId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commonAttributeSpectrumMappingId;

    /**
     * @var string
     *
     * @ORM\Column(name="commonAttributeSpectrumMapping", type="string", length=250, nullable=true)
     */
    private $commonAttributeSpectrumMapping;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="commonAttributeId", referencedColumnName="commonAttributeId")
     * })
     */
    private $commonAttributeId;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PrismProductsManager\Entity\CommonAttributesSpectrumMappingOverride", mappedBy="commonAttributeSpectrumMapping", cascade={"persist", "remove"})
     */
    private $commonAttributesSpectrumMappingOverride;

    /**
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return CommonAttributes
     */
    public function getCommonAttributeId()
    {
        return $this->commonAttributeId;
    }

    /**
     * @param CommonAttributes $commonAttributeId
     */
    public function setCommonAttributeId($commonAttributeId)
    {
        $this->commonAttributeId = $commonAttributeId;
    }

    /**
     * @return string
     */
    public function getCommonAttributeSpectrumMapping()
    {
        return $this->commonAttributeSpectrumMapping;
    }

    /**
     * @param string $commonAttributeSpectrumMapping
     */
    public function setCommonAttributeSpectrumMapping($commonAttributeSpectrumMapping)
    {
        $this->commonAttributeSpectrumMapping = $commonAttributeSpectrumMapping;
    }

    /**
     * @return int
     */
    public function getCommonAttributeSpectrumMappingId()
    {
        return $this->commonAttributeSpectrumMappingId;
    }

    /**
     * @return ArrayCollection
     */
    public function getCommonAttributesSpectrumMappingOverride()
    {
        return $this->commonAttributesSpectrumMappingOverride;
    }

    /**
     * @param ArrayCollection $commonAttributesSpectrumMappingOverride
     */
    public function setCommonAttributesSpectrumMappingOverride($commonAttributesSpectrumMappingOverride)
    {
        $this->commonAttributesSpectrumMappingOverride = $commonAttributesSpectrumMappingOverride;
    }
}
