<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QueueManagerBulkLogs
 *
 * @ORM\Table(name="QUEUE_Manager_Bulk_Logs")
 * @ORM\Entity
 */
class QueueManagerBulkLogs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="queueBulkLogsId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $queuebulklogsid;

    /**
     * @var string
     *
     * @ORM\Column(name="scriptDate", type="string", length=50, nullable=false)
     */
    private $scriptdate;

    /**
     * @var string
     *
     * @ORM\Column(name="log", type="text", nullable=false)
     */
    private $log;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get queuebulklogsid
     *
     * @return integer 
     */
    public function getQueuebulklogsid()
    {
        return $this->queuebulklogsid;
    }

    /**
     * Set scriptdate
     *
     * @param string $scriptdate
     * @return QueueManagerBulkLogs
     */
    public function setScriptdate($scriptdate)
    {
        $this->scriptdate = $scriptdate;

        return $this;
    }

    /**
     * Get scriptdate
     *
     * @return string 
     */
    public function getScriptdate()
    {
        return $this->scriptdate;
    }

    /**
     * Set log
     *
     * @param string $log
     * @return QueueManagerBulkLogs
     */
    public function setLog($log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * Get log
     *
     * @return string 
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return QueueManagerBulkLogs
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
