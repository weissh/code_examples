<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputValidationTypes
 *
 * @ORM\Table(name="INPUT_Validation_Types")
 * @ORM\Entity
 */
class InputValidationTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputValidationTypeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputvalidationtypeid;

    /**
     * @var string
     *
     * @ORM\Column(name="inputValidationType", type="string", length=80, nullable=true)
     */
    private $inputvalidationtype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;



    /**
     * Get inputvalidationtypeid
     *
     * @return integer 
     */
    public function getInputvalidationtypeid()
    {
        return $this->inputvalidationtypeid;
    }

    /**
     * Set inputvalidationtype
     *
     * @param string $inputvalidationtype
     * @return InputValidationTypes
     */
    public function setInputvalidationtype($inputvalidationtype)
    {
        $this->inputvalidationtype = $inputvalidationtype;

        return $this;
    }

    /**
     * Get inputvalidationtype
     *
     * @return string 
     */
    public function getInputvalidationtype()
    {
        return $this->inputvalidationtype;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputValidationTypes
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputValidationTypes
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }
}
