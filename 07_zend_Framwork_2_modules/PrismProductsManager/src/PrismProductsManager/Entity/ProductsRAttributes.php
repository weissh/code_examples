<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsRAttributes
 *
 * @ORM\Table(name="PRODUCTS_R_ATTRIBUTES", indexes={@ORM\Index(name="manufacturerId", columns={"manufacturerId"}), @ORM\Index(name="taxId", columns={"taxId"}), @ORM\Index(name="created", columns={"created"}), @ORM\Index(name="FK_PRODUCTS_R_ATTRIBUTES_PRODUCTS", columns={"productId"}), @ORM\Index(name="sku", columns={"sku"})})
 * @ORM\Entity
 */
class ProductsRAttributes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productAttributeId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productattributeid;

    /**
     * @var integer
     *
     * @ORM\Column(name="manufacturerId", type="integer", nullable=true)
     */
    private $manufacturerid;

    /**
     * @var integer
     *
     * @ORM\Column(name="taxId", type="integer", nullable=false)
     */
    private $taxid;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="ean13", type="string", length=30, nullable=true)
     */
    private $ean13;

    /**
     * @var string
     *
     * @ORM\Column(name="ecoTax", type="decimal", precision=17, scale=2, nullable=false)
     */
    private $ecotax;

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=32, nullable=true)
     */
    private $sku;

    /**
     * @var string
     *
     * @ORM\Column(name="supplierReference", type="string", length=32, nullable=true)
     */
    private $supplierreference;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="indexed", type="boolean", nullable=false)
     */
    private $indexed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDefaultProduct", type="boolean", nullable=false)
     */
    private $isdefaultproduct;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\Products
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Products")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="productId", referencedColumnName="productId")
     * })
     */
    private $productid;



    /**
     * Get productattributeid
     *
     * @return integer 
     */
    public function getProductattributeid()
    {
        return $this->productattributeid;
    }

    /**
     * Set manufacturerid
     *
     * @param integer $manufacturerid
     * @return ProductsRAttributes
     */
    public function setManufacturerid($manufacturerid)
    {
        $this->manufacturerid = $manufacturerid;

        return $this;
    }

    /**
     * Get manufacturerid
     *
     * @return integer 
     */
    public function getManufacturerid()
    {
        return $this->manufacturerid;
    }

    /**
     * Set taxid
     *
     * @param integer $taxid
     * @return ProductsRAttributes
     */
    public function setTaxid($taxid)
    {
        $this->taxid = $taxid;

        return $this;
    }

    /**
     * Get taxid
     *
     * @return integer 
     */
    public function getTaxid()
    {
        return $this->taxid;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return ProductsRAttributes
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set ean13
     *
     * @param string $ean13
     * @return ProductsRAttributes
     */
    public function setEan13($ean13)
    {
        $this->ean13 = $ean13;

        return $this;
    }

    /**
     * Get ean13
     *
     * @return string 
     */
    public function getEan13()
    {
        return $this->ean13;
    }

    /**
     * Set ecotax
     *
     * @param string $ecotax
     * @return ProductsRAttributes
     */
    public function setEcotax($ecotax)
    {
        $this->ecotax = $ecotax;

        return $this;
    }

    /**
     * Get ecotax
     *
     * @return string 
     */
    public function getEcotax()
    {
        return $this->ecotax;
    }

    /**
     * Set sku
     *
     * @param string $sku
     * @return ProductsRAttributes
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string 
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set supplierreference
     *
     * @param string $supplierreference
     * @return ProductsRAttributes
     */
    public function setSupplierreference($supplierreference)
    {
        $this->supplierreference = $supplierreference;

        return $this;
    }

    /**
     * Get supplierreference
     *
     * @return string 
     */
    public function getSupplierreference()
    {
        return $this->supplierreference;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProductsRAttributes
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set indexed
     *
     * @param boolean $indexed
     * @return ProductsRAttributes
     */
    public function setIndexed($indexed)
    {
        $this->indexed = $indexed;

        return $this;
    }

    /**
     * Get indexed
     *
     * @return boolean 
     */
    public function getIndexed()
    {
        return $this->indexed;
    }

    /**
     * Set isdefaultproduct
     *
     * @param boolean $isdefaultproduct
     * @return ProductsRAttributes
     */
    public function setIsdefaultproduct($isdefaultproduct)
    {
        $this->isdefaultproduct = $isdefaultproduct;

        return $this;
    }

    /**
     * Get isdefaultproduct
     *
     * @return boolean 
     */
    public function getIsdefaultproduct()
    {
        return $this->isdefaultproduct;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsRAttributes
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsRAttributes
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set productid
     *
     * @param \PrismProductsManager\Entity\Products $productid
     * @return ProductsRAttributes
     */
    public function setProductid(\PrismProductsManager\Entity\Products $productid = null)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return \PrismProductsManager\Entity\Products 
     */
    public function getProductid()
    {
        return $this->productid;
    }
}
