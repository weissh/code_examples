<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsSupplierPrice
 *
 * @ORM\Table(name="PRODUCTS_Supplier_Price", uniqueConstraints={@ORM\UniqueConstraint(name="Index 2", columns={"productIdFrom", "productOrVariantId"})})
 * @ORM\Entity
 */
class ProductsSupplierPrice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="supplierPriceId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $supplierpriceid;

    /**
     * @var string
     *
     * @ORM\Column(name="productIdFrom", type="string", nullable=false)
     */
    private $productidfrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="productOrVariantId", type="integer", nullable=false)
     */
    private $productorvariantid;

    /**
     * @var string
     *
     * @ORM\Column(name="currencyCode", type="string", length=10, nullable=false)
     */
    private $currencycode;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=6, scale=2, nullable=false)
     */
    private $price;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;



    /**
     * Get supplierpriceid
     *
     * @return integer 
     */
    public function getSupplierpriceid()
    {
        return $this->supplierpriceid;
    }

    /**
     * Set productidfrom
     *
     * @param string $productidfrom
     * @return ProductsSupplierPrice
     */
    public function setProductidfrom($productidfrom)
    {
        $this->productidfrom = $productidfrom;

        return $this;
    }

    /**
     * Get productidfrom
     *
     * @return string 
     */
    public function getProductidfrom()
    {
        return $this->productidfrom;
    }

    /**
     * Set productorvariantid
     *
     * @param integer $productorvariantid
     * @return ProductsSupplierPrice
     */
    public function setProductorvariantid($productorvariantid)
    {
        $this->productorvariantid = $productorvariantid;

        return $this;
    }

    /**
     * Get productorvariantid
     *
     * @return integer 
     */
    public function getProductorvariantid()
    {
        return $this->productorvariantid;
    }

    /**
     * Set currencycode
     *
     * @param string $currencycode
     * @return ProductsSupplierPrice
     */
    public function setCurrencycode($currencycode)
    {
        $this->currencycode = $currencycode;

        return $this;
    }

    /**
     * Get currencycode
     *
     * @return string 
     */
    public function getCurrencycode()
    {
        return $this->currencycode;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return ProductsSupplierPrice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsSupplierPrice
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsSupplierPrice
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
