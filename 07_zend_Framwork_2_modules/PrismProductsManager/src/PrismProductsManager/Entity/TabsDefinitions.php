<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TabsDefinitions
 *
 * @ORM\Table(name="TABS_Definitions", indexes={@ORM\Index(name="FK_TABS_Definitions_TABS", columns={"tabId"}), @ORM\Index(name="title", columns={"title"})})
 * @ORM\Entity
 */
class TabsDefinitions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tabDefinitionsId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $tabdefinitionsid;

    /**
     * @var integer
     *
     * @ORM\Column(name="languageId", type="integer", nullable=false)
     */
    private $languageid;

    /**
     * @var integer
     *
     * @ORM\Column(name="versionId", type="integer", nullable=false)
     */
    private $versionid;

    /**
     * @var string
     *
     * @ORM\Column(name="versionDescription", type="string", length=255, nullable=false)
     */
    private $versiondescription;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=3, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=30, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\Tabs
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Tabs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tabId", referencedColumnName="tabId")
     * })
     */
    private $tabid;



    /**
     * Get tabdefinitionsid
     *
     * @return integer 
     */
    public function getTabdefinitionsid()
    {
        return $this->tabdefinitionsid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return TabsDefinitions
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set versionid
     *
     * @param integer $versionid
     * @return TabsDefinitions
     */
    public function setVersionid($versionid)
    {
        $this->versionid = $versionid;

        return $this;
    }

    /**
     * Get versionid
     *
     * @return integer 
     */
    public function getVersionid()
    {
        return $this->versionid;
    }

    /**
     * Set versiondescription
     *
     * @param string $versiondescription
     * @return TabsDefinitions
     */
    public function setVersiondescription($versiondescription)
    {
        $this->versiondescription = $versiondescription;

        return $this;
    }

    /**
     * Get versiondescription
     *
     * @return string 
     */
    public function getVersiondescription()
    {
        return $this->versiondescription;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return TabsDefinitions
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return TabsDefinitions
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return TabsDefinitions
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TabsDefinitions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return TabsDefinitions
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return TabsDefinitions
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set tabid
     *
     * @param \PrismProductsManager\Entity\Tabs $tabid
     * @return TabsDefinitions
     */
    public function setTabid(\PrismProductsManager\Entity\Tabs $tabid = null)
    {
        $this->tabid = $tabid;

        return $this;
    }

    /**
     * Get tabid
     *
     * @return \PrismProductsManager\Entity\Tabs 
     */
    public function getTabid()
    {
        return $this->tabid;
    }
}
