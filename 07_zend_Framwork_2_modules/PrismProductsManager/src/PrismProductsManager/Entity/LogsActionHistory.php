<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogsActionHistory
 *
 * @ORM\Table(name="LOGS_Action_History")
 * @ORM\Entity
 */
class LogsActionHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="logId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $logid;

    /**
     * @var integer
     *
     * @ORM\Column(name="userId", type="integer", nullable=true)
     */
    private $userid;

    /**
     * @var string
     *
     * @ORM\Column(name="tableName", type="string", length=75, nullable=true)
     */
    private $tablename;

    /**
     * @var string
     *
     * @ORM\Column(name="actionName", type="string", length=75, nullable=true)
     */
    private $actionname;

    /**
     * @var string
     *
     * @ORM\Column(name="actionData", type="text", nullable=true)
     */
    private $actiondata;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;



    /**
     * Get logid
     *
     * @return integer 
     */
    public function getLogid()
    {
        return $this->logid;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     * @return LogsActionHistory
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer 
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set tablename
     *
     * @param string $tablename
     * @return LogsActionHistory
     */
    public function setTablename($tablename)
    {
        $this->tablename = $tablename;

        return $this;
    }

    /**
     * Get tablename
     *
     * @return string 
     */
    public function getTablename()
    {
        return $this->tablename;
    }

    /**
     * Set actionname
     *
     * @param string $actionname
     * @return LogsActionHistory
     */
    public function setActionname($actionname)
    {
        $this->actionname = $actionname;

        return $this;
    }

    /**
     * Get actionname
     *
     * @return string 
     */
    public function getActionname()
    {
        return $this->actionname;
    }

    /**
     * Set actiondata
     *
     * @param string $actiondata
     * @return LogsActionHistory
     */
    public function setActiondata($actiondata)
    {
        $this->actiondata = $actiondata;

        return $this;
    }

    /**
     * Get actiondata
     *
     * @return string 
     */
    public function getActiondata()
    {
        return $this->actiondata;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return LogsActionHistory
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return LogsActionHistory
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return LogsActionHistory
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }
}
