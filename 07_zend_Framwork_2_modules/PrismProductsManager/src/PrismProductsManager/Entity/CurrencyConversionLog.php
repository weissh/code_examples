<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CurrencyConversionLog
 *
 * @ORM\Table(name="CURRENCY_Conversion_Log")
 * @ORM\Entity
 */
class CurrencyConversionLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="logId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $logid;

    /**
     * @var string
     *
     * @ORM\Column(name="conversionRate", type="decimal", precision=13, scale=0, nullable=true)
     */
    private $conversionrate;

    /**
     * @var string
     *
     * @ORM\Column(name="plusFixed", type="decimal", precision=13, scale=0, nullable=true)
     */
    private $plusfixed;

    /**
     * @var string
     *
     * @ORM\Column(name="threshold", type="decimal", precision=13, scale=0, nullable=true)
     */
    private $threshold;

    /**
     * @var integer
     *
     * @ORM\Column(name="userId", type="integer", nullable=true)
     */
    private $userid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="conversionDate", type="datetime", nullable=true)
     */
    private $conversiondate;



    /**
     * Get logid
     *
     * @return integer 
     */
    public function getLogid()
    {
        return $this->logid;
    }

    /**
     * Set conversionrate
     *
     * @param string $conversionrate
     * @return CurrencyConversionLog
     */
    public function setConversionrate($conversionrate)
    {
        $this->conversionrate = $conversionrate;

        return $this;
    }

    /**
     * Get conversionrate
     *
     * @return string 
     */
    public function getConversionrate()
    {
        return $this->conversionrate;
    }

    /**
     * Set plusfixed
     *
     * @param string $plusfixed
     * @return CurrencyConversionLog
     */
    public function setPlusfixed($plusfixed)
    {
        $this->plusfixed = $plusfixed;

        return $this;
    }

    /**
     * Get plusfixed
     *
     * @return string 
     */
    public function getPlusfixed()
    {
        return $this->plusfixed;
    }

    /**
     * Set threshold
     *
     * @param string $threshold
     * @return CurrencyConversionLog
     */
    public function setThreshold($threshold)
    {
        $this->threshold = $threshold;

        return $this;
    }

    /**
     * Get threshold
     *
     * @return string 
     */
    public function getThreshold()
    {
        return $this->threshold;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     * @return CurrencyConversionLog
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer 
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set conversiondate
     *
     * @param \DateTime $conversiondate
     * @return CurrencyConversionLog
     */
    public function setConversiondate($conversiondate)
    {
        $this->conversiondate = $conversiondate;

        return $this;
    }

    /**
     * Get conversiondate
     *
     * @return \DateTime 
     */
    public function getConversiondate()
    {
        return $this->conversiondate;
    }
}
