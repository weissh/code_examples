<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QueueManager
 *
 * @ORM\Table(name="QUEUE_Manager", indexes={@ORM\Index(name="queueEntryType", columns={"queueEntryType"}), @ORM\Index(name="processed", columns={"processed", "processing"}), @ORM\Index(name="QUEUE_Manager_ibfk_2", columns={"queueReason"}), @ORM\Index(name="queueProcessId", columns={"queueProcessId"})})
 * @ORM\Entity
 */
class QueueManager
{
    /**
     * @var integer
     *
     * @ORM\Column(name="queueId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $queueid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="processed", type="boolean", nullable=false)
     */
    private $processed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="processing", type="boolean", nullable=false)
     */
    private $processing;

    /**
     * @var integer
     *
     * @ORM\Column(name="queueProcessId", type="integer", nullable=false)
     */
    private $queueprocessid;

    /**
     * @var string
     *
     * @ORM\Column(name="queueEntryId", type="string", length=60, nullable=false)
     */
    private $queueentryid;

    /**
     * @var string
     *
     * @ORM\Column(name="queueEntryBody", type="text", nullable=true)
     */
    private $queueentrybody;

    /**
     * @var string
     *
     * @ORM\Column(name="output", type="text", nullable=false)
     */
    private $output;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     * @var integer
     *
     * @ORM\Column(name="createdBy", type="integer", nullable=true)
     */
    private $createdby;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \PrismProductsManager\Entity\QueueEntryTypes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\QueueEntryTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="queueEntryType", referencedColumnName="queueEntryType")
     * })
     */
    private $queueentrytype;

    /**
     * @var \PrismProductsManager\Entity\QueueReasons
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\QueueReasons")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="queueReason", referencedColumnName="queueReason")
     * })
     */
    private $queuereason;



    /**
     * Get queueid
     *
     * @return integer 
     */
    public function getQueueid()
    {
        return $this->queueid;
    }

    /**
     * Set processed
     *
     * @param boolean $processed
     * @return QueueManager
     */
    public function setProcessed($processed)
    {
        $this->processed = $processed;

        return $this;
    }

    /**
     * Get processed
     *
     * @return boolean 
     */
    public function getProcessed()
    {
        return $this->processed;
    }

    /**
     * Set processing
     *
     * @param boolean $processing
     * @return QueueManager
     */
    public function setProcessing($processing)
    {
        $this->processing = $processing;

        return $this;
    }

    /**
     * Get processing
     *
     * @return boolean 
     */
    public function getProcessing()
    {
        return $this->processing;
    }

    /**
     * Set queueprocessid
     *
     * @param integer $queueprocessid
     * @return QueueManager
     */
    public function setQueueprocessid($queueprocessid)
    {
        $this->queueprocessid = $queueprocessid;

        return $this;
    }

    /**
     * Get queueprocessid
     *
     * @return integer 
     */
    public function getQueueprocessid()
    {
        return $this->queueprocessid;
    }

    /**
     * Set queueentryid
     *
     * @param string $queueentryid
     * @return QueueManager
     */
    public function setQueueentryid($queueentryid)
    {
        $this->queueentryid = $queueentryid;

        return $this;
    }

    /**
     * Get queueentryid
     *
     * @return string 
     */
    public function getQueueentryid()
    {
        return $this->queueentryid;
    }

    /**
     * Set queueentrybody
     *
     * @param string $queueentrybody
     * @return QueueManager
     */
    public function setQueueentrybody($queueentrybody)
    {
        $this->queueentrybody = $queueentrybody;

        return $this;
    }

    /**
     * Get queueentrybody
     *
     * @return string 
     */
    public function getQueueentrybody()
    {
        return $this->queueentrybody;
    }

    /**
     * Set output
     *
     * @param string $output
     * @return QueueManager
     */
    public function setOutput($output)
    {
        $this->output = $output;

        return $this;
    }

    /**
     * Get output
     *
     * @return string 
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return QueueManager
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set createdby
     *
     * @param integer $createdby
     * @return QueueManager
     */
    public function setCreatedby($createdby)
    {
        $this->createdby = $createdby;

        return $this;
    }

    /**
     * Get createdby
     *
     * @return integer 
     */
    public function getCreatedby()
    {
        return $this->createdby;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return QueueManager
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set queueentrytype
     *
     * @param \PrismProductsManager\Entity\QueueEntryTypes $queueentrytype
     * @return QueueManager
     */
    public function setQueueentrytype(\PrismProductsManager\Entity\QueueEntryTypes $queueentrytype = null)
    {
        $this->queueentrytype = $queueentrytype;

        return $this;
    }

    /**
     * Get queueentrytype
     *
     * @return \PrismProductsManager\Entity\QueueEntryTypes 
     */
    public function getQueueentrytype()
    {
        return $this->queueentrytype;
    }

    /**
     * Set queuereason
     *
     * @param \PrismProductsManager\Entity\QueueReasons $queuereason
     * @return QueueManager
     */
    public function setQueuereason(\PrismProductsManager\Entity\QueueReasons $queuereason = null)
    {
        $this->queuereason = $queuereason;

        return $this;
    }

    /**
     * Get queuereason
     *
     * @return \PrismProductsManager\Entity\QueueReasons 
     */
    public function getQueuereason()
    {
        return $this->queuereason;
    }
}
