<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ColoursGroups
 *
 * @ORM\Table(name="COLOURS_Groups", uniqueConstraints={@ORM\UniqueConstraint(name="colourGroupId", columns={"colourGroupId"})})
 * @ORM\Entity
 */
class ColoursGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="colourGroupId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $colourgroupid;

    /**
     * @var string
     *
     * @ORM\Column(name="groupCode", type="string", length=30, nullable=false)
     */
    private $groupcode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hasImage", type="boolean", nullable=false)
     */
    private $hasimage;

    /**
     * @var string
     *
     * @ORM\Column(name="hex", type="string", length=7, nullable=true)
     */
    private $hex;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    private $enabled;

    /**
     * @var integer
     *
     * @ORM\Column(name="imageId", type="integer", nullable=true)
     */
    private $imageid;



    /**
     * Get colourgroupid
     *
     * @return integer 
     */
    public function getColourgroupid()
    {
        return $this->colourgroupid;
    }

    /**
     * Set groupcode
     *
     * @param string $groupcode
     * @return ColoursGroups
     */
    public function setGroupcode($groupcode)
    {
        $this->groupcode = $groupcode;

        return $this;
    }

    /**
     * Get groupcode
     *
     * @return string 
     */
    public function getGroupcode()
    {
        return $this->groupcode;
    }

    /**
     * Set hasimage
     *
     * @param boolean $hasimage
     * @return ColoursGroups
     */
    public function setHasimage($hasimage)
    {
        $this->hasimage = $hasimage;

        return $this;
    }

    /**
     * Get hasimage
     *
     * @return boolean 
     */
    public function getHasimage()
    {
        return $this->hasimage;
    }

    /**
     * Set hex
     *
     * @param string $hex
     * @return ColoursGroups
     */
    public function setHex($hex)
    {
        $this->hex = $hex;

        return $this;
    }

    /**
     * Get hex
     *
     * @return string 
     */
    public function getHex()
    {
        return $this->hex;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return ColoursGroups
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set imageid
     *
     * @param integer $imageid
     * @return ColoursGroups
     */
    public function setImageid($imageid)
    {
        $this->imageid = $imageid;

        return $this;
    }

    /**
     * Get imageid
     *
     * @return integer 
     */
    public function getImageid()
    {
        return $this->imageid;
    }
}
