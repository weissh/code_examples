<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AttributesGroupsValidations
 *
 * @ORM\Table(name="ATTRIBUTES_Groups_Validations", uniqueConstraints={@ORM\UniqueConstraint(name="Index 1", columns={"attributeGroupId"}), @ORM\UniqueConstraint(name="Index 3", columns={"attributeGroupValidationId"})}, indexes={@ORM\Index(name="Index 4", columns={"validationType"})})
 * @ORM\Entity
 */
class AttributesGroupsValidations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="attributeGroupValidationId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $attributegroupvalidationid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isRequired", type="boolean", nullable=false)
     */
    private $isrequired;

    /**
     * @var string
     *
     * @ORM\Column(name="validationType", type="string", nullable=true)
     */
    private $validationtype;

    /**
     * @var \PrismProductsManager\Entity\AttributesGroups
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\AttributesGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="attributeGroupId", referencedColumnName="attributeGroupId")
     * })
     */
    private $attributegroupid;



    /**
     * Get attributegroupvalidationid
     *
     * @return integer 
     */
    public function getAttributegroupvalidationid()
    {
        return $this->attributegroupvalidationid;
    }

    /**
     * Set isrequired
     *
     * @param boolean $isrequired
     * @return AttributesGroupsValidations
     */
    public function setIsrequired($isrequired)
    {
        $this->isrequired = $isrequired;

        return $this;
    }

    /**
     * Get isrequired
     *
     * @return boolean 
     */
    public function getIsrequired()
    {
        return $this->isrequired;
    }

    /**
     * Set validationtype
     *
     * @param string $validationtype
     * @return AttributesGroupsValidations
     */
    public function setValidationtype($validationtype)
    {
        $this->validationtype = $validationtype;

        return $this;
    }

    /**
     * Get validationtype
     *
     * @return string 
     */
    public function getValidationtype()
    {
        return $this->validationtype;
    }

    /**
     * Set attributegroupid
     *
     * @param \PrismProductsManager\Entity\AttributesGroups $attributegroupid
     * @return AttributesGroupsValidations
     */
    public function setAttributegroupid(\PrismProductsManager\Entity\AttributesGroups $attributegroupid = null)
    {
        $this->attributegroupid = $attributegroupid;

        return $this;
    }

    /**
     * Get attributegroupid
     *
     * @return \PrismProductsManager\Entity\AttributesGroups 
     */
    public function getAttributegroupid()
    {
        return $this->attributegroupid;
    }
}
