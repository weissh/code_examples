<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputValidationMessagesOld
 *
 * @ORM\Table(name="INPUT_Validation_Messages_old", indexes={@ORM\Index(name="FK_INPUT_Validation_Messages_INPUT_Validations", columns={"inputValidationId"})})
 * @ORM\Entity
 */
class InputValidationMessagesOld
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputValidationMessageId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputvalidationmessageid;

    /**
     * @var string
     *
     * @ORM\Column(name="inputValidationMessageKey", type="string", length=250, nullable=true)
     */
    private $inputvalidationmessagekey;

    /**
     * @var string
     *
     * @ORM\Column(name="inputValidationMessage", type="string", length=250, nullable=true)
     */
    private $inputvalidationmessage;

    /**
     * @var \PrismProductsManager\Entity\InputValidations
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputValidations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputValidationId", referencedColumnName="inputValidationId")
     * })
     */
    private $inputvalidationid;



    /**
     * Get inputvalidationmessageid
     *
     * @return integer 
     */
    public function getInputvalidationmessageid()
    {
        return $this->inputvalidationmessageid;
    }

    /**
     * Set inputvalidationmessagekey
     *
     * @param string $inputvalidationmessagekey
     * @return InputValidationMessagesOld
     */
    public function setInputvalidationmessagekey($inputvalidationmessagekey)
    {
        $this->inputvalidationmessagekey = $inputvalidationmessagekey;

        return $this;
    }

    /**
     * Get inputvalidationmessagekey
     *
     * @return string 
     */
    public function getInputvalidationmessagekey()
    {
        return $this->inputvalidationmessagekey;
    }

    /**
     * Set inputvalidationmessage
     *
     * @param string $inputvalidationmessage
     * @return InputValidationMessagesOld
     */
    public function setInputvalidationmessage($inputvalidationmessage)
    {
        $this->inputvalidationmessage = $inputvalidationmessage;

        return $this;
    }

    /**
     * Get inputvalidationmessage
     *
     * @return string 
     */
    public function getInputvalidationmessage()
    {
        return $this->inputvalidationmessage;
    }

    /**
     * Set inputvalidationid
     *
     * @param \PrismProductsManager\Entity\InputValidations $inputvalidationid
     * @return InputValidationMessagesOld
     */
    public function setInputvalidationid(\PrismProductsManager\Entity\InputValidations $inputvalidationid = null)
    {
        $this->inputvalidationid = $inputvalidationid;

        return $this;
    }

    /**
     * Get inputvalidationid
     *
     * @return \PrismProductsManager\Entity\InputValidations 
     */
    public function getInputvalidationid()
    {
        return $this->inputvalidationid;
    }
}
