<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputFields
 *
 * @ORM\Table(name="INPUT_Fields", indexes={@ORM\Index(name="inputTypeId_idx", columns={"inputTypeId"}), @ORM\Index(name="fk_INPUT_Fields_INPUT_Tablefields1_idx", columns={"INPUT_Tablefields_inputTablefieldId"}), @ORM\Index(name="fk_INPUT_Fields_INPUT_Modules1_idx", columns={"INPUT_Modules_inputModuleId"}), @ORM\Index(name="fk_INPUT_Fields_INPUT_Field_R_Attributes1_idx", columns={"INPUT_Field_R_Attributes_inputFieldRAttributeId"})})
 * @ORM\Entity
 */
class InputFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputFieldId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputfieldid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * @var \PrismProductsManager\Entity\InputTablefields
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputTablefields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="INPUT_Tablefields_inputTablefieldId", referencedColumnName="inputTablefieldId")
     * })
     */
    private $inputTablefieldsInputtablefieldid;

    /**
     * @var \PrismProductsManager\Entity\InputModules
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputModules")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="INPUT_Modules_inputModuleId", referencedColumnName="inputModuleId")
     * })
     */
    private $inputModulesInputmoduleid;

    /**
     * @var \PrismProductsManager\Entity\InputFieldRAttributes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputFieldRAttributes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="INPUT_Field_R_Attributes_inputFieldRAttributeId", referencedColumnName="inputFieldRAttributeId")
     * })
     */
    private $inputFieldRAttributesInputfieldrattributeid;

    /**
     * @var \PrismProductsManager\Entity\InputFieldTypes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\InputFieldTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inputTypeId", referencedColumnName="inputFieldTypeId")
     * })
     */
    private $inputtypeid;

    /**
     *
     *  @var \PrismProductsManager\Entity\InputFieldRPages
     *
     *  @ORM\OneToMany(targetEntity="PrismProductsManager\Entity\InputFieldRPages", mappedBy="inputfieldid")
     */
    private $inputFieldRPages;

    /**
     *
     *  @var \PrismProductsManager\Entity\InputFieldRWorkflows
     *
     *  @ORM\OneToMany(targetEntity="PrismProductsManager\Entity\InputFieldRWorkflows", mappedBy="inputfieldid")
     */
    private $inputFieldRWorkflows;

    /**
     *
     *  @var \PrismProductsManager\Entity\InputFieldRPages
     *
     *  @ORM\OneToMany(targetEntity="PrismProductsManager\Entity\InputFieldsVisibility", mappedBy="inputfieldid")
     */
    private $inputFieldsVisibility;

    /**
     *
     *  @var \PrismProductsManager\Entity\InputValidations
     *
     *  @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\InputValidations", mappedBy="inputfieldid")
     */
    private $inputValidations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="editable", type="boolean", nullable=false)
     */
    private $editable;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isSchedulable", type="boolean", nullable=false)
     */
    private $schedulable;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hidden", type="boolean", nullable=false)
     */
    private $hidden;

    /**
     * @var string
     *
     * @ORM\Column(name="editableAtLevel", type="string", length=100, nullable=false)
     */
    private $editableAtLevel;

    /**
     * @return boolean
     */
    public function isHidden()
    {
        return $this->hidden;
    }

    /**
     * @param boolean $hidden
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @return string
     */
    public function getEditableAtLevel()
    {
        return $this->editableAtLevel;
    }

    /**
     * @param string $editableAtLevel
     */
    public function setEditableAtLevel($editableAtLevel)
    {
        $this->editableAtLevel = $editableAtLevel;
    }

    /**
     * @return boolean
     */
    public function isSchedulable()
    {
        return $this->schedulable;
    }

    /**
     * @param boolean $schedulable
     */
    public function setSchedulable($schedulable)
    {
        $this->schedulable = $schedulable;
    }

    /**
     * @return InputFieldRWorkflows
     */
    public function getInputFieldRWorkflows()
    {
        return $this->inputFieldRWorkflows;
    }

    /**
     * @param InputFieldRWorkflows $inputFieldRWorkflows
     */
    public function setInputFieldRWorkflows($inputFieldRWorkflows)
    {
        $this->inputFieldRWorkflows = $inputFieldRWorkflows;
    }

    
    /**
     * @return boolean
     */
    public function isEditable()
    {
        return $this->editable;
    }

    /**
     * @param boolean $editable
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;
    }

    /**
     * @return InputValidations
     */
    public function getInputValidations()
    {
        return $this->inputValidations;
    }

    /**
     * @return InputFieldRPages
     */
    public function getInputFieldsVisibility()
    {
        return $this->inputFieldsVisibility;
    }

    /**
     * @param InputFieldRPages $inputFieldsVisibility
     */
    public function setInputFieldsVisibility($inputFieldsVisibility)
    {
        $this->inputFieldsVisibility = $inputFieldsVisibility;
    }


    /**
     * Get inputfieldid
     *
     * @return integer 
     */
    public function getInputfieldid()
    {
        return $this->inputfieldid;
    }

    /**
     * @return InputFieldRPages
     */
    public function getInputFieldRPages()
    {
        return $this->inputFieldRPages;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputFields
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputFields
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set inputTablefieldsInputtablefieldid
     *
     * @param \PrismProductsManager\Entity\InputTablefields $inputTablefieldsInputtablefieldid
     * @return InputFields
     */
    public function setInputTablefieldsInputtablefieldid(\PrismProductsManager\Entity\InputTablefields $inputTablefieldsInputtablefieldid = null)
    {
        $this->inputTablefieldsInputtablefieldid = $inputTablefieldsInputtablefieldid;

        return $this;
    }

    /**
     * Get inputTablefieldsInputtablefieldid
     *
     * @return \PrismProductsManager\Entity\InputTablefields 
     */
    public function getInputTablefieldsInputtablefieldid()
    {
        return $this->inputTablefieldsInputtablefieldid;
    }

    /**
     * Set inputModulesInputmoduleid
     *
     * @param \PrismProductsManager\Entity\InputModules $inputModulesInputmoduleid
     * @return InputFields
     */
    public function setInputModulesInputmoduleid(\PrismProductsManager\Entity\InputModules $inputModulesInputmoduleid = null)
    {
        $this->inputModulesInputmoduleid = $inputModulesInputmoduleid;

        return $this;
    }

    /**
     * Get inputModulesInputmoduleid
     *
     * @return \PrismProductsManager\Entity\InputModules 
     */
    public function getInputModulesInputmoduleid()
    {
        return $this->inputModulesInputmoduleid;
    }

    /**
     * Set inputFieldRAttributesInputfieldrattributeid
     *
     * @param \PrismProductsManager\Entity\InputFieldRAttributes $inputFieldRAttributesInputfieldrattributeid
     * @return InputFields
     */
    public function setInputFieldRAttributesInputfieldrattributeid(\PrismProductsManager\Entity\InputFieldRAttributes $inputFieldRAttributesInputfieldrattributeid = null)
    {
        $this->inputFieldRAttributesInputfieldrattributeid = $inputFieldRAttributesInputfieldrattributeid;

        return $this;
    }

    /**
     * Get inputFieldRAttributesInputfieldrattributeid
     *
     * @return \PrismProductsManager\Entity\InputFieldRAttributes 
     */
    public function getInputFieldRAttributesInputfieldrattributeid()
    {
        return $this->inputFieldRAttributesInputfieldrattributeid;
    }

    /**
     * Set inputtypeid
     *
     * @param \PrismProductsManager\Entity\InputFieldTypes $inputtypeid
     * @return InputFields
     */
    public function setInputtypeid(\PrismProductsManager\Entity\InputFieldTypes $inputtypeid = null)
    {
        $this->inputtypeid = $inputtypeid;

        return $this;
    }

    /**
     * Get inputtypeid
     *
     * @return \PrismProductsManager\Entity\InputFieldTypes 
     */
    public function getInputtypeid()
    {
        return $this->inputtypeid;
    }
}
