<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommonAttributesValues
 *
 * @ORM\Table(name="COMMON_ATTRIBUTES_VALUES")
 * @ORM\Entity(repositoryClass="PrismProductsManager\Entity\Repository\CommonAttributesValuesRepository")
 */
class CommonAttributesValues
{
    const FIELD_ABBREV = 'commonAttributeValueAbbrev';
    const FIELD_SKU_CODE = 'commonattributevalueskucode';
    const FIELD_BASE_SIZE = 'commonAttributeValueBaseSize';

    /**
     * @var integer
     *
     * @ORM\Column(name="commonAttributeValueId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commonAttributeValueId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    private $enabled;

    /**
     * @var string
     *
     * @ORM\Column(name="commonAttributeValueSkuCode", type="string", length=10, nullable=false)
     */
    private $commonattributevalueskucode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="commonAttributeValueAbbrev", type="string", length=4, nullable=false)
     */
    private $commonAttributeValueAbbrev = '';

    /**
     * @var string
     *
     * @ORM\Column(name="commonAttributeValueBaseSize", type="string", length=4, nullable=false)
     */
    private $commonAttributeValueBaseSize = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="territoryId", type="integer", nullable=true)
     */
    private $territoryid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     * @var integer
     *
     * @ORM\Column(name="modifiedBy", type="integer", nullable=false)
     */
    private $modifiedby;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributes
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\CommonAttributes" , inversedBy="commonAttributeId")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="commonAttributeId", referencedColumnName="commonAttributeId")
     * })
     */
    private $commonattributeid;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributesValuesDefinitions
     *
     * @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesValuesDefinitions" , mappedBy="commonAttributeValueId", cascade={"persist"})
     */
    private $commonAttributeValuesDefinitions;

    /**
     * @var \PrismProductsManager\Entity\CommonAttributesValuesGuiRepresentation
     *
     * @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesValuesGuiRepresentation" , mappedBy="commonAttributeValueId", cascade={"persist"})
     */
    private $commonAttributeValuesGuiRepresentation;

    /**
     *
     * @var \PrismProductsManager\Entity\CommonAttributesValuesEquivalence
     *
     * @ORM\OneToMany(targetEntity="PrismProductsManager\Entity\CommonAttributesValuesEquivalence", mappedBy="commonAttributeValueId")
     */
    private $commonAttributesValuesEquivalence;

    /**
     *
     * @var \PrismProductsManager\Entity\CommonAttributesValuesChain
     *
     * @ORM\OneToMany(targetEntity="PrismProductsManager\Entity\CommonAttributesValuesChain", mappedBy="sourcevalueid")
     */
    private $commonAttributesSourceValuesChain;

    /**
     *
     * @var \PrismProductsManager\Entity\CommonAttributesValuesChain
     *
     * @ORM\OneToMany(targetEntity="PrismProductsManager\Entity\CommonAttributesValuesOverrides", mappedBy="commonattributevalueid")
     */
    private $commonAttributesValuesOverrides;

    /**
     *
     * @var \PrismProductsManager\Entity\CommonAttributesValuesChain
     *
     * @ORM\OneToMany(targetEntity="PrismProductsManager\Entity\CommonAttributesValuesChain", mappedBy="destinationvalueid")
     */
    private $commonAttributesDestinationValuesChain;

    /**
     *
     * @var \PrismProductsManager\Entity\CommonAttributesValuesAutoFromDate
     *
     * @ORM\OneToOne(targetEntity="PrismProductsManager\Entity\CommonAttributesValuesAutoFromDate", mappedBy="commonAttributeValueId")
     */
    private $commonAttributesValuesAutoFromDate;

    /**
     * @return string
     */
    public function getCommonAttributeValueBaseSize()
    {
        return $this->commonAttributeValueBaseSize;
    }

    /**
     * @param string $commonAttributeValueBaseSize
     */
    public function setCommonAttributeValueBaseSize($commonAttributeValueBaseSize)
    {
        $this->commonAttributeValueBaseSize = $commonAttributeValueBaseSize;
    }

    /**
     * @return string
     */
    public function getCommonAttributeValueAbbrev()
    {
        return $this->commonAttributeValueAbbrev;
    }

    /**
     * @param string $commonAttributeValueAbbrev
     */
    public function setCommonAttributeValueAbbrev($commonAttributeValueAbbrev)
    {
        $this->commonAttributeValueAbbrev = $commonAttributeValueAbbrev;
    }

    /**
     * @return CommonAttributesValuesChain
     */
    public function getCommonAttributesValuesOverrides()
    {
        return $this->commonAttributesValuesOverrides;
    }

    /**
     * @param CommonAttributesValuesChain $commonAttributesValuesOverrides
     */
    public function setCommonAttributesValuesOverrides($commonAttributesValuesOverrides)
    {
        $this->commonAttributesValuesOverrides = $commonAttributesValuesOverrides;
    }

    /**
     * @return CommonAttributesValuesChain
     */
    public function getCommonAttributesDestinationValuesChain()
    {
        return $this->commonAttributesDestinationValuesChain;
    }

    /**
     * @return CommonAttributesValuesChain
     */
    public function getCommonAttributesSourceValuesChain()
    {
        return $this->commonAttributesSourceValuesChain;
    }

    /**
     * Get commonAttributeValueId
     *
     * @return integer
     */
    public function getcommonAttributeValueId()
    {
        return $this->commonAttributeValueId;
    }

    /**
     * @param int $commonAttributeValueId
     */
    public function setcommonAttributeValueId($commonAttributeValueId)
    {
        $this->commonAttributeValueId = $commonAttributeValueId;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return CommonAttributesValues
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get commonattributevalueskucode
     *
     * @return string
     */
    public function getCommonattributevalueskucode()
    {
        return $this->commonattributevalueskucode;
    }

    /**
     * Set commonattributevalueskucode
     *
     * @param string $commonattributevalueskucode
     *
     * @return CommonAttributesValues
     */
    public function setCommonattributevalueskucode($commonattributevalueskucode)
    {
        $this->commonattributevalueskucode = $commonattributevalueskucode;

        return $this;
    }

    /**
     * Get territoryid
     *
     * @return integer
     */
    public function getTerritoryid()
    {
        return $this->territoryid;
    }

    /**
     * Set territoryid
     *
     * @param integer $territoryid
     *
     * @return CommonAttributesValues
     */
    public function setTerritoryid($territoryid)
    {
        $this->territoryid = $territoryid;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     *
     * @return CommonAttributesValues
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     *
     * @return CommonAttributesValues
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get modifiedby
     *
     * @return integer
     */
    public function getModifiedby()
    {
        return $this->modifiedby;
    }

    /**
     * Set modifiedby
     *
     * @param integer $modifiedby
     *
     * @return CommonAttributesValues
     */
    public function setModifiedby($modifiedby)
    {
        $this->modifiedby = $modifiedby;

        return $this;
    }

    /**
     * Get commonattributeid
     *
     * @return \PrismProductsManager\Entity\CommonAttributes
     */
    public function getCommonattributeid()
    {
        return $this->commonattributeid;
    }

    /**
     * Set commonattributeid
     *
     * @param \PrismProductsManager\Entity\CommonAttributes $commonattributeid
     *
     * @return CommonAttributesValues
     */
    public function setCommonattributeid(\PrismProductsManager\Entity\CommonAttributes $commonattributeid = null)
    {
        $this->commonattributeid = $commonattributeid;

        return $this;
    }

    /**
     * @return CommonAttributesValuesDefinitions
     */
    public function getCommonAttributeValuesDefinitions()
    {
        return $this->commonAttributeValuesDefinitions;
    }

    /**
     * @param CommonAttributesValuesDefinitions $commonAttributeValuesDefinitions
     */
    public function setCommonAttributeValuesDefinitions($commonAttributeValuesDefinitions)
    {
        $this->commonAttributeValuesDefinitions = $commonAttributeValuesDefinitions;
    }

    /**
     * @return CommonAttributesValuesGuiRepresentation
     */
    public function getCommonAttributeValuesGuiRepresentation()
    {
        return $this->commonAttributeValuesGuiRepresentation;
    }

    /**
     * @param CommonAttributesValuesGuiRepresentation $commonAttributeValuesGuiRepresentation
     */
    public function setCommonAttributeValuesGuiRepresentation($commonAttributeValuesGuiRepresentation)
    {
        $this->commonAttributeValuesGuiRepresentation = $commonAttributeValuesGuiRepresentation;
    }

    /**
     * @return CommonAttributesValuesEquivalence
     */
    public function getCommonAttributesValuesEquivalence()
    {
        return $this->commonAttributesValuesEquivalence;
    }

    /**
     * @param CommonAttributesValues $commonAttributesValuesEquivalence
     */
    public function setCommonAttributesValuesEquivalence($commonAttributesValuesEquivalence)
    {
        $this->commonAttributesValuesEquivalence = $commonAttributesValuesEquivalence;
    }

    /**
     * @return CommonAttributesValuesAutoFromDate
     */
    public function getCommonAttributesValuesAutoFromDate()
    {
        return $this->commonAttributesValuesAutoFromDate;
    }

    /**
     * @param CommonAttributesValuesAutoFromDate $commonAttributesValuesAutoFromDate
     */
    public function setCommonAttributesValuesAutoFromDate($commonAttributesValuesAutoFromDate)
    {
        $this->commonAttributesValuesAutoFromDate = $commonAttributesValuesAutoFromDate;
    }
}
