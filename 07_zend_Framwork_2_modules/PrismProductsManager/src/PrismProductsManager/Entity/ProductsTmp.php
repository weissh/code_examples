<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsTmp
 *
 * @ORM\Table(name="products_TMP")
 * @ORM\Entity
 */
class ProductsTmp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productsTmpId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productstmpid;

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=50, nullable=false)
     */
    private $sku;

    /**
     * @var integer
     *
     * @ORM\Column(name="spectrumId", type="integer", nullable=false)
     */
    private $spectrumid;

    /**
     * @var string
     *
     * @ORM\Column(name="productIdFrom", type="string", length=50, nullable=false)
     */
    private $productidfrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="productId", type="integer", nullable=false)
     */
    private $productid;

    /**
     * @var string
     *
     * @ORM\Column(name="categoryPMS", type="string", length=50, nullable=true)
     */
    private $categorypms;

    /**
     * @var string
     *
     * @ORM\Column(name="categorySPECTRUM", type="string", length=50, nullable=true)
     */
    private $categoryspectrum;

    /**
     * @var string
     *
     * @ORM\Column(name="subcategoryPMS", type="string", length=50, nullable=true)
     */
    private $subcategorypms;

    /**
     * @var string
     *
     * @ORM\Column(name="subcategorySPECTRUM", type="string", length=50, nullable=true)
     */
    private $subcategoryspectrum;

    /**
     * @var string
     *
     * @ORM\Column(name="rangePMS", type="string", length=50, nullable=true)
     */
    private $rangepms;

    /**
     * @var string
     *
     * @ORM\Column(name="rangeSPECTRUM", type="string", length=50, nullable=true)
     */
    private $rangespectrum;

    /**
     * @var string
     *
     * @ORM\Column(name="colourPMS", type="string", length=50, nullable=true)
     */
    private $colourpms;

    /**
     * @var string
     *
     * @ORM\Column(name="colourSPECTRUM", type="string", length=50, nullable=true)
     */
    private $colourspectrum;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionPMS", type="text", nullable=true)
     */
    private $descriptionpms;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionSPECTRUM", type="text", nullable=true)
     */
    private $descriptionspectrum;

    /**
     * @var string
     *
     * @ORM\Column(name="supplierPMS", type="string", length=50, nullable=true)
     */
    private $supplierpms;

    /**
     * @var string
     *
     * @ORM\Column(name="supplierSPECTRUM", type="string", length=50, nullable=true)
     */
    private $supplierspectrum;

    /**
     * @var float
     *
     * @ORM\Column(name="wasPricePMS", type="float", precision=10, scale=0, nullable=true)
     */
    private $waspricepms;

    /**
     * @var float
     *
     * @ORM\Column(name="wasPriceSPECTRUM", type="float", precision=10, scale=0, nullable=true)
     */
    private $waspricespectrum;

    /**
     * @var float
     *
     * @ORM\Column(name="nowPricePMS", type="float", precision=10, scale=0, nullable=true)
     */
    private $nowpricepms;

    /**
     * @var float
     *
     * @ORM\Column(name="nowPriceSPECTRUM", type="float", precision=10, scale=0, nullable=true)
     */
    private $nowpricespectrum;

    /**
     * @var float
     *
     * @ORM\Column(name="costPricePMS", type="float", precision=10, scale=0, nullable=true)
     */
    private $costpricepms;

    /**
     * @var float
     *
     * @ORM\Column(name="costPriceSPECTRUM", type="float", precision=10, scale=0, nullable=true)
     */
    private $costpricespectrum;



    /**
     * Get productstmpid
     *
     * @return integer 
     */
    public function getProductstmpid()
    {
        return $this->productstmpid;
    }

    /**
     * Set sku
     *
     * @param string $sku
     * @return ProductsTmp
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string 
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set spectrumid
     *
     * @param integer $spectrumid
     * @return ProductsTmp
     */
    public function setSpectrumid($spectrumid)
    {
        $this->spectrumid = $spectrumid;

        return $this;
    }

    /**
     * Get spectrumid
     *
     * @return integer 
     */
    public function getSpectrumid()
    {
        return $this->spectrumid;
    }

    /**
     * Set productidfrom
     *
     * @param string $productidfrom
     * @return ProductsTmp
     */
    public function setProductidfrom($productidfrom)
    {
        $this->productidfrom = $productidfrom;

        return $this;
    }

    /**
     * Get productidfrom
     *
     * @return string 
     */
    public function getProductidfrom()
    {
        return $this->productidfrom;
    }

    /**
     * Set productid
     *
     * @param integer $productid
     * @return ProductsTmp
     */
    public function setProductid($productid)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return integer 
     */
    public function getProductid()
    {
        return $this->productid;
    }

    /**
     * Set categorypms
     *
     * @param string $categorypms
     * @return ProductsTmp
     */
    public function setCategorypms($categorypms)
    {
        $this->categorypms = $categorypms;

        return $this;
    }

    /**
     * Get categorypms
     *
     * @return string 
     */
    public function getCategorypms()
    {
        return $this->categorypms;
    }

    /**
     * Set categoryspectrum
     *
     * @param string $categoryspectrum
     * @return ProductsTmp
     */
    public function setCategoryspectrum($categoryspectrum)
    {
        $this->categoryspectrum = $categoryspectrum;

        return $this;
    }

    /**
     * Get categoryspectrum
     *
     * @return string 
     */
    public function getCategoryspectrum()
    {
        return $this->categoryspectrum;
    }

    /**
     * Set subcategorypms
     *
     * @param string $subcategorypms
     * @return ProductsTmp
     */
    public function setSubcategorypms($subcategorypms)
    {
        $this->subcategorypms = $subcategorypms;

        return $this;
    }

    /**
     * Get subcategorypms
     *
     * @return string 
     */
    public function getSubcategorypms()
    {
        return $this->subcategorypms;
    }

    /**
     * Set subcategoryspectrum
     *
     * @param string $subcategoryspectrum
     * @return ProductsTmp
     */
    public function setSubcategoryspectrum($subcategoryspectrum)
    {
        $this->subcategoryspectrum = $subcategoryspectrum;

        return $this;
    }

    /**
     * Get subcategoryspectrum
     *
     * @return string 
     */
    public function getSubcategoryspectrum()
    {
        return $this->subcategoryspectrum;
    }

    /**
     * Set rangepms
     *
     * @param string $rangepms
     * @return ProductsTmp
     */
    public function setRangepms($rangepms)
    {
        $this->rangepms = $rangepms;

        return $this;
    }

    /**
     * Get rangepms
     *
     * @return string 
     */
    public function getRangepms()
    {
        return $this->rangepms;
    }

    /**
     * Set rangespectrum
     *
     * @param string $rangespectrum
     * @return ProductsTmp
     */
    public function setRangespectrum($rangespectrum)
    {
        $this->rangespectrum = $rangespectrum;

        return $this;
    }

    /**
     * Get rangespectrum
     *
     * @return string 
     */
    public function getRangespectrum()
    {
        return $this->rangespectrum;
    }

    /**
     * Set colourpms
     *
     * @param string $colourpms
     * @return ProductsTmp
     */
    public function setColourpms($colourpms)
    {
        $this->colourpms = $colourpms;

        return $this;
    }

    /**
     * Get colourpms
     *
     * @return string 
     */
    public function getColourpms()
    {
        return $this->colourpms;
    }

    /**
     * Set colourspectrum
     *
     * @param string $colourspectrum
     * @return ProductsTmp
     */
    public function setColourspectrum($colourspectrum)
    {
        $this->colourspectrum = $colourspectrum;

        return $this;
    }

    /**
     * Get colourspectrum
     *
     * @return string 
     */
    public function getColourspectrum()
    {
        return $this->colourspectrum;
    }

    /**
     * Set descriptionpms
     *
     * @param string $descriptionpms
     * @return ProductsTmp
     */
    public function setDescriptionpms($descriptionpms)
    {
        $this->descriptionpms = $descriptionpms;

        return $this;
    }

    /**
     * Get descriptionpms
     *
     * @return string 
     */
    public function getDescriptionpms()
    {
        return $this->descriptionpms;
    }

    /**
     * Set descriptionspectrum
     *
     * @param string $descriptionspectrum
     * @return ProductsTmp
     */
    public function setDescriptionspectrum($descriptionspectrum)
    {
        $this->descriptionspectrum = $descriptionspectrum;

        return $this;
    }

    /**
     * Get descriptionspectrum
     *
     * @return string 
     */
    public function getDescriptionspectrum()
    {
        return $this->descriptionspectrum;
    }

    /**
     * Set supplierpms
     *
     * @param string $supplierpms
     * @return ProductsTmp
     */
    public function setSupplierpms($supplierpms)
    {
        $this->supplierpms = $supplierpms;

        return $this;
    }

    /**
     * Get supplierpms
     *
     * @return string 
     */
    public function getSupplierpms()
    {
        return $this->supplierpms;
    }

    /**
     * Set supplierspectrum
     *
     * @param string $supplierspectrum
     * @return ProductsTmp
     */
    public function setSupplierspectrum($supplierspectrum)
    {
        $this->supplierspectrum = $supplierspectrum;

        return $this;
    }

    /**
     * Get supplierspectrum
     *
     * @return string 
     */
    public function getSupplierspectrum()
    {
        return $this->supplierspectrum;
    }

    /**
     * Set waspricepms
     *
     * @param float $waspricepms
     * @return ProductsTmp
     */
    public function setWaspricepms($waspricepms)
    {
        $this->waspricepms = $waspricepms;

        return $this;
    }

    /**
     * Get waspricepms
     *
     * @return float 
     */
    public function getWaspricepms()
    {
        return $this->waspricepms;
    }

    /**
     * Set waspricespectrum
     *
     * @param float $waspricespectrum
     * @return ProductsTmp
     */
    public function setWaspricespectrum($waspricespectrum)
    {
        $this->waspricespectrum = $waspricespectrum;

        return $this;
    }

    /**
     * Get waspricespectrum
     *
     * @return float 
     */
    public function getWaspricespectrum()
    {
        return $this->waspricespectrum;
    }

    /**
     * Set nowpricepms
     *
     * @param float $nowpricepms
     * @return ProductsTmp
     */
    public function setNowpricepms($nowpricepms)
    {
        $this->nowpricepms = $nowpricepms;

        return $this;
    }

    /**
     * Get nowpricepms
     *
     * @return float 
     */
    public function getNowpricepms()
    {
        return $this->nowpricepms;
    }

    /**
     * Set nowpricespectrum
     *
     * @param float $nowpricespectrum
     * @return ProductsTmp
     */
    public function setNowpricespectrum($nowpricespectrum)
    {
        $this->nowpricespectrum = $nowpricespectrum;

        return $this;
    }

    /**
     * Get nowpricespectrum
     *
     * @return float 
     */
    public function getNowpricespectrum()
    {
        return $this->nowpricespectrum;
    }

    /**
     * Set costpricepms
     *
     * @param float $costpricepms
     * @return ProductsTmp
     */
    public function setCostpricepms($costpricepms)
    {
        $this->costpricepms = $costpricepms;

        return $this;
    }

    /**
     * Get costpricepms
     *
     * @return float 
     */
    public function getCostpricepms()
    {
        return $this->costpricepms;
    }

    /**
     * Set costpricespectrum
     *
     * @param float $costpricespectrum
     * @return ProductsTmp
     */
    public function setCostpricespectrum($costpricespectrum)
    {
        $this->costpricespectrum = $costpricespectrum;

        return $this;
    }

    /**
     * Get costpricespectrum
     *
     * @return float 
     */
    public function getCostpricespectrum()
    {
        return $this->costpricespectrum;
    }
}
