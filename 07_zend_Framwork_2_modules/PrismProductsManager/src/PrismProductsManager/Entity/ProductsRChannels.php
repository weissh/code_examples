<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsRChannels
 *
 * @ORM\Table(name="PRODUCTS_R_CHANNELS", indexes={@ORM\Index(name="productId", columns={"productId"}), @ORM\Index(name="channelId", columns={"channelId"}), @ORM\Index(name="status", columns={"status"}), @ORM\Index(name="productId_channelId_status", columns={"productId", "channelId", "status"})})
 * @ORM\Entity
 */
class ProductsRChannels
{
    /**
     * @var integer
     *
     * @ORM\Column(name="productChannelId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productchannelid;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=false)
     */
    private $modified;

    /**
     * @var \PrismProductsManager\Entity\Channels
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Channels")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="channelId", referencedColumnName="channelId")
     * })
     */
    private $channelid;

    /**
     * @var \PrismProductsManager\Entity\Products
     *
     * @ORM\ManyToOne(targetEntity="PrismProductsManager\Entity\Products")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="productId", referencedColumnName="productId")
     * })
     */
    private $productid;



    /**
     * Get productchannelid
     *
     * @return integer 
     */
    public function getProductchannelid()
    {
        return $this->productchannelid;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return ProductsRChannels
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductsRChannels
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductsRChannels
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set channelid
     *
     * @param \PrismProductsManager\Entity\Channels $channelid
     * @return ProductsRChannels
     */
    public function setChannelid(\PrismProductsManager\Entity\Channels $channelid = null)
    {
        $this->channelid = $channelid;

        return $this;
    }

    /**
     * Get channelid
     *
     * @return \PrismProductsManager\Entity\Channels 
     */
    public function getChannelid()
    {
        return $this->channelid;
    }

    /**
     * Set productid
     *
     * @param \PrismProductsManager\Entity\Products $productid
     * @return ProductsRChannels
     */
    public function setProductid(\PrismProductsManager\Entity\Products $productid = null)
    {
        $this->productid = $productid;

        return $this;
    }

    /**
     * Get productid
     *
     * @return \PrismProductsManager\Entity\Products 
     */
    public function getProductid()
    {
        return $this->productid;
    }
}
