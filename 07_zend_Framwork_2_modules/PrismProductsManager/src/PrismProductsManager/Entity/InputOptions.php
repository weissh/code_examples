<?php

namespace PrismProductsManager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InputOptions
 *
 * @ORM\Table(name="INPUT_Options")
 * @ORM\Entity
 */
class InputOptions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="inputOptionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $inputoptionid;

    /**
     * @var string
     *
     * @ORM\Column(name="inputFieldName", type="string", length=50, nullable=false)
     */
    private $inputfieldname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedAt", type="datetime", nullable=false)
     */
    private $modifiedat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private $createdat;

    /**
     * Get inputoptionid
     *
     * @return integer 
     */
    public function getInputoptionid()
    {
        return $this->inputoptionid;
    }

    /**
     * Set inputfieldname
     *
     * @param string $inputfieldname
     * @return InputOptions
     */
    public function setInputfieldname($inputfieldname)
    {
        $this->inputfieldname = $inputfieldname;

        return $this;
    }

    /**
     * Get inputfieldname
     *
     * @return string 
     */
    public function getInputfieldname()
    {
        return $this->inputfieldname;
    }

    /**
     * Set modifiedat
     *
     * @param \DateTime $modifiedat
     * @return InputOptions
     */
    public function setModifiedat($modifiedat)
    {
        $this->modifiedat = $modifiedat;

        return $this;
    }

    /**
     * Get modifiedat
     *
     * @return \DateTime 
     */
    public function getModifiedat()
    {
        return $this->modifiedat;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return InputOptions
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }
}
