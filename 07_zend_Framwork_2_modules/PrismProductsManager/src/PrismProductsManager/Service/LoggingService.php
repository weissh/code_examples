<?php

namespace PrismProductsManager\Service;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use PrismProductsManager\Entity\LoggedUpdate;


/**
 * Class LoggingService
 * @package PrismProductsManager\Service
 */
class LoggingService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EntityRepository
     */
    private $loggedUpdateRepository;

    /**
     * LoggingService constructor.
     * @param EntityManager $entityManager
     * @param EntityRepository $loggedUpdateRepository
     */
    public function __construct(
        EntityManager $entityManager,
        EntityRepository $loggedUpdateRepository
    ) {
        $this->entityManager = $entityManager;
        $this->loggedUpdateRepository = $loggedUpdateRepository;
    }

    /**
     * @param $style
     * @param $option
     * @param $updateKey
     * @param $updateValue
     * @param LoggedUpdate|null $loggedUpdateEntity
     * @return bool
     */
    public function logUpdate($style, $option, $updateKey, $updateValue, LoggedUpdate $loggedUpdateEntity = null)
    {
        $sessionJson = isset($_SESSION['prism_cms']['storage']) ? $_SESSION['prism_cms']['storage'] : null;
        $userId = null;
        if (!is_null($sessionJson)) {
            $sessionUser = json_decode($sessionJson, true);
            $userId = isset($sessionUser['userId']) ? $sessionUser['userId'] : null;
        }

        if (is_null($loggedUpdateEntity)) {
            /** Create entity based on request data */
            $loggedUpdateEntity = new LoggedUpdate();
            $loggedUpdateEntity->setUserId($userId);
            $loggedUpdateEntity->setStyle($style);
            $loggedUpdateEntity->setOption($option);
            $loggedUpdateEntity->setUpdateKey($updateKey);
            $loggedUpdateEntity->setUpdateValue($updateValue);
            $loggedUpdateEntity->setBacktrace('');
            $loggedUpdateEntity->setCreated(new \DateTime());
            $loggedUpdateEntity->setModified(new \DateTime());
        }

        try {
            $this->entityManager->persist($loggedUpdateEntity);
            $this->entityManager->flush($loggedUpdateEntity);
            $this->entityManager->clear($loggedUpdateEntity);
        } catch (\Exception $ex) {
            //TODO: Log error
            return false;
        }

        return true;
    }
}
