<?php
namespace PrismProductsManager\Service;

use Doctrine\ORM\EntityManager;
use PrismProductsManager\Entity\InputFields;
use PrismProductsManager\Entity\ScheduleAttributes;
use PrismProductsManager\Entity\SchedulePrices;
use PrismProductsManager\Entity\Schedules;
use PrismProductsManager\Entity\ScheduleTrail;
use PrismProductsManager\Entity\ScheduleTypes;
use PrismProductsManager\Model\AttributesGroupMapper;
use PrismProductsManager\Model\ProductsMapper;
use \Doctrine\Common\Persistence\ObjectRepository as ObjectRepository;
use PrismQueueManager\Service\QueueService;
use Zend\Db\Adapter\Adapter;

/**
 * Class ScheduleService
 *
 * @package PrismProductsManager\Service
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class ScheduleService
{

    /**
     *
     * @var EntityManager
     */
    protected $doctrineEntity;

    /**
     *
     * @var ProductsMapper
     */
    protected $productsMapper;

    /**
     *
     * @var ObjectRepository
     */
    protected $schedulesRepository;

    /**
     *
     * @var ObjectRepository
     */
    protected $schedulePricesRepository;

    /**
     *
     * @var ObjectRepository
     */
    protected $scheduleTrailRepository;

    /**
     *
     * @var ObjectRepository
     */
    protected $scheduleTypesRepository;

    /**
     *
     * @var Schedules
     */
    protected $schedulesEntity;

    /**
     *
     * @var SchedulePrices
     */
    protected $schedulePricesEntity;

    /**
     *
     * @var ScheduleTrail
     */
    protected $scheduleTrailEntity;

    /**
     *
     * @var ScheduleTypes
     */
    protected $scheduleTypesEntity;

    /**
     *
     * @var ScheduleAttributes
     */
    protected $scheduleAttributesEntity;

    /**
     *
     * @var ObjectRepository
     */
    protected $currencyRepository;

    /**
     *
     * @var ObjectRepository
     */
    protected $territoriesRepository;

    /**
     *
     * @var InputService
     */
    protected $inputService;

    /**
     *
     * @var AttributesGroupMapper
     */
    protected $attributesMapper;

    /**
     *
     * @var CommonAttributesService
     */
    protected $commonAttributesService;

    /**
     *
     * @var
     *
     */
    protected $uniqueIdentifier;

    /**
     *
     * @var QueueService
     */
    protected $queueService;

    /**
     *
     * @var Adapter
     */
    protected $dbAdapter;

    /**
     *
     * @param EntityManager $doctrineEntity
     * @param ProductsMapper $productsMapper
     * @param ObjectRepository $schedulesRepository
     * @param ObjectRepository $schedulePricesRepository
     * @param ObjectRepository $scheduleTrailRepository
     * @param ObjectRepository $scheduleTypesRepository
     * @param ObjectRepository $territoriesRepository
     * @param ObjectRepository $currencyRepository
     * @param Schedules $schedulesEntity
     * @param SchedulePrices $schedulePricesEntity
     * @param ScheduleTrail $scheduleTrailEntity
     * @param ScheduleTypes $scheduleTypesEntity
     * @param ScheduleAttributes $scheduleAttributesEntity
     * @param InputService $inputService
     * @param CommonAttributesService $commonAttributesService
     * @param QueueService $queueService
     * @param Adapter $dbAdapter
     */
    public function __construct(EntityManager $doctrineEntity, ProductsMapper $productsMapper, AttributesGroupMapper $attributesMapper, ObjectRepository $schedulesRepository, ObjectRepository $schedulePricesRepository, ObjectRepository $scheduleTrailRepository, ObjectRepository $scheduleTypesRepository, ObjectRepository $territoriesRepository, ObjectRepository $currencyRepository, Schedules $schedulesEntity, SchedulePrices $schedulePricesEntity, ScheduleTrail $scheduleTrailEntity, ScheduleTypes $scheduleTypesEntity, ScheduleAttributes $scheduleAttributesEntity, InputService $inputService, CommonAttributesService $commonAttributesService, QueueService $queueService, Adapter $dbAdapter)
    {
        /**
         * Doctrine entity manager
         */
        $this->doctrineEntity = $doctrineEntity;
        /**
         * Mappers
         */
        $this->productsMapper = $productsMapper;
        $this->attributesMapper = $attributesMapper;
        /**
         * Repositories
         */
        $this->schedulesRepository = $schedulesRepository;
        $this->schedulePricesRepository = $schedulePricesRepository;
        $this->scheduleTrailRepository = $scheduleTrailRepository;
        $this->scheduleTypesRepository = $scheduleTypesRepository;
        $this->currencyRepository = $currencyRepository;
        $this->territoriesRepository = $territoriesRepository;
        /**
         * Entities
         */
        $this->schedulesEntity = $schedulesEntity;
        $this->schedulePricesEntity = $schedulePricesEntity;
        $this->scheduleTrailEntity = $scheduleTrailEntity;
        $this->scheduleTypesEntity = $scheduleTypesEntity;
        $this->scheduleAttributesEntity = $scheduleAttributesEntity;
        /**
         * Services
         */
        $this->inputService = $inputService;
        $this->commonAttributesService = $commonAttributesService;
        $this->queueService = $queueService;
        /**
         * DB Adapter
         */
        $this->dbAdapter = $dbAdapter;
    }

    /**
     *
     * @param
     *            $params
     * @return array
     */
    public function getAllScheduledPriceEntriesForField($params)
    {

        /**
         * Split the params into variables
         */
        $priceTable = isset($params['priceTable']) ? $params['priceTable'] : false;
        $priceColumn = isset($params['priceColumn']) ? $params['priceColumn'] : false;
        $productIdFrom = isset($params['productIdFrom']) ? $params['productIdFrom'] : false;
        $code = isset($params['code']) ? $params['code'] : false;
        $territory = ! empty($params['territory']) ? $params['territory'] : null;
        $currency = ! empty($params['currency']) ? $params['currency'] : null;

        /**
         * Get product id or variant Id
         */
        switch ($productIdFrom) {
            case 'PRODUCTS':
                $productOrVariantId = $this->productsMapper->getProductIdFromStyle($code);
                break;
            case 'PRODUCTS_R_ATTRIBUTES':
                $productOrVariantId = $this->productsMapper->getFirstVariantIdForOption($code);
                break;
            default:
                $productOrVariantId = null;
                break;
        }
        $this->scheduleTypesEntity = $this->scheduleTypesRepository->findOneBy(array(
            'scheduleType' => 'PRICES'
        ));
        /**
         * Retrieve all schedule events attached to this product/variant
         */
        $scheduleEvents = $this->getAllScheduledEventsFor($productOrVariantId, $productIdFrom);
        $fieldEvents = array();
        switch ($priceTable) {
            case 'PRODUCTS_Prices':
                foreach ($scheduleEvents as $scheduleEvent) {
                    /**
                     * Check if the product/variant schedule events matches to current field
                     */
                    if (! is_null($scheduleEvent->getSchedulePrices()) && $scheduleEvent->getSchedulePrices()->getPriceTable() == $priceTable && $scheduleEvent->getSchedulePrices()->getPriceColumn() == $priceColumn && ($scheduleEvent->getSchedulePrices()->getTerritoryId() === NULL || (! is_null($scheduleEvent->getSchedulePrices()->getTerritoryId()) && $scheduleEvent->getSchedulePrices()
                        ->getTerritoryId()
                        ->getIso2code() == $territory)) && ($scheduleEvent->getSchedulePrices()->getCurrencyId() === NULL || (! is_null($scheduleEvent->getSchedulePrices()->getCurrencyId()) && $scheduleEvent->getSchedulePrices()
                        ->getCurrencyId()
                        ->getIsoCode() == $currency))) {
                        $fieldEvents[] = array(
                            'event' => $scheduleEvent
                        );
                    }
                }
                break;
            case 'PRODUCTS_Supplier_Price':
                foreach ($scheduleEvents as $scheduleEvent) {
                    /**
                     * Check if the product/variant schedule events matches to current field
                     */
                    if (! is_null($scheduleEvent->getSchedulePrices()) && $scheduleEvent->getSchedulePrices()->getPriceTable() == $priceTable && $scheduleEvent->getSchedulePrices()->getPriceColumn() == $priceColumn) {

                        $fieldEvents[] = array(
                            'event' => $scheduleEvent
                        );
                    }
                }
                break;
        }

        return $fieldEvents;
    }

    /**
     *
     * @param
     *            $productOrVariantId
     * @param
     *            $productIdFrom
     * @param
     *            $attributeId
     * @param
     *            $scheduleTypeId
     * @param
     *            $marketId
     * @return array
     */
    public function getAllScheduledEventsForProductAndAttribute($productOrVariantId, $productIdFrom, $attributeId, $scheduleTypeId, $marketId)
    {
        $this->scheduleTypesEntity = $this->scheduleTypesRepository->find($scheduleTypeId);

        $scheduleEvents = $this->getAllScheduledEventsFor($productOrVariantId, $productIdFrom);
        $returnArray = array();

        if (! empty($scheduleEvents) && ! is_null($scheduleEvents)) {
            foreach ($scheduleEvents as $scheduleEvent) {
                if ($scheduleEvent->getScheduleAttributes()->getAttributeId() == $attributeId) {

                    $value = $this->commonAttributesService->getCommonAttributeValueById($scheduleEvent->getScheduleAttributes()
                        ->getattributeValue());
                    $codeValue = $value->getCommonAttributeValueAbbrev();
                    if (empty($codeValue)) {
                        $codeValue = $value->getcommonAttributeValuesDefinitions()->getvalue();
                    }
                    $returnArray[$scheduleEvent->getScheduleId()] = array(
                        'MARKET_ID' => strtolower($marketId),
                        'START_DATE' => $scheduleEvent->getscheduleStartDate()->format('Y-m-d H:i:s'),
                        'CODE' => $codeValue
                    );
                }
            }
        }

        return $returnArray;
    }

    /**
     *
     * @param
     *            $params
     * @return array
     */
    public function getAllScheduledAttributesEntriesForField($params)
    {
        /**
         * Split the params into variables
         */
        $productIdFrom = isset($params['productIdFrom']) ? $params['productIdFrom'] : false;
        $code = isset($params['code']) ? $params['code'] : false;
        $inputFieldId = isset($params['inputFieldId']) ? $params['inputFieldId'] : false;

        /**
         * Get product id or variant Id
         */
        switch ($productIdFrom) {
            case 'PRODUCTS':
                $productOrVariantId = $this->productsMapper->getProductIdFromStyle($code);
                break;
            case 'PRODUCTS_R_ATTRIBUTES':
                $productOrVariantId = $this->productsMapper->getFirstVariantIdForOption($code);
                break;
            default:
                $productOrVariantId = null;
                break;
        }

        $inputFieldEntity = $this->inputService->getInputField($inputFieldId);
        $attributeTable = $inputFieldEntity->getInputFieldRAttributesInputFieldRAttributeId()->getAttributeTable();

        $fieldEvents = array();
        $attributeId = false;
        $attributeValues = array();
        switch ($attributeTable) {
            case 'COMMON_ATTRIBUTES':
                $this->scheduleTypesEntity = $this->scheduleTypesRepository->findOneBy(array(
                    'scheduleType' => 'COMMON_ATTRIBUTES'
                ));
                $attributeId = $inputFieldEntity->getInputFieldRAttributesInputFieldRAttributeId()->getCommonAttributeId();
                $attributeValuesEntity = $inputFieldEntity->getInputFieldRAttributesInputFieldRAttributeId()
                    ->getCommonAttributes()
                    ->getCommonAttributesValues();
                $attributeValues = array();
                foreach ($attributeValuesEntity as $attributeValue) {
                    $attributeValues[$attributeValue->getCommonAttributeValueId()] = $attributeValue->getCommonAttributeValuesDefinitions()->getValue();
                }
                break;
            case 'ATTRIBUTES_Groups':
                $this->scheduleTypesEntity = $this->scheduleTypesRepository->findOneBy(array(
                    'scheduleType' => 'ATTRIBUTES_GROUPS'
                ));
                $attributeId = $inputFieldEntity->getInputFieldRAttributesInputFieldRAttributeId()
                    ->getAttributeGroupId()
                    ->getAttributeGroupId();
                $attributeValues = $this->attributesMapper->getAttributeValues($attributeId);
                break;
        }

        /**
         * Retrieve all schedule events attached to this product/variant
         */
        $scheduleEvents = $this->getAllScheduledEventsFor($productOrVariantId, $productIdFrom);

        foreach ($scheduleEvents as $scheduleEvent) {
            if ($scheduleEvent->getscheduleAttributes()->getattributeId() == $attributeId) {
                $fieldEvents[] = array(
                    'event' => $scheduleEvent,
                    'inputField' => $inputFieldEntity,
                    'attributeValues' => $attributeValues
                );
            }
        }
        return $fieldEvents;
    }

    /**
     *
     * @param
     *            $productOrVariantId
     * @param string $productIdFrom
     * @return array|bool
     */
    private function getAllScheduledEventsFor($productOrVariantId, $productIdFrom = 'PRODUCTS')
    {
        try {
            return $this->schedulesRepository->findBy(array(
                'productOrVariantId' => $productOrVariantId,
                'productIdFrom' => $productIdFrom,
                'scheduleTypeId' => $this->scheduleTypesEntity
            ));
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     *
     * @param
     *            $params
     * @return array|mixed
     */
    public function getAllPossiblePriceValuesToSchedule($params)
    {
        /**
         * Split the params into variables
         */
        $priceTable = isset($params['priceTable']) ? $params['priceTable'] : false;
        $priceColumn = isset($params['priceColumn']) ? $params['priceColumn'] : false;
        $possibleValues = array();
        switch ($priceTable) {
            case 'PRODUCTS_Supplier_Price':
                if ($priceColumn == 'currencyCode') {
                    $possibleValues = $this->productsMapper->getSuppliersCurrency(true);
                }
                break;
            case 'PRODUCTS_Prices':
                break;
        }
        return $possibleValues;
    }

    /**
     *
     * @param
     *            $params
     * @return array
     */
    public function getAllPossibleAttributesValuesToSchedule($params)
    {
        $possibleValues = array();
        $inputFieldEntity = $this->inputService->getInputField($params['inputFieldId']);
        $attributeTable = $inputFieldEntity->getinputFieldRAttributesInputfieldRAttributeid()->getAttributeTable();
        switch ($attributeTable) {
            case 'ATTRIBUTES_Groups':
                $attributeId = $inputFieldEntity->getinputFieldRAttributesInputfieldRAttributeid()
                    ->getAttributeGroupId()
                    ->getAttributeGroupId();
                $viewType = $inputFieldEntity->getinputFieldRAttributesInputfieldRAttributeid()
                    ->getAttributeGroupId()
                    ->getViewAs();
                switch ($viewType) {
                    case 'SELECT':
                        $possibleValues = $this->attributesMapper->getAttributeValues($attributeId);
                        break;
                    case 'CHECKBOX':
                        $possibleValues[0] = 'Not Checked';
                        $possibleValues[1] = 'Checked';
                }
                break;
            case 'COMMON_ATTRIBUTES':
                $viewAs = $inputFieldEntity->getInputFieldRAttributesInputFieldRAttributeId()
                    ->getCommonAttributes()
                    ->getcommonAttributeGuiRepresentation()
                    ->getcommonAttributesViewTypeId()
                    ->getcommonAttributeViewType();
                switch ($viewAs) {
                    case 'SELECT':
                    case 'MULTISELECT':
                        $commonAttributesValuesEntity = $inputFieldEntity->getInputFieldRAttributesInputFieldRAttributeId()
                            ->getCommonAttributes()
                            ->getCommonAttributesValues();
                        foreach ($commonAttributesValuesEntity as $attributeValue) {
                            $possibleValues[$attributeValue->getCommonAttributeValueId()] = $attributeValue->getCommonAttributeValuesDefinitions()->getValue();
                        }
                        break;
                    case 'CHECKBOX':
                        $possibleValues[0] = 'Not Checked';
                        $possibleValues[1] = 'Checked';
                }
                break;
                break;
        }
        return $possibleValues;
    }

    /**
     *
     * @param
     *            $scheduledDate
     * @param
     *            $scheduledSelectValue
     * @param
     *            $scheduledValue
     * @param
     *            $scheduledParams
     * @return bool
     */
    public function savePriceScheduleEntry($scheduledDate, $scheduledSelectValue, $scheduledValue, $scheduledParams)
    {
        try {
            $productIdOrVariantIds = null;

            switch ($scheduledParams->productIdFrom) {
                case 'PRODUCTS_R_ATTRIBUTES':
                    $productIdOrVariantIds = $this->productsMapper->getVariantsIdForOption($scheduledParams->code);
                    break;

                case 'PRODUCTS':
                    $productIdOrVariantIds = $this->productsMapper->getProductIdFromStyle($scheduledParams->code);
                    break;
            }

            $this->scheduleTypesEntity = $this->scheduleTypesRepository->findOneBy(array(
                'scheduleType' => 'PRICES'
            ));
            /** @var \PrismProductsManager\Entity\Currency $currencyEntity */
            $currencyEntity = $this->currencyRepository->findOneBy(array(
                'isocode' => $scheduledParams->currency
            ));
            /** @var \PrismProductsManager\Entity\Territories $territoryEntity */
            $territoryEntity = $this->territoriesRepository->findOneBy(array(
                'iso2code' => $scheduledParams->territory
            ));

            if (count($productIdOrVariantIds) > 1) {
                foreach ($productIdOrVariantIds as $variantId) {
                    $this->savePriceScheduleEntryForProductOrVariant($variantId['productAttributeId'], $currencyEntity, $territoryEntity, $scheduledParams, $scheduledSelectValue, $scheduledValue, $scheduledDate);
                }
            } else {
                $this->savePriceScheduleEntryForProductOrVariant($productIdOrVariantIds, $currencyEntity, $territoryEntity, $scheduledParams, $scheduledSelectValue, $scheduledValue, $scheduledDate);
            }
            $this->doctrineEntity->flush();
        } catch (\Exception $ex) {
            return false;
        }

        return true;
    }

    public function insertNewPriceSchedule($productOrVariantId, $productOrVariantIdFrom = 'PRODUCTS_R_ATTRIBUTES', $currencyId, $territoryId, $pricesTable = 'PRODUCTS_Prices', $priceColumn, $priceValue, $startDate)
    {
        try {
            $this->scheduleTypesEntity = $this->scheduleTypesRepository->findOneBy(array(
                'scheduleType' => 'PRICES'
            ));
            /** @var \PrismProductsManager\Entity\Currency $currencyEntity */
            $currencyEntity = $this->currencyRepository->find($currencyId);
            /** @var \PrismProductsManager\Entity\Territories $territoryEntity */
            $territoryEntity = $this->territoriesRepository->find($territoryId);

            /**
             * Refresh entity data
             */
            $this->schedulePricesEntity = new SchedulePrices();
            $this->schedulesEntity = new Schedules();

            /**
             * Hydrate $this->schedulePricesEntity
             */
            $this->schedulePricesEntity->setCreatedAt(new \DateTime('now'));
            $this->schedulePricesEntity->setCurrencyId($currencyEntity);
            $this->schedulePricesEntity->setTerritoryId($territoryEntity);
            $this->schedulePricesEntity->setPriceColumn($priceColumn);
            $this->schedulePricesEntity->setPriceTable($pricesTable);
            $this->schedulePricesEntity->setScheduleId($this->schedulesEntity);
            $this->schedulePricesEntity->setValue($priceValue);

            /**
             * Hydrate $this->schedulesEntity
             */
            $this->schedulesEntity->setScheduleTypeId($this->scheduleTypesEntity);
            $this->schedulesEntity->setCreatedAt(new \DateTime('now'));
            $this->schedulesEntity->setNumberOfTries(0);
            $this->schedulesEntity->setProcessing(null);
            $this->schedulesEntity->setScheduledBy(0);
            $this->schedulesEntity->setSchedulePrices($this->schedulePricesEntity);
            $this->schedulesEntity->setProductIdFrom($productOrVariantIdFrom);
            $this->schedulesEntity->setproductOrVariantId($productOrVariantId);
            $this->schedulesEntity->setScheduleStartDate($startDate);
            /**
             * Flush into db
             */
            $this->doctrineEntity->persist($this->schedulesEntity);
            $this->doctrineEntity->flush();
            return true;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     *
     * @param
     *            $scheduledDate
     * @param
     *            $scheduledSelectValue
     * @param
     *            $scheduledValue
     * @param
     *            $scheduledParams
     * @return bool
     */
    public function saveAttributesScheduleEntry($scheduledDate, $scheduledSelectValue, $scheduledValue, $scheduledParams)
    {
        try {
            $productIdOrVariantIds = null;

            switch ($scheduledParams->productIdFrom) {
                case 'PRODUCTS_R_ATTRIBUTES':
                    $productIdOrVariantIds = $this->productsMapper->getVariantsIdForOption($scheduledParams->code);
                    break;

                case 'PRODUCTS':
                    $productIdOrVariantIds = $this->productsMapper->getProductIdFromStyle($scheduledParams->code);
                    break;
            }
            $inputFieldEntity = $this->inputService->getInputField($scheduledParams->inputFieldId);

            $this->scheduleTypesEntity = $this->scheduleTypesRepository->findOneBy(array(
                'scheduleType' => $inputFieldEntity->getinputFieldRAttributesInputfieldrattributeid()
                    ->getAttributeTable()
            ));

            if (count($productIdOrVariantIds) > 1) {
                foreach ($productIdOrVariantIds as $variantId) {
                    $this->saveAttributeScheduleEntryForProductOrVariant($variantId['productAttributeId'], $scheduledParams, $scheduledSelectValue, $scheduledValue, $scheduledDate, $inputFieldEntity);
                }
            } else {
                $this->saveAttributeScheduleEntryForProductOrVariant($productIdOrVariantIds, $scheduledParams, $scheduledSelectValue, $scheduledValue, $scheduledDate, $inputFieldEntity);
            }
            $this->doctrineEntity->flush();
        } catch (\Exception $ex) {
            return false;
        }

        return true;
    }

    /**
     *
     * @param
     *            $productOrVariantId
     * @param
     *            $scheduledParams
     * @param
     *            $scheduledSelectValue
     * @param
     *            $scheduledValue
     * @param
     *            $scheduledDate
     * @param InputFields $inputFieldEntity
     * @return bool
     * @throws \Exception
     */
    private function saveAttributeScheduleEntryForProductOrVariant($productOrVariantId, $scheduledParams, $scheduledSelectValue, $scheduledValue, $scheduledDate, InputFields $inputFieldEntity)
    {
        try {
            /**
             * Refresh entity data
             */
            $this->scheduleAttributesEntity = new ScheduleAttributes();
            $this->schedulesEntity = new Schedules();

            $value = ! empty($scheduledSelectValue) ? $scheduledSelectValue : $scheduledValue;
            $attributeTable = $inputFieldEntity->getinputFieldRAttributesInputfieldrattributeid()->getattributetable();
            $attributeId = false;
            switch ($attributeTable) {
                case 'ATTRIBUTES_Groups':
                    $attributeId = $inputFieldEntity->getinputFieldRAttributesInputfieldrattributeid()
                        ->getAttributeGroupId()
                        ->getAttributeGroupId();
                    break;
                case 'COMMON_ATTRIBUTES':
                    $attributeId = $inputFieldEntity->getinputFieldRAttributesInputfieldrattributeid()->getCommonAttributeId();
                    break;
            }

            /**
             * Hydrate $this->scheduleAttributesEntity
             */
            $this->scheduleAttributesEntity->setScheduleId($this->schedulesEntity);
            $this->scheduleAttributesEntity->setCreatedAt(new \DateTime('now'));
            $this->scheduleAttributesEntity->setAttributeId($attributeId);
            $this->scheduleAttributesEntity->setAttributeValue($value);

            /**
             * Hydrate $this->schedulesEntity
             */
            $this->schedulesEntity->setScheduleTypeId($this->scheduleTypesEntity);
            $this->schedulesEntity->setCreatedAt(new \DateTime('now'));
            $this->schedulesEntity->setNumberOfTries(0);
            $this->schedulesEntity->setProcessing(null);
            $this->schedulesEntity->setScheduledBy(0);
            $this->schedulesEntity->setScheduleAttributes($this->scheduleAttributesEntity);
            $this->schedulesEntity->setProductIdFrom($scheduledParams->productIdFrom);
            $this->schedulesEntity->setproductOrVariantId($productOrVariantId);
            $this->schedulesEntity->setScheduleStartDate(new \DateTime($scheduledDate));
            /**
             * Flush into db
             */
            $this->doctrineEntity->persist($this->schedulesEntity);

            return true;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     *
     * @param
     *            $productIdOrVariantIds
     * @param
     *            $currencyEntity
     * @param
     *            $territoryEntity
     * @param
     *            $scheduledParams
     * @param
     *            $scheduledSelectValue
     * @param
     *            $scheduledValue
     * @param
     *            $scheduledDate
     * @return bool
     * @throws \Exception
     */
    private function savePriceScheduleEntryForProductOrVariant($productIdOrVariantIds, $currencyEntity, $territoryEntity, $scheduledParams, $scheduledSelectValue, $scheduledValue, $scheduledDate)
    {
        try {
            /**
             * Refresh entity data
             */
            $this->schedulePricesEntity = new SchedulePrices();
            $this->schedulesEntity = new Schedules();

            /**
             * Hydrate $this->schedulePricesEntity
             */
            $value = ! empty($scheduledSelectValue) ? $scheduledSelectValue : $scheduledValue;
            $this->schedulePricesEntity->setCreatedAt(new \DateTime('now'));
            $this->schedulePricesEntity->setCurrencyId($currencyEntity);
            $this->schedulePricesEntity->setTerritoryId($territoryEntity);
            $this->schedulePricesEntity->setPriceColumn($scheduledParams->priceColumn);
            $this->schedulePricesEntity->setPriceTable($scheduledParams->priceTable);
            $this->schedulePricesEntity->setScheduleId($this->schedulesEntity);
            $this->schedulePricesEntity->setValue($value);

            /**
             * Hydrate $this->schedulesEntity
             */
            $this->schedulesEntity->setScheduleTypeId($this->scheduleTypesEntity);
            $this->schedulesEntity->setCreatedAt(new \DateTime('now'));
            $this->schedulesEntity->setNumberOfTries(0);
            $this->schedulesEntity->setProcessing(null);
            $this->schedulesEntity->setScheduledBy(0);
            $this->schedulesEntity->setSchedulePrices($this->schedulePricesEntity);
            $this->schedulesEntity->setProductIdFrom($scheduledParams->productIdFrom);
            $this->schedulesEntity->setproductOrVariantId($productIdOrVariantIds);
            $this->schedulesEntity->setScheduleStartDate(new \DateTime($scheduledDate));
            /**
             * Flush into db
             */
            $this->doctrineEntity->persist($this->schedulesEntity);
            return true;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     *
     * @param
     *            $scheduleEventId
     * @return bool
     */
    public function deleteScheduleEvent($scheduleEventId)
    {
        try {
            $this->schedulesEntity = $this->schedulesRepository->find($scheduleEventId);
            if (! is_null($this->schedulesEntity->getSchedulePrices())) {
                $this->doctrineEntity->remove($this->schedulesEntity->getSchedulePrices());
            }
            if (! is_null($this->schedulesEntity->getScheduleAttributes())) {
                $this->doctrineEntity->remove($this->schedulesEntity->getScheduleAttributes());
            }
            $this->doctrineEntity->remove($this->schedulesEntity);
            $this->doctrineEntity->flush();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     *
     * @param
     *            $params
     * @return array
     */
    public function getAllScheduledEventsForProduct($params)
    {
        try {
            $productIdOrVariantIds = null;
            switch ($params['productIdFrom']) {
                case 'PRODUCTS_R_ATTRIBUTES':
                    $productIdOrVariantIds = $this->productsMapper->getFirstVariantIdForOption($params['code']);
                    break;

                case 'PRODUCTS':
                    $productIdOrVariantIds = $this->productsMapper->getProductIdFromStyle($params['code']);
                    break;
            }

            $scheduleEvents = $this->schedulesRepository->findBy(array(
                'productOrVariantId' => $productIdOrVariantIds,
                'productIdFrom' => $params['productIdFrom']
            ));
            $returnEvents = array();
            foreach ($scheduleEvents as $event) {
                $value = null;
                $fieldName = null;
                $scheduleType = $event->getScheduleTypeId()->getScheduleType();
                switch ($scheduleType) {
                    case 'ATTRIBUTES_GROUPS':
                        $attributeId = $event->getscheduleAttributes()->getAttributeId();
                        $attributeValue = $event->getscheduleAttributes()->getAttributeValue();
                        $attribute = $this->attributesMapper->fetchAttributeGroupById($attributeId);
                        $fieldName = $attribute['name'];
                        switch ($attribute['viewAs']) {
                            case 'CHECKBOX':
                                $value = 'Unchecked';
                                if ($attributeValue == 1) {
                                    $value = 'Checked';
                                }
                                break;
                            case 'SELECT':
                                $attributeValue = $this->attributesMapper->getAttributeValue($attributeValue);
                                $value = $attributeValue['attributeValue'];
                                break;
                            case 'TEXT':
                                $value = $attributeValue;
                                break;
                        }
                        break;
                    case 'COMMON_ATTRIBUTES':
                        $attributeId = $event->getscheduleAttributes()->getAttributeId();
                        $attributeValue = $event->getscheduleAttributes()->getAttributeValue();
                        $commonAttribute = $this->commonAttributesService->getCommonAttribute($attributeId);
                        $fieldName = $commonAttribute->getcommonAttributeDefinitions()->getdescription();
                        $attributeType = $commonAttribute->getcommonAttributeGuiRepresentation()
                            ->getcommonattributesviewtypeid()
                            ->getcommonattributeviewtype();
                        switch ($attributeType) {
                            case 'SELECT':
                            case 'MULTISELECT':
                                $value = $this->commonAttributesService->getCommonAttributeValueById($attributeValue);
                                $value = $value->getcommonAttributeValuesDefinitions()->getvalue();
                                break;
                            case 'CHECKBOX':
                                $value = 'Unchecked';
                                if ($attributeValue == 1) {
                                    $value = 'Checked';
                                }
                                break;
                            case 'TEXT':
                            case 'TEXTAREA':
                                $value = $attributeValue;
                                break;
                        }
                        break;
                    case 'PRICES':
                        $priceTable = $event->getschedulePrices()->getpriceTable();
                        switch ($priceTable) {
                            case 'PRODUCTS_Supplier_Price':
                                $fieldName = 'Product Supplier Currency';
                                if ($event->getSchedulePrices()->getPriceColumn() == 'price') {
                                    $fieldName = 'Supplier Price';
                                }
                                break;
                            case 'PRODUCTS_Prices':
                                $priceColumn = $event->getschedulePrices()->getPriceColumn();
                                $code = '';

                                $currencyId = $event->getSchedulePrices()->getCurrencyId();
                                if (! empty($currencyId)) {
                                    $code .= $event->getSchedulePrices()
                                        ->getcurrencyId()
                                        ->getIsoCode();
                                }
                                $territoryId = $event->getSchedulePrices()->getTerritoryId();
                                if (! empty($territoryId)) {
                                    $code .= '-' . $event->getschedulePrices()
                                        ->getTerritoryId()
                                        ->getIso2code();
                                }

                                $fieldName = '';
                                switch ($priceColumn) {
                                    case 'wasPrice':
                                        $fieldName = 'Original Price';
                                        break;
                                    case 'nowPrice':
                                        $fieldName = 'Current Price';
                                        break;
                                    case 'costPrice':
                                        $fieldName = 'Landed Cost';
                                        break;
                                }
                                $fieldName .= ' (' . $code . ')';
                                break;
                        }

                        $value = $event->getSchedulePrices()->getValue();
                        break;
                }
                $returnEvents[] = array(
                    'event' => $event,
                    'value' => $value,
                    'fieldName' => $fieldName
                );
            }
            return $returnEvents;
        } catch (\Exception $ex) {
            return array();
        }
    }

    /**
     *
     * @param
     *            $productOrVariantId
     * @param
     *            $productIdFrom
     * @param
     *            $priceTable
     * @param
     *            $priceColumn
     * @return mixed
     */
    public function getAllScheduledPricesForProduct($productOrVariantId, $productIdFrom, $priceTable, $priceColumn)
    {
        return $this->schedulesRepository->getPriceSchedulesForProduct($productOrVariantId, $productIdFrom, $priceTable, $priceColumn);
    }

    /**
     *
     * @param
     *            $date
     * @param
     *            $productOrVariantId
     * @param
     *            $productIdFrom
     * @param
     *            $territoryId
     * @param
     *            $currencyId
     * @return mixed
     */
    public function getWasPriceValidAtDate($date, $productOrVariantId, $productIdFrom, $territoryId, $currencyId)
    {
        return $this->schedulesRepository->getPriceSchedulesForWasPriceUpToDate($date, $productOrVariantId, $productIdFrom, $territoryId, $currencyId);
    }

    /**
     *
     * @return bool
     */
    public function processAllScheduledEvents()
    {
        $this->uniqueIdentifier = uniqid(time() . '-');
        /**
         * Try and lock records in the table using the unique identifier
         */
        if ($this->schedulesRepository->lockScheduleEventsForProcessing($this->uniqueIdentifier)) {
            try {
                /**
                 * Get all locked events to process
                 */
                $scheduleRecords = $this->schedulesRepository->findBy(array(
                    'processing' => $this->uniqueIdentifier
                ));
                foreach ($scheduleRecords as $scheduleEvent) {
                    try {
                        /**
                         * Try processing event
                         */
                        $this->processScheduleEvent($scheduleEvent);
                        /**
                         * Remove Entries for current schedule
                         */
                        $this->removeScheduleEvent($scheduleEvent);
                        /**
                         * Add record to the Queue Manager
                         */
                        $this->insertRecordToQueueManager($scheduleEvent);
                        /**
                         * Log Schedule entry
                         */
                        $this->logToTrailScheduleEvent($scheduleEvent);
                    } catch (\Exception $ex) {
                        $this->unlockRecordAndMarkAsFailed($scheduleEvent);
                        var_dump($ex->getMessage());
                    }
                }
            } catch (\Exception $ex) {
                // var_dump($ex->getMessage());
                /**
                 * If error happens unlock records
                 */
                $this->schedulesRepository->unlockRecords($this->uniqueIdentifier);
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param
     *            $scheduleEvent
     * @throws \Exception
     */
    private function insertRecordToQueueManager($scheduleEvent)
    {
        try {
            switch ($scheduleEvent->getProductIdFrom()) {
                case 'PRODUCTS':
                    $productId = $scheduleEvent->getProductOrVariantId();
                    break;
                case 'PRODUCTS_R_ATTRIBUTES':
                    $productId = $this->productsMapper->getProductIdFromVariantId($scheduleEvent->getProductOrVariantId());
                    break;
            }
            if (! empty($productId)) {
                switch ($scheduleEvent->getProductIdFrom()) {
                    case 'PRODUCTS':
                        $this->queueService->addSipEntryToQueue($productId, 5, $this->productsMapper, true);
                        break;
                    case 'PRODUCTS_R_ATTRIBUTES':
                        $this->queueService->addSipEntryToQueue($productId . '_' . $scheduleEvent->getProductOrVariantId(), 5, $this->productsMapper, true);
                        $this->queueService->addSpectrumEntryToQueueForCommonAttributes($scheduleEvent->getProductOrVariantId());
                        break;
                }
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            var_dump($ex->getMessage());
        }
    }

    /**
     *
     * @param
     *            $scheduleEvent
     * @return bool
     */
    private function logToTrailScheduleEvent($scheduleEvent)
    {
        try {
            $this->scheduleTrailEntity = new ScheduleTrail();
            $this->scheduleTrailEntity->setScheduleTypeId($scheduleEvent->getScheduleTypeId());
            $this->scheduleTrailEntity->setScheduleStartDate($scheduleEvent->getScheduleStartDate());
            $this->scheduleTrailEntity->setScheduleUpdatedDate(new \DateTime('now'));
            $this->scheduleTrailEntity->setQueueManagerId(0);
            $this->scheduleTrailEntity->setProductOrVariantId($scheduleEvent->getProductOrVariantId());
            $this->scheduleTrailEntity->setProductIdFrom($scheduleEvent->getProductIdFrom());
            $this->scheduleTrailEntity->setScheduleBby($scheduleEvent->getScheduledBy());
            $this->doctrineEntity->persist($this->scheduleTrailEntity);
            $this->doctrineEntity->flush($this->scheduleTrailEntity);
            return true;
        } catch (\Exception $ex) {
            var_dump($ex->getMessage());
            return false;
        }
    }

    /**
     *
     * @param
     *            $scheduleEvent
     * @return bool
     */
    private function removeScheduleEvent($scheduleEvent)
    {
        try {
            if (! is_null($scheduleEvent->getSchedulePrices())) {
                $this->doctrineEntity->remove($scheduleEvent->getSchedulePrices());
            }
            if (! is_null($scheduleEvent->getScheduleAttributes())) {
                $this->doctrineEntity->remove($scheduleEvent->getScheduleAttributes());
            }
            $this->doctrineEntity->remove($scheduleEvent);
            $this->doctrineEntity->flush();
            return true;
        } catch (\Exception $ex) {
            var_dump($ex->getMessage());
            return false;
        }
    }

    /**
     *
     * @param
     *            $scheduleEvent
     * @throws \Exception
     */
    private function processScheduleEvent($scheduleEvent)
    {
        try {
            $productOrVariantId = $scheduleEvent->getProductOrVariantId();
            $productIdFrom = $scheduleEvent->getProductIdFrom();

            switch ($scheduleEvent->getscheduleTypeId()->getScheduleType()) {
                case 'PRICES':
                    $this->productsMapper->addPriceToProductOrVariant($scheduleEvent, $productOrVariantId, $productIdFrom);
                    break;
                case 'COMMON_ATTRIBUTES':
                    $commonAttributeId = $scheduleEvent->getScheduleAttributes()->getAttributeId();
                    $commonAttributeValue = $scheduleEvent->getScheduleAttributes()->getAttributeValue();
                    $this->productsMapper->addCommonAttributeToProductOrVariant($productOrVariantId, $productIdFrom, $commonAttributeId, $commonAttributeValue);
                    break;
                case 'ATTRIBUTES_GROUPS':
                    $attributeId = $scheduleEvent->getScheduleAttributes()->getAttributeId();
                    $attributeData = $this->attributesMapper->fetchAttributeGroupById($attributeId);
                    $attributeValue = $scheduleEvent->getScheduleAttributes()->getAttributeValue();
                    $this->productsMapper->addAttributeToProductOrVariant($productOrVariantId, $productIdFrom, $attributeData, $attributeValue);
                    break;
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     *
     * @param
     *            $scheduleEvent
     * @return bool
     */
    private function unlockRecordAndMarkAsFailed($scheduleEvent)
    {
        try {
            $scheduleEvent->setProcessing(null);
            $nrOfTries = $scheduleEvent->getNumberOfTries();
            $scheduleEvent->setNumberOfTries($nrOfTries + 1);

            $this->doctrineEntity->persist($scheduleEvent);
            $this->doctrineEntity->flush();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }
}