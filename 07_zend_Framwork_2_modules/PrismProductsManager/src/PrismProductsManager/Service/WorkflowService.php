<?php

namespace PrismProductsManager\Service;

use Doctrine\ORM\EntityManager;
use \Doctrine\Common\Persistence\ObjectRepository as ObjectRepository;
use PrismProductsManager\Entity\CommonAttributes;
use PrismProductsManager\Entity\CommonAttributesDefinitions;
use PrismProductsManager\Entity\CommonAttributesGuiRepresentation;
use PrismProductsManager\Entity\CommonAttributesValues;
use PrismProductsManager\Entity\CommonAttributesValuesChain;
use PrismProductsManager\Entity\CommonAttributesValuesDefinitions;
use PrismProductsManager\Entity\CommonAttributesValuesEquivalence;
use PrismProductsManager\Entity\CommonAttributesValuesGuiRepresentation;
use PrismProductsManager\Entity\CommonAttributesValuesOverrides;
use PrismProductsManager\Entity\Territories;
use PrismProductsManager\Model\CommonAttributesMapper;
use PrismProductsManager\Model\ProductsMapper;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

/**
 * Class WorkflowService
 * @package PrismProductsManager\Service
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class WorkflowService
{
    /**
     * @var ProductsMapper
     */
    protected $productsMapper;

    /**
     * @var AssignFieldToTabsService
     */
    protected $assignFieldsToTabsService;

    /**
     * @var InputService
     */
    protected $inputService;

    /**
     * @var EntityManager
     */
    protected $doctrineEntity;

    /**
     * @var Adapter
     */
    protected $dbAdapter;

    /**
     * @var Sql
     */
    protected $sql;

    /**
     * @var string
     */
    protected $productsRWorkflowsTable = 'PRODUCTS_R_WORKFLOWS';
    /**
     * @param ProductsMapper $productsMapper
     * @param AssignFieldToTabsService $assignFieldsToTabsService
     * @param InputService $inputService
     * @param EntityManager $doctrineEntity
     * @param Adapter $dbAdapter
     * @param Sql $sql
     */
    public function __construct(
        ProductsMapper $productsMapper,
        AssignFieldToTabsService $assignFieldsToTabsService,
        InputService $inputService,
        EntityManager $doctrineEntity,
        Adapter $dbAdapter,
        Sql $sql

    ) {
        $this->productsMapper = $productsMapper;
        $this->assignFieldsToTabsService = $assignFieldsToTabsService;
        $this->inputService = $inputService;
        $this->doctrineEntity = $doctrineEntity;
        $this->dbAdapter = $dbAdapter;
        $this->sql = $sql;

    }

    /**
     * @param $style
     * @param $option
     * @return array|bool
     */
    public function checkIfNextWorkFlowHaveBeenCompletedFor($style, $option)
    {
        /** Set up variables */
        if (!is_null($style) && is_null($option)) {
            $productOrVariantId = $this->productsMapper->getProductIdFromStyle($style);
            $productIdFrom = 'PRODUCTS';
        }
        /** Set up variables */
        if (!is_null($style) && !is_null($option)) {
            $productOrVariantId = $this->productsMapper->getFirstVariantIdForOption($option);
            $productIdFrom = 'PRODUCTS_R_ATTRIBUTES';
        }


        if (!empty($productOrVariantId) && !empty($productIdFrom)) {
            /** Get next workflow that needs to be validated */
            $nextWorkflowToValidate = $this->getNextWorkflowsToValidateFor($productOrVariantId, $productIdFrom);
            /** Get all input fields for the next workflow that needs to be validated */
            $inputFields = $this->inputService->getAllInputFieldsForWorkflow($nextWorkflowToValidate);

            foreach ($inputFields as $inputField) {
                /** Loop the input fields and retrieve the value that is used for the style/option */
                $this->doctrineEntity->refresh($inputField);
                switch ($inputField->getInputTypeId()->getInputFieldTypeName()) {
                    /** Input field is a table field type */
                    case 'isModule':
                        /** If the input field is a module customer logic needs to be applied for individual modules */
                        $moduleRef = $inputField->getInputModulesInputModuleId()->getInputModuleRef();

                        switch ($moduleRef) {
                            case 'prices-module':
                                /** If the input is a price module check if the product/variant has prices */
                                $productPrices = $this->productsMapper->getAllProductPrices($productOrVariantId, $productIdFrom);
                                $supplierPrices = $this->productsMapper->getSupplierPrices($productOrVariantId, $productIdFrom);
                                $pricesFlag = $this->checkIfPricesCompleteWorkflow(
                                    $productPrices, $nextWorkflowToValidate, $supplierPrices
                                );
                                /** Prices are not set up for this status */
                                if (!$pricesFlag) {
                                    return false;
                                }
                                break;
                        }

                        break;
                    /** Input field is a table field type */
                    case 'isTableField':
                        /** Style is saved against the product entry not variant - no need to validate  */
                        $column = $inputField->getinputTablefieldsInputtablefieldid()->getColumn();
                        $table = $inputField->getinputTablefieldsInputtablefieldid()->getTable();
                        if ($table == 'PRODUCTS_Definitions') {
                            $this->sql->setTable($table);
                            $select = $this->sql->select()
                                ->columns(array($column))
                                ->where(array('productId' => $productOrVariantId, 'productIdFrom' => $productIdFrom));
                            $select->limit(1);
                            $statement = $this->sql->prepareStatementForSqlObject($select);
                            $result = $statement->execute()->next();

                            /** Check if value has been set against this attribute */
                            if (!isset($result[$column]) || strlen(str_replace(' ', '', $result[$column])) == 0) {
                                return false;
                            }
                        }
                        break;
                    /** Input field is a attribute type */
                    case 'isAttribute':
                        switch ($inputField->getInputFieldRAttributesInputFieldRAttributeId()->getAttributeTable()) {
                            case 'ATTRIBUTES_Groups':
                                /** Get attribute group Id and attribute view type */
                                $attributeGroupId = $inputField->getInputFieldRAttributesInputFieldRAttributeId()
                                                        ->getAttributeGroupId()
                                                        ->getAttributeGroupid();
                                $attributeType = $inputField->getInputFieldRAttributesInputFieldRAttributeId()
                                                    ->getAttributeGroupId()
                                                    ->getViewAs();
                                /** If it is select check if attribute has a selected value for product/variant */
                                $attributeUsed = $this->productsMapper->checkIfAttributeIsUsed(
                                    $productOrVariantId, $productIdFrom, $attributeGroupId, $attributeType
                                );
                                /** If the attribute doesn't have values then it means that the work flow has not been completed - return */
                                if (!$attributeUsed) {
                                    return false;
                                }
                                break;
                            case 'COMMON_ATTRIBUTES':
                                $visibilityBreakFlag = false;
                                foreach ($inputField->getinputFieldsVisibility() as $inputFieldVisibility) {
                                    $commonAttributeId = $inputFieldVisibility->getCommonAttribute()->getCommonAttributeId();
                                    $savedValueForProduct = $this->productsMapper->getCommonAttributesUsedForProduct($productOrVariantId, $productIdFrom, $commonAttributeId);
                                    if (!empty($savedValueForProduct['commonAttributeValue']) && $savedValueForProduct['commonAttributeValue'] == $inputFieldVisibility->getCommonAttributeValueId()) {
                                        $visibilityBreakFlag = true;
                                        break;
                                    }
                                }
                                if ($visibilityBreakFlag) {
                                    break;
                                }

                                $commonAttributeId = $inputField->getInputFieldRAttributesInputFieldRAttributeId()->getCommonAttributeId();
                                /** Check if common attribute has values */
                                $commonAttributeUsed = $this->productsMapper->checkIfCommonAttributeIsUsed(
                                    $productOrVariantId, $productIdFrom, $commonAttributeId
                                );
                                /** If the common attribute doesn't have values then it means that the work flow has not been completed - return */
                                if (!$commonAttributeUsed) {
                                    return false;
                                }
                                break;
                        }

                        break;
                }
            }
            return $nextWorkflowToValidate;
        }
        return false;
    }

    /**
     * @return array|bool
     */
    public function getAllWorkflows()
    {
        return $this->assignFieldsToTabsService->getAllWorkflows();
    }
    /**
     * @param $style
     * @param $option
     * @param $workflow
     * @return bool
     */
    public function saveWorkFlowToProduct($style, $option, $workflow)
    {
        try {
            /** Set up variables */
            if (!is_null($style) && is_null($option)) {
                $productOrVariantId = $this->productsMapper->getProductIdFromStyle($style);
                $productIdFrom = 'PRODUCTS';

                if (!empty($productOrVariantId) && !empty($productIdFrom)) {
                    /** Delete all work flows that were associated  */
                    $this->sql->setTable($this->productsRWorkflowsTable);
                    $action = $this->sql->delete()
                        ->where(array(
                            'productIdFrom' => $productIdFrom,
                            'productOrVariantId' => $productOrVariantId,
                        ));
                    $this->sql->prepareStatementForSqlObject($action)->execute();

                    $this->sql->setTable($this->productsRWorkflowsTable);
                    $insert = $this->sql->insert();
                    $insert->values(array(
                        'productIdFrom' => $productIdFrom,
                        'productOrVariantId' => $productOrVariantId,
                        'workflowId' => $workflow->getWorkflowId(),
                        'modifiedAt' => date('Y-m-d H:i:s'),
                        'createdAt' => date('Y-m-d H:i:s')
                    ));
                    $statement = $this->sql->prepareStatementForSqlObject($insert);

                    if ($statement->execute()) {
                        return true;
                    } else {
                        return false;
                    }
                }

            }
            /** Set up variables */
            if (!is_null($style) && !is_null($option)) {
                $variants = $this->productsMapper->getVariantsIdForOption($option);
                foreach ($variants as $variant) {
                    if (!empty($variant['productAttributeId'])) {
                        /** Delete all work flows that were associated  */
                        $this->sql->setTable($this->productsRWorkflowsTable);
                        $action = $this->sql->delete()
                            ->where(array(
                                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                                'productOrVariantId' => $variant['productAttributeId'],
                            ));
                        $this->sql->prepareStatementForSqlObject($action)->execute();

                        $this->sql->setTable($this->productsRWorkflowsTable);
                        $insert = $this->sql->insert();
                        $insert->values(array(
                            'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                            'productOrVariantId' => $variant['productAttributeId'],
                            'workflowId' => $workflow->getWorkflowId(),
                            'modifiedAt' => date('Y-m-d H:i:s'),
                            'createdAt' => date('Y-m-d H:i:s')
                        ));
                        $statement = $this->sql->prepareStatementForSqlObject($insert);
                        $statement->execute();
                    }
                }
                return true;
            }
            return false;
        } catch (\Exception $ex) {
            return false;
        }

    }
    /**
     * @param $productPrices
     * @param $nextWorkflowToValidate
     * @param $supplierPrices
     * @return bool
     */
    public function checkIfPricesCompleteWorkflow($productPrices, $nextWorkflowToValidate, $supplierPrices)
    {

        switch ($nextWorkflowToValidate->getWorkflowDescription()) {
            case 'Ready for PO':
                /** For ready for PO flag we need to validate supplier price and currency and cost price */
                if (empty($supplierPrices['currencyCode']) || empty($supplierPrices['price'])) {
                    /** Workflow is not completed because of supplier price */
                    return false;
                }
                if (!empty($productPrices)) {
                    foreach ($productPrices as $productPrice) {
                        /** Workflow is not completed because cost price is not set up */
                        if (empty($productPrice['costPrice'])) {
                            return false;
                        }
                    }
                } else {
                    /** Workflow is not completed because prices are not set up */
                    return false;
                }
                break;
            case 'Ready to Shoot':
            case 'Ready for Web Upload':
                if (!empty($productPrices)) {
                    foreach ($productPrices as $productPrice) {
                        /** Workflow is not completed because cost price is not set up */
                        if (empty($productPrice['nowPrice']) || empty($productPrice['wasPrice'])) {
                            return false;
                        }
                    }
                } else {
                    /** Workflow is not completed because prices are not set up */
                    return false;
                }
                break;

        }

        return true;
    }

    /**
     * @param $productOrVariantId
     * @param string $productIdFrom
     * @param boolean $forceLastWorkflow
     * @return array
     */
    public function getNextWorkflowsToValidateFor($productOrVariantId, $productIdFrom = 'PRODUCTS', $forceLastWorkflow = false)
    {
        /** Get used workflows and all workflows */
        $usedWorkflows = $this->productsMapper->getUsedWorkflowsFor($productOrVariantId, $productIdFrom);
        $tempusedWorkFlows = $usedWorkflows;
        if (!empty($usedWorkflows)) {
            $usedWorkflows = $usedWorkflows[key($usedWorkflows)];
        } else {
            $usedWorkflows = 0;
        }
        $allWorkflows = $this->assignFieldsToTabsService->getAllWorkflows();
        $nextWorkflowToValidate = array();
        /**
         * Looping all work flows and skipping the ones that are already marked as completed for this style/option
         * The moment we find one we break the loop
         */
        foreach ($allWorkflows as $workflow) {
            if ($workflow->getWorkflowId() <= $usedWorkflows) {
                continue;
            }

            $nextWorkflowToValidate[] = $workflow;
            /** We are breaking it in order to retrieve only one workflow that needs to be validated  */
            break;
        }
        if (!empty($nextWorkflowToValidate[0])) {
            return $nextWorkflowToValidate[0];
        }
        if ($forceLastWorkflow && empty($nextWorkflowToValidate)) {
            end($tempusedWorkFlows);
            $forcedLastWorkflow = key($tempusedWorkFlows);
            foreach ($allWorkflows as $workflow) {
                if ($workflow->getWorkflowId() == $forcedLastWorkflow) {
                    return $workflow;
                }
            }
        }
        return $nextWorkflowToValidate;
    }
}