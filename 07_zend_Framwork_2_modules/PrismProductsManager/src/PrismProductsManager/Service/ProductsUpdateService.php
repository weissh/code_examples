<?php

namespace PrismProductsManager\Service;

use PrismElasticSearch\Enum\ElasticSearchTypes;

class ProductsUpdateService
{
    protected $queueService;
    protected $_siteConfig;
    protected $config;
    //protected $elasticSearchIndexingService;

    /**
     * @param $queueService
     * @param $elasticSearchIndexingService
     */
    public function __construct($queueService, $siteConfig, $config /*, $elasticSearchIndexingService*/ )
    {
        $this->queueService = $queueService;
        $this->_siteConfig = $siteConfig;
        $this->config = $config;
       // $this->elasticSearchIndexingService = $elasticSearchIndexingService;
    }

    /**
     * @param $productId
     * @param int $variantId
     */
    public function updateProductAndVariantId($productId, $variantId = 0)
    {
        if (
            isset($this->config['clientConfig']['settings']['newSpectrumAndSipSynch']) &&
            $this->config['clientConfig']['settings']['newSpectrumAndSipSynch']
        )
        {
           /** do to - sync to new sip and spectrum methods */
        } else {
            $this->getQueueService()->addSpectrumEntryToQueue($productId);
            $this->getQueueService()->addIndexEntryToQueue($productId.'_'.$variantId, ElasticSearchTypes::PRODUCTS);
        }
    }

    /**
     * @param $productId
     */
    public function updateProductId($productId)
    {
        $this->updateProductAndVariantId($productId);
    }

    /**
     * @return mixed
     */
    public function getQueueService()
    {
        return $this->queueService;
    }

}
