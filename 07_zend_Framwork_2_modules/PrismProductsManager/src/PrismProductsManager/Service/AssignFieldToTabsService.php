<?php

namespace PrismProductsManager\Service;

use Doctrine\ORM\EntityManager;
use PrismProductsManager\Entity\InputFieldRAttributes;
use PrismProductsManager\Entity\InputFieldRPages;
use PrismProductsManager\Entity\InputFieldRValidations;
use PrismProductsManager\Entity\InputFieldRWorkflows;
use PrismProductsManager\Entity\InputFields;
use PrismProductsManager\Entity\InputFieldsets;
use PrismProductsManager\Entity\InputFieldsVisibility;
use PrismProductsManager\Entity\InputFieldTypes;
use PrismProductsManager\Entity\InputPages;
use PrismProductsManager\Entity\InputValidations;
use PrismProductsManager\Entity\InputValidationTypes;
use PrismProductsManager\Model\AttributesGroupMapper;
use Zend\Stdlib\Hydrator\ClassMethods;
use \Doctrine\Common\Persistence\ObjectRepository as ObjectRepository;


/**
 * Class AssignFieldToTabsService
 * @package PrismProductsManager\Service
 */
class AssignFieldToTabsService
{
    /**
     * @var Array
     */
    protected $siteConfig;
    /**
     * @var EntityManager
     */
    protected $doctrineEntity;
    /**
     * @var ObjectRepository
     */
    protected $inputFieldsRepository;
    /**
     * @var InputFields
     */
    protected $inputFieldsEntity;

    /**
     * @var InputFieldTypes
     */
    protected $inputFieldTypesEntity;

    /**
     * @var ObjectRepository
     */
    protected $inputFieldTypesRepository;

    /**
     * @var ObjectRepository
     */
    protected $inputPagesRepository;

    /**
     * @var ObjectRepository
     */
    protected $inputFieldRAttributesRepository;

    /**
     * @var CommonAttributesService
     */
    protected $commonAttributesService;
    /**
     * @var InputPages
     */
    protected $inputPagesEntity;
    /**
     * @var InputFieldsets
     */
    protected $inputFieldsetsEntity;
    /**
     * @var InputFieldRAttributes
     */
    protected $inputFieldRAttributesEntity;

    /**
     * @var InputFieldRPages
     */
    protected $inputFieldRPages;

    /**
     * @var ObjectRepository
     */
    protected $attributesGroupsRepository;

    /**
     * @var AttributesGroupMapper
     */
    protected $attributesMapper;

    /**
     * @var Int
     */
    protected $currentLanguage;

    /**
     * @var ObjectRepository
     */
    protected $inputFieldValidationsRepository;

    /**
     * @var ObjectRepository
     */
    protected $inputFieldValidationTypesRepository;

    /**
     * @var InputValidations
     */
    protected $inputFieldValidationsEntity;

    /**
     * @var ObjectRepository
     */
    protected $workflowsRepository;
    /**
     * @var InputValidationTypes
     */
    protected $inputFieldValidationTypesEntity;

    /**
     * @var ObjectRepository
     */
    protected $inputFieldRPagesRepository;
    /**
     * @param $siteConfig
     * @param EntityManager $doctrineEntity
     * @param ObjectRepository $inputFieldsRepository
     * @param ObjectRepository $inputFieldTypesRepository
     * @param ObjectRepository $inputPagesRepository
     * @param ObjectRepository $inputFieldsetsRepository
     * @param ObjectRepository $inputFieldRAttributesRepository
     * @param ObjectRepository $attributesGroupsRepository
     * @param ObjectRepository $inputFieldValidationsRepository
     * @param ObjectRepository $inputFieldValidationTypesRepository
     * @param ObjectRepository $workflowsRepository
     * @param ObjectRepository $inputFieldRPagesRepository
     * @param InputFields $inputFieldsEntity
     * @param InputFieldTypes $inputFieldTypesEntity
     * @param InputPages $inputPagesEntity
     * @param InputFieldsets $inputFieldsetsEntity
     * @param InputFieldRAttributes $inputFieldRAttributesEntity
     * @param InputFieldRPages $inputFieldRPagesEntity
     * @param InputValidations $inputFieldValidationsEntity
     * @param InputValidationTypes $inputFieldValidationTypesEntity
     * @param CommonAttributesService $commonAttributesService
     * @param AttributesGroupMapper $attributesMapper
     */
    public function __construct(
        $siteConfig,
        EntityManager $doctrineEntity,
        ObjectRepository $inputFieldsRepository,
        ObjectRepository $inputFieldTypesRepository,
        ObjectRepository $inputPagesRepository,
        ObjectRepository $inputFieldsetsRepository,
        ObjectRepository $inputFieldRAttributesRepository,
        ObjectRepository $attributesGroupsRepository,
        ObjectRepository $inputFieldValidationsRepository,
        ObjectRepository $inputFieldValidationTypesRepository,
        ObjectRepository $workflowsRepository,
        ObjectRepository $inputFieldRPagesRepository,
        InputFields $inputFieldsEntity,
        InputFieldTypes $inputFieldTypesEntity,
        InputPages $inputPagesEntity,
        InputFieldsets $inputFieldsetsEntity,
        InputFieldRAttributes $inputFieldRAttributesEntity,
        InputFieldRPages $inputFieldRPagesEntity,
        InputValidations $inputFieldValidationsEntity,
        InputValidationTypes $inputFieldValidationTypesEntity,
        CommonAttributesService $commonAttributesService,
        AttributesGroupMapper $attributesMapper
    ) {
        $this->doctrineEntity = $doctrineEntity;
        $this->siteConfig = $siteConfig;
        /** Repositories */
        $this->inputFieldsRepository = $inputFieldsRepository;
        $this->inputFieldTypesRepository = $inputFieldTypesRepository;
        $this->inputPagesRepository = $inputPagesRepository;
        $this->inputFieldsetsRepository = $inputFieldsetsRepository;
        $this->inputFieldRAttributesRepository = $inputFieldRAttributesRepository;
        $this->attributesGroupsRepository = $attributesGroupsRepository;
        $this->inputFieldValidationsRepository = $inputFieldValidationsRepository;
        $this->inputFieldValidationTypesRepository = $inputFieldValidationTypesRepository;
        $this->workflowsRepository = $workflowsRepository;
        $this->inputFieldRPagesRepository = $inputFieldRPagesRepository;
        /** Entities */
        $this->inputFieldsEntity = $inputFieldsEntity;
        $this->inputFieldTypesEntity = $inputFieldTypesEntity;
        $this->inputPagesEntity = $inputPagesEntity;
        $this->inputFieldsetsEntity = $inputFieldsetsEntity;
        $this->inputFieldRAttributesEntity = $inputFieldRAttributesEntity;
        $this->inputFieldRPages = $inputFieldRPagesEntity;
        $this->inputFieldValidationsEntity = $inputFieldValidationsEntity;
        $this->inputFieldValidationTypesEntity = $inputFieldValidationTypesEntity;
        /** Services */
        $this->commonAttributesService = $commonAttributesService;
        /** Mappers */
        $this->attributesMapper = $attributesMapper;
        /** Other */
        $this->currentLanguage = $this->siteConfig['languages']['current-language'];
    }

    /**
     * @return array
     */
    public function getAttributesInputFieldsByCriteria()
    {
        $attributeInputFieldType = $this->inputFieldTypesRepository->findOneBy(array('inputfieldtypename' => 'isAttribute'));

        $inputFields = $this->inputFieldsRepository->findBy(
            array('inputtypeid' => $attributeInputFieldType)
        );

        $inputFieldsArray = array();
        foreach ($inputFields as $inputField) {
            $validationType = 'N/A';
            $validations = $inputField->getInputValidations();
            if (!empty($validations)) {
                $validationType = $inputField->getInputValidations()->getInputValidationType()->getInputValidationType();
            }
            $fieldsets = array();
            $pageName = '';
            $pageUrl = '';
            $pageId = '';
            foreach ($inputField->getinputFieldRPages() as $inputFieldOnPage) {
                /** Get Input page entity */
                $inputPageId = $inputFieldOnPage->getInputPageId();
                $inputPageEntity = $this->inputPagesRepository->findOneBy(array('inputpageid' => $inputPageId->getInputPageId()));

                $inputFieldSetEntity = $this->inputFieldsetsRepository->findOneBy(
                    array('inputfieldsetid' => $inputFieldOnPage->getInputFieldsetsInputFieldsetId()->getInputFieldsetId())
                );

                $pageUrl = $inputPageEntity->getPageUrl();
                $pageId = $inputPageEntity->getInputPageId();
                $pageName = $inputPageEntity->getPageName();
                $fieldsets[$inputFieldSetEntity->getInputFieldsetId()] = $inputFieldSetEntity->getInputFieldsetName();
            }

            $inputFieldRAttributeEntity = $this->inputFieldRAttributesRepository->findOneBy(
                array('inputfieldrattributeid' => $inputField->getInputFieldRAttributesInputFieldRAttributeId()->getInputFieldRAttributeId())
            );
            $attributeData = array();
            switch ($inputFieldRAttributeEntity->getAttributeTable()) {
                case 'COMMON_ATTRIBUTES':

                    $attributeId = $inputFieldRAttributeEntity->getCommonAttributes()->getCommonAttributeId();
                    $attributeEntity = $this->commonAttributesService->getCommonAttribute($attributeId);

                    $attributeValues = $this->commonAttributesService->getCommonAttributeValuesById($attributeEntity->getCommonAttributeId());
                    $attributeData = array(
                        'table' => $inputFieldRAttributeEntity->getAttributeTable(),
                        'attributeName' => $attributeEntity->getCommonAttributeDefinitions()->getDescription(),
                        'valueCount' => count($attributeValues)
                    );
                    break;
                case 'ATTRIBUTES_Groups':

                    $attributeId = $inputFieldRAttributeEntity->getattributeGroupId()->getattributegroupid();
                    $attributeValues = $this->attributesMapper->getAttributeValues($attributeId,
                        $this->currentLanguage);
                    $attributeData = $this->attributesMapper->fetchAllAttributeGroups($this->currentLanguage,
                        $attributeId);

                    $attributeData = array(
                        'table' => $inputFieldRAttributeEntity->getAttributeTable(),
                        'attributeName' => $attributeData['name'],
                        'valueCount' => count($attributeValues)
                    );
                    break;
            }
            $inputFieldsArray[] = array(
                'attribute' => $attributeData,
                'inputFieldId' => $inputField->getInputFieldId(),
                'page-name' => $pageName,
                'page-url' => $pageUrl,
                'page-id' => $pageId,
                'fieldsets' => $fieldsets,
                'validationType' => $validationType
            );
        }

        return $inputFieldsArray;
    }


    /**
     * @param array $criteria
     * @param array $order
     * @return array
     */
    public function getAllInputPages($criteria = array(), $order = array())
    {
        return $this->inputPagesRepository->findBy($criteria, $order);
    }

    /**
     * @param array $criteria
     * @param array $order
     * @return array
     */
    public function getAllInputFieldsets($criteria = array(), $order = array())
    {
        return $this->inputFieldsetsRepository->findBy($criteria, $order);
    }

    /**
     * @param $productTabId
     * @return array
     */
    public function retrieveAllFieldsetsFromTab($productTabId)
    {
        $inputFieldSets = array();
        /** Retrieve all input fields from specified tab */
        $inputFields = $this->inputFieldsRepository->findAll();
        /** Iterate all input fields */
        foreach ($inputFields as $inputField) {
            /** Iterate all relations to multiple pages for a single input */
            foreach ($inputField->getInputFieldRPages() as $inputFieldRPage) {
                /** Retrieve the inputFieldsetId */
                $inputFieldsetId = $inputFieldRPage->getInputFieldsetsInputFieldsetId()->getInputFieldsetId();
                /**
                 * Check if the inputFieldsetId matches with the one sent in the function
                 * and if the inputFieldSet is already set up in array
                 **/
                if ($productTabId == $inputFieldRPage->getInputPageId()->getInputPageId() && !isset($inputFieldSets[$inputFieldsetId])) {
                    /** Retrieve the fieldset details and save it to return array */
                    $inputFieldsetEntity = $this->inputFieldsetsRepository->findOneBy(array('inputfieldsetid' => $inputFieldsetId));
                    $inputFieldSets[$inputFieldsetEntity->getInputFieldsetId()] = $inputFieldsetEntity->getInputFieldsetName();
                }
            }
        }
        return $inputFieldSets;
    }

    /**
     * @param $sanitizedData
     * @return bool
     */
    public function createNewField($sanitizedData)
    {
        try {
            /** Check if we need to create a new fieldset and create it if needed */
            if (empty($sanitizedData['inputFieldsets'])) {
                $this->createNewFieldSet($sanitizedData['newInputFieldset']);
            } else {
                /** If new field is not needed than hydrate inputFieldsetsEntity with data  */
                $this->inputFieldsetsEntity = $this->inputFieldsetsRepository->findOneBy(
                    array('inputfieldsetid' => $sanitizedData['inputFieldsets'])
                );
            }

            /** Retrieve inputTypeEntity */
            $this->inputFieldTypesEntity = $this->inputFieldTypesRepository->findOneBy(
                array('inputfieldtypename' => 'isAttribute')
            );

            /** Create new Input Field Relation To Attributes */
            $this->createInputFieldRelationToAttributes($sanitizedData);

            /** Create the input field and relations */
            $this->hydrateInputFieldEntity($sanitizedData);

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $sanitizedData
     * @return bool
     */
    protected function createInputFieldRelationToAttributes($sanitizedData)
    {
        try {
            /** Switch to identify what kind of attribute are we assigning to field */
            switch ($sanitizedData['attributeType']) {
                case 'COMMON_ATTRIBUTES':
                    /** Create new assignation in the table for common attributes */
                    $this->inputFieldRAttributesEntity->setattributetable('COMMON_ATTRIBUTES');
                    $commonAttribute = $this->commonAttributesService->getCommonAttributeById($sanitizedData['commonAttributes']);
                    $this->inputFieldRAttributesEntity->setCommonAttributes($commonAttribute);
                    break;
                case 'CORE_ATTRIBUTES':
                    /** Create new assignation in the table for core attributes */
                    $this->inputFieldRAttributesEntity->setattributetable('ATTRIBUTES_Groups');
                    $attributeGroup = $this->attributesGroupsRepository->findOneBy(
                        array('attributegroupid' => $sanitizedData['coreAttributes'])
                    );
                    $this->inputFieldRAttributesEntity->setAttributeGroupId($attributeGroup);
                    break;
                case 'MARKETING_ATTRIBUTES':
                    /** Create new assignation in the table for marketing attributes */
                    $this->inputFieldRAttributesEntity->setattributetable('ATTRIBUTES_Groups');
                    $attributeGroup = $this->attributesGroupsRepository->findOneBy(
                        array('attributegroupid' => $sanitizedData['marketingAttributes'])
                    );
                    $this->inputFieldRAttributesEntity->setAttributeGroupId($attributeGroup);
                    break;
                case 'MERCHANT_ATTRIBUTES':
                    /** Create new assignation in the table for marketing attributes */
                    $this->inputFieldRAttributesEntity->setattributetable('ATTRIBUTES_Groups');
                    $attributeGroup = $this->attributesGroupsRepository->findOneBy(
                        array('attributegroupid' => $sanitizedData['merchantAttributes'])
                    );
                    $this->inputFieldRAttributesEntity->setAttributeGroupId($attributeGroup);
                    break;
            }
            /** Save the data and refresh the entity */
            $this->doctrineEntity->persist($this->inputFieldRAttributesEntity);
            $this->doctrineEntity->flush($this->inputFieldRAttributesEntity);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }


    /**
     * @param $sanitizedData
     * @return bool
     */
    protected function hydrateInputFieldEntity($sanitizedData)
    {
        try {
            /** Set input field entity with data */
            $this->inputFieldsEntity->setCreatedAt(new \DateTime('now'));
            $this->inputFieldsEntity->setModifiedat(new \DateTime('now'));
            $this->inputFieldsEntity->setInputTablefieldsInputtablefieldid(null);
            $this->inputFieldsEntity->setInputModulesInputmoduleid(null);
            $this->inputFieldsEntity->setEditable(true);
            $this->inputFieldsEntity->setSchedulable($sanitizedData['isSchedulable']);
            $this->inputFieldsEntity->setEditableAtLevel($sanitizedData['editableAtLevel']);
            $this->inputFieldsEntity->setHidden($sanitizedData['isHidden']);
            $this->inputFieldsEntity->setInputtypeid($this->inputFieldTypesEntity);
            $this->inputFieldsEntity->setInputFieldRAttributesInputfieldrattributeid($this->inputFieldRAttributesEntity);
            /** Add data in queue */
            $this->doctrineEntity->persist($this->inputFieldsEntity);

            /** Set input field visibility value */
            if (!empty($sanitizedData['display-only-value'])) {
                foreach ($sanitizedData['display-only-value'] as $displayOnlyValue) {
                    /** Set Input Field Visibility entity with data */
                    $inputFieldVisibility = new InputFieldsVisibility();
                    $inputFieldVisibility->setInputfieldId($this->inputFieldsEntity);
                    $inputFieldVisibility->setCommonAttributeValueId($displayOnlyValue);
                    $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($displayOnlyValue);
                    $inputFieldVisibility->setCommonAttribute($commonAttributeValueEntity->getCommonAttributeId());
                    /** Add data in queue */
                    $this->doctrineEntity->persist($inputFieldVisibility);
                }
            }
            $this->inputPagesEntity = $this->inputPagesRepository->findOneBy(array('inputpageid' => $sanitizedData['inputPages']));
            /** Set input inputFieldRPages entity */
            $this->inputFieldRPages->setInputPageId($this->inputPagesEntity);
            $this->inputFieldRPages->setInputFieldId($this->inputFieldsEntity);
            $this->inputFieldRPages->setOrder(0);
            $this->inputFieldRPages->setEnabled(1);
            $this->inputFieldRPages->setModifiedAt(new \DateTime('now'));
            $this->inputFieldRPages->setCreatedAt(new \DateTime('now'));
            $this->inputFieldRPages->setInputFieldsetsInputFieldsetId($this->inputFieldsetsEntity);
            /** Add data in queue */
            $this->doctrineEntity->persist($this->inputFieldRPages);
            /** Execute inserts  */
            $this->doctrineEntity->flush();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $newFieldset
     * @return bool
     */
    protected function createNewFieldSet($newFieldset)
    {
        try {
            /** Set entity data */
            $this->inputFieldsetsEntity->setInputfieldsetname($newFieldset);
            $this->inputFieldsetsEntity->setCreatedat(new \DateTime('now'));
            $this->inputFieldsetsEntity->setModifiedat(new \DateTime('now'));
            /** Save entity data  */
            $this->doctrineEntity->persist($this->inputFieldsetsEntity);
            $this->doctrineEntity->flush($this->inputFieldsetsEntity);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $displayValue
     * @return array
     */
    public function getCommonAttributesValues($displayValue = null)
    {
        $returnArray = array();
        if (!empty($displayValue)) {
            foreach ($displayValue as $value) {
                $valueEntity = $this->commonAttributesService->getCommonAttributeValueById($value);
                $returnArray[$value] = $valueEntity->getCommonAttributeid()->getCommonAttributeDefinitions()->getDescription() . ' - ' . $valueEntity->getCommonAttributeValuesDefinitions()->getValue();
            }
        }
        return $returnArray;
    }


    /**
     * @param $inputFieldId
     * @return array
     * @throws \Exception
     */
    public function getInputFieldDetails($inputFieldId)
    {
        $inputFieldEntity = $this->inputFieldsRepository->findOneBy(array('inputfieldid' => $inputFieldId));

        if (is_null($inputFieldEntity->getinputFieldRAttributesInputfieldRAttributeId())) {
            throw new \Exception('Cannot edit other types of fields');
        }

        $inputFieldRAttributeId = $inputFieldEntity->getInputFieldRAttributesInputFieldRAttributeId()->getInputFieldRAttributeId();

        $inputFieldRAttributeEntity = $this->inputFieldRAttributesRepository->findOneBy(
            array('inputfieldrattributeid' => $inputFieldRAttributeId)
        );

        $attributeType = $inputFieldRAttributeEntity->getAttributeTable();


        switch ($attributeType) {
            case 'COMMON_ATTRIBUTES':
                $commonAttributes = $inputFieldRAttributeEntity->getCommonAttributes()->getCommonAttributeId();
                $coreAttributes = 0;
                $marketingAttributes = 0;
                $merchantAttributes = 0;
                break;
            default:
                $attributeGroup = $this->attributesGroupsRepository->findOneBy(
                    array('attributegroupid' => $inputFieldRAttributeEntity->getAttributeGroupId()->getAttributeGroupId())
                );
                $commonAttributes = 0;
                $coreAttributes = 0;
                $marketingAttributes = 0;
                $merchantAttributes = 0;
                switch ($attributeGroup->getGroupType()) {
                    case 'Core':
                        $attributeType = 'CORE_ATTRIBUTES';
                        $coreAttributes = $attributeGroup->getAttributeGroupId();
                        break;
                    case 'Marketing':
                        $attributeType = 'MARKETING_ATTRIBUTES';
                        $marketingAttributes = $attributeGroup->getAttributeGroupId();
                        break;
                    case 'Merchant':
                        $attributeType = 'MERCHANT_ATTRIBUTES';
                        $merchantAttributes = $attributeGroup->getAttributeGroupId();
                        break;
                }
                break;
        }
        $displayValues = array();
        foreach ($inputFieldEntity->getinputFieldsVisibility() as $displayValue) {
            $displayValues[] = $displayValue->getCommonAttributeValueId();
        }

        $inputPageId = null;
        $inputFieldset = null;
        foreach ($inputFieldEntity->getInputFieldRPages() as $pageEntity) {
            $inputFieldset = $pageEntity->getInputFieldsetsInputFieldsetId()->getInputFieldsetId();
            $inputPageId = $pageEntity->getInputPageId()->getInputPageId();

        }

        return array(
            'inputFieldId' => $inputFieldEntity->getInputFieldId(),
            'inputPages' => $inputPageId,
            'inputFieldsets' => $inputFieldset,
            'attributeType' => $attributeType,
            'commonAttributes' => $commonAttributes,
            'coreAttributes' => $coreAttributes,
            'marketingAttributes' => $marketingAttributes,
            'merchantAttributes' => $merchantAttributes,
            'display-only-value[]' => $this->getCommonAttributesValues($displayValues),
            'isSchedulable' => $inputFieldEntity->isSchedulable(),
            'editableAtLevel' => $inputFieldEntity->getEditableAtLevel(),
            'isHidden' => $inputFieldEntity->isHidden()
        );
    }

    /**
     * @param $sanitizedData
     * @return bool
     */
    public function editInputField($sanitizedData)
    {
        try {
            /** Check if we need to create a new fieldset and create it if needed */
            if (empty($sanitizedData['inputFieldsets'])) {
                $this->createNewFieldSet($sanitizedData['newInputFieldset']);
            } else {
                /** If new field is not needed than hydrate inputFieldsetsEntity with data  */
                $this->inputFieldsetsEntity = $this->inputFieldsetsRepository->findOneBy(
                    array('inputfieldsetid' => $sanitizedData['inputFieldsets'])
                );
            }
            /** Hydrate entities and save data */
            $this->hydrateInputFieldEntityForEdit($sanitizedData);


            return true;
        } catch (\Exception $ex) {
            return false;
        }

    }


    /**
     * @param $inputArray
     * @return bool
     */
    public function saveInputFieldsOrder($inputArray)
    {
        try {
            foreach ($inputArray as $inputData) {
                $inputFieldRPage = $this->inputFieldRPagesRepository->find($inputData['inputFieldRPageId']);
                $inputFieldRPage->setOrder($inputData['order']);
                $this->doctrineEntity->persist($inputFieldRPage);
                $this->doctrineEntity->flush($inputFieldRPage);
            }

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $inputFieldsetId
     * @return array
     */
    public function getInputFieldsAssignedToFieldset($inputFieldsetId, $inputPageId)
    {
        $inputFieldsRPages = $this->inputFieldRPagesRepository->findBy(array('inputFieldsetsInputfieldsetid' => $inputFieldsetId, 'inputpageid' => $inputPageId), array('order' => 'ASC'));

        $returnArray = array();
        foreach ($inputFieldsRPages as $inputFieldsRPage) {
            $inputFieldEntity = $inputFieldsRPage->getinputfieldid();
            $fieldName = '';
            switch (true) {
                case $inputFieldEntity->getinputTablefieldsInputtablefieldid():

                    $columnName = $inputFieldEntity->getInputTablefieldsInputtablefieldid()->getColumn();
                    // Creat label from column name
                    $regex = '/(?<!^)((?<![[:upper:]])[[:upper:]]|[[:upper:]](?![[:upper:]]))/';
                    $fieldName = preg_replace( $regex, ' $1', $columnName );
                    $fieldName = strtoupper($fieldName);
                    switch ($fieldName) {
                        case 'STYLE':
                            $fieldName = 'STYLE CODE';
                            break;
                        case 'PRODUCT NAME':
                            $fieldName = 'STYLE NAME';
                            break;
                        case 'SHORT PRODUCT NAME':
                            $fieldName = 'STYLE SHORT NAME';

                    }
                    break;
                case $inputFieldEntity->getinputModulesInputmoduleid():
                    break;
                case $inputFieldEntity->getinputFieldRAttributesInputfieldrattributeid():
                    switch ($inputFieldEntity->getinputFieldRAttributesInputfieldrattributeid()->getattributetable()) {
                        case 'COMMON_ATTRIBUTES';
                            $fieldName = $inputFieldEntity->getinputFieldRAttributesInputfieldrattributeid()
                                ->getcommonAttributes()
                                ->getcommonAttributeDefinitions()
                                ->getdescription();
                            break;
                        case 'ATTRIBUTES_Groups':
                            $fieldName = $inputFieldEntity->getinputFieldRAttributesInputfieldrattributeid()
                                ->getattributeGroupId()
                                ->getattributeGroupDefinition()
                                ->getname();
                            break;
                    }
                    break;
            }
            $returnArray[] = array('entity' => $inputFieldsRPage, 'label' => $fieldName );

        }
        return $returnArray;
    }

    /**
     * @param $sanitizedData
     * @return bool
     */
    private function hydrateInputFieldEntityForEdit($sanitizedData)
    {
        try {
            /** Retrieve input field entity */
            $inputFieldEntity = $this->inputFieldsRepository->findOneBy(array('inputfieldid' => $sanitizedData['inputFieldId']));

            $inputPageEntity = $this->inputPagesRepository->findOneBy(array('inputpageid' => $sanitizedData['inputPages']));
            /** Update Input Field set and input Page */
            foreach ($inputFieldEntity->getInputFieldRPages() as $inputFieldRPage) {
                /** Check if the input page has been saved */
                if ($inputPageEntity->getInputPageId() != $inputFieldRPage->getInputPageId()->getInputPageId()) {
                    $inputFieldRPage->setInputPageId($inputPageEntity);
                }
                $inputFieldRPage->setInputFieldsetsInputFieldsetId($this->inputFieldsetsEntity);
                $this->doctrineEntity->persist($inputFieldRPage);
            }
            /** Set the is schedulable flag */

            /** Create new relation for Input Field R Attributes Table */
            $oldInpuTFieldRAttributesEntity = $inputFieldEntity->getinputFieldRAttributesInputFieldRAttributeId();
            $this->createInputFieldRelationToAttributes($sanitizedData);
            /** Assign new relation to input field */
            $inputFieldEntity->setinputFieldRAttributesInputFieldRAttributeId($this->inputFieldRAttributesEntity);
            $inputFieldEntity->setSchedulable($sanitizedData['isSchedulable']);
            $inputFieldEntity->setEditableAtLevel($sanitizedData['editableAtLevel']);
            $inputFieldEntity->setHidden($sanitizedData['isHidden']);
            $this->doctrineEntity->persist($inputFieldEntity);
            /** Delete old relation to Input Field R Attributes Table */
            $this->doctrineEntity->remove($oldInpuTFieldRAttributesEntity);
            $this->doctrineEntity->flush();

            /** Delete old Input Field Visibility entries ( for the display-only-value ) */
            $this->deleteInputFieldVisibilityEntries($inputFieldEntity->getinputFieldsVisibility());
            /** Create new Input Field Visibility entry ( for display-only-value ) */
            $this->createInputFieldVisibilityEntries($sanitizedData, $inputFieldEntity);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $sanitizedData
     * @param InputFields $inputFieldEntity
     * @return bool
     */
    private function createInputFieldVisibilityEntries($sanitizedData, InputFields $inputFieldEntity)
    {
        try {
            /** Set input field visibility value */
            if (!empty($sanitizedData['display-only-value'])) {
                foreach ($sanitizedData['display-only-value'] as $displayOnlyValue) {
                    /** Set Input Field Visibility entity with data */
                    $inputFieldVisibility = new InputFieldsVisibility();
                    $inputFieldVisibility->setInputfieldId($inputFieldEntity);
                    $inputFieldVisibility->setCommonAttributeValueId($displayOnlyValue);
                    $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($displayOnlyValue);
                    $inputFieldVisibility->setCommonAttribute($commonAttributeValueEntity->getCommonAttributeId());
                    /** Add data in queue */
                    $this->doctrineEntity->persist($inputFieldVisibility);
                }
                $this->doctrineEntity->flush();
            }
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $inputFieldVisibilityEntries
     * @return bool
     */
    private function deleteInputFieldVisibilityEntries($inputFieldVisibilityEntries)
    {
        try {
            if (!empty($inputFieldVisibilityEntries)) {
                foreach ($inputFieldVisibilityEntries as $inputFieldVisibilityEntity) {
                    $this->doctrineEntity->remove($inputFieldVisibilityEntity);
                }
                $this->doctrineEntity->flush();
            }
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $inputFieldId
     * @return bool|object
     */
    public function getInputField($inputFieldId)
    {
        try {
            return $this->inputFieldsRepository->findOneBy(array('inputfieldid' => $inputFieldId));
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $inputFieldId
     * @return bool
     */
    public function fieldIsAttributeType($inputFieldId)
    {
        try {
            $inputField = $this->getInputField($inputFieldId);
            $inputFieldType = $this->inputFieldTypesRepository->findOneBy(
                array('inputfieldtypeid' => $inputField->getInputTypeId()->getInputFieldTypeId())
            );
            if ($inputFieldType->getInputFieldTypeName() != 'isAttribute') {
                return false;
            }
            return false;
        } catch (\Exception $ex) {
            return true;
        }
    }

    /**
     * @param $inputFieldId
     * @return bool
     */
    public function deleteInputField($inputFieldId)
    {
        try {
            $inputField = $this->getInputField($inputFieldId);
            /** Delete Input Field Visibility ( display values ) */
            $this->deleteInputFieldVisibilityEntries($inputField->getInputFieldsVisibility());
            /** Delete Input Field R Pages */
            $this->deleteInputFieldRPages($inputField->getInputFieldRPages());
            /** Delete Input Validations */
            $this->deleteInputValidations($inputField->getInputValidations());
            /** Delete Input Workflow */
            $this->deleteInputWorkflow($inputField->getinputFieldRWorkflows());
            /** Delete Input Field */
            $this->deleteInputFieldAndInputFieldRAttributes($inputField);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    private function deleteInputWorkflow($inputFieldRWorkflows)
    {
        try {
            foreach ($inputFieldRWorkflows as $inputFieldRWorkflow) {
                $this->doctrineEntity->remove($inputFieldRWorkflow);
            }
            $this->doctrineEntity->flush();
            return true;
        } catch (\Exception $ex) {
            return false;
        }

    }
    /**
     * @param $inputFieldValidation
     * @return bool
     */
    public function deleteInputValidations($inputFieldValidation)
    {
        try {
            if (!is_null($inputFieldValidation)) {
                $this->doctrineEntity->remove($inputFieldValidation);
                $this->doctrineEntity->flush($inputFieldValidation);
            }
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $inputField
     * @return bool
     */
    public function deleteInputFieldAndInputFieldRAttributes($inputField)
    {
        try {
            $this->doctrineEntity->remove($inputField);
            $this->doctrineEntity->flush($inputField);

            $this->doctrineEntity->remove($inputField->getInputFieldRAttributesInputFieldRAttributeId());
            $this->doctrineEntity->flush($inputField->getInputFieldRAttributesInputFieldRAttributeId());

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }
    /**
     * @param $inputFieldRPages
     * @return bool
     */
    private function deleteInputFieldRPages($inputFieldRPages)
    {
        try {
            if (!empty($inputFieldRPages)) {
                foreach ($inputFieldRPages as $inputFieldRPage) {
                    $this->doctrineEntity->remove($inputFieldRPage);
                }
                $this->doctrineEntity->flush();
            }
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $commonAttributeId
     * @return bool
     */
    public function deleteInputFieldsForCommonAttribute($commonAttributeId)
    {
        try {
            $inputFieldRAttributes = $this->inputFieldRAttributesRepository->findBy(array('attributetable' => 'COMMON_ATTRIBUTES', 'commonAttributeId' => $commonAttributeId));
            foreach ($inputFieldRAttributes as $inputFieldRAttribute) {
                $inputField = $this->inputFieldsRepository->findOneBy(array('inputFieldRAttributesInputfieldrattributeid' => $inputFieldRAttribute));
                $this->deleteInputField($inputField->getInputFieldId());
            }
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $inputFieldId
     * @return InputValidations
     */
    public function retrieveValidationForInputField($inputFieldId)
    {
        $this->inputFieldsEntity = $this->inputFieldsRepository->findOneBy(array('inputfieldid' => $inputFieldId));
        return $this->inputFieldsEntity->getinputValidations();
    }

    /**
     * @return array
     */
    public function getInputValidationTypeForForm()
    {
        $inputValidations = $this->inputFieldValidationTypesRepository->findAll();
        $returnArray = array();
        foreach ($inputValidations as $inputValidation) {
            $returnArray[$inputValidation->getInputValidationTypeId()] = $inputValidation->getInputValidationType();
        }
        return $returnArray;
    }

    /**
     * @param $data
     * @return bool
     */
    public function saveInputFieldValidation($data)
    {
        try {
            /** Check if input field has validation  */
            if (!empty($data['inputValidationId'])) {
                $this->inputFieldValidationsEntity = $this->inputFieldValidationsRepository->findOneBy(
                    array('inputvalidationid' => $data['inputValidationId'])
                );
            }

            $this->inputFieldsEntity = $this->inputFieldsRepository->findOneBy(array('inputfieldid' => $data['inputFieldId']));
            $this->inputFieldValidationTypesEntity = $this->inputFieldValidationTypesRepository->findOneBy(
                array('inputvalidationtypeid' => $data['inputValidationType'])
            );

            /** Hydrate data */
            $this->inputFieldValidationsEntity->setPattern($data['pattern']);
            $this->inputFieldValidationsEntity->setRequired(0);
            $this->inputFieldValidationsEntity->setInputValidationMinValue($data['minValue']);
            $this->inputFieldValidationsEntity->setInputValidationMaxValue($data['maxValue']);
            $this->inputFieldValidationsEntity->setInputFieldId($this->inputFieldsEntity);
            $this->inputFieldValidationsEntity->setInputvalidationtype($this->inputFieldValidationTypesEntity);
            /** Save data */
            $this->doctrineEntity->persist($this->inputFieldValidationsEntity);
            $this->doctrineEntity->flush($this->inputFieldValidationsEntity);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $inputFieldId
     * @return bool
     */
    public function deleteInputFieldRelationToWorkflows($inputFieldId)
    {
        try {
            $inputFieldEntity = $this->getInputField($inputFieldId);
            foreach ($inputFieldEntity->getinputFieldRWorkflows() as $workflowEntity) {
                $this->doctrineEntity->remove($workflowEntity);
            }
            $this->doctrineEntity->flush();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $selectedWorkflows
     * @param $inputFieldId
     * @return bool
     */
    public function createInputFieldRelationToWorkflows($selectedWorkflows, $inputFieldId)
    {
        try {
            $inputFieldId = $this->getInputField($inputFieldId);
            foreach ($selectedWorkflows as $workflow) {
                $workflowEntity = $this->workflowsRepository->find($workflow);
                $inputFieldRWorkflowEntity = new InputFieldRWorkflows();
                $inputFieldRWorkflowEntity->setInputfieldid($inputFieldId);
                $inputFieldRWorkflowEntity->setWorkflowid($workflowEntity);
                $inputFieldRWorkflowEntity->setCreatedat(new \DateTime('now'));
                $inputFieldRWorkflowEntity->setModifiedat(new \DateTime('now'));
                $this->doctrineEntity->persist($inputFieldRWorkflowEntity);
            }
            $this->doctrineEntity->flush();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @return array|bool
     */
    public function getAllWorkflows()
    {
        try {
            return $this->workflowsRepository->findBy(array(), array('workflowpriority' => 'ASC'));
        } catch (\Exception $ex) {
            return false;
        }
    }
}