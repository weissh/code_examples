<?php

namespace PrismProductsManager\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use PrismProductsManager\Entity\CommonAttributes;
use PrismProductsManager\Entity\CommonAttributesGuiRepresentation;
use PrismProductsManager\Entity\CommonAttributesValues;
use PrismProductsManager\Entity\CommonAttributesValuesAutoFromDate;
use PrismProductsManager\Entity\CommonAttributesValuesChain;
use PrismProductsManager\Entity\CommonAttributesViewType;
use PrismProductsManager\Entity\HistoricalValue;
use PrismProductsManager\Entity\HistorySummary;
use PrismProductsManager\Entity\Products;
use PrismUsersManager\Model\UsersClientMapper;
use Symfony\Component\Config\Definition\Exception\Exception;
use Zend\Authentication\AuthenticationService;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Driver\Pdo\Result;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Sql;

/**
 * Class HistoricalService
 *
 * @package PrismProductsManager\Service
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class HistoricalService
{
    const HISTORICAL_VIEW = 'HISTORY_SUMMARY';

    /** @var EntityManager $entityManager */
    private $entityManager;
    /** @var \Doctrine\ORM\EntityRepository $commonAttributesValuesAutoFromDateRepository */
    private $commonAttributesValuesAutoFromDateRepository;
    /** @var \Doctrine\ORM\EntityRepository $commonAttributesValuesChainRepository */
    private $commonAttributesValuesChainRepository;
    /** @var CommonAttributesService $commonAttributesService */
    private $commonAttributesService;
    /** @var AuthenticationService $authService */
    private $authService;
    /** @var \Doctrine\ORM\EntityRepository $historicalValueRepository */
    private $historicalValueRepository;
    /** @var Adapter $dbAdapter */
    private $dbAdapter;
    /** @var \Doctrine\ORM\EntityRepository $productsRepository */
    private $productsRepository;
    /** @var \Doctrine\ORM\EntityRepository $commonAttributesValuesDefinitions */
    private $commonAttributesValuesDefinitions;
    /** @var array $cachedStyleCodes */
    private $cachedStyleCodes = [];
    /** @var \Doctrine\ORM\EntityRepository $historySummaryRepository */
    private $historySummaryRepository;
    /** @var  UsersClientMapper $usersClientMapper */
    private $usersClientMapper;
    /** @var array $cachedUsers */
    private $cachedUsers = [];

    public function __construct(
        EntityManager $entityManager,
        CommonAttributesService $commonAttributesService,
        AuthenticationService $authService,
        Adapter $dbAdapter,
        UsersClientMapper $usersClientMapper
    )
    {
        $this->entityManager = $entityManager;
        $this->commonAttributesService = $commonAttributesService;
        $this->commonAttributesValuesAutoFromDateRepository = $this->entityManager->getRepository('PrismProductsManager\Entity\CommonAttributesValuesAutoFromDate');
        $this->commonAttributesValuesChainRepository = $this->entityManager->getRepository('PrismProductsManager\Entity\CommonAttributesValuesChain');
        $this->historicalValueRepository = $this->entityManager->getRepository('PrismProductsManager\Entity\HistoricalValue');
        $this->productsRepository = $this->entityManager->getRepository('PrismProductsManager\Entity\Products');
        $this->commonAttributesValuesDefinitions = $this->entityManager->getRepository('PrismProductsManager\Entity\CommonAttributesValuesDefinitions');
        $this->historySummaryRepository = $this->entityManager->getRepository('PrismProductsManager\Entity\HistorySummary');
        $this->usersClientMapper = $usersClientMapper;
        $this->authService = $authService;
        $this->dbAdapter = $dbAdapter;

    }

    /**
     * @param \DateTime $dateTime
     * @return \Doctrine\Common\Collections\Collection
     */
    public function retrieveCurrentFromDate(\DateTime $dateTime)
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->gte('fromDate', $dateTime));
        $criteria->andWhere(Criteria::expr()->eq('processed', 0));
        return $this->commonAttributesValuesAutoFromDateRepository->matching($criteria);
    }

    /**
     * @param Collection $validFromDatesToChange
     * @return ArrayCollection
     */
    public function filterEntitiesAllowedToChange(Collection $validFromDatesToChange)
    {
        $filteredEntities = new ArrayCollection();

        /** @var CommonAttributesValuesAutoFromDate $dateEntity */
        foreach ($validFromDatesToChange as $dateEntity) {
            $entityDate = $dateEntity->getFromDate();
            /** @var CommonAttributesValues $commonAttributeValue */
            $commonAttributeValue = $dateEntity->getCommonAttributeValueId();
            $commonAttributeValueId = $commonAttributeValue->getcommonAttributeValueId();
            $commonAttributeEntity = $commonAttributeValue->getCommonattributeid();
            $commonAttributeId = $commonAttributeEntity->getCommonattributeid();

            $chainedDestinationValues = $commonAttributeValue->getCommonAttributesDestinationValuesChain();

            $chainedValueSiblings = new ArrayCollection();

            /** @var CommonAttributesValuesChain $chainedDestinationValue */
            foreach ($chainedDestinationValues as $chainedDestinationValue) {
                $sourceValueId = $chainedDestinationValue->getSourcevalueid();
                $sourceValues = $this->commonAttributesValuesChainRepository->findBy([
                    'sourcevalueid' => $sourceValueId
                ]);
                /** @var CommonAttributesValuesChain $sourceValue */
                foreach ($sourceValues as $sourceValue) {
                    $destinationCommonAttributeValueEntity = $sourceValue->getDestinationvalueid();
                    if (!is_null($destinationCommonAttributeValueEntity)) {
                        $destinationCommonAttributeEntity = $destinationCommonAttributeValueEntity->getCommonattributeid();
                        $destinationCommonAttributeId = $destinationCommonAttributeEntity->getCommonattributeid();
                        $destinationCommonAttributeValueId = $destinationCommonAttributeValueEntity->getcommonAttributeValueId();

                        if ($destinationCommonAttributeId == $commonAttributeId &&
                            $commonAttributeValueId != $destinationCommonAttributeValueId
                        ) {
                            $chainedValueSiblings->add($destinationCommonAttributeValueEntity);
                        }
                    }
                }
            }

            /** @var CommonAttributesValues $chainedValueSibling */
            foreach ($chainedValueSiblings as $chainedValueSibling) {
                $chainedValueAutoFromDate = $chainedValueSibling->getCommonAttributesValuesAutoFromDate();
                if (!is_null($chainedValueAutoFromDate)) {
                    $chainedValueFromDate = $chainedValueAutoFromDate->getFromDate();
                    if ($chainedValueFromDate instanceof \DateTime && $entityDate > $chainedValueFromDate) {
                        $filteredEntities->add([
                            'from' => $chainedValueSibling,
                            'to' => $commonAttributeValue
                        ]);
                    }
                }
            }
        }

        return $filteredEntities;
    }

    /**
     * @param Collection $filteredValuesToChange
     * @return Collection
     */
    public function changeFilterDates(Collection $filteredValuesToChange)
    {
        $filterValueDatesChanged = new ArrayCollection();

        foreach ($filteredValuesToChange as $filteredValue) {
            /** @var CommonAttributesValues|null $fromEntity */
            $fromEntity = isset($filteredValue['from']) ? $filteredValue['from'] : null;
            /** @var CommonAttributesValues|null $toEntity */
            $toEntity = isset($filteredValue['to']) ? $filteredValue['to'] : null;
#
            if ($fromEntity instanceof CommonAttributesValues &&
                $toEntity instanceof CommonAttributesValues
            ) {
                $fromCommonAttributeValueId = $fromEntity->getcommonAttributeValueId();
                $toCommonAttributeValueId = $toEntity->getcommonAttributeValueId();
                $commonAttributeId = $fromEntity->getCommonattributeid()->getCommonattributeid();

                $historicalEntriesCollection = $this->commonAttributesService->updateAllProductsCommonAttributeValues(
                    $commonAttributeId,
                    $fromCommonAttributeValueId,
                    $toCommonAttributeValueId,
                    $toEntity);

                try {
                    $this->recordChanges($historicalEntriesCollection);
                } catch (\Exception $ex) {
                    //TODO: DO SOMETHING
                }

                $dateEntity = $toEntity->getCommonAttributesValuesAutoFromDate();
                if ($dateEntity instanceof CommonAttributesValuesAutoFromDate) {
                    $filterValueDatesChanged->add($dateEntity);
                }
            }
        }
        return $filterValueDatesChanged;
    }

    /**
     * @param $whereAll
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function retrieveRecordsFromView($whereAll)
    {
        $sql = new Sql($this->dbAdapter);
        $sql->setTable(self::HISTORICAL_VIEW);
        $select = $sql->select()
            ->columns(
                ['id', 'optionOrStyleCode', 'attribute', 'attributeValue',
                    'fromDate', 'modified', 'modifiedBy', 'created', 'userName']
            )
            ->where($whereAll, PredicateSet::COMBINED_BY_OR);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     * @param $dataRow
     * @return array
     */
    public function updateEntryFromUpload($dataRow)
    {
        $errors = [];
        $updateNewData = [];
        /**
         * 0 => string 'id (DO NOT UPDATE)' (length=18)
         * 1 => string 'optionOrStyleCode (DO NOT UPDATE)' (length=17)
         * 2 => string 'attribute (DO NOT UPDATE)' (length=9)
         * 3 => string 'attributeValue' (length=14)
         * 4 => string 'fromDate' (length=8)
         * 5 => string 'modified' (length=8)
         * 6 => string 'modifiedBy (DO NOT UPDATE)' (length=10)
         * 7 => string 'created' (length=7)
         * 8 => string 'createdBy (DO NOT UPDATE)' (length=9)
         * 9 => string 'Delete - Add 1 to delete entry' (length=30)
         */
        $historicalEntryId = isset($dataRow[0]) ? $dataRow[0] : false;
        $historicalValue = isset($dataRow[3]) ? $dataRow[3] : false;
        $historicalFromDate = isset($dataRow[4]) ? $dataRow[4] : false;
        $historicalModifiedDate = isset($dataRow[5]) ? $dataRow[5] : false;
        $historicalCreatedDate = isset($dataRow[7]) ? $dataRow[7] : false;
        $historicalDeleteFlag = isset($dataRow[9]) ? $dataRow[9] : false;

        $updateFlag = false;
        $deleteFlag = false;
        $entryRecordsArray = [];

        /** @var HistoricalValue $historicalEntry */
        $historicalEntry = $this->getHistoricalEntity($historicalEntryId);
        if (is_null($historicalEntry)) {
            $errors[] = "Id ({$historicalEntryId}) cannot be matched to a record ";
        }

        if (!empty($historicalDeleteFlag)) {
            if ($historicalDeleteFlag != '1') {
                $errors[] = "Delete flag can only have values 1 or 0.";
            } else {
                $deleteFlag = true;
            }
        }

        if ($deleteFlag === true) {
            $entryRecords = $this->retrieveAllRecordsAlike($historicalEntryId);
            if (!empty($entryRecords)) {
                $result = $this->deleteAllRecordsRelatedWith($historicalEntryId);
                if (!empty($result['success'])) {
                    return ['success' => true, 'errors' => []];
                } else {
                    return ['success' => false, 'errors' => [$result['error']]];
                }
            }
        }

        /** Validate dates */
        if (!empty($historicalFromDate)) {
            if (!$this->isDateValid($historicalFromDate)) {
                $errors[] = "Historical From Date ({$historicalFromDate}) is not a valid format.";
            } else {
                /** Check if they are changed and update them. */#
                $historicalFromDate = new \DateTime($historicalFromDate);
                if ($historicalFromDate != $historicalEntry->getFromDate()) {
                    $updateFlag = true;
                    $updateNewData['fromData'] = $historicalFromDate;
                }
            }
        }

        $updateData = $this->validateDataForEntry($historicalEntryId, ['value' => $historicalValue]);

        if ($updateData['valid'] === true) {
            if ($historicalEntry->isDeleted() || $historicalEntry->getValue() != $updateData['data']['value']) {

                /** Need to update the value */
                $updateNewData['value'] = $updateData['data']['value'];
                $updateFlag = true;
            }
        } else {
            $errors = array_merge($errors, $updateData['errors']);
        }
        if ($updateFlag && !empty($updateNewData)) {
            if (empty($errors)) {
                /** let's start to update them */
                $entryRecords = $this->retrieveAllRecordsAlike($historicalEntryId);
                if (!empty($entryRecords)) {
                    if (!empty($entryRecords)) {
                        $result = $this->deleteAllRecordsRelatedWith($historicalEntryId);
                        if (!empty($result['success'])) {
                            foreach ($entryRecords as $entryRecord) {
                                $newEntryRecord = clone $entryRecord;
                                foreach ($updateNewData as $column => $value) {
                                    $method = 'set' . ucfirst($column);
                                    if (method_exists($newEntryRecord, 'set' . ucfirst($column))) {
                                        $lower = strtolower($column);
                                        if (strpos($lower, 'date') !== false) {
                                            $value = new \DateTime($value);
                                        }
                                        $newEntryRecord->$method($value);
                                    }
                                }
                                $newEntryRecord->setHistoricalValueId(null);
                                $newEntryRecord->setDeleted(false);
                                $newEntryRecord->setModified(new \DateTime('now'));
                                $this->entityManager->persist($newEntryRecord);
                                $entryRecordsArray[] = $newEntryRecord;
                            }
                        }
                    }
                    try {
                        $this->entityManager->flush();
                        if (!empty($entryRecordsArray)) {
                            foreach ($entryRecordsArray as $entryRecordArray) {
                                $this->entityManager->refresh($entryRecordArray);

                                $historySummaryData = $this->formatHistoricalSummaryFromHistoricalValue($entryRecordArray);
                                /** @var HistorySummary $historySummary */
                                $historySummary = $this->hydrateHistorySummaryData($historySummaryData);
                                $this->insertHistorySummary($historySummary);
                            }
                        }
                        return ['success' => true];
                    } catch (\Exception $ex) {
                        return ['success' => false, 'error' => $ex->getMessage()];
                    }
                }
            } else {
                return ['success' => false, 'errors' => $errors];
            }
        }

        /** We don't need to update anything */
        return ['success' => false, 'errors' => $errors];
    }

    /**
     * @param Collection $changedFilterDates
     * @return bool
     */
    public function markDatesAsProcessed(Collection $changedFilterDates)
    {
        try {
            /** @var CommonAttributesValuesAutoFromDate $commonAttributeValueAutoFromDate */
            foreach ($changedFilterDates as $commonAttributeValueAutoFromDate) {
                $commonAttributeValueAutoFromDate->setProcessed(1);
                $this->entityManager->persist($commonAttributeValueAutoFromDate);
            }
            $this->entityManager->flush();
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @param ArrayCollection $historicalEntriesCollection
     */
    public function recordChanges(ArrayCollection $historicalEntriesCollection)
    {
        $entryRecordsArray = [];

        try {
            $flush = false;
            /** @var HistoricalValue $historicalEntry */

            foreach ($historicalEntriesCollection as $historicalEntry) {
                if ($historicalEntry instanceof HistoricalValue) {

                    $isStyle = $historicalEntry->isStyle();
                    $productOrVariantId = $historicalEntry->getProductOrVariantId();
                    $optionCode = $historicalEntry->getOptionCode();

                    if ($isStyle === true && !empty($productOrVariantId) && empty($optionCode)) {
                        if (!isset($this->cachedStyleCodes[$productOrVariantId])) {
                            /** @var Products $productEntity */
                            $productEntity = $this->productsRepository->find($productOrVariantId);
                            $this->cachedStyleCodes[$productOrVariantId] = $productEntity->getStyle();
                        }
                        $historicalEntry->setOptionCode($this->cachedStyleCodes[$productOrVariantId]);
                    }


                    if ($historicalEntry->getHistoricalValueId() === null) {
                        /** Check if the historical entry is already in the database with the same values */
                        /** @var HistoricalValue $alreadyInDb */
                        $alreadyInDb = $this->historicalValueRepository->findOneBy([
                            'productOrVariantId' => $historicalEntry->getProductOrVariantId(),
                            'style' => $historicalEntry->isStyle(),
                            'optionCode' => $historicalEntry->getOptionCode(),
                            'commonAttribute' => $historicalEntry->getCommonAttribute(),
                            'attributeName' => $historicalEntry->getAttributeName(),
                            'value' => $historicalEntry->getValue(),
                            'fromDate' => $historicalEntry->getFromDate(),
                            'updateType' => $historicalEntry->getUpdateType(),
                            'deleted' => 0
                        ]);
                        if ($alreadyInDb !== null) {
                            /** History already contains this value for this product */
                            $alreadyInDbId = $alreadyInDb->getHistoricalValueId();
                            /** @var HistoricalValue $otherRecords */
                            $otherRecords = $this->historicalValueRepository->findOneBy([
                                'productOrVariantId' => $historicalEntry->getProductOrVariantId(),
                                'style' => $historicalEntry->isStyle(),
                                'optionCode' => $historicalEntry->getOptionCode(),
                                'commonAttribute' => $historicalEntry->getCommonAttribute(),
                                'attributeName' => $historicalEntry->getAttributeName(),
                                'deleted' => 0
                            ], [
                                'historicalValueId' => 'DESC'
                            ]);
                            if ($alreadyInDbId == $otherRecords->getHistoricalValueId()) {
                                continue;
                            }
                        }
                    }
                    $commonAttribute = $historicalEntry->getCommonAttribute();
                    if ($commonAttribute !== null) {
                        if (!$commonAttribute instanceof \Doctrine\Common\Persistence\Proxy) {
                            $commonAttribute = $this->commonAttributesService->getCommonAttribute($commonAttribute->getCommonattributeid());
                            $historicalEntry->setCommonAttribute($commonAttribute);
                        }
                        if ($commonAttribute->isKeepHistory()) {

                            $this->entityManager->persist($historicalEntry);
                            $flush = true;
                        }
                    } else {

                        $this->entityManager->persist($historicalEntry);
                        $flush = true;
                    }

                }
                if ($flush) {
                    $entryRecordsArray[] = $historicalEntry;
                    $this->entityManager->flush($historicalEntry);
                }
            }

            if (!empty($entryRecordsArray)) {
                foreach ($entryRecordsArray as $entryRecordArray) {
                    $this->entityManager->refresh($entryRecordArray);

                    $historySummaryData = $this->formatHistoricalSummaryFromHistoricalValue($entryRecordArray);
                    /** @var HistorySummary $historySummary */
                    $historySummary = $this->hydrateHistorySummaryData($historySummaryData);
                    $this->insertHistorySummary($historySummary);
                }
            }
        } catch (\Exception $ex) {
            var_dump($ex->getMessage());
            die;
        }


    }


    public function hydrateHistoricalEntity($data = [])
    {
        if (!isset($data['value'])) {
            $data['value'] = '';
        }
        $this->validateHistoricalEntityData($data);
        $userId = !isset($data['userId']) ? $this->getUserId() : $data['userId'];
        $historicalValue = new HistoricalValue();
        $historicalValue->setProductOrVariantId($data['productOrVariantId']);
        $historicalValue->setStyle($data['isStyle']);
        $historicalValue->setUserId($userId);
        if (!empty($data['commonAttribute'])) {
            $historicalValue->setCommonAttribute($data['commonAttribute']);
        }

        $historicalValue->setOptionCode('');
        if (!empty($data['optionCode'])) {
            $historicalValue->setOptionCode($data['optionCode']);
        }

        $historicalValue->setAttributeName($data['attributeName']);
        $historicalValue->setValue($data['value']);
        $historicalValue->setFromDate($data['fromDate']);
        $historicalValue->setUpdateType($data['updateType']);
        $historicalValue->setModified(new \DateTime());
        $historicalValue->setModifiedBy($userId);
        $historicalValue->setCreated(new \DateTime());

        return $historicalValue;
    }

    private function getUserId()
    {
        $array = $this->authService->getStorage()->read();
        return isset($array['userId']) ? $array['userId'] : null;
    }

    private function validateHistoricalEntityData($data)
    {

        if (empty($data['productOrVariantId'])) {
            throw new \Exception('Key productOrVariantId could not be found in Historical array data. ' . json_encode($data));
        }
        if (!isset($data['isStyle'])) {
            throw new \Exception('Key isStyle could not be found in Historical array data. ' . json_encode($data));
        }
        if (($data['commonAttribute'] !== null && !$data['commonAttribute'] instanceof CommonAttributes)) {
            throw new \Exception('Key commonAttribute needs to be instance of CommonAttributes entity class. ' . json_encode($data));
        }
        if (!isset($data['attributeName'])) {
            throw new \Exception('Key attributeName could not be found in Historical array data. ' . json_encode($data));
        }
        /** @var CommonAttributes $commonAttribute */
        $commonAttribute = $data['commonAttribute'];
        if (empty($commonAttribute) && empty($data['attributeName'])) {
            throw new \Exception('Either Common Attribute entity or attributeName fields needs to be setup. ' . json_encode($data));
        }
        if (!isset($data['value'])) {
            throw new \Exception('Key value could not be found in Historical array data. ' . json_encode($data));
        }
        if (!isset($data['updateType'])) {
            throw new \Exception('Key updateType could not be found in Historical array data. ' . json_encode($data));
        }

        if (!$data['isStyle'] && !isset($data['optionCode'])) {
            throw new \Exception('Key optionCode could not be found in Historical array data. ' . json_encode($data));
        }
    }

    /**
     * @param $entryId
     * @return array
     */
    private function retrieveAllRecordsAlike($entryId)
    {
        $whereArray = ['historicalValueId' => $entryId];
        /** @var HistoricalValue $historicalEntry */
        $historicalEntry = $this->historicalValueRepository->findOneBy($whereArray);

        if (!$historicalEntry->isStyle()) {
            $whereArray = [
                'style' => $historicalEntry->isStyle(),
                'optionCode' => $historicalEntry->getOptionCode(),
                'userId' => $historicalEntry->getUserId(),
                'commonAttribute' => $historicalEntry->getCommonAttribute(),
                'attributeName' => $historicalEntry->getAttributeName(),
                'value' => $historicalEntry->getValue(),
                'fromDate' => $historicalEntry->getFromDate(),
                'updateType' => $historicalEntry->getUpdateType(),
            ];
        }

        return $this->historicalValueRepository->findBy($whereArray);
    }

    /**
     * @param $entryId
     * @return array
     */
    public function deleteAllRecordsRelatedWith($entryId)
    {
        $historicalEntriesToDelete = $this->retrieveAllRecordsAlike($entryId);

        if (empty($historicalEntriesToDelete)) {
            return ['success' => false, 'error' => 'Could not find any historical entries to remove.'];
        }

        return $this->deleteRecords($historicalEntriesToDelete);
    }

    /**
     * @param $entryId
     * @param $updateData
     * @return array
     */
    public function updateHistoricalEntry($entryId, $updateData)
    {
        $validatedData = $this->validateDataForEntry($entryId, $updateData);
        $entryRecordsArray = [];
        if (!empty($validatedData['valid'])) {
            $updateData = $validatedData['data'];
            $entryRecords = $this->retrieveAllRecordsAlike($entryId);
            if (!empty($entryRecords)) {
                $result = $this->deleteAllRecordsRelatedWith($entryId);

                if (!empty($result['success'])) {
                    /** @var HistoricalValue $entryRecord */
                    foreach ($entryRecords as $entryRecord) {

                        $newEntryRecord = clone $entryRecord;
                        foreach ($updateData as $column => $value) {

                            $method = 'set' . ucfirst($column);
                            if (method_exists($newEntryRecord, 'set' . ucfirst($column))) {
                                $lower = strtolower($column);
                                if (strpos($lower, 'date') !== false) {
                                    if (!empty($value)) {
                                        $value = new \DateTime($value);
                                        $newEntryRecord->$method($value);
                                    }
                                } else {
                                    $newEntryRecord->$method($value);
                                }

                            }
                        }
                        $newEntryRecord->setHistoricalValueId(null);
                        $newEntryRecord->setDeleted(false);
                        $newEntryRecord->setModified(new \DateTime('now'));

                        $this->entityManager->persist($newEntryRecord);
                        $entryRecordsArray[] = $newEntryRecord;
                    }
                    try {

                        $this->entityManager->flush();

                        foreach ($entryRecordsArray as $entryRecordArray) {
                            $this->entityManager->refresh($entryRecordArray);

                            $historySummaryData = $this->formatHistoricalSummaryFromHistoricalValue($entryRecordArray);
                            /** @var HistorySummary $historySummary */
                            $historySummary = $this->hydrateHistorySummaryData($historySummaryData);
                            $this->insertHistorySummary($historySummary);
                        }
                        return ['success' => true];
                    } catch (\Exception $ex) {
                        return ['success' => false, 'error' => $ex->getMessage()];
                    }

                }
                return ['success' => false, 'error' => 'Could not update old record with deleted flag.'];
            }

            return ['success' => false, 'error' => 'Could not find records to updated.'];
        }
        return ['success' => false, 'error' => implode("\n", $validatedData['errors'])];
    }

    /**
     * @param HistorySummary $historySummary
     */
    private function insertHistorySummary(HistorySummary $historySummary)
    {
        $fromDate = $historySummary->getFromDate();
        if ($fromDate == '0000-00-00 00:00:00') {
            $fromDate = null;
        }

        $historySummaryRecord = $this->historySummaryRepository->findOneBy([
            'optionOrStyleCode' => $historySummary->getOptionOrStyleCode(),
            'attribute' => $historySummary->getAttribute(),
            'attributeValueHash' => $historySummary->getAttributeValueHash(),
            'fromDate' => $fromDate,
            'updateType' => $historySummary->getUpdateType(),
            'modifiedTrunc' => $historySummary->getModifiedTrunc(),
            'createdTrunc' => $historySummary->getCreatedTrunc()
        ]);

        if (is_null($historySummaryRecord)) {
            $this->entityManager->persist($historySummary);
            $this->entityManager->flush($historySummary);
        }
    }

    /**
     * @param array $historySummaryData
     * @return HistorySummary
     */
    private function hydrateHistorySummaryData($historySummaryData = [])
    {
        $historySummary = new HistorySummary();
        if (isset($historySummaryData['optionOrStyleCode'])) {
            $historySummary->setOptionOrStyleCode($historySummaryData['optionOrStyleCode']);
        }
        if (isset($historySummaryData['isStyle'])) {
            $historySummary->setIsStyle($historySummaryData['isStyle']);
        }
        if (isset($historySummaryData['userName'])) {
            $historySummary->setUsername($historySummaryData['userName']);
        }
        if (isset($historySummaryData['commonAttributeId'])) {
            $historySummary->setCommonAttributeId($historySummaryData['commonAttributeId']);
        }
        if (isset($historySummaryData['attributeValue'])) {
            $historySummary->setAttributeValue($historySummaryData['attributeValue']);
        }
        if (isset($historySummaryData['modified'])) {
            $historySummary->setModified($historySummaryData['modified']);
        }
        if (isset($historySummaryData['modifiedBy'])) {
            $historySummary->setModifiedBy($historySummaryData['modifiedBy']);
        }
        if (isset($historySummaryData['created'])) {
            $historySummary->setCreated($historySummaryData['created']);
        }
        if (isset($historySummaryData['createdBy'])) {
            $historySummary->setCreatedBy($historySummaryData['createdBy']);
        }
        if (isset($historySummaryData['attribute'])) {
            $historySummary->setAttribute($historySummaryData['attribute']);
        }
        if (isset($historySummaryData['attributeValueHash'])) {
            $historySummary->setAttributeValueHash($historySummaryData['attributeValueHash']);
        } elseif (isset($historySummaryData['attributeValue'])) {
            $historySummary->setAttributeValueHash(md5($historySummaryData['attributeValue']));
        }
        if (isset($historySummaryData['fromDate'])) {
            $historySummary->setFromDate($historySummaryData['fromDate']);
        }
        if (isset($historySummaryData['updateType'])) {
            $historySummary->setUpdateType($historySummaryData['updateType']);
        }
        if (isset($historySummaryData['modifiedTrunc'])) {
            $historySummary->setModifiedTrunc($historySummaryData['modifiedTrunc']);
        }
        if (isset($historySummaryData['createdTrunc'])) {
            $historySummary->setCreatedTrunc($historySummaryData['createdTrunc']);
        }
        if (isset($historySummaryData['id'])) {
            $historySummary->setId($historySummaryData['id']);
        }

        return $historySummary;
    }

    /**
     * @param $entryId
     * @return null|object
     */
    public function getHistoricalEntity($entryId)
    {
        return $this->historicalValueRepository->find($entryId);
    }

    /**
     * @param $entryId
     * @param $updateData
     * @return array
     */
    private function validateDataForEntry($entryId, $updateData)
    {
        /** @var HistoricalValue $historicalEntry */
        $historicalEntry = $this->historicalValueRepository->find($entryId);

        $newValue = isset($updateData['value']) ? $updateData['value'] : false;
        if ($newValue === false) {
            return [
                'valid' => false,
                'errors' => ['Could not find value in request.'],
                'data' => $updateData
            ];
        }

        if (empty($historicalEntry->getAttributeName())) {
            /** This is a common attribute */
            $commonAttribute = $historicalEntry->getCommonAttribute();
            if (!is_null($commonAttribute)) {
                $viewType = false;
                /** @var CommonAttributesViewType $commonAttributeGuiRepresentation */
                $commonAttributeGuiRepresentation = $commonAttribute->getCommonAttributeGuiRepresentation();
                if ($commonAttributeGuiRepresentation instanceof CommonAttributesGuiRepresentation) {

                    $commonAttributeViewType = $commonAttributeGuiRepresentation->getCommonattributesviewtypeid();
                    if ($commonAttributeViewType instanceof CommonAttributesViewType) {
                        $viewType = $commonAttributeViewType->getCommonattributeviewtype();
                    }
                }
                $formattedNewValue = strtolower($newValue);
                $formattedNewValue = str_replace(' ', '', $formattedNewValue);
                switch ($viewType) {
                    case CommonAttributesViewType::VIEW_TYPE_MULTISELECT:
                    case CommonAttributesViewType::VIEW_TYPE_SELECT:
                        $commonAttributeValues = $commonAttribute->getCommonAttributeValues();
                        $matched = false;

                        $currentValue = $historicalEntry->getValue();
                        $newCommonAttributeValueEntity = false;
                        $currentCommonAttributeValue = false;
                        /** @var CommonAttributesValues $commonAttributeValue */
                        foreach ($commonAttributeValues as $commonAttributeValue) {
                            $definition = $commonAttributeValue->getCommonAttributeValuesDefinitions();
                            $definitionValue = $definition->getValue();
                            $formattedDefinitionValue = strtolower($definitionValue);
                            $formattedDefinitionValue = str_replace(' ', '', $formattedDefinitionValue);
                            if ($formattedDefinitionValue == $formattedNewValue) {
                                $matched = true;
                                $newCommonAttributeValueEntity = $commonAttributeValue;
                                $updateData['value'] = $commonAttributeValue->getcommonAttributeValueId();
                                break;
                            }
                            if ($currentValue == $commonAttributeValue->getcommonAttributeValueId()) {
                                $currentCommonAttributeValue = $commonAttributeValue;
                            }
                        }
                        if (!$matched) {
                            return [
                                'valid' => false,
                                'errors' => ["Could not match value {$newValue} with existing attribute values."],
                                'data' => $updateData
                            ];
                        }

                        if ($currentCommonAttributeValue !== false) {
                            $sourceValues = $currentCommonAttributeValue->getCommonAttributesDestinationValuesChain();
                            $chainedWith = [];
                            /** @var CommonAttributesValuesChain $sourceValue */
                            foreach ($sourceValues as $sourceValue) {
                                /** @var CommonAttributesValues $destinationValue */
                                $destinationValue = $sourceValue->getDestinationvalueid();
                                if (!empty($definitionValue)) {
                                    /** @var CommonAttributes $destinationCommonAttributeId */
                                    $destinationCommonAttributeId = $destinationValue->getCommonattributeid();
                                    $commonAttributeId = $destinationCommonAttributeId->getCommonattributeid();
                                    $chainedWith[$commonAttributeId][] = $destinationValue->getcommonAttributeValueId();
                                }
                            }
                            if (!empty($newCommonAttributeValueEntity)) {
                                $sourceValuesNew = $newCommonAttributeValueEntity->getCommonAttributesDestinationValuesChain();
                                $chainedWithNew = [];
                                foreach ($sourceValuesNew as $sourceValueNew) {
                                    $destinationValueNew = $sourceValueNew->getDestinationvalueid();
                                    if (!empty($destinationValueNew)) {
                                        /** @var CommonAttributes $destinationCommonAttributeId */
                                        $destinationCommonAttributeIdNew = $destinationValueNew->getCommonattributeid();
                                        $commonAttributeIdNew = $destinationCommonAttributeIdNew->getCommonattributeid();
                                        $chainedWithNew[$commonAttributeIdNew][] = $destinationValueNew->getcommonAttributeValueId();
                                    }
                                }
                            }

                            $currentCommonAttributes = array_keys($chainedWithNew);

                            foreach ($currentCommonAttributes as $currentCommonAttribute) {
                                $commonAttributeTmp = $this->commonAttributesService->getCommonAttribute($currentCommonAttribute);
                                /** @var HistoricalValue $historyEntity */
                                $historyEntity = $this->historicalValueRepository->findOneBy(
                                    [
                                        'productOrVariantId' => $historicalEntry->getProductOrVariantId(),
                                        'style' => $historicalEntry->isStyle(),
                                        'commonAttribute' => $commonAttributeTmp,
                                        'deleted' => false
                                    ],
                                    [
                                        'historicalValueId' => 'DESC'
                                    ]
                                );
                                if (!isset($chainedWithNew[$currentCommonAttribute])) {
                                    return [
                                        'valid' => false,
                                        'errors' => ["New value is not chained with the correct value of common attribute id $currentCommonAttribute."],
                                        'data' => $updateData
                                    ];
                                } else {
                                    if (!empty($historyEntity)) {
                                        $currentValueForChainedAttribute = $historyEntity->getValue();

                                        if (!empty($currentValueForChainedAttribute)) {

                                            if (!in_array($currentValueForChainedAttribute, $chainedWithNew[$currentCommonAttribute])) {
                                                return [
                                                    'valid' => false,
                                                    'errors' => ["New value is not chained with the correct value of  common attribute id $currentCommonAttribute."],
                                                    'data' => $updateData
                                                ];
                                            }
                                        }
                                    } else {

                                    }
                                }
                            }
                        }

                        break;
                    case CommonAttributesViewType::VIEW_TYPE_CHECKBOX:
                        if (!in_array($newValue, ['0', '1'])) {
                            return [
                                'valid' => false,
                                'errors' => ['Attribute type checkbox must have only 0 or 1 values.'],
                                'data' => $updateData
                            ];
                        }
                        break;
                    case CommonAttributesViewType::VIEW_TYPE_DATEPICKER:
                    case CommonAttributesViewType::VIEW_TYPE_DATEPICKER_DATE_ONLY:
                        if (!$this->isDateValid($newValue)) {
                            return [
                                'valid' => false,
                                'errors' => ['Value is not a valid date.'],
                                'data' => $updateData
                            ];
                        }
                        break;
                    case CommonAttributesViewType::VIEW_TYPE_TEXT:
                    case CommonAttributesViewType::VIEW_TYPE_TEXTAREA:
                        break;
                    default:
                        break;
                }
            }
        } else {
            if (!preg_match("/^[a-zA-Z]+$/", $newValue)) {
                /** This is a price */
                $newValueFormatted = number_format($newValue, 2);
                $updateData['value'] = $newValueFormatted;
                if (!is_numeric($newValueFormatted)) {
                    return ['valid' => true, 'errors' => ['Value is not numeric format.'], 'data' => $updateData];
                }
            }
        }
        return ['valid' => true, 'errors' => [], 'data' => $updateData];
    }

    /**
     * @param array $historicalEntriesToDelete
     * @return array
     */
    private function deleteRecords($historicalEntriesToDelete = [])
    {
        $historicalSummaryEntriesToDelete = [];

        /** @var HistoricalValue $historicalEntryToDelete */
        foreach ($historicalEntriesToDelete as $historicalEntryToDelete) {
            $historicalSummaryEntriesToDelete[] = $historicalEntryToDelete;
            $historicalEntryToDelete->setDeleted(true);
            $this->entityManager->persist($historicalEntryToDelete);
        }
        try {
            $this->deleteRecordsFromSummaryRelatedWith($historicalSummaryEntriesToDelete);
            $this->entityManager->flush();
            return ['success' => true];
        } catch (\Exception $ex) {
            return ['success' => false, 'error' => $ex->getMessage()];
        }
    }

    /**
     * @param HistoricalValue $historicalValue
     * @return array
     */
    private function formatHistoricalSummaryFromHistoricalValue(HistoricalValue $historicalValue)
    {
        $optionOrStyleCode = $historicalValue->getOptionCode();
        $attribute = $historicalValue->getAttributeName();
        $commonAttributeId = $historicalValue->getCommonAttribute();
        $attributeValue = $historicalValue->getValue();
        if (!empty($commonAttributeId)) {
            /** @var CommonAttributes $commonAttribute */
            $commonAttribute = $this->commonAttributesService->getCommonAttribute($commonAttributeId);
            if (!is_null($commonAttribute)) {
                $commonAttributeDefinition = $commonAttribute->getCommonAttributeDefinitions();
                if (!is_null($commonAttributeDefinition)) {
                    $attribute = $commonAttributeDefinition->getDescription();
                }
                /** @var CommonAttributesGuiRepresentation $commonAttributeGuiRepresentation */
                $commonAttributeGuiRepresentation = $commonAttribute->getCommonAttributeGuiRepresentation();
                if (!is_null($commonAttributeGuiRepresentation)) {

                    /** @var CommonAttributesViewType $commonAttributeViewType */
                    $commonAttributeViewType = $commonAttributeGuiRepresentation->getCommonattributesviewtypeid();
                    if (!is_null($commonAttributeViewType)) {
                        $commonAttributeViewType = $commonAttributeViewType->getCommonattributeviewtype();
                        if ($commonAttributeViewType == 'SELECT' || $commonAttributeViewType == 'MULTISELECT' && !empty($attributeValue)) {
                            /** @var CommonAttributesValues $commonAttributeValue */
                            $commonAttributeValue = $this->commonAttributesService->getCommonAttributeValueById($attributeValue);
                            if (!is_null($commonAttributeValue)) {
                                /** @var $commonAttributeValueDefinition $commonAttributeValueDefinition */
                                $commonAttributeValueDefinition = $commonAttributeValue->getCommonAttributeValuesDefinitions();
                                if (!is_null($commonAttributeValueDefinition)) {
                                    $attributeValue = $commonAttributeValueDefinition->getValue();
                                }
                            }
                        }
                    }
                }
            }
        }
        $attributeValueHash = md5($attributeValue);

        $fromDate = $historicalValue->getFromDate();
        if ($fromDate instanceof \DateTime) {
            $fromDate = $fromDate->format('Y-m-d H:i:s');
        } elseif ($fromDate === null) {
            $fromDate = '0000-00-00 00:00:00';
        }
        $updateType = $historicalValue->getUpdateType();

        $modifiedAtTrunc = $historicalValue->getModified();
        if (!is_null($modifiedAtTrunc)) {
            $modifiedAtTrunc = $modifiedAtTrunc->format('Y-m-d H:i');
        }
        $createdAtTrunc = $historicalValue->getCreated();
        if (!is_null($createdAtTrunc)) {
            $createdAtTrunc = $createdAtTrunc->format('Y-m-d H:i');
        }

        $userId = $historicalValue->getUserId();
        $username = "";

        if (!empty($userId)) {
            if (!isset($this->cachedUsers[$userId]['emailAddress'])) {
                $userData = $this->usersClientMapper->getUser($userId);
                if (!empty($userData)) {
                    $username = isset($userData->emailAddress) ? $userData->emailAddress : '';
                    $this->cachedUsers[$userId]['emailAddress'] = $username;
                }
            } else {
                $username = $this->cachedUsers[$userId]['emailAddress'];
            }
        }
        $commonAttribute = $historicalValue->getCommonAttribute();
        $commonAttributeId = null;
        if (!empty($commonAttribute)) {
            $commonAttributeId = $commonAttribute->getCommonattributeid();
        }


        $returnArray = [
            'id' => $historicalValue->getHistoricalValueId(),
            'optionOrStyleCode' => $optionOrStyleCode,
            'isStyle' => $historicalValue->isStyle(),
            'userName' => $username,
            'commonAttributeId' => $commonAttributeId,
            'attributeValue' => $attributeValue,
            'modified' => $historicalValue->getModified(),
            'modifiedBy' => $historicalValue->getModifiedBy(),
            'created' => $historicalValue->getCreated(),
            'createdBy' => $userId,
            'attribute' => $attribute,
            'attributeValueHash' => $attributeValueHash,
            'fromDate' => $fromDate,
            'updateType' => $updateType,
            'modifiedTrunc' => $modifiedAtTrunc,
            'createdTrunc' => $createdAtTrunc
        ];
        return $returnArray;
    }

    /**
     * @param array $deleteSummaryRecordsRelatedWith
     * @param bool $flush
     */
    private function deleteRecordsFromSummaryRelatedWith(array $deleteSummaryRecordsRelatedWith, $flush = false)
    {
        foreach ($deleteSummaryRecordsRelatedWith as $historicalValue) {

            if ($historicalValue instanceof HistoricalValue) {
                $historicalSummaryData = $this->formatHistoricalSummaryFromHistoricalValue($historicalValue);

                $historySummaryEntries = $this->historySummaryRepository->findBy([
                    'optionOrStyleCode' => $historicalSummaryData['optionOrStyleCode'],
                    'attribute' => $historicalSummaryData['attribute'],
                    'attributeValueHash' => $historicalSummaryData['attributeValueHash'],
                    'fromDate' => $historicalSummaryData['fromDate'],
                    'updateType' => $historicalSummaryData['updateType'],
                    'modifiedTrunc' => $historicalSummaryData['modifiedTrunc'],
                    'createdTrunc' => $historicalSummaryData['createdTrunc']
                ]);

                if (!empty($historySummaryEntries)) {
                    foreach ($historySummaryEntries as $historySummaryEntry) {
                        $this->entityManager->remove($historySummaryEntry);
                    }
                }
            }
        }

        if ($flush === true) {
            $this->entityManager->flush();
        }

    }

    /**
     * @param $str
     * @return bool
     */
    function isDateValid($str)
    {
        try {
            $dt = new \DateTime(trim($str));
        } catch (\Exception $e) {
            return false;
        }
        $month = $dt->format('m');
        $day = $dt->format('d');
        $year = $dt->format('Y');
        if (checkdate($month, $day, $year)) {
            return true;
        } else {
            return false;
        }
    }

    public function validateDate($date, $format = 'Y-m-d H:i')
    {
        $d = \DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

}