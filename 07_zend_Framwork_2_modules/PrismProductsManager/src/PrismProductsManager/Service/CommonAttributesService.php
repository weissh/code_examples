<?php

namespace PrismProductsManager\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectRepository as ObjectRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use PrismProductsManager\Entity\CommonAttributes;
use PrismProductsManager\Entity\CommonAttributesDefinitions;
use PrismProductsManager\Entity\CommonAttributesGuiRepresentation;
use PrismProductsManager\Entity\CommonAttributesMapping;
use PrismProductsManager\Entity\CommonAttributesValues;
use PrismProductsManager\Entity\CommonAttributesValuesAutoFromDate;
use PrismProductsManager\Entity\CommonAttributesValuesChain;
use PrismProductsManager\Entity\CommonAttributesValuesDefinitions;
use PrismProductsManager\Entity\CommonAttributesValuesEquivalence;
use PrismProductsManager\Entity\CommonAttributesValuesGuiRepresentation;
use PrismProductsManager\Entity\CommonAttributesValuesOverrides;
use PrismProductsManager\Model\CommonAttributesMapper;
use Zend\Db\Sql\Sql;

/**
 * Class CommonAttributesService
 *
 * @package PrismProductsManager\Service
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class CommonAttributesService
{
    /**
     * @var Array
     */
    protected $siteConfig;
    /**
     * @var Object Doctrine Entity Manager
     */
    protected $doctrineEntity;
    /**
     * @var CommonAttributesMapper
     */
    protected $commonAttributesMapper;

    /**
     * @var Int
     */
    protected $currentLanguage;

    /**
     * @var ObjectRepository
     */
    protected $commonAttributesEntityRepository;

    /**
     * @var ObjectRepository
     */
    protected $commonAttributesViewTypeEntityRepository;

    /**
     * @var CommonAttributes
     */
    protected $commonAttributeEntity;
    /**
     * @var CommonAttributesDefinitions
     */
    protected $commonAttributeDefinitionEntity;
    /**
     * @var CommonAttributesGuiRepresentation
     */
    protected $commonAttributeGUIRepresentationEntity;
    /**
     * @var CommonAttributesValues
     */
    protected $commonAttributeValuesEntity;
    /**
     * @var CommonAttributesValuesDefinitions
     */
    protected $commonAttributeValuesDefinitionEntity;
    /**
     * @var CommonAttributesValuesEquivalence
     */
    protected $commonAttributeValuesEquivalenceEntity;
    /**
     * @var CommonAttributesValuesGuiRepresentation
     */
    protected $commonAttributeValuesGuiRepresentationEntity;
    /**
     * @var ObjectRepository
     */
    protected $commonAttributesValuesEntityRepository;
    /**
     * @var ObjectRepository
     */
    protected $commonAttributesValuesGuiRepresentationRepository;

    /**
     * @var ObjectRepository
     */
    protected $commonAttributesValuesEquivalenceRepository;

    /**
     * @var CommonAttributesValuesChain
     */
    protected $commonAttributeValuesChain;

    /**
     * @var ObjectRepository
     */
    protected $commonAttributesValuesChainRepository;

    /**
     * @var ObjectRepository
     */
    protected $commonAttributesDefinitionsRepository;

    /**
     * @var ObjectRepository
     */
    protected $territoriesRepository;
    /**
     * @var \Zend\Db\Sql\Sql
     */
    protected $sql;

    /**
     * @var string
     */
    protected $territoriesTable = 'TERRITORIES';

    /**
     * @var CommonAttributesMapping
     */
    protected $commonAttributeMappingEntity;

    /**
     * @param                                         $siteConfig
     * @param EntityManager                           $doctrineEntity
     * @param CommonAttributesMapper                  $commonAttributesMapper
     * @param ObjectRepository                        $commonAttributesEntityRepository
     * @param ObjectRepository                        $commonAttributesViewTypeEntityRepository
     * @param CommonAttributes                        $commonAttributeEntity
     * @param CommonAttributesDefinitions             $commonAttributeDefinitionEntity
     * @param CommonAttributesGuiRepresentation       $commonAttributeGUIRepresentationEntity
     * @param CommonAttributesValues                  $commonAttributeValuesEntity
     * @param CommonAttributesValuesDefinitions       $commonAttributeValuesDefinitionEntity
     * @param CommonAttributesValuesDefinitions       $commonAttributeValuesDefinitionEntity
     * @param CommonAttributesValuesEquivalence       $commonAttributeValuesEquivalenceEntity
     * @param CommonAttributesValuesGuiRepresentation $commonAttributeValuesGuiRepresentationEntity
     * @param CommonAttributesMapping                 $commonAttributeMapping
     * @param ObjectRepository                        $commonAttributesValuesRepository
     * @param ObjectRepository                        $commonAttributesValuesGuiRepresentationRepository
     * @param ObjectRepository                        $commonAttributesValuesEquivalenceRepository
     * @param CommonAttributesValuesChain             $commonAttributeValuesChain
     * @param ObjectRepository                        $commonAttributesValuesChainRepository
     * @param ObjectRepository                        $commonAttributesDefinitionsRepository
     * @param ObjectRepository                        $territoriesRepository
     * @param Sql                                     $sql
     */
    public function __construct(
        $siteConfig,
        EntityManager $doctrineEntity,
        CommonAttributesMapper $commonAttributesMapper,
        ObjectRepository $commonAttributesEntityRepository,
        ObjectRepository $commonAttributesViewTypeEntityRepository,
        CommonAttributes $commonAttributeEntity,
        CommonAttributesDefinitions $commonAttributeDefinitionEntity,
        CommonAttributesGuiRepresentation $commonAttributeGUIRepresentationEntity,
        CommonAttributesValues $commonAttributeValuesEntity,
        CommonAttributesValuesDefinitions $commonAttributeValuesDefinitionEntity,
        CommonAttributesValuesEquivalence $commonAttributeValuesEquivalenceEntity,
        CommonAttributesValuesGuiRepresentation $commonAttributeValuesGuiRepresentationEntity,
        CommonAttributesMapping $commonAttributeMapping,
        ObjectRepository $commonAttributesValuesRepository,
        ObjectRepository $commonAttributesValuesGuiRepresentationRepository,
        ObjectRepository $commonAttributesValuesEquivalenceRepository,
        CommonAttributesValuesChain $commonAttributeValuesChain,
        ObjectRepository $commonAttributesValuesChainRepository,
        ObjectRepository $commonAttributesDefinitionsRepository,
        ObjectRepository $territoriesRepository,
        Sql $sql
    ) {
        $this->siteConfig = $siteConfig;
        $this->currentLanguage = $this->siteConfig['languages']['current-language'];
        $this->doctrineEntity = $doctrineEntity;
        $this->commonAttributesMapper = $commonAttributesMapper;
        $this->sql = $sql;
        /** Repositories */
        $this->commonAttributesEntityReposetory = $commonAttributesEntityRepository;
        $this->commonAttributesViewTypeEntityRepository = $commonAttributesViewTypeEntityRepository;
        $this->commonAttributesValuesEntityRepository = $commonAttributesValuesRepository;
        $this->commonAttributesValuesGuiRepresentationRepository = $commonAttributesValuesGuiRepresentationRepository;
        $this->commonAttributesValuesEquivalenceRepository = $commonAttributesValuesEquivalenceRepository;
        $this->commonAttributesValuesChainRepository = $commonAttributesValuesChainRepository;
        $this->commonAttributesDefinitionsRepository = $commonAttributesDefinitionsRepository;
        $this->territoriesRepository = $territoriesRepository;
        /** Common Attribute Entities */
        $this->commonAttributeEntity = $commonAttributeEntity;
        $this->commonAttributeDefinitionEntity = $commonAttributeDefinitionEntity;
        $this->commonAttributeGUIRepresentationEntity = $commonAttributeGUIRepresentationEntity;
        $this->commonAttributeMappingEntity = $commonAttributeMapping;
        /** Common Attribute Values Entities */
        $this->commonAttributeValuesEntity = $commonAttributeValuesEntity;
        $this->commonAttributeValuesDefinitionEntity = $commonAttributeValuesDefinitionEntity;
        $this->commonAttributeValuesEquivalenceEntity = $commonAttributeValuesEquivalenceEntity;
        $this->commonAttributeValuesGuiRepresentationEntity = $commonAttributeValuesGuiRepresentationEntity;
        $this->commonAttributeValuesChain = $commonAttributeValuesChain;
    }

    /**
     * Method used to retrieve common attributes based on params
     *
     * @param int    $page
     * @param int    $limit
     * @param string $search
     * @param        $attributeGroupId
     *
     * @return mixed
     */
    public function getCommonAttributes($page = 1, $limit = 10, $search = '', $attributeGroupId = null)
    {
        $offset = ($page == 0) ? 0 : ($page - 1) * $limit;

        return $this->commonAttributesEntityReposetory->getCommonAttributes($limit, $offset, $this->currentLanguage, $search, $attributeGroupId);
    }

    /**
     * @return mixed
     */
    public function getAllCommonAttributes()
    {
        return $this->commonAttributesEntityReposetory->getAllCommonAttributes();
    }

    /**
     * Get list of all common attribute the is set as size property
     * @return mixed
     */
    public function getAllSizePropertyCommonAttributes()
    {
        $isSizeProperty = $this->commonAttributesEntityReposetory->getAllSizePropertyCommonAttributes();
        $sizePropertyCommonAttributes = [];

        foreach ($isSizeProperty as $commonAttribute) {
            $sizePropertyCommonAttributes[$commonAttribute->getCommonattributeid()] =
                $commonAttribute->getCommonAttributeDefinitions()->getDescription();
        }

        return $sizePropertyCommonAttributes;
    }

    /**
     * @return mixed
     */
    public function getCascadeCommonAttributes()
    {
        return $this->commonAttributesEntityReposetory->getCascadeCommonAttributes();
    }

    /**
     * @param $commonAttributeValueDefinition
     *
     * @return object
     */
    public function getCommonAttributeByName($commonAttributeValueDefinition)
    {
        return $this->commonAttributesDefinitionsRepository->findOneBy(
            ['description' => $commonAttributeValueDefinition, 'languageid' => $this->currentLanguage]
        );
    }

    /**
     * @param $supplier
     * @param $supplierCode
     * @param $commonAttributeId
     * @param $commonAttributeDefinition
     *
     * @return bool
     * @throws \Exception
     */
    public function updateSupplierRecord($supplier, $supplierCode, $commonAttributeId, $commonAttributeDefinition)
    {
        if (!isset($supplier['active']) || !isset($supplier['supplierName'])) {
            return false;
        }

        $commonAttribute = $commonAttributeDefinition->getCommonAttributeId();
        $commonAttributeValues = $commonAttribute->getCommonAttributeValues();

        foreach ($commonAttributeValues as $commonAttributeValue) {
            $savedSupplierCode = $commonAttributeValue->getCommonAttributeValueAbbrev();

            if ($savedSupplierCode == $supplierCode && array_key_exists('supplierName', $supplier)) {
                try {
                    // Supplier matched - need to update
                    $commonAttributeValue->setEnabled($supplier['active']);
                    $this->doctrineEntity->persist($commonAttributeValue);
                    $commonAttributeValueDefinition = $commonAttributeValue->getCommonAttributeValuesDefinitions();
                    $commonAttributeValueDefinition->setValue($supplier['supplierName']);
                    $this->doctrineEntity->persist($commonAttributeValueDefinition);
                    $this->doctrineEntity->flush();

                    return true;
                } catch (\Exception $ex) {
                    throw $ex;
                }
            }
        }

        return false;
    }

    /**
     * @param $supplier
     * @param $supplierCode
     * @param $commonAttributeDefinition
     */
    public function insertNewSupplier($supplier, $supplierCode, $commonAttributeDefinition)
    {
        if (!isset($supplier['active']) || !isset($supplier['supplierName'])) {
            return false;
        }

        $commonAttributeId = $commonAttributeDefinition->getCommonAttributeId()->getCommonAttributeId();
        $sanitizedData = [
            'commonAttributeValueSkuCode'  => '',
            'commonAttributeValueAbbrev'   => $supplierCode,
            'commonAttributeValueBaseSize' => '',
            'value'                        => $supplier['supplierName'],
            'active'                       => $supplier['active'],
        ];
        $this->resetValueEntity();
        $this->createNewCommonAttributeValue($sanitizedData, $commonAttributeId);
    }

    /**
     * @param string $mappingCode
     * @param string $identifier
     * @param array  $data
     *
     * @throws \Doctrine\ORM\EntityNotFoundException
     * @throws \Exception
     */
    public function upsertCommandAttributeOptions($mappingCode, $identifier, array $data)
    {
        $camRepo = $this->doctrineEntity->getRepository(CommonAttributesMapping::class);
        /** @var \PrismProductsManager\Entity\CommonAttributesMapping $commonAttributeMapping */
        $commonAttributeMapping = $camRepo->findOneBy(['commonAttributeMapping' => $mappingCode]);

        if (!$commonAttributeMapping) {
            throw new EntityNotFoundException(sprintf("Common Attribute Mapping not found. Code = %s", $mappingCode));
        }

        $commonAttribute = $commonAttributeMapping->getCommonAttributeId();
        $commonAttributeId = $commonAttribute->getCommonattributeid();

        /** @var \PrismProductsManager\Entity\Repository\CommonAttributesValuesRepository $commonAttributesValueRepository */
        $commonAttributesValueRepository = $this->doctrineEntity->getRepository(CommonAttributesValues::class);

        $identifierValues = array_column($data, $identifier);

        $dataIndexByIdentifier = [];
        foreach ($data as $item) {
            $dataIndexByIdentifier[$item[$identifier]] = $item;
        }

        $commonAttributeValueFields = [
            CommonAttributesValues::FIELD_SKU_CODE,
            CommonAttributesValues::FIELD_ABBREV,
            CommonAttributesValues::FIELD_BASE_SIZE,
        ];

        $commonAttributeValueDefinitionField = 'value';

        /** @var \PrismProductsManager\Entity\CommonAttributesValues[] $commonAttributeValues */
        $commonAttributeValues = $commonAttributesValueRepository->findBy([
            'commonattributeid' => $commonAttributeId,
            $identifier         => $identifierValues,
        ]);

        $this->doctrineEntity->getConnection()->beginTransaction();
        try {
            $getIdentifierValueMethod = 'get' . ucfirst($identifier);
            foreach ($commonAttributeValues as $commonAttributeValue) {
                $identifierValue = $commonAttributeValue->$getIdentifierValueMethod();
                $commonAttributeValueData = $dataIndexByIdentifier[$identifierValue];
                foreach ($commonAttributeValueFields as $field) {
                    if (array_key_exists($field, $commonAttributeValueData)) {
                        $setMethod = 'set' . ucfirst($field);
                        $commonAttributeValue->$setMethod($commonAttributeValueData[$field]);
                    }
                }

                $commonAttributeValue->getCommonAttributeValuesDefinitions()
                    ->setValue($commonAttributeValueData[$commonAttributeValueDefinitionField]);

                unset($dataIndexByIdentifier[$identifierValue]);
            }

            foreach ($dataIndexByIdentifier as $insertData) {
                $commonAttributeValue = new CommonAttributesValues();
                $commonAttributeValue->setCommonattributeid($commonAttribute);
                $commonAttributeValue->setEnabled(true);
                foreach ($commonAttributeValueFields as $field) {
                    if (array_key_exists($field, $insertData)) {
                        $setMethod = 'set' . ucfirst($field);
                        $commonAttributeValue->$setMethod($insertData[$field]);
                    }
                }
                $commonAttributeValue->setTerritoryid(1);
                $commonAttributeValue->setModifiedat(new \DateTime());
                $commonAttributeValue->setCreatedat(new \DateTime());
                $commonAttributeValue->setModifiedby(0);

                $this->doctrineEntity->persist($commonAttributeValue);


                $commonAttributeValueDefinition = new CommonAttributesValuesDefinitions();
                $commonAttributeValueDefinition->setcommonAttributeValueId($commonAttributeValue);
                $commonAttributeValueDefinition->setLanguageid(1);
                $commonAttributeValueDefinition->setValue($insertData[$commonAttributeValueDefinitionField]);
                $commonAttributeValueDefinition->setCreatedat(new \DateTime());
                $commonAttributeValueDefinition->setModifiedat(new \DateTime());

                $this->doctrineEntity->persist($commonAttributeValueDefinition);


                $order = $commonAttributesValueRepository->getMaxOrder($commonAttributeId) + 1;

                $commonAttributeValueGuiRepresentation = new CommonAttributesValuesGuiRepresentation();
                $commonAttributeValueGuiRepresentation->setcommonAttributeValueId($commonAttributeValue);
                $commonAttributeValueGuiRepresentation->setCommonattributevalueorder($order);
                $commonAttributeValueGuiRepresentation->setPmsdisplay(1);
                $commonAttributeValueGuiRepresentation->setShopdisplay(1);
                $commonAttributeValueGuiRepresentation->setIsDefault(false);
                $commonAttributeValueGuiRepresentation->setPage('');
                $commonAttributeValueGuiRepresentation->setCreatedat(new \DateTime());
                $commonAttributeValueGuiRepresentation->setModifiedat(new \DateTime());
                $commonAttributeValueGuiRepresentation->setModifiedby(null);

                $this->doctrineEntity->persist($commonAttributeValueGuiRepresentation);
            }

            $this->doctrineEntity->flush();

            $this->doctrineEntity->getConnection()->commit();
        } catch (\Exception $e) {
            $this->doctrineEntity->getConnection()->rollBack();
            throw $e;
        }
    }

    /**
     * @param string $mappingCode
     * @param string $identifier
     * @param array  $identifierValues
     *
     * @throws \Doctrine\ORM\EntityNotFoundException
     * @throws \Exception
     */
    public function deleteCommonAttributeOption($mappingCode, $identifier, array $identifierValues)
    {
        $camRepo = $this->doctrineEntity->getRepository(CommonAttributesMapping::class);
        /** @var \PrismProductsManager\Entity\CommonAttributesMapping $commonAttributeMapping */
        $commonAttributeMapping = $camRepo->findOneBy(['commonAttributeMapping' => $mappingCode]);

        if (!$commonAttributeMapping) {
            throw new EntityNotFoundException(sprintf("Common Attribute Mapping not found. Code = %s", $mappingCode));
        }

        $commonAttribute = $commonAttributeMapping->getCommonAttributeId();
        $commonAttributeId = $commonAttribute->getCommonattributeid();

        /** @var \PrismProductsManager\Entity\Repository\CommonAttributesValuesRepository $commonAttributesValueRepository */
        $commonAttributesValueRepository = $this->doctrineEntity->getRepository(CommonAttributesValues::class);

        /** @var \PrismProductsManager\Entity\CommonAttributesValues[] $commonAttributeValues */
        $commonAttributeValues = $commonAttributesValueRepository->findBy([
            'commonattributeid' => $commonAttributeId,
            $identifier         => $identifierValues,
        ]);

        $this->doctrineEntity->getConnection()->beginTransaction();
        try {
            foreach ($commonAttributeValues as $commonAttributeValue) {
                $this->doctrineEntity->remove($commonAttributeValue->getCommonAttributeValuesDefinitions());
                $this->doctrineEntity->remove($commonAttributeValue->getCommonAttributeValuesGuiRepresentation());
                $this->doctrineEntity->remove($commonAttributeValue);
            }

            $this->doctrineEntity->flush();

            $this->doctrineEntity->getConnection()->commit();
        } catch (\Exception $e) {
            $this->doctrineEntity->getConnection()->rollBack();
            throw $e;
        }
    }

    /**
     * @param $attributeValue
     * @param $commonAttributeId
     *
     * @return bool
     */
    public function getCommonAttributeValueByName($attributeValue, $commonAttributeId)
    {
        $attributeValues = $this->commonAttributesValuesEntityRepository->findBy(['commonattributeid' => $commonAttributeId]);

        foreach ($attributeValues as $attributeValueEntity) {
            $valueDescription = $attributeValueEntity->getcommonAttributeValuesDefinitions()->getValue();
            if ($attributeValue == $valueDescription) {
                return $attributeValueEntity;
            }
        }

        return false;
    }

    /**
     * Retrieves common attributes display types and returns them as array
     *
     * @return array
     */
    public function getCommonAttributesDisplayType()
    {
        $commonAttributesViewTypes = $this->commonAttributesViewTypeEntityRepository->findAll();
        $commonAttributesViewTypesArray = [];
        foreach ($commonAttributesViewTypes as $commonAttributesViewType) {
            $commonAttributesViewTypesArray[$commonAttributesViewType->getCommonAttributeViewTypeId()] =
                $commonAttributesViewType->getCommonAttributeViewType();
        }

        return $commonAttributesViewTypesArray;
    }

    /**
     * Function used to create a new Common Attribute
     *
     * @param $sanitizedData
     *
     * @return bool
     */
    public function createNewCommonAttribute($sanitizedData)
    {
        try {
            /** Set Data in child entities */
            $this->hydrateCommonAttributesEntities($sanitizedData);

            if (trim($sanitizedData['mapping']) === '') {
                $this->commonAttributeEntity->setCommonAttributeMapping(null);
            }
            /** Save data */
            $this->doctrineEntity->persist($this->commonAttributeEntity);
            $this->doctrineEntity->flush();

            //relate common attribute to size property common attributes
            if($sanitizedData['isSize'] == '1'){
                $commonAttributeId = $this->commonAttributeEntity->getCommonattributeid();
                $this->commonAttributesMapper->createCommonAttributeSizePropertyRelation($commonAttributeId, $sanitizedData['commonAttributeSizeProperty']);
            }


            /** Insert new record has been successful */
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function createCommonAttributeSizePropertyRelation($commonAttributeId, $commonAttributeSizeProperty)
    {
        return $this->commonAttributesMapper->createCommonAttributeSizePropertyRelation($commonAttributeId, $commonAttributeSizeProperty);
    }

    /**
     * Function used to edit existing Common Attribute
     *
     * @param $sanitizedData
     *
     * @return bool
     */
    public function editCommonAttribute($sanitizedData)
    {
        try {
            /** Get common attribute entity from db */
            $this->commonAttributeEntity = $this->commonAttributesEntityReposetory->find($sanitizedData['commonAttributeId']);
            if (!is_null($this->commonAttributeEntity->getCommonAttributeGuiRepresentation())) {
                $this->commonAttributeGUIRepresentationEntity = $this->commonAttributeEntity->getCommonAttributeGuiRepresentation();
            }
            if (!is_null($this->commonAttributeEntity->getCommonAttributeDefinitions())) {
                $this->commonAttributeDefinitionEntity = $this->commonAttributeEntity->getCommonAttributeDefinitions();
            }

            /** Check if the chain value has been modified  */
            if ($sanitizedData['chained'] != $this->commonAttributeEntity->isChained()) {
                /** If the value has been modified check if the attribute values have chained values */
                foreach ($this->commonAttributeEntity->getCommonAttributesValues() as $value) {
                    foreach ($value->getCommonAttributesSourceValuesChain() as $chainValues) {
                        /** Delete all chain values */
                        $this->doctrineEntity->remove($chainValues);
                    }
                }
                $this->doctrineEntity->flush();
            }
            $commonAttributeMapping = $this->commonAttributeEntity->getCommonAttributeMapping();
            if (!empty($commonAttributeMapping)) {
                $this->doctrineEntity->remove($this->commonAttributeEntity->getCommonAttributeMapping());
                $this->doctrineEntity->flush($this->commonAttributeEntity->getCommonAttributeMapping());
            }

            /** Hydrate data */
            $this->hydrateCommonAttributesEntities($sanitizedData);

            if (trim($sanitizedData['mapping']) === '') {
                $this->commonAttributeEntity->setCommonAttributeMapping(null);
            }

            /** Save data */
            $this->doctrineEntity->persist($this->commonAttributeEntity);
            $this->doctrineEntity->flush();

            //manage size property common attribute
            $this->editCommonAttributeSizePropertyRelation($sanitizedData['commonAttributeId'], $sanitizedData['commonAttributeSizeProperty'], $sanitizedData['isSize']);

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function editCommonAttributeSizePropertyRelation($commonAttributeId, $commonAttributeSizeProperty, $isSize)
    {
        try {
            if($isSize == '0'){
                $this->commonAttributesMapper->deleteCommonAttributeSizePropertyRelation($commonAttributeId, null);
            }

            // all size properties was deleted
            if($isSize == '1' && empty($commonAttributeSizeProperty)){
                $this->commonAttributesMapper->deleteCommonAttributeSizePropertyRelation($commonAttributeId, null);
            }

            if($isSize == '1' && !empty($commonAttributeSizeProperty)){
                $currentRelation = $this->commonAttributesMapper->getCommonAttributesSizePropertyRelations($commonAttributeId);
                if(empty($currentRelation)){
                    $this->commonAttributesMapper->createCommonAttributeSizePropertyRelation($commonAttributeId, $commonAttributeSizeProperty);
                }else{
                    //relation to delete
                    $relationsToDelete = array_values(array_diff($currentRelation, $commonAttributeSizeProperty));
                    if(!empty($relationsToDelete)){
                        $this->commonAttributesMapper->deleteCommonAttributeSizePropertyRelation($commonAttributeId, $relationsToDelete);
                    }
                    //relation to add
                    $relationsToCreate = array_values(array_diff($commonAttributeSizeProperty, $currentRelation));
                    $this->commonAttributesMapper->createCommonAttributeSizePropertyRelation( $commonAttributeId, $relationsToCreate);
                }
            }

            return true;
        } catch (\Exception $ex) {
            return false;
        }
        return true;
    }

    /**
     * @param $commonAttributeValues
     *
     * @return bool
     */
    public function checkIfValuesHaveDefault($commonAttributeValues)
    {
        $hasDefaultValue = false;

        foreach ($commonAttributeValues as $commonAttributeValue) {
            $hasDefaultValue = $commonAttributeValue->getcommonAttributeValuesGuiRepresentation()->getIsDefault();
            if ($hasDefaultValue) {
                break;
            }
        }

        return $hasDefaultValue;
    }

    /**
     * @param $setDefaultValue
     *
     * @return bool
     */
    public function setValuesAsDefault($setDefaultValue)
    {
        try {
            $commonAttributesValueEntity = $this->getCommonAttributeValueById($setDefaultValue);

            if (!empty($commonAttributesValueEntity)) {
                $commonAttributesValueEntity->getcommonAttributeValuesGuiRepresentation()->setisDefault(true);
                $this->doctrineFlush($commonAttributesValueEntity->getcommonAttributeValuesGuiRepresentation());
            }

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $commonAttValueId
     *
     * @return bool
     */
    public function isValuesDefault($commonAttValueId)
    {
        try {
            $commonAttributesValueEntity = $this->getCommonAttributeValueById($commonAttValueId);

            if (!empty($commonAttributesValueEntity)) {
                return $commonAttributesValueEntity->getcommonAttributeValuesGuiRepresentation()->getIsDefault();
            }
            else {
                return false;
            }
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $commonAttributeValueId
     */
    public function getTimeIntervalForCommonAttributeValue($commonAttributeValueId)
    {
        $this->sql->setTable('COMMON_ATTRIBUTES_VALUES_Time_Interval');
        $select = $this->sql->select()
            ->where('commonAttributeValueId = ' . $commonAttributeValueId)
            ->limit(1);
        $statement = $this->sql->prepareStatementForSqlObject($select);

        return $statement->execute()->next();
    }

    /**
     * @param $commonAttributeValueId
     */
    public function getAutoStartDateForCommonAttributeValue($commonAttributeValueId)
    {
        $this->sql->setTable('COMMON_ATTRIBUTES_VALUES_Auto_From_Date');
        $select = $this->sql->select()
            ->where('commonAttributeValueId = ' . $commonAttributeValueId)
            ->limit(1);
        $statement = $this->sql->prepareStatementForSqlObject($select);

        return $statement->execute()->next();
    }

    /**
     * @param $startDate
     * @param $endDate
     * @param $commonAttributeValueId
     *
     * @return bool
     */
    public function saveTimePeriodForCommonAttributeValueId($startDate, $endDate, $commonAttributeValueId)
    {
        try {
            /** Delete old values */
            $this->sql->setTable('COMMON_ATTRIBUTES_VALUES_Time_Interval');
            $action = $this->sql->delete()
                ->where(['commonAttributeValueId' => $commonAttributeValueId]);
            $statement = $this->sql->prepareStatementForSqlObject($action);
            $statement->execute();
            /** Insert new values */
            $this->sql->setTable('COMMON_ATTRIBUTES_VALUES_Time_Interval');
            $action = $this->sql->insert();
            $action->values([
                'startDate'              => $startDate,
                'endDate'                => $endDate,
                'commonAttributeValueId' => $commonAttributeValueId,
            ]);
            $statement = $this->sql->prepareStatementForSqlObject($action);
            $statement->execute();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $fromDate
     * @param $commonAttributeValueId
     * @return bool
     */
    public function saveAutoDateFromForCommonAttributeValueId($fromDate, $commonAttributeValueId)
    {
        try {
            /** Delete old values */
            $this->sql->setTable('COMMON_ATTRIBUTES_VALUES_Auto_From_Date');
            $action = $this->sql->delete()
                ->where(['commonAttributeValueId' => $commonAttributeValueId]);
            $statement = $this->sql->prepareStatementForSqlObject($action);
            $statement->execute();
            /** Insert new values */
            $this->sql->setTable('COMMON_ATTRIBUTES_VALUES_Auto_From_Date');
            $action = $this->sql->insert();
            $action->values([
                'fromDate' => $fromDate,
                'commonAttributeValueId' => $commonAttributeValueId,
            ]);
            $statement = $this->sql->prepareStatementForSqlObject($action);
            $statement->execute();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Function used to return common attribute using id
     *
     * @param $commonAttributeId
     *
     * @return CommonAttributes
     */
    public function getCommonAttributeById($commonAttributeId)
    {
        return $this->commonAttributesEntityReposetory->find($commonAttributeId);
    }

    /**
     * Method used to delete a common attribute with all values
     *
     * @param $commonAttributeId
     *
     * @return bool
     */
    public function deleteCommonAttribute($commonAttributeId)
    {
        try {
            $this->commonAttributeEntity = $this->getCommonAttributeById($commonAttributeId);
            $this->commonAttributeValuesEntity = $this->commonAttributeEntity->getCommonAttributesValues();
            $this->commonAttributeGUIRepresentationEntity = $this->commonAttributeEntity->getCommonAttributeGuiRepresentation();
            $this->commonAttributeDefinitionEntity = $this->commonAttributeEntity->getCommonAttributeDefinitions();
            $this->commonAttributeMappingEntity = $this->commonAttributeEntity->getCommonAttributeMapping();

            /** Delete child entities from Common Attributes Values */
            if (count($this->commonAttributeValuesEntity) != 0) {
                foreach ($this->commonAttributeValuesEntity as $value) {
                    $this->deleteCommonAttributeValue($value);
                }
            }


            /** Delete child entities from Common Attributes */
            $guiRepresentationEntity = $this->getCommonAttributeById($commonAttributeId)->getCommonAttributeGuiRepresentation();
            if (!empty($guiRepresentationEntity)) {
                $this->doctrineEntity->remove($guiRepresentationEntity);
            }
            $commonAttributesDefinitionsEntity = $this->getCommonAttributeById($commonAttributeId)->getCommonAttributeDefinitions();
            if (!empty($commonAttributesDefinitionsEntity)) {
                $this->doctrineEntity->remove($commonAttributesDefinitionsEntity);
            }
            if (!empty($this->commonAttributeMappingEntity)) {
                $this->doctrineEntity->remove($this->commonAttributeMappingEntity);
            }

            $this->doctrineEntity->flush();
            /** Delete Common Attributes Entity */
            $this->doctrineEntity->remove($this->getCommonAttributeById($commonAttributeId));
            $this->doctrineEntity->flush();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param CommonAttributesValues $valuesEntity
     */
    public function deleteCommonAttributeValue(CommonAttributesValues $valuesEntity)
    {
        /** Retrieve Common Attributes Values child entities */
        $commonAttributeValuesGuiRepresentationEntity = $valuesEntity->getCommonAttributeValuesGuiRepresentation();
        $commonAttributeValuesDefinitionEntity = $valuesEntity->getCommonAttributeValuesDefinitions();
        $commonAttributeValuesEquivalenceEntity = $valuesEntity->getCommonAttributesValuesEquivalence();
        $commonAttributesSourceValuesChain = $valuesEntity->getcommonAttributesSourceValuesChain();
        $commonAttributesDestinationValuesChain = $valuesEntity->getcommonAttributesDestinationValuesChain();

        /** Delete Common Attributes Values child entities */
        if (!empty($commonAttributeValuesGuiRepresentationEntity)) {
            $this->doctrineEntity->remove($commonAttributeValuesGuiRepresentationEntity);
        }
        if (!empty($commonAttributeValuesDefinitionEntity)) {
            $this->doctrineEntity->remove($commonAttributeValuesDefinitionEntity);
        }

        foreach ($commonAttributesSourceValuesChain as $chainSourceValue) {
            $this->doctrineEntity->remove($chainSourceValue);
        }
        foreach ($commonAttributesDestinationValuesChain as $chainDestinationValue) {
            $this->doctrineEntity->remove($chainDestinationValue);
        }
        /** Loop equivalence values and remove them */
        foreach ($commonAttributeValuesEquivalenceEntity as $valueEquivalence) {
            $this->doctrineEntity->remove($valueEquivalence);
        }
        $this->doctrineEntity->flush();

        /** Remove current value entity */
        $this->doctrineEntity->remove($this->commonAttributesValuesEntityRepository->find($valuesEntity->getCommonAttributeValueId()));
        $this->doctrineEntity->flush();
    }

    /**
     * @param $commonAttributeValueId
     *
     * @return bool
     */
    public function deleteAttributeValue($commonAttributeValueId)
    {
        try {
            /** @var CommonAttributesValues */
            $commonAttributeValueEntity = $this->commonAttributesValuesEntityRepository->find($commonAttributeValueId);

            /** If the value is set up as default we need to set up a new value as default before deleting this one */
            if ($commonAttributeValueEntity->getCommonAttributeValuesGuiRepresentation()->getIsDefault()) {
                $commonAttributeValues = $this->commonAttributesValuesEntityRepository->getCommonAttributesValues(
                    $commonAttributeValueEntity->getCommonAttributeId()->getCommonAttributeId(),
                    $this->currentLanguage
                );
                foreach ($commonAttributeValues as $value) {
                    if ($value->getCommonAttributeValueId() != $commonAttributeValueEntity->getCommonAttributeValueId()) {
                        $value->getCommonAttributeValuesGuiRepresentation()->setIsDefault(1);
                        $this->doctrineEntity->persist($value);
                        $this->doctrineEntity->flush();
                        break;
                    }
                }
            }

            $this->deleteCommonAttributeValue($commonAttributeValueEntity);

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function getCommonAttributeIdFromMapping($mapping)
    {
        $repo = $this->doctrineEntity->getRepository('PrismProductsManager\Entity\CommonAttributesMapping');
        $commonAttributeMappingEntity = $repo->findOneBy(['commonAttributeMapping' => $mapping]);
        echo json_encode($commonAttributeMappingEntity);
        die('here2');
    }

    /**
     * Method used to create a new common attribute value
     *
     * @param array $sanitizedData
     * @param int   $commonAttributeId
     * @param int   $isDefault
     *
     * @return bool
     */
    public function createNewCommonAttributeValue($sanitizedData, $commonAttributeId, $isDefault = 0)
    {
        try {
            $this->hydrateCommonAttributeValuesEntities($sanitizedData, $commonAttributeId, $isDefault);
            $this->doctrineEntity->persist($this->commonAttributeValuesEntity);
            $this->doctrineEntity->flush();

            if(isset($sanitizedData['sizePropertyCommonAttributeCount']) && $sanitizedData['sizePropertyCommonAttributeCount'] > 0){
                $commonAttributeValueId = $this->commonAttributeValuesEntity->getcommonAttributeValueId();
                $this->commonAttributesMapper->createCommonAttributeValueSizePropertyRelation($commonAttributeId, $commonAttributeValueId, $sanitizedData);
            }

            /** Insert new record has been successful */
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Method used to edit common attribute values
     *
     * @param $sanitizedData
     *
     * @return bool
     */
    public function editNewCommonAttributeValue($sanitizedData, $commonAttributeId)
    {
        try {
            /** Retrieve entity based on id */
            $commonAttributeValuesEntity = $this->commonAttributesValuesEntityRepository->find($sanitizedData['commonAttributeValueId']);
            /** Hydrate entity with changed data */
            $commonAttributeValuesEntity->setCommonAttributeValueSkuCode($sanitizedData['commonAttributeValueSkuCode']);
            $commonAttributeValuesEntity->setCommonAttributeValueAbbrev($sanitizedData['commonAttributeValueAbbrev']);
            $commonAttributeValuesEntity->setCommonAttributeValueBaseSize($sanitizedData['commonAttributeValueBaseSize']);
            $commonAttributeValuesEntity->getCommonAttributeValuesDefinitions()->setValue($sanitizedData['value']);
            /** Save entity to database */
            $this->doctrineEntity->persist($commonAttributeValuesEntity);
            $this->doctrineEntity->flush();

            if($sanitizedData['sizePropertyCommonAttributeCount'] > 0){
                $this->commonAttributesMapper->editCommonAttributeValueSizePropertyRelation($commonAttributeId, $sanitizedData['commonAttributeValueId'], $sanitizedData);
            }


            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $commonAttributeValueId
     *
     * @return object
     */
    public function getCommonAttributeValueById($commonAttributeValueId)
    {
        return $this->commonAttributesValuesEntityRepository->find($commonAttributeValueId);
    }

    /**
     * Method used to set new order values to values
     *
     * @param $updateArray
     *
     * @return bool
     */
    public function recalculateCommonAttributesValuesOrder($updateArray)
    {
        try {
            /** Iterate orders, retrieve entity and save new order to entity */
            foreach ($updateArray as $orderEntry) {
                $commonAttributesGuiRepresentationEntity = $this->commonAttributesValuesEntityRepository->find($orderEntry['commonAttributeValueId']);
                $commonAttributesGuiRepresentationEntity->getCommonAttributeValuesGuiRepresentation()
                    ->setCommonAttributeValueOrder($orderEntry['order']);
                $this->doctrineEntity->persist($commonAttributesGuiRepresentationEntity);
            }
            $this->doctrineEntity->flush();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $commonAttributeId
     *
     * @return mixed
     */
    public function getCommonAttributeValues($commonAttributeId)
    {
        return $this->commonAttributesValuesEntityRepository->getCommonAttributesValues($commonAttributeId, $this->currentLanguage);
    }

    /**
     * @param $commonAttributeValueId
     * @param $oldCommonAttributeValueId
     *
     * @return bool
     */
    public function changeDefaultForCommonAttributeValue($commonAttributeValueId, $oldCommonAttributeValueId)
    {
        try {
            /** Retrieve entities for common attributes values */
            $oldCommonAttributeValueEntity = $this->commonAttributesValuesEntityRepository->find($oldCommonAttributeValueId);
            $commonAttributeValueEntity = $this->commonAttributesValuesEntityRepository->find($commonAttributeValueId);

            /** Set up common attributes entities with new default values */
            $oldCommonAttributeValueEntity->getCommonAttributeValuesGuiRepresentation()->setIsDefault(0);
            $commonAttributeValueEntity->getCommonAttributeValuesGuiRepresentation()->setIsDefault(1);
            /** Save updated entities */
            $this->doctrineEntity->persist($oldCommonAttributeValueEntity);
            $this->doctrineEntity->persist($commonAttributeValueEntity);
            $this->doctrineEntity->flush();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Method used to change enable status for a common attribute value
     *
     * @param $commonAttributeValueId
     * @param $newStatus
     *
     * @return bool
     */
    public function changeCommonAttributeStatus($commonAttributeValueId, $newStatus)
    {
        try {
            /** Retrieve common attribute value entity */
            $commonAttributeValueEntity = $this->commonAttributesValuesEntityRepository->find($commonAttributeValueId);
            /** Update status in entity */
            $commonAttributeValueEntity->setEnabled($newStatus);
            /** Save entity in db */
            $this->doctrineEntity->persist($commonAttributeValueEntity);
            $this->doctrineEntity->flush();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $uniqueGroupId
     *
     * @return array
     */
    public function getEquivalenceByGroup($uniqueGroupId)
    {
        return $this->commonAttributesValuesEquivalenceRepository->findBy(['uniquegroupid' => $uniqueGroupId]);
    }

    /**
     * @param array $equivalenceGroups
     *
     * @return array
     */
    public function getCommonAttributeValuesByEquivalenceGroups($equivalenceGroups = [])
    {
        $returnArray = [];
        foreach ($equivalenceGroups as $equivalenceEntity) {
            $tmp = $this->commonAttributesValuesEntityRepository->findBy(
                ['commonAttributeValueId' => $equivalenceEntity->getCommonAttributeValueId()->getCommonAttributeValueId()]
            );
            if (!empty($tmp[0])) {
                $returnArray[] = $tmp[0];
            }
        }

        return $returnArray;
    }

    /**
     * @param $commonAttributeValueId
     * @param $commonAttributeUniqueGroupId
     *
     * @return bool
     */
    public function removeCommonAttributeValueFromEquivalenceGroup($commonAttributeValueId, $commonAttributeUniqueGroupId)
    {
        try {
            $commonAttributeValueEquivalenceEntity = $this->commonAttributesValuesEquivalenceRepository->findOneBy(
                ['uniquegroupid' => $commonAttributeUniqueGroupId, 'commonAttributeValueId' => $commonAttributeValueId]
            );
            $this->doctrineEntity->remove($commonAttributeValueEquivalenceEntity);
            $this->doctrineEntity->flush();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $searchString
     *
     * @return mixed
     */
    public function searchThroughCommonAttributes($searchString)
    {
        $objectResult = $this->commonAttributesEntityReposetory->getCommonAttributes(10, 0, $this->currentLanguage, $searchString);
        $searchResult = [];
        foreach ($objectResult as $object) {
            $entry = [
                'commonAttributeId'          => $object->getCommonAttributeId(),
                'commonAttributeDescription' => $object->getCommonAttributeDefinitions()->getDescription(),
            ];
            $searchResult[] = $entry;
        }

        return $searchResult;
    }

    /**
     * @param $commonAttributeId
     * @param $searchString
     *
     * @return array
     */
    public function searchThroughCommonAttributesValues($commonAttributeId, $searchString)
    {
        $commonAttributesValuesEntities =
            $this->commonAttributesValuesEntityRepository->getCommonAttributesValues($commonAttributeId, $this->currentLanguage, $searchString);
        $searchResult = [];
        foreach ($commonAttributesValuesEntities as $object) {
            $entry = [
                'commonAttributeValueId'          => $object->getCommonAttributeValueId(),
                'commonAttributeValueDescription' => $object->getcommonattributeid()->getcommonAttributeDefinitions()->getDescription() . ' - '
                    . $object->getCommonAttributeValuesDefinitions()->getValue(),
            ];
            $searchResult[] = $entry;
        }

        return $searchResult;
    }

    /**
     * @param $commonAttributeValues
     *
     * @return bool
     */
    public function createNewCommonAttributeValueEquivalencesGroup($commonAttributeValues)
    {
        try {
            $lastGroup = $this->commonAttributesValuesEquivalenceRepository->findOneBy([], ['uniquegroupid' => 'DESC'], 1);
            $newGroup = $lastGroup->getUniqueGroupId() + 1;
            foreach ($commonAttributeValues as $commonAttributeValue) {
                $this->commonAttributeValuesEquivalenceEntity = new CommonAttributesValuesEquivalence();
                $this->commonAttributeValuesEquivalenceEntity->setUniqueGroupid($newGroup);
                $this->commonAttributeValuesEquivalenceEntity->setModifiedAt(new \DateTime('now'));
                $this->commonAttributeValuesEquivalenceEntity->setCreatedAt(new \DateTime('now'));
                $this->commonAttributeValuesEquivalenceEntity->setCommonAttributeValueId($this->getCommonAttributeValueById($commonAttributeValue));
                $this->commonAttributeValuesEquivalenceEntity->setModifiedBy(0);
                $this->doctrineEntity->persist($this->commonAttributeValuesEquivalenceEntity);
                $this->doctrineEntity->flush();
            }

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param array $criteria
     *
     * @return object
     */
    public function getCommonAttributeValueBy($criteria = [])
    {
        return $this->commonAttributesValuesEntityRepository->findOneBy($criteria);
    }

    /**
     * @param $destinationValueId
     * @param $sourceValueId
     *
     * @return bool
     */
    public function removeChain($destinationValueId, $sourceValueId)
    {
        try {
            $chainEntity = $this->commonAttributesValuesChainRepository->findOneBy(
                ['sourcevalueid' => $sourceValueId, 'destinationvalueid' => $destinationValueId]
            );
            $this->doctrineEntity->remove($chainEntity);
            $this->doctrineEntity->flush();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $destinationValueId
     * @param $sourceValueId
     *
     * @return bool
     */
    public function addNewChainValue($destinationValueId, $sourceValueId)
    {
        try {
            $chainEntity = $this->commonAttributesValuesChainRepository->findOneBy(
                ['sourcevalueid' => $sourceValueId, 'destinationvalueid' => $destinationValueId]
            );
            if (!is_null($chainEntity)) {
                throw new \Exception('Value is already linked');
            }
            $sourceValueEntity = $this->getCommonAttributeValueById($sourceValueId);
            $destinationValueEntity = $this->getCommonAttributeValueById($destinationValueId);

            $this->commonAttributeValuesChain->setSourceValueId($sourceValueEntity);
            $this->commonAttributeValuesChain->setDestinationValueId($destinationValueEntity);
            $this->commonAttributeValuesChain->setModifiedat(new \DateTime('now'));
            $this->commonAttributeValuesChain->setCreatedat(new \DateTime('now'));
            $this->commonAttributeValuesChain->setModifiedby(0);
            $this->doctrineEntity->persist($this->commonAttributeValuesChain);
            $this->doctrineEntity->flush();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $commonAttributeId
     *
     * @return object
     */
    public function getCommonAttribute($commonAttributeId)
    {
        return $this->commonAttributesEntityReposetory->findOneBy(['commonattributeid' => $commonAttributeId]);
    }

    /**
     * @param $commonAttributeId
     *
     * @return array
     */
    public function getCommonAttributeValuesById($commonAttributeId)
    {
        return $this->commonAttributesValuesEntityRepository->findBy(['commonattributeid' => $commonAttributeId]);
    }

    /**
     * @param       $attributeDescription
     * @param array $commonAttributes
     *
     * @return bool
     */
    public function getCommonAttributeValueAssignedToProduct($attributeDescription, $commonAttributes = [])
    {
        try {
            /** Retrieve common attribute entity */

            $this->commonAttributeValuesDefinitionEntity = $this->commonAttributesDefinitionsRepository->findOneBy(
                ['description' => $attributeDescription]
            );


            if (!is_null($this->commonAttributeValuesDefinitionEntity)) {
                $commonAttributeEntity = $this->commonAttributeValuesDefinitionEntity->getCommonAttributeId();


                if (!empty($commonAttributeEntity)) {
                    $commonAttributeId = $commonAttributeEntity->getCommonAttributeId();
                    foreach ($commonAttributes as $commonAttribute) {
                        if ($commonAttribute[0]['commonAttributeId'] == $commonAttributeId) {
                            return $commonAttribute[0]['commonAttributeValue'];
                        }
                    }
                }
            }


            return false;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getAllTerritories($onlyOverrides = false)
    {
        $this->sql->setTable($this->territoriesTable);
        $select = $this->sql->select();
        if ($onlyOverrides) {
            $select->where('overrides = 1');
        }
        $statement = $this->sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    /**
     * @param $commonAttributeEntity
     *
     * @return bool
     */
    public function deleteTerritoryOverrides($commonAttributeEntity)
    {
        try {
            foreach ($commonAttributeEntity->getcommonAttributesValuesOverrides() as $overrideEntity) {
                $this->doctrineEntity->remove($overrideEntity);
            }
            $this->doctrineEntity->flush();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $commonAttributeValueEntity
     * @param $formData
     * @param $territory
     *
     * @return bool
     */
    public function createNewTerritoryOverride($commonAttributeValueEntity, $formData, $territory)
    {
        try {
            if (!empty($formData['skuCode'][$territory]) && !empty($formData['value'][$territory])) {
                $territoryEntity = $this->territoriesRepository->findOneBy(['territoryid' => $territory]);
                $commonAttributesValuesOverride = new CommonAttributesValuesOverrides();
                $commonAttributesValuesOverride->setCommonattributevalue($formData['value'][$territory]);
                $commonAttributesValuesOverride->setCommonattributevalueskucode($formData['skuCode'][$territory]);
                $commonAttributesValuesOverride->setCommonattributevalueid($commonAttributeValueEntity);
                $commonAttributesValuesOverride->setTerritoryid($territoryEntity);
                $commonAttributesValuesOverride->setCreatedAt(new \DateTime('now'));
                $commonAttributesValuesOverride->setModifiedAt(new \DateTime('now'));

                $this->doctrineEntity->persist($commonAttributesValuesOverride);

                return true;
            }

            return false;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function doctrineFlush()
    {
        try {
            $this->doctrineEntity->flush();

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $attributeValueId
     *
     * @return bool
     */
    public function removeDefaultValueFromValue($attributeValueId)
    {
        try {
            $this->commonAttributeValuesEntity = $this->getCommonAttributeValueById($attributeValueId);
            $this->commonAttributeValuesGuiRepresentationEntity = $this->commonAttributeValuesEntity->getCommonAttributeValuesGuiRepresentation();
            $this->commonAttributeValuesGuiRepresentationEntity->setIsDefault(false);
            $this->doctrineEntity->persist($this->commonAttributeValuesGuiRepresentationEntity);
            $this->doctrineEntity->flush($this->commonAttributeValuesGuiRepresentationEntity);

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $commonAttributeValueId
     *
     * @return bool
     */
    public function getCommonAttributeValueCodeByValueId($commonAttributeValueId)
    {
        try {
            $commonAttributeValueEntity = $this->getCommonAttributeValueById($commonAttributeValueId);

            return $commonAttributeValueEntity->getCommonAttributeValueSkuCode();
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param $value
     * @param $commonAttributeId
     * @param $activeFlag
     *
     * @return bool
     */
    public function insertNewAttributeValue($value, $commonAttributeId, $activeFlag, $isDefault)
    {
        try {

            $sanitizedData = [
                'active'                       => $activeFlag,
                'commonAttributeValueSkuCode'  => '',
                'commonAttributeValueAbbrev'   => '',
                'commonAttributeValueBaseSize' => '',
                'value'                        => $value,
            ];
            $this->resetValueEntity();

            return $this->createNewCommonAttributeValue($sanitizedData, $commonAttributeId, $isDefault);
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     *
     * @param int $commonAttributeValueId
     *
     * @return boolean
     */
    public function getCommonAttributeValueByCommonAttributeId($commonAttributeValueId)
    {
        try {
            return $this->commonAttributesMapper->getCommonAttributeValueByCommonAttributeId($commonAttributeValueId);
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function isSameValueCrossOptions($commonAttributeId, $variantsIds, $commonAttributeIdValue, $commonAttributeIdDataType)
    {
        try {
            return $this->commonAttributesMapper->isSameValueCrossOptions($commonAttributeId, $variantsIds, $commonAttributeIdValue,
                $commonAttributeIdDataType);
        } catch (\Exception $ex) {
            return true;
        }
    }

    public function updateCommonAttbuteValueOnStyleLevel($productOrVariantId, $commonAttributeId, $commonAttributeIdValue, $productIdFrom = "PRODUCTS"
    ) {
        try {
            return $this->commonAttributesMapper->updateCommonAttbuteValueOnStyleLevel($productOrVariantId, $commonAttributeId,
                $commonAttributeIdValue, $productIdFrom);
        } catch (\Exception $ex) {
            return true;
        }
    }

    public function insertNewAttributeValueForProduct($productOrVariantId, $commonAttributeId, $commonAttributeIdValue, $productIdFrom = "PRODUCTS")
    {
        try {
            return $this->commonAttributesMapper->insertNewAttributeValueForProduct($productOrVariantId, $commonAttributeId, $commonAttributeIdValue,
                $productIdFrom);
        } catch (\Exception $ex) {
            return true;
        }
    }

    /**
     *
     */
    private function resetValueEntity()
    {
        $this->commonAttributeValuesDefinitionEntity = new CommonAttributesValuesDefinitions();
        $this->commonAttributeValuesGuiRepresentationEntity = new CommonAttributesValuesGuiRepresentation();
        $this->commonAttributeValuesEntity = new CommonAttributesValues();
    }

    /**
     * Function used to hydrate entities with data
     * Params is passed as reference into function
     *
     * @param Array $sanitizedData
     *
     * @return bool
     */
    private function hydrateCommonAttributesEntities($sanitizedData = [])
    {
        try {
            /** Common Attributes Definition Entity */
            if (!is_null($this->commonAttributeDefinitionEntity)) {
                $this->commonAttributeDefinitionEntity->setDescription($sanitizedData['description']);
                $this->commonAttributeDefinitionEntity->setLanguageId($this->currentLanguage);
                $this->commonAttributeDefinitionEntity->setCreatedAt(new \DateTime('now'));
                $this->commonAttributeDefinitionEntity->setCommonAttributeDataElement($sanitizedData['commonAttributeDataElement']);
                $this->commonAttributeDefinitionEntity->setModifiedAt(new \DateTime('now'));
                $this->commonAttributeDefinitionEntity->setModifiedBy(0);
                $this->commonAttributeDefinitionEntity->setCommonattributeId($this->commonAttributeEntity);
            }
            /** Common Attributes GUI Representation Entity */
            if (!is_null($this->commonAttributeGUIRepresentationEntity) && $sanitizedData['commonAttributeType']) {
                $this->commonAttributeGUIRepresentationEntity->setCommonAttributesViewTypeId(
                    $this->commonAttributesViewTypeEntityRepository->find($sanitizedData['commonAttributeType'])
                );
                $this->commonAttributeGUIRepresentationEntity->setPmsDisplay(1);
                $this->commonAttributeGUIRepresentationEntity->setShopdisplay(1);
                $this->commonAttributeGUIRepresentationEntity->setModifiedAt(new \DateTime('now'));
                $this->commonAttributeGUIRepresentationEntity->setCreatedAt(new \DateTime('now'));
                $this->commonAttributeGUIRepresentationEntity->setModifiedBy(0);
                $this->commonAttributeGUIRepresentationEntity->setCommonAttributesParentId($this->commonAttributeEntity);
                $this->commonAttributeGUIRepresentationEntity->setCommonattributeId($this->commonAttributeEntity);
            }
            /** Common Attributes Mapping Entity */
            if (!is_null($this->commonAttributeMappingEntity)) {
                $this->commonAttributeMappingEntity->setCommonAttributeMapping($sanitizedData['mapping']);
                $this->commonAttributeMappingEntity->setCommonAttributeId($this->commonAttributeEntity);
                $this->commonAttributeMappingEntity->setModifiedAt(new \DateTime('now'));
                $this->commonAttributeMappingEntity->setCreatedAt(new \DateTime('now'));
            }
            /** Set data in main commonAttributes entity */
            if (!is_null($this->commonAttributeEntity)) {
                $this->commonAttributeEntity->setCommonAttributeDefinitions($this->commonAttributeDefinitionEntity);
                $this->commonAttributeEntity->setCommonAttributeGuiRepresentation($this->commonAttributeGUIRepresentationEntity);
                $this->commonAttributeEntity->setCommonAttributeMapping($this->commonAttributeMappingEntity);
                $this->commonAttributeEntity->setUsedForfiltering($sanitizedData['usedForFiltering']);
                $this->commonAttributeEntity->setTicketByDefault($sanitizedData['isTicketByDefault']);
                $this->commonAttributeEntity->setUsedForMerchantCategories($sanitizedData['usedForMerchantCategories']);
                $this->commonAttributeEntity->setUsedForMerchantCategories($sanitizedData['usedForMerchantCategories']);
                $this->commonAttributeEntity->setIsSize($sanitizedData['isSize']);
                $this->commonAttributeEntity->setIsSizeProperty($sanitizedData['isSizeProperty']);
                $this->commonAttributeEntity->setChained($sanitizedData['chained']);
                $this->commonAttributeEntity->setPeriod($sanitizedData['period']);
                $this->commonAttributeEntity->setAutomaticFromDate($sanitizedData['autoFromDate']);
                $this->commonAttributeEntity->setKeepHistory($sanitizedData['keepHistory']);
                $this->commonAttributeEntity->setIsEditable($sanitizedData['isEditable']);
                $this->commonAttributeEntity->setEnabled(1);
                $this->commonAttributeEntity->setEditableOnLevel($sanitizedData['EditableOnLevel']);

                $this->commonAttributeEntity->setIscolour(0);
                $this->commonAttributeEntity->setModifiedAt(new \DateTime('now'));
                $this->commonAttributeEntity->setCreatedAt(new \DateTime('now'));
                $this->commonAttributeEntity->setModifiedBy('0');
            }

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param array $sanitizedData
     * @param int   $commonAttributeId
     * @param int   $isDefault
     *
     * @return bool
     */
    private function hydrateCommonAttributeValuesEntities($sanitizedData, $commonAttributeId, $isDefault = 0)
    {
        try {
            /** Common Attributes Values Definition Entity */
            if (!is_null($this->commonAttributeValuesDefinitionEntity)) {
                $this->commonAttributeValuesDefinitionEntity->setLanguageId($this->currentLanguage);
                $this->commonAttributeValuesDefinitionEntity->setValue($sanitizedData['value']);
                $this->commonAttributeValuesDefinitionEntity->setModifiedAt(new \DateTime('now'));
                $this->commonAttributeValuesDefinitionEntity->setCreatedAt(new \DateTime('now'));
                $this->commonAttributeValuesDefinitionEntity->setCommonAttributeValueId($this->commonAttributeValuesEntity);
            }
            /** Common Attributes Values GUI Representation Entity */
            $commonAttribute = $this->getCommonAttributeById($commonAttributeId);
            if (!is_null($this->commonAttributeValuesGuiRepresentationEntity)) {
                /** Setup new available order value */
                $latestOrder = 0;
                if ($isDefault) {
                    $isDefault = 1;
                    foreach ($commonAttribute->getCommonAttributesValues() as $value) {
                        if ($latestOrder < $value->getCommonAttributeValuesGuiRepresentation()->getCommonAttributeValueOrder()) {
                            $latestOrder = $value->getcommonAttributeValuesGuiRepresentation()->getCommonAttributeValueOrder();
                        }
                        if ($value->getcommonAttributeValuesGuiRepresentation()->getisDefault()) {
                            $isDefault = 0;
                        }
                    }
                }

                $newOrder = $latestOrder + 1;
                $this->commonAttributeValuesGuiRepresentationEntity->setCommonAttributeValueOrder($newOrder);
                $this->commonAttributeValuesGuiRepresentationEntity->setPmsDisplay(1);
                $this->commonAttributeValuesGuiRepresentationEntity->setShopDisplay(1);
                $this->commonAttributeValuesGuiRepresentationEntity->setIsDefault($isDefault);
                $this->commonAttributeValuesGuiRepresentationEntity->setPage('');
                $this->commonAttributeValuesGuiRepresentationEntity->setModifiedAt(new \DateTime('now'));
                $this->commonAttributeValuesGuiRepresentationEntity->setCreatedAt(new \DateTime('now'));
                $this->commonAttributeValuesGuiRepresentationEntity->setModifiedBy(0);
                $this->commonAttributeValuesGuiRepresentationEntity->setCommonAttributeValueId($this->commonAttributeValuesEntity);
            }
            /** Set data in main commonAttributes Values entity */
            if (!is_null($this->commonAttributeValuesEntity)) {
                $this->commonAttributeValuesEntity->setCommonAttributeValuesDefinitions($this->commonAttributeValuesDefinitionEntity);
                $this->commonAttributeValuesEntity->setCommonAttributeValuesGuiRepresentation($this->commonAttributeValuesGuiRepresentationEntity);
                $this->commonAttributeValuesEntity->setCommonAttributeId($commonAttribute);
                $active = isset($sanitizedData['active']) ? $sanitizedData['active'] : 1;
                $this->commonAttributeValuesEntity->setEnabled($active);
                $this->commonAttributeValuesEntity->setCommonAttributeValueSkuCode($sanitizedData['commonAttributeValueSkuCode']);
                $this->commonAttributeValuesEntity->setCommonAttributeValueAbbrev($sanitizedData['commonAttributeValueAbbrev']);
                $this->commonAttributeValuesEntity->setCommonAttributeValueBaseSize($sanitizedData['commonAttributeValueBaseSize']);
                $this->commonAttributeValuesEntity->setTerritoryId(1);
                $this->commonAttributeValuesEntity->setModifiedAt(new \DateTime('now'));
                $this->commonAttributeValuesEntity->setCreatedAt(new \DateTime('now'));
                $this->commonAttributeValuesEntity->setModifiedBy(0);
            }

            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function getAttributeTypeByProcessKey($process_key)
    {
        $result =  $this->commonAttributesEntityReposetory->getAttributeType($process_key);
        if ( ! empty($result[0]['commonAttributeGuiRepresentation']['commonattributesviewtypeid'])) {
            return $result[0]['commonAttributeGuiRepresentation']['commonattributesviewtypeid']['commonattributeviewtype'];
        }

        return false;
    }

    public function getCommonAttributeIdByProcessKey($process_key)
    {
        $result =  $this->commonAttributesEntityReposetory->getAttributeType($process_key);

        if ( ! empty($result[0]['commonattributeid'])) {
            return $result[0]['commonattributeid'];
        }

        return false;
    }

    /**
     * get list of size property common attributes linked to common attributes with IsSize flag
     * @param int $commonAttributeId
     */
    public function getSizePropertyCommonAttributesWithValues($commonAttributeId)
    {
        $list = [];
        $commonAttributesSizePropertyRelations = $this->getCommonAttributesSizePropertyRelations($commonAttributeId);
        foreach ($commonAttributesSizePropertyRelations as $value){
            $propertySizeCommonAttribute = $this->getCommonAttribute($value);
            $list[] = [
              'commonAttributeId' => $propertySizeCommonAttribute->getCommonattributeid(),
              'description' => $propertySizeCommonAttribute->getCommonAttributeDefinitions()->getDescription(),
              'commonAttributeVaules' => $this->getCommonAttributesValuesByCommonAtributeId($propertySizeCommonAttribute->getCommonattributeid())
            ];
        }
        return $list;
    }

    /**
     * Get the list of common attribute set as size property
     * @param $commonAttributeId
     * @return array
     */
    public function getCommonAttributesSizePropertyRelations($commonAttributeId)
    {
        return $this->commonAttributesMapper->getCommonAttributesSizePropertyRelations($commonAttributeId);
    }

    /**
     * Get list of common attributes set as size property with possible values (active)
     * @param $commonAttributeId
     * @return array
     */
    public function getCommonAttributesValuesByCommonAtributeId($commonAttributeId)
    {
        $list = [];
        $values = $this->getCommonAttributeValues($commonAttributeId);
        foreach ($values as $value){
            if($value->getEnabled() === true){
                $list[$value->getcommonAttributeValueId()] = $value->getCommonAttributeValuesDefinitions()->getValue();
            }
        }
        return $list;
    }

    /**
     * @param int $commonAttributeValueParentId
     * @return array
     */
    public function getCommonAttributeValueSizePropertyIdsByParnetValueId($commonAttributeValueParentId)
    {
        return $this->commonAttributesMapper->getCommonAttributeValueSizePropertyIdsByParnetValueId($commonAttributeValueParentId);
    }

    /**
     * @param $commonAttributeId
     * @param $fromCommonAttributeValueId
     * @param $toCommonAttributeValueId
     * @param CommonAttributesValues $toEntity
     * @return ArrayCollection
     */
    public function updateAllProductsCommonAttributeValues(
        $commonAttributeId,
        $fromCommonAttributeValueId,
        $toCommonAttributeValueId,
        CommonAttributesValues $toEntity)
    {
        return $this->commonAttributesMapper->updateAllProductsCommonAttributeValues(
            $commonAttributeId,
            $fromCommonAttributeValueId,
            $toCommonAttributeValueId,
            $toEntity);
    }
}
