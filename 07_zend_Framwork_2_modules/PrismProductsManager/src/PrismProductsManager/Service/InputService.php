<?php

namespace PrismProductsManager\Service;

use PrismProductsManager\Model\AttributesGroupMapper;
use PrismProductsManager\Model\CommonAttributesMapper;
use PrismProductsManager\Model\ProductsMapper;
use Zend\Db\Adapter\Adapter;
use PrismProductsManager\Model\ProductsEntity;
use PrismProductsManager\Model\ProductsMerchantCategoriesEntity;
use PrismProductsManager\Model\Entity\ProductsTabsEntity;
use PrismProductsManager\Form\MerchantCategorySelectForm;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Stdlib\Hydrator\ClassMethods;
use PrismProductsManager\Model\ProductsMapperVariants;
use PrismProductsManager\Model\ProductsMapperRelatedProducts;
use PrismProductsManager\Model\ProductsMapperSearchProducts;
use PrismProductsManager\Model\ProductsMapperActivateProduct;
use PrismProductsManager\Model\ProductsMapperAttributes;
use PrismProductsManager\Model\ProductsMapperPrices;
use PrismCommon\Adapter\CacheAdapter;

/**
 * Class InputService
 * @package PrismProductsManager\Service
 */
class InputService
{

    /**
     * @var
     */
    private $doctrineEntity;
    /**
     * @var
     */
    private $_sql;
    /**
     * @var CommonAttributesMapper
     */
    private $commonAttributeMaper;
    /**
     * @var Adapter
     */
    private $dbAdapter;
    /**
     * @var
     */
    protected $currentLanguage;
    /**
     * @var
     */
    protected $inputValidationRepository;
    /**
     * @var
     */
    protected $inputFieldRAttributesRepository;
    /**
     * @var
     */
    protected $inputFieldsRepository;
    /**
     * @var AttributesGroupMapper
     */
    protected $attributesGroupMapper;
    /**
     * @var
     */
    protected $inputFieldRWorkflows;

    protected $productMapper;

    protected $inputPagesRepository;

    /**
     * @var CacheAdapter
     */
    protected $cacheAdapter;


    /**
     * @param Adapter $dbAdapter
     * @param $siteConfig
     * @param $doctrineEntity
     * @param $sql
     * @param CommonAttributesMapper $commonAttributeMaper
     * @param AttributesGroupMapper $attributesGroupMapper
     */
    public function __construct(
        Adapter $dbAdapter,
        $siteConfig,
        $doctrineEntity,
        Sql $sql,
        CommonAttributesMapper $commonAttributeMaper,
        AttributesGroupMapper $attributesGroupMapper,
        CacheAdapter $cacheAdapter
    ) {

        $this->currentLanguage = $siteConfig['languages']['current-language'];
        $this->doctrineEntity = $doctrineEntity;
        $this->_sql = $sql;
        $this->commonAttributeMaper = $commonAttributeMaper;
        $this->attributesGroupMapper = $attributesGroupMapper;
        $this->cacheAdapter  = $cacheAdapter;
        $this->dbAdapter = $dbAdapter;

        $this->inputValidationTypesRepository = $this->doctrineEntity->getRepository('PrismProductsManager\Entity\InputValidationTypes');
        $this->inputFieldRAttributesRepository = $this->doctrineEntity->getRepository('PrismProductsManager\Entity\InputFieldRAttributes');
        $this->inputFieldsRepository = $this->doctrineEntity->getRepository('PrismProductsManager\Entity\InputFields');
        $this->commonAttributesValuesChainRepository = $this->doctrineEntity->getRepository('PrismProductsManager\Entity\CommonAttributesValuesChain');
        $this->commonAttributesRepository = $this->doctrineEntity->getRepository('PrismProductsManager\Entity\CommonAttributes');
        $this->commonAttributesValuesRepository = $this->doctrineEntity->getRepository('PrismProductsManager\Entity\CommonAttributesValues');
        $this->inputFieldRWorkflows = $this->doctrineEntity->getRepository('PrismProductsManager\Entity\InputFieldRWorkflows');
        $this->inputPagesRepository = $this->doctrineEntity->getRepository('PrismProductsManager\Entity\InputPages');
    }

    /**
     * @param $style
     * @return bool
     */
    public function getProductIdByStyleCode($style)
    {
        $this->_sql->setTable('PRODUCTS');
        $select = $this->_sql->select()
            ->where('style = "' . $style . '"')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();
        if (!empty($result['productId'])) {
            return $result['productId'];
        }
        return false;
    }

    /**
     * @param null $pageUrl
     * @param null $style
     * @param null $option
     * @param array $optionsData
     * @return array
     */
    public function getPageInputFields($pageUrl = null, $style = null, $option = null, $optionsData = array())
    {
        if (!is_null($pageUrl)) {
            $inputFieldsResponse = $this->inputPagesRepository->getAllPagesInputs($pageUrl);
        } else {
            $inputFieldsResponse = $this->inputPagesRepository->getAllPagesInputs();
        }

        $inputFieldsArrayReturn = array();
        foreach ($inputFieldsResponse as $inputFields) {
            $pageInputFields = $inputFields->getInputFieldRPages();

            foreach ($pageInputFields as $inputField) {
                /** $inputFieldsArrayReturn is being passed by reference */
                $this->getInputFieldArray($inputFields, $inputField, $inputFieldsArrayReturn, $style, $option, $optionsData);
            }
        }

        return $inputFieldsArrayReturn;
    }

    /**
     * @param $inputFieldId
     * @param $style
     * @param $allOptions
     * @return array
     */
    public function getDynamicArrayFields($inputFieldId, $style)
    {
        try {
            $inputFieldEntity = $this->inputFieldsRepository->find($inputFieldId);
            $dynamicArray = array();
            foreach ($inputFieldEntity->getinputFieldRPages() as $inputFieldRPages) {
                $this->getInputFieldArray(false, $inputFieldRPages, $dynamicArray, $style, null, array(), true);
            }

            return $dynamicArray;
        }
        catch (\Exception $ex) {
            return null;
        }
    }
    /**
     * @param $inputFields
     * @param $inputField
     * @param $inputFieldsArrayReturn
     * @param $style
     * @param $option
     * @param $optionsData
     */
    private function getInputFieldArray($inputFields, $inputField, &$inputFieldsArrayReturn, $style, $option, $optionsData, $justOne = false)
    {

        $skipInput = $this->checkIfFieldIsVisible($inputField, $style, $option);

        if ($skipInput) {
            return;
        }

        // Set enabled var
        $isFieldType = true;
        $inputType = $inputField->getInputfieldid()->getInputtypeid()->getInputfieldtypename();
        if (!empty($inputFields)) {
            $pageName = $inputFields->getPageName();
        } else {
            $pageName = 'no-page';
        }


        if ($inputField->getInputFieldsetsInputfieldsetid()) {
            $fieldSetId = $inputField->getInputFieldsetsInputfieldsetid()->getInputfieldsetname();
        } else {
            $fieldSetId = ' ';
        }

        // Get all TableFields
        // Find current enable input
        // Build array for the zf2 form generator

        switch ($isFieldType) {
            case $inputField->getInputfieldid()->getInputTablefieldsInputtablefieldid():


                $fieldType = $inputField->getInputfieldid()->getInputTablefieldsInputtablefieldid()->getType();
                $fieldId = $inputField->getInputFieldId()->getInputFieldId();
                $columnName = $inputField->getInputfieldid()->getInputTablefieldsInputtablefieldid()->getColumn();

                // Creat label from column name
                $regex = '/(?<!^)((?<![[:upper:]])[[:upper:]]|[[:upper:]](?![[:upper:]]))/';
                $label = preg_replace($regex, ' $1', $columnName);
                $label = ucfirst($label);

                switch ($label) {
                    case 'Style':
                        $label = 'Style Code';
                        break;
                    case 'Product Name':
                        $label = 'Style Name';
                        break;
                    case 'Short Product Name':
                        $label = 'Style Short Name';
                }
                // Build the elements array
                $element = array();
                $element['name'] = "inputfield[$fieldId]";
                $element['type'] = $fieldType;
                $element['label'] = $label;
                $element['fieldSetId'] = $fieldSetId;
                $element['inputType'] = $inputType;
                $element['inputFieldId'] = $inputField->getInputfieldid();

                /** Add validation to return array */
                if (!is_null($inputField->getInputfieldid()->getInputValidations())) {
                    $validationType = $this->inputValidationTypesRepository->findOneBy(
                        array('inputvalidationtypeid' => $inputField->getInputfieldid()->getInputValidations()->getInputValidationType()->getInputValidationTypeId())
                    );
                    $element['validations'] = array(
                        'type' => $validationType->getinputvalidationtype(),
                        'min' => $inputField->getInputfieldid()->getInputValidations()->getInputValidationMinValue(),
                        'max' => $inputField->getInputfieldid()->getInputValidations()->getInputValidationMaxValue(),
                        'pattern' => $inputField->getInputfieldid()->getInputValidations()->getPattern(),
                        'required' => $inputField->getInputfieldid()->getInputValidations()->isRequired()
                    );
                }
                // Build the rest of the array
                if ($justOne) {
                    $inputFieldsArrayReturn = $element;
                } else {
                    $inputFieldsArrayReturn[$pageName][$fieldSetId][$fieldId] = $element;
                }

                break;
            case $inputField->getInputfieldid()->getInputFieldRAttributesInputfieldrattributeid():

                switch ($inputField->getInputfieldid()->getInputFieldRAttributesInputfieldrattributeid()->getAttributeTable()) {
                    case 'ATTRIBUTES_Groups':
                        $attributeGroupId = $inputField->getInputfieldid()
                            ->getInputFieldRAttributesInputfieldrattributeid()
                            ->getattributeGroupId()
                            ->getattributegroupid();
                        $attributeGroupDetails = $this->attributesGroupMapper->fetchAllAttributeGroups($this->currentLanguage, $attributeGroupId);
                        $attributeGroupValues = $this->attributesGroupMapper->getAttributeValues($attributeGroupId, $this->currentLanguage, false);

                        if ($attributeGroupDetails['viewAs'] == 'SELECT') {
                            $attributeGroupValues[''] = '-- Select --';
                            asort($attributeGroupValues);
                        }

                        $fieldId = $inputField->getInputFieldId()->getInputFieldId();
                        $optionsHaveDifferentValues = $this->checkIfOptionsHaveDifferentValues($style, $option, $attributeGroupId, 'ATTRIBUTES_Groups', $optionsData, $inputField->getinputfieldid()->geteditableAtLevel());
                        $element = array(
                            'name' => "inputfield[$fieldId]",
                            'values' => $attributeGroupValues,
                            'values-chain' => array(),
                            'type' => strtolower($attributeGroupDetails['viewAs']),
                            'label' => $attributeGroupDetails['name'],
                            'fieldSetId' => $fieldSetId,
                            'inputType' => 'isAttribute',
                            'inputFieldId' => $inputField->getInputfieldid(),
                            'validations' => array(
                                'type' => ''
                            ),
                            'optionsHaveDifferentValues' => $optionsHaveDifferentValues
                        );
                        if ($optionsHaveDifferentValues) {
                            $element['name'] = 'button-' . $element['name'];
                        }
                        if ($justOne) {
                            $inputFieldsArrayReturn = $element;
                        } else {
                            $inputFieldsArrayReturn[$pageName][$fieldSetId][$fieldId] = $element;
                        }
                        break;
                    case 'COMMON_Attributes':
                    default:
                        $commonAttribute = $inputField->getInputfieldid()
                            ->getInputFieldRAttributesInputfieldrattributeid()
                            ->getcommonAttributes();
                        $commonAttributeId = $commonAttribute->getcommonattributeid();
                        $commonAttributeData = $this->commonAttributeMaper->getCommonAttributeViewType($commonAttributeId);

                        $fieldType = strtolower($commonAttributeData['commonAttributeViewType']);

                        // Generate field name
                        $fieldId = $inputField->getInputFieldId()->getInputFieldId();
                        // Get Label of attribute
                        $label = $commonAttributeData['description'];

                        $inputFieldId = $inputField->getInputfieldid();

                        // Build the elements array
                        $element = array();

                        if ($fieldType === "select" || $fieldType === "multiselect") {
                            // Get attribute values
                            $commonAttributeValuesRepository = $this->doctrineEntity->getRepository('PrismProductsManager\Entity\CommonAttributesValues');

                            $values = $commonAttributeValuesRepository->getCommonAttributesValues($commonAttributeId, $this->currentLanguage);

                            $destinationChainArray = array();
                            foreach ($values as $value) {
                                if ($value->getEnabled()) {
                                    $valueId = $value->getCommonAttributeValueId();

                                    // get the default value
                                    if($value->getCommonAttributeValuesGuiRepresentation()->getIsDefault()){
                                        $element['defaultValue'] = $valueId;
                                    }

                                    /** Built the array of input fields to update with values on select */
                                    foreach ($value->getcommonAttributesSourceValuesChain() as $chainedValue) {
                                        /** Iterate all chained values and retrieve the common attribute id for the chained value */
                                        $destinationValueEntity = $commonAttributeValuesRepository->findOneBy(
                                            array('commonAttributeValueId' => $chainedValue->getDestinationValueId()->getcommonAttributeValueId())
                                        );
                                        $destinationCommonAttributeId = $destinationValueEntity->getCommonAttributeId()->getCommonAttributeId();
                                        /** Retrieve the input fields where that common attribute is used */
                                        $inputFieldRAttributes = $this->inputFieldRAttributesRepository->findBy(
                                            array('attributetable' => 'COMMON_ATTRIBUTES', 'commonAttributeId' => $destinationCommonAttributeId)
                                        );
                                        foreach ($inputFieldRAttributes as $inputFieldRAttribute) {
                                            $inputFieldEntity = $this->inputFieldsRepository->findOneBy(
                                                array('inputFieldRAttributesInputfieldrattributeid' => $inputFieldRAttribute)
                                            );
                                            if (!empty($inputFieldEntity)) {
                                                $destinationChainArray[$chainedValue->getSourceValueId()->getCommonAttributeValueId()][$inputFieldEntity->getInputFieldId()][] = array(
                                                    'commonAttribute' => $destinationCommonAttributeId,
                                                    'valueAvailable' => $chainedValue->getDestinationValueId()->getCommonAttributeValueId()
                                                );
                                            }
                                        }
                                    }
                                    $abbrevCode = $value->getCommonAttributeValueAbbrev();

                                    $value = $value->getCommonAttributeValuesDefinitions()->getValue();
                                    if (!empty($abbrevCode)) {
                                        $value = $abbrevCode . ' - ' . $value;
                                    }

                                    $element['values'][ $valueId ] = html_entity_decode($value, ENT_QUOTES);
                                }

                            }
                            if ($fieldType == 'select') {
                                $selectArray = array('' => '-- Select --');
                                if (isset($element['values'])) {
                                    $element['values'] = $selectArray + $element['values'];
                                } else {
                                    $element['values'] = $selectArray;
                                }
                            }
                            $element['values-chain'] = $destinationChainArray;
                        }

                        $element['name'] = "inputfield[$fieldId]";
                        $element['type'] = $fieldType;
                        $element['label'] = $label;
                        $element['fieldSetId'] = $fieldSetId;
                        $element['inputFieldId'] = $inputFieldId;
                        $element['inputType'] = $inputType;
                        $element['tickedByDefault'] = $commonAttribute->isTicketByDefault();

                        $optionsHaveDifferentValues = $this->checkIfOptionsHaveDifferentValues($style, $option, $commonAttributeId, 'COMMON_Attributes', $optionsData, $inputField->getinputfieldid()->geteditableAtLevel());

                        if ($optionsHaveDifferentValues) {
                            $element['name'] = 'button-' . $element['name'];
                        }

                        $element['optionsHaveDifferentValues'] = $optionsHaveDifferentValues;

                        /** Add validation to return array */
                        $validationsArray = $inputField->getInputFieldId()->getInputValidations();

                        if (!empty($validationsArray)) {
                            $validationType = $this->inputValidationTypesRepository->findOneBy(
                                array(
                                    'inputvalidationtypeid' => $inputField->getInputFieldId()->getInputValidations()->getInputValidationType()->getInputValidationTypeId()
                                )
                            );
                            $element['validations'] = array(
                                'type' => $validationType->getinputvalidationtype(),
                                'min' => $inputField->getInputfieldid()->getInputValidations()->getInputValidationMinValue(),
                                'max' => $inputField->getInputfieldid()->getInputValidations()->getInputValidationMaxValue(),
                                'pattern' => $inputField->getInputfieldid()->getInputValidations()->getPattern(),
                                'required' => $inputField->getInputfieldid()->getInputValidations()->isRequired()
                            );
                        }

                        // Build the rest of the array
                        if ($justOne) {
                            $inputFieldsArrayReturn = $element;
                        } else {
                            $inputFieldsArrayReturn[$pageName][$fieldSetId][$fieldId] = $element;
                        }
                        break;
                }
                break;
            case $inputField->getInputFieldId()->getInputModulesInputModuleId():
                //case true:
                $inputFieldsArrayReturn[$pageName][$fieldSetId][$inputType] = $inputField->getInputfieldid()->getInputModulesInputmoduleid();
                break;
        }
    }

    public function changeKey( $array, $old_key, $new_key) {

        if( ! array_key_exists( $old_key, $array ) )
            return $array;

        $keys = array_keys( $array );
        $keys[ array_search( $old_key, $keys ) ] = $new_key;

        return array_combine( $keys, $array );
    }
    /**
     * @param string/null $style
     * @param string/null $option
     * @param integer $attributeId
     * @param string $attributesTable
     * @param array $optionsData
     * @param string $editableAtLevel
     * @return bool
     */
    private function checkIfOptionsHaveDifferentValues($style, $option, $attributeId, $attributesTable, $optionsData, $editableAtLevel)
    {
        $differentValuesFlag = false;
        //to show the blue button if the style level is different then get the main productId from the style
        $styleId = $this->getProductIdFromStyle($style);

        if (!empty($optionsData)) {
            $attributesForOptionsArray = array();
            if (!empty($style) && empty($option) && $editableAtLevel == 'OPTION') {
                /** Loop all options and retrieve data for the attribute */
                foreach ($optionsData as $optionCode => $variant) {
                    switch ($attributesTable) {
                        case 'ATTRIBUTES_Groups':
                            $attributesForOptionsArray[] = $this->attributesGroupMapper->getAttributeValueForProduct($attributeId, $variant['productAttributeId'], 1);
                            break;
                        case 'COMMON_Attributes':
                            $attributesForOptionsArray[] = $this->commonAttributeMaper->getAttributeValueForProduct($attributeId, $variant['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES');

                            $attributesForStyleArray[] = $this->commonAttributeMaper->getAttributeValueForProduct($attributeId, $styleId, 'PRODUCTS');

                            if(!in_array(null, $attributesForStyleArray, true)){
                                $attributesForOptionsArray= array_merge($attributesForOptionsArray, $attributesForStyleArray);
                            }
                            break;
                    }
                }
                /** Check if all values in array are the same */
                if (count(array_unique($attributesForOptionsArray)) !== 1) {
                    $differentValuesFlag = true;
                }
            }

        }
        return $differentValuesFlag;
    }
    /**
     * @param $inputField
     * @param $style
     * @param $option
     * @return bool
     */
    private function checkIfFieldIsVisible($inputField, $style, $option)
    {
        $skipInput = false;
        $inputFieldVisibilities = $inputField->getinputfieldid()->getinputFieldsVisibility();
        foreach ($inputFieldVisibilities as $inputFieldVisibility) {
            $targetCommonAttributeId = $inputFieldVisibility->getcommonAttribute()->getCommonAttributeId();
            $targetCommonAttributeValue = $this->commonAttributesValuesRepository->findOneBy(
                array('commonAttributeValueId' => $inputFieldVisibility->getcommonattributevalueid())
            );
            /** We are editing at style level and need to check product attributes */
            if (!is_null($style) && is_null($option)) {
                $productId = $this->getProductIdByStyleCode($style);
                if ($productId) {
                    $this->_sql->setTable('PRODUCTS_Common_Attributes');
                    $select = $this->_sql->select()
                        ->where('productOrVariantId = ' . $productId)
                        ->where('productIdFrom = "PRODUCTS"')
                        ->where('commonAttributeId = ' . $targetCommonAttributeId)
                        ->where('commonAttributeValue = ' . $targetCommonAttributeValue->getcommonAttributeValueId());
                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $result = $statement->execute()->next();
                    if (!empty($result)) {
                        $skipInput = true;
                    }
                }
            } elseif (!is_null($style) && !is_null($option)) {
                /** We are editing at option level and need to check product option attributes */
                $variantId = $this->getFirstVariantIdByOptionCode($option);
                if ($variantId) {
                    $this->_sql->setTable('PRODUCTS_Common_Attributes');
                    $select = $this->_sql->select()
                        ->where('productOrVariantId = ' . $variantId)
                        ->where('productIdFrom = "PRODUCTS_R_ATTRIBUTES"')
                        ->where('commonAttributeId = ' . $targetCommonAttributeId)
                        ->where('commonAttributeValue = ' . $targetCommonAttributeValue->getcommonAttributeValueId());
                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $result = $statement->execute()->next();
                    if (!empty($result)) {
                        $skipInput = true;
                    }
                }
            }
        }
        return $skipInput;
    }
    /**
     * @param $inputFieldId
     * @return bool
     */
    public function getAttributeIdForInputId($inputFieldId)
    {
        try {
            $inputFieldEntity = $this->getInputField($inputFieldId);
            $attributeTable = $inputFieldEntity->getinputFieldRAttributesInputfieldrattributeid()->getAttributeTable();
            switch ($attributeTable) {
                case 'COMMON_ATTRIBUTES':
                    return $inputFieldEntity->getinputFieldRAttributesInputfieldrattributeid()->getCommonAttributeId();
                    break;
                case 'ATTRIBUTES_Groups':
                    return $inputFieldEntity->getinputFieldRAttributesInputfieldrattributeid()->getAttributeGroupId();
                    break;
            }
            return false;
        } catch (\Exception $ex) {
            return false;
        }

    }

    /**
     * @return mixed
     */
    public function getAllPages()
    {
        // check if list cached in redis
        $inputPages = $this->cacheAdapter->getCache('listPageTabs', true);
        if(null !== $inputPages){
            return $inputPages;
        }

        $inputPages = $this->doctrineEntity->getRepository('PrismProductsManager\Entity\InputPages')->findBy(array(),array('pageOrder'=>'asc'));
        $this->cacheAdapter->setCache('listPageTabs', $inputPages);
        return $inputPages;
    }

    /**
     *
     */
    public function getInputTypes(){

    }

    /**
     *
     */
    public function assignPagesToFields()
    {
        $this->_sql->setTable(array('am' => 'attribute_migration'));
        $select = $this->_sql->select();
        $select->columns(array('*'));
        // $select->order('attributeName ASC');
        // $select->where->like('pd.makeLive', "$date%");
        // $select->where("pd.status = 'INACTIVE'");

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach($results as $res)
        {

            $data = array('inputPageId' => $res['pageId'],
                        'inputFieldId' => $res['InputFieldId'],
                        'INPUT_Fieldsets_inputFieldSetId' => $res['fieldSetId']
            );
            //var_dump($data);
            $this->addInputFieldRPages($data);
        }
    }


    /**
     *
     */
    public function migrateAttribtuesToInputFields()
    {

        $this->_sql->setTable(array('am' => 'attribute_migration'));
        $select = $this->_sql->select();
        $select->columns(array('*'));
        // $select->order('attributeName ASC');
        // $select->where->like('pd.makeLive', "$date%");
        // $select->where("pd.status = 'INACTIVE'");

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $duplicateRows = array();
        $existingRows = array();
        $usableRows = array();
        foreach($results as $res){

            if(in_array($res['attributeName'],$existingRows)){
                    array_push($duplicateRows,$res['attributeName']);
            } else {
                array_push($existingRows,$res['attributeName']);
                array_push($usableRows,$res);
            }




            // Insert
        }

        foreach($usableRows as $row){
            // Insert INPUT_Field_R_Attributes
            $data = array('attributeTable' => 'COMMON_ATTRIBUTES','attributeId' => $row['commonAttributeId']);
            $attributeId = $this->addInputFieldRAttribute($data);

            $data = array('inputTypeId' => 7,
                'INPUT_Field_R_Attributes_inputFieldRAttributeId' => $attributeId);
            $fielId = $this->addInputField($data);

            $this->updateAttributesTable($row['attributeName'],$fielId);

        }
    }

    /**
     * @param $data
     * @return int
     */
    public function addInputFieldRPages($data)
    {
        /** Insert data in $this->commonAttributesTable */
        $this->_sql->setTable('INPUT_Field_R_Pages');
        $action = $this->_sql->insert();
        $action->values($data);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
        return $this->dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
    }

    /**
     * @param $data
     * @return int
     */
    public function addInputFieldRAttribute($data)
    {
        /** Insert data in $this->commonAttributesTable */
        $this->_sql->setTable('INPUT_Field_R_Attributes');
        $action = $this->_sql->insert();
        $action->values($data);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
        return $this->dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
    }

    /**
     * @param $data
     * @return int
     */
    public function addInputField($data)
    {
        /** Insert data in $this->commonAttributesTable */
        $this->_sql->setTable('INPUT_Fields');
        $action = $this->_sql->insert();
        $action->values($data);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $statement->execute();
        return $this->dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
    }


    /**
     *
     */
    public function migrateAttributes()
    {

        $this->_sql->setTable(array('am' => 'attribute_migration'));
        $select = $this->_sql->select();
        $select->columns(array('*'));
       // $select->order('attributeName ASC');
       // $select->where->like('pd.makeLive', "$date%");
       // $select->where("pd.status = 'INACTIVE'");

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $uniqueAttribute = array();
        foreach($results as $res){

            $attributeValues = array();
            // Create COMMON_ATTRIBUTES value

            // Create Common attributes definition value

            // Get all unique attreibutes


            $values = $this->getAttributeValues($res['id']);

            foreach($values as $v){

                $attributeValues[] =  $v;
            }
            $uniqueAttribute[$res['id']] = array(
                        'name' => $res['attributeName'],
                        'id' => $res['id'] ,
                        'values' => $attributeValues,
                        'inputType' => $res['attributeTypeId'],);

            // Create common attribute gui r        epresentation
            //$this->commonAttributeMaper->addNewCommonAttribute();

            //var_dump(ucwords($res['attributeName']));
        }

        $duplicateAttributes = array();
        $attributes = array();
        // Find all duplicate attributes without values
        foreach($uniqueAttribute as $key => $values){
            if(in_array($values['name'],$attributes)){
                array_push($duplicateAttributes,$values['id']);
            } else {
                array_push($attributes,$values['name']);
            }
        }

        $finalUniqueAttributes = array();
        foreach($uniqueAttribute as $key => $value){
            if(in_array($value['id'],$duplicateAttributes) && empty($value['values'])){
               // var_dump($value);

            } else{
                if(empty($value['values'])){
                    $value['values'] = array(array('attributeValue' => 'N/A'));
                }
                array_push($finalUniqueAttributes,$value);
            }
        }

        foreach($finalUniqueAttributes as $values){
            //var_dump($values);
            $data = array();
            $data['isSize'] = 0;
            $data['isColour'] = 0;

            $attrName = strtolower($values['name']);

            if (strpos($attrName,'size') !== false) {
                $data['isSize'] = 1;

            }
            if (strpos($attrName,'colour') !== false) {
                $data['isColour'] = 1;
            }
            $data['commonAttributesViewTypeId'] = $values['inputType'];
            $data['description'] = $values['name'];

            $commonAttributeId = $this->commonAttributeMaper->addNewCommonAttribute($data);
            $order = 0;
            foreach($values['values'] as $attrValues){
                ++$order;
                $attrValueData['commonAttributeValueSkuCode'] = $values['id'];
                $attrValueData['value'] = $attrValues['attributeValue'];
                $attrValueData['commonAttributeValueOrder'] = $order;

                $this->commonAttributeMaper->addNewCommonAttributeValue($attrValueData,$commonAttributeId);
            }

            $this->updateAttributesTable($values['name'],$commonAttributeId);

        }

        //var_dump($finalUniqueAttributes);

    }

    /**
     * @param $attrName
     * @param $InputFieldId
     */
    public function updateAttributesTable($attrName,$InputFieldId)
    {
        $update = $this->_sql->update();
        $update->table('attribute_migration');
        $update->set(array('InputFieldId' => $InputFieldId));
        $update->where(array(
            'attributeName' => $attrName,
        ));
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();

    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAttributeValues($id)
    {
        $this->_sql->setTable(array('am' => 'attribute_values'));
        $select = $this->_sql->select();
        $select->columns(array('*'));
        $select->where(array('attributeId' => $id));
        // $select->order('attributeName ASC');
        // $select->where->like('pd.makeLive', "$date%");
        // $select->where("pd.status = 'INACTIVE'");

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Parse the form data and return the same form data without the fields that are editable at option level
     * @param $formData
     * @return mixed
     */
    public function getFieldsThatAreEditableAtStyleLevel($formData)
    {
        foreach ($formData as $inputFieldName => $inputFieldValue) {
            /** Check if the key is a input type field **/
            if (strpos($inputFieldName, 'inputfield[') !== false) {
                $inputFieldId = str_replace('inputfield[', '', $inputFieldName);
                $inputFieldId = str_replace(']', '', $inputFieldId);
                $inputFieldEntity = $this->getInputField($inputFieldId);
                if (!empty($inputFieldEntity)) {
                    $editableAtLevel = $inputFieldEntity->getEditableAtLevel();
                    if ($editableAtLevel != 'STYLE') {
                        unset($formData[$inputFieldName]);
                    }
                }

            }
        }
        return $formData;
    }

    /**
     * @param $inputFieldId
     * @return mixed
     */
    public function getInputField($inputFieldId)
    {
        return $this->inputFieldsRepository->findOneBy(array('inputfieldid' => $inputFieldId));
    }

    /**
     * @return array
     */
    public function getSizeBrides()
    {
        $returnArray = array();
        $sizeCommonAttributes = $this->commonAttributesRepository->findBy(array('issize' => 1));
        $commonAttributeValuesRepository = $this->doctrineEntity->getRepository('PrismProductsManager\Entity\CommonAttributesValues');
        foreach ($sizeCommonAttributes as $sizeCommonAttribute) {
            if ($sizeCommonAttribute->getUsedForMerchantCategories() ) {
                $commonAttributeId = $sizeCommonAttribute->getCommonAttributeId();
                $commonAttributesValues =  $commonAttributeValuesRepository->getCommonAttributesValues($commonAttributeId, $this->currentLanguage);

                $returnArray[$commonAttributeId]['values'] = $commonAttributesValues;
                $returnArray[$commonAttributeId]['commonAttribute'] = $sizeCommonAttribute;
            }
        }
        return $returnArray;
    }

    /**
     * @param $option
     * @return bool
     */
    public function getFirstVariantIdByOptionCode($option)
    {
        $this->_sql->setTable('PRODUCTS_R_ATTRIBUTES');
        $select = $this->_sql->select()
            ->where('sku LIKE "'.$option.'%"')
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();
        if (!empty($result['productAttributeId'])) {
            return $result['productAttributeId'];
        }
        return false;
    }

    /**
     * @param $nextWorkflowToValidate
     * @return array
     */
    public function getAllInputFieldsForWorkflow($nextWorkflowToValidate)
    {
        $inputFieldsRWorkflows = $this->inputFieldRWorkflows->findBy(array('workflowid' => $nextWorkflowToValidate));
        $inputFieldsEntityArray = array();
        foreach ($inputFieldsRWorkflows as $inputFieldRWorkflow) {
            $inputFieldsEntityArray[] = $inputFieldRWorkflow->getinputfieldid();
        }
        return $inputFieldsEntityArray;
    }


    /**
     * @param $inputFieldId
     * @return bool
     */
    public function getInputFieldLabelName($inputFieldId)
    {
        try {
            $inputFieldEntity = $this->inputFieldsRepository->find($inputFieldId);
            $attributeType = $inputFieldEntity->getinputFieldRAttributesInputFieldRAttributeId()->getAttributeTable();
            $fieldName = false;
            switch ($attributeType) {
                case 'ATTRIBUTES_Groups':
                    $fieldName = $inputFieldEntity->getInputFieldRAttributesInputFieldRAttributeId()
                        ->getAttributeGroupId()
                        ->getAttributeGroupDefinition()
                        ->getName();

                    break;
                case 'COMMON_ATTRIBUTES':
                    $fieldName = $inputFieldEntity->getinputFieldRAttributesInputFieldRAttributeId()
                                    ->getcommonAttributes()
                                    ->getcommonAttributeDefinitions()
                                    ->getdescription();
                    break;
            }
            return $fieldName;
        } catch (\Exception $ex) {
            return false;
        }

    }
    /**
     * get the product id from the style
     * @param string $style
     * @return int|boolean
     */
    public function getProductIdFromStyle($style)
    {
        $this->_sql->setTable('PRODUCTS');
        $select = $this->_sql->select()->where('style = "' . $style . '"');
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->next();
        if (! empty($result['productId'])) {
            return $result['productId'];
        }
        return false;
    }
}
