<?php

namespace PrismProductsManager\Service;

use PrismProductsManager\Model\ColoursMapper;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;

/**
 * Class ProductImportService
 * @package PrismProductsManager\Service
 */
class ProductImportService
{
    /**
     * @var string
     */
    private $errorsFolder = '/tmp/';
    /**
     * @var Adapter
     */
    private $pmsAdapter;

    /**
     * @var CommonAttributesService
     */
    private $commonAttributesService;

    /**
     * @var ColoursMapper
     */
    private $coloursMapper;
    /**
     * @var Sql
     */
    private $pmsSql;
    /**
     * @var bool
     */
    private $verbose = false;
    /**
     * @var array
     */
    private $errors = array();
    /**
     * @var array
     */
    private $spreadsheetHeaders = array();
    /**
     * @var array
     */
    private $spreadsheetCommonAttributesColumns = array();
    /**
     * @var array
     */
    private $baseSizesArray = array();
    /**
     * @var array
     */
    private $coloursArray = array();
    /**
     * @var array
     */
    private $suppliersArray = array();
    /**
     * @var array
     */
    private $currencies = array();
    /**
     * @var array
     */
    private $usedProductCodes = array();
    /**
     * @var array
     */
    private $commonAttributes = array();
    /**
     * @var array
     */
    private $linesOkToProcess = array();
    /**
     * @var array
     */
    private $channels = array();
    private $trimChars = " \t\n\r\0\x0B\xc2\xa0";
    /**
     * @var array
     */
    private $productsProcessed = array(
        'main_products_table' => array(),
        'definitions' => array(),
        'queue' => array(),
        'supplier_price' => array(),
        'price' => array(),
        'supplier' => array(),
        'common_attributes' => array(),
    );
    /**
     * @var array
     */
    private $priceColumnsMapping = array(
        // territory id
        1 => array(
            // currency id
            1 => array(
                'nowPrice' => 'UK Current Price (GBP)',
                'wasPrice' => 'UK Original Price (GBP)',
                'costPrice' => 'Landed Cost (GBP)',
            ),
            4 => array(
                'nowPrice' => 'GB Current Price (AUD)',
                'wasPrice' => 'GB Original Price (AUD)',
                'costPrice' => '',
            )
        ),
        2 => array(
            1 => array(
                'nowPrice' => 'ROW Current Price (GBP)',
                'wasPrice' => 'ROW Original Price (GBP)',
                'costPrice' => 'Landed Cost (GBP)',
            ),
        ),
        3 => array(
            3 => array(
                'nowPrice' => 'EU Current Price (EUR)',
                'wasPrice' => 'EU Original Price (EUR)',
                'costPrice' => '',
            ),
        ),
        4 => array(
            3 => array(
                'nowPrice' => 'DE Current Price (EUR)',
                'wasPrice' => 'DE Original Price (EUR)',
                'costPrice' => '',
            ),
        ),
        5 => array(
            2 => array(
                'nowPrice' => 'US Current Price (USD)',
                'wasPrice' => 'US Original Price (USD)',
                'costPrice' => 'Landed Cost (USD)',
            ),
        ),
        6 => array(
            5 => array(
                'nowPrice' => 'CA Current Price (CAD)',
                'wasPrice' => 'CA Original Price (CAD)',
                'costPrice' => 'Landed Cost (CAD)',
            ),
        ),
    );

    /**
     * @param Adapter $pmsAdapter
     * @param CommonAttributesService $commonAttributesService
     * @param ColoursMapper $coloursMapper
     */
    public function __construct(
        Adapter $pmsAdapter,
        CommonAttributesService $commonAttributesService,
        ColoursMapper $coloursMapper
    ) {
        $this->pmsAdapter = $pmsAdapter;
        $this->pmsSql = new Sql($pmsAdapter);
        $this->commonAttributesService = $commonAttributesService;
        $this->coloursMapper = $coloursMapper;
        $this->loadBaseSizesArray();
        $this->loadColoursArray();
        $this->loadSuppliersArray();
        $this->loadCurrenciesArray();
        $this->loadCommonAttributesArray();
        $this->loadChannelsArray();
    }

    /**
     * @param $verbose
     */
    public function verbose($verbose)
    {
        $this->verbose = $verbose;
    }

    /**
     * @param string $filePath
     * @return bool
     */
    public function validateExcelFile($filePath)
    {
        $success = true;

        $this->printMessage('Trying to read the file');

        $extension = array_pop(explode('.', $filePath));
        if ($extension != 'csv') {
            $this->setError("File {$filePath} is not a valid CSV file!");
            return false;
        }

        $errorFile = $this->errorsFolder . 'validation_errors_'.uniqid().'.csv';
        file_put_contents($errorFile, '"Row", "Column Name", "Error"'."\n", FILE_APPEND);

        $row = 1;
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            while (($columnArray = fgetcsv($handle, 0, ",", '"')) !== FALSE) {
                $this->printProgress('Processing row: '.$row);

                if ($row == 1) {
                    // header field
                    $column = 1;
                    foreach ($columnArray as $headerName => $headerValue) {
                        if (!empty($headerValue)) {
                            $this->spreadsheetHeaders[$column] = $headerValue;
                        } else {
                            $this->setError("Header for column no {$column} cannot be empty!");
                            return false;
                        }
                        $column++;
                    }
                    $this->checkIfMandatoryColumnsExists();
                } else {
                    // data row
                    $newColumnArray = array();
                    if (count($columnArray) != count($this->spreadsheetHeaders)) {
                        $this->setError("ROW {$row}: Row count doesn't match the columns count");
                        continue;
                    }
                    foreach ($columnArray as $columnNo => $columnValue) {
                        $newColumnArray[$this->spreadsheetHeaders[$columnNo + 1]] = trim($columnValue, $this->trimChars);
                    }
                    $response = $this->validateSpreadSheetRow($row, $newColumnArray);

                    if ($response !== true) {
                        $success = false;
                        // response has errors
                        foreach ($response as $columnName => $errorsArray) {
                            foreach ($errorsArray as $errorMessage) {
                                file_put_contents($errorFile, '"'.$row.'", "'.$columnName.'", "'.$errorMessage.'"'."\n", FILE_APPEND);
                            }
                        }
                    } else {
                        $this->linesOkToProcess[] = $row;
                    }
                }
                $row++;
            }
            fclose($handle);
        }

        $this->printMessage('Processing row: '.$row);

        if (count($this->linesOkToProcess) > 0 && !$success) {
            return count($this->linesOkToProcess);
        } else if (!$success) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param string $filePath
     * @return bool
     */
    public function import($filePath)
    {
        $success = true;

        $errorFile = $this->errorsFolder . 'import_errors_'.uniqid().'.csv';
        file_put_contents($errorFile, '"Row", "Error"'."\n", FILE_APPEND);

        $row = 1;
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            while (($columnArray = fgetcsv($handle, 0, ",", '"')) !== FALSE) {
                $this->printProgress('Processing row: '.$row);

                if ($row == 1) {
                    // header field
                    $column = 1;
                    foreach ($columnArray as $headerName => $headerValue) {
                        if (!empty($headerValue)) {
                            $this->spreadsheetHeaders[$column] = $headerValue;
                        } else {
                            $this->setError("Header for column no {$column} cannot be empty!");
                            return false;
                        }
                        $column++;
                    }
                } else {
                    if (!in_array($row, $this->linesOkToProcess)) {
                        $this->printMessage('Skipping row: '.$row.'...');
                        $row++;
                        continue;
                    }
                    // data row
                    $newColumnArray = array();
                    foreach ($columnArray as $columnNo => $columnValue) {
                        if (isset($this->spreadsheetCommonAttributesColumns[strtoupper($this->spreadsheetHeaders[$columnNo + 1])])) {
                            $newColumnArray['attributes'][$this->spreadsheetHeaders[$columnNo + 1]] = trim($columnValue, $this->trimChars);
                        } else {
                            $newColumnArray[$this->spreadsheetHeaders[$columnNo + 1]] = trim($columnValue, $this->trimChars);
                        }
                    }
                    $response = $this->processSpreadsheetRow($newColumnArray);
                    if (!empty($response)) {
                        $success = false;
                        // response has errors
                        foreach ($response as $errorsArray) {
                            foreach ($errorsArray as $errorMessage) {
                                file_put_contents($errorFile, '"'.$row.'", "'.$errorMessage.'"'."\n", FILE_APPEND);
                            }
                        }
                    }
                }
                $row++;
            }
            fclose($handle);
        }

        $this->printMessage('Processing row: '.$row);

        return $success;
    }

    /**
     * @return array
     * @internal param $columnArray
     */
    public function checkIfMandatoryColumnsExists()
    {
        $errorArray = array();
        $fixedColumnsArray = array(
            'SKU', 'SpectrumId', 'Colour', 'Size Bridge', 'Base Size', 'Size Description', 'Size Code',
            'Style Code', 'Style Name', 'Style Short Name', 'Supplier', 'Barcode', 'Enabled', 'Trading Currency',
            'Trading Currency Cost', 'Landed Cost (GBP)', 'Landed Cost (USD)', 'Landed Cost (CAD)', 'UK Current Price (GBP)',
            'UK Original Price (GBP)', 'ROW Current Price (GBP)', 'ROW Original Price (GBP)', 'US Current Price (USD)',
            'US Original Price (USD)', 'CA Current Price (CAD)', 'CA Original Price (CAD)', 'DE Current Price (EUR)',
            'DE Original Price (EUR)', 'EU Current Price (EUR)', 'EU Original Price (EUR)', 'GB Current Price (AUD)',
            'GB Original Price (AUD)', 'SizeCodeMeta', 'SizeCodeRangeMeta'
        );

        foreach ($fixedColumnsArray as $fixedColumn) {
            $flag = false;
            foreach ($this->spreadsheetHeaders as $header) {
                if ($header == $fixedColumn) {
                    $flag = true;
                }
            }
            if (!$flag) {
                $this->setError("Column $fixedColumn is not defined in csv file");
            }
        }
        return $errorArray;
    }

    public function importColours()
    {
        $this->coloursMapper->importColours();
    }
    /**
     * @param $row
     * @param $columnArray
     * @return array|bool
     */
    private function validateSpreadSheetRow($row, $columnArray)
    {

        foreach ($columnArray as $columnName => $columnValue) {
            $columnValue = trim($columnValue, $this->trimChars);

            switch ($columnName) {
                case 'SKU':
                    // SKU
                    if (isset($this->usedProductCodes[$columnValue])) {
                        $errorMessages[$columnName][] = "SKU value already exists on line ".$this->usedProductCodes[$columnValue];
                    } else {
                        $this->usedProductCodes[$columnValue] = $row;
                    }
                    if (strlen($columnValue) > 14) {
                        $errorMessages[$columnName][] = "SKU value cannot exceed 14 characters";
                    }
                    if ($columnArray['Style Code'].$columnArray['Colour'].$columnArray['Size Code'] != str_replace(' ', '', $columnValue)) {
                        $errorMessages[$columnName][] = "SKU value must be equal to Style + Colour + Size";
                    }
                    if (!preg_match('/^[A-Za-z0-9\s]+$/', $columnValue)) {
                        $errorMessages[$columnName][] = "SKU can only contain letters and numbers";
                    }
                    break;
                case 'SpectrumId':
                    // spectrum id
                    break;
                case 'Colour':
                    // Colour
                    // add the colour to the $usedColoursArray which will be validated at the end
                    // we do this to reduce the number of queries to the DB
                    if (!isset($this->coloursArray[$columnValue])) {
                        $errorMessages[$columnName][] = "Colour {$columnValue} is not a valid colour code";
                    }
                    break;
                case 'SizeCodeMeta':
                case 'SizeCodeRangeMeta':
                case 'SIZEGROUP':
                case 'Size Bridge':
                    // Size Bridge
                    // check if size bridge with name exists
                    // check if the size description is found inside the size bridge
                    // check if the size code is used inside the size bridge
                    break;
                case 'Base Size':
                    // base size
                    $baseSize = $this->getBaseSize($columnArray, $columnValue);

                    if (!isset($this->baseSizesArray[$baseSize])) {
                        $errorMessages[$columnName][] = "Base size {$columnValue} is not defined and could not be lookup up by custom logic.";
                    }
                    break;
                case 'Size Description':
                case 'Size Code':
                    if (empty($columnValue)) {
                        $errorMessages[$columnName][] = "Field cannot be empty";
                    }
                    // Size Description
                    // Size Code
                    // Validation is done in the Size Bridge section
                    break;
                case 'Style Code':
                    // Style Code
                    if (strlen($columnValue) > 7) {
                        $errorMessages[$columnName][] = "Style value cannot exceed 7 characters";
                    }
                    if (!preg_match('/^[A-Za-z0-9]+$/',$columnValue)) {
                        $errorMessages[$columnName][] = "Style can only contain letters and numbers";
                    }
                    break;
                case 'Style Name':
                case 'Style Short Name':
                    // Product Name and Short Name
                    if (strlen($columnValue) < 2) {
                        $errorMessages[$columnName][] = "The product name should be at least 2 characters long";
                    }
                    break;
                case 'Barcode':
                    // Barcode
                    if (!preg_match('/^[A-Za-z0-9]+$/',$columnValue)) {
                        $errorMessages[$columnName][] = "Barcode can only contain letters and numbers";
                    }
                    break;
                case 'Enabled':
                    // Enabled
                    if (!in_array($columnValue, array(0, 1))) {
                        $errorMessages[$columnName][] = "Enabled flag can only be 0 or 1";
                    }
                    break;
                case 'Trading Currency':
                    // Trading Currency
                    if (!isset($this->currencies[$columnValue])) {
                        $errorMessages[$columnName][] = "Currency {$columnValue} is not a valid currency";
                    }
                    break;
                case 'Trading Currency Cost':
                case 'Landed Cost (GBP)':
                case 'Landed Cost (USD)':
                case 'Landed Cost (CAD)':
                case 'UK Current Price (GBP)':
                case 'UK Original Price (GBP)':
                case 'ROW Current Price (GBP)':
                case 'ROW Original Price (GBP)':
                case 'US Current Price (USD)':
                case 'US Original Price (USD)':
                case 'CA Current Price (CAD)':
                case 'CA Original Price (CAD)':
                case 'DE Current Price (EUR)':
                case 'DE Original Price (EUR)':
                case 'EU Current Price (EUR)':
                case 'EU Original Price (EUR)':
                case 'GB Current Price (AUD)':
                case 'GB Original Price (AUD)':
                    // Prices
                    $columnValue = (float) $columnValue;
                    if ($columnValue < 0) {
                        $errorMessages[$columnName][] = "The price must be a positive decimal number or 0";
                    }
                    break;
                default:
                    // common attributes
                    // get common attribute type
                    if ($columnValue != '') {
                        // ignore empty columns
                        $commonAttributeName = strtoupper($columnName);
                        if (isset($this->commonAttributes[$commonAttributeName])) {
                            $this->spreadsheetCommonAttributesColumns[$commonAttributeName] = 1;
                            switch ($this->commonAttributes[$commonAttributeName]['commonAttributeViewType']) {
                                case 'CHECKBOX':
                                    if (empty($columnValue)) {
                                        $columnValue = 0;
                                    }
                                    // Enabled
                                    if (!in_array($columnValue, array(0, 1))) {
                                        $errorMessages[$columnName][] = "Field value can only be 0 or 1";
                                    }
                                    break;
                                case 'SELECT':
                                case 'MULTISELECT':
                                    if (empty($columnValue)) {
                                        break;
                                    }
                                    if ($this->commonAttributes[$commonAttributeName]['commonAttributeViewType'] == 'SELECT') {
                                        $valuesToCompare = array($columnValue);
                                    } else {
                                        $valuesToCompare = explode(',', $columnValue);
                                    }

                                    foreach ($valuesToCompare as $value) {
                                        /**
                                         *  $firstValueStripping
                                         *      - Trim special chars ( space chars ) from beginning and ending of the string
                                         *      - Decode html entities using utf 8
                                         *   $secondValueProcessing
                                         *      - Trim special chars ( space chars ) again
                                         *   $valueUpgraded
                                         *      - Setting value to uppercase
                                         */
                                        $firstValueProcessing = html_entity_decode(trim($value, $this->trimChars) , ENT_QUOTES, 'UTF-8');
                                        $secondValueProcessing = trim($firstValueProcessing, $this->trimChars);
                                        $valueUpgraded = strtoupper($secondValueProcessing);

                                        if (!in_array($valueUpgraded, $this->commonAttributes[$commonAttributeName]['values'])) {
                                            /** Insert new value to the common attribute - existing value cannot be found */
                                            $commonAttributeId = $this->commonAttributes[$commonAttributeName]['commonAttributeId'];

                                          
                                            $insertValueFlag = false;
                                            if (strlen($valueUpgraded) < 250) {
                                                $insertValueFlag = $this->commonAttributesService->insertNewAttributeValue($valueUpgraded, $commonAttributeId, 0, 0);
                                                $this->loadCommonAttributesArray();
                                            }

                                            if (!$insertValueFlag) {
                                                $errorMessages[$columnName][] = "Value {$valueUpgraded} not found in list (and could not be inserted) for common attribute ({$commonAttributeName})";
                                            }
                                        }
                                    }
                                    break;
                                case 'DATEPICKER':
                                    if (empty($columnValue)) {
                                        break;
                                    }
                                    $fieldValue = explode(' ', $columnValue);
                                    $dateSection = $fieldValue[0];
                                    $timeSection = $fieldValue[1];

                                    $dateArray = explode('-', $dateSection);
                                    $timeArray = explode(':', $timeSection);

                                    $strtotimeDate = date('Y-m-d H:i:s', strtotime($dateArray[0] . '-' . $dateArray[1] . '-' . $dateArray[2] . ' ' . $timeArray[0] . ':' . $timeArray[1] . ':' . $timeArray[2]));

                                    if ($strtotimeDate != $columnValue) {
                                        $errorMessages[$columnName][] = "Field should be a date in the following format YYYY-MM-DD HH:MM:SS";
                                    }
                                    break;
                                case 'DATEPICKER - DATE ONLY':
                                    if (empty($columnValue)) {
                                        break;
                                    }
                                    $dateArray = explode('-', $columnValue);

                                    if (isset($dateArray[0]) && isset($dateArray[1]) && isset($dateArray[2])) {
                                        $strtotimeDate = date('Y-m-d', strtotime($dateArray[0] . '-' . $dateArray[1] . '-' . $dateArray[2]));
                                        if ($strtotimeDate != $columnValue) {
                                            $errorMessages[$columnName][] = "Field should be a date in the following format YYYY-MM-DD";
                                        }
                                    }
                                    break;
                                case 'TEXT':
                                    break;
                                case 'TEXTAREA':
                                    break;
                                default:
                                    $errorMessages[$columnName][] = "Common attribute type {$this->commonAttributes[$commonAttributeName]['commonAttributeViewType']} does not exist";
                                    break;
                            }
                        } else {
                            $errorMessages[$columnName][] = "Common attribute does not exist";
                        }
                    }
                    break;
            }
        }

        if (!empty($errorMessages)) {
            return $errorMessages;
        }

        return true;
    }

    /**
     * @param $columnArray
     * @param $columnValue
     * @return string
     */
    private function getBaseSize($columnArray, $columnValue)
    {
        if (!isset($this->baseSizesArray[$columnValue])) {
            /** If the base Size cannot be found apply custom logic to determine base size */
            $sizeToLookBaseSize = isset($columnArray['Size Code']) ? trim($columnArray['Size Code'], $this->trimChars) : false;
            $brandToLookBaseSize = isset($columnArray['Brand']) ? trim($columnArray['Brand'], $this->trimChars) : false;
            $productTypeToLookBaseSize = isset($columnArray['ProductType']) ? trim($columnArray['ProductType'], $this->trimChars) : false;
            $sizeCodeRangeMeta = isset($columnArray['SizeCodeRangeMeta']) ? trim($columnArray['SizeCodeRangeMeta'], $this->trimChars) : false;

            return $this->applyCustomLogicToDetermineBaseSize(
                $sizeToLookBaseSize,
                $brandToLookBaseSize,
                $productTypeToLookBaseSize,
                $sizeCodeRangeMeta
            );
        }

        return $columnValue;
    }
    /**
     * @param $sizeToLookBaseSize
     * @param $brandToLookBaseSize
     * @param $productTypeToLookBaseSize
     * @param $sizeCodeRangeMeta
     * @return string
     */
    private function applyCustomLogicToDetermineBaseSize(
        $sizeToLookBaseSize,
        $brandToLookBaseSize,
        $productTypeToLookBaseSize,
        $sizeCodeRangeMeta
    )
    {
        $baseSize = '';

        if ($brandToLookBaseSize == 'LONG ELEGANT LEGS') {
            $baseSize .= 'e';
        } elseif($brandToLookBaseSize == 'TTYA X LONG TALL SALLY') {
            $baseSize .= 'y';
        } elseif ($productTypeToLookBaseSize == 'Footwear') {
            $baseSize .= 's';
        }

        /** Sizes S,M,L ( when not in range to XS and XL ) have base sizes S-StoL, M-StoL and L-StoL */
        if ($sizeCodeRangeMeta == 'SL' && ctype_alpha($sizeToLookBaseSize)) {
            $baseSize .= $sizeCodeRangeMeta . '-StoL';
        } else {
            $baseSize .= $sizeToLookBaseSize;
        }

        return $baseSize;
    }

    /**
     * @return string
     */
    public function truncateTablesForImport()
    {
        $query = 'SET FOREIGN_KEY_CHECKS = 0;
                    TRUNCATE PRODUCTS;
                    TRUNCATE PRODUCTS_ATTRIBUTES_Combination;
                    TRUNCATE PRODUCTS_ATTRIBUTES_Combination_Definitions;
                    TRUNCATE PRODUCTS_Barcodes;
                    TRUNCATE PRODUCTS_Common_Attributes;
                    TRUNCATE PRODUCTS_Cross_Border;
                    TRUNCATE PRODUCTS_Definitions;
                    TRUNCATE PRODUCTS_Locations;
                    TRUNCATE PRODUCTS_Media;
                    TRUNCATE PRODUCTS_Media_External;
                    TRUNCATE PRODUCTS_Merchant_Categories;
                    TRUNCATE PRODUCTS_Merchant_Categories_Definitions;
                    TRUNCATE PRODUCTS_Mis_Spells;
                    TRUNCATE PRODUCTS_Prices;
                    TRUNCATE PRODUCTS_R_ATTRIBUTES;
                    TRUNCATE PRODUCTS_R_ATTRIBUTES_R_SKU_Rules;
                    TRUNCATE PRODUCTS_R_CATEGORIES;
                    TRUNCATE PRODUCTS_R_CHANNELS;
                    TRUNCATE PRODUCTS_R_CLIENTS_Websites;
                    TRUNCATE PRODUCTS_R_PRODUCTS;
                    TRUNCATE PRODUCTS_R_SPECTRUM;
                    TRUNCATE PRODUCTS_R_SUPPLIERS;
                    TRUNCATE PRODUCTS_R_TAXES;
                    TRUNCATE PRODUCTS_R_WORKFLOWS;
                    TRUNCATE PRODUCTS_Supplier_Price;
                    SET FOREIGN_KEY_CHECKS = 1;';
        try {
            $this->pmsAdapter->query($query)->execute();
            return 'Tables truncated'. "\n";
        } catch (\Exception $ex) {
            return $ex->getMessage(). "\n";
        }
    }
    /**
     * @param $columnArray
     * @return bool|array
     */
    private function processSpreadsheetRow($columnArray)
    {
        $this->pmsAdapter->getDriver()->getConnection()->beginTransaction();

        list($productId, $variantId, $errorsArray) = $this->actionProductAndVariant($columnArray);

        if (empty($errorsArray)) {
            $errorsArray = $this->actionProductAndVariantDefinitions($productId, $variantId, $columnArray);
        }

        if (empty($errorsArray)) {
            $errorsArray = $this->actionProductSupplierPrices($productId, $variantId, $columnArray);
        }

        if (empty($errorsArray)) {
            $errorsArray = $this->actionProductPrices($productId, $variantId, $columnArray);
        }

        if (empty($errorsArray)) {
            //list($errorsArray) = $this->actionProductSuppliers($productId, $variantId, $columnArray);
        }

        if (empty($errorsArray)) {
            $errorsArray = $this->actionProductColour($variantId, $columnArray);
        }

        if (empty($errorsArray)) {
            $errorsArray = $this->actionProductSize($variantId, $columnArray);
        }

        if (empty($errorsArray)) {
            $errorsArray = $this->actionCommonAttributes($productId, $variantId, $columnArray);
        }

        if (!empty($errorsArray)) {
            $this->pmsAdapter->getDriver()->getConnection()->rollback();
        } else {
            $this->pmsAdapter->getDriver()->getConnection()->commit();
        }

        return $errorsArray;
    }

    /**
     * @param $columnArray
     * @return array
     */
    private function actionProductAndVariant($columnArray)
    {
        $errorsArray = array();
        $productId = false;
        $variantId = false;

        if (strlen($columnArray['Style Code']) < 6) {
            //$columnArray['Style Code'] = str_pad($columnArray['Style Code'], 6, ' ', STR_PAD_RIGHT);
        }
        try {
            //$variantId = $this->checkIfVariantExists($columnArray['SKU']);
        } catch (\Exception $e) {
            $this->setError($e->getMessage());
            $errorsArray[] = $e->getMessage();
        }

        if (empty($errorsArray)) {
            if (!$variantId) {
                // let's create the variant
                try {
                    $productId = $this->checkIfProductExists($columnArray['Style Code']);
                } catch (\Exception $e) {
                    $this->setError($e->getMessage());
                    $errorsArray[] = $e->getMessage();
                }

                if (empty($errorsArray)) {
                    if (!$productId && !in_array($columnArray['Style Code'], $this->productsProcessed['main_products_table'])) {
                        // create the product first
                        $insert = $this->pmsSql->insert('PRODUCTS')
                            ->values(array(
                                'productTypeId' => '1',
                                'productMerchantCategoryId' => null,
                                'ean13' => $columnArray['Barcode'],
                                'style' => $columnArray['Style Code'],
                                'sku' => $columnArray['SKU'],
                                'customizable' => '1',
                                'status' => 'ACTIVE',
                                'created' => date('Y-m-d H:i:s'),
                                'modified' => date('Y-m-d H:i:s')
                            ));
                        $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
                        $statement->execute();

                        $productId = $this->pmsAdapter->getDriver()->getLastGeneratedValue();

                        // add the channels for the product
                        $channelsInsertStatement = '';

                        foreach ($this->channels as $channelId) {
                            $channelsInsertStatement .= "('{$productId}', '{$channelId}', 'ACTIVE', NOW(), NOW()),";
                        }
                        if ($channelsInsertStatement !== '') {
                            $channelsInsertStatement = rtrim($channelsInsertStatement, ',');
                            $this->pmsAdapter->query("
                                INSERT INTO
                                `PRODUCTS_R_CHANNELS` (`productId`, `channelId`, `status`, `created`, `modified`)
                                VALUES {$channelsInsertStatement}
                            ")->execute();
                        }

                        if (!in_array($productId, $this->productsProcessed['queue'])) {
                            // add the product to the QUEUE MANAGER for spectrum sync
                            $insert = $this->pmsSql->insert('QUEUE_Manager')
                                ->values(array(
                                    'processed' => '0',
                                    'processing' => '0',
                                    'priority' => '0',
                                    'queueProcessId' => '0',
                                    'queueEntryType' => '8',
                                    'queueEntryId' => $productId,
                                    'queueEntryBody' => '{}',
                                    'queueReason' => '3',
                                    'output' => '',
                                    'createdAt' => date('Y-m-d H:i:s'),
                                    'modifiedAt' => date('Y-m-d H:i:s'),
                                ));
                            $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
                            $statement->execute();
                            $this->productsProcessed['queue'][] = $productId;
                        }

                        $this->productsProcessed['main_products_table'][] = $columnArray['Style Code'];
                    }

                    // create the variant now
                    $insert = $this->pmsSql->insert('PRODUCTS_R_ATTRIBUTES')
                        ->values(array(
                            'productId' => $productId,
                            'ean13' => $columnArray['Barcode'],
                            'sku' => $columnArray['SKU'],
                            'status' => ($columnArray['Enabled'] == 1) ? 'ACTIVE' : 'INACTIVE',
                            'created' => date('Y-m-d H:i:s'),
                            'modified' => date('Y-m-d H:i:s')
                        ));
                    $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
                    $statement->execute();

                    $variantId = $this->pmsAdapter->getDriver()->getLastGeneratedValue();

                    if (!empty($columnArray['SpectrumId'])) {
                        // add the entry to the Products Specturm table
                        $insert = $this->pmsSql->insert('PRODUCTS_R_SPECTRUM')
                            ->values(array(
                                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                                'productId' => $variantId,
                                'spectrumId' => $columnArray['SpectrumId'],
                                'created' => date('Y-m-d H:i:s'),
                                'modified' => date('Y-m-d H:i:s'),
                            ));
                        $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
                        $statement->execute();
                    }

                    // insert the barcode in the barcodes table
                    $insert = $this->pmsSql->insert('PRODUCTS_Barcodes')
                        ->values(array(
                            'barcode' => $columnArray['Barcode'],
                            'inUse' => 1,
                            'created' => date('Y-m-d H:i:s'),
                            'modified' => date('Y-m-d H:i:s')
                        ));
                    $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
                    $statement->execute();
                }
            } else {
                // update variant status
                $update = $this->pmsSql->update('PRODUCTS_R_ATTRIBUTES')
                    ->set(array(
                        'status' => ($columnArray['Enabled'] == 1) ? 'ACTIVE' : 'INACTIVE',
                    ))
                    ->where('productAttributeId = '.$variantId);
                $statement = $this->pmsSql->prepareStatementForSqlObject($update);
                $statement->execute();
            }
        }

        return array($productId, $variantId, $errorsArray);
    }

    /**
     * @param $productId
     * @param $variantId
     * @param $columnArray
     * @return array
     */
    private function actionProductAndVariantDefinitions($productId, $variantId, $columnArray)
    {
        $errorsArray = array();
        // check if the product definition exists
        if (!in_array($productId, $this->productsProcessed['definitions'])) {
            // check if the definition is set up for the product
            $select = $this->pmsSql->select('PRODUCTS_Definitions')
                ->where(array(
                    'productId' => $productId,
                    'productIdFrom' => 'PRODUCTS',
                    'languageId' => 1,
                ));

            $statement = $this->pmsSql->prepareStatementForSqlObject($select);
            $result = $statement->execute();

            if ($result->count() == 0) {
                // we need to add a description for this product
                $insert = $this->pmsSql->insert('PRODUCTS_Definitions')
                    ->values(array(
                        'productId' => $productId,
                        'productIdFrom' => 'PRODUCTS',
                        'languageId' => '1',
                        'versionId' => '1',
                        'shortProductName' => $columnArray['Style Short Name'],
                        'productName' => $columnArray['Style Name'],
                        'status' => 'ACTIVE',
                        'created' => date('Y-m-d H:i:s'),
                        'modified' => date('Y-m-d H:i:s')
                    ));
                $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
                $statement->execute();
            }
            $this->productsProcessed['definitions'][] = $productId;
        }

        // insert or update the variants definitions
        /*$select = $this->pmsSql->select('PRODUCTS_Definitions')
            ->where(array(
                'productId' => $variantId,
                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                'languageId' => 1,
            ));

        $statement = $this->pmsSql->prepareStatementForSqlObject($select);
        $result = $statement->execute();*/

        if (true) {//$result->count() == 0) {
            // insert new definition
            $insert = $this->pmsSql->insert('PRODUCTS_Definitions')
                ->values(array(
                    'productId' => $variantId,
                    'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                    'languageId' => '1',
                    'versionId' => '1',
                    'shortProductName' => $columnArray['Style Short Name'],
                    'productName' => $columnArray['Style Name'],
                    'status' => 'ACTIVE',
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ));
            $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
            $statement->execute();
        } else {
            // update existing definition
            // update variant status
            $productDefinitions = array();//$result->next();
            $productDefinitionsId = $productDefinitions['productDefinitionsId'];
            $update = $this->pmsSql->update('PRODUCTS_Definitions')
                ->set(array(
                    'shortProductName' => $columnArray['Style Short Name'],
                    'productName' => $columnArray['Style Name'],
                ))
                ->where('productDefinitionsId = '.$productDefinitionsId);
            $statement = $this->pmsSql->prepareStatementForSqlObject($update);
            $statement->execute();
        }

        return $errorsArray;
    }

    /**
     * @param $productId
     * @param $variantId
     * @param $columnArray
     * @return array
     */
    private function actionProductSupplierPrices($productId, $variantId, $columnArray)
    {
        $errorsArray = array();

        if (!in_array($productId, $this->productsProcessed['supplier_price'])) {
            // check if the product has supplier prices
            $select = $this->pmsSql->select('PRODUCTS_Supplier_Price')
                ->where(array(
                    'productOrVariantId' => $productId,
                    'productIdFrom' => 'PRODUCTS',
                ));

            $statement = $this->pmsSql->prepareStatementForSqlObject($select);
            $result = $statement->execute();

            if ($result->count() == 0) {
                $insert = $this->pmsSql->insert('PRODUCTS_Supplier_Price')
                    ->values(array(
                        'productOrVariantId' => $productId,
                        'productIdFrom' => 'PRODUCTS',
                        'currencyCode' => $columnArray['Trading Currency'],
                        'price' => $columnArray['Trading Currency Cost'],
                        'created' => date('Y-m-d H:i:s'),
                        'modified' => date('Y-m-d H:i:s')
                    ));
                $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
                $statement->execute();
            }
            $this->productsProcessed['supplier_price'][] = $productId;
        }

        /*$select = $this->pmsSql->select('PRODUCTS_Supplier_Price')
            ->where(array(
                'productOrVariantId' => $variantId,
                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
            ));

        $statement = $this->pmsSql->prepareStatementForSqlObject($select);
        $result = $statement->execute();*/

        if (true){//$result->count() == 0) {
            $insert = $this->pmsSql->insert('PRODUCTS_Supplier_Price')
                ->values(array(
                    'productOrVariantId' => $variantId,
                    'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                    'currencyCode' => $columnArray['Trading Currency'],
                    'price' => $columnArray['Trading Currency Cost'],
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ));
            $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
            $statement->execute();
        } else {
            $productSupplierPrices = array();//$result->next();
            $productSupplierPricesId = $productSupplierPrices['supplierPriceId'];
            $update = $this->pmsSql->update('PRODUCTS_Supplier_Price')
                ->set(array(
                    'currencyCode' => $columnArray['Trading Currency'],
                    'price' => $columnArray['Trading Currency Cost'],
                ))
                ->where('supplierPriceId = '.$productSupplierPricesId);
            $statement = $this->pmsSql->prepareStatementForSqlObject($update);
            $statement->execute();
        }
        return $errorsArray;
    }

    /**
     * @param $productId
     * @param $variantId
     * @param $columnArray
     * @return array
     */
    private function actionProductPrices($productId, $variantId, $columnArray)
    {
        $errorsArray = array();

        foreach ($this->priceColumnsMapping as $territoryId => $currenciesArray) {
            foreach ($currenciesArray as $currencyId => $currencyArray) {
                if (!in_array($productId.'_'.$territoryId.'_'.$currencyId, $this->productsProcessed['price'])) {
                    $select = $this->pmsSql->select('PRODUCTS_Prices')
                        ->where(array(
                            'productOrVariantId' => $productId,
                            'productIdFrom' => 'PRODUCTS',
                            'regionId' => $territoryId,
                            'currencyId' => $currencyId,
                        ));

                    $statement = $this->pmsSql->prepareStatementForSqlObject($select);
                    $result = $statement->execute();

                    if ($result->count() == 0) {
                        $insert = $this->pmsSql->insert('PRODUCTS_Prices')
                            ->values(array(
                                'productOrVariantId' => $productId,
                                'productIdFrom' => 'PRODUCTS',
                                'regionId' => $territoryId,
                                'currencyId' => $currencyId,
                                'nowPrice' => !empty($currencyArray['nowPrice']) ? $columnArray[$currencyArray['nowPrice']] : 0,
                                'wasPrice' => !empty($currencyArray['wasPrice']) ? $columnArray[$currencyArray['wasPrice']] : 0,
                                'costPrice' => !empty($currencyArray['costPrice']) ? $columnArray[$currencyArray['costPrice']] : 0,
                                'created' => date('Y-m-d H:i:s'),
                                'modified' => date('Y-m-d H:i:s')
                            ));
                        $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
                        $statement->execute();
                    }
                    $this->productsProcessed['price'][] = $productId.'_'.$territoryId.'_'.$currencyId;
                }

                /*$select = $this->pmsSql->select('PRODUCTS_Prices')
                    ->where(array(
                        'productOrVariantId' => $variantId,
                        'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                        'regionId' => $territoryId,
                        'currencyId' => $currencyId,
                    ));

                $statement = $this->pmsSql->prepareStatementForSqlObject($select);
                $result = $statement->execute();*/

                if (true){//$result->count() == 0) {
                    $insert = $this->pmsSql->insert('PRODUCTS_Prices')
                        ->values(array(
                            'productOrVariantId' => $variantId,
                            'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                            'regionId' => $territoryId,
                            'currencyId' => $currencyId,
                            'nowPrice' => !empty($currencyArray['nowPrice']) ? $columnArray[$currencyArray['nowPrice']] : 0,
                            'wasPrice' => !empty($currencyArray['wasPrice']) ? $columnArray[$currencyArray['wasPrice']] : 0,
                            'costPrice' => !empty($currencyArray['costPrice']) ? $columnArray[$currencyArray['costPrice']] : 0,
                            'created' => date('Y-m-d H:i:s'),
                            'modified' => date('Y-m-d H:i:s')
                        ));
                    $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
                    $statement->execute();
                } else {
                    $productPrices = array();//$result->next();
                    $productPricesId = $productPrices['productPriceId'];
                    $update = $this->pmsSql->update('PRODUCTS_Prices')
                        ->set(array(
                            'nowPrice' => !empty($currencyArray['nowPrice']) ? $columnArray[$currencyArray['nowPrice']] : 0,
                            'wasPrice' => !empty($currencyArray['wasPrice']) ? $columnArray[$currencyArray['wasPrice']] : 0,
                            'costPrice' => !empty($currencyArray['costPrice']) ? $columnArray[$currencyArray['costPrice']] : 0,
                        ))
                        ->where('productPriceId = '.$productPricesId);
                    $statement = $this->pmsSql->prepareStatementForSqlObject($update);
                    $statement->execute();
                }
            }
        }

        return $errorsArray;
    }

    /**
     * @param $variantId
     * @param $columnArray
     * @return array
     */
    private function actionProductColour($variantId, $columnArray)
    {
        $errorsArray = array();
        $colourName = trim($columnArray['Colour'], $this->trimChars);
        $colourGroupId = $this->coloursArray[$colourName];

        /*$select = $this->pmsSql->select('PRODUCTS_R_ATTRIBUTES_R_SKU_Rules')
            ->where(array(
                'productAttributeId' => $variantId,
                'skuRuleTable' => 'COLOURS_Groups',
            ));

        $statement = $this->pmsSql->prepareStatementForSqlObject($select);
        $result = $statement->execute();*/

        if (true) {//$result->count() == 0) {
            $insert = $this->pmsSql->insert('PRODUCTS_R_ATTRIBUTES_R_SKU_Rules')
                ->values(array(
                    'productAttributeId' => $variantId,
                    'skuRuleId' => $colourGroupId,
                    'skuRuleTable' => 'COLOURS_Groups',
                    'isCommonAttribute' => '0',
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ));
            $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
            $statement->execute();
        }

        return $errorsArray;
    }

    /**
     * @param $variantId
     * @param $columnArray
     * @return array
     */
    private function actionProductSize($variantId, $columnArray)
    {
        $errorsArray = array();
        $baseSize = $this->getBaseSize($columnArray, $columnArray['Base Size']);
        $baseSizeDetails = $this->baseSizesArray[$baseSize];

        /*$select = $this->pmsSql->select('PRODUCTS_R_ATTRIBUTES_R_SKU_Rules')
            ->where(array(
                'productAttributeId' => $variantId,
                'skuRuleTable' => 'COMMON_ATTRIBUTES_VALUES',
            ));

        $statement = $this->pmsSql->prepareStatementForSqlObject($select);
        $result = $statement->execute();*/

        if (true){//$result->count() == 0) {
            $insert = $this->pmsSql->insert('PRODUCTS_R_ATTRIBUTES_R_SKU_Rules')
                ->values(array(
                    'productAttributeId' => $variantId,
                    'skuRuleId' => $baseSizeDetails['commonAttributeValueId'],
                    'skuRuleTable' => 'COMMON_ATTRIBUTES_VALUES',
                    'isCommonAttribute' => '1',
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ));
            $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
            $statement->execute();
        }

        return $errorsArray;

    }

    /**
     * @param $productId
     * @param $variantId
     * @param $columnArray
     * @return array
     */
    private function actionCommonAttributes($productId, $variantId, $columnArray)
    {
        $errorsArray = array();

        $insertQueryArray = array();
        foreach ($columnArray['attributes'] as $attributeName => $attributeValue) {
            if ($attributeValue == '') {
                continue;
            }
            $commonAttributeDetails = $this->commonAttributes[strtoupper($attributeName)];

            $commonAttributeId = $commonAttributeDetails['commonAttributeId'];

            switch ($commonAttributeDetails['commonAttributeViewType']) {
                case 'SELECT':
                    $finalValues = array(array_search(strtoupper($attributeValue), $commonAttributeDetails['values']));
                    break;
                case 'MULTISELECT':
                    $attributeValue = explode(',', $attributeValue);

                    $finalValues = array();
                    foreach ($attributeValue as $attrValue) {
                        $finalValues[] = array_search(strtoupper($attrValue), $commonAttributeDetails['values']);
                    }
                    break;
                default:
                    $finalValues = array($attributeValue);
                    break;
            }

            // insert common attributes for product
            if (!in_array($productId.'_'.$commonAttributeId, $this->productsProcessed['common_attributes'])) {
                $select = $this->pmsSql->select('PRODUCTS_Common_Attributes')
                    ->where(array(
                        'productOrVariantId' => $productId,
                        'productIdFrom' => 'PRODUCTS',
                        'commonAttributeId' => $commonAttributeId,
                    ));

                $statement = $this->pmsSql->prepareStatementForSqlObject($select);
                $result = $statement->execute();

                if ($result->count() == 0) {
                    foreach ($finalValues as $value) {
                        $value = addslashes($value);
                        $insertQueryArray[] = "('{$productId}','PRODUCTS','{$commonAttributeId}','{$value}', NOW(), NOW())";
                        /*$insert = $this->pmsSql->insert('PRODUCTS_Common_Attributes')
                            ->values(array(
                                'productOrVariantId' => $productId,
                                'productIdFrom' => 'PRODUCTS',
                                'commonAttributeId' => $commonAttributeId,
                                'commonAttributeValue' => $value,
                                'created' => date('Y-m-d H:i:s'),
                                'modified' => date('Y-m-d H:i:s')
                            ));
                        $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
                        $statement->execute();*/
                    }
                }

                $this->productsProcessed['common_attributes'][] = $productId.'_'.$commonAttributeId;
            }

            //insert common attributes for variant
            /*$select = $this->pmsSql->select('PRODUCTS_Common_Attributes')
                ->where(array(
                    'productOrVariantId' => $variantId,
                    'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                    'commonAttributeId' => $commonAttributeId,
                ));

            $statement = $this->pmsSql->prepareStatementForSqlObject($select);
            $result = $statement->execute();*/

            switch ($commonAttributeDetails['commonAttributeViewType']) {
                case 'MULTISELECT':
                    /*if ($result->count() > 0) {
                        // delete all the current values
                        $delete = $this->pmsSql->delete('PRODUCTS_Common_Attributes')
                            ->where(array(
                                'productOrVariantId' => $variantId,
                                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                                'commonAttributeId' => $commonAttributeId,
                            ));

                        $statement = $this->pmsSql->prepareStatementForSqlObject($delete);
                        $statement->execute();
                    }*/

                    // insert the new ones
                    foreach ($finalValues as $value) {
                        $value = addslashes($value);
                        $insertQueryArray[] = "('{$variantId}','PRODUCTS_R_ATTRIBUTES','{$commonAttributeId}','{$value}', NOW(), NOW())";

                       /* $insert = $this->pmsSql->insert('PRODUCTS_Common_Attributes')
                            ->values(array(
                                'productOrVariantId' => $variantId,
                                'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                                'commonAttributeId' => $commonAttributeId,
                                'commonAttributeValue' => $value,
                                'created' => date('Y-m-d H:i:s'),
                                'modified' => date('Y-m-d H:i:s')
                            ));
                        $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
                        $statement->execute();*/

                    }
                    break;
                default:
                    if (false) {//$result->count() > 0) {
                        // update the current values all the current values
                        foreach ($finalValues as $value) {
                            $productCommonAttributes = array();//$result->next();
                            $productCommonAttributesId = $productCommonAttributes['productsCommonAttributesId'];
                            $update = $this->pmsSql->update('PRODUCTS_Common_Attributes')
                                ->set(array(
                                    'commonAttributeValue' => $value
                                ))
                                ->where('productsCommonAttributesId = '.$productCommonAttributesId);
                            $statement = $this->pmsSql->prepareStatementForSqlObject($update);
                            $statement->execute();
                        }
                    } else {
                        // insert the new ones
                        foreach ($finalValues as $value) {
                            $value = addslashes($value);
                            $insertQueryArray[] = "('{$variantId}','PRODUCTS_R_ATTRIBUTES','{$commonAttributeId}','{$value}', NOW(), NOW())";

                            /*$insert = $this->pmsSql->insert('PRODUCTS_Common_Attributes')
                                ->values(array(
                                    'productOrVariantId' => $variantId,
                                    'productIdFrom' => 'PRODUCTS_R_ATTRIBUTES',
                                    'commonAttributeId' => $commonAttributeId,
                                    'commonAttributeValue' => $value,
                                    'created' => date('Y-m-d H:i:s'),
                                    'modified' => date('Y-m-d H:i:s')
                                ));
                            $statement = $this->pmsSql->prepareStatementForSqlObject($insert);
                            $statement->execute();*/
                        }
                    }
                    break;
            }
        }

        if (!empty($insertQueryArray)) {
            $this->pmsAdapter->query("
              INSERT INTO PRODUCTS_Common_Attributes
              (`productOrVariantId`, `productIdFrom`, `commonAttributeId`, `commonAttributeValue`, `created`, `modified`)
              VALUES " . implode(',', $insertQueryArray) . "
            ")->execute();
            }
        return $errorsArray;
    }

    /**
     * @param $style
     * @return bool
     * @throws \Exception
     */
    private function checkIfProductExists($style)
    {
        $select = $this->pmsSql->select('PRODUCTS')
            ->where(array('style' => $style));

        $statement = $this->pmsSql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if ($result->count() == 1) {
            $variantDetails = $result->next();
            return $variantDetails['productId'];
        } else if ($result->count() == 0) {
            return false;
        } else {
            throw new \Exception('More than 2 products found for style '.$style);
        }
    }

    /**
     * @param $sku
     * @return bool
     * @throws \Exception
     */
    private function checkIfVariantExists($sku)
    {
        $select = $this->pmsSql->select('PRODUCTS_R_ATTRIBUTES')
            ->where(array('sku' => $sku));

        $statement = $this->pmsSql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if ($result->count() == 1) {
            $variantDetails = $result->next();
            return $variantDetails['productAttributeId'];
        } else if ($result->count() == 0) {
            return false;
        } else {
            throw new \Exception('More than 2 variants found for SKU '.$sku);
        }
    }

    /**
     *
     */
    private function loadBaseSizesArray()
    {
        $result = $this->pmsAdapter->query("
            SELECT CAV.commonAttributeId, CAV.commonAttributeValueId, CAV.commonAttributeValueBaseSize
            FROM COMMON_ATTRIBUTES_VALUES CAV
            INNER JOIN COMMON_ATTRIBUTES CA ON CAV.commonAttributeId=CA.commonAttributeId
            WHERE CA.isSize=1 AND CAV.commonAttributeValueBaseSize != ''
        ")->execute();

        if ($result->count() > 0) {
            foreach ($result as $row) {
                $this->baseSizesArray[$row['commonAttributeValueBaseSize']] = array(
                    'commonAttributeValueId' => $row['commonAttributeValueId'],
                    'commonAttributeId' => $row['commonAttributeId']
                );
            }
        }
    }

    /**
     *
     */
    private function loadColoursArray()
    {
        $select = $this->pmsSql->select('COLOURS_Groups');
        $statement = $this->pmsSql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if ($result->count() > 0) {
            foreach ($result as $row) {
                $this->coloursArray[$row['groupCode']] = $row['colourGroupId'];
            }
        }
    }

    /**
     *
     */
    private function loadSuppliersArray()
    {
        $result = $this->pmsAdapter->query("
            SELECT AD.attributeValue, A.attributeId, A.attributeGroupId
            FROM ATTRIBUTES_Group_Definitions AGD
            INNER JOIN ATTRIBUTES_Groups AG ON AG.attributeGroupId= AGD.attributeGroupId AND AGD.languageId=1
            INNER JOIN ATTRIBUTES A ON A.attributeGroupId=AG.attributeGroupId
            INNER JOIN ATTRIBUTES_Definitions AD ON AD.attributeId = A.attributeId
            WHERE AGD.`name`='Supplier'
        ")->execute();

        if ($result->count() > 0) {
            foreach ($result as $row) {
                $this->suppliersArray[strtolower($row['attributeValue'])] = array(
                    'attributeId' => $row['attributeId'],
                    'attributeGroupId' => $row['attributeGroupId']
                );
            }
        }
    }

    /**
     *
     */
    private function loadCurrenciesArray()
    {
        $select = $this->pmsSql->select('CURRENCY_Codes');
        $statement = $this->pmsSql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if ($result->count() > 0) {
            foreach ($result as $row) {
                $this->currencies[$row['alphabeticCode']] = $row['currencyCodeId'];
            }
        }
    }

    /**
     *
     */
    private function loadCommonAttributesArray()
    {
        $result = $this->pmsAdapter->query("
            SELECT
            CAD.commonAttributeId,
            CAD.description,
            CAV.commonAttributeViewType
            FROM COMMON_ATTRIBUTES_Definitions CAD
            INNER JOIN COMMON_ATTRIBUTES_GUI_Representation CADG
              ON CAD.commonAttributeId = CADG.commonAttributeId
              AND CAD.languageId=1
            INNER JOIN COMMON_ATTRIBUTES_View_Type CAV
              ON CADG.commonAttributesViewTypeId = CAV.commonAttributeViewTypeId
              AND CAV.commonAttributeViewType NOT IN ('SELECT', 'MULTISELECT')
        ")->execute();

        if ($result->count() > 0) {
            foreach ($result as $row) {
                $this->commonAttributes[strtoupper($row['description'])] = $row;
                $this->commonAttributes[strtoupper($row['description'])]['values'] = array();
            }
        }

        $result = $this->pmsAdapter->query("
            SELECT
            CAD.commonAttributeId,
            CAD.description,
            CAVT.commonAttributeViewType,
            UPPER(CAVD.`value`) as `value`,
            CAVD.commonAttributeValueId
            FROM COMMON_ATTRIBUTES_Definitions CAD
            INNER JOIN COMMON_ATTRIBUTES_GUI_Representation CADG
              ON CAD.commonAttributeId = CADG.commonAttributeId
              AND CAD.languageId=1
            INNER JOIN COMMON_ATTRIBUTES_View_Type CAVT
              ON CADG.commonAttributesViewTypeId = CAVT.commonAttributeViewTypeId
              AND CAVT.commonAttributeViewType IN ('SELECT', 'MULTISELECT')
            LEFT JOIN COMMON_ATTRIBUTES_VALUES CAV
              ON CAD.commonAttributeId = CAV.commonAttributeId
            LEFT JOIN COMMON_ATTRIBUTES_VALUES_Definitions CAVD
              ON CAV.commonAttributeValueId = CAVD.commonAttributeValueId
              AND CAVD.languageId=1
        ")->execute();

        if ($result->count() > 0) {
            foreach ($result as $row) {
                if (!isset($this->commonAttributes[strtoupper($row['description'])])) {
                    $valuesArray = array(
                        $row['commonAttributeValueId'] => html_entity_decode($row['value'])
                    );
                    unset($row['value']);
                    $this->commonAttributes[strtoupper($row['description'])] = $row;
                    $this->commonAttributes[strtoupper($row['description'])]['values'] = $valuesArray;
                } else {
                    $this->commonAttributes[strtoupper($row['description'])]['values'][$row['commonAttributeValueId']] = html_entity_decode($row['value']);
                }
            }
        }
    }

    /**
     *
     */
    private function loadChannelsArray()
    {
        $result = $this->pmsAdapter->query("
            SELECT channelId
            FROM CHANNELS
            WHERE `status` = 'ACTIVE'
        ")->execute();

        if ($result->count() > 0) {
            foreach ($result as $row) {
                $this->channels[] = $row['channelId'];
            }
        }
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param $error
     */
    private function setError($error)
    {
        if ($this->verbose) {
            echo "\033[0;31m [!] \033[0m".$error."\n";
        }

        $this->errors[] = $error;
    }

    /**
     * @param $message
     * @param bool|false $noDate
     */
    private function printMessage($message, $noDate = false)
    {
        if ($this->verbose) {
            if ($noDate) {
                echo $message . "\n";
            } else {
                echo '['.date('Y-m-d H:i:s').'] '.$message . "\n";
            }
        }
    }

    /**
     * @param $message
     */
    private function printProgress($message)
    {
        if ($this->verbose) {
            echo "\033[1;33m ". $message. " \033[0m " ."\r";
        }
    }
}