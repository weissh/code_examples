<?php
namespace PrismProductsManager\Controller;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use PrismProductsManager\Model\ApiCategoriesMapper;
use PrismProductsManager\Model\CategoriesMapper;


/**
 * Api Categories Controller
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiCategoriesController extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * Api categories Mapper
     * PrismProductsManager\Model\ApiCategoriesMapper
     * @var object
     */
    protected $_apiCategoriesMapper;
    /**
     * Categories Mapper
     * PrismProductsManager\Model\CategoriesMapper
     * @var object
     */
    protected $_categoriesMapper;

    public function __construct(ApiCategoriesMapper $apiCategoriesMapper, CategoriesMapper $categoriesMapper)
    {
        $this->_apiCategoriesMapper = $apiCategoriesMapper;
        $this->_categoriesMapper = $categoriesMapper;
    }

    public function get($id)
    {

        $this->_methodNotAllowed();
    }

    public function getList()
    {
        $categories = $this->_categoriesMapper->fetchAllMarketingCategories();
        $hierarchy = $this->params()->fromRoute('hierarchy');
        if ($hierarchy == 'format') {
            $categories = $this->_categoriesMapper->processGroupCategories($categories);
        }
        if (!empty($categories)) {
            return new JsonModel($categories);
        } else {
            throw new \Exception('Categories were not found', 400);
        }
    }

    public function create($data)
    {
        $this->_methodNotAllowed();
    }

    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}