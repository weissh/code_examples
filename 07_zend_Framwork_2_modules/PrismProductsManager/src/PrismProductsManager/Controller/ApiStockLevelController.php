<?php
namespace PrismProductsManager\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\Http\Client;
use Zend\Http\Request;
use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;

/**
 * Class ApiStockLevelController
 * @package PrismProductsManager\Controller
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiStockLevelController extends AbstractRestfulController  implements ControllerApiAwareInterface
{

    /**
     * @param $config
     */
    public function __construct($config)
    {
        $this->_config = $config;
    }

    /**
     * @param mixed $id
     * @return array|mixed|void
     * @throws \Exception
     */
    public function get($id)
    {
        $this->_methodNotAllowed();
    }

    /**
     * @throws \Exception
     */
    public function getList()
    {
        $this->_methodNotAllowed();
    }

    /**
     * @param mixed $data
     * @return JsonModel
     * @throws \Exception
     */
    public function create($data)
    {
        if ($this->_config['spectrum-secret-cache'] != $data['secret'] || empty($data['data'])) {
            $this->_methodNotAllowed();
        }

        $websites =  $this->_config['siteConfig']['websites']['clear-cache'];
        $return = array();
        foreach ($websites as $key => $website) {
            $return[$key] = $this->_sendCacheRequestToShopServer($website, $data['data'], $this->_config['shop-secret-cache']);
        }

        return new JsonModel($return);
    }

    /**
     * @param $website
     * @param $data
     * @param $shopSecret
     * @return bool|string
     */
    private function _sendCacheRequestToShopServer($website, $data, $shopSecret)
    {
        $website .= 'api/stock-update';

        $client = new Client($website, array(
            'adapter' => 'Zend\Http\Client\Adapter\Curl',
            'curloptions' => array(CURLOPT_SSL_VERIFYPEER => false),
        ));

        $client->setEncType(Client::ENC_URLENCODED);
        $client->setParameterPost(array('secret' => $shopSecret, 'data' => $data));
        $client->setMethod(Request::METHOD_POST);

        $response = $client->send();
        $result = $response->getBody();

        if ($response->isSuccess()) {
            return $result;
        } else {
            return false;
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @throws \Exception
     */
    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    /**
     * @param mixed $id
     * @throws \Exception
     */
    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }

}