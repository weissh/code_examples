<?php
namespace PrismProductsManager\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;


/**
 * ApiTopMenuController
 *
 * @author Hani
 *
 * @version
 *
 */
class ApiProductByUrlNameController extends AbstractRestfulController  implements ControllerApiAwareInterface
{
    protected $_productsMapper;

    public function getList()
    {
        $urlName = $this->params()->fromRoute('urlName');
        $websiteId = $this->params()->fromRoute('websiteId');
        $languageId = $this->params()->fromRoute('languageId');

        $productId = null;
        $productId  = $this->_getProductMapper()->getProductIdByUrlName($urlName, $languageId, 'ACTIVE');

        $productDetails = array();
        if($productId !== null){
            $productDetails = $this->_getProductMapper()->getProductDetailsById($productId['productId'], $languageId);
        }

        if ($productDetails != FALSE) {
            return new JsonModel($productDetails);
        } else {
            throw new \Exception('I could not get the product details pleaes try again later', 400);
        }

    }

    /**
     * The default action - show the home page
     */
    public function get($id)
    {
        $this->_methodNotAllowed();
/*
        if($id){
            $topNavigationData = $this->_getProductMapper()->fetchTopNavigationById($id);

            if ($topNavigationData != FALSE) {
                return new JsonModel($topNavigationData);
            }
        }

        throw new \Exception('I could not get the navigation', 400);
        */

    }

    protected function _getProductMapper()
    {
        if (!$this->_productsMapper) {
            $sm = $this->getServiceLocator();
            $this->_productsMapper = $sm->get('PrismProductsManager\Model\ProductsMapper');
        }
        return $this->_productsMapper;
    }

    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }

}