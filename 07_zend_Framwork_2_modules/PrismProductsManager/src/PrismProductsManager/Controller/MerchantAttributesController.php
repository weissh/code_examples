<?php

namespace PrismProductsManager\Controller;

use PrismElasticSearch\Enum\ElasticSearchTypes;
use PrismProductsManager\Form\AddAttributesMerchantGroupForm;
use PrismQueueManager\Service\QueueService;
use Zend\Mvc\Controller\AbstractActionController;
use PrismProductsManager\Form\DownloadBulkForm;
use PrismProductsManager\Form\UploadBulkForm;
use PrismProductsManager\Form\SearchForm;
use PrismProductsManager\Form\AddAttributesColourGroupsForm;
use PrismProductsManager\Form\AddAttributesExistingColoursGroupForm;
use Zend\View\Model\ViewModel;
use PrismProductsManager\Model\AttributesGroupMapper;
use PrismProductsManager\Model\ColoursMapper;

/**
 * Class MerchantAttributesController
 * @package PrismProductsManager\Controller
 */
class MerchantAttributesController extends AbstractActionController
{
    protected $_bulkDownloadForm;
    /**
     *  inject the bulk upload template form
     * @var object
     */
    protected $_bulkUploadForm;
    /**
     *  inject the search form
     * @var object
     */
    protected $_searchForm;
    protected $_attributesGroupMapper;
    protected $_addAttributesColourGroupsForm;
    protected $_addAttributesExistingColoursGroupForm;
    protected $_coloursMapper;
    /**
     *  Inject the attributes merchant group
     */
    protected $_addAttributesMerchantGroup;

    /**
     *  used to check if form (add/edit) is _valid
     * @var boolean
     */
    protected $_valid;
    /**
     * @var Object
     *  Mappers for module/PrismProductsManager/src/PrismProductsManager/Model/AttributesGroupMapper.php
     */
    protected $_attributesMapper;
    /**
     * @var
     */
    protected $_colours;

    protected $_simplePagination;
    protected $_queueService;
    protected $_productsMapper;

    /**
     * @param DownloadBulkForm $downloadBulkForm
     * @param UploadBulkForm $uploadBulkForm
     * @param AddAttributesMerchantGroupForm $addAttributesMerchantGroupForm
     * @param AddAttributesColourGroupsForm $addAttributesColourGroupsForm
     * @param AddAttributesExistingColoursGroupForm $addAttributesExistingColoursGroupForm
     * @param AttributesGroupMapper $attributesGroupMapper
     * @param ColoursMapper $coloursMapper
     * @param \PrismProductsManager\Form\SearchForm $searchForm
     * @param $simplePagination
     * @param $queueService
     * @param $productsMapper
     */
    public function __construct(
        DownloadBulkForm $downloadBulkForm,
        UploadBulkForm $uploadBulkForm,
        AddAttributesMerchantGroupForm $addAttributesMerchantGroupForm,
        AddAttributesColourGroupsForm $addAttributesColourGroupsForm,
        AddAttributesExistingColoursGroupForm $addAttributesExistingColoursGroupForm,
        AttributesGroupMapper $attributesGroupMapper,
        ColoursMapper $coloursMapper,
        SearchForm $searchForm,
        $simplePagination,
        QueueService $queueService,
        $productsMapper
    ) {
        $this->_addAttributesMerchantGroupForm = $addAttributesMerchantGroupForm;
        $this->_bulkDownloadForm = $downloadBulkForm;
        $this->_bulkUploadForm = $uploadBulkForm;
        $this->_addAttributesColourGroupsForm = $addAttributesColourGroupsForm;
        $this->_addAttributesExistingColoursGroupForm = $addAttributesExistingColoursGroupForm;
        $this->_attributesGroupMapper = $attributesGroupMapper;
        $this->_coloursMapper = $coloursMapper;
        $this->_searchForm = $searchForm;
        $this->_simplePagination = $simplePagination;
        $this->_queueService = $queueService;
        $this->_productsMapper = $productsMapper;
    }

    /**
     *  Attributes Groups management
     * @return ViewModel
     */
    public function indexAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $addAttributeMerchantGroupForm = $this->_addAttributesMerchantGroupForm;
        $addAttributeMerchantGroupForm->get('submit')->setValue('Save');

        $orderBy = $this->params()->fromRoute('orderby');
        $orderbyOption = $this->params()->fromRoute('orderbyoption');

        $request = $this->getRequest();

        //Set the values for the $_valid to false
        //If true Popup opens on page load
        $this->_valid['MerchantAdd'] = false;
        $this->_valid['MerchantEdit'] = false;
        $submitedFormData = $request->getPost();

        if ($request->isPost()) {
            // In case of search redirect to search method
            if (isset($submitedFormData->search)) {
                $route = array(
                    'controller' => 'MerchantAttributes',
                );
                // Set data for form and check if the search form is valid
                $this->_searchForm->setData($submitedFormData);
                // Check if search form is valid
                if ($this->_searchForm->isValid()) {
                    $search = $this->_searchForm->getData();
                    $type = 'success';
                    $message = 'Search results for ' . $search['search'];
                    $route['search'] = urlencode($search['search']);
                } else {
                    $type = 'error';
                    $message = 'Cannot find any results.';
                }
                //Redirect based on the message and route
                $this->_errorValue($route, $type, $message);
            }

            $addAttributeMerchantGroupForm->setData($submitedFormData);
            $addAttributeMerchantGroupForm->formData = $submitedFormData;

            if ($addAttributeMerchantGroupForm->isvalid()) {
                //Check if the form is submitted for add or edit
                if (empty($submitedFormData['attributeGroupId'])) {
                    // Add new Merchant Group
                    $this->isCmsUserAuthorized(array('add'));
                    $id = $this->_attributesGroupMapper->saveMerchantGroup($addAttributeMerchantGroupForm->getData());
                    $this->_errorValue(array('controller' => 'MerchantAttributes', 'action' => 'values', 'id' => $id),
                        'success', 'The Group has been added.');
                } else {
                    // Edit Merchant Group
                    $this->isCmsUserAuthorized(array('edit'));
                    $id = $this->_attributesGroupMapper->updateMerchantGroup($addAttributeMerchantGroupForm->getData());
                    $this->_errorValue(array('controller' => 'MerchantAttributes', 'action' => 'values', 'id' => $id),
                        'success', 'The Group has been modified.');
                }
            } else {
                //Check if the form is submitted for add or edit
                if (empty($submitedFormData['attributeGroupId'])) {
                    //Add new Merchant Group
                    $this->_valid['MerchantAdd'] = true;
                } else {
                    //Edit Merchant Group
                    $this->_valid['MerchantEdit'] = true;
                }
            }
        }

        $search = $this->params()->fromRoute('search');
        // If the request if for search get the groups using search params
        if (isset($search) && !empty($search)) {
            $groups = $this->_attributesGroupMapper->fetchAllMerchantsAttributes(urldecode($search), $orderBy,
                $orderbyOption);
            // If search results are empty redirect to main page without search
            if (empty($groups)) {
                $route = array(
                    'controller' => 'MerchantAttributes',
                    'action' => 'index',
                );
                $type = 'error';
                $message = 'Cannot find any results.';
                $this->_errorValue($route, $type, $message);
            }
        } else {
            //Get Groups with all children's
            $groups = $this->_attributesGroupMapper->fetchAllMerchantsAttributes($search = null, $orderBy,
                $orderbyOption);
        }

        //Set up the pagination
        $matches = $this->getEvent()->getRouteMatch();
        $route = $this->_simplePagination->getRoute($matches);
        $page = $matches->getParam('page', 1);

        $paginator = $this->_simplePagination->load($matches, $groups, $nrPerPage = 8);

        // Set per page if search is valid
        if (isset($search) && !empty($search)) {
            $search = '/search/' . $search;
            $paginator->setItemCountPerPage(15);
        } else {
            $paginator->setItemCountPerPage(15);
        }

        return new ViewModel(array(
            'attributesMerchantGroupForm' => $addAttributeMerchantGroupForm,
            'paginator' => $paginator,
            'valid' => $this->_valid,
            'search' => $search,
            'searchForm' => $this->_searchForm,
            'route' => $route,
            'orderby' => $orderBy,
            'orderbyOption' => $orderbyOption,
        ));
    }

    /**
     *  Used for Group attributes management page
     * @return ViewModel
     */
    public function valuesAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $id = $this->params()->fromRoute('id');

        // Get and set the colours attribute values add form
        $attributesColourGroupsAddForm = $this->_addAttributesColourGroupsForm;
        $attributesColourGroupsAddForm->get('submit')->setValue('Save');
        $addAttributesExistingColoursGroupForm = $this->_addAttributesExistingColoursGroupForm;
        $addAttributesExistingColoursGroupForm->get('submit')->setValue('Save');

        // Set the data for the colour groups
        $colourGroups = $this->_coloursMapper->fetchAllColorsGroupsOptions($id);
        $addAttributesExistingColoursGroupForm->get('colourGroups')->setValueOptions($colourGroups);

        // Set the existing colour groups array
        $colours = $this->_coloursMapper->fetchAllColorsOptions();
        $attributesColourGroupsAddForm->get('colours')->setValueOptions($colours);

        $request = $this->getRequest();

        $valid['add'] = false;
        $valid['edit'] = false;

        /**
         * Saves the attribute in the specific group or throws an error
         * Redirects to Groups management page if attribute added from there or
         * to specific group attributes management page.
         */
        if ($request->isPost()) {
            $this->isCmsUserAuthorized(array('add', 'edit'));
            $submitedFormData = $this->params()->fromPost();
            // Check if the submit is for Colours or other attributes
            if (isset($submitedFormData['colourGroupId'])) {
                // For the edit we need to check the values from the form and see
                $attributesColourGroupsAddForm->setSubmitedData($submitedFormData);
                $attributesColourGroupsAddForm->setData($submitedFormData);
                if ($attributesColourGroupsAddForm->isValid()) {
                    $processedData = $attributesColourGroupsAddForm->getData();

                    //Return true for adding a new colour
                    //Return false for editing an existing colour
                    $route = array(
                        'controller' => 'MerchantAttributes',
                        'action' => 'values',
                        'id' => $id
                    );

                    if (empty($processedData['colourGroupId']) && $this->_coloursMapper->checkIfColourGroupExists($processedData['colourGroupName'],
                            $processedData['colourGroupCode'])
                    ) {

                        $status = 'error';
                        $message = 'Colour group already exist!';

                    } else {
                        $status = 'success';
                        $saveColour = $this->_coloursMapper->saveColourAttribute($processedData, $id);
                        $colourGroupId = $saveColour['colourGroupId'];

                        if ($saveColour['saveMethod'] == 'add') {
                            $message = 'The colour has been added';
                            // add to spectrum
                            $this->_queueService->addSpectrumColourToQueue($colourGroupId);

                        } else {

                            // we need to update the products belonging to this colour group id in elastic-search
                            $message = 'The colour has been edited';
                            // update in spectrum
                            $this->_queueService->addSpectrumColourToQueue($colourGroupId);

                        }

                        // update the entries for all the products contained in the colour
                        $productsIdsForColour = $this->_coloursMapper->getAllProductsForColour($processedData['colourGroupId']);

                        if (!empty($productsIdsForColour)) {

                            foreach ($productsIdsForColour as $productId) {
                                $this->_queueService->addIndexEntryToQueue($productId . '_0',
                                    elasticSearchTypes::PRODUCTS);
                            }
                        }

                    }
                    // Redirect based on the message and route
                    $this->_errorValue($route, $status, $message);
                } else {
                    // Set the valid variable to true
                    // Used to open modal after post
                    if (empty($submitedFormData['colourGroupId'])) {
                        $valid['add'] = true;

                    } else {
                        $valid['edit'] = true;

                    }
                }
            } elseif (isset($submitedFormData['size-group-id'])) {
                // If the group is merchant size group
                // update the group sizes
                // redirect with success
                if ($this->_attributesGroupMapper->modifySizesGroup($submitedFormData)) {
                    $message = 'The group has been updated';
                    $type = 'success';
                } else {
                    $message = 'You cannot remove size attributes that are used in products.';
                    $type = 'error';
                }
                $route = array(
                    'controller' => 'Attributes',
                    'action' => 'values',
                    'id' => $id
                );
                $this->_errorValue($route, $type, $message);
            }
        }

        // Get the Group with attributes
        // Redirects if no groups if found in database
        $group = $this->_attributesGroupMapper->getGroupById($id);

        // If the group is a size group then get all sizes from source table
        $sizes = array();

        switch ($group['where']) {
            case 'size-shirts':
            case 'size-clothes':
            case 'size-belts':
            case 'size-jackets':
            case 'size-trousers':
            case 'size-swimshorts':
                $sizes = $this->_attributesGroupMapper->fetchAllSizesShirtsAndClothes($group['parentSourceTable']);
                break;
            case 'size-accessories':
            case 'size-shoes':
                $sizes = $this->_attributesGroupMapper->fetchAllSizesFromTable($group['parentSourceTable']);
                break;
        }

        // Throw error if the group is invalid
        if ($id === null || empty($group)) {
            $this->_errorValue('attributes', 'error', 'Group was not found in the database');
        }

        // Return to view variables
        return new ViewModel(array(
            'group' => $group,
            'valid' => $valid,
            'id' => $id,
            'sizes' => $sizes,
            'attributesColourGroupsAddForm' => $attributesColourGroupsAddForm,
            'addAttributesExistingColoursGroupForm' => $addAttributesExistingColoursGroupForm
        ));
    }

    public function addExistingColourAction()
    {
        $this->isCmsUserAuthorized(array('add'));
        $id = $this->params()->fromRoute('id');
        $addAttributesExistingColoursGroupForm = $this->_addAttributesExistingColoursGroupForm;

        $route = array(
            'controller' => 'MerchantAttributes',
            'action' => 'values',
            'id' => $id
        );
        $request = $this->getRequest();
        if ($request->isPost()) {
            $submitedFormData = $this->params()->fromPost();
            // For the edit we need to check the values from the form and see
            $addAttributesExistingColoursGroupForm->setData($submitedFormData);
            if ($addAttributesExistingColoursGroupForm->isValid()) {
                $processedData = $addAttributesExistingColoursGroupForm->getData();
                if ($this->_coloursMapper->bindColourGroupsToAttributeGroup($processedData['colourGroups'], $id)) {
                    $type = 'success';
                    $message = 'Colour groups added!';
                } else {
                    $type = 'error';
                    $message = 'Couldn\'t add the color groups specified!';
                }
                // Redirect based on the message and route
            } else {
                $type = 'error';
                $message = 'Invalid value specified for the color groups field!';
            }
            return $this->_errorValue($route, $type, $message);
        } else {
            throw new \Exception('Invalid request type!');
        }
    }

    /**
     * Used to download the template for the selected Merchant Group
     * Used to Upload the template for the selected Merchant Group
     * Process template and save data from the template
     * @return ViewModel
     */
    public function bulkAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $bulkDownloadForm = $this->_bulkDownloadForm;
        $bulkDownloadForm->get('submit')->setValue('Download Template');

        $bulkUploadForm = $this->_bulkUploadForm;
        $bulkUploadForm->get('submit')->setValue('Upload Template');

        $request = $this->getRequest();
        $submitedFormData = $this->params()->fromPost();

        /*
         *  Download template or Upload Template
         *  Upload template validates form, proceses data with the bulk method
         *  in the module/PrismProductsManager/src/PrismProductsManager/Model/AttributesGroupMapper
         */
        $response = array();
        $valid = true;
        if ($request->isPost()) {
            if ($submitedFormData['submit'] == 'Download Template') {

                $response = $this->_attributesGroupMapper->bulk($submitedFormData);

            } elseif ($submitedFormData['submit'] == 'Upload Template') {
                $this->isCmsUserAuthorized(array('add', 'edit'));
                $file = $this->params()->fromFiles('feedAttachment');
                $nonFile = $request->getPost()->toArray();
                $data = array_merge($nonFile, array('feedAttachment' => $file));
                $bulkUploadForm->setData($data);

                if ($bulkUploadForm->isValid()) {
                    /*
                     * Check if the file has errors and if the file was uploaded
                     */
                    if ($file['error'] == UPLOAD_ERR_OK && is_uploaded_file($file['tmp_name'])) {
                        // at this point if we have any errors a excel file will be downloaded
                        // else we will refresh the page and show the success message
                        $response = $this->_attributesGroupMapper->bulk($data);
                        $this->_errorValue(array('controller' => 'MerchantAttributes', 'action' => 'bulk'), 'success',
                            'The file returned no errors.');
                    }
                }
            }
        }
        return new ViewModel(array(
            'bulkDownloadForm' => $bulkDownloadForm,
            'bulkUploadForm' => $bulkUploadForm,
            'bulkErrors' => $response,
            //'valid' => $valid
        ));
    }

    /**
     * Delete merchant groups or merchant attributes
     *
     */
    protected function deleteMerchantAction()
    {
        $this->isCmsUserAuthorized(array('delete'));
        /*
         * Check's if delete came from Group management page or
         * attributes management page and set the route for later redirect.
         */
        if ($this->params()->fromRoute('delete') == 'group') {
            $route = array(
                'controller' => 'Attributes',
                'action' => 'index',
            );
        } else {
            $route = array(
                'controller' => 'Attributes',
                'action' => 'values',
                'id' => $this->params()->fromRoute('from')
            );
        }

        /*
         * Deletes the Attribute or Group
         * Set the Type ( error / success )
         * Set the message for the redirect
         */
        if ($this->_attributesGroupMapper->deleteMerchant($this->params()->fromRoute())) {
            if ($this->params()->fromRoute('delete') == 'group') {
                $message = 'The group with all the attribute values has been deleted.';
            } else {
                $message = 'The attribute value has been deleted.';
            }
            $type = 'success';
        } else {
            if ($this->params()->fromRoute('delete') == 'group') {
                $message = 'This group has attributes used in product variants and therefore cannot be deleted.';
            } else {
                $message = 'The attribute is used in product variants and therefore cannot be deleted. ' . __LINE__;
            }
            $type = 'error';
        }
        /*
         * Redirect based on $route, $type, $message
         */
        $this->_errorValue($route, $type, $message);
        return;
/*
        $response = $this->getResponse();
        $response->setStatusCode(200);
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/json');

        return $response;
*/
    }

    public function merchantAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $group = $this->_attributesGroupMapper->getGroupSourceTable($this->params()->fromRoute('id'));

        return new ViewModel(array(
            'group' => $group
        ));
    }

    /**
     * Redirect in case of error
     * @param $route
     * @param $type
     * @param $message
     *
     * @param string $alert
     * @return null
     */
    protected function _errorValue($route, $type, $message, $alert = [])
    {
        $alert['type'] = $type;
        $alert['bold'] = ucfirst($type) . ':';
        $alert['messages'] = array();
        $alert['messages'][0] = $message;

        $this->flashmessenger()->setNamespace('alert')
            ->addMessage($alert);

        return $this->redirect()->toRoute(null, $route);
    }

    /**
     * Ajax function used for enableing colours
     * Set 1/0 in the COLOURS Table
     */
    public function enableColourAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $params = $this->params()->fromPost();
        $this->_attributesGroupMapper->enableColours($params);

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/json');

        return $response;
    }
}
