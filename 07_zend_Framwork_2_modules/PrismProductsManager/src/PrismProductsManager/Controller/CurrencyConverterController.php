<?php

namespace PrismProductsManager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;
use Zend\Session\Container;

class CurrencyConverterController extends AbstractActionController
{

    protected $_currencyMapper = null;

    protected $_queryPaginator = null;

    public function indexAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $alert = array();
        $request = $this->getRequest();
        $submitedformData = $this->params()->fromPost();
        $currencies = $this->_getCurrencyMapper()->getAvailableCurrenciesForForm();
        $session = new Container('containername');

        if ($request->isPost()) {
            $this->isCmsUserAuthorized(array('edit'));
            if (array_key_exists('CurrencyUpdate', $submitedformData)) {
                $data = $this->_getCurrencyMapper()->convertPrices($submitedformData);
                $session->offsetSet('updatedProdlist', $data['updatedProducts']);
                return $this->redirect()->toRoute('currency-convert',
                    array(
                      'controller'=>'CurrencyConverter',
                      'action' => 'priceList'
                    )
                );

            }
        }

        return new ViewModel(array(
            'Currencies' => $currencies,
            'updatedProducts' => (isset($data['updatedProducts'])) ? $data['updatedProducts'] : 0,
            'updatedProductsCount' => (isset($data['updatedProductsCount'])) ? $data['updatedProductsCount'] : 0
        ));
    }

    /**
     * This is a convenience method to load the CurrencyMapper
     *
     * @return _elementTypesMapper
     */
    protected function _getCurrencyMapper()
    {
        if (!$this->_currencyMapper) {
            $sm = $this->getServiceLocator();
            $this->_currencyMapper = $sm->get('PrismProductsManager\Model\CurrencyMapper');
        }
        return $this->_currencyMapper;
    }

    public function priceListAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $session = new Container('containername');
        $updatedProductList = $session->offsetGet('updatedProdlist');
        $page = $this->params()->fromRoute('page');
        $currencies = $this->_getCurrencyMapper()->getAllCurrencies();

        $productPriceList = $this->_getCurrencyMapper()->getProductList($page);

        $matches = $this->getEvent()->getRouteMatch();
        $route['name'] = $matches->getMatchedRouteName();
        $route['params'] = $matches->getParams();

        return new ViewModel(array(
            'Currencies' => $currencies,
            'productPriceList' => $productPriceList,
            'updatedProductList' => $updatedProductList,
            'paginator' => $productPriceList,
            'route' => $route
        ));
    }

    /***
     *
     * Will initiate a download of the CSV Export
     *
     */
    public function csvExportAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $session = new Container('containername');
        $updatedProductList = $session->offsetGet('updatedProdlist');

        //Get product price list with no result limit
        $productPriceList = $this->_getCurrencyMapper()->getProductList(false);
        $this->_getCurrencyMapper()->exportDataArray($productPriceList, $updatedProductList);

    }


}

