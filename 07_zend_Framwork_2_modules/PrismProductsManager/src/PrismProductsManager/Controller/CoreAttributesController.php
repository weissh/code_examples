<?php

namespace PrismProductsManager\Controller;

use PrismProductsManager\Model\AttributesGroupMapper;
use PrismProductsManager\Form\EditCoreAttributeGroupForm;
use PrismProductsManager\Form\SearchForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;

/**
 * Class CoreAttributesController
 * @package PrismProductsManager\Controller
 */
class CoreAttributesController extends AbstractActionController
{
    /**
     * Attributes Group Mapper Injected
     * @var object
     */
    protected $_attributesMapper;
    /**
     * Core Attributes Edit Form
     * \PrismProductsManager\Form\EditCoreAttributeGroupForm
     * @var object
     */
    protected $_editCoreAttributesForm;
    /**
     * Search form
     * \PrismProductsManager\Form\SearchForm
     * @var object
     */
    protected $_searchForm;
    /**
     * Used to check if form (edit) is _valid
     * @var boolean
     */
    protected $_valid;

    protected $_simplePagination;

    /**
     * @param AttributesGroupMapper $attributesMapper
     * @param \PrismProductsManager\Form\EditCoreAttributeGroupForm $editCoreAttributesForm
     * @param \PrismProductsManager\Form\SearchForm $searchForm
     */
    public function __construct(AttributesGroupMapper $attributesMapper, EditCoreAttributeGroupForm $editCoreAttributesForm,
                                SearchForm $searchForm, $simplePagination)
    {
        $this->_attributesMapper = $attributesMapper;
        $this->_editCoreAttributesForm = $editCoreAttributesForm;
        $this->_searchForm = $searchForm;
        $this->_simplePagination = $simplePagination;
    }

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $request = $this->getRequest();

        $orderBy = $this->params()->fromRoute('orderby');
        $orderbyOption = $this->params()->fromRoute('orderbyoption');

        //Set the values for the $_valid to false
        //If true Popup opens on page load
        $this->_valid['CoreEdit'] = false;
        $submitedData = $request->getPost();

        if ($request->isPost()) {

            // In case of search redirect to search method
            if (isset($submitedData->search)) {
                $route = array(
                    'controller' => 'CoreAttributes',
                    'action' => 'index',
                );
                // Set data for form and check if the search form is valid
                $this->_searchForm->setData($submitedData);
                // Check if search form is valid
                if ($this->_searchForm->isValid()) {
                    $search = $this->_searchForm->getData();
                    $type = 'success';
                    $message = 'Search results for ' . $search['search'];
                    $route['search'] = urlencode($search['search']);
                } else {
                    $type = 'error';
                    $message = 'Cannot find any results.';
                }

                //Redirect based on the message and route
                $this->_errorValue($route, $type, $message);
            }

            $this->_editCoreAttributesForm->setData($submitedData);
            if ($this->_editCoreAttributesForm->isValid()) {
                $this->isCmsUserAuthorized(array('edit'));
                $this->_attributesMapper->saveCoreGroupAttribute($this->_editCoreAttributesForm->getData());
                //Set Route for the redirect
                $route = array(
                    'controller'=>'core-attributes',
                    'action' => 'index'
                );
                $message = 'The core group attribute public name was modified.';
                $this->_errorValue($route, 'success', $message);
            } else {
                $this->_valid['CoreEdit'] = true;
            }
        }
        $search = $this->params()->fromRoute('search');
        // If the request if for search get the groups using search params

        if (isset($search) && !empty($search)) {

            //Get Groups with all children's from search
            $groups = $this->_attributesMapper->fetchAllCoreAttributes(urldecode($search), $orderBy, $orderbyOption);
            // If search results are empty redirect to main page without search
            if (empty($groups)) {
                $route = array(
                    'controller' => 'CoreAttributes',
                    'action' => 'index',
                );
                $type = 'error';
                $message = 'Cannot find any results.';
                $this->_errorValue($route, $type, $message);
            }
        } else {
            //Get Groups with all children's
            $groups = $this->_attributesMapper->fetchAllCoreAttributes($search = null, $orderBy, $orderbyOption);
        }

        //Set up the pagination
        $matches = $this->getEvent()->getRouteMatch();
        $route = $this->_simplePagination->getRoute($matches);
        $page = $matches->getParam('page', 1);

        $paginator = $this->_simplePagination->load($matches, $groups, $nrPerPage = 12);

        return new ViewModel(array(
            'paginator' => $paginator,
            'valid' => $this->_valid,
            'editCoreAttributes' => $this->_editCoreAttributesForm,
            'searchForm' => $this->_searchForm,
            'search' => $search,
            'orderby' => $orderBy,
            'orderbyOption' => $orderbyOption,
            'route' => $route
        ));
    }

    /**
     *  Used for Group attributes management page
     *  @return ViewModel
     */
    public function valuesAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $id = $this->params()->fromRoute('id');

        //Get the Group with attributes
        //Redirects if no groups if found in database
        $group = $this->_attributesMapper->getGroupById($id);

        //Throw error if the group is invalid
        if ($id === null || empty($group)) {
            $this->_errorValue('attributes', 'error', 'Group was not found in the database');
        }

        //Return to view variables
        return new ViewModel(array(
            'group' => $group,
        ));

    }
    /**
     * Redirect in case of error
     * @param $route
     * @param $type
     * @param $message
     *
     * @param string $alert
     * @return null
     */
    protected function _errorValue($route, $type, $message, $alert = [])
    {
        $alert['type'] = $type;
        $alert['bold'] = ucfirst($type).':';
        $alert['messages'] = array();
        $alert['messages'][0] = $message;

        $this->flashmessenger()->setNamespace('alert')
            ->addMessage($alert);

        return $this->redirect()->toRoute(null ,$route);
    }
}