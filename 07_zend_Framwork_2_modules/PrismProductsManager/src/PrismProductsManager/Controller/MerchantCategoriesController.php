<?php

namespace PrismProductsManager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;
use PrismProductsManager\Model\MerchantCategoriesMapper;
use PrismProductsManager\Form\AddMerchantCategoryForm;
use PrismProductsManager\Form\SearchForm;

class MerchantCategoriesController extends AbstractActionController
{
    /**
     * Inject the db adapter into the controller
     */
    protected $_dbAdapter;
    /**
     * Inject the merchant Categories Mapper
     */
    protected $_merchantCategoriesMapper;
    /**
     * Inject the Add Merchant Category Form
     */
    protected $_addMerchantCategoryForm;
    /**
     * Inject the Search Form
     */
    protected $_searchForm;
    /**
     * Boolean variable to open modal after form submition
     */
    protected $_valid;

    protected $_simplePagination;

    /**
     * @param $dbAdapter
     * @param \PrismProductsManager\Model\MerchantCategoriesMapper $merchantCategoriesMapper
     * @param \PrismProductsManager\Form\AddMerchantCategoryForm $addMerchantCategoryForm
     * @param \PrismProductsManager\Form\SearchForm $searchForm
     */
    public function __construct($dbAdapter, MerchantCategoriesMapper $merchantCategoriesMapper,
                                AddMerchantCategoryForm $addMerchantCategoryForm, SearchForm  $searchForm, $simplePagination)
    {
        $this->_dbAdapter = $dbAdapter;
        $this->_merchantCategoriesMapper = $merchantCategoriesMapper;
        $this->_addMerchantCategoryForm = $addMerchantCategoryForm;
        $this->_searchForm = $searchForm;
        $this->_simplePagination = $simplePagination;
    }

    /**
     *
     */
    public function indexAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $orderBy = $this->params()->fromRoute('orderby');
        $orderbyOption = $this->params()->fromRoute('orderbyoption');

        $searchRoute = $this->params()->fromRoute('search');
        // If the request if for search get the groups using search params
        if (isset($searchRoute) && !empty($searchRoute)) {
            //Get Groups with all children's from search
            // Get all merchant categories and process them into a hierarchy for the front end
            $allCategories = $this->_merchantCategoriesMapper->fetchAllMerchantCategories('all', urldecode($searchRoute), $orderBy, $orderbyOption);

            // If search results are empty redirect to main page without search
            if (empty($allCategories)) {
                $route = array(
                    'controller' => 'MarketingAttributes',
                    'action' => 'index',
                );
                $type = 'error';
                $message = 'Cannot find any results.';
                $this->_errorValue($route, $type, $message);
            }
        } else {
            //Get Groups with all children's
            // Get all merchant categories and process them into a hierarchy for the front end
            $allCategories = $this->_merchantCategoriesMapper->fetchAllMerchantCategories('', null, $orderBy, $orderbyOption);
        }

        $hierarchyCategories = $this->_merchantCategoriesMapper->processGroupCategories($allCategories, $searchRoute);

        $request = $this->getRequest();
        $submitedformData = $request->getPost();
        $searchData = $request->getPost();
        // Set the _valid to be false, if true dialog opens on page load
        $this->_valid['Add']  = false;
        $this->_valid['Edit']  = false;

        if ($request->isPost()) {

            // In case of search redirect to search method
            if (isset($searchData->search)) {
                $route = array(
                    'controller' => 'MerchantCategories',
                    'action' => 'index',
                );
                // Set data for form and check if the search form is valid
                $this->_searchForm->setData($searchData);
                // Check if search form is valid
                if ($this->_searchForm->isValid()) {
                    $search = $this->_searchForm->getData();
                    $type = 'success';
                    $message = 'Search results for ' . $search['search'];
                    $route['search'] = urlencode($search['search']);
                } else {
                    $type = 'error';
                    $message = 'Cannot find any results.';
                }

                //Redirect based on the message and route
                $this->_errorValue($route, $type, $message);
            }

            // Verify if the form submitted is for edit and if the category name has changed
            $uniqueName = false;
            if (!empty($submitedformData['productMerchantCategoryId']) && $this->_merchantCategoriesMapper->checkCategoryNameDifference($submitedformData)) {
                $uniqueName = true;
            }
            // Set the flag in the form
            $this->_addMerchantCategoryForm->setUniqueName($uniqueName);

            $submitedformData->skuRulesNr = count($submitedformData->skuRules);
            $this->_addMerchantCategoryForm->setData($submitedformData);

            $this->_addMerchantCategoryForm->parentFlag = $submitedformData['parentId'];

            if ($this->_addMerchantCategoryForm->isValid()) {
                if (!empty($submitedformData->productMerchantCategoryId)) {
                    $this->isCmsUserAuthorized(array('edit'));
                    // Edit existing category
                    $this->_merchantCategoriesMapper->updateMerchantCategory($this->_addMerchantCategoryForm->getData());
                    $message = 'The merchant category has been modified';
                } else {
                    $this->isCmsUserAuthorized(array('add'));
                    // Add new category
                    $this->_merchantCategoriesMapper->saveMerchantCategory($this->_addMerchantCategoryForm->getData());
                    $message = 'The merchant category was added.';
                }

                $route = array(
                    'controller'=>'MerchantCategories',
                    'action' => 'index'
                );

                $this->_errorValue($route, 'success', $message);
            } else {
                $this->_valid['Add']  = true;
            }
        }

        //Set up the pagination
        $matches = $this->getEvent()->getRouteMatch();
        $route = $this->_simplePagination->getRoute($matches);
        $page = $matches->getParam('page', 1);

        $paginator = $this->_simplePagination->load($matches, $hierarchyCategories, $nrPerPage = 3);

        if (isset($searchRoute) && !empty($searchRoute)) {
            $searchRoute = '/search/' . $searchRoute;
            $paginator->setItemCountPerPage(10);
        } else {
            $paginator->setItemCountPerPage(3);
        }


        return new ViewModel(array(
            'paginator' => $paginator,
            'addMerchantCategoryForm' => $this->_addMerchantCategoryForm,
            'valid' => $this->_valid,
            'searchForm' => $this->_searchForm,
            'search' => $searchRoute,
            'orderby' => $orderBy,
            'orderbyOption' => $orderbyOption,
            'route' => $route
        ));
    }

    /**
     * Used to delete Marketing categories
     * @return null
     */
    public function deleteAction()
    {
        $this->isCmsUserAuthorized(array('delete'));
        $id = $this->params()->fromRoute('id');

        // Delete the merchant category
        $this->_merchantCategoriesMapper->deleteMerchantCategory($id);
        $route = array(
            'controller'=>'MerchantCategories',
            'action' => 'index'
        );
        $message = 'The merchant category was deleted';
        $this->_errorValue($route, 'success', $message);
    }

    /**
     * Redirect in case of error
     * @param $route
     * @param $type
     * @param $message
     *
     * @param string $alert
     * @return null
     */
    protected function _errorValue($route, $type, $message, $alert = [])
    {
        $alert['type'] = $type;
        $alert['bold'] = ucfirst($type).':';
        $alert['messages'] = array();
        $alert['messages'][0] = $message;

        $this->flashmessenger()->setNamespace('alert')
            ->addMessage($alert);

        return $this->redirect()->toRoute(null ,$route);
    }
    /**
     * Ajax function used for enableing categories
     * Set 1/0 in the Categories_definition Table
     */
    public function enableCategoryAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $params = $this->params()->fromPost();
        $this->_merchantCategoriesMapper->enableCategory($params);

        $response = $this->getResponse();
        $response->setContent(true);
        $response->setStatusCode(200);
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/json');

        return $response;
    }
}
