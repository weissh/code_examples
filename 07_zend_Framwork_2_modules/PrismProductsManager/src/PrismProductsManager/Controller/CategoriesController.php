<?php

namespace PrismProductsManager\Controller;

use Elasticsearch\Endpoints\Update;
use PrismContentManager\Service\UpdateNavigation;
use PrismElasticSearch\Enum\ElasticSearchTypes;
use PrismQueueManager\Service\QueueService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use PrismProductsManager\Form\AddCategoryForm;
use PrismProductsManager\Form\SearchForm;
use PrismProductsManager\Model\CategoriesMapper;
use PrismProductsManager\Form\AddVariantToCategoryForm;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;
use Zend\Json\Json;
use Zend\Http\Client;
use Zend\Http\Request;


/**
 * Class CategoriesController
 * @package PrismProductsManager\Controller
 */
class CategoriesController extends AbstractActionController
{
    /**
     * @var AddCategoryForm
     */
    protected $_addCategoryForm;

    /**
     * Inject the Search Form
     */
    protected $_searchForm;
    /**
     * Inject the categories mapper
     *
     * @var object
     */
    protected $_categoriesMapper;
    /**
     * Injection of the DBS Adaptor
     */
    protected $_dbAdaptor;
    /**
     * Injection of the Zf2 service locator
     */
    protected $_serviceLocator;

    /**
     * @var
     */
    protected $_simplePagination;
    /**
     * @var
     */
    protected $_queueService;
    /**
     * @var
     */
    protected $_productsMapper;

    /**
     * @var AddVariantToCategoryForm
     */
    protected $_addVariantToCategoryForm;

    /**
     * @var UpdateNavigation
     */
    protected $_updateNavigationService;

    /**
     * @var string
     */
    protected $_websiteUrl;

    /**
     * @var string
     */
    protected $_languageId;

    /**
     * @var string
     */
    protected $_websiteId;

    /**
     * @var
     */
    protected $_siteConfig;

    /**
     * @var string
     */
    protected $clientConfig;
    /**
     * @param AddCategoryForm $addCategoryForm
     * @param CategoriesMapper $categoriesMapper
     * @param $_dbAdapter
     * @param \PrismProductsManager\Form\SearchForm $searchForm
     */
    public function __construct(
        AddCategoryForm $addCategoryForm, CategoriesMapper $categoriesMapper,
        $_dbAdapter, SearchForm $searchForm, $services, $simplePagination,
        QueueService $queueService, $productsMapper, AddVariantToCategoryForm $addVariantToCategoryForm,
        UpdateNavigation $updateNavigationService, $websiteUrl, $languageId, $websiteId, $config,$clientConfig
    )
    {
        $this->_addCategoryForm = $addCategoryForm;
        $this->_categoriesMapper = $categoriesMapper;
        $this->_dbAdaptor = $_dbAdapter;
        $this->_searchForm = $searchForm;
        $this->_serviceLocator = $services;
        $this->_simplePagination = $simplePagination;
        $this->_queueService = $queueService;
        $this->_productsMapper = $productsMapper;
        $this->_addVariantToCategoryForm = $addVariantToCategoryForm;
        $this->_updateNavigationService = $updateNavigationService;
        $this->_websiteUrl = $websiteUrl;
        $this->_languageId = $languageId;
        $this->_websiteId = $websiteId;
        $this->_siteConfig = $config;
        $this->_clientConfig = $clientConfig;
    }

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $addCategoryForm = $this->_addCategoryForm;
        $addVariantToCategoryForm = $this->_addVariantToCategoryForm;

        $request = $this->getRequest();
        $submitedFormData = $this->params()->fromPost();
        $searchData = $request->getPost();

        // Get all the categories and inject them into form
        $parentCategories = $this->_categoriesMapper->fetchAllMarketingCategories('list');
        $addCategoryForm->get('categoryParentId')->setValueOptions($parentCategories);

        $search = $this->params()->fromRoute('search');

        $orderBy = $this->params()->fromRoute('orderby');
        $orderbyOption = $this->params()->fromRoute('orderbyoption');
        $categoryid = $this->params()->fromRoute('categoryid');

        // If the request if for search get the groups using search params
        if (isset($search) && !empty($search)) {
            //Get Groups with all children's from search
            // Get all merchant categories and process them into a hierarchy for the front end
            $allCategories = $this->_categoriesMapper->fetchAllMarketingCategories('all', urldecode($search));

            // If search results are empty redirect to main page without search
            if (empty($allCategories)) {
                $route = array(
                    'controller' => 'Categories',
                    'action' => 'index',
                );
                $type = 'error';
                $message = 'Cannot find any results.';
                $this->_errorValue($route, $type, $message);
            }
        } else {
            //Get Groups with all children's
            // Get all merchant categories and process them into a hierarchy for the front end
            $allCategories = $this->_categoriesMapper->fetchAllMarketingCategories('all');
        }

        // Get all the categories in a hierarchy
        $hierarchyCategories = $this->_categoriesMapper->processGroupCategories($allCategories, $search);

        //Set up the pagination
        $matches = $this->getEvent()->getRouteMatch();
        $route = $this->_simplePagination->getRoute($matches);
        $page = $matches->getParam('page', 1);

        $paginator = $this->_simplePagination->load($matches, $hierarchyCategories, $nrPerPage = 8);

        if (isset($search) && !empty($search)) {
            $search = '/search/' . $search;
            $paginator->setItemCountPerPage(8);
        } else {
            $paginator->setItemCountPerPage(4);
        }
        // Get all websites and inject them into form
        $websites = $this->_categoriesMapper->fetchAllWebsites();
        $defaultWebsite = $this->_categoriesMapper->fetchDefaultWebsite();
        $addCategoryForm->get('websites')->setValueOptions($websites);
        $addCategoryForm->get('websites')->setValue($defaultWebsite);

        //Some flags to be used by the categories popup
        $savedMarketingCategoryProducts = array();
        $savedMarketingCategoryId = 0;

        $addVariantToCategoryForm->setAttribute('action', $addVariantToCategoryForm->getAttribute('action').'/page/'.$page);

        $showRemoveProductButton = false;

        $valid['add'] = false;
        if ($request->isPost()) {
            // In case of search redirect to search method
            if (isset($submitedFormData['search'])) {
                $route = array(
                    'controller' => 'Categories',
                    'action' => 'index',
                );
                // Set data for form and check if the search form is valid
                $this->_searchForm->setData($searchData);
                // Check if search form is valid
                if ($this->_searchForm->isValid()) {
                    $search = $this->_searchForm->getData();
                    $type = 'success';
                    $message = 'Search results for ' . $search['search'];
                    $route['search'] = urlencode($search['search']);
                } else {
                    $type = 'error';
                    $message = 'Cannot find any results.';
                }

                //Redirect based on the message and route
                $this->_errorValue($route, $type, $message);
            }

            // Check if request came from the add variant to categories form
            if (isset($submitedFormData['saveProductCodes']) && isset($submitedFormData['marketingCategoryId'])) {
                $data = $this->_addVariantsToCategories($submitedFormData, $addVariantToCategoryForm);
                $savedMarketingCategoryProducts = $data['savedMarketingCategoryProducts'];
                $dataImploded = '';
                if (isset($savedMarketingCategoryProducts['nonExistingProducts'])) {
                    $dataImploded = implode("\r\n", $savedMarketingCategoryProducts['nonExistingProducts']);
                }
                $addVariantToCategoryForm->get('productCodes')->setValue($dataImploded);
                $addVariantToCategoryForm->get('showAddVariantPopup')->setValue(1);
                $addVariantToCategoryForm->get('showRemoveVariantPopup')->setValue(0);
                $addVariantToCategoryForm->get('marketingCategoryName')->setValue($submitedFormData['marketingCategoryName']);
            } else if (isset($submitedFormData['removeProductCodes']) && isset($submitedFormData['marketingCategoryId'])) {
                $data = $this->_removeVariantsFromCategories($submitedFormData, $addVariantToCategoryForm);
                $savedMarketingCategoryProducts = $data['removedMarketingCategoryProducts'];
                $dataImploded = '';
                if (isset($savedMarketingCategoryProducts['productsNotFoundInThiscategory'])) {
                    $dataImploded = implode("\r\n", $savedMarketingCategoryProducts['productsNotFoundInThiscategory']);
                }
                if (isset($savedMarketingCategoryProducts['nonExistingProducts'])) {
                    $dataImploded .= "\r\n";
                    $dataImploded .= implode("\r\n", $savedMarketingCategoryProducts['nonExistingProducts']);
                }
                if (isset($savedMarketingCategoryProducts['defaultCategoryProducts']) && count($savedMarketingCategoryProducts['defaultCategoryProducts']) > 0) {
                    $showRemoveProductButton = true;
                    $dataImploded .= "\r\n";
                    $dataImploded .= implode("\r\n", $savedMarketingCategoryProducts['defaultCategoryProducts']);
                }
                $addVariantToCategoryForm->get('productCodes')->setValue($dataImploded);
                $addVariantToCategoryForm->get('showRemoveVariantPopup')->setValue(1);
                $addVariantToCategoryForm->get('showAddVariantPopup')->setValue(0);
                $addVariantToCategoryForm->get('marketingCategoryName')->setValue($submitedFormData['marketingCategoryName']);
            } else if (isset($submitedFormData['updatePriority'])) {
                $data = $submitedFormData;
                $updatedPriorities = $this->_updateVariantProductPriorities($data);
                //Set the flag to show the manage products popup
                $addVariantToCategoryForm->get('marketingCategoryId')->setValue($submitedFormData['categoriesId']);
                $addVariantToCategoryForm->get('showManageProductsGroup')->setValue(1);
            } else {

                // Flag used to determine if the category name is the same with the name from the database
                // Needed for form validation of unique
                $duplicateFlag = true;
                if (!empty($submitedFormData['categoryId'])) {
                    if ($this->_categoriesMapper->fetchMarketingCategory($submitedFormData)) {
                        $duplicateFlag = false;
                    }
                } else {
                    if (!isset($submitedFormData['search'])) {
                        //Check to see if the new category name has the same parent as an existing one.
                        $validateSameParent = true;
                        $sameParent = $this->_categoriesMapper->fetchMarketingCategory($submitedFormData, $validateSameParent);
                        if ($sameParent === false) {
                            $duplicateFlag = false;
                            $addCategoryForm->setValidateCategoryNameFlag(false);
                        }
                    }
                }

                // Set data for the form
                $addCategoryForm->setData($submitedFormData);
                // Set the data for the mapper
                $addCategoryForm->setDBAdapter($this->_dbAdaptor);
                $addCategoryForm->setSubmittedData($submitedFormData, $duplicateFlag);

                if (!empty($submitedFormData['categoryId'])) {
                    if ($this->_categoriesMapper->checkIfCategoryHasChangedUrl($submitedFormData)) {
                        $addCategoryForm->setDuplicateUrlFlag(true);
                    }
                }

                if ($addCategoryForm->isValid()) {
                    $data = $addCategoryForm->getData();
                    $this->_categoriesMapper->setSubmittedData($data);
                    if (empty($submitedFormData['categoryId'])) {
                        $this->isCmsUserAuthorized(array('add'));
                        // Enter here if add new category
                        $this->_categoriesMapper->saveMarketingCategory();
                        $route = array(
                            'controller'=>'Categories',
                            'action' => 'index'
                        );
                        $this->_errorValue($route, 'success', 'The category has been added.');
                    } else {
                        $navigationDataUpdate = array(
                            'source' => 'category',
                            'title' => $data['categoryName'],
                            'href' => 'category/' . $data['urlName'],
                            'id' => $data['categoryId'],
                            'action' => 'update'
                        );
                        $this->_updateNavigationService->synchronizeNavigation($navigationDataUpdate);
                        $this->isCmsUserAuthorized(array('edit'));
                        // Enter here if edit existing category

                        $this->_categoriesMapper->updateMarketingCategory();
                        // update products in elastic-search
                        $productsIdsArray = $this->_categoriesMapper->getProductsIdsFromCategory($data['categoryId']);

                        foreach ($productsIdsArray as $productId) {
                            $this->_queueService->addIndexEntryToQueue($productId.'_0', ElasticSearchTypes::PRODUCTS);
                        }
                        $route = array(
                            'controller'=>'Categories',
                            'action' => 'index'
                        );
                        $this->_errorValue($route, 'success', 'The category has been modified.');
                    }
                } else {
                    $valid['add'] = true;
                    $valid['edit'] = true;
                }
            }
        }
        return new ViewModel(array(
            'addCategoryForm' => $addCategoryForm,
            'valid' => $valid,
            'submitedFormData' => $submitedFormData,
            'hierarchyCategories' => $hierarchyCategories,
            'paginator' => $paginator,
            'searchForm' => $this->_searchForm,
            'addVariantToCategoryForm' => $addVariantToCategoryForm,
            'search' => $search,
            'route' => $route,
            'page' => $page,
            'savedMarketingCategoryProducts' => $savedMarketingCategoryProducts,
            'savedMarketingCategoryId' => $savedMarketingCategoryId,
            'orderby' => $orderBy,
            'orderbyOption' => $orderbyOption,
            'categoryId' => $categoryid,
            'showRemoveProductButton' => $showRemoveProductButton
        ));
    }

    /**
     * @param $data
     * @return array|int
     */
    protected function _updateVariantProductPriorities($data)
    {
        $variantProritiesArray = $data['variantPriority'];
        $categoryId = $data['categoriesId'];
        $updatedvariantProductsPriorities = array();
        if (count($variantProritiesArray) >0 && $categoryId > 0) {
            //$updatedvariantProductsPriorities = $this->_categoriesMapper->updateVariantProductPriorities($variantProritiesArray, $categoryId);
            $updatedvariantProductsPriorities = $this->_categoriesMapper->updateVariantProductPrioritiesReloaded($variantProritiesArray, $categoryId);
        }

        return $updatedvariantProductsPriorities;
    }

        /**
     * Function to remove variants from marketing category
     *
     * @param type $submitedFormData
     * @param type $addVariantToCategoryForm
     * @return type
     */
    protected function _removeVariantsFromCategories($submitedFormData, $addVariantToCategoryForm)
    {
        $productCodesArray = array();
        $addVariantToCategoryForm->setData($submitedFormData);
        $removedMarketingCategoryProducts = array();
        if ($addVariantToCategoryForm->isValid()) {
            $data = $addVariantToCategoryForm->getData();
            if (isset($data['productCodes'])) {
                $productCodesArray = explode("\r\n", $data['productCodes']);
                if (count($productCodesArray) == 0) {
                    $productCodesArray = explode(",", $data['productCodes']);
                }
            }
            if (count($productCodesArray) > 0) {
                $removeMarketingCategoryId = $data['marketingCategoryId'];
                $removedMarketingCategoryProducts = $this->_categoriesMapper->removeVariantsFromMarketingCategory($productCodesArray, $removeMarketingCategoryId);

                foreach($productCodesArray as $productCode) {
                    if (!in_array($productCode, $removedMarketingCategoryProducts['nonExistingProducts'])) {
                        $key = array_search($productCode, $productCodesArray);
                        unset($productCodesArray[$key]);
                    }
                }
            }
        }

        return array('unprocessedProductCodesArray' => $productCodesArray, 'removedMarketingCategoryProducts' => $removedMarketingCategoryProducts);
    }

    /**
     * Function to add variants to marketing category
     *
     * @param type $submitedFormData
     * @param type $addVariantToCategoryForm
     * @return type
     */
    protected function _addVariantsToCategories($submitedFormData, $addVariantToCategoryForm)
    {
        $productCodesArray = array();
        $addVariantToCategoryForm->setData($submitedFormData);
        $savedMarketingCategoryProducts = array();
        if ($addVariantToCategoryForm->isValid()) {
            $data = $addVariantToCategoryForm->getData();
            //$productCodesArray = array();
            if (isset($data['productCodes'])) {
                $productCodesArray = explode("\r\n", $data['productCodes']);
                if (count($productCodesArray) == 0) {
                    $productCodesArray = explode(",", $data['productCodes']);
                }
            }
            if (count($productCodesArray) > 0) {
                $savedMarketingCategoryId = $data['marketingCategoryId'];
                $savedMarketingCategoryProducts = $this->_categoriesMapper->saveVariantsForMarketingCategory($productCodesArray, $savedMarketingCategoryId);

                /*foreach($productCodesArray as $productCode) {
                    if (!in_array($productCode, $savedMarketingCategoryProducts)) {
                        $key = array_search($productCode, $productCodesArray);
                        unset($productCodesArray[$key]);
                    }
                }*/
            }
        }

        return array('unprocessedProductCodesArray' => $productCodesArray, 'savedMarketingCategoryProducts' => $savedMarketingCategoryProducts);
    }

    /**
     * Used to delete Marketing categories
     * @return null
     */
    public function deleteAction()
    {
        $this->isCmsUserAuthorized(array('delete'));
        $route = array(
            'controller'=>'Categories',
            'action' => 'index'
        );
        $data = $this->params()->fromRoute();

        // Check if the category has been used in products
        // False for not used and true for used
        $usedFlag = false;
        if ($this->_categoriesMapper->checkMarketingCategoryOnProducts($data['id'])) {
            $usedFlag = true;
        }

        // Check if the category is locked ( front end validation is ok but we need to play it safe )
        if ($this->_categoriesMapper->checkMarketingCategoryLocked($data['id'])) {
            $usedFlag = true;
            $this->_errorValue($route,'error','You cannot delete a locked category.');
        }

        //Check if it's a default categroy for any product
        $defaultCategoryProducts = $this->_categoriesMapper->checkIfMarketingCategoryIsDefaultForAnyProduct($data['id']);
        $defaultProductDetails = $this->_productsMapper->getDefaultProductDetailsByProductIdAndProductIdFrom($defaultCategoryProducts);
        $isSetAsDefaultCategoryForAProduct = isset($defaultProductDetails['isSetAsDefaultCategoryForAProduct'])
                                                ? $defaultProductDetails['isSetAsDefaultCategoryForAProduct'] : false;
        $defaultCategoryProductsArray = isset($defaultProductDetails['defaultCategoryProductsArray'])
                                                ? $defaultProductDetails['defaultCategoryProductsArray'] : array();

        if (!$usedFlag && $isSetAsDefaultCategoryForAProduct === false) {
            // Delete category ( set status to deleted )
            $navigationDataUpdate = array(
                'source' => 'category',
                'title' => '',
                'href' => '',
                'id' => $data['id'],
                'action' => 'delete'
            );
            $this->_updateNavigationService->synchronizeNavigation($navigationDataUpdate);

            $this->_categoriesMapper->deleteMarketingCategory($data['id']);
            // Set var for redirect
            $message = 'The product marketing category has been deleted';
            $type = 'success';
        }
        else if ($isSetAsDefaultCategoryForAProduct === true) {
            // Set var for redirect
            $productNameList = '';
            if (count($defaultCategoryProductsArray) > 0) {
                $productNameList = '<ul>';
                foreach ($defaultCategoryProductsArray as $productName) {
                    if (trim($productName) != '') {
                        $productNameList .= "<li>{$productName}</li>";
                    }
                }
                $productNameList .= '</ul>';
            }
            $message = 'This category is set as default for some products so you cannot delete it. '
                    . 'Please change the default marketing category for the product(s) first.<br>' . $productNameList;
            $type = 'error';
        } else {
            // Set var for redirect
            $message = 'The product marketing category is used in products and cannot be deleted';
            $type = 'error';
        }
        $this->_errorValue($route, $type, $message);
    }

    /**
     * Redirect in case of error
     * @param $route
     * @param $type
     * @param $message
     *
     * @param string $alert
     * @return null
     */
    protected function _errorValue($route, $type, $message, $alert = [])
    {
        $alert['type'] = $type;
        $alert['bold'] = ucfirst($type).':';
        $alert['messages'] = array();
        $alert['messages'][0] = $message;

        $this->flashmessenger()->setNamespace('alert')
            ->addMessage($alert);

        return $this->redirect()->toRoute(null ,$route);
    }
    /**
     * Ajax function used for enableing categories
     * Set 1/0 in the Categories_definition Table
     */
    public function enableCategoryAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $params = $this->params()->fromPost();

        $this->_categoriesMapper->enableCategory($params);
        if ($params['checked'] == '0') {
            $this->_clearCategoryCache($params['id']);
        }
        $response = $this->getResponse();
        $response->setStatusCode(200);
        $response->setContent(true);
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/json');

        return $response;
    }
    /**
     * Ajax function used for locking / unlocking
     */
    public function lockCategoryAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $params = $this->params()->fromPost();
        $affectedRows = $this->_categoriesMapper->changeLockCategory($params);

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/json');
        echo json_encode(array('affectedRows'=>$affectedRows));
        return $response;
    }

    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function getAllProductsForSpecificMarketingCategoryAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $params = $this->params()->fromPost();
        $orderBy = $params['orderBy'];
        $orderByOption = $params['orderByOption'];
        //$products = $this->_categoriesMapper->getProductsForMarketingCategory($params['id']);
        $products = $this->_categoriesMapper->getProductsWithColourForMarketingCategory($params['id'], $styleColourLength = 9, $sku = '', $orderBy, $orderByOption);
        $marketingGroupName = $this->_categoriesMapper->getMarketingCategoryById($params['id']);

        $productsView = new ViewModel();
        $productsView->setTerminal(true);
        $productsView->setTemplate('prism-products-manager/categories/partials/add-products-to-category');
        $productsView->setVariable('products', $products);
        $productsView->setVariable('categoryId', $params['id']);
        $productsHtmlOutput = $this->getServiceLocator()
            ->get('viewrenderer')
            ->render($productsView);
        $productsHtmlOutput = utf8_encode($productsHtmlOutput);

        $productCount = $products['productCount'];
        $variantCount = $products['variantCount'];

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $response->setContent(json_encode(array('html' => $productsHtmlOutput, 'marketingGroup' => $marketingGroupName, 'productCount' => $productCount, 'variantCount' => $variantCount)));
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/html');

        return $response;
    }

    /**
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function getAllProductsFromSearchAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $params = $this->params()->fromPost();
        //$products = $this->_categoriesMapper->getAllProductsFromSearch($params['search'], $params['id']);
        $styleColourLength = 9;
        $products = $this->_categoriesMapper->getProductsWithColourForMarketingCategory($params['id'], $styleColourLength, $params['search']);

        $productsView = new ViewModel();
        $productsView->setTerminal(true);
        //$productsView->setTemplate('prism-products-manager/categories/partials/search-add-products-to-category');
        $productsView->setTemplate('prism-products-manager/categories/partials/add-products-to-category');
        $productsView->setVariable('products', $products);
        $productsView->setVariable('categoryId', $params['id']);
        $productsHtmlOutput = $this->getServiceLocator()
            ->get('viewrenderer')
            ->render($productsView);
        $productsHtmlOutput = utf8_encode($productsHtmlOutput);

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $response->setContent($productsHtmlOutput);
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/html');

        return $response;

    }

    /**
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function addProductToMarketingGroupAction()
    {
        $this->isCmsUserAuthorized(array('add'));
        $params = $this->params()->fromPost();
        $this->_categoriesMapper->addProductToMarketingCategory($params['productId'], $params['categoryId']);

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $response->setContent('');
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/html');

        return $response;
    }

    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function deleteProductFromCategoryAction()
    {
        $this->isCmsUserAuthorized(array('delete'));
        $params = $this->params()->fromPost();
        $this->_categoriesMapper->deleteProductFromMarketingCategory($params['categoryId'], $params['productId']);

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $response->setContent('');
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/html');

        return $response;
    }

    /**
     *
     */
    public function moveMarketingCategoryUpAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $params = $this->params()->fromRoute();
        $categoryId = $params['id'];
        $position = $params['position'];
        $page = $params['page'];

        if ($this->_categoriesMapper->updatePositionWithPreviousAvailableCategoryPosition($categoryId, $position, 'up')) {
            $type = 'success';
            $message = 'Category has been moved up in the hierarchy.';
        } else {
            $type = 'error';
            $message = 'Category has reached the top in the hierarchy.';
        }

        $route = array(
            'controller'=>'Categories',
            'action' => 'index',
            'page' => $page
        );

        $this->_errorValue($route, $type, $message);
    }

    /**
     *
     */
    public function moveMarketingCategoryDownAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $params = $this->params()->fromRoute();
        $categoryId = $params['id'];
        $position = $params['position'];
        $page = $params['page'];

        if ($this->_categoriesMapper->updatePositionWithPreviousAvailableCategoryPosition($categoryId, $position, 'down')) {
            $type = 'success';
            $message = 'Category has been moved down in the hierarchy.';
        } else {
            $type = 'error';
            $message = 'Category has reached the bottom in the hierarchy.';
        }

        $route = array(
            'controller'=>'Categories',
            'action' => 'index',
            'page' => $page
        );

        $this->_errorValue($route, $type, $message);
    }

    /**
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function getAttributesForMarketingCategoryAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $params = $this->params()->fromPost();
        $categoryId = $params['categoryId'];
        $search = $params['search'];

        $attributesGroups = $this->_categoriesMapper->getAttributesGroupsForMarketingCategory($categoryId, $search);
        $category = $this->_categoriesMapper->getMarketingCategoryById($categoryId);

        $marketingCategoriesView = new ViewModel();
        $marketingCategoriesView->setTerminal(true);
        $marketingCategoriesView->setTemplate('prism-products-manager/categories/partials/manage-attributes-for-category');
        $marketingCategoriesView->setVariable('groups', $attributesGroups);
        $marketingCategoriesView->setVariable('category', $category);
        $marketingCategoriesView->setVariable('categoryId', $categoryId);

        $htmlOutput = $this->_serviceLocator
            ->get('viewrenderer')
            ->render($marketingCategoriesView);

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $response->setContent($htmlOutput);
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/html');

        return $response;
    }

    /**
     *
     */
    public function addAttributeToGroupAction()
    {
        $this->isCmsUserAuthorized(array('add'));
        $params = $this->params()->fromPost();
        $checked = $params['checked'];
        $attributeGroup = $params['attributeGroup'];
        $categoryId = $params['categoryId'];

        $this->_categoriesMapper->addAttributeToMarketingGroup($checked, $attributeGroup, $categoryId);

        echo $checked;echo $attributeGroup;die;
    }

    /**
     * @param $categoryId
     * @return string
     */
    protected function _clearCategoryCache($categoryId)
    {
        $category = $this->_categoriesMapper->_getMarketingCategoryById($categoryId);

        $websites =  $this->_siteConfig['siteConfig']['websites']['clear-cache'];

        foreach ($websites as $website) {
            $url = sprintf(
                "%sadmin-tasks/clear-category-cache/%s/%s/%s",
                $website,
                $this->_websiteId,
                $this->_languageId,
                $category['urlName']
            );

            $client = new Client();
            $client->setEncType(Client::ENC_URLENCODED);
            $client->setUri($url);
            $client->setMethod(Request::METHOD_GET);

            $response = $client->send();
            $result = $response->getBody();
            if (!$response->isSuccess()) {
                //@TODO add the action to cron so the cache will be updated later
            }
        }

        return $result;
    }
}
