<?php
namespace PrismProductsManager\Controller;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use PrismContentManager\Model\PagesMapper;
use PrismElasticSearch\Enum\ElasticSearchTypes;
use PrismProductsManager\Model\CategoriesMapper;
use PrismProductsManager\Model\ProductsMapper;
use PrismQueueManager\Service\QueueService;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class ApiSitemapController extends AbstractRestfulController implements ControllerApiAwareInterface
{
    protected $_categoryMapper;
    protected $_pagesMapper;
    protected $_config;

    public function __construct(CategoriesMapper $categoryMapper, PagesMapper $pagesMapper, $config)
    {
        $this->_categoryMapper = $categoryMapper;
        $this->_pagesMapper = $pagesMapper;
        $this->_config = $config;
    }

    public function get($id)
    {
        $this->_methodNotAllowed();
    }
    public function getList()
    {
        $categories = $this->_categoryMapper->fetchAllMarketingCategories('', null, true, true);
        $returnCategory = array();
        foreach ($categories as $category) {
            $returnCategory[] = array(
                'categoryName' => $category['categoryName'],
                'categoryUrl' => $this->_config['siteConfig']['websites']['website-url'][1] . 'category/' . $category['urlName']
            );
        }
        $returnConfig = $this->_config['siteConfig']['sitemap'];

        $pages = $this->_pagesMapper->getAllPages();
        $returnPages = array();
        foreach ($pages as $page) {
            if ($page['isLandingPage'] != '1') {
                $returnPages[] = array(
                    'pageName' => $page['contentName'],
                    'pageUrl' => $this->_config['siteConfig']['websites']['website-url'][1] . 'pages/' . $page['urlName'],
                );
            }
        }
        $returnArray = array(
            'categories' => $returnCategory,
            'config' => $returnConfig,
            'pages' => $returnPages,
            'website' => $this->_config['siteConfig']['websites']['website-url'][1]
        );

        return new JsonModel($returnArray);;

    }

    public function create($data)
    {
        $this->_methodNotAllowed();
    }

    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}