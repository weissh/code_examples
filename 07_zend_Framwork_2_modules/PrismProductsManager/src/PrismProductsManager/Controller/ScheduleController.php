<?php

namespace PrismProductsManager\Controller;

use PrismElasticSearch\Enum\ElasticSearchTypes;
use Zend\Console\Request as ConsoleRequest;
use PrismProductsManager\Form\MarketingCategorySelectForm;
use PrismProductsManager\Form\MerchantCategorySelectForm;
use PrismProductsManager\Form\ProductAttributesSelectForm;
use PrismProductsManager\Form\ProductVariantsCreationForm;
use PrismProductsManager\Form\ProductsActivationForm;
use PrismProductsManager\Form\ProductsBasicInformationForm;
use PrismProductsManager\Form\ProductsRelatedProductForm;
use PrismProductsManager\Form\ProductSupplierPriceForm;
use PrismProductsManager\Form\SearchForm;
use PrismProductsManager\Model\CategoriesMapper;
use PrismProductsManager\Model\ColoursMapper;
use PrismProductsManager\Model\MerchantCategoriesMapper;
use PrismProductsManager\Model\ProductsMapper;
use PrismProductsManager\Model\TaxMapper;
use PrismProductsManager\Service\InputService;
use PrismProductsManager\Service\ScheduleService;
use PrismQueueManager\Service\QueueService;
use Zend\Console\Adapter\AdapterInterface;
use Zend\Console\Console;
use Zend\Form\Form;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use PrismProductsManager\Model\BulkActionsMapper;

/**
 * Class ScheduleController
 * @package PrismProductsManager\Controller
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class ScheduleController extends AbstractActionController
{
    /**
     * @var ScheduleService
     */
    protected $scheduleService;

    /**
     * @var Object
     */
    protected $viewRenderer;
    /**
     * @var InputService
     */
    protected $inputService;

    /**
     * @var BulkActionsMapper
     */
    protected $bulkActionMapper;

    /**
     * @param ScheduleService $scheduleService
     * @param $viewRenderer
     * @param InputService $inputService
     */
    public function __construct(
        ScheduleService $scheduleService,
        $viewRenderer,
        InputService $inputService,
        BulkActionsMapper $bulkActionMapper
    )
    {
        /** Services */
        $this->scheduleService = $scheduleService;
        $this->viewRenderer = $viewRenderer;
        $this->inputService = $inputService;
        $this->bulkActionMapper = $bulkActionMapper;
    }

    public function scheduleEventsProcessAction()
    {
        if ($this->getRequest() instanceof ConsoleRequest) {
            $this->scheduleService->processAllScheduledEvents();
        } else {
            throw new \Exception('Denied !');
        }
    }
    /**
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function getScheduledEntriesForPricesAction()
    {
        $this->isCmsUserAuthorized(array('view'));

        $request = $this->getRequest();

        //Retrieve the submitted data form.
        $params = $this->params()->fromPost();

        if ($request->isPost()) {
            $scheduleEntries = $this->scheduleService->getAllScheduledPriceEntriesForField($params);
            $possibleValues = $this->scheduleService->getAllPossiblePriceValuesToSchedule($params);
            // get schedule from bulk process
            $bulkSchedule = $this->bulkActionMapper->getScheduledPrices($params);
            return $this->getModalForManagement($params, $scheduleEntries, $possibleValues, false, $bulkSchedule);
        }

        $response = $this->getResponse();
        $response->setStatusCode(401);
        $response->setContent('Unauthorized');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }

    /**
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function getScheduledEntriesForAttributesAction()
    {
        $this->isCmsUserAuthorized(array('view'));

        $request = $this->getRequest();

        $params = $this->params()->fromPost();

        if ($request->isPost()) {
            $scheduleEntries = $this->scheduleService->getAllScheduledAttributesEntriesForField($params);
            $possibleValues = $this->scheduleService->getAllPossibleAttributesValuesToSchedule($params);

            $fieldName = $this->inputService->getInputFieldLabelName($params['inputFieldId']);

            // get schedule from bulk process
            $bulkSchedule = $this->bulkActionMapper->getScheduledBulkProcessesByKey($fieldName, $params['code']);

            return $this->getModalForManagement($params, $scheduleEntries, $possibleValues, $fieldName, $bulkSchedule);
        }
        return $this->error();
    }

    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function getAllScheduleEventsAction()
    {
        $this->isCmsUserAuthorized(array('view'));

        $request = $this->getRequest();

        $params = $this->params()->fromPost();

        if ($request->isPost()) {
            $scheduleEntries = $this->scheduleService->getAllScheduledEventsForProduct($params);
            $optionOrStyleCode = $params['code'];
            return $this->getModalForManagementAllSchedules($params, $scheduleEntries, $optionOrStyleCode);
        }
        return $this->error();
    }

    /**
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function saveScheduledEntriesAction()
    {
        $this->isCmsUserAuthorized(array('add'));
        $request = $this->getRequest();

        $params = $this->params()->fromPost();

        if ($request->isPost()) {
            $scheduledDate = isset($params['scheduledDate']) ? $params['scheduledDate'] : false;
            $scheduledParams = isset($params['scheduledParams']) ? json_decode($params['scheduledParams']) : false;
            $scheduledSelectValue = isset($params['scheduledSelectValue']) ? $params['scheduledSelectValue'] : false;
            $scheduledValue = isset($params['scheduledValue']) ? $params['scheduledValue'] : false;

            $scheduleEntries = array();
            $possibleValues = array();
            $errors = $this->checkIfDataIsValid($scheduledParams, $scheduledSelectValue, $scheduledValue, $scheduledDate);
            $successFlag = false;
            if (empty($errors)) {
                if (isset($scheduledParams->priceTable)) {
                    $successFlag = $this->scheduleService->savePriceScheduleEntry($scheduledDate, $scheduledSelectValue, $scheduledValue, $scheduledParams);
                    $scheduleEntries = $this->scheduleService->getAllScheduledPriceEntriesForField((array)$scheduledParams);
                    $possibleValues = $this->scheduleService->getAllPossiblePriceValuesToSchedule((array)$scheduledParams);
                } else {
                    $successFlag = $this->scheduleService->saveAttributesScheduleEntry($scheduledDate, $scheduledSelectValue, $scheduledValue, $scheduledParams);
                    $scheduleEntries = $this->scheduleService->getAllScheduledAttributesEntriesForField((array)$scheduledParams);
                    $possibleValues = $this->scheduleService->getAllPossibleAttributesValuesToSchedule((array)$scheduledParams);
                }
            }

            if ($successFlag) {
                return $this->getModalForManagement((array)$scheduledParams, $scheduleEntries, $possibleValues);
            } else {
                $response = $this->getResponse();
                $response->setContent(json_encode(array('html' => false, 'errors' => $errors)));
                $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
                return $response;
            }
        }
        return $this->error();
    }

    /**
     * @param $scheduledParams
     * @param $scheduledSelectValue
     * @param $scheduledValue
     * @param $scheduledDate
     * @return bool
     */
    private function checkIfDataIsValid($scheduledParams, $scheduledSelectValue, $scheduledValue, $scheduledDate)
    {

        if ($scheduledSelectValue || !$scheduledValue) {
            $value = $scheduledSelectValue;
        } else {
            $value = $scheduledValue;
        }
        $inputFieldsForm = $this->getServiceLocator()->get('FormElementManager')->get('PrismProductsManager\Form\InputFieldsForm');

        $inputName = str_replace('button-', '', $scheduledParams->inputName);

        /** Validate scheduled value */
        $validatorChain = $inputFieldsForm->getInputFilter()->get($inputName)->getValidatorChain();

        $form = new Form();
        $form->add(array(
            'type' => 'Zend\Form\Element\Text',
            'name' => 'value',
            'attributes' => array(),
            'options' => array(),
        ));
        $form->getInputFilter()->get('value')->setValidatorChain($validatorChain);
        $data = array('value' => $value);
        $form->setData($data);
        $errors = array();

        /** Validate scheduled Date */
        $dateValidator = new \Zend\Validator\Date(array('format' => 'Y-m-d H:i:s'));
        if (!$dateValidator->isValid($scheduledDate)) {
            $errors['date'] = 'Not a valid date.';
        }

        /** Validate value */
        if (strlen($value) == 0) {
            $errors['value'][] = 'Value cannot be empty';
        }
        if ($form->isValid() && empty($errors)) {
            return $errors;
        } else {
            if (isset($form->getMessages()['value'])) {
                foreach ($form->getMessages()['value'] as $message) {
                    $errors['value'][] = $message;
                }
            }
        }
        return $errors;
    }

    /**
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function deleteScheduleEventAction()
    {
        $this->isCmsUserAuthorized(array('add'));
        $request = $this->getRequest();

        $params = $this->params()->fromPost();

        if ($request->isPost()) {
            $scheduledParams = isset($params['scheduledParams']) ? json_decode($params['scheduledParams']) : false;
            $scheduleEventId = isset($params['scheduleEventId']) ? $params['scheduleEventId'] : false;
            $bulkScheduleId  = isset($params['bulkScheduleId']) ? $params['bulkScheduleId'] : false;

            if($bulkScheduleId != false){
                return $this->deleteBulkSchedule($scheduledParams, $bulkScheduleId);
            }

            $successFlag = $this->scheduleService->deleteScheduleEvent($scheduleEventId);

            if ($successFlag) {
                if (!empty($scheduledParams->priceTable)) {
                    /** Delete Price */
                    $scheduleEntries = $this->scheduleService->getAllScheduledPriceEntriesForField((array)$scheduledParams);
                    $possibleValues = $this->scheduleService->getAllPossiblePriceValuesToSchedule((array)$scheduledParams);
                    $bulkSchedule = $this->bulkActionMapper->getScheduledPrices((array)$scheduledParams);

                    return $this->getModalForManagement((array)$scheduledParams, $scheduleEntries, $possibleValues, false, $bulkSchedule);
                } elseif (!empty($scheduledParams->inputFieldId)) {
                    /** Delete Attributes */
                    $scheduleEntries = $this->scheduleService->getAllScheduledAttributesEntriesForField((array)$scheduledParams);
                    $possibleValues = $this->scheduleService->getAllPossibleAttributesValuesToSchedule((array)$scheduledParams);

                    $fieldName = $this->inputService->getInputFieldLabelName($params['inputFieldId']);

                    // get schedule from bulk process
                    $bulkSchedule = $this->bulkActionMapper->getScheduledBulkProcessesByKey($fieldName, $params['code']);

                    return $this->getModalForManagement((array)$scheduledParams, $scheduleEntries, $possibleValues, $fieldName, $bulkSchedule);
                } else {
                    /** Delete From Main  */
                    $scheduleEntries = $this->scheduleService->getAllScheduledEventsForProduct((array)$scheduledParams);
                    return $this->getModalForManagementAllSchedules((array)$scheduledParams, $scheduleEntries, $scheduledParams->code);
                }
            } else {
                $response = $this->getResponse();
                $response->setContent(json_encode(array('html' => false)));
                $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
                return $response;
            }
        }
        return $this->error();
    }

    public function deleteBulkSchedule($scheduledParams, $bulkScheduleId)
    {
        $this->bulkActionMapper->deleteBulkProcessById($bulkScheduleId);

        if (!empty($scheduledParams->priceTable)) {
            /** Delete Price */
            $scheduleEntries = $this->scheduleService->getAllScheduledPriceEntriesForField((array)$scheduledParams);
            $possibleValues = $this->scheduleService->getAllPossiblePriceValuesToSchedule((array)$scheduledParams);
            $bulkSchedule = $this->bulkActionMapper->getScheduledPrices((array)$scheduledParams);

            return $this->getModalForManagement((array)$scheduledParams, $scheduleEntries, $possibleValues, false, $bulkSchedule);
        }

        if (!empty($scheduledParams->inputFieldId)) {
            /** Delete Attributes */
            $scheduleEntries = $this->scheduleService->getAllScheduledAttributesEntriesForField((array)$scheduledParams);
            $possibleValues = $this->scheduleService->getAllPossibleAttributesValuesToSchedule((array)$scheduledParams);

            $fieldName = $this->inputService->getInputFieldLabelName($scheduledParams->inputFieldId);

            // get schedule from bulk process
            $bulkSchedule = $this->bulkActionMapper->getScheduledBulkProcessesByKey($fieldName, $scheduledParams->code);

            return $this->getModalForManagement((array)$scheduledParams, $scheduleEntries, $possibleValues, $fieldName, $bulkSchedule);
        }
    }

    /**
     * @param array $params
     * @param $scheduleEntries
     * @param $possibleValues
     * @param bool|false $fieldName
     * @return \Zend\Stdlib\ResponseInterface
     */
    private function getModalForManagement(array $params, $scheduleEntries, $possibleValues, $fieldName = false, $bulkSchedule = null)
    {
        $manageSchedulesModal = new ViewModel();
        $manageSchedulesModal->setTerminal(true);
        $manageSchedulesModal->setTemplate('prism-products-manager/schedule/get-scheduled-entries');
        $manageSchedulesModal->setVariable('scheduleEntries', $scheduleEntries);
        $manageSchedulesModal->setVariable('possibleValues', $possibleValues);
        $manageSchedulesModal->setVariable('params', $params);
        $manageSchedulesModal->setVariable('bulkSchedules', $bulkSchedule);

        if (!is_null($fieldName)) {
            $manageSchedulesModal->setVariable('fieldName', $fieldName);
        } else {
            $manageSchedulesModal->setVariable('fieldName', isset($params['fieldName']) ? $params['fieldName'] : false);
        }

        $manageSchedulesModalOutput = $this->viewRenderer->render($manageSchedulesModal);
        $manageSchedulesModalOutput = utf8_encode($manageSchedulesModalOutput);
        /** Return Response */
        $response = $this->getResponse();
        $response->setContent(json_encode(array('html' => $manageSchedulesModalOutput)));
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @param array $params
     * @param $scheduleEntries
     * @param $optionOrStyleCode
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    private function getModalForManagementAllSchedules(array $params, $scheduleEntries, $optionOrStyleCode)
    {
        $manageSchedulesModal = new ViewModel();
        $manageSchedulesModal->setTerminal(true);
        $manageSchedulesModal->setTemplate('prism-products-manager/schedule/get-all-scheduled-entries');
        $manageSchedulesModal->setVariable('scheduleEntries', $scheduleEntries);
        $manageSchedulesModal->setVariable('optionOrStyleCode', $optionOrStyleCode);
        $manageSchedulesModal->setVariable('params', $params);
        $manageSchedulesModalOutput = $this->viewRenderer->render($manageSchedulesModal);
        $manageSchedulesModalOutput = utf8_encode($manageSchedulesModalOutput);
        /** Return Response */
        $response = $this->getResponse();
        $response->setContent(json_encode(array('html' => $manageSchedulesModalOutput)));
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @return \Zend\Stdlib\ResponseInterface
     */
    private function error()
    {
        $response = $this->getResponse();
        $response->setContent(json_encode(array('html' => false)));
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        return $response;
    }
}