<?php
namespace PrismProductsManager\Controller;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use PrismProductsManager\Model\ApiProductsMapper;


/**
 * Api Products by Categories Id Controller
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiProductsByCategoriesIdController extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * Api Products Mapper
     * PrismProductsManager\Model\ApiProductsMapper
     * @var object
     */
    protected $_apiProductsMapper;

    public function __construct(ApiProductsMapper $apiProductsMapper)
    {
        $this->_apiProductsMapper = $apiProductsMapper;

    }
    public function get($id)
    {
        $websiteId = $this->params()->fromRoute('website');
        $languageId = $this->params()->fromRoute('language');
        $productData = array();
        $validator = new \Zend\Validator\NotEmpty(\Zend\Validator\NotEmpty::INTEGER);
        if ($validator->isValid($id) && $validator->isValid($websiteId) && $validator->isValid($languageId)) {
            $productData =  $this->_apiProductsMapper->getProductsByCategoryId($id, $websiteId, $languageId);
        }

        if (!empty($productData)) {
            return new JsonModel($productData);
        } else {
            throw new \Exception('Product not found', 400);
        }
    }

    public function getList()
    {
        $this->_methodNotAllowed();
    }

    public function create($data)
    {
        $this->_methodNotAllowed();
    }

    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}