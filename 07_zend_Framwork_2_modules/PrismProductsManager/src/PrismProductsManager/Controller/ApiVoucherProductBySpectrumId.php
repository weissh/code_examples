<?php
namespace PrismProductsManager\Controller;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use PrismMediaManager\Model\ProductFolderMapper;
use PrismProductsManager\Model\ApiProductsMapper;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 * Api Voucher Product by Spectrum Id Controller
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiVoucherProductBySpectrumId extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * Api Product Mapper
     * PrismProductsManager\Model\ApiProductsMapper
     * @var object
     */
    protected $_apiProductsMapper;

    /**
     * @var ProductFolderMapper
     */
    protected $_productFolderMapper;

    /**
     * @param ApiProductsMapper $apiProductsMapper
     * @param ProductFolderMapper $productFolderMapper
     */
    public function __construct(
        ApiProductsMapper $apiProductsMapper,
        ProductFolderMapper $productFolderMapper)
    {
        $this->_apiProductsMapper = $apiProductsMapper;
        $this->_productFolderMapper = $productFolderMapper;
    }

    /**
     * @param mixed $id
     * @return array|mixed|void
     * @throws \Exception
     */
    public function get($id)
    {
        $this->_methodNotAllowed();
    }

    /**
     * @return JsonModel
     * @throws \Exception
     */
    public function getList()
    {
        $spectrumId = $this->params()->fromRoute('spectrumId');
        if (empty($spectrumId)) {
            $this->_methodNotAllowed();
        }
        $evoucherProduct = array();
        $intValidator = new \Zend\Validator\NotEmpty(\Zend\Validator\NotEmpty::INTEGER);
        if ($intValidator->isValid($spectrumId)) {
            $evoucherProduct = $this->_apiProductsMapper->getVoucherProductBySpectrumId($spectrumId);
            if ($evoucherProduct) {
                $evoucherProduct['images'] = $this->_productFolderMapper->getSpecialImageByProductAndVariant($evoucherProduct['productId'], 0, 'default', 'full');
            }
        }

        if ($evoucherProduct) {
            $returnArray['success'] = true;
            $returnArray['response'] = array(
                'hits' => array(
                    'total' => 1,
                    'hits' => array(
                        0 => array(
                            '_source' => array(
                                'productId' => $evoucherProduct['productId'],
                                'variantId' => 0,
                                'spectrumId' => $evoucherProduct['spectrumId'],
                                'product_id_colour' => '',
                                'related_products' => array(),
                                'channel' => array(),
                                'name' => $evoucherProduct['productName'],
                                'short_description' => $evoucherProduct['shortDescription'],
                                'description' => $evoucherProduct['longDescription'],
                                'metaTitle' => $evoucherProduct['metaTitle'],
                                'metaDescription' => $evoucherProduct['metaDescription'],
                                'metaKeywords' => $evoucherProduct['metaKeyword'],
                                'care_information' => '',
                                'style' => $evoucherProduct['style'],
                                'sku' => $evoucherProduct['sku'],
                                'qty' => '0',
                                'mis_spells' => '',
                                'tags' => '',
                                'url' => $evoucherProduct['urlName'],
                                'images' => array('basket' => $evoucherProduct['images']['imageUrl']),
                                'categories' => array(),
                                'attributes' => array(),

                            )
                        )
                    )
                )
            );
        } else {
            $returnArray['success'] = false;
            $returnArray['response'] = array(
                'hits' => array(
                    'total' => 0,
                    'hits' => array(
                        0 => array(
                                )
                        )
                    )
                );
        }
        return new JsonModel($returnArray);
    }

    /**
     * @param mixed $data
     * @return array|mixed|void
     * @throws \Exception
     */
    public function create($data)
    {
        $this->_methodNotAllowed();
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|mixed|void
     * @throws \Exception
     */
    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    /**
     * @param mixed $id
     * @return array|mixed|void
     * @throws \Exception
     */
    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}