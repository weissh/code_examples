<?php

namespace PrismProductsManager\Controller;

use PrismElasticSearch\Enum\ElasticSearchTypes;
use PrismQueueManager\Service\QueueService;
use Zend\Mvc\Controller\AbstractActionController;
use PrismProductsManager\Form\BulkDownloadForm;
use PrismProductsManager\Form\BulkUploadForm;
use PrismProductsManager\Form\SearchForm;
use Zend\View\Model\ViewModel;
use PrismProductsManager\Model\BulkActionsMapper;

/**
 * Class BulkActionsController
 * @package PrismProductsManager\Controller
 */
class BulkActionsController extends AbstractActionController
{
    /**
     * @var BulkActionsMapper
     */
    protected $_bulkActionsMapper;

    /**
     *  inject the bulk upload template form
     * @var object
     */
    protected $_bulkDownloadForm;

    /**
     *  inject the search form
     * @var object
     */
    protected $_bulkUploadForm;

    /**
     *  used to check if form (add/edit) is _valid
     * @var boolean
     */
    protected $_valid;

    /**
     * @var
     */
    protected $_siteConfig;

    /**
     * @param BulkDownloadForm $downloadBulkForm
     * @param BulkUploadForm $uploadBulkForm
     * @param QueueService $queueService
     * @param BulkActionsMapper $bulkActionsMapper
     * @param $productsMapper
     * @param $config
     */
    public function __construct(
        BulkDownloadForm $downloadBulkForm,
        BulkUploadForm $uploadBulkForm,
        QueueService $queueService,
        BulkActionsMapper $bulkActionsMapper,
        $productsMapper,
        $config
    ) {
        $this->_bulkDownloadForm = $downloadBulkForm;
        $this->_bulkUploadForm = $uploadBulkForm;
        $this->_queueService = $queueService;
        $this->_productsMapper = $productsMapper;
        $this->_bulkActionsMapper = $bulkActionsMapper;
        $this->_siteConfig = $config;
    }

    /**
     * @return ViewModel
     * @throws \Exception
     */
    public function indexAction()
    {
        $response = array();

        $this->isCmsUserAuthorized(array('view'));

        $availableTemplates = $this->_siteConfig['siteConfig']['bulk-templates'];
        $availableCurrencies = $this->_siteConfig['siteConfig']['currencies']['currencies-list'];

        $bulkDownloadForm = $this->_bulkDownloadForm;
        $bulkDownloadForm->get('downloadType')->setValueOptions($availableTemplates);

        $availableCurrencyArray = array();
        foreach ($availableCurrencies as $key => $value) {
            $tmp = array($key => $value['currency-code']);
            $availableCurrencyArray = $availableCurrencyArray + $tmp;
        }
        $availableCurrencyArray = $availableCurrencyArray + array('All' => 'All Currencies');

        $bulkDownloadForm->get('currencyType')->setValueOptions($availableCurrencyArray);
        $bulkDownloadForm->get('submit')->setValue('Download Template');

        $bulkUploadForm = $this->_bulkUploadForm;
        $bulkUploadForm->get('uploadType')->setValueOptions($availableTemplates);
        $bulkUploadForm->get('submit')->setValue('Upload Template');

        $request = $this->getRequest();
        $submitedFormData = $this->params()->fromPost();

        if ($request->isPost()) {

            // Files can be very large, hence we will give them plenty of space as well as increased time out limit
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', '0');

            $data = $request->getPost();

            // Download Template
            if ($data['submit'] == "Download Template") {

                // Currency required for filter
                if (isset($data['currencyType'])) {
                    $this->_bulkActionsMapper->bulk($submitedFormData);
                } else {
                    $response = array('Result'=>'Error','Errors'=>array('No Currency Selected. Select Currency.'));
                }

            // Upload Template
            } else if ($data['submit'] == "Upload Template") {

                $file = $this->params()->fromFiles('feedAttachment');

                if ($file['name'] == '') {
                    $response = array('Result'=>'Error','Errors'=>array('No file selected'));
                } else {

                    $nonFile = $request->getPost()->toArray();
                    $data = array_merge($nonFile, array('feedAttachment' => $file));

                    if ($file['error'] == UPLOAD_ERR_OK && is_uploaded_file($file['tmp_name'])) {
                        $response = $this->_bulkActionsMapper->bulkUploadFile($data);
                    } else {
                        $response = array('Result' => 'Error','Errors' => array('Failed to upload.'));
                    }
                }
            }
        }
        /*
         *  Return template upload/download selection view
         */
        return new ViewModel(array(
            'bulkDownloadForm' => $bulkDownloadForm,
            'bulkUploadForm' => $bulkUploadForm,
            'bulkResponse' => $response
        ));
    }
}