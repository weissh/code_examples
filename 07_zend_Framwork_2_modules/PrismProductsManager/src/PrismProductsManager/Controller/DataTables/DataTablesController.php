<?php

namespace PrismProductsManager\Controller\DataTables;

use PrismProductsManager\Model\ProductsMapper;
use Zend\Console\Adapter\AdapterInterface;
use Zend\Console\Console;
use Zend\Form\Form;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class DataTablesController
 * @package PrismProductsManager\Controller
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class DataTablesController extends AbstractActionController
{
    private $dbPmsConfig;
    private $productsMapper;
    private $config;

    public function __construct(
        $dbPmsConfig,
        ProductsMapper $productsMapper,
        $allConfig
    )
    {
        $this->dbPmsConfig = $dbPmsConfig;
        $this->productsMapper = $productsMapper;
        $this->config = $allConfig;
    }


    /**
     * @return JsonModel
     */
    public function serverSideProcessingAction()
    {
        /*
         * DataTables example server-side processing script.
         *
         * Please note that this script is intentionally extremely simply to show how
         * server-side processing can be implemented, and probably shouldn't be used as
         * the basis for a large complex system. It is suitable for simple use cases as
         * for learning.
         *
         * See http://datatables.net/usage/server-side for full details on the server-
         * side processing requirements of DataTables.
         *
         * @license MIT - http://datatables.net/license_mit
         */

        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
         */

        // DB table to use
        $table = 'PRODUCTS_DISPLAY_VIEW';

        // Table's primary key
        $primaryKey = 'Product ID';

        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        if (!empty($this->config['clientConfig']['search-array'])) {
            $columns = $this->config['clientConfig']['search-array'];
        } else {
            $columns = array(
                array( 'db' => 'Product Name', 'dt' => 0 ),
                array( 'db' => 'Season',  'dt' => 1 ),
                array( 'db' => 'Category',   'dt' => 2 ),
                array( 'db' => 'Sub-Category',     'dt' => 3 ),
                array( 'db' => 'Web-Department',     'dt' => 4 ),
                array( 'db' => 'Web-SubDepartment',     'dt' => 5 ),
                array( 'db' => 'Style/Option Code',     'dt' => 6 ),
                array( 'db' => 'Nr Of Options',     'dt' => 7 ),
                array( 'db' => 'WorkFlow',     'dt' => 8 ),
                array( 'db' => 'Modified Date',     'dt' => 9 ),
                array( 'db' => 'Created Date',     'dt' => 10 ),
                array( 'db' => 'Actions',     'dt' => 11 ),
                array( 'db' => 'option',     'dt' => 12 ),
                array( 'db' => 'Completed-Workflows',     'dt' => 13 ),
                array( 'db' => 'Not-Completed-Workflows',     'dt' => 14 ),
            );
        }


        $dbDetails = explode(';', $this->dbPmsConfig['dsn']);
        $dbName = str_replace('mysql:dbname=', '', $dbDetails[0]);
        $dbhost = str_replace('host=', '', $dbDetails[1]);

        // SQL server connection information
        $sql_details = array(
            'user' => $this->dbPmsConfig['username'],
            'pass' => $this->dbPmsConfig['password'],
            'db'   => $dbName,
            'host' => $dbhost
        );

        $whereAll = '';
        /** Search complex for workflows */
        if (!empty($_GET['columns'][13]['search']['value'])) {
            /** Completed workflows */
            $completedWorkflows = $_GET['columns'][13]['search']['value'];
            /** Reset column to not use in the datatables */
            $_GET['columns'][13]['search']['value'] = '';
            $whereAll = 'lastCompletedWorkflow = ' . $completedWorkflows;
        }

        if (!empty($_GET['columns'][14]['search']['value'])) {
            /** Non Completed workflows */
            $nonCompletedWorkflows = $_GET['columns'][14]['search']['value'];
            /** Reset column to not use in the datatables */
            $_GET['columns'][14]['search']['value'] = '';
            $whereAll = 'lastCompletedWorkflow < ' . $nonCompletedWorkflows;
        }


        $results = $this->processResults(SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, null, $whereAll));


        return new JsonModel($results);
    }


    public function processResults($results)
    {
        if (!empty($results['data'])) {
            foreach ($results['data'] as &$result) {
                $result[9] = '';
                $result[10] = '';
                if (!empty($result[9])) {
                    $result[9] = $this->showDate($result[9]);
                }
                if (!empty($result[10])) {
                    $result[10] = $this->showDate($result[10]);
                }

                $style = isset($result[6]) ? $result[6] : false;

                if (!empty($style)) {
                    $options = $this->productsMapper->fetchAllOptionsForProduct(array('style' => $style));
                    /** Number of options */
                    $result[7] = count($options['options']);
                    /** Workflow */
                    if(!isset($options['lessCompletedWorkflows']['percentage']) || $options['lessCompletedWorkflows']['percentage'] == null){
                        $options['lessCompletedWorkflows']['percentage'] = 0;
                    }
                    $result[8] = '<div class="progress work-flow-bar pull-left">
                                <div class="progress-bar progress-background-' . $options['lessCompletedWorkflows']['percentage'] . '" role="progressbar" aria-valuenow="' . $options['lessCompletedWorkflows']['percentage']. '"
                                     aria-valuemin="0" aria-valuemax="100" style="width:' . $options['lessCompletedWorkflows']['percentage']. '%">
                                </div>
                            </div>
                            <button
                                type="button"
                                class="btn btn-sm more-info-workflow pull-left"
                                data-container="body"
                                data-trigger="focus"
                                data-toggle="popover"
                                data-placement="top"
                                data-html="true"
                                data-content="';

                    if (!empty($options['lessCompletedWorkflows']['workflows'])) {
                        foreach ($options['lessCompletedWorkflows']['workflows'] as $workflow) {
                            $result[8] .= "<div class='col-sm-12'>";
                            $result[8] .= $workflow["workflowId"] . '. ';
                            $result[8] .= $workflow["workflowDescription"];
                            $result[8] .= $workflow["completed"] == 1 ? " - completed" : " - not completed";
                            $result[8] .= '</div>';
                        }
                    } else {
                        $result[8] .= "<div class='col-sm-12'>No options are created.</div>";
                    }
                    $result[8] .= '">
                    More
                </button>';
                }
            }
        }
        return $results;
    }

}
