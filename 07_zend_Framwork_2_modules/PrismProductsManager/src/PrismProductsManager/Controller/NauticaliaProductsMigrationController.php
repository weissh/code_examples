<?php

namespace PrismProductsManager\Controller;

use PrismProductsManager\Model\NauticaliaProductsMigrationMapper;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

/**
 * Class NauticaliaProductsMigrationController
 * @package PrismProductsManager\Controller
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class NauticaliaProductsMigrationController extends AbstractActionController
{
    /**
     * @var NauticaliaProductsMigrationMapper
     */
    protected $nauticaliaProductMigrationMapper;
    /**
     * @param NauticaliaProductsMigrationMapper $nauticaliaProductMigrationMapper
     * @throws \Exception
     */
    public function __construct(NauticaliaProductsMigrationMapper $nauticaliaProductMigrationMapper)
    {
//        throw new \Exception('Invalid action');
        $this->nauticaliaProductMigrationMapper = $nauticaliaProductMigrationMapper;
    }

    /**
     * @throws \Exception
     */
    public function indexAction()
    {
        throw new \Exception('denied');
    }
    public function copyProductDescriptionToVariantLevelAction()
    {
        $pmsProducts = $this->nauticaliaProductMigrationMapper->getAllPMSVariants(1, 25000);
        $this->nauticaliaProductMigrationMapper->transferDetailsToVariant($pmsProducts);
    }
    public function copySkuFromOldPMSIntoAttributesImportTablesAction()
    {
        $this->nauticaliaProductMigrationMapper->copyOverSkuFromOldPMS();
        die('done');
    }

    /**
     * Script Clears Data first and after inserts new data
     */
    public function migrateAttributesAction()
    {
        /** Migrate Attribute Action */
        $this->nauticaliaProductMigrationMapper->prepareForAttributesMigration();
        /**  */
    }
    public function migrateCollectionVariantNamesAction()
    {
        $this->nauticaliaProductMigrationMapper->migrateCollectionVariantNames();
    }
    public function migrateAttributeRecipientsAction()
    {
        /** Migrate Recipients from categories */
        $this->nauticaliaProductMigrationMapper->migrateRecipients();
    }
    /**
     * Nauticalia Migrate Products Action
     * @return JsonModel
     */
    public function migrateProductsAction()
    {
        /**
         * Retrieve the old products from the old cms ( accepts limit as a parameter )
         */
        $oldProducts = $this->nauticaliaProductMigrationMapper->getAllProducts(10);
        $oldProductsGrouped = $this->nauticaliaProductMigrationMapper->groupProductsByColumn($oldProducts);
        $this->nauticaliaProductMigrationMapper->emptyDatabase();
        $productsErrors = array();
        foreach ($oldProductsGrouped as $oldProduct) {
            /**
             * Loops all of the old products and build the object required for the new PMS/CMS
             */
            $oldProductObject = $this->nauticaliaProductMigrationMapper->buildObjectForDatabase($oldProduct);
            /**
             * Run integrity checks on the objects to ensure data is correct
             */
            $errorArray = $this->nauticaliaProductMigrationMapper->integrityCheckObject($oldProductObject, $oldProduct);
            if (empty($errorArray)) {
                /**
                 * Save the object into the database and make sure the data was inserted without any problems
                 */
                $saveResult = $this->nauticaliaProductMigrationMapper->saveObjectInDatabase($oldProductObject);
                if (!$saveResult) {
                    $productsErrors[] = $saveResult;
                }
            } else {
                $productsErrors[] = $errorArray;
            }
        }
        $response = array('errors' => $productsErrors);
        return new JsonModel($response);
    }
}