<?php
namespace PrismProductsManager\Controller;

use PrismElasticSearch\Enum\ElasticSearchTypes;
use PrismProductsManager\Model\ProductsMapper;
use PrismQueueManager\Service\QueueService;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class ApiProductsController extends AbstractRestfulController
{
    protected $_productsMapper;
    protected $_queueService;
    protected $_acceptedStatuses = array('ACTIVE', 'INACTIVE');

    public function __construct(ProductsMapper $productsMapper, QueueService $queueService)
    {
        $this->_productsMapper = $productsMapper;
        $this->_queueService = $queueService;
    }

    public function get($id)
    {
        $this->_methodNotAllowed();
    }

    public function getList()
    {
        $this->_methodNotAllowed();
    }

    public function create($data)
    {
        if ($data != false) {
            // validate the received array
            switch (true) {
                // validate the incoming data structure
                case is_array($data)
                    && isset($data['status'])
                    && isset($data['spectrumId'])
                    && isset($data['prices']):

                    $mainProductId = false;
                    if (!empty($data['spectrumId'])) {
                        // verify if spectrumId is binded to any product
                        $productData = $this->_productsMapper->getProductBySpectrumId($data['spectrumId']);
                        if (!empty($productData['productId']) && !empty($productData['productIdFrom'])) {
                            if (!empty($data['status'])) {
                                // check if we must do a status update
                                if (in_array($data['status'], $this->_acceptedStatuses)) {
                                    if ($productData['productIdFrom'] == 'PRODUCTS') {
                                        $productId = $productData['productId'];
                                        $mainProductId = empty($mainProductId) ? $productId : $mainProductId;
                                        // if it is a main product
                                        // check to see if we got variants for this product
                                        $productVariants = $this->_productsMapper->getVariants($productData['productId']);
                                        if ($productVariants['dataCount'] > 0) {
                                            // it has variants so we need to make sure we activate all the variants when activating the product
                                            foreach ($productVariants['data'] as $variantArray) {
                                                if ($variantArray['status'] == 'INACTIVE') {
                                                    $this->_productsMapper->updateStatus($variantArray['productAttributeId'], 1, $data['status']);
                                                }
                                            }
                                        }
                                        $this->_productsMapper->updateStatus($productId, 0, $data['status']);
                                    } else {
                                        // if it is a variant
                                        $variantId = $productData['productId'];
                                        $productId = $this->_productsMapper->getProductIdFromVariantId($productData['productId']);
                                        $mainProductId = empty($mainProductId) ? $productId : $mainProductId;
                                        if ($data['status'] == 'ACTIVE') {
                                            // if the status of the main product is inactive, set it to active
                                            $this->_productsMapper->updateStatus($productId, 0, $data['status']);
                                            // set the variant status to active
                                            $this->_productsMapper->updateStatus($variantId, 1, $data['status']);
                                        } else {
                                            $this->_productsMapper->updateStatus($variantId, 1, $data['status']);
                                            // if status of the product is active, check if all the variants have been deactivated
                                            $productVariants = $this->_productsMapper->getVariants($productId);
                                            $allInactive = true;
                                            foreach ($productVariants['data'] as $variantArray) {
                                                if ($variantArray['status'] == 'ACTIVE') {
                                                    $allInactive = false;
                                                }
                                            }
                                            if ($allInactive) {
                                                $this->_productsMapper->updateStatus($productId, 0, $data['status']);
                                            }
                                        }
                                        $this->_productsMapper->updateStatus($productId, 0, $data['status']);
                                    }
                                } else {
                                    throw new \Exception('The status should be: '.implode(' OR ', $this->_acceptedStatuses).'!', 400);
                                }
                            }
                            if (!empty($data['prices'])) {
                                $productId = $productData['productId'];
                                if ($productData['productIdFrom'] != 'PRODUCTS') {
                                    $productId = $this->_productsMapper->getProductIdFromVariantId($productId);
                                }
                                $mainProductId = empty($mainProductId) ? $productId : $mainProductId;
                                foreach ($data['prices'] as $currencyCode => $pricesArray) {
                                    $currencyId = $this->_productsMapper->getCurrencyIdByString($currencyCode);
                                    if ($currencyId) {
                                        if (isset($pricesArray['price']) && isset($pricesArray['wasPrice'])) {
                                            $price = $pricesArray['price'];
                                            $wasPrice = $pricesArray['wasPrice'];

                                            // we need to update the entries in the current price config
                                            $this->_productsMapper->updatePriceFromSpectrum($productData['productId'], $productData['productIdFrom'], $currencyId, $price, $wasPrice);
                                        } else {
                                            throw new \Exception('Invalid structure for prices section!', 400);
                                        }
                                    } else {
                                        throw new \Exception('Could not find any currency Id for currency: '.$currencyCode, 400);
                                    }
                                }
                            }

                            if (!empty($data['prices']) && !empty($data['status'])) {
                                throw new \Exception('No status or prices specified for update!', 400);
                            } else {
                                if ($mainProductId) {
                                    $this->_queueService->addIndexEntryToQueue($mainProductId.'_0', ElasticSearchTypes::PRODUCTS);
                                } else {
                                    throw new \Exception('Could not establish a productId!', 400);
                                }
                            }
                        } else {
                            throw new \Exception('No product is matching the spectrumId provided!', 400);
                        }
                    } else {
                        throw new \Exception('Spectrum Id cannot be empty!', 400);
                    }

                    break;
                default:
                    throw new \Exception('Invalid structure for the document sent!', 400);
            }
        } else {
            throw new \Exception('No data was submitted', 400);
        }
    }

    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}