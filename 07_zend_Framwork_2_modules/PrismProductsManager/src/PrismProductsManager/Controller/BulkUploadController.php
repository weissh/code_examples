<?php
namespace PrismProductsManager\Controller;

use PrismElasticSearch\Enum\ElasticSearchTypes;
use PrismQueueManager\Service\QueueService;
use Zend\Mvc\Controller\AbstractActionController;
use PrismProductsManager\Form\BulkDownloadForm;
use PrismProductsManager\Form\BulkUploadForm;
use PrismProductsManager\Form\SearchForm;
use Zend\View\Model\ViewModel;
use PrismProductsManager\Model\BulkActionsMapper;
use Zend\Filter\StringToLower;
use \Zend\I18n\Filter\Alnum;
use Zend\Filter\Word\CamelCaseToDash;

// Files can be very large, hence we will give them plenty of space as well as increased time out limit
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');

/**
 * Class BulkUploadController
 *
 * @package PrismProductsManager\Controller
 */
class BulkUploadController extends AbstractActionController
{

    /**
     *
     * @var BulkUploadForm
     */
    protected $uploadTemplateForm;

    /**
     *
     * @var array
     */
    protected $availableTemplates = array(
        'All_Common_Attributes' => 'All Common Attributes',
        'Common_Attributes_Cascade' => 'Common Attributes Cascade',
        'Price' => 'Prices',
        'Price_cascade'  => 'Prices Cascade',
    );

    /**
     *
     * @var BulkActionsMapper
     */
    protected $bulkActionsMapper;

    /**
     *
     * @param BulkUploadForm $uploadTemplateForm
     * @param BulkActionsMapper $bulkActionsMapper
     */
    public function __construct(BulkUploadForm $uploadTemplateForm, BulkActionsMapper $bulkActionsMapper)

    {
        $this->uploadTemplateForm = $uploadTemplateForm;
        $this->bulkActionsMapper = $bulkActionsMapper;
    }

    /**
     *
     * @return ViewModel
     * @throws \Exception
     */
    public function indexAction()
    {
        $response = array();

        // $this->bulkActionsMapper-
        $this->uploadTemplateForm->get('uploadType')->setValueOptions($this->availableTemplates);
        $this->uploadTemplateForm->get('submit')->setValue('Upload Template');
        $request = $this->getRequest();
        $submitedFormData = $this->params()->fromPost();

        if ($request->isPost()) {

            if ($submitedFormData['submit'] == 'Download Template') {
                $this->bulkActionsMapper->downloadTemplate($submitedFormData['downloadType']);
            }

            if ($submitedFormData['submit'] == 'Upload Template') {

                $file = $this->params()->fromFiles('feedAttachment');
                $submitedFormData['feedAttachment'] = $file;
                $this->uploadTemplateForm->setData($submitedFormData);

                if ($this->uploadTemplateForm->isValid()) {
                    // get logged user details
                    $userId =  $this->authplugin()->getClientId($this->authplugin()->getSessionRead('userId'));
                   $response = $this->bulkActionsMapper->uplaodTemplate($userId ,$submitedFormData);
                }
            }
        }

        return new ViewModel(array(
            'uploadTemplateForm' => $this->uploadTemplateForm,
            'bulkResponse' => $response
        ));
    }

    public function processBulkAction()
    {
        $this->bulkActionsMapper->processBulkRecords();
        return new ViewModel();
    }

    public function notifySipSpectrumAction()
    {
        $this->bulkActionsMapper->notifySipAndSpectrum();
        return new ViewModel();
    }

    protected function errorResponse($errors = null)
    {
        $errorsArray = array();

        if(!empty($errors)){
            foreach ($errors as $key => $error) {
                 $errorsArray[] = $error[key($error)];
            }
        }

        return array(
            'Result' => 'Error',
            'Errors' => $errorsArray
        );
    }


    /**
     * Convert file name
     * lowercase, space to underscore, remove not alphanumeric char
     * add date() into file name
     *
     * @param unknown $file
     */
    protected function renameUploadedFile($file)
    {
        $temp = explode('.', $_FILES['feedAttachment']['name']);
        $ext = array_pop($temp);
        $fileName = implode('.', $temp);
        $date = date('YmdHis');

        $filter = new StringToLower('UTF-8');
        $fileName = $filter->filter($fileName);

        $filter = new Alnum(true);
        $fileName = $filter->filter($fileName);

        $fileName = str_replace(' ', '_', $fileName) . "_" . $date . "." . $ext;

        // $filter = new CamelCaseToDash();
        // $fileName =$filter->filter($fileName) . "_". $date . "." . $ext;

        $_FILES['feedAttachment']['name'] = $fileName;
        $file['name'] = $fileName;

        return $file;
    }

    /**
     *
     * @return ViewModel
     * @throws \Exception
     */
    public function __indexAction()
    {
        $response = array();

        $this->uploadTemplateForm->get('uploadType')->setValueOptions($this->availableTemplates);
        $this->uploadTemplateForm->get('submit')->setValue('Upload Template');

        $request = $this->getRequest();
        $submitedFormData = $this->params()->fromPost();

        if ($request->isPost()) {
            // Files can be very large, hence we will give them plenty of space as well as increased time out limit
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', '0');

            switch ($submitedFormData['submit']) {
                case 'Download Template':
                    $this->bulkActionsMapper->downloadTemplate($submitedFormData['downloadType']);
                    break;
                default:
                    $file = $this->params()->fromFiles('feedAttachment');

                    $allowedMimes = array(
                        'text/plain',
                        'text/csv',
                        'application/vnd.ms-excel',
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'application/octet-stream'
                    );
                    $fileMime = mime_content_type($file['tmp_name']);
                    if (! in_array($fileMime, $allowedMimes)) {
                        throw new \Exception('Invalid file mime type');
                    }

                    $submitedFormData['feedAttachment'] = $file;
                    $this->uploadTemplateForm->setData($submitedFormData);
                    /**
                     * Validate the form file
                     */
                    if ($this->uploadTemplateForm->isValid()) {
                        $submitedFormData = $this->uploadTemplateForm->getData();
                        if ($file['name'] == '') {
                            $response = array(
                                'Result' => 'Error',
                                'Errors' => array(
                                    'No file selected'
                                )
                            );
                        } else {
                            // re-name the file
                            $file = $this->renameUploadedFile($file);
                            $data = array_merge($submitedFormData, array(
                                'feedAttachment' => $file
                            ));

                            if ($file['error'] == UPLOAD_ERR_OK && is_uploaded_file($file['tmp_name'])) {
                                /**
                                 * Process Uploaded File
                                 */
                                $response = $this->bulkActionsMapper->bulkUpload($data);
                            } else {
                                $response = array(
                                    'Result' => 'Error',
                                    'Errors' => array(
                                        'Failed to upload.'
                                    )
                                );
                            }
                        }
                    } else {
                        $errors = $this->uploadTemplateForm->getMessages();
                        $errorsArray = array();
                        foreach ($errors as $key => $error) {
                            $errorsArray[] = $error[key($error)];
                        }
                        $response = array(
                            'Result' => 'Error',
                            'Errors' => $errorsArray
                        );
                    }
                    break;
            }
        }

        return new ViewModel(array(
            'uploadTemplateForm' => $this->uploadTemplateForm,
            'bulkResponse' => $response
        ));
    }
}