<?php
namespace PrismProductsManager\Controller;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use PrismProductsManager\Model\AttributesGroupMapper;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 * Api Size Guide Controller
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiSizeGuideController extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * Attributes Group Mapper
     * PrismProductsManager\Model\AttributesGroupMapper
     * @var object
     */
    protected $_attributesGroupMapper;

    public function __construct(AttributesGroupMapper $attributesGroupMapper)
    {
        $this->_attributesGroupMapper = $attributesGroupMapper;
    }
    public function get($id)
    {
        $this->_methodNotAllowed();
    }

    public function getList()
    {
        $category = $this->params()->fromRoute('category');
        $table = $this->params()->fromRoute('table');

        $sizeGuide =  $this->_attributesGroupMapper->getSizeGuideBySizeCategoryAndTable($category, $table);
        if (!empty($sizeGuide)) {
            return new JsonModel($sizeGuide);
        } else {
            throw new \Exception('Size guide could not be fetched', 400);
        }
    }

    public function create($data)
    {

    }

    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}