<?php

namespace PrismProductsManager\Controller;

use PrismProductsManager\Controller\DataTables\SSP;
use PrismProductsManager\Entity\CommonAttributes;
use PrismProductsManager\Entity\CommonAttributesGuiRepresentation;
use PrismProductsManager\Entity\CommonAttributesValues;
use PrismProductsManager\Entity\CommonAttributesViewType;
use PrismProductsManager\Entity\HistoricalValue;
use PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesDefinitions;
use PrismProductsManager\Service\HistoricalService;
use Zend\Http\Request;
use Zend\I18n\Validator\DateTime;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

class HistoryController extends AbstractActionController
{
    /** @var HistoricalService $historicalService */
    private $historicalService;

    /** @var array $dbPmsConfig */
    private $dbPmsConfig;

    private $viewRenderer;

    /**
     * HistoryController constructor.
     * @param HistoricalService $historicalService
     * @param array $dbPmsConfig
     * @param PhpRenderer $viewRenderer
     */
    public function __construct(
        HistoricalService $historicalService,
        $dbPmsConfig = [],
        PhpRenderer $viewRenderer
        )
    {
        $this->historicalService = $historicalService;
        $this->dbPmsConfig = $dbPmsConfig;
        $this->viewRenderer = $viewRenderer;
    }

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $this->isCmsUserAuthorized(['view']);

        return new ViewModel();
    }

    /**
     * @return JsonModel
     */
    public function deleteEntryAction()
    {
        $this->isCmsUserAuthorized(['edit']);
        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $postData = $this->params()->fromPost();
            if (!empty($postData['entry'])) {
                $result = $this->historicalService->deleteAllRecordsRelatedWith($postData['entry']);
                return new JsonModel($result);
            }

            return new JsonModel(['success' => false, 'error' => 'Could not find the entry parameter.']);
        }
        return new JsonModel(['success' => false, 'error' => 'Request type not accepted.']);
    }

    /**
     * @return JsonModel
     */
    public function editEntryLoadAction()
    {
        $this->isCmsUserAuthorized(['edit']);
        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $postData = $this->params()->fromPost();
            if (!empty($postData['entry'])) {
                /** @var HistoricalValue $historicalEntry */
                $historicalEntry = $this->historicalService->getHistoricalEntity($postData['entry']);
                $commonAttribute = $historicalEntry->getCommonAttribute();
                $historicalValue = $historicalEntry->getValue();
                if ($commonAttribute instanceof CommonAttributes) {
                    $viewType = false;
                    /** @var CommonAttributesViewType $commonAttributeGuiRepresentation */
                    $commonAttributeGuiRepresentation = $commonAttribute->getCommonAttributeGuiRepresentation();
                    if ($commonAttributeGuiRepresentation instanceof CommonAttributesGuiRepresentation) {

                        $commonAttributeViewType = $commonAttributeGuiRepresentation->getCommonattributesviewtypeid();
                        if ($commonAttributeViewType instanceof CommonAttributesViewType) {
                            $viewType = $commonAttributeViewType->getCommonattributeviewtype();

                        }
                    }
                    if ($viewType == CommonAttributesViewType::VIEW_TYPE_SELECT ||
                        $viewType == CommonAttributesViewType::VIEW_TYPE_MULTISELECT
                    ) {
                        $commonAttributeValues = $commonAttribute->getCommonAttributeValues();
                        /** @var CommonAttributesValues $commonAttributeValue */
                        foreach ($commonAttributeValues as $commonAttributeValue) {
                            if ($commonAttributeValue->getcommonAttributeValueId() == $historicalValue) {
                                $definitionEntity = $commonAttributeValue->getCommonAttributeValuesDefinitions();
                                if ($definitionEntity instanceof \PrismProductsManager\Entity\CommonAttributesValuesDefinitions) {
                                    $historicalValue = $definitionEntity->getValue();
                                    break;
                                }
                            }
                        }
                    }
                }

                $editModal = new ViewModel();
                $editModal->setTerminal(true);
                $editModal->setTemplate('prism-products-manager/history/partials/entry-edit');
                $editModal->setVariable('historicalEntry', $historicalEntry);
                $editModal->setVariable('historicalValue', $historicalValue);
                $editModalOutput = $this->viewRenderer->render($editModal);
                $editModalOutput = utf8_encode($editModalOutput);
                $returnArray = array('success' => true, 'html' => $editModalOutput);
                return new JsonModel($returnArray);
            }

            return new JsonModel(['success' => false, 'error' => 'Could not find the entry parameter.']);
        }
        return new JsonModel(['success' => false, 'error' => 'Request type not accepted.']);
    }

    /**
     * @return JsonModel
     */
    public function editEntryRequestAction()
    {
        $this->isCmsUserAuthorized(['edit']);
        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $postData = $this->params()->fromPost();

            if (isset($postData['entry']) &&
                isset($postData['attributeValue']) &&
                isset($postData['fromDate'])
            ) {
                $entryId = $postData['entry'];
                $attributeValue = $postData['attributeValue'];
                $fromDate = $postData['fromDate'];

                $result = $this->historicalService->updateHistoricalEntry($entryId, ['value' => $attributeValue, 'fromDate' => $fromDate]);
                if (!empty($result['success'])) {
                    return new JsonModel(['success' => true]);
                } else {
                    $errorMessage = 'Unknown error. Please contact support.';
                    if (isset($result['error'])) {
                        $errorMessage = $result['error'];
                    }
                    return new JsonModel(['success' => false, 'error' => $errorMessage]);
                }

            }
            return new JsonModel(['success' => false, 'error' => 'Could not locate required parameters.']);
        }
        return new JsonModel(['success' => false, 'error' => 'Request type not accepted.']);
    }

    public function downloadCsvAction()
    {
        $this->isCmsUserAuthorized(['view']);
        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $postData = $this->params()->fromPost();
            $whereAll = [];

            if (!empty($postData['fromDateFrom'])) {
                $whereAll[] = "fromDate >= '". $postData['fromDateFrom'] . "'";
            }
            if (!empty($postData['fromDateTo'])) {
                $whereAll[] = "fromDate <= '". $postData['fromDateTo'] ."'";
            }
            if (!empty($postData['createdDateFrom'])) {
                $whereAll[] = "created >= '". $postData['createdDateFrom'] ."'";
            }
            if (!empty($postData['createdDateTo'])) {
                $whereAll[] = "created <= '". $postData['createdDateTo'] ."'";
            }
            if (!empty($postData['modifiedDateFrom'])) {
                $whereAll[] = "modified >= '". $postData['modifiedDateFrom'] ."'";
            }
            if (!empty($postData['modifiedDateTo'])) {
                $whereAll[] = "modified <= '". $postData['modifiedDateTo'] ."'";
            }
            if (!empty($postData['freeSearch'])) {
                $whereAll[] = "optionOrStyleCode LIKE '%". $postData['freeSearch'] ."%'";
                $whereAll[] = "userName LIKE '%". $postData['freeSearch'] ."%'";
                $whereAll[] = "attribute LIKE '%". $postData['freeSearch'] ."%'";
                $whereAll[] = "attributeValue LIKE '%". $postData['freeSearch'] ."%'";
                $whereAll[] = "modifiedBy LIKE '%". $postData['freeSearch'] ."%'";
            }
            $records = $this->historicalService->retrieveRecordsFromView($whereAll);
            $filename = 'history_download_csv_'.time().'.csv';
            $f = fopen('/tmp/' . $filename, 'w');
            // loop over the input array
            fputcsv($f, [
                'id (DO NOT UPDATE COLUMN)', 'Style/Option Code (DO NOT UPDATE COLUMN)', 'Field Updated (DO NOT UPDATE COLUMN)', 'Field Value',
                'From Date', 'Modified Date', 'Modified By (DO NOT UPDATE COLUMN)', 'Created Date', 'Created By (DO NOT UPDATE COLUMN)', 'Delete - Add 1 to delete entry'
            ], ",");

            foreach ($records as $line) {
                // generate csv lines from the inner arrays
                fputcsv($f, $line, ",");
            }
            // reset the file pointer to the start of the file
            fseek($f, 0);
            // tell the browser it's going to be a csv file
            header('Content-Type: application/csv');
            // tell the browser we want to save it instead of displaying it
            header('Content-Disposition: attachment; filename="'.$filename.'";');
            // make php send the generated csv lines to the browser
            readfile("/tmp/$filename");
            fpassthru($f);
            die;
        }
    }


    public function uploadCsvAction()
    {
        set_time_limit(0);
        $this->isCmsUserAuthorized(['edit']);
        /** @var Request $request */
        $request = $this->getRequest();

        if ($request->isPost()) {
            $tmpName = $_FILES['history-upload']['tmp_name'];
            $dataArray = array_map('str_getcsv', file($tmpName));
            if (isset($dataArray[0][0]) &&  strpos($dataArray[0][0], 'id') !== false) {
                unset($dataArray[0]);
            }

            $filename = 'history_upload_csv_errors_'.time().'.csv';
            $f = fopen('/tmp/' . $filename, 'w');
            // loop over the input array
            fputcsv($f, ['id', 'row index', 'errors', ], ",");
            $errors = false;
            if (count($dataArray) > 300) {
                $errors = true;
                fputcsv($f, [0, 0, "Please upload maximum of 300 entries."], ",");
            } else {
                foreach ($dataArray as $index => $dataRow) {
                    $result = $this->historicalService->updateEntryFromUpload($dataRow);

                    if (empty($result['success'])) {
                        $errors = true;
                        $index = $index + 1;
                        fputcsv($f, [$dataRow[0], $index, implode("\n", $result['errors'] )], ",");
                    }
                }
            }

            if ($errors === false) {
                fputcsv($f, ['No errors triggered.'], ",");
            }
            // reset the file pointer to the start of the file
            fseek($f, 0);
            // tell the browser it's going to be a csv file
            header('Content-Type: application/csv');
            // tell the browser we want to save it instead of displaying it
            header('Content-Disposition: attachment; filename="'.$filename.'";');
            // make php send the generated csv lines to the browser
            readfile("/tmp/$filename");
            fpassthru($f);
            die;
        }
    }
    /**
     * @return JsonModel
     */
    public function serverSideProcessingAction()
    {
        $this->isCmsUserAuthorized(['view']);

        // DB table to use
        $table = 'HISTORY_SUMMARY';

        // Table's primary key
        $primaryKey = 'id';

        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = [
            [ 'db' => 'id',  'dt' => 0 ],
            [ 'db' => 'optionOrStyleCode',  'dt' => 1 ],
            [ 'db' => 'attribute',          'dt' => 2 ],
            [ 'db' => 'attributeValue',     'dt' => 3 ],
            [ 'db' => 'fromDate',           'dt' => 4 ],
            [ 'db' => 'updateType',         'dt' => 5 ],
            [ 'db' => 'modified',           'dt' => 6 ],
            [ 'db' => 'modifiedBy',         'dt' => 7 ],
            [ 'db' => 'created',            'dt' => 8 ],
            [ 'db' => 'userName',           'dt' => 9 ],
        ];

        $dbDetails = explode(';', $this->dbPmsConfig['dsn']);
        $dbName = str_replace('mysql:dbname=', '', $dbDetails[0]);
        $dbhost = str_replace('host=', '', $dbDetails[1]);

        // SQL server connection information
        $sql_details = array(
            'user' => $this->dbPmsConfig['username'],
            'pass' => $this->dbPmsConfig['password'],
            'db'   => $dbName,
            'host' => $dbhost
        );

        $whereAll = $this->filterDates();

        $results = SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, null, $whereAll);

        $results['data'] = $this->processResults($results['data']);
        return new JsonModel($results);
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function filterDates()
    {
        $whereAll = '';

        $autoDateFrom = isset($_GET['autoDateFrom']) ? $_GET['autoDateFrom'] : false;
        $autoDateTo = isset($_GET['autoDateTo']) ? $_GET['autoDateTo'] : false;
        $createdDateFrom = isset($_GET['createdDateFrom']) ? $_GET['createdDateFrom'] : false;
        $createdDateTo = isset($_GET['createdDateTo']) ? $_GET['createdDateTo'] : false;
        $modifiedDateFrom = isset($_GET['modifiedDateFrom']) ? $_GET['modifiedDateFrom'] : false;
        $modifiedDateTo = isset($_GET['modifiedDateTo']) ? $_GET['modifiedDateTo'] : false;


        if (!empty($autoDateFrom) && !$this->historicalService->validateDate($autoDateFrom)) {
            throw new \Exception("Auto Date From is not a valid date");
        }
        if (!empty($autoDateTo) && !$this->historicalService->validateDate($autoDateTo)) {
            throw new \Exception("Auto Date to is not a valid date");
        }
        if (!empty($createdDateFrom) && !$this->historicalService->validateDate($createdDateFrom)) {
            throw new \Exception("Created Date From is not a valid date");
        }
        if (!empty($createdDateTo) && !$this->historicalService->validateDate($createdDateTo)) {
            throw new \Exception("Created Date To is not a valid date");
        }
        if (!empty($modifiedDateFrom) && !$this->historicalService->validateDate($modifiedDateFrom)) {
            throw new \Exception("Modified Date From is not a valid date");
        }
        if (!empty($modifiedDateTo) && !$this->historicalService->validateDate($modifiedDateTo)) {
            throw new \Exception("Modified Date To is not a valid date");
        }

        if (!empty($autoDateFrom)) {
            $autoDateFrom = strtotime($autoDateFrom);
            $autoDateFrom = date('Y-m-d H:i:s', $autoDateFrom);
            $whereAll[] = "fromDate >= '$autoDateFrom'";
        }
        if (!empty($autoDateTo)) {
            $autoDateTo = strtotime($autoDateTo);
            $autoDateTo = date('Y-m-d H:i:s', $autoDateTo);
            $whereAll[] = "fromDate <= '$autoDateTo'";
        }
        if (!empty($createdDateFrom)) {
            $createdDateFrom = strtotime($createdDateFrom);
            $createdDateFrom = date('Y-m-d H:i:s', $createdDateFrom);
            $whereAll[] = "created >= '$createdDateFrom'";
        }
        if (!empty($createdDateTo)) {
            $createdDateTo = strtotime($createdDateTo);
            $createdDateTo = date('Y-m-d H:i:s', $createdDateTo);
            $whereAll[] = "created <= '$createdDateTo'";
        }
        if (!empty($modifiedDateFrom)) {
            $modifiedDateFrom = strtotime($modifiedDateFrom);
            $modifiedDateFrom = date('Y-m-d H:i:s', $modifiedDateFrom);
            $whereAll[] = "modified >= '$modifiedDateFrom'";
        }
        if (!empty($modifiedDateTo)) {
            $modifiedDateTo = strtotime($modifiedDateTo);
            $modifiedDateTo = date('Y-m-d H:i:s', $modifiedDateTo);
            $whereAll[] = "modified <= '$modifiedDateTo'";
        }

        return $whereAll;
    }
    /**
     * @param $results
     * @return mixed
     */
    private function processResults($results)
    {

        foreach ($results as &$result)  {
            $rowId = $result[0];


            if (isset($result[5])) {
                switch ($result[5]) {
                    case HistoricalValue::UPDATE_TYPE_Interface:
                        $result[5] = 'Interface';
                        break;
                    case HistoricalValue::UPDATE_TYPE_Bulk:
                        $result[5] = 'Bulk';
                        break;
                    case HistoricalValue::UPDATE_TYPE_Scheduled:
                        $result[5] = 'Scheduled';
                        break;
                    case HistoricalValue::UPDATE_TYPE_Auto_Date:
                        $result[5] = 'From Date';
                        break;
                }
            }
            $count = count($result);
            $result[$count] = "
                <button class=\"btn btn-primary edit-historical-record\" type=\"button\" data-entry-id=\"{$rowId}\">Edit</button>
                <button class=\"btn btn-danger delete-historical-record\" type=\"button\" data-entry-id=\"{$rowId}\">Delete</button>
            ";
        }
        return $results;
    }

}