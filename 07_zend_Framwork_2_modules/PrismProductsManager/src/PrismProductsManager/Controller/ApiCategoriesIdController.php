<?php
namespace PrismProductsManager\Controller;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use PrismProductsManager\Model\ApiCategoriesMapper;


/**
 * Api Categories Controller
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiCategoriesIdController extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * Api Categories Mapper
     * PrismProductsManager\Model\ApiCategoriesMapper
     * @var object
     */
    protected $_apiCategoriesMapper;

    public function __construct(ApiCategoriesMapper $ApiCategoriesMapper)
    {
        $this->_apiCategoriesMapper = $ApiCategoriesMapper;
    }
    public function get($id)
    {

        $productData = array();
        $validator = new \Zend\Validator\NotEmpty(\Zend\Validator\NotEmpty::INTEGER);
        if ($validator->isValid($id)) {
            $productData =  $this->_apiCategoriesMapper->getProductsByCategoryId($id);
        }

        if (!empty($productData)) {
            return new JsonModel($productData);
        } else {
            throw new \Exception('Product not found', 400);
        }
    }

    public function getList()
    {
        $this->_methodNotAllowed();
    }

    public function create($data)
    {
        $this->_methodNotAllowed();
    }

    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}