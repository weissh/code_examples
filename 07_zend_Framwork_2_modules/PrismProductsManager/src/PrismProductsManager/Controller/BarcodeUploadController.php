<?php

namespace PrismProductsManager\Controller;

use PrismProductsManager\Form\UploadBarcodesForm;
use PrismProductsManager\Model\ProductsMapper;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class BarcodeUploadController extends AbstractActionController
{

    protected $_uploadBarcodesForm;
    protected $_productsMapper;

    public function __construct(UploadBarcodesForm $uploadBarcodesForm, ProductsMapper $productsMapper)
    {
        $this->_uploadBarcodesForm = $uploadBarcodesForm;
        $this->_productsMapper = $productsMapper;
    }

    public function indexAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $request = $this->getRequest();

        if ($request->isPost()) {
            ini_set('max_execution_time', '300');
            $this->isCmsUserAuthorized(array('add', 'edit'));
            $file = $this->params()->fromFiles('barcodesFile');
            $nonFile = $request->getPost()->toArray();
            $data = array_merge($nonFile, array('barcodesFile' => $file));
            $this->_uploadBarcodesForm->setData($data);

            if ($this->_uploadBarcodesForm->isValid()) {

                if ($file['error'] == UPLOAD_ERR_OK && is_uploaded_file($file['tmp_name'])) {

                    // at this point if we have any errors a excel file will be downloaded
                    // else we will refresh the page and show the success message
                    $this->_productsMapper->bulkUploadBarcodes($data);
                    $this->_errorValue(array('controller'=>'BarcodeUpload','action' => 'index'), 'success', 'The file was processed successfully.');
                }
            }
        }
        return array(
            'uploadForm' => $this->_uploadBarcodesForm
        );
    }

    public function downloadTemplateAction()
    {
        $this->_productsMapper->downloadExcelBarcodesTemplate();
    }

    /**
     * Redirect in case of error
     * @param $route
     * @param $type
     * @param $message
     *
     * @param string $alert
     * @return null
     */
    protected function _errorValue($route, $type, $message, $alert = [])
    {
        $alert['type'] = $type;
        $alert['bold'] = ucfirst($type).':';
        $alert['messages'] = array();
        $alert['messages'][0] = $message;

        $this->flashmessenger()->setNamespace('alert')
            ->addMessage($alert);

        return $this->redirect()->toRoute(null ,$route);
    }
}

