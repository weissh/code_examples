<?php
namespace PrismProductsManager\Controller;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use PrismProductsManager\Model\ApiCategoriesMapper;


/**
 * Api Categories by Id Controller
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiCategoriesByNameController extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * Api Categories Mapper
     * PrismProductsManager\Model\ApiCategoriesMapper
     * @var object
     */
    protected $_apiCategoriesMapper;

    public function __construct(ApiCategoriesMapper $apiCategoriesMapper, $pagesMapper)
    {
        $this->_apiCategoriesMapper = $apiCategoriesMapper;
        $this->_pagesMapper = $pagesMapper;

    }
    public function get($id)
    {
        $data = preg_replace('/[^a-zA-Z0-9-_]|[,;]$/s', '', $id);
        $websiteId = $this->params()->fromRoute('website');
        $languageId = $this->params()->fromRoute('language');

        if (empty($data)) {
            $this->_methodNotAllowed();
        }

        $categoryReturn = array();
        $validator = new \Zend\Validator\NotEmpty(\Zend\Validator\NotEmpty::INTEGER);

        if ($validator->isValid($websiteId) && $validator->isValid($languageId)) {

            $categoryContent = $this->_apiCategoriesMapper->getCategoryByName($data, $websiteId, $languageId);

            if (isset($categoryContent)) {
                $pageContent = $this->_pagesMapper->getCategoryPageByCategoryId($categoryContent['categoryId'], $languageId, $websiteId);
                $categoryReturn = array('categoryContent' => $categoryContent, 'pageContent' => $pageContent);
            } else {
                $categoryReturn = array('categoryContent' => false, 'pageContent' => false);
            }

        }
        return new JsonModel($categoryReturn);
    }

    public function getList()
    {
        $this->_methodNotAllowed();
    }

    public function create($data)
    {
        $this->_methodNotAllowed();
    }

    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}