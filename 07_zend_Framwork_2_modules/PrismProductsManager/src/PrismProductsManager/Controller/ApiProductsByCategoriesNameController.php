<?php
namespace PrismProductsManager\Controller;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use PrismProductsManager\Model\ApiProductsMapper;


/**
 * Api Products By Category Name Controller
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiProductsByCategoriesNameController extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * Api Categories Mapper
     * PrismProductsManager\Model\ApiCategoriesMapper
     * @var object
     */
    protected $_apiProductsMapper;
    protected $_categoriesMapper;

    public function __construct(ApiProductsMapper $apiProductsMapper, $categoriesMapper, $pagesMapper)
    {
        $this->_apiProductsMapper = $apiProductsMapper;
        $this->_categoriesMapper = $categoriesMapper;
        $this->_pagesMapper = $pagesMapper;
    }

    public function get($id)
    {

        $data = preg_replace('/[^a-zA-Z0-9-_]|[,;]$/s', '', $id);
        $websiteId = $this->params()->fromRoute('website');
        $languageId = $this->params()->fromRoute('language');
        $limit = $this->params()->fromRoute('limit');
        $page = $this->_params()->fromRoute('page');

        if (empty($data)) {
            $this->_methodNotAllowed();
        }
        $validator = new \Zend\Validator\NotEmpty(\Zend\Validator\NotEmpty::INTEGER);
        if ($validator->isValid($websiteId) && $validator->isValid($languageId)) {
            $productData =  $this->_apiProductsMapper->getProductsByCategoryName($data, $websiteId, $languageId, $limit, $page);
            $categoryContent = $this->_categoriesMapper->getCategoryPageByName($data, $websiteId, $languageId);
            $pageContent = $this->_pagesMapper->getCategoryPageByCategoryId($categoryContent, $languageId, $websiteId);

            if ($pageContent && $pageContent['contentShopHtml'] != '') {
                $return = array('content' => $pageContent);
            } else {
                $return = array('products' => $productData);
            }
        }

        if (!empty($return)) {
            return new JsonModel($return);
        } else {
            throw new \Exception('Product not found', 400);
        }
    }

    public function getList()
    {
        $this->_methodNotAllowed();
    }

    public function create($data)
    {
        $this->_methodNotAllowed();
    }

    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}