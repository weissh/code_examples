<?php

namespace PrismProductsManager\Controller;

use PrismProductsManager\Model\ProductsMigrationMapper;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

/**
 * Class ProductsMigrationController
 * @package PrismProductsManager\Controller
 */
class ProductsMigrationController extends AbstractActionController
{
    /**
     * @var ProductsMigrationMapper
     */
    protected $_productMigrationMapper;
    /**
     * @param ProductsMigrationMapper $productMigrationMapper
     * @throws \Exception
     */
    public function __construct(ProductsMigrationMapper $productMigrationMapper)
    {
//        throw new \Exception('Invalid action');
        $this->_productMigrationMapper = $productMigrationMapper;
    }
    public function lechameauB2CtmpTableAction()
    {
        $response = $this->_productMigrationMapper->lechameaub2cTmpTable();
        $response = array('success' => $response);
        return new JsonModel($response);
    }
    public function importCostPriceAction()
    {
        $response = $this->_productMigrationMapper->importCostPrice();
        $response = array('success' => $response);
        return new JsonModel($response);
    }
    public function changeLongDescriptionAction()
    {
        $response = $this->_productMigrationMapper->changeLongDescription();
        $response = array('success' => $response);
        return new JsonModel($response);
    }
    public function importAdditionalInfoFromPLOAction()
    {
        $response = $this->_productMigrationMapper->importAdditionalInfoFromPLO();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    public function importRelatedProductsForLCMAction()
    {
        $response = $this->_productMigrationMapper->importRelatedProductsForLCM();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    public function importProductsForLCMAction()
    {
        $response = $this->_productMigrationMapper->importProductsForLCM();
        $response = array('success' => $response);
        return new JsonModel($response);
    }
    public function deleteCategoriesTempAction()
    {
        $response = $this->_productMigrationMapper->deleteCategoriesTemp();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    public function insertCategoriesTempAction()
    {
        $response = $this->_productMigrationMapper->insertCategoriesTemp();
        $response = array('success' => $response);
        return new JsonModel($response);
    }
    public function migrateAttributesFromFeedAction()
    {
        $response = $this->_productMigrationMapper->migrateAttributesFromFeed();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    public function migrateProductsFromSpectrumAction()
    {
        $response = $this->_productMigrationMapper->migrateProductsFromSpectrum();
        $response = array('success' => $response);
        return new JsonModel($response);
    }
    public function migrateSpectrumSizesAction() {
        $response = $this->_productMigrationMapper->migrateSpectrumSizes();
        $response = array('success' => $response);
        return new JsonModel($response);
    }
    public function migrateSpectrumColoursAction()
    {
        $response = $this->_productMigrationMapper->migrateSpectrumColour();
        $response = array('success' => $response);
        return new JsonModel($response);
    }
    /**
     * @return JsonModel
     */
    public function migrateColoursAction()
    {
        $response = $this->_productMigrationMapper->migrateColours();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function migrateMerchantCategoriesAction()
    {
        $response = $this->_productMigrationMapper->migrateMerchantCategories();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function migrateMarketingCategoriesAction()
    {
        $response = $this->_productMigrationMapper->migrateMarketingCategories();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function migrateProductsAction()
    {
        $response = $this->_productMigrationMapper->migrateProducts();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function migrateUsersOSAction()
    {
        $response = $this->_productMigrationMapper->migrateUsersForOS();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    public function migrateUsersNauticaliaAction()
    {
        $response = $this->_productMigrationMapper->migrateUsersForNauticalia();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function migrateAttributesAction()
    {
        $response = $this->_productMigrationMapper->migrateCoreAttributes();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function changeUrlForCategoriesAction()
    {
        $response = $this->_productMigrationMapper->changeUrlForCategories();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function updateVariantCategoriesFromParentCategoriesAction()
    {
        $response = $this->_productMigrationMapper->copyCategoriesFromParents();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function updateAttributesForProductsAction()
    {
        $response = $this->_productMigrationMapper->updateAttributesForProducts();
        $response = array('success' => $response);
        return new JsonModel($response);
    }
    /**
     * Needs to be run after products, attributes and categories import
     * @return JsonModel
     */
    public function changeSizesForBeltsAction()
    {
        $response = array();
        $response[] = $this->_productMigrationMapper->updateMerchantCategorySku();
        $response[] = $this->_productMigrationMapper->updateMerchantAttributesForProducts();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function fixCategoriesForSingleVariantsProductsAction()
    {
        $response = $this->_productMigrationMapper->fixCategoriesForSingleVariantsProducts();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function fixDefaultCategoryAction()
    {
        $response = $this->_productMigrationMapper->fixDefaultCategory();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function fixImagesAction()
    {
        $response = $this->_productMigrationMapper->fixImages();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function fixProductsUrlsAction()
    {
        $response = $this->_productMigrationMapper->fixProductsUrls();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     *
     */
    public function getProductsWithoutImagesAction()
    {
        $this->_productMigrationMapper->getProductsWithoutImages();
        exit;
    }

    /**
     *
     */
    public function importCareInformationAction()
    {
        $this->_productMigrationMapper->importCareInformation();
        exit;
    }

    /**
     *
     */
    public function updateProductStatusAction()
    {
        $this->_productMigrationMapper->productStatuses();
        exit;
    }

    /**
     *
     */
    public function migrateNewMarketingAttributesAction()
    {
        $this->_productMigrationMapper->migrateNewMarketingAttributes();
        exit;
    }

    /**
     *
     */
    public function bindNewMarketingAttributesAction()
    {
        $this->_productMigrationMapper->bindNewMarketingAttributes();
        exit;
    }

    /**
     * @return JsonModel
     */
    public function moveAttributesFromProductToVariantsAction()
    {
        $response = $this->_productMigrationMapper->moveAttributesFromProductToVariants();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function populateProductsTmpAction()
    {
        $response = $this->_productMigrationMapper->populateProductsTmp();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function updateSpectrumWithUnprocessedUsersAction()
    {
        $response = $this->_productMigrationMapper->updateSpectrumWithUnprocessedUsers();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function populateUsersReportsAction()
    {
        $response = $this->_productMigrationMapper->populateUsersTmp();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function importUsersFromSpectrumAction()
    {
        $response = $this->_productMigrationMapper->importUsersFromSpectrum();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     *
     */
    public function migrateSuppliersAction()
    {
        $this->_productMigrationMapper->syncSuppliers();
        die;
    }

    /**
     * @return JsonModel
     */
    public function pmsProductReportAction()
    {
        $response = $this->_productMigrationMapper->productReport();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     * @return JsonModel
     */
    public function changeScreeningQuantityAction()
    {
        $response = $this->_productMigrationMapper->changeScreeningQuantity();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    public function copySupplierPriceFromProductLevelAction()
    {
        $response = $this->_productMigrationMapper->copySupplierPriceFromProductToVariants();
        $response = array('success' => $response);
        return new JsonModel($response);
    }

    /**
     *  THIS FUNCTION IS TO CALCULATE NEW ACCOUNT NR FOR TNP - SPECTRUM AND GATEWAY
     */
    public function tnpTESTAction()
    {
        throw new \Exception('Invalid action'); // Please ask Cristian Popescu before un-commenting this line

        $response = $this->_productMigrationMapper->tnpAccount();
        $response = array('success' => $response);
        return new JsonModel($response);
    }
}