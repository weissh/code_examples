<?php

namespace PrismProductsManager\Controller;

use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use PrismProductsManager\Entity\Repository\BulkProcessingRepository;
use PrismProductsManager\Form\SearchBulkProcessingForm;
use PrismUsersManager\Model\UsersClientMapper;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Paginator;
use Zend\View\Model\ViewModel;

class BulkProcessesController extends AbstractActionController
{
    private $repository;
    private $usersClientMapper;
    private $cachedSubmittedBy;

    public function __construct(BulkProcessingRepository $repository, UsersClientMapper $usersClientMapper)
    {
        $this->repository = $repository;
        $this->usersClientMapper = $usersClientMapper;
    }

    public function indexAction()
    {
        $keyword      = $this->params()->fromQuery('keyword', '');
        $process_type = $this->params()->fromQuery('process_type', 0);
        $status       = $this->params()->fromQuery('status', 0);
        $page         = (int) $this->params()->fromRoute('page', 1);
        $newSearch    = $this->params()->fromQuery('new-search', 0);

        $searchForm = new SearchBulkProcessingForm();
        $searchForm->init();

        $searchForm->setData($this->getRequest()->getQuery());
        $searchForm->isValid();

        // if search form triggered then set the page value to 1
        if(isset($newSearch) && $newSearch == 1 ){
            $page = 1;
        }

        $list = $this->loadBulkProcessData($keyword, $process_type, $status, $page);
        $count = $this->loadBulkProcessData($keyword, $process_type, $status, $page, true);

        foreach ($list as $key => $bulkRow) {
            $submittedBy = $bulkRow->getSubmittedBy();
            if (isset($this->cachedSubmittedBy[$submittedBy])) {
                $list[$key]->setSubmittedBy($this->cachedSubmittedBy[$submittedBy]);
            } else {
                $submittedByUser = $this->usersClientMapper->getUser($submittedBy);

                if (empty($submittedByUser)) {
                    $submittedByUser = '';
                } else {
                    $submittedByUser = $submittedByUser->emailAddress;
                }

                $list[$key]->setSubmittedBy($submittedByUser);
                $this->cachedSubmittedBy[$submittedBy] = $submittedByUser;
            }
        }

        //Check if the form was submitted with search then set the page value to empty
        //if ($searchForm->isPost() && !empty($search)) {
        //    $page= 1;
        //}

        return new ViewModel([
            'form' => $searchForm,
            'list' => $list,
            'keyword' => $keyword,
            'process_type' => $process_type,
            'status' => $status,
            'page' => $page,
            'perPageGet' => 100,
            'count' => $count,
        ]);
    }

    /**
     * @param $keyword
     * @param $process_type
     * @param $status
     * @param $page
     * @param bool $selectCount
     * @return array|int
     */
    private function loadBulkProcessData($keyword, $process_type, $status, $page, $selectCount = false)
    {
        return $this->repository->getBulkProcessingByProcessType($keyword, $process_type, $status, $page, $selectCount);
    }

    // for ajax call to reload existing
    public function reloadBulkProcessAction()
    {
        $keyword      = $this->params()->fromQuery('keyword', '');
        $process_type = $this->params()->fromQuery('process_type', 0);
        $status       = $this->params()->fromQuery('status', 0);
        $page         = (int) $this->params()->fromRoute('page', 1);

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);

        $list = $this->loadBulkProcessData($keyword, $process_type, $status, $page);

        foreach ($list as $key => $bulkRow) {
            $submittedBy = $bulkRow->getSubmittedBy();
            if (isset($this->cachedSubmittedBy[$submittedBy])) {
                $list[$key]->setSubmittedBy($this->cachedSubmittedBy[$submittedBy]);
            } else {
                $submittedByUser = $this->usersClientMapper->getUser($submittedBy);

                if (empty($submittedByUser)) {
                    $submittedByUser = '';
                } else {
                    $submittedByUser = $submittedByUser->emailAddress;
                }

                $list[$key]->setSubmittedBy($submittedByUser);
                $this->cachedSubmittedBy[$submittedBy] = $submittedByUser;
            }
        }

        $viewModel->setVariables([
            'list' => $list,
            'keyword' => $keyword,
            'process_type' => $process_type,
            'status' => $status,
            'page' => $page,
            'perPageGet' => 100,
            'count' => $this->loadBulkProcessData($keyword, $process_type, $status, $page, true),
        ]);

        return $viewModel;
    }
}