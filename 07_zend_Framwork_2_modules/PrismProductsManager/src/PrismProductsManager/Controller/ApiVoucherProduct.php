<?php
namespace PrismProductsManager\Controller;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use PrismMediaManager\Model\ProductFolderMapper;
use PrismProductsManager\Model\ApiProductsMapper;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 * Api Voucher Product Controller
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiVoucherProduct extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * Api Product Mapper
     * PrismProductsManager\Model\ApiProductsMapper
     * @var object
     */
    protected $_apiProductsMapper;

    /**
     * @var ProductFolderMapper
     */
    protected $_productFolderMapper;

    /**
     * @param ApiProductsMapper $apiProductsMapper
     * @param ProductFolderMapper $productFolderMapper
     */
    public function __construct(
        ApiProductsMapper $apiProductsMapper,
        ProductFolderMapper $productFolderMapper)
    {
        $this->_apiProductsMapper = $apiProductsMapper;
        $this->_productFolderMapper = $productFolderMapper;
    }

    /**
     * @param mixed $id
     * @return array|mixed|void
     * @throws \Exception
     */
    public function get($id)
    {
        $this->_methodNotAllowed();
    }

    /**
     * @return JsonModel
     * @throws \Exception
     */
    public function getList()
    {

        $websiteId = $this->params()->fromRoute('websiteId');
        $languageId = $this->params()->fromRoute('languageId');

        if (empty($websiteId) || empty($languageId)) {
            $this->_methodNotAllowed();
        }

        $evoucherProduct = array();
        $intValidator = new \Zend\Validator\NotEmpty(\Zend\Validator\NotEmpty::INTEGER);
        if ($intValidator->isValid($websiteId) && $intValidator->isValid($languageId)) {
            $evoucherProduct = $this->_apiProductsMapper->getVoucherProduct($websiteId, $languageId);
            $evoucherProduct['images'] = $this->_productFolderMapper->getSpecialImageByProductAndVariant($evoucherProduct['productId'], 0, 'default', 'full');
        }

        return new JsonModel($evoucherProduct);
    }

    /**
     * @param mixed $data
     * @return array|mixed|void
     * @throws \Exception
     */
    public function create($data)
    {
        $this->_methodNotAllowed();
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|mixed|void
     * @throws \Exception
     */
    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    /**
     * @param mixed $id
     * @return array|mixed|void
     * @throws \Exception
     */
    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}