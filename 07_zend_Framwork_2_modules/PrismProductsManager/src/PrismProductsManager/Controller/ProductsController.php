<?php
namespace PrismProductsManager\Controller;

use PrismElasticSearch\Enum\ElasticSearchTypes;
use PrismProductsManager\Form\InputFieldsForm;
use PrismProductsManager\Form\MarketingCategorySelectForm;
use PrismProductsManager\Form\MerchantCategorySelectForm;
use PrismProductsManager\Form\ProductAttributesSelectForm;
use PrismProductsManager\Form\ProductVariantsCreationForm;
use PrismProductsManager\Form\ProductsActivationForm;
use PrismProductsManager\Form\ProductsRelatedProductForm;
use PrismProductsManager\Model\ColoursMapper;
use PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesDefinitionsTable;
use PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable;
use PrismProductsManager\Model\ProductsMapper;
use PrismProductsManager\Service\CommonAttributesService;
use PrismProductsManager\Service\InputService;
use PrismProductsManager\Service\WorkflowService;
use PrismQueueManager\Service\QueueService;
use Zend\Db\Sql\Select;
use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use PrismCommon\Adapter\CacheAdapter;

/**
 * Class ProductsController
 *
 * @package PrismProductsManager\Controller
 */
class ProductsController extends AbstractActionController
{
    // TODO re-factor to use the methods in Service/ProductsUpdateService instead of calling the queueService Directly

    /**
     *
     * @var null
     */
    protected $theMediaServerAPI = null;

    /**
     *
     * @var ProductsMapper
     */
    protected $_productsMapper;

    /**
     *
     * @var
     *
     */
    protected $_attributesGroupMapper;

    /**
     *
     * @var MerchantCategorySelectForm
     */
    protected $_merchantCategorySelectForm;

    /**
     *
     * @var MarketingCategorySelectForm
     */
    protected $_marketingCategorySelectForm;

    /**
     *
     * @var ProductAttributesSelectForm
     */
    protected $_productAttributeSelectForm;

    /**
     *
     * @var
     *
     */
    protected $_productsBasicInformationForm;

    /**
     *
     * @var ProductsActivationForm
     */
    protected $_productsActivationForm;

    /**
     *
     * @var ProductVariantsCreationForm
     */
    protected $_productVariantCreationForm;

    /**
     *
     * @var ProductsRelatedProductForm
     */
    protected $_productRelatedProductsForm;

    /**
     *
     * @var object form
     */
    protected $_productSupplierPriceForm;

    /**
     *
     * @var
     *
     */
    protected $_currentLanguage;

    /**
     *
     * @var
     *
     */
    protected $_currentWebsite;

    /**
     *
     * @var string
     */
    protected $_firstPartOfSku;

    /**
     *
     * @var
     *
     */
    protected $_currencyConfig;

    /**
     *
     * @var QueueService
     */
    protected $_queueService;

    /**
     *
     * @var
     *
     */
    protected $_siteConfig;

    /**
     *
     * @var inputService
     */
    protected $inputService;

    /**
     *
     * @var inputFieldsForm
     */
    protected $inputFieldsForm;

    /**
     *
     * @var CommonAttributesService
     */
    protected $commonAttributesService;

    /**
     *
     * @var ColoursMapper
     */
    protected $coloursMapper;

    /**
     *
     * @var string
     */
    protected $errorType = 'success';

    /**
     *
     * @var string
     */
    protected $errorMessage = '';

    /**
     *
     * @var WorkflowService
     */
    protected $workflowService;

    /**
     * @var
     */
    protected $cacheAdapter;

    /**
     * ProductsController constructor.
     * @param ProductsMapper $productsMapper
     * @param $languagesConfig
     * @param $websitesConfig
     * @param $currencyConfig
     * @param $siteUrl
     * @param QueueService $queueService
     * @param $config
     * @param $clientConfig
     * @param CommonAttributesValuesDefinitionsTable $commonAttributesValuesDefinitionsTable
     * @param CommonAttributesValuesTable $commonAttributesValuesTable
     * @param InputService $inputService
     * @param InputFieldsForm $inputFieldsForm
     * @param CommonAttributesService $commonAttributesService
     * @param ColoursMapper $coloursMapper
     * @param WorkflowService $workflowService
     * @param CacheAdapter $cacheAdapter
     */
    public function __construct(
        ProductsMapper $productsMapper,
        $languagesConfig,
        $websitesConfig,
        $currencyConfig,
        $siteUrl,
        QueueService $queueService,
        $config,
        $clientConfig,
        CommonAttributesValuesDefinitionsTable $commonAttributesValuesDefinitionsTable,
        CommonAttributesValuesTable $commonAttributesValuesTable,
        InputService $inputService,
        InputFieldsForm $inputFieldsForm,
        CommonAttributesService $commonAttributesService,
        ColoursMapper $coloursMapper,
        WorkflowService $workflowService,
        CacheAdapter $cacheAdapter
    )
    {
        $this->_productsMapper = $productsMapper;
        $this->_siteUrl = $siteUrl;
        $this->_currentLanguage = $languagesConfig['current-language'];
        $this->_currentWebsite = $websitesConfig['current-website-id'];
        $this->_currencyConfig = $currencyConfig;
        $this->_firstPartOfSku = 'style';
        $this->_queueService = $queueService;
        $this->_siteConfig = $config;
        $this->clientConfig = $clientConfig;
        $this->commonAttributesValuesDefinitionsTable = $commonAttributesValuesDefinitionsTable;
        $this->commonAttributesValuesTable = $commonAttributesValuesTable;
        $this->inputService = $inputService;
        $this->inputFieldsForm = $inputFieldsForm;
        $this->commonAttributesService = $commonAttributesService;
        $this->coloursMapper = $coloursMapper;
        $this->workflowService = $workflowService;
        $this->cacheAdapter = $cacheAdapter;
    }

    /**
     *
     * @return ViewModel
     */
    public function indexAction()
    {

        return new ViewModel([]);
    }

    /**
     *
     * @return ViewModel
     */
    public function displayAction()
    {
        $option = null;
        /**
         * Check if the user is authorized to PMS
         */
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));

        /**
         * Retrieve url params
         */
        $pageUrl = $this->params('displayPage');
        $style = $this->params('style');

        if($this->params('option')){
            $option = $this->_productsMapper->formatOptionCode($this->params('option'));
        }


        /**
         * Retrieve all product tabs
         */
        $inputPages = $this->inputService->getAllPages();
        $formError = false;

        /**
         * Retrieve all input fields for current page
         */
        $allOptions = array();
        if (empty($option) && ! empty($style)) {
            //$allOptions = $this->_productsMapper->getAllOptionsForStyle($style, true);
            $allOptions = $this->getOptionsData($style);
        }

        /**
         * Take field values from the form (save 1.5 seconds)
         */
        $inputFieldsElements = $this->inputFieldsForm->getOriginalDynamicPageFields();

        /**
         * Check if the tab has price module
         */
        $pricesFlag = false;
        foreach ($inputFieldsElements as $fieldsets) {
            foreach ($fieldsets as $inputFieldSet) {
                foreach ($inputFieldSet as $inputField => $inputFieldData) {
                    if ($inputField == 'isModule') {
                        if ($inputFieldData->getinputmoduleref() == 'prices-module') {
                            $pricesFlag = true;
                            break;
                        }
                    }
                }
            }
        }

        $request = $this->getRequest();
        $validForm = false;
        /**
         * Check if request is post - needed for create / edit
         */
        if ($request->isPost()) {
            $submitedFormData = $this->params()->fromPost();

            /**
             * Check if the option creation values are populated to create new options (skus)
             */
            if (! empty($submitedFormData['option-creation-colour']) && ! empty($submitedFormData['option-creation-size'])) {
                $this->createNewOption($style, $submitedFormData);
            }
            $inputFieldData = array();
            /**
             * Build array to set the form with data
             */
            if (! empty($submitedFormData['inputfield'])) {
                foreach ($submitedFormData['inputfield'] as $inputFieldId => $inputFieldValue) {
                    $inputFieldData['inputfield[' . $inputFieldId . ']'] = $inputFieldValue;
                }
            }
            /**
             * Build array to set the form with data
             */
            $priceData = array();
            if (! empty($submitedFormData['prices'])) {
                foreach ($submitedFormData['prices'] as $pricesDetails => $price) {
                    $priceData['prices[' . $pricesDetails . ']'] = $price;
                }
            }

            /**
             * This was added to allow inject disabled and validate common attributes type select for the style or
             * options that was previously set with this attributes
             *
             * This is a temporray solution that should be removed and managed in better way
             * To this Display action need refactoring
             */
            $this->updateSelectValueOptionWithDisabledCommonAttriubtes($inputFieldData);

            /**
             * Set the form with data *
             */
            $mergedData = array_merge($submitedFormData, $inputFieldData, $priceData);

            unset($mergedData['inputfield']);
            $this->inputFieldsForm->setData($mergedData);

            /**
             * Check if the form is valid
             */
            /**
             * Comment out form validation until form elements are finished
             */
            if ($this->inputFieldsForm->isValid()) {
                $validForm = true;

                $formData = $this->inputFieldsForm->getData();
                /**
                 * If no style is present it means that we need to create new style
                 */
                if (is_null($style)) {
                    $styleFlag = $this->_productsMapper->createNewStyle($formData);
                    if ($styleFlag) {
                        /**
                         * Check if any workflows have been completed
                         */
                        $this->checkIfWorkflowIsCompleted($style, $option);

                        $route = array(
                            'controller' => 'ProductsController',
                            'action' => 'display',
                            'stage' => $pageUrl,
                            'style' => $styleFlag
                        );
                        /**
                         * Check if any workflows have been completed
                         */
                        return $this->_errorValue($route, 'success', 'Style was successful created.');
                    } else {
                        /** Jira ticket FUL-478 **/
                        $this->showFlashMessenger(false, 'error', 'Insufficient barcodes available.');
                    }
                } else {
                    $updatePricesFlag = true;
                    /**
                     * If a style is present it means that we need to modify the style or the option depends on the level we are
                     */
                    if (is_null($option)) {
                        /**
                         * Edit style level
                         */
                        $styleFlag = $this->_productsMapper->updateStyle($style, $formData, $updatePricesFlag);
                        if ($styleFlag) {

                            if (isset($this->clientConfig['settings']['showOptionsWhenSavingAtStyleLevel']) &&
                                $this->clientConfig['settings']['showOptionsWhenSavingAtStyleLevel'] == true)
                            {
                                /**
                                 * In this case it will save at option level based on what you have selected on the ui for options
                                 */
                                /**
                                 * Check if the changes to go other option level and save changes for the options
                                 */
                                if (isset($submitedFormData['options-to-copy'])) {
                                    $optionsToCopy = json_decode($submitedFormData['options-to-copy']);
                                    if (! empty($optionsToCopy)) {

                                        try {
                                            $this->_productsMapper->startTransaction();
                                            foreach ($optionsToCopy as $optionCode) {
                                                $optionData = $this->_productsMapper->updateOption($optionCode, $formData, $updatePricesFlag);

                                                if ($updatePricesFlag) {
                                                    /**
                                                     * Add sip entry in case prices where updated
                                                     */
                                                    $this->_queueService->addSipEntryToQueue($optionCode, 6, $this->_productsMapper, true);
                                                }
                                                /**
                                                 * Add Sip entry for product data
                                                 */
                                                $this->_queueService->addSipEntryToQueue($optionCode, 5, $this->_productsMapper, true);

                                                /**
                                                 * Add to spectrum
                                                 */
                                                $variants = $this->_productsMapper->getVariantsIdForOption($optionCode);
                                                foreach ($variants as $variant) {
                                                    $this->_queueService->addSpectrumEntryToQueueForCommonAttributes($variant['productAttributeId']);
                                                }

                                                $this->checkIfWorkflowIsCompleted($style, $optionCode);

                                                // re-cache all option data
                                                $this->updateCachedOptionsData($style);
                                            }
                                            $this->_productsMapper->commitTransaction();

                                        } catch (\Exception $ex) {
                                            $this->_productsMapper->rollbackTransaction();
                                            throw $ex;
                                        }
                                    }
                                }
                            } else {
                                /**
                                 * In this case it will save at option all level all fields that are at style level
                                 */
                                try {
                                    $this->_productsMapper->startTransaction();
                                    foreach ($allOptions as $optionCode => $variants) {
                                        $oldRadyForWebValue = $this->isReadyForWeb($optionCode);

                                        $this->_productsMapper->updateOption($optionCode, $formData, true);

                                        $newRadyForWebValue = $this->isReadyForWeb($optionCode);

                                        //This condition is been added to check if option was turn off ready for web so update to SIP will be fored
                                        $forceSipUpdate = false;

                                        if ($oldRadyForWebValue == true && $newRadyForWebValue == false) {
                                            $forceSipUpdate = true;
                                        }

                                        $variants = $this->_productsMapper->getVariantsIdForOption($optionCode);
                                        foreach ($variants as $variant) {
                                            $this->_queueService->addSpectrumEntryToQueueForCommonAttributes($variant['productAttributeId']);
                                        }

                                        $this->_queueService->addSipEntryToQueue($optionCode, 5, $this->_productsMapper, true, $forceSipUpdate);
                                        if ($updatePricesFlag) {
                                            /**
                                             * Add Sip entry for Product Prices
                                             */
                                            $this->_queueService->addSipEntryToQueue($optionCode, 6, $this->_productsMapper, true, $forceSipUpdate);
                                        }

                                        $this->checkIfWorkflowIsCompleted($style, $optionCode);

                                        // re-cache all option data
                                        $this->updateCachedOptionsData($style);
                                    }
                                    $this->_productsMapper->commitTransaction();
                                } catch (\Exception $ex) {
                                    $this->_productsMapper->rollbackTransaction();
                                    throw $ex;
                                }
                            }
                            /**
                             * Check if any workflows have been completed
                             */
                            $this->checkIfWorkflowIsCompleted($style, $option);

                            $this->showFlashMessenger(false, 'success', 'Style was successfully updated.');
                        } else {
                            $this->showFlashMessenger(false, 'error', 'An error has occurred. Please contact PRISM DM quoting code ESR-001');
                        }
                    } else {
                        /**
                         * Edit option level
                         */
                        $optionData = $this->_productsMapper->updateOption($option, $formData, $updatePricesFlag, true);

                        if ($optionData) {
                            $msg = 'Option was successfully updated.';
                            $type = 'success';

                            /**
                             * Check if any workflows have been completed
                             */
                            $this->checkIfWorkflowIsCompleted($style, $option);

                            // re-cache all option data
                            $this->updateCachedOptionsData($style);

                            /**
                             * Add Spectrum Entry
                             */
                            $variants = $this->_productsMapper->getVariantsIdForOption($option);
                            foreach ($variants as $variant) {
                                $this->_queueService->addSpectrumEntryToQueueForCommonAttributes($variant['productAttributeId']);
                            }
                                /**
                                 * Add Sip entry for product data
                                 */
                                $this->_queueService->addSipEntryToQueue($option, 5, $this->_productsMapper, true);
                                if ($updatePricesFlag) {
                                    /**
                                     * Add Sip entry for Product Prices
                                     */
                                    $this->_queueService->addSipEntryToQueue($option, 6, $this->_productsMapper, true);
                                }

                        } else {
                            $msg = 'An error has occurred. Please contact PRISM DM quoting code ESR-001.';
                            $type = 'error';
                        }
                        $this->showFlashMessenger(false, $type, $msg);
                    }
                }
            } else {
                $formError = true;
            }
        }else{

            /**
             * Retrieve price data if the form was not submitted 
             */
            $priceData = array();
            if (! empty($style) && $pricesFlag) {
                $priceData = $this->_productsMapper->getProductPricesForStyleOrOption($style, $option);
            }


            /**
             * Hydrate form with price data
             */
            if (! empty($priceData)) {
                $this->hydrateFormWithPriceData($priceData);
            }
        }

        $options = array();
        /**
         * Retrieve all options for style
         */
        if (! empty($style)) {
            //$options = $this->_productsMapper->getAllOptionsForStyle($style);
            $options = $this->getOptionsData($style);
        }

        /**
         * Hydrate form if style level selected
         */
        if (! empty($style) && empty($option) && (!$request->isPost() || $validForm)) {
            $this->hydrateFormDataForStyle($style, $inputFieldsElements);
        }



        $lastWorkflowForOption = array();
        /**
         * Get the next available workflow for the current option
         */
        if (! empty($option)) {
            $variantDetails = $this->_productsMapper->getVariantDetailsByStyle($option);
            if (empty($variantDetails)) {
                $variantDetails = $this->_productsMapper->getVariantDetailsByStyle(str_replace(' ', '', $option));
            }
            $lastWorkflowForOption = $this->workflowService->getNextWorkflowsToValidateFor($variantDetails['productAttributeId'], 'PRODUCTS_R_ATTRIBUTES', true);
        }

        /**
         * Hydrate form if option level selected
         */
        if (! empty($option) && (!$request->isPost() || $validForm)) {
            $this->hydrateFormDataForOption($option, $inputFieldsElements);
        }

        /**
         * Retrieve the value of the ProductType attribute that is set up to this style
         */
        if (! is_null($style)) {
            $commonAttributes = $this->_productsMapper->getCommonAttributesUsedForStyle($style);
            $productTypeValue = $this->commonAttributesService->getCommonAttributeValueAssignedToProduct('Product Type', $commonAttributes);
        }

        /**
         * Get all size bridges
         */
        $sizeBridges = $this->inputService->getSizeBrides();
        $colours = $this->coloursMapper->fetchAllColourGroups($this->_currentLanguage, null, true);

        /**
         * Retrieve all styles
         */
        $optionsData = array();
        $styles = array();

        $styleDetails = array();
        $selectedCurrencyCode = '';
        if (! empty($style)) {
            $styleDetails = $this->_productsMapper->getStyleInfo($style);
            $selectedCurrencyCode = $this->_productsMapper->getSelectedCurrencyBySKUGroup($option);
        }
        $this->layout()->setVariable('selectedCurrencyCode', $selectedCurrencyCode);

        // add allowed chained data
        $this->inputFieldsForm->setAllowedChainData();

        return new ViewModel(array(
            'inputFieldElements' => $inputFieldsElements,
            'inputFieldsForm' => $this->inputFieldsForm,
            'inputPages' => $inputPages,
            'viewTab' => $pageUrl,
            'currencyConfig' => $this->_currencyConfig,
            'style' => $style,
            'styleDetails' => $styleDetails,
            'option' => $option,
            'sizeBridges' => $sizeBridges,
            'colours' => $colours,
            'options' => $options,
            'optionsData' => $optionsData,
            'formError' => $formError,
            'styles' => $styles,
            'workflows' => $this->workflowService->getAllWorkflows(),
            'lastWorkflowForOption' => $lastWorkflowForOption,
            'errorType' => $this->errorType,
            'errorMessage' => $this->errorMessage,
        ));
    }

    public function saveBarcodesAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        // Get the request
        $request = $this->getRequest();
        $result = array();

        if ($request->isPost()) {
            // Retrieve the submitted data form.
            $params = $this->params()->fromPost();
            $values = isset($params['values']) ? $params['values'] : array();

            $result = $this->_productsMapper->assignBarcodesAgainstOptions($values);
        }

        $response = $this->getResponse();

        $response->setContent(json_encode($result));
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        return $response;

    }
    public function manageBarcodesAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        // Get the request
        $request = $this->getRequest();

        // Retrieve the submitted data form.
        $params = $this->params()->fromPost();

        $optionCode = isset($params['option']) ? $params['option'] : '';
        $style = isset($params['style']) ? $params['style'] : '';

        $returnArray = array('success' => false, 'html' => '');

        if (!empty($style) && $request->isPost()) {
            $productId = $this->_productsMapper->getProductIdFromStyle($style);
            if (!empty($productId)) {
                /** If option code is present get SKUS based on option **/
                if (!empty($optionCode)) {
                    $variants = $this->_productsMapper->getVariantsIdForOption($optionCode);
                } else {
                    /** If no option code is provided get all variants for style */
                    $variants = $this->_productsMapper->getVariantsBasedOnProductId($productId);
                }

                $skuRowsView = new ViewModel();
                $skuRowsView->setTerminal(true);
                $skuRowsView->setTemplate('prism-products-manager/products/partials/manage-barcodes/sku-rows');
                $skuRowsView->setVariable('variants', $variants);
                $skuRowsView->setVariable('styleCode', $style);
                $skuRowsView->setVariable('optionCode', $optionCode);
                $skuRowsViewOutput = $this->getServiceLocator()
                    ->get('viewrenderer')
                    ->render($skuRowsView);
                $skuRowsView = utf8_encode($skuRowsViewOutput);
                $returnArray = array('success' => true, 'html' => $skuRowsView);
            }
        }

        $response = $this->getResponse();

        $response->setContent(json_encode($returnArray));
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        return $response;
    }


    /**
     * Check if the common attribute "ready for web" is slected
     *
     * @param string $optionCode
     * @return boolean
     */
    protected function isReadyForWeb($optionCode)
    {
        return $this->_productsMapper->isReadyForWeb($optionCode);
    }

    /**
     * This should be used only to inject disable common attibute value into vlidation form
     * The reason for this is some legacy product was copied from old system and client do not wish
     * some of the attibute to be in option hover if a product was set with deisabled attribute they want to make
     * avalible to re use.
     * please note this will work only untill you change the attribute valuel.
     *
     * Currently it work with single select only
     *
     * @param unknown $inputFieldData
     */
    protected function updateSelectValueOptionWithDisabledCommonAttriubtes($inputFieldData)
    {
        $formElements = $this->inputFieldsForm->getElements();
        // Get only fields with value
        foreach ($inputFieldData as $fieldId => $value) {
            if (! empty($value)) {
                // Get the valueOption from the form object
                if (array_key_exists($fieldId, $formElements)) {
                    if ($formElements[$fieldId]->getAttributes()['type'] == 'select') {
                        $valueOptionList = $formElements[$fieldId]->getvalueOptions();
                        if (is_array($value)) {
                            $chagnedMade = false;
                            foreach ($value as $valueRow) {
                                if (! array_key_exists($valueRow, $valueOptionList)) {
                                    $optionValue = $this->getCommonAttributeValueCodeByValueId($valueRow);
                                    $valueOptionList[$valueRow] = $optionValue;
                                    $chagnedMade = true;
                                }
                            }
                            if ($chagnedMade == true) {
                                $this->inputFieldsForm->getElements()[$fieldId]->setValueOptions($valueOptionList);
                                // $s = $this->inputFieldsForm->getElements()[$fieldId]->getValueOptions();
                            }
                        } else {
                            if (! array_key_exists($value, $valueOptionList)) {
                                $optionValue = $this->getCommonAttributeValueCodeByValueId($value);
                                $valueOptionList[$value] = $optionValue;
                                $this->inputFieldsForm->getElements()[$fieldId]->setValueOptions($valueOptionList);
                                // $s = $this->inputFieldsForm->getElements()[$fieldId]->getValueOptions();
                            }
                        }
                    }
                }
            }
        }
        return;
    }

    /**
     * To update the price of style without updating the options price
     * The reason is to prepare update original values that will be sued to setup new added options
     */
    public function saveValuesAtStyleLevelForPricesAction()
    {
        $this->isCmsUserAuthorized(array(
            'edit'
        ));
        $request = $this->getRequest();

        if ($request->isPost()) {
            /**
             * Retrieve the submitted data form.
             */
            $submitedFormData = $this->params()->fromPost();
            $style = $submitedFormData['style'];
            $tab = $submitedFormData['tab'];

            unset($submitedFormData['style'], $submitedFormData['tab']);
            foreach ($submitedFormData['values'] as &$value) {
                if (is_numeric($value)) {
                    $value = floatval($value);
                }
            }

            $this->_productsMapper->savePriceDataAgainst($submitedFormData, $style);
        }

        $route = array(
            'controller' => 'ProductsController',
            'action' => 'display',
            'displayPage' => $tab,
            'style' => $style
        );
        return $this->_errorValue($route, 'success', 'Values have been modified');
    }

    /**
     *
     * @return null
     */
    public function saveValuesAtOptionLevelForPricesAction()
    {
        $this->isCmsUserAuthorized(array(
            'edit'
        ));
        $request = $this->getRequest();

        if ($request->isPost()) {
            /**
             * Retrieve the submitted data form.
             */
            $submitedFormData = $this->params()->fromPost();
            $style = $submitedFormData['style'];
            $tab = $submitedFormData['tab'];

            unset($submitedFormData['style'], $submitedFormData['tab']);
            foreach ($submitedFormData['values'] as &$value) {
                if (is_numeric($value)) {
                    $value = floatval($value);
                }
            }
            /**
             * If the all values are the same then update also the style level
             */
            if (count(array_unique($submitedFormData['values'])) == 1) {
                $this->_productsMapper->savePriceDataAgainst($submitedFormData, $style);
            }
            $this->_productsMapper->savePriceDataAgainst($submitedFormData, $style, 'PRODUCTS_R_ATTRIBUTES');

            /**
             * Add data to queue
             */
            $productId = $this->_productsMapper->getProductIdFromStyle($style);

            $options = $this->_productsMapper->getOptionsCode($submitedFormData);
            foreach ($options as $optionCode) {
                $this->_queueService->addSipEntryToQueue($optionCode, 6, $this->_productsMapper, true);


                $variants = $this->_productsMapper->getVariantsIdForOption($optionCode);
                foreach ($variants as $variant) {
                    $this->_queueService->addSpectrumEntryToQueueForCommonAttributes($variant['productAttributeId']);
                }
            }

            $route = array(
                'controller' => 'ProductsController',
                'action' => 'display',
                'displayPage' => $tab,
                'style' => $style
            );
            return $this->_errorValue($route, 'success', 'Values have been modified');
        }
    }

    /**
     * To update the value of style without updateing the options values
     * The reason is to preper upate original vlaues that will be sued to setup new added options
     */
    public function saveValuesAtStyleLevelAction()
    {
        $this->isCmsUserAuthorized(array(
            'edit'
        ));
        $request = $this->getRequest();

        if ($request->isPost()) {
            /**
             * Retrieve the submitted data form.
             */
            $submitedFormData = $this->params()->fromPost();
            $style = $submitedFormData['style'];
            $tab = $submitedFormData['tab'];

            unset($submitedFormData['style'], $submitedFormData['tab'], $submitedFormData['submit']);

            $this->_productsMapper->saveStyleValue($submitedFormData);
        }

        $route = array(
            'controller' => 'ProductsController',
            'action' => 'display',
            'displayPage' => $tab,
            'style' => $style
        );
        return $this->_errorValue($route, 'success', 'Values have been modified');
    }

    /**
     *
     * @return null
     */
    public function saveValuesAtOptionLevelAction()
    {
        $this->isCmsUserAuthorized(array(
            'edit'
        ));
        $request = $this->getRequest();

        if ($request->isPost()) {
            /**
             * Retrieve the submitted data form.
             */
            $submitedFormData = $this->params()->fromPost();
            $style = $submitedFormData['style'];
            $tab = $submitedFormData['tab'];

            unset($submitedFormData['style'], $submitedFormData['tab']);
            if ($this->isValueSameToAllOptions($submitedFormData) == true) {
                $this->_productsMapper->saveDataAgainstStyle($submitedFormData);
            }

            $this->_productsMapper->saveDataAgainstOption($submitedFormData);

            // Submit data into Spectrum and SIP
            $options = $this->_productsMapper->getOptionsFromInputFieldData($submitedFormData);
            $this->saveToSIPAndSpectrumPopup($options);

            $route = array(
                'controller' => 'ProductsController',
                'action' => 'display',
                'displayPage' => $tab,
                'style' => $style
            );
            return $this->_errorValue($route, 'success', 'Values have been modified');
        }
    }

    protected function saveToSIPAndSpectrumPopup($options)
    {
        foreach ($options as $optionId){

            $variants = $this->_productsMapper->getVariantsIdForOption($optionId);

            foreach ($variants as $variant) {
                $variantId = $variant['productAttributeId'];
                $this->_queueService->addSpectrumEntryToQueueForCommonAttributes($variantId);
                $this->_queueService->addSipEntryToQueue($optionId, 5, $this->_productsMapper, true);
            }
        }
    }

    /**
     * Comapre the vlue of a common attribute if same accross the option
     * This will be used to update the value on style level
     *
     * @param array $submitedFormData
     * @return boolean
     */
    public function isValueSameToAllOptions($submitedFormData)
    {
        //if is an select, text etc.
        if ($this->getValueType($submitedFormData) == 1 && count(array_unique($submitedFormData)) == 1) {
            return true;
        }
        //if it is an multiselect
        else {
            //it is an multiselect for shure?
            if($this->getValueType($submitedFormData) == 2) {
                //take first array from multiarray
                $first = array_values($submitedFormData)[0];
                //serialize it
                $first = serialize($first);

                //loop submited data and compare serialized arrays
                foreach ($submitedFormData as $option => $values)
                {
                    if($first == serialize($values)){
                        continue;
                    }
                    else {
                        return false;
                    }
                }

                return true;
            }
            else {
                return false;
            }
        }
    }

    /**
     * check if the given value is string, simpel array, multidemensioal array or object
     *
     * @return int 0= string, 1= simpel array, 2 = mlutidemensioal array, 3 = object
     */
    protected function getValueType($value)
    {
        if (is_scalar($value)) {
            return 0;
        }
        if (is_object($value)) {
            return 3;
        }

        if (is_array($value)) {
            if (count($value) != count($value, COUNT_RECURSIVE)) {
                return 2;
            }
            return 1;
        }
    }

    /**
     *
     * @param
     *            $style
     * @param
     *            $option
     * @return array|bool
     */
    private function checkIfWorkflowIsCompleted($style, $option)
    {
        /**
         * Returns false if the next workflow is not completed and if it is completed returns the workflow entity
         */
        $workflow = $this->workflowService->checkIfNextWorkFlowHaveBeenCompletedFor($style, $option);

        if ($workflow) {
            /**
             * Save the workflow against the product
             */
            $successFlag = $this->workflowService->saveWorkFlowToProduct($style, $option, $workflow);
            /**
             * If new workflow has been saved against the product check if the next workflow is completed or not recursive
             */
            if ($successFlag) {
                return $this->checkIfWorkflowIsCompleted($style, $option);
            }
        }
        return $workflow;
    }

    /**
     *
     * @return mixed
     */
    public function getOptionsForProductAction()
    {
        $this->isCmsUserAuthorized(array(
            'view'
        ));
        // Retrieve the submitted data form.
        $style = $this->params()->fromPost('style', false);

        if ($style) {
            $options = $this->_productsMapper->fetchAllOptionsForProduct(array(
                'style' => $style
            ));
            $options = $options['options'];

            $optionRowsView = new ViewModel();
            $optionRowsView->setTerminal(true);
            $optionRowsView->setTemplate('prism-products-manager/products/partials/input-field-partials/option-rows');
            $optionRowsView->setVariable('options', $options);
            $optionRowsViewOutput = $this->getServiceLocator()
                ->get('viewrenderer')
                ->render($optionRowsView);
            $optionRowsView = utf8_encode($optionRowsViewOutput);
            $response = array(
                'data' => $optionRowsView
            );
        } else {
            $response = array(
                'data' => array()
            );
        }

        return $this->getResponse()->setContent(json_encode($response));
    }

    public function getPricesValuesForAllOptionsAction()
    {
        $this->isCmsUserAuthorized(array(
            'view'
        ));

        // Retrieve the submitted data form.
        $style = $this->params()->fromPost('style', false);
        $tab = $this->params()->fromPost('tab', false);
        $priceType = $this->params()->fromPost('priceType', false);
        $territory = $this->params()->fromPost('territory', false);
        $currency = $this->params()->fromPost('currency', false);

        if (! empty($style) && ! empty($priceType)) {

            /**
             * Retrieve Style price data
             */
            $priceData = $this->_productsMapper->getProductPricesForStyleOrOption($style, null);

//            $allOptions = $this->_productsMapper->getAllOptionsForStyle($style, true);
            $allOptions = $this->getOptionsData($style);

            $possibleValues = array();
            $currentValues = false;
            $type = 'text';
            $label = '';
            $styleCurrentPriceValue = false;

            $stylePossibleValues = [];
            $currentValueCodeCurrency = [];

            switch ($priceType) {
                case 'supplier-currency':
                    /**
                     * Select Type
                     */
                    $type = 'select';
                    $label = 'Trading Currency';
                    $possibleValues = $this->_productsMapper->getSuppliersCurrency(true);
                    $stylePossibleValues = $this->_productsMapper->getSuppliersCurrency(true);
                    $currentValues = $this->_productsMapper->getSupplierCurrenciesForOptions($allOptions);
                    $styleCurrentPriceValue = $priceData['supplierPrice']['currencyCode'];

                    // apply selected values per-sku
                    foreach ($currentValues as $keyValue => $currentValue) {
                        $currentValueCodeCurrency[$keyValue] = $this->_productsMapper->getSelectedCurrencyBySKUGroup($keyValue);
                    }

                    break;
                case 'supplier-price':
                    /**
                     * Text Type
                     */
                    $label = 'Trading Currency Cost';
                    $currentValues = $this->_productsMapper->getSupplierPriceForOptions($allOptions);
                    $styleCurrentPriceValue = $priceData['supplierPrice']['price'];
                    break;
                case 'costPrice':
                case 'nowPrice':
                case 'wasPrice':
                    switch ($priceType) {
                        case 'costPrice':
                            $label = 'Landed Cost (' . $currency . ')';
                            $styleCurrentPriceValue = $priceData[$priceType][$currency];
                            break;
                        case 'nowPrice':
                            $label = 'Current Price (' . $currency . '-' . $territory . ')';
                            $styleCurrentPriceValue = $priceData[$priceType][$currency][$territory];
                            break;
                        case 'wasPrice':
                            $label = 'Original Price (' . $currency . '-' . $territory . ')';
                            $styleCurrentPriceValue = $priceData[$priceType][$currency][$territory];
                            break;
                    }
                    /**
                     * Text Type
                     */
                    $currentValues = $this->_productsMapper->getPriceForAllOptions($allOptions, $priceType, $territory, $currency);
                    break;
            }

            // Build Table view
            $optionsWithValues = new ViewModel();
            $optionsWithValues->setTerminal(true);
            $optionsWithValues->setTemplate('prism-products-manager/products/partials/product-creation-tabs/options-with-values-for-prices');
            $optionsWithValues->setVariable('currentValues', $currentValues);
            $optionsWithValues->setVariable('possibleValues', $possibleValues);
            $optionsWithValues->setVariable('stylePossibleValues', $stylePossibleValues);
            $optionsWithValues->setVariable('label', $label);
            $optionsWithValues->setVariable('type', $type);
            $optionsWithValues->setVariable('priceType', $priceType);
            $optionsWithValues->setVariable('currency', $currency);
            $optionsWithValues->setVariable('territory', $territory);
            $optionsWithValues->setVariable('style', $style);
            $optionsWithValues->setVariable('tab', $tab);
            $optionsWithValues->setVariable('styleCurrentPriceValue', $styleCurrentPriceValue);
            $optionsWithValues->setVariable('currentValueCodeCurrency', $currentValueCodeCurrency);

            $optionsWithValuesOutput = $this->getServiceLocator()
                ->get('viewrenderer')
                ->render($optionsWithValues);
            $optionsWithValuesOutput = utf8_encode($optionsWithValuesOutput);

            $response = array(
                'html' => $optionsWithValuesOutput
            );
            return $this->getResponse()->setContent(JSON::encode($response));
        }
    }

    /**
     * Filter only values allowed by parent field
     *
     * @param array $dynamicField
     * @param array $activeChain
     * @return mixed
     */
    protected function filterChainedVaules($dynamicField, $activeChain = [])
    {
        if(empty($activeChain)){
            return $dynamicField;
        }

        $i = 0;
        //filter only parent allowed values
        foreach ($dynamicField['values'] as $fieldKey => $fieldValue){
            if($i >0){
                if(!in_array($fieldKey, $activeChain)){
                    unset($dynamicField['values'][$fieldKey]);
                }
            }
            $i++;
        }
        return $dynamicField;
    }

    /**
     *
     * @return mixed
     * @throws \Exception
     */
    public function getInputValuesForAllOptionsAction()
    {
        $this->isCmsUserAuthorized(array('view'));

        // Retrieve the submitted data form.
        $style = $this->params()->fromPost('style', false);
        $inputFieldId = $this->params()->fromPost('inputFieldId', false);
        $tab = $this->params()->fromPost('tab', false);
        $activeChain = $this->params()->fromPost('chainAllowedValues', []);

        if ($style && $inputFieldId) {
            $allOptions = array();
            if (empty($option) && ! empty($style)) {
//                $allOptions = $this->_productsMapper->getAllOptionsForStyle($style, true);
                $allOptions = $this->getOptionsData($style);

            }

            $dynamicField = $this->inputService->getDynamicArrayFields($inputFieldId, $style);

            $dynamicField = $this->filterChainedVaules($dynamicField, $activeChain);

            // get the dynamicFields on option level
            $dynamicFields = $this->_productsMapper->formatArrayForAllOptions($dynamicField, $allOptions);
            $dynamicStyleLevelField = array_values($dynamicFields)[0]['label'];

            //get product id from style
            $productId = $this->_productsMapper->getProductIdFromStyle($style);

            //get field id
            $filedId = array_values($dynamicFields)[0]['entity']->getInputFieldId();
            $inputFieldEntity = $this->inputService->getInputField($filedId);

            //get common attribute id
            $commonAttributeId = $inputFieldEntity->getInputFieldRAttributesInputfieldrattributeid()->getCommonAttributeId();

            //get all available values for this common atribute
            $styleTypeArray = array_values($dynamicFields)[0]['all-values'];
            //get common attribute type
            $styleType = array_values($dynamicFields)[0]['type'];

            //get all values of common atribute for specific style
            $styleValueArray = $this->_productsMapper->getCommonAttributesFor($productId, "PRODUCTS", $commonAttributeId);

            $styleValue = [];

            //if common attribute is select or multiselect get values and ids for values
            //else just fetch one value
            if($styleType == "select" || $styleType == "multiselect") {
                foreach ($styleTypeArray as $valueId => $value) {
                    if(!empty($styleValueArray)) {
                        foreach (array_values($styleValueArray)[0] as $row) {
                            //chceck selected value
                            if ($row['commonAttributeValue'] == $valueId) {
                                //if have abbrCode
                                if (strpos($styleTypeArray[$valueId], " - " !== false)) {
                                    $styleValue[array_values($styleTypeArray)[2][$valueId] . " - " . $value . "|"] = $valueId;
                                } else {
                                    $styleValue[$value . "|"] = $valueId;
                                }
                                continue 2;
                            }
                        }
                    }

                    //if have abbrCode and the value si not selected
                    if(strpos($styleTypeArray[$valueId], " - " !== false)) {
                        $styleValue[array_values($styleTypeArray)[2][$valueId] . " - " . $value] = $valueId;
                    }
                    else {
                        $styleValue[$value] = $valueId;
                    }
                }
            }
            else {
                if(!empty($styleValueArray)){
                    $styleValue = array_values($styleValueArray)[0][0]['commonAttributeValue'];
                }
            }


            // Build Table view
            $optionsWithValues = new ViewModel();
            $optionsWithValues->setTerminal(true);
            $optionsWithValues->setTemplate('prism-products-manager/products/partials/product-creation-tabs/options-with-values');
            $optionsWithValues->setVariable('dynamicFields', $dynamicFields);
            $optionsWithValues->setVariable('label', $dynamicStyleLevelField);
            $optionsWithValues->setVariable('styleValue', $styleValue);
            $optionsWithValues->setVariable('styleType', $styleType);
            $optionsWithValues->setVariable('styleFiledId', $filedId);
            $optionsWithValues->setVariable('style', $style);
            $optionsWithValues->setVariable('tab', $tab);
            $optionsWithValuesOutput = $this->getServiceLocator()
                ->get('viewrenderer')
                ->render($optionsWithValues);
            //$optionsWithValuesOutput = utf8_encode($optionsWithValuesOutput);

            $response = array(
                'html' => $optionsWithValuesOutput
            );
            return $this->getResponse()->setContent(JSON::encode($response));
        }

        throw new \Exception('Style or inputFieldId are not set !');
    }

    public function getDynamicFieldsOnStylevel($style, $dynamicField)
    {
        return true;
        $this->_productsMapper->formatArrayForStyle($dynamicField, $style);
        return true;
    }

    /**
     *
     * @param
     *            $priceData
     */
    private function hydrateFormWithPriceData($priceData)
    {
        /**
         * Hydrate supplier price
         */
        $this->inputFieldsForm->get('prices[supplier-currency]')->setValue($priceData['supplierPrice']['currencyCode']);
        $this->inputFieldsForm->get('prices[supplier-price]')->setValue($priceData['supplierPrice']['price']);
        /**
         * Hydrate Cost Price
         */
        foreach ($priceData['costPrice'] as $currencyCode => $costPrice) {
            $this->inputFieldsForm->get('prices[costPrice-' . $currencyCode . ']')->setValue($costPrice);
        }
        /**
         * Hydrate Now Price
         */
        foreach ($priceData['nowPrice'] as $currencyCode => $territories) {
            foreach ($territories as $territory => $price)
                $this->inputFieldsForm->get('prices[nowPrice-' . $currencyCode . '-' . $territory . ']')->setValue($price);
        }
        /**
         * Hydrate Was Price
         */
        foreach ($priceData['wasPrice'] as $currencyCode => $territories) {
            foreach ($territories as $territory => $price)
                $this->inputFieldsForm->get('prices[wasPrice-' . $currencyCode . '-' . $territory . ']')->setValue($price);
        }
    }

    /**
     *
     * @param null $style
     * @param array $submitedFormData
     * @return bool
     */
    private function createNewOption($style = null, $submitedFormData = array())
    {
        if (! empty($style) && ! empty($submitedFormData)) {
            try {
                $productData = $this->_productsMapper->getProductDataForStyleOrOption($style);

                if (isset($productData['tableField']['productId'])) {

                    $supplierPrice = [];

                    $supplierDetails = $this->_productsMapper->getProductPricesForStyleOrOption($style, null);

                    // in case price was not added set $supplierPrice to empty array
                    if (isset($supplierDetails['supplierPrice']) && !empty($supplierDetails['supplierPrice'])) {

                        $supplierPrice = [
                            'currencyCode' => $supplierDetails['supplierPrice']['currencyCode'],
                            'price' => $supplierDetails['supplierPrice']['price']
                        ];
                    }


                    $option = $this->_productsMapper->createNewOptionForProduct($productData['tableField']['productId'], $style, $submitedFormData, $productData['prices'], $supplierPrice);

                    /**
                     * Add spectrum entry
                     */
                    $variants = $this->_productsMapper->getVariantsIdForOption($option['optionCode']);
                    foreach ($variants as $variant) {
                        $this->_queueService->addSpectrumEntryToQueueForCommonAttributes($variant['productAttributeId']);
                    }

                        $this->_queueService->addSipEntryToQueue($option['optionCode'], 5, $this->_productsMapper, true);
                        /**
                         * Add Sip Data for Product Prices
                         */
                        $this->_queueService->addSipEntryToQueue($option['optionCode'], 6, $this->_productsMapper, true);

                    $this->checkIfWorkflowIsCompleted($style, $option['optionCode']);

                    // rebuild cached options data
                    $this->updateCachedOptionsData($style);

                    return true;
                }
            } catch (\Exception $ex) {
                return false;
            }
        }
        return false;
    }

    /**
     *
     * @param    $option
     * @param    $inputFieldsElements
     */
    private function hydrateFormDataForOption($option, $inputFieldsElements)
    {
        /**
         * Retrieve data to hydrate form
         */
        $hydrateData = $this->_productsMapper->getProductDataForStyleOrOption($option, 'option');
        $this->hydrateFormElements($inputFieldsElements, $hydrateData);
    }

    /**
     *
     * @param     $style
     * @param     $inputFieldsElements
     */
    private function hydrateFormDataForStyle($style, $inputFieldsElements)
    {
        /**
         * Retrieve data to hydrate form
         */
        $hydrateData = $this->_productsMapper->getProductDataForStyleOrOption($style, 'style');

        $this->hydrateFormElements($inputFieldsElements, $hydrateData);
    }

    /**
     *
     * @param
     *            $inputFieldsElements
     * @param
     *            $hydrateData
     */
    private function hydrateFormElements($inputFieldsElements, $hydrateData)
    {
        /**
         * Populate form elements with values
         */
        foreach ($inputFieldsElements as $tabs) {
            foreach ($tabs as $inputFields) {
                foreach ($inputFields as $inputFieldId => $inputField) {

                    if ($inputFieldId != 'isModule') {
                        if (strpos($inputField['name'], 'button-') !== false) {
                            continue;
                        }
                        /**
                         * Check if input field is common attribute, normal attribute or table field
                         */
                        switch ($inputField['inputType']) {
                            case 'isAttribute':
                                /**
                                 * Input field is attribute ( common or normal )
                                 */

                                switch ($inputField['inputFieldId']->getinputFieldRAttributesInputFieldRAttributeId()->getAttributeTable()) {
                                    case 'COMMON_ATTRIBUTES':
                                        /**
                                         * Input Field is Common Attribute
                                         */
                                        $commonAttributeId = $inputField['inputFieldId']->getInputFieldRAttributesInputFieldRAttributeId()->getCommonAttributeId();
                                        /**
                                         * Check if value exists in the array and set the value in the form
                                         */
                                        if (isset($hydrateData['commonAttributes'][$commonAttributeId])) {
                                            $valueToSet = array();
                                            foreach ($hydrateData['commonAttributes'][$commonAttributeId] as $value) {
                                                $valueToSet[] = $value['commonAttributeValue'];
                                            }
                                            if (count($valueToSet) > 1) {
                                                $valueToSet = array_unique($valueToSet);
                                            }
                                            $viewType = $inputField['inputFieldId']->getInputFieldRAttributesInputFieldRAttributeId()
                                                ->getCommonAttributes()
                                                ->getCommonAttributeGuiRepresentation()
                                                ->getCommonAttributesViewTypeId()
                                                ->getCommonAttributeViewType();

                                            if (count($valueToSet) > 1 || ($viewType == 'PRODUCT' && !empty($valueToSet[0]))) {


                                                if ($viewType == 'PRODUCT') {
                                                    $options = array();
                                                    foreach ($valueToSet as $sku) {
                                                        $variantId = $this->_productsMapper->getVariantIdFromSku($sku);
                                                        if (!$variantId) {
                                                            continue;
                                                        }
                                                        $colour = $this->_productsMapper->getColourForVariantId($variantId);
                                                        $colourName = $colour['name'];
                                                        $size = $this->_productsMapper->getSizeUsedForVariantId($variantId);

                                                        $options[$variantId] = array('variantId' => $variantId,'sku' => $sku, 'colour' => $colourName, 'size' => $size);
                                                    }
                                                    $this->inputFieldsForm->get($inputField['name'])->setAttribute('data-options', json_encode($options));
                                                } else {
                                                    $this->inputFieldsForm->get($inputField['name'])->setValue($valueToSet);
                                                }

                                            } else {
                                                $this->inputFieldsForm->get($inputField['name'])->setValue($valueToSet[0]);

                                                // START added by Hani
                                                $formValueOption = $this->inputFieldsForm->get($inputField['name'])->getOptions();
                                                if ($this->isValueInformOptions($formValueOption, $value) == false) {
                                                    if ($this->inputFieldsForm->get($inputField['name']) instanceof \Zend\Form\Element\Select) {

                                                        $addedCommonAttributeVaule = null;
                                                        if (! empty($value['commonAttributeValue'])) {
                                                            $addedCommonAttributeVaule = $this->getCommonAttributeValueCodeByValueId($value['commonAttributeValue']);
                                                        }

                                                        if (null !== $addedCommonAttributeVaule) {
                                                            // get the missing value by
                                                            $newFormValueOption = $this->createNewFormOptionWithMissingAttribute($formValueOption, $value['commonAttributeValue'], $addedCommonAttributeVaule);
                                                            $this->inputFieldsForm->get($inputField['name'])->setValueOptions($newFormValueOption);
                                                        }
                                                    }
                                                }
                                                // END added by Hani
                                                $this->inputFieldsForm->get($inputField['name'])->setAttribute('data-already-saved', 1);
                                            }
                                        }
                                        break;
                                    case 'ATTRIBUTES_Groups':
                                        /**
                                         * Input Field is Normal Attribute
                                         */
                                        $attributeGroupId = $inputField['inputFieldId']->getInputFieldRAttributesInputFieldRAttributeId()
                                            ->getAttributeGroupId()
                                            ->getAttributeGroupId();
                                        /**
                                         * Check if value exists in the array and set the value in the form
                                         */
                                        if (isset($hydrateData['attributes'][$attributeGroupId])) {
                                            $valueToSet = $hydrateData['attributes'][$attributeGroupId]['attributeValue'];
                                            $this->inputFieldsForm->get($inputField['name'])->setValue($valueToSet);
                                            $this->inputFieldsForm->get($inputField['name'])->setAttribute('data-already-saved', 1);
                                        }
                                        break;
                                }
                                break;
                            case 'isTableField':
                                /**
                                 * Input field is table field
                                 */
                                $fieldName = $inputField['inputFieldId']->getinputTablefieldsInputtablefieldid()->getcolumn();

                                if (isset($hydrateData['tableField'][$fieldName])) {
                                    $this->inputFieldsForm->get($inputField['name'])->setValue($hydrateData['tableField'][$fieldName]);
                                }
                                break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Get the common attribute value for $this->inputFieldsForm->get($inputField['name'])
     *
     * @param unknown $commonAttributeValueId
     */
    public function getCommonAttributeValueCodeByValueId($commonAttributeValueId)
    {
        $value = null;

        $value = $this->commonAttributesService->getCommonAttributeValueByCommonAttributeId($commonAttributeValueId);

        if ($value) {
            if (! empty($value['commonAttributeValueAbbrev'])) {
                return $value['commonAttributeValueAbbrev'] . " - " . $value['value'];
            }

            return $value['value'];
        }

        return $value;
    }

    /**
     * This hsould be sued to update the from slection
     * option with common attributes that was previously set and later was disabled
     *
     * @param Array $formOptions
     * @param Array $value
     */
    protected function isValueInformOptions($formOptions = null, $value)
    {
        $isInFrom = false;

        if ($formOptions == null) {
            return true;
        }

        if (isset($formOptions['value_options'])) {
            if (array_key_exists($value['commonAttributeValue'], $formOptions['value_options'])) {
                $isInFrom = true;
            }
        }
        return $isInFrom;
    }

    /**
     * add the missing key value into value_options
     *
     * @param unknown $formValueOption
     * @param unknown $commonAttributeId
     * @param unknown $commonAttributeVaule
     */
    protected function createNewFormOptionWithMissingAttribute($formValueOption, $commonAttributeId, $commonAttributeVaule)
    {
        if (isset($formValueOption['value_options'])) {
            if (is_array($formValueOption['value_options'])) {
                $formValueOption['value_options'][$commonAttributeId] = $commonAttributeVaule;
                return $formValueOption['value_options'];
            }
        }
    }

    /**
     *
     * @return \Zend\Stdlib\Message
     */
    public function getAllProductAddOnsByVariantAction()
    {
        $this->isCmsUserAuthorized(array(
            'view'
        ));
        // Get the request
        $request = $this->getRequest();
        // Retrieve the submitted data form.
        $submitedFormData = $this->params()->fromPost();

        if (isset($submitedFormData)) {
            $productAddOns = $this->_productsMapper->getProductAddOnsByVariantId($submitedFormData['variantId']);
        }

        // Build Table view
        $productAddOnView = new ViewModel();
        $productAddOnView->setTerminal(true);
        $productAddOnView->setTemplate('prism-products-manager/products/partials/add-edit/popover-partials/product-add-ons-table');
        $productAddOnView->setVariable('productAddOns', $productAddOns);
        $productAddOnViewOutput = $this->getServiceLocator()
            ->get('viewrenderer')
            ->render($productAddOnView);
        $productAddOnView = utf8_encode($productAddOnViewOutput);

        $response = array(
            'productAddOns' => $productAddOnView
        );
        return $this->getResponse()->setContent(JSON::encode($response));
    }

    /**
     *
     * @return \Zend\Stdlib\Message
     */
    public function removeProductAddOnAction()
    {
        $this->isCmsUserAuthorized(array(
            'view',
            'edit'
        ));
        // Retrieve the submitted data form.
        $submitedFormData = $this->params()->fromPost();
        $addOnId = $submitedFormData['addOnId'];

        $response = $this->_productsMapper->removeAddOnProduct($addOnId);

        if ($response == true) {
            $response = array(
                'result' => 'success'
            );
        } else {
            $response = array(
                'result' => 'error'
            );
        }

        return $this->getResponse()->setContent(JSON::encode($response));
    }

    /**
     *
     * @return mixed
     */
    public function getAllSavedVariantsAction()
    {

        // Get required information
        $productId = $this->params('id');
        $allSavedVariants = $this->_productsMapper->getVariants($productId);
        $productVariantCreationForm = $this->_productVariantCreationForm;
        $productVariantCreationForm->get('productId')->setValue($productId);
        if (isset($productDetails[$productId][$this->_firstPartOfSku])) {
            $productVariantCreationForm->get('firstPartOfSku')->setValue($productDetails[$productId][$this->_firstPartOfSku]);
        }
        // Override the variant counter
        if (isset($allSavedVariants['dataCount']) && $allSavedVariants['dataCount'] > 0) {
            $productVariantCreationForm->get('productVariantCount')->setValue($allSavedVariants['dataCount']);
        }

        // Build Table view
        $variantsTableView = new ViewModel();
        $variantsTableView->setTerminal(true);
        $variantsTableView->setTemplate('prism-products-manager/products/partials/add-edit/partial-variant-table');
        $variantsTableView->setVariable('allSavedVariants', $allSavedVariants);
        $variantsTableView->setVariable('productId', $productId);
        $variantsTableViewOutput = $this->getServiceLocator()
            ->get('viewrenderer')
            ->render($variantsTableView);
        $variantsTableViewOutput = utf8_encode($variantsTableViewOutput);

        // Build Sku Rules
        $skuRule1 = array();
        $skuRule2 = array();
        if (isset($variants['skuRule1']) && $variants['skuRule2']) {
            foreach ($allSavedVariants['data'] as $variants) {
                if (! array_key_exists($variants['skuRule1'], $skuRule1)) {
                    $skuRule1[$variants['skuRule1']] = array(
                        'value' => "^(" . $variants['skuRule1'] . ")",
                        'label' => $variants['skuRule1']
                    );
                    // $skuRule1[$variants['skuRule1']] = '{ "value": "^('.$variants['skuRule1'].')", "label": "'.$variants['skuRule1'].'"}';
                }
                if (! array_key_exists($variants['skuRule2'], $skuRule2)) {
                    $skuRule2[$variants['skuRule2']] = array(
                        'value' => "^(" . $variants['skuRule2'] . ")",
                        'label' => $variants['skuRule2']
                    );
                }
            }
        }

        $response = array(
            'tableElement' => $variantsTableViewOutput,
            'skuRule1' => $skuRule1,
            'skuRule2' => $skuRule2
        );
        return $this->getResponse()->setContent(JSON::encode($response));
    }

    /**
     *
     * @return JsonModel
     */
    public function deleteProductTabAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        // Get the request
        $request = $this->getRequest();

        // Retrieve the submitted data form.
        $productId = $this->params()->fromPost('productId', false);
        $tabId = $this->params()->fromPost('tabId', false);
        if ($productId && $tabId) {
            $this->_productsMapper->deleteProductTab($productId, $tabId);

            if (! $this->isManualSync()) {
                $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS);
            }
            return new JsonModel(array(
                'success' => true
            ));
        }
    }

    /**
     *
     * @return JsonModel
     */
    public function editProductTabAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        // Get the request
        $request = $this->getRequest();

        // Retrieve the submitted data form.
        $productId = $this->params()->fromPost('productId', false);
        $tabId = $this->params()->fromPost('tabId', false);
        if ($productId && $tabId) {
            $tab = $this->_productsMapper->getProductTab($productId, $tabId);
            return new JsonModel($tab);
        }
    }

    /**
     * for updating the product
     *
     * @return \Zend\View\Model\ViewModel
     */
    public function updateAction()
    {
        return new ViewModel();
    }

    /**
     * use for bulk action on variants
     */
    public function bulkAction()
    {
        $request = $this->getRequest();

        $action = $this->params('task');
        $productId = $this->params('id');
        $variants = $this->params('variants');

        $result = array(
            'success' => false
        );
        if ($action == 'reset-marketing-categories') {
            $this->_resetVariantsMarketingCategories($productId, $variants);
            $result = array(
                'success' => true
            );

            $navigationDataUpdate = array(
                'source' => 'product',
                'title' => '',
                'href' => '',
                'id' => $productId,
                'action' => 'suspend'
            );

            // queue the products for reindexing
            if (! $this->isManualSync()) {
                $productDetails = $this->_productsMapper->getProductDetailsById($productId, 1);
                $productVariants = $this->_productsMapper->getVariants($productId);
                $productCategories = $this->_productsMapper->getProductCategoriesByProductId($productId);
                $this->_queueService->addSpectrumEntryToQueue($productId, $productDetails, $productVariants, $productCategories);
                $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS, $navigationDataUpdate);
            }
        }
        return new JsonModel($result);
    }

    /**
     * Generic function to handle ajax request in post and route to the correct function.
     * Also send response to the front end.
     *
     * @throws \Exception
     * @return type
     */
    public function ajaxGetAction($params)
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        // Get the request
        $request = $this->getRequest();
        $ar = array(
            'success' => $request
        );
        return new JsonModel($ar);
    }

    /**
     * Generic function to handle ajax request and route to the correct function.
     * Also send response to the front end.
     *
     * @throws \Exception
     * @return type
     */
    public function ajaxAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        // Get the request
        $request = $this->getRequest();

        // Retrieve the submitted data form.
        $params = $this->params()->fromPost();

        if ($request->isPost()) {
            // Ajax request to get merchant category
            if (isset($params['getMerchantCategories']) && $params['getMerchantCategories'] > 0) {
                $result = $this->_ajaxGetMerchantCategories($params);
            }
            // Ajax request to get merchant category
            if (isset($params['getMarketingCategories']) && $params['getMarketingCategories'] > 0) {
                $result = $this->_ajaxGetMarketingCategories($params);
            }
            // Ajax request to get sku building rules
            if (isset($params['getSkuBuildingAttributes']) && $params['getSkuBuildingAttributes'] > 0) {
                $result = $this->_ajaxGetSkuBuildingAttributes($params);
            }
            // Ajax request to get sku building rules
            if (isset($params['getVariants']) && $params['getVariants'] > 0) {
                $result = $this->_ajaxGetVariants($params);
            }
            // Ajax request to get attribute values
            if (isset($params['getAttributeValues']) && $params['getAttributeValues'] > 0) {
                $result = $this->_ajaxGetAttributeValues($params);
            }
            // Ajax request to save variants
            if (isset($params['saveVariants']) && $params['saveVariants'] > 0) {
                $result = $this->_ajaxSaveVariants($params);
            }
            // Ajax request to search for variants
            if (isset($params['searchProduct']) && $params['searchProduct'] > 0) {
                $result = $this->_ajaxSearchProduct($params);
            }
            // Ajax request to save related products
            if (isset($params['saveRelatedProducts']) && $params['saveRelatedProducts'] > 0) {
                $result = $this->_ajaxSaveRelatedProducts($params);
            }
            // Ajax request to update variant barcode
            if (isset($params['updateVariantBarcode']) && $params['updateVariantBarcode'] > 0) {
                throw new \Exception('This action is invalid!');
                $result = $this->_ajaxUpdateVariantBarcode($params);
            }

            // Ajax request to save variants
            if (isset($params['deleteSelectedProducts']) && $params['deleteSelectedProducts'] > 0) {
                $result = $this->_ajaxDeleteSelectedProducts($params);
            }
            $response = $this->getResponse();
            // $response->setStatusCode(200);
            $response->setContent(json_encode($result));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }
    }

    // search products
    /**
     *
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function searchProductsAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        $request = $this->getRequest();

        $searchTerm = $this->params()->fromQuery('query', false);
        $searchResult['items'] = $this->_productsMapper->searchByProduct($searchTerm);

        if (isset($searchResult['items']) && ! empty($searchResult['items'])) {
            foreach ($searchResult['items'] as $result) {
                $searchResult['suggestions'][] = array(
                    "value" => $result['sku'],
                    'data' => $result['sku']
                );
            }
        } else {
            $searchResult['suggestions'][] = array(
                "value" => '',
                "data" => "No data found"
            );
        }

        $response = $this->getResponse();
        // $response->setStatusCode(200);
        $response->setContent(json_encode($searchResult));
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Enable a batch of products
     *
     * @param type $data
     * @return type
     */
    protected function _bulkEnableProducts($data)
    {
        if (trim($data['productCodes']) != '') {
            $productCodesArray = explode("\r\n", $data['productCodes']);
            if (count($productCodesArray) == 0) {
                $productCodesArray = explode(",", $data['productCodes']);
            }
            $returnArray = array();
            if (count($productCodesArray) > 0) {
                $activateVariant = true;
                $status = 'ACTIVE';
                // Get the parents ProductId and then activate the parent product
                $this->_productsMapper->activateParentProductByVariantSku($productCodesArray);
                $bulkUpdatedProducts = $this->_productsMapper->updateProductStatusByName($productCodesArray, $status, $activateVariant, false);
                if (isset($bulkUpdatedProducts['productUpdatedArray']) && count($bulkUpdatedProducts['productUpdatedArray']) > 0) {
                    foreach ($bulkUpdatedProducts['productUpdatedArray'] as $skuName => $productId) {
                        // Update elastic search and Spectrum
                        // Commented out for now because of a bug
                        if (! $this->isManualSync()) {
                            $navigationDataUpdate = array(
                                'source' => 'product',
                                'title' => '',
                                'href' => '',
                                'id' => $productId,
                                'action' => 'activate'
                            );
                            $productDetails = $this->_productsMapper->getProductDetailsById($productId, 1);
                            $productVariants = $this->_productsMapper->getVariants($productId);
                            $productCategories = $this->_productsMapper->getProductCategoriesByProductId($productId);
                            $this->_queueService->addSpectrumEntryToQueue($productId, $productDetails, $productVariants, $productCategories);
                            $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS, $navigationDataUpdate);
                        }

                        $returnArray['productUpdatedArray'][] = $skuName;
                    }
                }
                $returnArray['productNotUpdatedArray'] = $bulkUpdatedProducts['productNotUpdatedArray'];
                return $returnArray;
            }
        }

        return array();
    }

    /**
     * Disable a batch of products
     *
     * @param type $data
     * @return type
     */
    protected function _bulkDisableProducts($data)
    {
        if (trim($data['productCodes']) != '') {
            $productCodesArray = explode("\r\n", $data['productCodes']);
            if (count($productCodesArray) == 0) {
                $productCodesArray = explode(",", $data['productCodes']);
            }
            $returnArray = array();
            $bulkUpdatedProducts = array();
            if (count($productCodesArray) > 0) {
                $activateVariant = true;
                $status = 'INACTIVE';
                $bulkUpdatedProducts = $this->_productsMapper->updateProductStatusByName($productCodesArray, $status, $activateVariant, false);
                if (isset($bulkUpdatedProducts['productUpdatedArray']) && count($bulkUpdatedProducts['productUpdatedArray']) > 0) {
                    foreach ($bulkUpdatedProducts['productUpdatedArray'] as $skuName => $productId) {
                        // Update elstic search and Spectrum
                        // Commented out for now because of a bug
                        if (! $this->isManualSync()) {
                            $navigationDataUpdate = array(
                                'source' => 'product',
                                'title' => '',
                                'href' => '',
                                'id' => $productId,
                                'action' => 'suspend'
                            );
                            $productDetails = $this->_productsMapper->getProductDetailsById($productId, 1);
                            $productVariants = $this->_productsMapper->getVariants($productId);
                            $productCategories = $this->_productsMapper->getProductCategoriesByProductId($productId);
                            $this->_queueService->addSpectrumEntryToQueue($productId, $productDetails, $productVariants, $productCategories);
                            $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS, $navigationDataUpdate);
                        }
                        $returnArray['productUpdatedArray'][] = $skuName;
                    }
                }
                $returnArray['productNotUpdatedArray'] = $bulkUpdatedProducts['productNotUpdatedArray'];
                return $returnArray;
            }
        }

        return array();
    }

    /**
     * Delete a batch of products
     *
     * @param type $data
     * @return type
     */
    protected function _bulkDeleteProducts($data)
    {
        if (trim($data['productCodes']) != '') {
            $productCodesArray = explode("\r\n", $data['productCodes']);
            if (count($productCodesArray) == 0) {
                $productCodesArray = explode(",", $data['productCodes']);
            }
            $returnArray = array();
            $bulkUpdatedProducts = array();
            if (count($productCodesArray) > 0) {
                $activateVariant = true;
                $status = 'DELETED';
                $bulkUpdatedProducts = $this->_productsMapper->updateProductStatusByName($productCodesArray, $status, $activateVariant);
                if (isset($bulkUpdatedProducts['productUpdatedArray']) && count($bulkUpdatedProducts['productUpdatedArray']) > 0) {
                    foreach ($bulkUpdatedProducts['productUpdatedArray'] as $skuName => $productId) {
                        // Update elstic search and Spectrum
                        // Commented out for now because of a bug
                        if (! $this->isManualSync()) {
                            $navigationDataUpdate = array(
                                'source' => 'product',
                                'title' => '',
                                'href' => '',
                                'id' => $productId,
                                'action' => 'suspend'
                            );
                            $productDetails = $this->_productsMapper->getProductDetailsById($productId, 1);
                            $productVariants = $this->_productsMapper->getVariants($productId);
                            $productCategories = $this->_productsMapper->getProductCategoriesByProductId($productId);
                            $this->_queueService->addSpectrumEntryToQueue($productId, $productDetails, $productVariants, $productCategories);
                            $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS, $navigationDataUpdate);
                        }

                        $returnArray['productUpdatedArray'][] = $skuName;
                    }
                }
                $returnArray['productNotUpdatedArray'] = $bulkUpdatedProducts['productNotUpdatedArray'];
                return $returnArray;
            }
        }

        return array();
    }

    /**
     *
     * @return mixed
     * @throws \Exception
     */
    public function getVariantAttributesAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        $languageId = $this->_currentLanguage;
        $request = $this->getRequest();

        // if ($request->isXmlHttpRequest()) {

        $productId = $this->params('id', false);
        $variantId = $this->params('variantId', false);
        $variantIds = explode('-', $variantId);

        $viewOnly = $this->params('viewOnly', false);
        sort($variantIds);
        $unmatchedElements = false;

        if (count($variantIds) > 0) {

            // if we are trying to edit multiple variants at once we must check if the selected variants

            $fullAttributesArray = array();

            foreach ($variantIds as $vId) {
                $allAttributesArray = $this->_productsMapper->getAllAttributesAssignedToProduct($vId, 1, $languageId);
                $skuList = $this->_productsMapper->getVariantsSkus($vId);
                foreach ($allAttributesArray as $key => $value) {
                    $fullAttributesArray[] = array(
                        $key => array(
                            'value' => $value,
                            'variantId' => $vId,
                            'sku' => $skuList[0]
                        )
                    );
                }
            }
            $allAttributesArray = $fullAttributesArray;
        } else {
            $allAttributesArray = $this->_productsMapper->getAllAttributesAssignedToProduct($productId, 1, $languageId);
        }

        $productAttributesSelectForm = $this->_productAttributeSelectForm;
        // get generated variant attributes
        $dynamicallyGeneratedAttributes = $productAttributesSelectForm->getDynamicallyGeneratedAttributes();
        $url = '/products/get-variant-attributes/';
        if (! empty($productId)) {
            $url = $url . $productId;
        }
        if (! empty($variantId)) {
            $url = $url . '/' . $variantId;
        }
        // override form action (i.e url)
        $productAttributesSelectForm->setAttribute('action', $url);
        if ($request->isPost()) {
            $submitedFormData = $this->params()->fromPost();
            // we must save the merchant attributes for this variant
            if (count($variantIds) > 0) {
                // we must save the attributtes for each variant
                foreach ($variantIds as $variantId) {
                    $submitedFormData['variantId'] = $variantId;
                    $this->_saveProductAttributes($productAttributesSelectForm, $submitedFormData, true);
                }
                $productAttributesSelectForm->get('variantId')->setValue(implode('-', $variantIds));
            } else {
                $this->_saveProductAttributes($productAttributesSelectForm, $submitedFormData, true);
            }
            $productDetails = $this->_productsMapper->getMinimumProductsDetails($productId);
            if ($productDetails['status'] == 'ACTIVE') {

                if (! $this->isManualSync()) {
                    // update the product in Spectrum
                    $productDetails = $this->_productsMapper->getProductDetailsById($productId, 1);
                    $productVariants = $this->_productsMapper->getVariants($productId);
                    $productCategories = $this->_productsMapper->getProductCategoriesByProductId($productId);
                    $this->_queueService->addSpectrumEntryToQueue($productId, $productDetails, $productVariants, $productCategories);

                    // update the product in Elastic Search
                    $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS);
                }
            }
            return true;
        }
        // Set the product attributes that have been selected.
        if (count($variantIds) > 0) {
            // we must set the value of the variant Id field to multiple variant ids
            $productAttributesSelectForm->get('variantId')->setValue(implode('-', $variantIds));
        }

        if (is_array($allAttributesArray) && count($allAttributesArray) > 0) {
            $tempArray = array();
            $leftOverArray = array();
            // Set form input values
            foreach ($allAttributesArray as $allAttributes) {

                foreach ($allAttributes as $attributeKey => $attributeValue) {
                    if (in_array($attributeKey, $tempArray)) {
                        // if in temp array, this type input is already set, we will push it to the left over array
                        $leftOverArray[$attributeKey][] = array(
                            'AttributeKey' => $attributeKey,
                            'variantID' => $attributeValue['variantId'],
                            'value' => $attributeValue['value'],
                            'sku' => $attributeValue['sku']
                        );
                    } else {
                        // Otherwise set the value and push to tempArray
                        array_push($tempArray, $attributeKey);
                        if ($productAttributesSelectForm->has($attributeKey)) {
                            $productAttributesSelectForm->get($attributeKey)->setValue($attributeValue['value']);
                        }
                    }
                }
            }
            // Create & render pop over elements, inject them into the form element
            $popoverArray = $leftOverArray;
            foreach ($popoverArray as $key => $tmp) {
                // Create & render view
                $popover = false;
                if ($productAttributesSelectForm->has($key)) {
                    $inputValue = $productAttributesSelectForm->get($key)->getValue();

                    // decide if popover is required
                    foreach ($tmp as $val) {
                        if ($val['value'] != $inputValue) {
                            $popover = true;
                        }
                    }
                }

                // add popover
                if ($popover === true) {

                    $popOverView = new ViewModel();
                    $popOverView->setTerminal(true);
                    $popOverView->setTemplate('prism-products-manager/products/partials/add-edit/popover-partials/bulk-attribute-popover');
                    $popOverView->setVariable('attributeDetails', array(
                        $key => $tmp
                    ));

                    $popOverElement = $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($popOverView);
                    $currentClass = $productAttributesSelectForm->get($key)->getAttribute('class');
                    $currentClass .= ' multiple-variant-attributes ';
                    $productAttributesSelectForm->get($key)->setAttribute('class', $currentClass);

                    $inputType = $productAttributesSelectForm->get($key)->getAttribute('type');

                    // if input check box, we will add different style to any toher input type
                    if ($inputType == 'checkbox') {
                        $productAttributesSelectForm->get($key)->setLabelAttributes(array(
                            'class' => 'multiple-variant-attributes-checkbox'
                        ));
                        $productAttributesSelectForm->get($key)->setAttributes(array(
                            'data-toggle' => 'popover',
                            'data-placement' => 'bottom',
                            'data-html' => 'true',
                            'data-content' => $popOverElement
                        ));
                    } else {
                        // any other input type
                        $productAttributesSelectForm->get($key)->setLabelAttributes(array(
                            'data-toggle' => 'popover',
                            'data-placement' => 'bottom',
                            'data-html' => 'true',
                            'data-content' => $popOverElement
                        ));
                    }

                    // Set unmatched elements true
                    $unmatchedElements = true;
                }
            }
        } else {
            // the variant doesn't have any attributes set yet
            $error = array(
                'result' => 'error',
                'msg' => 'Something went wrong and the variant(s) attributes are not set!'
            );
            return $this->getResponse()->setContent(JSON::encode($error));
        }

        $variantAttributesModalView = new ViewModel();
        $variantAttributesModalView->setTerminal(true);
        if ($viewOnly > 0) {
            $variantAttributesModalView->setTemplate('prism-products-manager/products/partials/view/partial-mandatory-attributes');
            $productAttributes = array(
                'attributes' => $allAttributesArray
            );
            $variantAttributesModalView->setVariable('productDetails', $productAttributes);
        } else {
            $variantAttributesModalView->setTemplate('prism-products-manager/products/partials/add-edit/partial-mandatory-attributes');
        }
        $variantAttributesModalView->setVariable('productAttributesSelectForm', $productAttributesSelectForm);
        $variantAttributesModalView->setVariable('dynamicallyGeneratedAttributes', $dynamicallyGeneratedAttributes);

        $variantAttributesModalOutput = $this->getServiceLocator()
            ->get('viewrenderer')
            ->render($variantAttributesModalView);
        $variantAttributesModalOutput = utf8_encode($variantAttributesModalOutput);

        $responseArray = array(
            'success' => true,
            'variantAttributesForm' => $variantAttributesModalOutput,
            'unmatchedElements' => $unmatchedElements,
            'warning' => "<span class='danger'> Unmatched elements found. If you change/add any of the values bellow, the changes will be applied to all this product variants. Please select marked labels to see changes.</span>"
        );

        // } else {
        // throw new \Exception('Invalid request type');
        // }
        return $this->getResponse()->setContent(JSON::encode($responseArray));
    }






    /**
     * Get which tab we should set as active.
     *
     * @param
     *            $stage
     * @return string
     */
    protected function _getActiveTab($stage = 1)
    {
        switch ($stage) {
            case 2:
                $productActiveTab = 'merchant-category';
                break;
            case 3:
                $productActiveTab = 'marketing-category';
                break;
            case 4:
                $productActiveTab = 'mandatory-attribute';
                break;
            case 5:
                $productActiveTab = 'variant-creation';
                break;
            case 6:
                $productActiveTab = 'related-products';
                break;
            case 7:
                $productActiveTab = 'product-add-on';
                break;
            case 8:
                $productActiveTab = 'product-tabs';
                break;
            default:
                $productActiveTab = 'basic-information';
        }

        return $productActiveTab;
    }

    /**
     * This function needs to move to a service
     *
     * Redirect in case of error
     *
     * @param
     *            $route
     * @param
     *            $type
     * @param
     *            $message
     *
     * @param string $alert
     * @return null
     */
    protected function _errorValue($route, $type, $message, $alert = [])
    {
        $this->showFlashMessenger($type, $message, $alert);
        if (isset($route['exactMatch']) && $route['exactMatch']) {
            return $this->redirect()->toRoute($route['exactMatch']);
        } else {
            return $this->redirect()->toRoute(null, $route);
        }
    }

    /**
     *
     * @param
     *            $type
     * @param
     *            $message
     * @param string $alert
     */
    protected function showFlashMessenger($normalMessager = true, $type, $message, $alert = [])
    {
        $alert['type'] = $type;
        $alert['bold'] = ucfirst($type) . ':';
        $alert['messages'] = array();
        $alert['messages'][0] = $message;
        if ($normalMessager) {
            $this->flashmessenger()
                ->setNamespace('alert')
                ->addMessage($alert);
        } else {
            $this->errorType = $type;
            $this->errorMessage = $message;
        }
    }
    /**
     *
     * @param
     *            $productId
     */
    protected function _getProductByID($productId)
    {}



    /**
     * We only want to move to the manage product screen
     */
    protected function _moveToManageProduct()
    {
        $statusMessage = 'The product was created successfully';
        $status = 'success';
        $route = array(
            'controller' => 'Products',
            'action' => 'index'
        );
        $this->_errorValue($route, $status, $statusMessage);
    }

    /**
     * We only want to move to the manage product screen
     */
    protected function _moveToNextStage($productId, $nextStage, $message, $status = '')
    {
        $statusMessage = '';
        if (trim($message) != '') {
            $statusMessage = $message;
            if ($status == '') {
                $status = 'success';
            }
        }
        $route = array(
            'controller' => 'Products',
            'action' => 'edit',
            'id' => $productId,
            'stage' => $nextStage
        );
        $this->_errorValue($route, $status, $statusMessage);
    }

    /**
     * Activate a product
     *
     * @param
     *            $productsActivationForm
     * @param
     *            $submitedFormData
     * @param
     *            $productId
     * @param
     *            $stage
     * @param
     *            $action
     */
    protected function _activateProduct($productsActivationForm, $submitedFormData, $productId, $stage, $action = 'add')
    {
        $productsActivationForm->setData($submitedFormData);
        if ($productsActivationForm->isValid()) {
            $data = $productsActivationForm->getData();
            if (! $this->_productsMapper->checkIfAnyVoucherProductIsActive($submitedFormData['productId'])) {
                $productActivated = $this->_productsMapper->activateProduct($data);
                if ($this->_productsMapper->checkIfProductIsVoucher($submitedFormData['productId'])) {
                    $this->_clearEvoucherCache();
                }
            } elseif (! $this->_productsMapper->checkIfProductIsVoucher($submitedFormData['productId'])) {
                $productActivated = $this->_productsMapper->activateProduct($data);
            }
            $route = array(
                'controller' => 'Products',
                'action' => $action,
                'id' => $productId,
                'stage' => $stage
            );
            $statusMessage = "Product activation failed. Please try again";
            if ($this->_productsMapper->checkIfProductIsVoucher($submitedFormData['productId']) && $this->_productsMapper->checkIfAnyVoucherProductIsActive($submitedFormData['productId'])) {
                $statusMessage = 'You can have only one evoucher product active. An evoucher product is already active.';
            }
            $status = 'error';
            if (isset($productActivated['errorMessage']) && trim($productActivated['errorMessage']) != '') {
                $statusMessage .= $productActivated['errorMessage'];
            }
            $evoucherType = $this->_productsMapper->getVoucherTypeId($this->_currentLanguage);

            if (isset($productActivated['success']) && $productActivated['success'] === true) {
                // check here if we already have a spectrum Id
                // Skip activating evoucher products in Spectrum till the API to insert them in Spectrum is fixed
                if (isset($productActivated['productTypeId'])) {
                    $productDetails = $this->_productsMapper->getProductDetailsById($productId, 1);
                    $productVariants = $this->_productsMapper->getVariants($productId);
                    $productCategories = $this->_productsMapper->getProductCategoriesByProductId($productId);
                    $this->_queueService->addSpectrumEntryToQueue($productId, $productDetails, $productVariants, $productCategories);
                }
                $navigationDataUpdate = array(
                    'source' => 'product',
                    'title' => '',
                    'href' => '',
                    'id' => $productId,
                    'action' => 'activate'
                );
                if ($productActivated['productTypeId'] != $evoucherType['productTypeId']) {
                    if (! $this->isManualSync()) {
                        $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS, $navigationDataUpdate);
                    }
                }
                $statusMessage = 'Product was activated successfully.';
                $status = 'success';
            }
            $this->_errorValue($route, $status, $statusMessage);
        }
    }

    /**
     * Deactivate a product
     *
     * @param
     *            $productsActivationForm
     * @param
     *            $submitedFormData
     * @param
     *            $productId
     * @param
     *            $stage
     * @param
     *            $action
     * @return s void
     */
    protected function _deactivateProduct($productsActivationForm, $submitedFormData, $productId, $stage, $action = 'add')
    {
        $productsActivationForm->setData($submitedFormData);
        if ($productsActivationForm->isValid()) {
            $data = $productsActivationForm->getData();
            $productDeactivated = $this->_productsMapper->deactivateProduct($data);
            $route = array(
                'controller' => 'Products',
                'action' => $action,
                'id' => $productId,
                'stage' => $stage
            );
            $statusMessage = "Product deactivation failed. Please try again";
            $status = 'error';
            if ($productDeactivated['success'] === true) {
                if (isset($productDeactivated['productTypeId'])) {
                    $productDetails = $this->_productsMapper->getProductDetailsById($productId, 1);
                    $productVariants = $this->_productsMapper->getVariants($productId);
                    $productCategories = $this->_productsMapper->getProductCategoriesByProductId($productId);
                    $this->_queueService->addSpectrumEntryToQueue($productId, $productDetails, $productVariants, $productCategories);
                }
                if ($this->_productsMapper->checkIfProductIsVoucher($submitedFormData['productId'])) {
                    $this->_clearEvoucherCache();
                }
                $navigationDataUpdate = array(
                    'source' => 'product',
                    'title' => '',
                    'href' => '',
                    'id' => $productId,
                    'action' => 'suspend'
                );
                if (! $this->isManualSync()) {
                    $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS, $navigationDataUpdate);
                }
                $statusMessage = 'Product was deactivated successfully.';
                $status = 'success';
            }
            $this->_errorValue($route, $status, $statusMessage);
        }
    }



    /**
     * Deactivate a product marketing category
     *
     * @param
     *            $categoryId
     * @param
     *            $productId
     * @param
     *            $stage
     * @param
     *            $action
     */
    protected function _deactivateMarketingCategory($submitedFormData, $productId, $stage, $action = 'add')
    {
        $route = array(
            'controller' => 'Products',
            'action' => $action,
            'id' => $productId,
            'stage' => $stage
        );
        $statusMessage = "Marketing category deactivation failed. Please try again";
        $status = 'error';

        $categoryId = isset($submitedFormData['categoryId']) ? $submitedFormData['categoryId'] : 0;
        $variantId = isset($submitedFormData['variantId']) ? $submitedFormData['variantId'] : 0;

        $isVariant = false;
        if ($variantId > 0) {
            $productId = $variantId;
            $route['stage'] = 5;
            $route['variantId'] = $variantId;
            $route['showcategorypopup'] = 1;
            $isVariant = true;
        }

        if (is_numeric($categoryId) && is_numeric($productId)) {
            $productCategoryDeactivated = $this->_productsMapper->deleteMarketingCategory($productId, $categoryId, $isVariant);
            if ($productCategoryDeactivated > 0) {
                $statusMessage = 'Marketing category was deactivated successfully.';
                $status = 'success';
            }
            $productDetails = $this->_productsMapper->getMinimumProductsDetails($productId);
            if ($productDetails['status'] == 'ACTIVE') {
                if (! $this->isManualSync()) {
                    // update the product in Elastic Search
                    $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS);
                }
            }
        } else {
            $statusMessage = 'Product id or category id is not numeric';
        }

        $this->_errorValue($route, $status, $statusMessage);
    }

    /**
     * Deactivate a product marketing category
     *
     * @param
     *            $categoryId
     * @param
     *            $productId
     * @param
     *            $stage
     * @param
     *            $action
     */
    protected function _setAsDefaultCategory($submitedFormData, $productId, $stage, $action = 'add')
    {
        $route = array(
            'controller' => 'Products',
            'action' => $action,
            'id' => $productId,
            'stage' => $stage
        );
        $categoryId = $submitedFormData['categoryId'];
        $variantId = isset($submitedFormData['variantId']) ? $submitedFormData['variantId'] : 0;
        $applyToVariants = isset($submitedFormData['apply-to-variants']) ? $submitedFormData['apply-to-variants'] : 0;
        $isVariant = false;
        if ($variantId > 0) {
            $productId = $variantId;
            $route['stage'] = 5;
            $route['variantId'] = $variantId;
            $route['showcategorypopup'] = 1;
            $isVariant = true;
        }
        $statusMessage = "Failed to change the default marketing category. Please refresh your page and try again";
        $status = 'error';
        if (is_numeric($categoryId) && is_numeric($productId)) {
            // Set the default category for variants
            if ($applyToVariants == 1) {
                $this->_productsMapper->_setAsDefaultCategoryForVariants($productId, $categoryId);
            }

            $productDefaultMarketingCategoryChanged = $this->_productsMapper->setAsDefaultCategory($productId, $categoryId, $isVariant);
            if ($productDefaultMarketingCategoryChanged > 0) {
                $statusMessage = 'Successfully changed the default marketing category.';
                $status = 'success';
            }
            $productDetails = $this->_productsMapper->getMinimumProductsDetails($productId);
            if ($productDetails['status'] == 'ACTIVE') {
                if (! $this->isManualSync()) {
                    // update the product in Elastic Search
                    $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS);
                }
            }
        } else {
            $statusMessage = 'Product id or category id is not numeric. Please refresh your page and try again';
        }
        $this->_errorValue($route, $status, $statusMessage);
    }

    /**
     * Update the merchant category column in the products table
     *
     * @param
     *            $merchantCategorySelectForm
     * @param
     *            $submitedFormData
     * @return mixed
     */
    protected function _saveMerchantCategory($merchantCategorySelectForm, $submitedFormData)
    {
        $merchantCategorySelectForm->setData($submitedFormData);
        if ($merchantCategorySelectForm->isValid()) {
            $productId = $submitedFormData['productId'];
            $data = $merchantCategorySelectForm->getData();
            $merchantCategorySaved = $this->_productsMapper->updateMerchantCategory($data);
            $stage = 3;
            $statusMessage = 'The merchant category was saved successfully.';
            $status = 'success';
            $route = array(
                'controller' => 'Products',
                'action' => 'edit',
                'id' => $productId,
                'stage' => $stage
            );
            $this->_errorValue($route, $status, $statusMessage);
            return $merchantCategorySaved;
        }
    }

    /**
     * Update or Add product add ons
     *
     * @param
     *            $productsAddOnForm
     * @param
     *            $submitedFormData
     * @return mixed
     */
    protected function _saveProductAddOns($productsAddOnForm, $submitedFormData, $stage = false, $edit = false)
    {
        $response = false;

        $productId = $productsAddOnForm['productId'];

        if (isset($productsAddOnForm['productId']) && isset($productsAddOnForm['product-add-on-search']) && isset($productsAddOnForm['productOrVariantIdFrom']) && isset($productsAddOnForm['productOrVariantIdAddOn']) && isset($productsAddOnForm['add-on-variants-select'])) {

            if ($this->_productsMapper->saveProductAddOn($productsAddOnForm)) {
                $response = true;
            } else {
                $response = false;
            }
        }

        $route = array(
            'controller' => 'Products',
            'action' => 'edit',
            'id' => $productId,
            'stage' => 7
        );

        $this->_errorValue($route, 'success', "Add On product has been added");

        return $response;
    }

    /**
     * Save products basic information
     *
     * @param object $productsBasicInformationForm
     * @param array $submitedFormData
     * @param
     *            $updateData
     * @param
     *            $saveAndClose
     * @return mixed
     */
    protected function _addBasicInformation($productsBasicInformationForm, $submitedFormData, $updateData = false, $saveAndClose = false)
    {
        $productsBasicInformationForm->setData($submitedFormData);
        if ($productsBasicInformationForm->isValid()) {
            // need to do another validation for all the prices fields
            $data = $productsBasicInformationForm->getData();
            $websitesSelected = $data['websites'];
            $error = 0;

            if (! empty($submitedFormData['productId'])) {
                $productDetails = $this->_productsMapper->getMinimumProductsDetails($submitedFormData['productId']);
                if ($productDetails['status'] != 'ACTIVE') {
                    if (! empty($data['makeLive']) && $data['makeLive'] != '0000-00-00 00:00:00') {
                        if (! preg_match('/^[0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[\s]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}$/', $data['makeLive'])) {
                            $productsBasicInformationForm->get('makeLive')->setMessages(array(
                                'The Make Live Date format is invalid!'
                            ));
                            $error ++;
                        } else
                            if (strtotime($data['makeLive']) <= time()) {
                                $productsBasicInformationForm->get('makeLive')->setMessages(array(
                                    'The Make Live Date should be in the future!'
                                ));
                                $error ++;
                            }
                    }
                }
            } else {
                if (! empty($data['makeLive']) && $data['makeLive'] != '0000-00-00 00:00:00') {
                    if (! preg_match('/^[0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}[\s]{1}[0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}$/', $data['makeLive'])) {
                        $productsBasicInformationForm->get('makeLive')->setMessages(array(
                            'The Make Live Date format is invalid!'
                        ));
                        $error ++;
                    } else
                        if (strtotime($data['makeLive']) <= time()) {
                            $productsBasicInformationForm->get('makeLive')->setMessages(array(
                                'The Make Live Date should be in the future!'
                            ));
                            $error ++;
                        }
                }
            }

            foreach ($websitesSelected as $websiteId) {
                foreach ($data as $elementName => $elementValue) {
                    if (strpos($elementName, 'Price' . ucfirst(strtolower($this->_currencyConfig[$websiteId]))) !== false) {
                        if ($elementValue == 'not_set' || $elementValue == "" || (float) $elementValue < 0 || preg_match('/^[0-9]{1,11}[\.]{1}[0-9]{2}$/', $elementValue) == 0) {
                            $productsBasicInformationForm->get($elementName)->setMessages(array(
                                'Invalid value specified for this field! Only numbers with 2 decimals are accepted!'
                            ));
                            $error ++;
                        }
                    }
                }
            }
            if ($error > 0) {
                return array();
            }
            $productSaved = $this->_productsMapper->saveProductDetails($data, $updateData);
            if (! empty($productSaved)) {
                if (isset($productSaved['productId'])) {
                    $productId = $productSaved['productId'];
                    $route = array(
                        'controller' => 'Products',
                        'action' => 'edit',
                        'id' => $productId,
                        'stage' => 2
                    );
                    if ($saveAndClose === true) {
                        $route = array(
                            'controller' => 'Products',
                            'action' => 'index'
                        );
                    }
                    $actionTaken = 'saved';
                    $navigationDataUpdate = array();
                    if ($updateData === true) {
                        $actionTaken = 'updated';
                        $navigationDataUpdate = array(
                            'source' => 'product',
                            'title' => $data['productName'],
                            'href' => $data['urlName'],
                            'id' => $data['productId'],
                            'action' => 'update'
                        );
                    }

                    // Save the product spulier price. This will work for new product or for already exising product
                    $this->_saveProductSuplierPrice($productId, $submitedFormData['productSupplierPrice'], $submitedFormData['productSupplierCurrency']);
                    if (! $this->isManualSync()) {
                        $productDetails = $this->_productsMapper->getProductDetailsById($productId, 1);
                        $productVariants = $this->_productsMapper->getVariants($productId);
                        $productCategories = $this->_productsMapper->getProductCategoriesByProductId($productId);
                        $this->_queueService->addSpectrumEntryToQueue($productId, $productDetails, $productVariants, $productCategories);
                        $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS, $navigationDataUpdate);
                    }

                    $this->_errorValue($route, 'success', $productSaved['productName'] . "'s basic information {$actionTaken} successfully.");
                }
            }
            return $productSaved;
        }
        return array();
    }

    /**
     * Save product Attributes
     *
     * @param
     *            $productAttributesSelectForm
     * @param
     *            $submitedFormData
     * @param
     *            $isAjaxRequest
     * @return mixed
     */
    protected function _saveProductAttributes($productAttributesSelectForm, $submitedFormData, $isAjaxRequest = false)
    {
        $productAttributesSelectForm->setData($submitedFormData);
        if ($productAttributesSelectForm->isValid()) {

            $productId = $submitedFormData['productId'];
            $attributesSavedCount = $this->_productsMapper->saveProductAttributes($submitedFormData);
            if (! $isAjaxRequest) {
                $productDetails = $this->_productsMapper->getMinimumProductsDetails($productId);
                if ($productDetails['status'] == 'ACTIVE') {
                    // update the product in Elastic Search
                    if (! $this->isManualSync()) {
                        $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS);
                    }
                }
                $statusMessage = 'The product attributes was saved successfully.';
                $status = 'success';
                $stage = 5;
                $route = array(
                    'controller' => 'Products',
                    'action' => 'edit',
                    'id' => $productId,
                    'stage' => $stage
                );
                $this->_errorValue($route, $status, $statusMessage);
                return $attributesSavedCount;
            }
        }
        return array();
    }

    /**
     * Get the next available barcode - AJAX
     *
     * @return string|bool
     */
    public function getNextBarcodeAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $newBarcode = $this->_productsMapper->getNextAvailableBarcodeForProduct();
            return $this->getResponse()->setContent(JSON::encode(array(
                'barcode' => $newBarcode
            )));
        } else {
            throw new \Exception('Invalid request type!');
        }
    }

    /**
     * Delete selected Products by Ajax Request
     *
     * @param array $params
     * @return integer
     */
    protected function _ajaxDeleteSelectedProducts($params)
    {
        $this->isCmsUserAuthorized(array(
            'delete'
        ));
        $productsDeletedCount = 0;
        if (! empty($params['selectedProducts']) && is_array($params['selectedProducts'])) {
            // Run the function to update status to DELETE for products.
            $productsDeletedCount = $this->_productsMapper->deleteSelectedProducts($params['selectedProducts']);
        }
        return $params['selectedProducts'];
        return $productsDeletedCount;
    }

    /**
     * Ajax search for a product
     *
     * @param type $params
     * @return type
     */
    protected function _ajaxSearchPoroduct($params)
    {
        $searchResults = array();
        if (isset($params['searchQuery']) && trim($params['searchQuery'] != '')) {
            // Run the function to search for products.
            $searchResults = $this->_productsMapper->searchByProductName($params['searchQuery']);
        }
        return $searchResults;
    }

    /**
     *
     * @param
     *            $params
     * @return int|\PrismProductsManager\Model\type
     */
    protected function _ajaxSaveRelatedProducts($params)
    {
        $saveRelatedProducts = 0;
        $productId = isset($params['productId']) ? $params['productId'] : 0;
        if ($productId > 0) {
            $nextStage = 6;
            // Save the related products.
            $saveRelatedProducts = $this->_productsMapper->saveRelatedProducts($params);
            if ($saveRelatedProducts) {
                $productDetails = $this->_productsMapper->getMinimumProductsDetails($productId);
                if ($productDetails['status'] == 'ACTIVE') {
                    // update the product in Elastic Search
                    if (! $this->isManualSync()) {
                        $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS);
                    }
                }
            }
        }
        return $saveRelatedProducts;
    }

    /**
     * Update variants barcode
     *
     * @param type $params
     * @return type
     */
    protected function _ajaxUpdateVariantBarcode($params)
    {
        if (count($params) > 0) {
            $variantId = isset($params['variantId']) ? $params['variantId'] : 0;
            if ($variantId > 0) {
                // Run the function to update barcode for variants.
                $barcodeUpdated = $this->_productsMapper->updateVariantBarcode($variantId, $params['barcode']);
                return $barcodeUpdated;
            }
        }
    }

    /**
     *
     * @param type $params
     * @return type
     */
    protected function _ajaxGetVariants($params)
    {
        if (count($params) > 0) {
            $productId = isset($params['productId']) ? $params['productId'] : 0;
            if ($productId > 0) {
                // Run the function to retrieve product variants.
                $allSavedVariants = $this->_productsMapper->getVariants($productId);
                $productDetails = $this->_productsMapper->getMinimumProductsDetails($productId);
                // $allSavedVariants contains all the existing variants. we will use the view renderer to
                // render them and return the HTMl back to the view
                $variantsView = new ViewModel();
                $variantsView->setTerminal(true);
                $variantsView->setTemplate('prism-products-manager/products/partials/add-edit/partial-variant-table');
                $variantsView->setVariable('productId', $productId);
                $variantsView->setVariable('allSavedVariants', $allSavedVariants);
                $variantsView->setVariable('productStatus', $productDetails['status']);
                $variantsViewOutput = $this->getServiceLocator()
                    ->get('viewrenderer')
                    ->render($variantsView);
                $variantsViewOutput = utf8_encode($variantsViewOutput);
                return array(
                    'data' => $variantsViewOutput,
                    'dataCount' => $allSavedVariants['dataCount']
                );
            }
        }
    }

    /**
     *
     * @param
     *            $params
     * @return mixed
     */
    protected function _ajaxSaveVariants($params = array())
    {
        if (count($params) > 0) {
            $productId = isset($params['productId']) ? $params['productId'] : 0;
            if ($productId > 0) {
                // We have already extracted the product ID.
                unset($params['productId']);
                // Run the function to save variants.
                // if the main product hasn't been already saved in spectrum
                $productAlreadySavedInSpectrum = $this->_productsMapper->checkIfProductHasBeenSavedInSpectrum($productId);

                if (! $productAlreadySavedInSpectrum) {
                    $savedVariantsResponse = $this->_productsMapper->saveVariants($params, $productId);
                    if (is_array($savedVariantsResponse)) {
                        return array(
                            'success' => false,
                            'error' => $savedVariantsResponse['error']
                        );
                    } else {
                        // if product is active
                        $productDetails = $this->_productsMapper->getMinimumProductsDetails($productId);
                        if (! $this->isManualSync()) {
                            if ($productDetails['status'] == 'ACTIVE') {
                                // update the product in Elastic Search
                                $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS);
                            }
                            $this->_queueService->addSpectrumEntryToQueue($productId);
                        }
                        return array(
                            'success' => true,
                            'error' => $savedVariantsResponse
                        );
                    }
                } else {
                    throw new \Exception('You cannot add any more variants since the product has already been saved in SPECTRUM!');
                }
            }
        }
    }

    /**
     * Function to retrieve attribute value using ajax post
     *
     * @param
     *            $params
     * @return type
     */
    protected function _ajaxGetAttributeValues($params)
    {
        $merchantCategoriesAttributeValues = array();
        $selectedAttributeId = $params['selectedAttribute'];
        if ($selectedAttributeId > 0) {
            $languageId = $this->_currentLanguage;
            // Run the function to retrieve the merchant category.
            if (! empty($params['commonAttribute'])) {
                $select = $this->commonAttributesValuesTable->getTableGateway()
                    ->getSql()
                    ->select();
                $select->where('commonAttributeId = ' . $params['selectedAttribute']);
                $select->where($this->commonAttributesValuesDefinitionsTable->getTable() . '.languageId = ' . $this->_currentLanguage);
                $select->join($this->commonAttributesValuesDefinitionsTable->getTable(), $this->commonAttributesValuesDefinitionsTable->getTable() . '.commonAttributeValueId = ' . $this->commonAttributesValuesTable->getTable() . '.commonAttributeValueId', '*', Select::JOIN_LEFT);
                $data = $this->commonAttributesValuesTable->getTableGateway()->selectWith($select);
                $dataCount = $data->count();
                $data = $this->commonAttributesValuesTable->listResultsWithKeyAs($data, '@COMMON_ATTRIBUTES_VALUES', 'commonAttributeValueId', 'value', true);
                $merchantCategoriesAttributeValues = array(
                    'data' => $data,
                    'dataCount' => $dataCount,
                    'attributeType' => 'common'
                );
            } else {
                $merchantCategoriesAttributeValues = $this->_merchantCategoriesMapper->fetchMerchantAttributeValues($selectedAttributeId, $languageId);
            }
        }
        return $merchantCategoriesAttributeValues;
    }

    /**
     * Function to retrieve categories using ajax post
     *
     * @param type $params
     * @return type
     */
    protected function _ajaxGetMerchantCategories($params)
    {
        // Retrieve the categoryLevel and parentId
        $categoryLevel = $params['categoryLevel'];
        $parentId = $params['merchantCategoryId'];

        // Set some defaults
        $result = array(
            'success' => 0,
            'dataCount' => 0,
            'categoryLevel' => $categoryLevel,
            'parentId' => $parentId
        );

        if ($parentId > 0) {
            // Run the function to retrieve the merchant category.
            $merchantCategories = $this->_merchantCategoriesMapper->fetchMerchantCategories($parentId);
            // Get the result count
            $dataCount = $merchantCategories['dataCount'];
            if ($dataCount > 0) {
                $result['success'] = 1;
                $result['dataCount'] = $dataCount;
                $result['data'] = $merchantCategories['data'];
                $result['childrenCount'] = $merchantCategories['childrenCount'];
            }
        }
        return $result;
    }


    /**
     *
     * @param
     *            $params
     * @return array|\PrismProductsManager\Model\type
     */
    protected function _ajaxGetSkuBuildingAttributes($params)
    {
        $skuBuildingAttributes = array();
        if (isset($params['merchantCategoryId']) && $params['merchantCategoryId'] > 0) {
            $languageId = $this->_currentLanguage;
            $skuBuildingAttributes = $this->_merchantCategoriesMapper->fetchSkuBuildingAttribute($params['merchantCategoryId'], $languageId);
        }

        return $skuBuildingAttributes;
    }

    /**
     *
     * @throws \Exception
     */
    protected function _clearEvoucherCache()
    {
        $websites = $this->_siteConfig['siteConfig']['websites']['clear-cache'];
        foreach ($websites as $website) {
            $url = sprintf("%sadmin-tasks/clear-evoucher-cache/%s/%s", $website, $this->_currentWebsite, $this->_currentLanguage);

            $client = new Client();
            $client->setEncType(Client::ENC_URLENCODED);
            $client->setUri($url);
            $client->setMethod(Request::METHOD_GET);

            $response = $client->send();
            $result = $response->getBody();
            if (! $response->isSuccess()) {
                // @TODO add the action to cron so the cache will be updated later
            }
        }

        return $result;
    }

    /**
     * Using the makeLive date switch the product definition based on webisteId, languageId and productId
     * Note: this work with contnet version
     * Steps:
     * - Get all definition that should be active today
     * - Inactivate the old definiotion
     * - Activate the new defintion
     * - Re-queue the product for elasticSearch procesing
     */
    public function productDefinitionMakeLiveAction()
    {
        $makeLiveList = $this->_productsMapper->proccessProductDefinitionList();

        for ($i = 0, $count = count($makeLiveList); $i < $count; $i ++) {
            $productId = $makeLiveList[$i];

            // add to Elastic Search queue
            $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS);
        }
        return new JsonModel($makeLiveList);
    }

    /**
     * Using makeLive date activate prdouct or product variation to be availble for website
     */
    public function ProductMakeLiveAction()
    {
        // $makeLiveList =
    }

    /**
     *
     * @param
     *            $productId
     * @param
     *            $productSupplierPrice
     * @param
     *            $productSupplierCurrency
     */
    protected function _saveProductSuplierPrice($productId, $productSupplierPrice, $productSupplierCurrency)
    {
        if (! empty($productId)) {
            $this->_productsMapper->saveProcductSupplierPrice($productId, $productSupplierPrice, $productSupplierCurrency);
        }
    }

    /**
     *
     * @param
     *            $productId
     * @param
     *            $variants
     */
    protected function _resetVariantsMarketingCategories($productId, $variants)
    {
        if (! is_array($variants)) {
            $variants = explode("-", $variants);
        }
        $result = $this->_productsMapper->resetVariantsMarketingCategories($productId, $variants);
        return $result;
    }

    /**
     *
     * @throws \Exception
     */
    public function getAllVariantsAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        $request = $this->getRequest();
        $responseArray = array();

        if ($request->isXmlHttpRequest()) {
            $variants = $this->_productsMapper->getAllVariantsSelect();

            return new jsonModel(array(
                'data' => $variants
            ));
        } else {
            throw new \Exception('Invalid access type');
        }
    }

    /**
     *
     * @throws \Exception
     */
    public function getWhatToUpdateInfoAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $params = $request->getPost();

            $targetToUpdate = ! empty($params['targetToUpdate']) ? $params['targetToUpdate'] : '';
            $whatToUpdate = ! empty($params['whatToUpdate']) ? $params['whatToUpdate'] : '';
            $htmlOutput = '';
            switch ($whatToUpdate) {
                case 'images':
                    $images = $this->_productsMapper->getImagesForProduct($targetToUpdate, 1);
                    $imagesHtml = new ViewModel();
                    $imagesHtml->setTerminal(true);
                    $imagesHtml->setTemplate('prism-products-manager/products/partials/add-edit/transfer-images');
                    $imagesHtml->setVariable('images', $images);
                    $imagesHtmlOutput = $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($imagesHtml);
                    $htmlOutput = utf8_encode($imagesHtmlOutput);
                    break;
                case 'basic-details':
                    $variantDetails = $this->_productsMapper->getVariantDetails($targetToUpdate, $this->_currentLanguage);
                    $variantDetailsHtml = new ViewModel();
                    $variantDetailsHtml->setTerminal(true);
                    $variantDetailsHtml->setTemplate('prism-products-manager/products/partials/add-edit/transfer-variant-details');
                    $variantDetailsHtml->setVariable('variantDetails', $variantDetails);
                    $variantDetailsHtmlOutput = $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($variantDetailsHtml);
                    $htmlOutput = utf8_encode($variantDetailsHtmlOutput);
                    break;
                case 'prices':
                    $allPricesCamelCase = $this->_productsMapper->getProductPrices($targetToUpdate, false, 1);
                    $supplierPrices = $this->_productsMapper->getSupplierPrices($targetToUpdate);

                    $allPrices = array();
                    foreach ($allPricesCamelCase as $price => $priceValue) {
                        $priceName = preg_split('/(?=[A-Z])/', $price);
                        $priceNameImploded = implode(' ', $priceName);
                        $allPrices[] = array(
                            'name' => ucfirst($priceNameImploded),
                            'value' => $priceValue
                        );
                    }
                    $variantPricesHtml = new ViewModel();
                    $variantPricesHtml->setTerminal(true);
                    $variantPricesHtml->setTemplate('prism-products-manager/products/partials/add-edit/transfer-prices');
                    $variantPricesHtml->setVariable('allPrices', $allPrices);
                    $variantPricesHtml->setVariable('supplierPrice', $supplierPrices);
                    $variantPricesHtmlOutput = $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($variantPricesHtml);
                    $htmlOutput = utf8_encode($variantPricesHtmlOutput);
                    break;
            }
            return new JsonModel(array(
                'html' => $htmlOutput
            ));
        } else {
            throw new \Exception('Invalid access type !');
        }
    }

    /**
     *
     * @throws \Exception
     */
    public function transferToVariantsAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));

        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $params = $request->getPost();
            $variants = ! empty($params['variants']) ? $params['variants'] : '';
            $oldProductId = ! empty($params['productId']) ? $params['productId'] : '';
            $variants = explode('-', $variants);
            $serialized = ! empty($params['serialized']) ? $params['serialized'] : '';
            parse_str($serialized, $serialized);
            $selected = false;
            $response = array();
            if (! empty($serialized['select-all'])) {
                $productId = $this->_productsMapper->getProductIdFromVariantId($serialized['all-sku-variants']);
                $targetVariantId = $serialized['all-sku-variants'];
            } else {
                $productId = $this->_productsMapper->getProductIdFromVariantId($serialized['sku-select-product']);
                $targetVariantId = $serialized['sku-select-product'];
            }
            if (! empty($serialized['transfer-images'])) {
                $response = $this->_productsMapper->transferImagesToVariant($productId, $targetVariantId, $variants, $oldProductId);
                $selected = true;
            }

            if (! empty($serialized['transfer-basic-details'])) {
                $response = $this->_productsMapper->transferBasicDetails($variants, $targetVariantId);
                $selected = true;
            }

            if (! empty($serialized['transfer-prices'])) {
                $response = $this->_productsMapper->transferPrices($variants, $targetVariantId);
                $selected = true;
            }

            if (! $selected) {
                $errors[] = 'Please select what you want to transfer !';
                $response['success'] = false;
                $response['error'] = $errors;
            } else {
                if (! $this->isManualSync()) {
                    // update the product in Spectrum
                    $this->_queueService->addSpectrumEntryToQueue($oldProductId);
                    // update the product in Elastic Search
                    $this->_queueService->addIndexEntryToQueue($oldProductId . '_0', ElasticSearchTypes::PRODUCTS);
                }
            }
            return new JsonModel($response);
        } else {
            throw new \Exception('Invalid access type !');
        }
    }

    /**
     *
     * @return JsonModel
     * @throws \Exception
     */
    public function searchForProductAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        // Get the request
        $request = $this->getRequest();

        // Retrieve the submitted data form.
        $params = $this->params()->fromPost();

        if ($request->isPost()) {

            $productsArray = $this->_productsMapper->searchRelatedProducts($params['searchString']);
            $relatedProducts = array();
            if (! empty($params['variantId'])) {
                $relatedVariantsArray = $this->_productsMapper->getVariantRelatedVariants($params['variantId']);
                $relatedProducts = $this->_productsMapper->getVariantDetailsForRelated($relatedVariantsArray);
            }
            $productsArray = $this->_productsMapper->excludeAlreadySavedProductsFromSearchDisplay($productsArray, $relatedProducts);
            $relatedProductsView = new ViewModel();
            $relatedProductsView->setTerminal(true);
            $relatedProductsView->setTemplate('prism-products-manager/products/partials/add-edit/partial-related-products-build');
            $relatedProductsView->setVariable('products', $productsArray['products']);
            $relatedProductsView->setVariable('relatedProducts', $relatedProducts);
            $relatedProductsView->setVariable('reachedLimit', $productsArray['reachedLimit']);
            $relatedProductsHtmlOutput = $this->getServiceLocator()
                ->get('viewrenderer')
                ->render($relatedProductsView);
            $relatedProductsHtmlOutput = utf8_encode($relatedProductsHtmlOutput);

            return new JsonModel(array(
                'html' => $relatedProductsHtmlOutput
            ));
        }
        throw new \Exception('Not allowed !');
    }

    /**
     */
    public function saveRelatedProductsAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        // Get the request
        $request = $this->getRequest();

        // Retrieve the submitted data form.
        $params = $this->params()->fromPost();

        $relatedVariants = ! empty($params['relatedVariantsIds']) ? $params['relatedVariantsIds'] : array();
        $variantsIds = ! empty($params['variantsIds']) ? $params['variantsIds'] : array();
        $productId = $params['productId'];

        if ($request->isPost()) {
            $this->_productsMapper->saveRelatedVariants($relatedVariants, $variantsIds);
            if (! $this->isManualSync()) {
                $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS);
            }

            return new JsonModel(array(
                'success' => true
            ));
        }
    }

    /**
     */
    public function checkRelatedProductsAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        // Get the request
        $request = $this->getRequest();

        // Retrieve the submitted data form.
        $params = $this->params()->fromPost();

        if ($request->isPost()) {
            $relatedVariantsArray = $this->_productsMapper->checkIfVariantsHaveSameRelatedVariants($params['selectedVariants']);

            if (! $relatedVariantsArray['error']) {
                $productsArray = $this->_productsMapper->getVariantDetailsForRelated($relatedVariantsArray['relatedVariants']);
                $relatedProductsView = new ViewModel();
                $relatedProductsView->setTerminal(true);
                $relatedProductsView->setTemplate('prism-products-manager/products/partials/add-edit/partial-related-products-build');
                $relatedProductsView->setVariable('relatedProducts', $productsArray);
                $relatedProductsView->setVariable('products', array());
                $relatedProductsView->setVariable('reachedLimit', false);
                $relatedProductsHtmlOutput = $this->getServiceLocator()
                    ->get('viewrenderer')
                    ->render($relatedProductsView);
                $relatedProductsHtmlOutput = utf8_encode($relatedProductsHtmlOutput);

                return new JsonModel(array(
                    'error' => false,
                    'htmlData' => $relatedProductsHtmlOutput
                ));
            } else {
                return new JsonModel(array(
                    'error' => true,
                    'htmlData' => ''
                ));
            }
        }
    }


    /**
     *
     * @return JsonModel
     */
    public function syncVariantsAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        // Get the request
        $request = $this->getRequest();

        if ($request->isPost()) {
            // Retrieve the submitted data form.
            $submittedData = $this->params()->fromPost();
            $productId = $submittedData['productId'];
            if (! empty($submittedData)) {
                try {
                    $navigationDataUpdate = array(
                        'source' => 'product',
                        'title' => '',
                        'href' => '',
                        'id' => $productId,
                        'action' => 'activate'
                    );
                    $productDetails = $this->_productsMapper->getProductDetailsById($productId, 1);
                    $productVariants = $this->_productsMapper->getVariants($productId);
                    $productCategories = $this->_productsMapper->getProductCategoriesByProductId($productId);
                    $this->_queueService->addSpectrumEntryToQueue($productId, $productDetails, $productVariants, $productCategories);

                    $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS, $navigationDataUpdate);
                    return new JsonModel(array(
                        'data' => true
                    ));
                } catch (\Exception $ex) {
                    return new JsonModel(array(
                        'data' => false
                    ));
                }
            }
        }
        return new JsonModel(array(
            'data' => false
        ));
    }

    /**
     *
     * @return JsonModel
     */
    public function saveSkuAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        // Get the request
        $request = $this->getRequest();

        if ($request->isPost()) {
            // Retrieve the submitted data form.
            $submittedData = $this->params()->fromPost();
            $variantId = $submittedData['variantId'];
            $sku = strtoupper($submittedData['sku']);
            $validator = new \Zend\Validator\Regex(array(
                'pattern' => '/^[0-9a-zA-Z_-]{1,14}.$/'
            ));
            $alreadySavedInSpectrum = $this->_productsMapper->variantIsSavedInSpectrum($variantId);
            if ($validator->isValid($sku) && ! $alreadySavedInSpectrum) {
                if (! empty($submittedData)) {
                    try {
                        if (! $this->_productsMapper->checkIfSkuIsUnique($sku)) {
                            $this->_productsMapper->saveSkuForVariant($variantId, $sku);
                            return new JsonModel(array(
                                'data' => true
                            ));
                        }
                    } catch (\Exception $ex) {
                        return new JsonModel(array(
                            'data' => false
                        ));
                    }
                }
            }
        }
        return new JsonModel(array(
            'data' => false
        ));
    }

    /**
     *
     * @return JsonModel
     */
    public function modifyProductTabStatusAction()
    {
        $this->isCmsUserAuthorized(array(
            'add',
            'edit'
        ));
        // Get the request
        $request = $this->getRequest();

        if ($request->isPost()) {
            // Retrieve the submitted data form.
            $newStatus = $this->params()->fromPost('newStatus', false);
            $tabId = $this->params()->fromPost('tabId', false);
            $productId = $this->params()->fromPost('productId', false);
            $this->_productsMapper->updateProductTabStatus($newStatus, $tabId);

            if (! $this->isManualSync()) {
                $this->_queueService->addIndexEntryToQueue($productId . '_0', ElasticSearchTypes::PRODUCTS);
            }
        }
        return new JsonModel(array(
            'success' => true
        ));
    }

    /**
     *
     * @return JsonModel
     */
    public function getOptionsDataForStyleAction()
    {
        $this->isCmsUserAuthorized(array(
            'view'
        ));
        $request = $this->getRequest();


        if ($request->isPost()) {
            $style = $this->params()->fromPost('style', false);
            $formData = $this->params()->fromPost('formData', false);
            $prices = $this->params()->fromPost('prices', false);
            parse_str($formData, $formData);
            if (! empty($style)) {
//                $options = $this->_productsMapper->getAllOptionsForStyle($style);
                $options = $this->getOptionsData($style);

                $finalData = $this->_productsMapper->getValuesForUpdatedFields($formData, $options, $prices);

                $optionsCheckView = new ViewModel();
                $optionsCheckView->setTerminal(true);
                $optionsCheckView->setTemplate('prism-products-manager/products/partials/product-creation-tabs/options-check');
                $optionsCheckView->setVariable('finalData', $finalData);
                $optionsCheckView->setVariable('prices', $prices);
                $optionsCheckViewOutput = $this->getServiceLocator()
                    ->get('viewrenderer')
                    ->render($optionsCheckView);
                $optionsCheckViewOutput = utf8_encode($optionsCheckViewOutput);

                return new JsonModel(array(
                    'html' => $optionsCheckViewOutput
                ));
            }
        }
    }

    /**
     *
     * @return bool|JsonModel
     */
    public function generateStyleAction()
    {
        $this->isCmsUserAuthorized(array(
            'view'
        ));
        $request = $this->getRequest();

        if ($request->isPost()) {
            $categoryId = $this->params()->fromPost('categoryId', false);
            $seasonId = $this->params()->fromPost('seasonId', false);

            if ($categoryId && $seasonId) {
                $categoryCode = $this->commonAttributesService->getCommonAttributeValueCodeByValueId($categoryId);
                $seasonCode = $this->commonAttributesService->getCommonAttributeValueCodeByValueId($seasonId);
                $styleCode = $this->_productsMapper->retrieveNextAvailableStyle($categoryCode . $seasonCode);

                return new JsonModel(array(
                    'styleCode' => $styleCode
                ));
            }
        }
        return false;
    }

    /**
     *
     * @return bool|mixed|\Zend\Stdlib\Message
     */
    public function manageSkuForOptionAction()
    {
        $this->isCmsUserAuthorized(array(
            'view'
        ));
        $request = $this->getRequest();

        if ($request->isPost()) {
            $method = $this->params()->fromPost('method', false);
            /**
             * Needed for fetch method
             */
            $option = $this->params()->fromPost('option', false);
            /**
             * Needed for update method
             */
            $dataArray = $this->params()->fromPost('dataArray', false);

            switch ($method) {
                case 'fetch':
                    $skuArray = array();
                    $productData = $this->_productsMapper->getVariantDetailsByStyle($option, false);
                    if (empty($productData)) {
                        $option = str_replace(' ', '', $option);
                        $productData = $this->_productsMapper->getVariantDetailsByStyle($option, false);
                    }
                    // Build Table view
                    $manageSkusView = new ViewModel();
                    $manageSkusView->setTerminal(true);
                    $manageSkusView->setTemplate('prism-products-manager/products/partials/product-creation-tabs/manage-skus');
                    $manageSkusView->setVariable('productData', $productData);
                    $manageSkusViewOutput = $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($manageSkusView);
                    $manageSkusViewOutput = utf8_encode($manageSkusViewOutput);

                    return $this->getResponse()->setContent(json_encode(array(
                        'html' => $manageSkusViewOutput
                    )));
                    break;
                case 'update':
                    try {
                        foreach ($dataArray as $data) {
                            $this->_productsMapper->updateVariantStatusByName('NOSKU:)', $data['status'], $data['variantId']);
                        }
                        return $this->getResponse()->setContent(json_encode(array(
                            'success' => true
                        )));
                    } catch (\Exception $ex) {
                        return $this->getResponse()->setContent(json_encode(array(
                            'success' => false
                        )));
                    }
                    break;
            }
        }
        return $this->getResponse()->setContent(json_encode(array(
            'success' => false
        )));
    }

    /**
     * @param $style
     * @return array|mixed|null
     */
    protected function getOptionsData($style)
    {
        $cacheKey = $style . '_options_list_products';

        $optionsDataCached = $this->cacheAdapter->getCache($cacheKey, true);

        if (null !== $optionsDataCached) {
            return $optionsDataCached;
        }

        $optionsData = $this->_productsMapper->getAllOptionsForStyle($style);

        $this->cacheAdapter->setCache($cacheKey, $optionsData);

        return $optionsData;
    }

    /**
     * updateCachedOptionsData
     *
     * To be used after creating new option of after updating option data
     *
     * @param $style
     * @return array|mixed|null
     */
    protected function updateCachedOptionsData($style){
        $cacheKey = $style . '_options_list_products';

        $this->cacheAdapter->removeCache($cacheKey);

        return $this->getOptionsData($style);
    }

}

