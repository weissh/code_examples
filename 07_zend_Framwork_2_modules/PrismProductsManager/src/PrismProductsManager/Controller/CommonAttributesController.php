<?php

namespace PrismProductsManager\Controller;

use PrismCommon\Service\SimplePagination;
use PrismProductsManager\Form\CommonAttributesForm;
use PrismProductsManager\Form\CommonAttributesValuesForm;
use PrismProductsManager\Form\SearchForm;
use PrismProductsManager\Form\TerritoryOverrideForm;
use PrismProductsManager\Model\CommonAttributesMapper;
use PrismProductsManager\Model\ProductsMapper;
use PrismProductsManager\Service\AssignFieldToTabsService;
use PrismProductsManager\Service\CommonAttributesService;
use Zend\Db\Adapter\Adapter;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Validator\Date;
use Zend\View\Model\ViewModel;
use Zend\View\View;

/**
 * Class CommonAttributesController
 * @package PrismProductsManager\Controller
 * @author  Hani Weiss <hani.weiss@whistl.co.uk>
 */
class CommonAttributesController extends AbstractActionController
{
    /**
     * @var SimplePagination
     */
    protected $simplePagination;
    /**
     * @var SearchForm
     */
    protected $searchForm;
    /**
     * @var Adapter
     */
    private $dbAdapter;
    /**
     * @var CommonAttributesValuesForm
     */
    protected $commonAttributesValuesForm;
    /**
     * @var Array
     */
    protected $clientConfig;
    /**
     * @var Boolean
     */
    protected $errorFlag = false;
    /**
     * @var CommonAttributesMapper
     */
    protected $commonAttributesMapper;
    /**
     * @var CommonAttributesForm
     */
    protected $commonAttributesForm;
    /**
     * @var CommonAttributesService
     */
    protected $commonAttributesService;
    /**
     * @var Object
     */
    protected $viewRenderer;
    /**
     * @var ProductsMapper
     */
    protected $productsMapper;

    /**
     * @var AssignFieldToTabsService
     */
    protected $assignFieldsToTabsService;
    /**
     * @param SimplePagination $simplePagination
     * @param SearchForm $searchForm
     * @param $config
     * @param CommonAttributesMapper $commonAttributesMapper
     * @param CommonAttributesForm $commonAttributesForm
     * @param Adapter $dbAdapter
     * @param CommonAttributesValuesForm $commonAttributesValuesForm
     * @param View $viewRenderer
     * @param CommonAttributesService $commonAttributesService
     * @param ProductsMapper $productsMapper
     * @param AssignFieldToTabsService $assignFieldsToTabsService
     * @param TerritoryOverrideForm $territoryOverrideForm
     */
    public function __construct(
        SimplePagination $simplePagination,
        SearchForm $searchForm,
        $config,
        CommonAttributesMapper $commonAttributesMapper,
        CommonAttributesForm $commonAttributesForm,
        Adapter $dbAdapter,
        CommonAttributesValuesForm $commonAttributesValuesForm,
        $viewRenderer,
        CommonAttributesService $commonAttributesService,
        ProductsMapper $productsMapper,
        AssignFieldToTabsService $assignFieldsToTabsService,
        TerritoryOverrideForm $territoryOverrideForm
    )
    {
        $this->simplePagination = $simplePagination;
        $this->searchForm = $searchForm;
        $this->clientConfig = $config['clientConfig'];
        $this->commonAttributesMapper = $commonAttributesMapper;
        $this->commonAttributesForm = $commonAttributesForm;
        $this->dbAdapter = $dbAdapter;
        $this->commonAttributesValuesForm = $commonAttributesValuesForm;
        $this->viewRenderer = $viewRenderer;
        $this->commonAttributesService = $commonAttributesService;
        $this->productsMapper = $productsMapper;
        $this->assignFieldsToTabsService = $assignFieldsToTabsService;
        $this->territoryOverrideForm = $territoryOverrideForm;
    }

    /**
     * Function to display common attributes, create new attribute and edit existing attributes
     * @return ViewModel
     */
    public function indexAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $request = $this->getRequest();
        /**
         *  Set the values for the errorFlag to false
         *  If true Popup opens on page load
         */
        $this->errorFlag = false;

        $perPageGet = $this->params()->fromQuery('perPageGet', 10);
        $page = $this->params()->fromRoute('page', 1);
        $search = $this->params()->fromPost('search', '');
        if (empty($search)) {
            $search = $this->params()->fromRoute('search', '');
        }

        if ($request->isPost() && empty($search)) {
            $this->commonAttributesForm->setData($request->getPost()->toArray());
            if ($this->commonAttributesForm->isValid()) {
                $sanitizedData = $this->commonAttributesForm->getData();

                /**
                 * If common attribute id does not exist add new attribute
                 * else edit existing attribute
                 */
                if (empty($sanitizedData['commonAttributeId'])) {
                    $sanitizedData['isEditable'] = 1;
                    if ($this->isCmsUserAuthorized(array('add')) && !$this->commonAttributesService->createNewCommonAttribute($sanitizedData)) {
                        /** Redirect to main page in case of exception */
                        return $this->redirectToMainPage('The attribute could not be created. Please contact support.', $page);
                    } else {
                        /** Redirect to main page to success */
                        return $this->redirectToMainPage('Attribute has been created.', $page, 'index', 'CommonAttributes', 'success');
                    }
                } else {
                    if ($this->isCmsUserAuthorized(array('edit')) && !$this->commonAttributesService->editCommonAttribute($sanitizedData)) {
                        /** Redirect to main page in case of exception */
                        return $this->redirectToMainPage('The attribute could not be edited. Please contact support.', $page);
                    } else {
                        /** Redirect to main page to success */
                        return $this->redirectToMainPage('Attribute has been edited.', $page, 'index', 'CommonAttributes', 'success');
                    }
                }
            } else {
                $this->errorFlag = true;
            }
        }
        
        if ($perPageGet == 'All') {
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', '0');
            
            $limit = 0;
        } else {
            $limit = $perPageGet;
        }

        //Check if the form was submitted with search then set the page value to empty
        if ($request->isPost() && !empty($search)) {
            $page= 1;
        }

        $commonAttributes = $this->commonAttributesService->getCommonAttributes($page, $limit, $search);

        if ($this->isFeatureEnabled() && $this->checkIfUserIsAuthorized()) {
            return new ViewModel(array(
                'allowed' => true,
                'commonAttributes' => $commonAttributes,
                'page' => $page,
                'searchForm' => $this->searchForm,
                'commonAttributesForm' => $this->commonAttributesForm,
                'errorFlag' => $this->errorFlag,
                'search' => $search, 
                'perPageGet' => $perPageGet,
            ));
        } else {
            return new ViewModel(array(
               'allowed' => false,
                'search' => false,
                'perPageGet' => $perPageGet,
            ));
        }
    }

    public function removeDefaultValueAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->getPost()->toArray();
            $successFlag = $this->commonAttributesService->removeDefaultValueFromValue($formData['commonAttributeValueId']);

            $response = $this->getResponse();
            $response->setContent(json_encode(array('success' => $successFlag)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }
        $response = $this->getResponse();
        $response->setStatusCode(401);
        $response->setContent('Unauthorized');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }

    /**
     * @return null
     */
    public function saveTerritoryOverridesAction()
    {
        $this->isCmsUserAuthorized(array('edit','add'));
        $request = $this->getRequest();
        $formData = array();

        if ($request->isPost()) {
            $formData = $request->getPost()->toArray();
            /** Retrieve the commonAttributeValue Doctrine Entity */
            $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueBy(array('commonAttributeValueId' => $formData['commonAttributeValueId']));

            /** Check if the form data is empty and delete the old territory overrides */
            if (!empty($formData) && $this->commonAttributesService->deleteTerritoryOverrides($commonAttributeValueEntity)) {
                /** Create new territory override for each territory */
                foreach ($formData['skuCode'] as $territory => $skuCode) {
                    $this->commonAttributesService->createNewTerritoryOverride($commonAttributeValueEntity, $formData, $territory);
                }
                $successFlag = $this->commonAttributesService->doctrineFlush();
                /** Redirect based on save response */
                if ($successFlag) {
                    /** Redirect to common attribute page - success */
                    $msg = 'Territory overrides have been saved.';
                    $type = 'success';
                } else {
                    /** Redirect to common attribute page - error */
                    $msg = 'An error has occurred while trying to create new territory overrides. Please contact support with following code : CRT-TO-' . $commonAttributeValueEntity->getCommonAttributeId()->getCommonAttributeId();
                    $type = 'error';
                }
                return $this->redirectToMainPage(
                    $msg,
                    '1',
                    'viewAttribute',
                    'CommonAttributes',
                    $type,
                    $commonAttributeValueEntity->getCommonAttributeId()->getCommonAttributeId()
                );
            }
        }
        return $this->redirectToMainPage(
            'An error has occurred while trying to create new territory overrides. Please contact support with following code: CRT-TO2-' . json_encode($formData),
            '1',
            'index',
            'CommonAttributes',
            'error'
        );
    }

    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function getTerritoryOverridesAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $request = $this->getRequest();
        $commonAttributeValueId = $this->params()->fromPost('commonAttributeValueId', null);

        if ($request->isPost() && !is_null($commonAttributeValueId)) {
            $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueBy(array('commonAttributeValueId' => $commonAttributeValueId));
            /** Retrieve all values */
            $valueOverrides = array();
            foreach($commonAttributeValueEntity->getCommonAttributesValuesOverrides() as $valueOverride) {
                $valueOverrides[] = $valueOverride;
            }
            /** Hydrate form with data */
            $this->territoryOverrideForm->get('commonAttributeValueId')->setValue($commonAttributeValueEntity->getCommonAttributeValueId());
            if (!empty($valueOverride)) {
                foreach ($valueOverrides as $valueOverride) {
                    /** hydrate sku code */
                    $this->territoryOverrideForm->get('skuCode['.$valueOverride->getterritoryId()->getterritoryId().']')
                                                    ->setValue($valueOverride->getCommonAttributeValueSkuCode());
                    /** hydrate value */
                    $this->territoryOverrideForm->get('value['.$valueOverride->getterritoryId()->getterritoryId().']')
                                                    ->setValue($valueOverride->getCommonAttributeValue());
                }
            }
            $territories = $this->commonAttributesService->getAllTerritories(true);
            /** Load html  */
            $overrideValuesModal = new ViewModel();
            $overrideValuesModal->setTerminal(true);
            $overrideValuesModal->setTemplate('prism-products-manager/common-attributes/partials/territory-overrides-modal');
            $overrideValuesModal->setVariable('territoryOverrideForm', $this->territoryOverrideForm);
            $overrideValuesModal->setVariable('commonAttributeValueEntity', $commonAttributeValueEntity);
            $overrideValuesModal->setVariable('territories', $territories);
            $overrideValuesModalOutput = $this->viewRenderer->render($overrideValuesModal);
            $overrideValuesModalOutput = utf8_encode($overrideValuesModalOutput);
            /** Return Response */
            $response = $this->getResponse();
            $response->setContent(json_encode(array('html' => $overrideValuesModalOutput)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }

        $response = $this->getResponse();
        $response->setStatusCode(401);
        $response->setContent('Unauthorized');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }
    /**
     * Method used to manage attribute values
     * @return ViewModel
     */
    public function viewAttributeAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $request = $this->getRequest();
        /**
         *  Set the values for the errorFlag to false
         *  If true Popup opens on page load
         */
        $this->errorFlag = false;
        $commonAttributeId = $this->params()->fromRoute('attributeId', 1);
        $setDefaultValue = $this->params()->fromRoute('set-default', false);

        if ($setDefaultValue) {
            $setAsDefault = $this->commonAttributesService->setValuesAsDefault($setDefaultValue);
            if ($setAsDefault) {
                return $this->redirectToMainPage(
                    'The attribute value has been set up as default.',
                    1,
                    'view-attribute',
                    'common-attributes',
                    'success',
                    $commonAttributeId
                );
            } else {
                return $this->redirectToMainPage(
                    'The attribute value could not be set up as default. Please contact support.',
                    1,
                    'view-attribute',
                    'common-attributes',
                    'error',
                    $commonAttributeId
                );
            }
        }

        if ($request->isPost()) {
            $this->commonAttributesValuesForm->setData($request->getPost()->toArray());
            if ($this->commonAttributesValuesForm->isValid()) {
                $sanitizedData = $this->commonAttributesValuesForm->getData();
                /**
                 * If common attribute value id does not exist add new attribute value
                 * else edit existing attribute value
                 */
                if (empty($sanitizedData['commonAttributeValueId'])) {
                    $this->isCmsUserAuthorized(array('add'));

                    if (!$this->commonAttributesService->createNewCommonAttributeValue($sanitizedData, $commonAttributeId)) {
                        return $this->redirectToMainPage(
                            'The attribute value could not be created. Please contact support.',
                            1,
                            'view-attribute',
                            'common-attributes',
                            'error',
                            $commonAttributeId
                        );
                    } else {
                        return $this->redirectToMainPage(
                            'The attribute value has been created.',
                            1,
                            'view-attribute',
                            'common-attributes',
                            'success',
                            $commonAttributeId
                        );
                    }
                } else {
                    $this->isCmsUserAuthorized(array('edit'));
                    if (!$this->commonAttributesService->editNewCommonAttributeValue($sanitizedData, $commonAttributeId)) {
                        return $this->redirectToMainPage(
                            'The attribute value could not be edited. Please contact support.',
                            1,
                            'view-attribute',
                            'common-attributes',
                            'error',
                            $commonAttributeId
                        );
                    } else {
                        return $this->redirectToMainPage(
                            'The attribute value has been edited.',
                            1,
                            'view-attribute',
                            'common-attributes',
                            'success',
                            $commonAttributeId
                        );
                    }
                }
            } else {
                $this->errorFlag = true;
            }
        }
        $commonAttribute = $this->commonAttributesService->getCommonAttributeById($commonAttributeId);
        $commonAttributeValues = $this->commonAttributesService->getCommonAttributeValues($commonAttributeId);

        $hasDefaultValue = $this->commonAttributesService->checkIfValuesHaveDefault($commonAttributeValues);

        if ($this->isFeatureEnabled() && $this->checkIfUserIsAuthorized()) {
            return new ViewModel(array(
                'allowed' => true,
                'commonAttribute' => $commonAttribute,
                'errorFlag' => $this->errorFlag,
                'commonAttributeValueAddForm' => $this->commonAttributesValuesForm,
                'commonAttributeValues' => $commonAttributeValues,
                'hasDefaultValue' => $hasDefaultValue
            ));
        } else {
            return new ViewModel(array(
                'allowed' => false
            ));
        }
    }

    /**
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function getCommonAttributeValueTimeIntervalAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $commonAttributeValueId = $this->params()->fromPost('commonAttributeValueId', null);
        $request = $this->getRequest();

        if ($request->isPost() && !is_null($commonAttributeValueId)) {
            $timeInterval = $this->commonAttributesService->getTimeIntervalForCommonAttributeValue($commonAttributeValueId);

            /** Load modal for editing attribute with hydrated form */
            $timeIntervalModal = new ViewModel();
            $timeIntervalModal->setTerminal(true);
            $timeIntervalModal->setTemplate('prism-products-manager/common-attributes/partials/common-attribute-values-time-period');
            $timeIntervalModal->setVariable('commonAttributeValueId', $commonAttributeValueId);
            $timeIntervalModal->setVariable('timeInterval', $timeInterval);
            $timeIntervalModalOutput = $this->viewRenderer->render($timeIntervalModal);
            $timeIntervalModalOutput = utf8_encode($timeIntervalModalOutput);
            /** Return Response */
            $response = $this->getResponse();
            $response->setContent(json_encode(array('html' => $timeIntervalModalOutput)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }
        $response = $this->getResponse();
        $response->setStatusCode(401);
        $response->setContent('Unauthorized');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }

    /**
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function getCommonAttributeValueAutoFromDateAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $commonAttributeValueId = $this->params()->fromPost('commonAttributeValueId', null);
        $request = $this->getRequest();

        if ($request->isPost() && !is_null($commonAttributeValueId)) {
            $autoFromDate = $this->commonAttributesService->getAutoStartDateForCommonAttributeValue($commonAttributeValueId);

            /** Load modal for editing attribute with hydrated form */
            $timeIntervalModal = new ViewModel();
            $timeIntervalModal->setTerminal(true);
            $timeIntervalModal->setTemplate('prism-products-manager/common-attributes/partials/common-attribute-values-auto-from-date');
            $timeIntervalModal->setVariable('commonAttributeValueId', $commonAttributeValueId);
            $timeIntervalModal->setVariable('autoFromDate', $autoFromDate);
            $timeIntervalModalOutput = $this->viewRenderer->render($timeIntervalModal);
            $timeIntervalModalOutput = utf8_encode($timeIntervalModalOutput);
            /** Return Response */
            $response = $this->getResponse();
            $response->setContent(json_encode(array('html' => $timeIntervalModalOutput)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }
        $response = $this->getResponse();
        $response->setStatusCode(401);
        $response->setContent('Unauthorized');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }


    /**
     * @return bool|\Zend\Stdlib\ResponseInterface
     */
    public function saveCommonAttributeValueTimeIntervalAction()
    {
        $this->isCmsUserAuthorized(array('add', 'edit'));
        $startDate = $this->params()->fromPost('startDate', null);
        $endDate = $this->params()->fromPost('endDate', null);
        $commonAttributeValueId = $this->params()->fromPost('commonAttributeValueId', null);

        $request = $this->getRequest();
        if ($request->isPost() && !is_null($startDate) && !is_null($endDate) && !is_null($commonAttributeValueId)) {
            $successFlag = $this->commonAttributesService->saveTimePeriodForCommonAttributeValueId($startDate, $endDate, $commonAttributeValueId);
            $response = $this->getResponse();
            if ($successFlag) {
                $response->setContent(json_encode(array('success' => true)));
            } else {
                $response->setContent(json_encode(array('success' => false)));
            }
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }
        return false;
    }

    /**
     * @return bool|\Zend\Stdlib\ResponseInterface
     */
    public function saveCommonAttributeValueAutoFromDateAction()
    {
        $this->isCmsUserAuthorized(array('add', 'edit'));
        $fromDate = $this->params()->fromPost('fromDate', null);
        $commonAttributeValueId = $this->params()->fromPost('commonAttributeValueId', null);

        $request = $this->getRequest();
        if ($request->isPost() && !is_null($fromDate) && !is_null($commonAttributeValueId)) {

            $dateValidator = new Date();
            $response = $this->getResponse();

            if ($dateValidator->isValid($fromDate)) {
                $successFlag = $this->commonAttributesService->saveAutoDateFromForCommonAttributeValueId($fromDate, $commonAttributeValueId);
                if ($successFlag) {
                    $response->setContent(json_encode(array('success' => true)));
                } else {
                    $response->setContent(json_encode(array('success' => false)));
                }

            } else {
                $response->setContent(json_encode(array('success' => false, 'error' => 'Date is not valid.')));
            }

            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;


        }
        return false;
    }
    /**
     * Method used to delete a common attribute
     * @return null / redirect
     */
    public function deleteAttributeAction()
    {
        $this->isCmsUserAuthorized(array('delete'));

        $commonAttributeId = $this->params()->fromRoute('attributeId', null);
        $commonAttribute = $this->commonAttributesService->getCommonAttributeById($commonAttributeId);

        if (is_null($commonAttribute)) {
            return $this->redirectToMainPage('Attribute could not be deleted. Please contact support with following code : DEL-NL-' . $commonAttributeId, 1);
        }
        if (
            ($one = $this->assignFieldsToTabsService->deleteInputFieldsForCommonAttribute($commonAttributeId)) &&
            ($two = $this->productsMapper->deleteCommonAttributeAssignedToProductsValues($commonAttributeId)) &&
            ($three = $this->commonAttributesService->deleteCommonAttribute($commonAttributeId))
        ) {
            return $this->redirectToMainPage('Attribute has been deleted.', 1, 'index', 'CommonAttributes', 'success');
        } else {
            return $this->redirectToMainPage('An error has occurred while trying to delete the attribute. Please contact support with following code : DEL-ER-' . $commonAttributeId . " @{$one}@{$two}@{$three}@", 1);
        }
    }

    /**
     * Method used to change order for common attributes values
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function changeValueOrderAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $updateArray = $this->params()->fromPost('updateArray', array());

        if (!empty($updateArray)) {
            if ($this->commonAttributesService->recalculateCommonAttributesValuesOrder($updateArray)) {
                $response = $this->getResponse();
                $response->setContent(json_encode(array('success' => true)));
                $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
                return $response;
            }
        }
        $response = $this->getResponse();
        $response->setStatusCode(400);
        $response->setContent('Bad Request');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }
    /**
     * @return null
     */
    public function deleteCommonAttributeValueAction()
    {
        $this->isCmsUserAuthorized(array('delete'));

        $commonAttributeValueId = $this->params()->fromRoute('valueId', null);
        $commonAttributeId = $this->params()->fromRoute('attributeId', null);


        if (is_null($commonAttributeValueId)) {
            return $this->redirectToMainPage(
                'The attribute value could not be deleted. Please contact support with following code : DEL-NL-' . $commonAttributeValueId,
                1,
                'view-attribute',
                'common-attributes',
                'error',
                $commonAttributeId
            );
        }

        if (!$this->productsMapper->commonAttributeValueIsUsedInProducts($commonAttributeValueId)) {
            if($this->commonAttributesService->isValuesDefault($commonAttributeValueId)) {
                return $this->redirectToMainPage(
                    'Attribute Values is used as default value in products. Please set new default value.',
                    1,
                    'view-attribute',
                    'common-attributes',
                    'error',
                    $commonAttributeId
                );
            }
            else {
                if (!$this->commonAttributesService->deleteAttributeValue($commonAttributeValueId)) {
                    return $this->redirectToMainPage(
                        'An error has occurred while trying to delete the value. Please contact support with following code : DEL-ER-' . $commonAttributeValueId,
                        1,
                        'view-attribute',
                        'common-attributes',
                        'error',
                        $commonAttributeId
                    );
                } else {
                    return $this->redirectToMainPage(
                        'Attribute value has been deleted.',
                        1,
                        'view-attribute',
                        'common-attributes',
                        'success',
                        $commonAttributeId
                    );
                }
            }
        }
        else {
            return $this->redirectToMainPage(
                'Attribute Values is used in products.',
                1,
                'view-attribute',
                'common-attributes',
                'error',
                $commonAttributeId
            );
        }
    }

    /**
     * Method used to get the modal when editing a common attribute value
     *
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function getCommonAttributeValueAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $commonAttributeValueId= $this->params()->fromPost('commonAttributeValueId', null);
        $commonAttributeId = $this->params()->fromPost('commonAttributeId', null);
        $request = $this->getRequest();

        if ($request->isPost() && !is_null($commonAttributeValueId) && !is_null($commonAttributeId)) {
            $commonAttributeValue = $this->commonAttributesService->getCommonAttributeValueById($commonAttributeValueId);

            /** Hydrate form with data from doctrine entity */
            $this->commonAttributesValuesForm->get('commonAttributeValueId')->setValue(
                $commonAttributeValue->getCommonAttributeValueId()
            );
            $this->commonAttributesValuesForm->get('commonAttributeValueSkuCode')->setValue(
                $commonAttributeValue->getCommonattributevalueskucode()
            );
            $this->commonAttributesValuesForm->get('commonAttributeValueAbbrev')->setValue(
                $commonAttributeValue->getCommonAttributeValueAbbrev()
            );
            $this->commonAttributesValuesForm->get('commonAttributeValueBaseSize')->setValue(
                $commonAttributeValue->getCommonAttributeValueBaseSize()
            );
            $this->commonAttributesValuesForm->get('value')->setValue(
                $commonAttributeValue->getCommonAttributeValuesDefinitions()->getValue()
            );

            if($this->commonAttributesValuesForm->get('sizePropertyCommonAttributeCount')->getValue() > 0){
                for($i=0; $i < $this->commonAttributesValuesForm->get('sizePropertyCommonAttributeCount')->getValue(); $i++){

                    $propertySizeValue = $this->commonAttributesMapper->getCommonAttributesSizePropertyValues($commonAttributeValue->getCommonAttributeValueId(),
                        $this->commonAttributesValuesForm->get('sizePropertyCommonAttribute_' . $i)->getLabelAttributes()['data-common-attribute-id'],
                        $this->commonAttributesValuesForm->get('sizePropertyCommonAttribute_' . $i)->getLabelAttributes()['data-common-attribute-size-property-id']);

                    $this->commonAttributesValuesForm->get('sizePropertyCommonAttribute_' . $i)->setValue($propertySizeValue);

                    $propertySizeValueId = $this->commonAttributesValuesForm->get('sizePropertyCommonAttribute_' . $i)->getLabelAttributes()['data-common-attribute-size-property-id'];
                    $this->commonAttributesValuesForm->get('sizePropertyCommonAttributeId_' . $i)->setValue($propertySizeValueId);
                }
            }

            /** Load modal for editing attribute with hydrated form */
            $editCommonAttributeValueModal = new ViewModel();
            $editCommonAttributeValueModal->setTerminal(true);
            $editCommonAttributeValueModal->setTemplate('prism-products-manager/common-attributes/partials/common-attribute-values-add-form');
            $editCommonAttributeValueModal->setVariable('commonAttributesValuesForm', $this->commonAttributesValuesForm);
            $editCommonAttributeValueModal->setVariable('commonAttributeId', $commonAttributeId);
            $editCommonAttributeValueModalOutput = $this->viewRenderer->render($editCommonAttributeValueModal);
            $editCommonAttributeValueModalOutput = utf8_encode($editCommonAttributeValueModalOutput);
            /** Return Response */
            $response = $this->getResponse();
            $response->setContent(json_encode(array('html' => $editCommonAttributeValueModalOutput)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }
        $response = $this->getResponse();
        $response->setStatusCode(401);
        $response->setContent('Unauthorized');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }
    /**
     * Method used to get the modal when editing a common attribute
     *
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function getCommonAttributeAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $commonAttributeId = $this->params()->fromPost('commonAttributeId', null);
        $page = $this->params()->fromPost('page', 1);

        $request = $this->getRequest();
        if ($request->isPost() && !is_null($commonAttributeId)) {
            $commonAttribute = $this->commonAttributesService->getCommonAttributeById($commonAttributeId);

            /** Hydrate form with data from doctrine entity */
            $this->commonAttributesForm->get('commonAttributeId')->setValue(
                $commonAttribute->getCommonAttributeId()
            );

            $this->commonAttributesForm->get('isEditable')->setValue($this->_isEditable($commonAttribute->getIsEditable()));
            $this->commonAttributesForm->get('description')->setValue(
                $commonAttribute->getCommonAttributeDefinitions()->getDescription()
            );
            if (!$commonAttribute->isEditable()) {
                $this->commonAttributesForm->get('description')->setAttribute('readonly', true);
            }

            if ($commonAttribute->getCommonAttributeGuiRepresentation() !== null){
                $this->commonAttributesForm->get('commonAttributeType')->setValue(
                    $commonAttribute->getCommonAttributeGuiRepresentation()->getCommonAttributesViewTypeId()->getCommonAttributeViewTypeId()
                );
                if (!$commonAttribute->isEditable()) {
                    $this->commonAttributesForm->get('commonAttributeType')->setAttribute('disabled', true);
                }
            }

            if ($commonAttribute->getCommonAttributeMapping() !== null) {
                $this->commonAttributesForm->get('mapping')->setValue(
                    $commonAttribute->getCommonAttributeMapping()->getCommonAttributeMapping()
                );
                if (!$commonAttribute->isEditable()) {
                    $this->commonAttributesForm->get('mapping')->setAttribute('readonly', true);
                }
            }

            $this->commonAttributesForm->get('usedForFiltering')->setValue(
                $commonAttribute->getUsedForFiltering()
            );
            $this->commonAttributesForm->get('usedForMerchantCategories')->setValue(
                $commonAttribute->getUsedForMerchantCategories()
            );
            $this->commonAttributesForm->get('isSize')->setValue(
                $commonAttribute->getIsSize()
            );
            //commonAttributeSizeProperty
            $relatedSizeProperty = $this->commonAttributesService->getCommonAttributesSizePropertyRelations($commonAttributeId);
            if(!empty($relatedSizeProperty)){
                $this->commonAttributesForm->get('commonAttributeSizeProperty')->setValue($relatedSizeProperty);
            }

            $this->commonAttributesForm->get('isSizeProperty')->setValue(
                $commonAttribute->getIsSizeProperty()
            );
            $this->commonAttributesForm->get('chained')->setValue(
                $commonAttribute->isChained()
            );
            $this->commonAttributesForm->get('period')->setValue(
                $commonAttribute->isPeriod()
            );

            $this->commonAttributesForm->get('keepHistory')->setValue(
                $commonAttribute->isKeepHistory()
            );

            $this->commonAttributesForm->get('autoFromDate')->setValue(
                $commonAttribute->isAutomaticFromDate()
            );

            $this->commonAttributesForm->get('isTicketByDefault')->setValue(
                $commonAttribute->isTicketByDefault()
            );
            $this->commonAttributesForm->get('commonAttributeDataElement')->setValue(
                $commonAttribute->getCommonAttributeDefinitions()->getCommonAttributeDataElement()
            );
            $this->commonAttributesForm->get('EditableOnLevel')->setValue(
                $commonAttribute->getEditableOnLevel()
            );
            /** Load modal for editing attribute with hydrated form */
            $editCommonAttributeModal = new ViewModel();
            $editCommonAttributeModal->setTerminal(true);
            $editCommonAttributeModal->setTemplate('prism-products-manager/common-attributes/partials/common-attribute-add-form');
            $editCommonAttributeModal->setVariable('page', $page);
            $editCommonAttributeModal->setVariable('onEditState', true);
            $editCommonAttributeModal->setVariable('commonAttributesForm', $this->commonAttributesForm);
            $editCommonAttributeModalOutput = $this->viewRenderer->render($editCommonAttributeModal);
            $editCommonAttributeModalOutput = utf8_encode($editCommonAttributeModalOutput);

            $response = $this->getResponse();
            //$response->setStatusCode(200);
            $response->setContent(json_encode(array('html' => $editCommonAttributeModalOutput)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }
        $response = $this->getResponse();
        $response->setStatusCode(401);
        $response->setContent('Unauthorized');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }

    protected function _isEditable($value = null)
    {
        if($value === false || $value === 0) {
            return 0;
        }

        return 1;
    }

    /**
     * Method used to change default values for common attributes values
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function changeDefaultValueAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $commonAttributeValueId = $this->params()->fromPost('commonAttributeValueId', null);
        $oldCommonAttributeValueId = $this->params()->fromPost('oldCommonAttributeValueId', null);

        $success = false;
        $request = $this->getRequest();
        if ($request->isPost() && !is_null($commonAttributeValueId) && !is_null($oldCommonAttributeValueId)) {
            $success = $this->commonAttributesService->changeDefaultForCommonAttributeValue($commonAttributeValueId, $oldCommonAttributeValueId);
        }
        $response = $this->getResponse();
        $response->setContent(json_encode(array('success' => $success)));
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Method used to retrieve equivalence modal
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function retrieveValueEquivalenceAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $commonAttributeValueId = $this->params()->fromPost('commonAttributeValueId', null);

        if (!is_null($commonAttributeValueId)) {
            /** Retrieve commonAttributeValueEntity  */
            $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($commonAttributeValueId);
            /** Loop commonAttributeValues and retrieve groups of value equivalence for $commonAttributeValueEntity */
            $equivalenceGroupValues = array();
            foreach ($commonAttributeValueEntity->getCommonAttributesValuesEquivalence() as $valueEquivalence) {
                $equivalenceGroups = $this->commonAttributesService->getEquivalenceByGroup($valueEquivalence->getUniqueGroupId());
                $equivalenceGroupValues[$valueEquivalence->getUniqueGroupId()] = $this->commonAttributesService->getCommonAttributeValuesByEquivalenceGroups($equivalenceGroups);
            }
            /** Load modal for editing attribute with hydrated form */
            $equivalenceGroupsModal = new ViewModel();
            $equivalenceGroupsModal->setTerminal(true);
            $equivalenceGroupsModal->setTemplate('prism-products-manager/common-attributes/partials/common-attribute-values-equivalence');
            $equivalenceGroupsModal->setVariable('equivalenceGroups', $equivalenceGroupValues);
            $equivalenceGroupsModal->setVariable('commonAttributeValueEntity', $commonAttributeValueEntity);
            $equivalenceGroupsModalOutput = $this->viewRenderer->render($equivalenceGroupsModal);
            $equivalenceGroupsModalOutput = utf8_encode($equivalenceGroupsModalOutput);

            $response = $this->getResponse();
            //$response->setStatusCode(200);
            $response->setContent(json_encode(array('html' => $equivalenceGroupsModalOutput)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }
        $response = $this->getResponse();
        $response->setStatusCode(400);
        $response->setContent('Bad Request');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }

    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function retrieveValueChainedAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $commonAttributeValueId = $this->params()->fromPost('commonAttributeValueId', null);
        if (!is_null($commonAttributeValueId)) {
            /** Retrieve commonAttributeValueEntity  */
            $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($commonAttributeValueId);
            /** Loop commonAttributeValues and retrieve groups of value equivalence for $commonAttributeValueEntity */
            $chainedValues = array();
            foreach ($commonAttributeValueEntity->getcommonAttributesSourceValuesChain() as $valueSource) {
                $destinationValue = $this->commonAttributesService->getCommonAttributeValueBy(
                    array('commonAttributeValueId' => $valueSource->getDestinationValueId()->getCommonAttributeValueId())
                );
                $chainedValues[] = $destinationValue;
            }
            /** Load modal for editing attribute with hydrated form */
            $chainGroupsModal = new ViewModel();
            $chainGroupsModal->setTerminal(true);
            $chainGroupsModal->setTemplate('prism-products-manager/common-attributes/partials/common-attribute-values-chain');
            $chainGroupsModal->setVariable('chainedValues', $chainedValues);
            $chainGroupsModal->setVariable('commonAttributeValueEntity', $commonAttributeValueEntity);
            $chainGroupsModalOutput = $this->viewRenderer->render($chainGroupsModal);
            $chainGroupsModalOutput = utf8_encode($chainGroupsModalOutput);

            $response = $this->getResponse();
            $response->setContent(json_encode(array('html' => $chainGroupsModalOutput)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }
        $response = $this->getResponse();
        $response->setStatusCode(400);
        $response->setContent('Bad Request');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }
    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function removeValueEquivalenceAction()
    {
        $this->isCmsUserAuthorized(array('delete'));
        $commonAttributeValueId = $this->params()->fromPost('commonAttributeValueId', null);
        $commonAttributeUniqueGroupId = $this->params()->fromPost('commonAttributeUniqueGroupId', null);
        $request = $this->getRequest();

        if ($request->isPost() && !is_null($commonAttributeValueId) && !is_null($commonAttributeUniqueGroupId)) {
            $actionFlag = $this->commonAttributesService->removeCommonAttributeValueFromEquivalenceGroup($commonAttributeValueId, $commonAttributeUniqueGroupId);
            $response = $this->getResponse();
            $response->setContent(json_encode(array('success' => $actionFlag)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }
        $response = $this->getResponse();
        $response->setStatusCode(400);
        $response->setContent('Bad Request');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }
    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function removeValueChainAction()
    {
        $this->isCmsUserAuthorized(array('delete'));
        $destinationValueId = $this->params()->fromPost('destinationValueId', null);
        $sourceValueId = $this->params()->fromPost('sourceValueId', null);
        $request = $this->getRequest();

        if ($request->isPost() && !is_null($destinationValueId) && !is_null($sourceValueId)) {
            $actionFlag = $this->commonAttributesService->removeChain($destinationValueId, $sourceValueId);
            $response = $this->getResponse();
            $response->setContent(json_encode(array('success' => $actionFlag)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }
        $response = $this->getResponse();
        $response->setStatusCode(400);
        $response->setContent('Bad Request');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }

    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function addChainValueAction()
    {
        $this->isCmsUserAuthorized(array('add'));
        $destinationValueId = $this->params()->fromPost('destinationValueId', null);
        $sourceValueId = $this->params()->fromPost('sourceValueId', null);
        $request = $this->getRequest();
        if ($request->isPost() && !is_null($destinationValueId) && !is_null($sourceValueId)) {
            $actionFlag = $this->commonAttributesService->addNewChainValue($destinationValueId, $sourceValueId);
            if ($actionFlag) {
                /** Retrieve commonAttributeValueEntity  */
                $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($sourceValueId);
                /** Loop commonAttributeValues and retrieve groups of value equivalence for $commonAttributeValueEntity */
                $chainedValues = array();
                foreach ($commonAttributeValueEntity->getcommonAttributesSourceValuesChain() as $valueSource) {
                    $destinationValue = $this->commonAttributesService->getCommonAttributeValueBy(
                        array('commonAttributeValueId' => $valueSource->getDestinationValueId()->getCommonAttributeValueId())
                    );
                    $chainedValues[] = $destinationValue;
                }
                /** Load modal for editing attribute with hydrated form */
                $chainGroupsModal = new ViewModel();
                $chainGroupsModal->setTerminal(true);
                $chainGroupsModal->setTemplate('prism-products-manager/common-attributes/partials/common-attribute-values-chain');
                $chainGroupsModal->setVariable('chainedValues', $chainedValues);
                $chainGroupsModal->setVariable('commonAttributeValueEntity', $commonAttributeValueEntity);
                $chainGroupsModalOutput = $this->viewRenderer->render($chainGroupsModal);
                $chainGroupsModalOutput = utf8_encode($chainGroupsModalOutput);

                $response = $this->getResponse();
                $response->setContent(json_encode(array('success' => $actionFlag, 'html' => $chainGroupsModalOutput)));
                $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
                return $response;
            }
        }
        $response = $this->getResponse();
        $response->setStatusCode(400);
        $response->setContent('Bad Request');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }
    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function searchCommonAttributesAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $searchString = $this->params()->fromPost('searchString', null);
        $request = $this->getRequest();

        if ($request->isPost() && !is_null($searchString)) {
            $searchResult = $this->commonAttributesService->searchThroughCommonAttributes($searchString);
            $response = $this->getResponse();
            $response->setContent(json_encode(array('search' => $searchResult)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }

        $response = $this->getResponse();
        $response->setStatusCode(400);
        $response->setContent('Bad Request');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }

    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function searchCommonAttributesValuesAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $searchString = $this->params()->fromPost('searchString', null);
        $commonAttributeId = $this->params()->fromPost('commonAttributeId', null);
        $request = $this->getRequest();

        if ($request->isPost() && !is_null($searchString) && !is_null($commonAttributeId)) {
            $commonAttributesValues = $this->commonAttributesService->searchThroughCommonAttributesValues($commonAttributeId, $searchString);
            $response = $this->getResponse();
            $response->setContent(json_encode(array('search' => $commonAttributesValues)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }
        $response = $this->getResponse();
        $response->setStatusCode(400);
        $response->setContent('Bad Request');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }

    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function createEquivalenceGroupAction()
    {
        $this->isCmsUserAuthorized(array('add'));
        $commonAttributeValues = $this->params()->fromPost('commonAttributeValues', null);
        $commonAttributeValueId = $this->params()->fromPost('commonAttributeValueId', null);

        $request = $this->getRequest();
        if ($request->isPost() && !empty($commonAttributeValues)) {
            $successFlag = $this->commonAttributesService->createNewCommonAttributeValueEquivalencesGroup($commonAttributeValues);
            if ($successFlag) {
                /** Retrieve commonAttributeValueEntity  */
                $commonAttributeValueEntity = $this->commonAttributesService->getCommonAttributeValueById($commonAttributeValueId);
                /** Loop commonAttributeValues and retrieve groups of value equivalence for $commonAttributeValueEntity */
                $equivalenceGroupValues = array();
                foreach ($commonAttributeValueEntity->getCommonAttributesValuesEquivalence() as $valueEquivalence) {
                    $equivalenceGroups = $this->commonAttributesService->getEquivalenceByGroup($valueEquivalence->getUniqueGroupId());
                    $equivalenceGroupValues[$valueEquivalence->getUniqueGroupId()] = $this->commonAttributesService->getCommonAttributeValuesByEquivalenceGroups($equivalenceGroups);
                }
                /** Load modal for editing attribute with hydrated form */
                $equivalenceGroupsModal = new ViewModel();
                $equivalenceGroupsModal->setTerminal(true);
                $equivalenceGroupsModal->setTemplate('prism-products-manager/common-attributes/partials/common-attribute-values-equivalence');
                $equivalenceGroupsModal->setVariable('equivalenceGroups', $equivalenceGroupValues);
                $equivalenceGroupsModal->setVariable('commonAttributeValueEntity', $commonAttributeValueEntity);
                $equivalenceGroupsModalOutput = $this->viewRenderer->render($equivalenceGroupsModal);
                $equivalenceGroupsModalOutput = utf8_encode($equivalenceGroupsModalOutput);

                $response = $this->getResponse();
                //$response->setStatusCode(200);
                $response->setContent(json_encode(array('html' => $equivalenceGroupsModalOutput)));
                $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
                return $response;
            }
        }
        $response = $this->getResponse();
        $response->setStatusCode(400);
        $response->setContent('Bad Request');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }

    /**
     * Method used to change the enable status for a common attribute value
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function changeStatusValueAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $commonAttributeValueId = $this->params()->fromPost('commonAttributeValueId', null);
        $newStatus = $this->params()->fromPost('newStatus', null);

        $success = false;
        $request = $this->getRequest();
        if ($request->isPost() && !is_null($commonAttributeValueId) && !is_null($newStatus)) {
            $success = $this->commonAttributesService->changeCommonAttributeStatus($commonAttributeValueId, $newStatus);
        }
        $response = $this->getResponse();
        $response->setContent(json_encode(array('success' => $success)));
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        return $response;
    }
    /**
     * @return bool
     */
    private function isFeatureEnabled()
    {
        if (isset($this->clientConfig['features']['commonAttributes']) &&
            $this->clientConfig['features']['commonAttributes']
        ) {
            return true;
        }
        return false;
    }

    /**
     * Redirect in case of error
     * @param $route
     * @param $type
     * @param $message
     *
     * @param string $alert
     * @return null
     */
    protected function _errorValue($route, $type, $message, $alert = [])
    {
        $alert['type'] = $type;
        $alert['bold'] = ucfirst($type).':';
        $alert['messages'] = array();
        $alert['messages'][0] = $message;

        $this->flashmessenger()->setNamespace('alert')
            ->addMessage($alert);

        return $this->redirect()->toRoute(null ,$route);
    }

    /**
     * @param string $message
     * @param int $page
     * @param string $action
     * @param string $controller
     * @param string $type
     * @param $commonAttribute
     * @return null
     */
    private function redirectToMainPage(
        $message = '',
        $page = 1,
        $action = 'index',
        $controller = 'CommonAttributes',
        $type = 'error',
        $commonAttribute = null
    )
    {
        /**
         * Redirect to main page with message.
         * */
        $route = array(
            'controller' => $controller,
            'action' => $action,
            'page' => $page
        );
        if (!is_null($commonAttribute)) {
            $route['attributeId'] = $commonAttribute;
        }
        return $this->_errorValue($route, $type, $message);
    }
    /**
     * Method used to check if user is administrator
     * Only administrators should have access to common attributes if config flag is set up
     * @return bool
     */
    private function checkIfUserIsAuthorized()
    {
        if ($this->clientConfig['settings']['commonAttributesForAdmin']) {
            $groupDetails = $this->getCurrentUser();
            if (!empty($groupDetails)) {
                if ($groupDetails->groupName == 'Administrator') {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

}
