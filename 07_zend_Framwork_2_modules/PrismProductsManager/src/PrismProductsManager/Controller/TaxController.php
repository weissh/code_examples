<?php

namespace PrismProductsManager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;
use PrismProductsManager\Model\TaxMapper;
use PrismProductsManager\Form\AddNewTaxForm;

class TaxController extends AbstractActionController
{
    
    protected $_taxMapper;
    
    protected $_addNewTaxForm;

    public function __construct(TaxMapper $taxMapper, AddNewTaxForm $addNewTaxForm, $languagesConfig)
    {
        $this->_taxMapper = $taxMapper;
        $this->_addNewTaxForm = $addNewTaxForm;
        $this->_currentLanguage = $languagesConfig['current-language'];
    }    

    public function indexAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $matches = $this->getEvent()->getRouteMatch();
        $page = $matches->getParam('page', 1);
        $addNewTaxForm = $this->_addNewTaxForm;

        $taxList = $this->_taxMapper->getTaxLists($this->_currentLanguage);
        $paginator = new Paginator(
            new ArrayAdapter($taxList)
        );
        $paginator->setCurrentPageNumber($page);
        return new ViewModel(array(
            'taxList' => $taxList,
            'paginator' => $paginator,
            'addNewTaxForm' => $addNewTaxForm,
            'search' => ''
        ));
    }

    public function addAction()
    {
        $request = $this->getRequest();
        $submitedFormData = $this->params()->fromPost();
        $addNewTaxForm = $this->_addNewTaxForm;

        $route = array(
            'controller' => 'Tax',
            'action' => 'index',
        );

        if ($request->isPost()) {
            $addNewTaxForm->setData($submitedFormData);
            if ($addNewTaxForm->isValid()) {
                $validatedData = $addNewTaxForm->getData();
                $id = !empty($validatedData['id']) ? $validatedData['id'] : 0;
                //checking permissions
                if ($id == 0) {
                    $this->isCmsUserAuthorized(array('add'));
                } else {
                    $this->isCmsUserAuthorized(array('edit'));
                }
                if (!$this->_taxMapper->saveTax($validatedData, $id, $this->_currentLanguage)) {
                    $type = 'error';
                    $message = 'Could not save the tax!';
                } else {
                    $type = 'success';
                    $message = 'Taxed saved successfully!';
                }

            } else {
                $type = 'error';
                $message = 'Invalid values specified in the form!';
            }
        } else {
            $type = 'error';
            $message = 'Invalid request type!';
        }
        $this->_errorValue($route, $type, $message);
    }

    public function deactivateAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $matches = $this->getEvent()->getRouteMatch();
        $id = $matches->getParam('id', 0);
        $route = array(
            'controller'=>'Tax',
            'action' => 'index',
        );
        if($id == 0) {
            $type = 'error';
            $message = 'Entry id not specified!';
        } else {
            if (!$this->_taxMapper->toggleTax($id, true)) {
                $type = 'error';
                $message = 'Could not deactivate the tax!';
            } else {
                $type = 'success';
                $message = 'Taxed deactivated successfully!';
            }
        }
        $this->_errorValue($route, $type, $message);
    }

    public function activateAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $matches = $this->getEvent()->getRouteMatch();
        $id = $matches->getParam('id', 0);
        $route = array(
            'controller'=>'Tax',
            'action' => 'index',
        );
        if($id == 0) {
            $type = 'error';
            $message = 'Entry id not specified!';
        } else {
            if (!$this->_taxMapper->toggleTax($id, false)) {
                $type = 'error';
                $message = 'Could not activate the tax!';
            } else {
                $type = 'success';
                $message = 'Taxed activated successfully!';
            }
        }
        $this->_errorValue($route, $type, $message);
    }

    public function deleteAction()
    {
        $this->isCmsUserAuthorized(array('delete'));
        $matches = $this->getEvent()->getRouteMatch();
        $id = $matches->getParam('id', 0);
        $route = array(
            'controller'=>'Tax',
            'action' => 'index',
        );
        if($id == 0) {
            $type = 'error';
            $message = 'Entry id not specified!';
        } else {
            if (!$this->_taxMapper->deleteTax($id)) {
                $type = 'error';
                $message = 'Could not delete the tax!';
            } else {
                $type = 'success';
                $message = 'Taxed deleted successfully!';
            }
        }
        $this->_errorValue($route, $type, $message);
    }

    /**
     * Redirect in case of error
     * @param $route
     * @param $type
     * @param $message
     *
     * @param string $alert
     * @return null
     */
    protected function _errorValue($route, $type, $message, $alert = [])
    {
        $alert['type'] = $type;
        $alert['bold'] = ucfirst($type).':';
        $alert['messages'] = array();
        $alert['messages'][0] = $message;
        $this->flashmessenger()->setNamespace('alert')
            ->addMessage($alert);

        return $this->redirect()->toRoute(null ,$route);
    }


}

