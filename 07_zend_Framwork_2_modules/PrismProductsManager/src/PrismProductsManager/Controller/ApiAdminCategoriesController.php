<?php
namespace PrismProductsManager\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 * ApiProductsController
 *
 * @author
 *
 *
 * @version
 *
 *
 */
class ApiAdminCategoriesController extends AbstractRestfulController
{
    protected $_categoriesTable;
    
    public function get($id)
    {
        $productData = array(
            'id' => $id,
            'productName' => 'White Shoos',
            'productDescription' => 'Text about products that is more then 255 characters'
        );
        // $productData = FALSE;
        
        // $this->getServiceLocator()->get('blablabla');
        
        if ($productData != FALSE) {
            return new JsonModel($productData);
        } else {
            throw new \Exception('Product not found', 400);
        }
    }

    public function getList()
    {
        $this->_methodNotAllowed();
    }

    public function create($data)
    {
        $categories = $this->getCategoriesTable()->fetchAll();
        return new JsonModel(array(
            'categories' => $categories,
        ));
        
        /*$request = $this->getRequest();
        $response = $this->getResponse();
        $post_data = array();
        if ($request->isPost()) {
            $post_data = $request->getPost();
            //$categoriesData = $this->_getCategoriesTable()->saveCategory($post_data);
            $categoriesData = $this->_getCategoriesTable()->getCategories();
        }
        if($categoriesData)
            $result = array('success' => true, 'Message' => 'Product was created successfully');
            print_r($categoriesData); exit;
        return new JsonModel($categoriesData);*/
    }

    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    
    public function getCategoriesTable()
    {
        if (!$this->_categoriesTable) {
            $sm = $this->getServiceLocator();
            $this->_categoriesTable = $sm->get('PrismProductsManager-Model-CategoriesTable');
        }
        return $this->_categoriesTable;
    }
    
    /**
     * Inject the categories table to be used.
     * @return type
     */
    protected function _getCategoriesTable()
    {
        if (!$this->_categoriesTable) {
            $sm = $this->getServiceLocator();
            //$this->_categoriesTable = $sm->get('PrismProductsManager\Model\CategoriesTable');
            $this->_categoriesTable = $sm->get('ProductCategoriesMapper');
        }
        return $this->_categoriesTable;
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}