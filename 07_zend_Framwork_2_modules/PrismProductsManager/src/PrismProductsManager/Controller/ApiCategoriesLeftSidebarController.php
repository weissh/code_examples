<?php
namespace PrismProductsManager\Controller;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use PrismProductsManager\Model\ApiCategoriesMapper;


/**
 * Api Categories by Id Controller
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiCategoriesLeftSidebarController extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * Api Categories Mapper
     * PrismProductsManager\Model\ApiCategoriesMapper
     * @var object
     */
    protected $_apiCategoriesMapper;

    protected $_categoriesMapper;

    public function __construct(ApiCategoriesMapper $apiCategoriesMapper, $categoriesMapper)
    {

        $this->_apiCategoriesMapper = $apiCategoriesMapper;
        $this->_categoriesMapper = $categoriesMapper;
    }
    public function get($id)
    {
        $websiteId = $this->params()->fromRoute('website');
        $languageId = $this->params()->fromRoute('language');
        $categoryDataForSidebar = array();
        $categoryDataContent = array();
        $validator = new \Zend\Validator\NotEmpty(\Zend\Validator\NotEmpty::INTEGER);
        if ($validator->isValid($id) && $validator->isValid($websiteId) && $validator->isValid($languageId)) {
            $categoryDataForSidebar =  $this->_categoriesMapper->getMarketingCategoryForSideBar($id, $websiteId, $languageId);
            $categoryDataContent =  $this->_apiCategoriesMapper->getCategoryByName($id, $websiteId, $languageId);
        }

        if (!empty($categoryDataContent)) {
            return new JsonModel(array('categoryDataForSidebar' => $categoryDataForSidebar, 'categoryDataContent' => $categoryDataContent ));
        } else {
            throw new \Exception('Product not found', 400);
        }
    }

    public function getList()
    {
        $this->_methodNotAllowed();
    }

    public function create($data)
    {
        $this->_methodNotAllowed();
    }

    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}