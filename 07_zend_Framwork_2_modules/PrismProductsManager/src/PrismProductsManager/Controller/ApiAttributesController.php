<?php
namespace PrismProductsManager\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 * ApiProductsController
 *
 * @author
 *
 *
 * @version
 *
 *
 */
class ApiAttributesController extends AbstractRestfulController
{

    public function get($id)
    {
        $productData = array(
            'id' => $id,
            'productName' => 'White Shoos',
            'productDescription' => 'Text about products that is more then 255 characters'
        );
        // $productData = FALSE;
        
        // $this->getServiceLocator()->get('blablabla');
        
        if ($productData != FALSE) {
            return new JsonModel($productData);
        } else {
            throw new \Exception('Product not found', 400);
        }
    }

    public function getList()
    {
        $this->_methodNotAllowed();
    }

    public function create($data)
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $post_data = array();
        if ($request->isPost()) {
            $post_data = $request->getPost();
        }
        $result = array('success' => true, 'Message' => 'Product was created successfully');
            
        return new JsonModel($post_data);
    }

    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}