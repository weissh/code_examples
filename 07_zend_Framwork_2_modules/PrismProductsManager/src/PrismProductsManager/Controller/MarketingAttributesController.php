<?php

namespace PrismProductsManager\Controller;

use PrismElasticSearch\Enum\ElasticSearchTypes;
use PrismProductsManager\Model\AttributesGroupMapper;
use PrismQueueManager\Service\QueueService;
use Zend\Mvc\Controller\AbstractActionController;
use PrismProductsManager\Form\AddAttributesMarketingGroupForm;
use PrismProductsManager\Form\AddAttributesMarketingForm;
use PrismProductsManager\Form\SearchForm;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;

/**
 * Class MarketingAttributesController
 * @package PrismProductsManager\Controller
 */
class MarketingAttributesController extends AbstractActionController
{
    /**
     *  inject the attributes marketing group form
     *  @var object
     */
    protected $_addAttributesMarketingGroupForm;
    /**
     *  inject the attributes marketing values form
     *  @var object
     */
    protected $_addAttributesMarketingForm;
    /**
     *  inject the search form
     *  @var object
     */
    protected $_searchForm;
    /**
     *  used to check if form (add/edit) is _valid
     *  @var boolean
     */
    protected $_valid;
    /**
     *  @var Object
     *  Mappers for module/PrismProductsManager/src/PrismProductsManager/Model/AttributesGroupMapper.php
     */
    protected $_attributesMapper;
    /**
     *  @var
     */
    protected $_colours;

    protected $_simplePagination;
    protected $_queueService;
    protected $_productsMapper;

    /**
     * @param AddAttributesMarketingGroupForm $addAttributesMarketingGroupForm
     * @param AddAttributesMarketingForm $addAttributesMarketingForm
     * @param \PrismProductsManager\Model\AttributesGroupMapper $attributesMapper
     * @param \PrismProductsManager\Form\SearchForm $searchForm
     */
    public function __construct(
        AddAttributesMarketingGroupForm $addAttributesMarketingGroupForm, AddAttributesMarketingForm $addAttributesMarketingForm,
        AttributesGroupMapper $attributesMapper, SearchForm $searchForm, $simplePagination,
        QueueService $queueService, $productsMapper
    )
    {
        $this->_addAttributesMarketingGroupForm = $addAttributesMarketingGroupForm;
        $this->_addAttributesMarketingForm = $addAttributesMarketingForm;
        $this->_attributesMapper = $attributesMapper;
        $this->_searchForm = $searchForm;
        $this->_simplePagination = $simplePagination;
        $this->_queueService = $queueService;
        $this->_productsMapper = $productsMapper;
    }
    /**
     *  Attributes Groups management
     *  @return ViewModel
     */
    public function indexAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $addAttributeMarketingGroupForm = $this->_addAttributesMarketingGroupForm;
        $addAttributeMarketingGroupForm->get('submit')->setValue('Save');

        $addAttributeMarketingForm = $this->_addAttributesMarketingForm;
        $addAttributeMarketingForm->get('submit')->setValue('Save');

        $request = $this->getRequest();

        $orderBy = $this->params()->fromRoute('orderby');
        $orderbyOption = $this->params()->fromRoute('orderbyoption');

        //Set the values for the $_valid to false
        //If true Popup opens on page load
        $this->_valid['MarketingAdd'] = false;
        $this->_valid['MarketingEdit'] = false;
        $submitedData = $request->getPost();

        if ($request->isPost()) {

            // In case of search redirect to search method
            if (isset($submitedData->search)) {
                $route = array(
                    'controller' => 'MarketingAttributes',
                    'action' => 'index',
                );
                // Set data for form and check if the search form is valid
                $this->_searchForm->setData($submitedData);
                // Check if search form is valid
                if ($this->_searchForm->isValid()) {
                    $search = $this->_searchForm->getData();
                    $type = 'success';
                    $message = 'Search results for ' . $search['search'];
                    $route['search'] = urlencode($search['search']);
                } else {
                    $type = 'error';
                    $message = 'Cannot find any results.';
                }

                //Redirect based on the message and route
                $this->_errorValue($route, $type, $message);
            }

            //Add data to form to be used on the validation
            $addAttributeMarketingGroupForm->setData($submitedData);
            //Check's if form is valid
            //Check if request is for add or for edit
            if ($addAttributeMarketingGroupForm->isvalid()) {

                $addAttributeMarketingGroupForm->formData = $addAttributeMarketingGroupForm->getData();
                $attributesTable  = $this->_attributesMapper;

                if ($submitedData['attributeId'] == '') {
                    $this->isCmsUserAuthorized(array('add'));
                    $id = $attributesTable->saveAttributeGroup($addAttributeMarketingGroupForm->getData());
                    //Set Route for the redirect
                    $route = array(
                        'controller'=>'Attributes',
                        'action' => 'values',
                        'id' => $id
                    );
                    $this->_errorValue($route, 'success', 'The Group has been added.');
                } else {
                    $this->isCmsUserAuthorized(array('edit'));
                    if ($attributesTable->updateAttributeGroup($addAttributeMarketingGroupForm->getData())) {
                        // get all products that have this marketing attribute set and re-index them
                        $productsIdsArray = $this->_attributesMapper->getAllProductsWithThisAttribute($submitedData['attributeId']);
                        foreach ($productsIdsArray as $productId) {
                            $this->_queueService->addIndexEntryToQueue($productId.'_0', ElasticSearchTypes::PRODUCTS);
                        }
                        //Set Route for the redirect
                        $route = array(
                            'controller'=>'MarketingAttributes',
                            'action' => 'values',
                            'id' => $submitedData['attributeId']
                        );
                        $this->_errorValue($route, 'success', 'The Group has been modified.');
                        $this->_valid['MarketingEdit'] = false;
                    } else {
                        $route = array(
                            'controller'=>'MarketingAttributes',
                            'action' => 'index',
                        );
                        $this->_errorValue($route, 'error', 'You cannot change the group type from multiple values to single text value.');
                    }
                }
            } else {
                if ($submitedData['attributeId'] == '') {
                    $this->_valid['MarketingAdd'] = true;
                } else {
                    $this->_valid['MarketingEdit'] = true;
                }
            }
        }
        $search = $this->params()->fromRoute('search');
        // If the request if for search get the groups using search params
        if (isset($search) && !empty($search)) {
            //Get Groups with all children's from search
            $groups = $this->_attributesMapper->fetchAllMarketingAttributes(urldecode($search), $orderBy, $orderbyOption);
            // If search results are empty redirect to main page without search
            if (empty($groups)) {
                $route = array(
                    'controller' => 'MarketingAttributes',
                    'action' => 'index',
                );
                $type = 'error';
                $message = 'Cannot find any results.';
                $this->_errorValue($route, $type, $message);
            }
        } else {
            //Get Groups with all children's
            $groups = $this->_attributesMapper->fetchAllMarketingAttributes($search = null, $orderBy, $orderbyOption);
        }

        //Get Marketing Groups used to populate the Attribute Parent Select
        $parentGroups = $this->_attributesMapper->fetchAll(true, array('attributeGroupId','name'));
        $finalParentArray = array(0 => 'ROOT');
        foreach ($parentGroups as $options) {
            $finalParentArray[$options['attributeGroupId']] = $options['name'];
        }
        $addAttributeMarketingGroupForm->get('attributeGroupParent')->setValueOptions($finalParentArray);

        //Set up the pagination
        $matches = $this->getEvent()->getRouteMatch();
        $route = $this->_simplePagination->getRoute($matches);
        $page = $matches->getParam('page', 1);

        $paginator = $this->_simplePagination->load($matches, $groups, $nrPerPage = 8);

        return new ViewModel(array(
            'attributesGroupAddForm' => $addAttributeMarketingGroupForm,
            'attributesAddForm' => $addAttributeMarketingForm,
            'paginator' => $paginator,
            'search' => $search,
            'valid' => $this->_valid,
            'searchForm' => $this->_searchForm,
            'route' => $route,
            'orderby' => $orderBy,
            'orderbyOption' => $orderbyOption,
        ));
    }

    /**
     *  Used for Group attributes management page
     *  @return ViewModel
     */
    public function valuesAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        //Get and set the attribute values add form
        $addAttributeMarketingForm = $this->_addAttributesMarketingForm;
        $addAttributeMarketingForm->get('submit')->setValue('Save');

        $id = $this->params()->fromRoute('id');

        $request = $this->getRequest();
        $submitedformData = $this->params()->fromPost();

        $valid['add'] = false;
        $valid['edit'] = false;

        //Saves the attribute in the specific group or throws an error
        //Redirects to Groups management page if attribute added from there or
        //to specific group attributes management page.
        if ($request->isPost()) {

            $addAttributeMarketingForm->setData($submitedformData);
            if (!empty($submitedformData['attributeValueId']) && !empty($submitedformData['attributeGroupId'])) {
                $this->isCmsUserAuthorized(array('edit'));
            } else {
                $this->isCmsUserAuthorized(array('add'));
            }
            if ($addAttributeMarketingForm->isvalid()) {

                if (!$this->_attributesMapper->saveAttributeValues($addAttributeMarketingForm->getData(), $id)) {
                    $route = array(
                        'controller'=>'Attributes',
                        'action' => 'index',
                    );
                } else {
                    $route = array(
                        'controller'=>'Attributes',
                        'action' => 'values',
                        'id' => $id
                    );
                }
                $type = 'success';
                if (!empty($submitedformData['attributeValueId']) && !empty($submitedformData['attributeGroupId'])) {
                    $message = 'Attribute values was modified.';
                } else {
                    $message = 'Attribute values was added.';
                }
            } else {
                if ($id === null) {
                    $route = array(
                        'controller'=>'Attributes',
                        'action' => 'index'
                    );
                } else {
                    $route = array(
                        'controller'=>'Attributes',
                        'action' => 'values',
                        'id' => $id
                    );
                }
                $type = 'error';
                $message = $addAttributeMarketingForm->getMessages();
                $message = array_shift($message['attributeValue']);
            }
            $this->_errorValue($route, $type, $message);
        }

        //Get the Group with attributes
        //Redirects if no groups if found in database
        $group = $this->_attributesMapper->getGroupById($id);

        //Throw error if the group is invalid
        if ($id === null || empty($group)) {
            $this->_errorValue('attributes', 'error', 'Group was not found in the database');
        }

        //Return to view variables
        return new ViewModel(array(
            'attributesAddForm' => $this->_addAttributesMarketingForm,
            'group' => $group,
            'valid' => $valid,
            'id' => $id,
        ));
    }

    /**
     * Used to delete Groups or Attributes
     * @return null
     */
    public function deleteAction()
    {
        $this->isCmsUserAuthorized(array('delete'));
        //Check's if delete came from Group management page or
        //attributes management page and set the route for later redirect.
        if ($this->params()->fromRoute('delete') == 'group') {
            $route = array(
                'controller'=>'Attributes',
                'action' => 'index',
            );
        } else {
            $route = array(
                'controller'=>'Attributes',
                'action' => 'values',
                'id' => $this->params()->fromRoute('from')
            );
        }

        //Deletes the Attribute or Group
        //Set the Type ( error / success )
        //Set the message for the redirect
        if ($this->_attributesMapper->delete($this->params()->fromRoute())) {
            if ($this->params()->fromRoute('type') == 'group') {
                $message = 'The group with all the attribute values has been deleted.';
            } else {
                $message = 'The attribute value has been deleted.';
            }
            $type = 'success';
        } else {
            if ($this->params()->fromRoute('type') == 'group') {
                $message = 'This group has attributes used in product variants and therefore cannot be deleted.';
            } else {
                $message = 'The attribute is used in product variants and therefore cannot be deleted. ' . __LINE__;
            }
            $type = 'error';
        }

        //Redirect based on $route, $type, $message
        $this->_errorValue($route, $type, $message);
    }

    public function merchantAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $group = $this->_attributesMapper->getGroupSourceTable($this->params()->fromRoute('id'));

        return new ViewModel(array(
            'group' => $group
        ));
    }
    /**
     * Redirect in case of error
     * @param $route
     * @param $type
     * @param $message
     *
     * @param string $alert
     * @return null
     */
    protected function _errorValue($route, $type, $message, $alert = [])
    {
        $alert['type'] = $type;
        $alert['bold'] = ucfirst($type).':';
        $alert['messages'] = array();
        $alert['messages'][0] = $message;

        $this->flashmessenger()->setNamespace('alert')
            ->addMessage($alert);

        return $this->redirect()->toRoute(null ,$route);
    }

    /**
     * Ajax function used for enableing colours
     * Set 1/0 in the COLOURS Table
     */
    public function enableColourAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $params = $this->params()->fromPost();
        $this->_attributesMapper->enableColours($params);

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/json');

        return $response;
    }
    /**
     *
     */
    public function setFilterAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $params = $this->params()->fromPost();

        $this->_attributesMapper->setFilter($params);

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $response->setContent('');
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/json');

        return $response;
    }

    public function activateAttributeAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $params = $this->params()->fromRoute();
        if ($this->_attributesMapper->activateAttributeValue($params['id'])){
            $message = 'The attribute value has been deactivated.';
        } else {
            $message = 'The attribute value has been activated.';
        }
        $route = array(
            'controller'=>'Attributes',
            'action' => 'values',
            'id' => $params['from']
        );

        $type = 'success';

        $this->_errorValue($route, $type, $message);
    }
}