<?php
namespace PrismProductsManager\Controller;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use PrismProductsManager\Model\ApiCategoriesMapper;


/**
 * Api Categories by Id Controller
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ApiCategoriesByIdController extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * Api Categories Mapper
     * PrismProductsManager\Model\ApiCategoriesMapper
     * @var object
     */
    protected $_apiCategoriesMapper;

    public function __construct(ApiCategoriesMapper $apiCategoriesMapper)
    {
        $this->_apiCategoriesMapper = $apiCategoriesMapper;

    }
    public function get($id)
    {

        $websiteId = $this->params()->fromRoute('website');
        $languageId = $this->params()->fromRoute('language');
        $categoryData = array();
        $validator = new \Zend\Validator\NotEmpty(\Zend\Validator\NotEmpty::INTEGER);
        if ($validator->isValid($id) && $validator->isValid($websiteId) && $validator->isValid($languageId)) {
            $categoryData =  $this->_apiCategoriesMapper->getCategoryById($id, $websiteId, $languageId);
        }

        if (!empty($categoryData)) {
            return new JsonModel($categoryData);
        } else {
            throw new \Exception('Product not found', 400);
        }
    }

    public function getList()
    {
        $this->_methodNotAllowed();
    }

    public function create($data)
    {
        $this->_methodNotAllowed();
    }

    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}