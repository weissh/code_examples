<?php

namespace PrismProductsManager\Controller\RestfulAPI;

use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as v;
use Zend\Http\Headers;
use Zend\Http\PhpEnvironment\Response as ResponseStatus;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Mvc\Exception\DomainException;
use Zend\Mvc\Exception\RuntimeException;
use Zend\Mvc\MvcEvent;

class AbstractApiController extends AbstractRestfulController
{
    /**
     * @var string
     */
    protected $sharedSecret;

    /**
     * WebCategoryAPIController constructor.
     *
     * @param string $sharedSecret
     */
    public function __construct($sharedSecret)
    {
        $this->sharedSecret = $sharedSecret;
    }

    /**
     * @param mixed               $id
     * @param array|object|string $request
     *
     * @return array
     */
    public function delete($id, $request)
    {
        $this->response->setStatusCode(405);

        return [
            'content' => 'Method Not Allowed',
        ];
    }

    /**
     * @param \Zend\Mvc\MvcEvent $e
     *
     * @return array|mixed|\Zend\Http\Response|\Zend\Stdlib\ResponseInterface
     */
    public function onDispatch(MvcEvent $e)
    {
        $routeMatch = $e->getRouteMatch();
        if (!$routeMatch) {
            /**
             * @todo Determine requirements for when route match is missing.
             *       Potentially allow pulling directly from request metadata?
             */
            throw new DomainException('Missing route matches; unsure how to retrieve action');
        }

        $request = $e->getRequest();

        // Was an "action" requested?
        $action = $routeMatch->getParam('action', false);
        if ($action) {
            // Handle arbitrary methods, ending in Action
            $method = static::getMethodFromAction($action);
            if (!method_exists($this, $method)) {
                $method = 'notFoundAction';
            }
            $return = $this->$method();
            $e->setResult($return);

            return $return;
        }

        try {

            // RESTful methods
            $method = strtolower($request->getMethod());
            switch ($method) {
                // Custom HTTP methods (or custom overrides for standard methods)
                case (isset($this->customHttpMethodsMap[$method])):
                    $callable = $this->customHttpMethodsMap[$method];
                    $action = $method;
                    $return = call_user_func($callable, $e);
                    break;
                // DELETE
                case 'delete':
                    $id = $this->getIdentifier($routeMatch, $request);
                    $data = $this->processBodyContent($request);

                    if ($id !== false) {
                        $action = 'delete';
                        $return = $this->delete($id, $data);
                        break;
                    }

                    $action = 'deleteList';
                    $return = $this->deleteList($data);
                    break;
                // GET
                case 'get':
                    $id = $this->getIdentifier($routeMatch, $request);
                    if ($id !== false) {
                        $action = 'get';
                        $return = $this->get($id);
                        break;
                    }
                    $action = 'getList';
                    $return = $this->getList();
                    break;
                // HEAD
                case 'head':
                    $id = $this->getIdentifier($routeMatch, $request);
                    if ($id === false) {
                        $id = null;
                    }
                    $action = 'head';
                    $headResult = $this->head($id);
                    $response = ($headResult instanceof Response) ? clone $headResult : $e->getResponse();
                    $response->setContent('');
                    $return = $response;
                    break;
                // OPTIONS
                case 'options':
                    $action = 'options';
                    $this->options();
                    $return = $e->getResponse();
                    break;
                // PATCH
                case 'patch':
                    $id = $this->getIdentifier($routeMatch, $request);
                    $data = $this->processBodyContent($request);

                    if ($id !== false) {
                        $action = 'patch';
                        $return = $this->patch($id, $data);
                        break;
                    }

                    // TODO: This try-catch should be removed in the future, but it
                    // will create a BC break for pre-2.2.0 apps that expect a 405
                    // instead of going to patchList
                    try {
                        $action = 'patchList';
                        $return = $this->patchList($data);
                    } catch (RuntimeException $ex) {
                        $response = $e->getResponse();
                        $response->setStatusCode(405);

                        return $response;
                    }
                    break;
                // POST
                case 'post':
                    $action = 'create';
                    $return = $this->processPostData($request);
                    break;
                // PUT
                case 'put':
                    $id = $this->getIdentifier($routeMatch, $request);
                    $data = $this->processBodyContent($request);

                    if ($id !== false) {
                        $action = 'update';
                        $return = $this->update($id, $data);
                        break;
                    }

                    $action = 'replaceList';
                    $return = $this->replaceList($data);
                    break;
                // All others...
                default:
                    $response = $e->getResponse();
                    $response->setStatusCode(405);

                    return $response;
            }
        } catch (NestedValidationException $nve) {
            return $this->_jsonResponse(['error' => true, 'message' => $nve->getMessages()], ResponseStatus::STATUS_CODE_400);
        } catch (\Exception $ex) {
            return $this->_jsonResponse(['error' => true, 'message' => $ex->getMessage()], ResponseStatus::STATUS_CODE_500);
        }

        $routeMatch->setParam('action', $action);
        $e->setResult($return);

        return $return;
    }

    /**
     * @param array|null $data
     * @param int        $statusCode
     *
     * @return \Zend\Http\Response
     */
    protected function _jsonResponse($data = null, $statusCode = ResponseStatus::STATUS_CODE_200)
    {
        $response = new Response();
        $response->setStatusCode($statusCode);
        $response->setHeaders((new Headers())->addHeaderLine('Content-Type', 'application/json'));
        if ($data) {
            $response->setContent(json_encode($data));
        }

        return $response;
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    protected function _isAuthorized(array $data)
    {
        $sharedSecret = !empty($data['sharedSecret']) ? $data['sharedSecret'] : null;

        return $sharedSecret === $this->sharedSecret;
    }

    /**
     * @param string $message
     *
     * @return \Zend\Http\Response
     */
    protected function _jsonNotFound($message)
    {
        return $this->_jsonResponse([
            'message' => $message,
            'error'   => true,
        ], ResponseStatus::STATUS_CODE_404);
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    protected function _getParams($data)
    {
        $validator = v::allOf(
            v::arrayVal(),
            v::key('requestParams', v::arrayVal(), true)
        );

        $validator->assert($data);

        return $data['requestParams'];
    }
}
