<?php
namespace PrismProductsManager\Controller\RestfulAPI;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use PrismProductsManager\Model\ProductsMapper;
use PrismProductsManager\Service\ScheduleService;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ProductPriceAPIController extends AbstractRestfulController implements ControllerApiAwareInterface
{

    /**
     * @var ProductsMapper
     */
    protected $productMapper;
    /**
     * @var array
     */
    protected $config;
    /**
     * @var ScheduleService
     */
    protected $scheduleService;
    /**
     * @param array $config
     * @param ProductsMapper $productMapper
     * @param ScheduleService $scheduleService
     */
    public function __construct(
        array $config,
        ProductsMapper $productMapper,
        ScheduleService $scheduleService
    )
    {
        $this->config = $config;
        $this->productMapper = $productMapper;
        $this->scheduleService = $scheduleService;
    }

    /**
     * @param mixed $id
     * @return array|mixed|void
     * @throws \Exception
     */
    public function get($id)
    {
        return $this->methodNotAllowed();
    }

    /**
     * @throws \Exception
     */
    public function getList()
    {
        return $this->methodNotAllowed();
    }

    /**
     * @param mixed $data
     * @return void|Response
     */
    public function create($data)
    {
        try {
            $sharedSecret = !empty($data['sharedSecret']) ? $data['sharedSecret'] : '';
            $variantIds = !empty($data['variantIds']) ? $data['variantIds'] : array();

            /** 418 - I am a teapot !! */
            $teapost = isset($data['teapot']) && is_bool($data['teapot']) ? $data['teapot'] : false;
            if ($teapost) {
                return $this->teapot();
            }

            /** If the shared secret does not match with the stored version kick him out */
            if ($this->config['sipAPI']['sharedSecret'] !== $sharedSecret) {
                return $this->notAuthorized();
            }

            $productPrices = array();
            /** Iterate options */
            $territoryMapping = array(
                'GB#GBP' => 'gb#GBP',
                'EU#EUR' => 'eu#EUR',
                'DE#EUR' => 'de#EUR',
                'US#USD' => 'us#USD',
                'CA#CAD' => 'ca#CAD',
                'GB#AUD' => 'au#AUD',
            );
            $processedOptions = array();
            foreach ($variantIds as $variantId) {
                $optionCode = $this->productMapper->getOptionCodeForVariantId($variantId);
                if (in_array($optionCode, $processedOptions)) {
                    continue;
                }
                $processedOptions[] = $optionCode;
                $productOption = array(
                    'OPTION_CODE' => $optionCode
                );

                /** Get all current prices for option */
                $optionPrices = $this->productMapper->getAllProductPrices($variantId, 'PRODUCTS_R_ATTRIBUTES');
                /** Iterate prices and save into formatted array prices */

                foreach ($optionPrices as $optionPrice) {
                    $territoryData = $this->productMapper->getTerritoryCodeById($optionPrice['regionId']);
                    $currencyData = $this->productMapper->getCurrencyCodeById($optionPrice['currencyId']);

                    $territoryCurrencyKey = $territoryData['iso2Code'].'#'.$currencyData['isoCode'];
                    if (!isset($territoryMapping[$territoryCurrencyKey])) {
                        continue;
                    }

                    list($territoryUsed, $currencyUsed) = explode('#', $territoryMapping[$territoryCurrencyKey]);
                    $onlineFrom = $optionPrice['modified'];
                    $onlineFromStr = strtotime($onlineFrom);
                    $onlineFromUTC = gmdate('Y-m-d H:i:s', $onlineFromStr);
                    $productOption['PRICES'][] = array(
                        'MARKET_ID' => $territoryUsed,
                        'CURRENCY_ISO' => $currencyUsed,
                        'VALUE' => $optionPrice['nowPrice'],
                        'WAS_PRICE' => $optionPrice['wasPrice'],
                        'ONLINE_FROM' => $onlineFromUTC
                    );
                }

                /** Get all scheduled now prices for option */
                $scheduledPrices = $this->scheduleService->getAllScheduledPricesForProduct(
                    $variantId,
                    'PRODUCTS_R_ATTRIBUTES',
                    'PRODUCTS_Prices',
                    'nowPrice'
                );

                /** Iterate through the scheduled prices and add them to the array */
                foreach ($scheduledPrices as $scheduledPrice) {
                    /** @var \PrismProductsManager\Entity\Schedules $scheduledPrice */
                    $territory = $scheduledPrice->getSchedulePrices()->getTerritoryId();
                    $currency = $scheduledPrice->getSchedulePrices()->getCurrencyId();
                    $territoryIsoCode = $scheduledPrice->getSchedulePrices()->getTerritoryId()->getIso2code();
                    $currencyIsoCode = $scheduledPrice->getSchedulePrices()->getCurrencyId()->getIsocode();

                    $territoryCurrencyKey = $territoryIsoCode.'#'.$currencyIsoCode;
                    if (!isset($territoryMapping[$territoryCurrencyKey])) {
                        continue;
                    }
                    list($territoryUsed, $currencyUsed) = explode('#', $territoryMapping[$territoryCurrencyKey]);

                    $wasPrice = $this->scheduleService->getWasPriceValidAtDate(
                        $scheduledPrice->getScheduleStartDate(),
                        $variantId,
                        'PRODUCTS_R_ATTRIBUTES',
                        $territory->getTerritoryId(),
                        $currency->getCurrencyId()
                    );
                    if (!empty($wasPrice[0])) {
                        $wasPrice = $wasPrice[0]->getSchedulePrices()->getValue();
                    } else {
                        $wasPrice = $this->productMapper->getPriceColumnForProduct(
                            $variantId,
                            'PRODUCTS_R_ATTRIBUTES',
                            $territory->getTerritoryId(),
                            $currency->getCurrencyId()
                        );
                    }
                    $onlineFrom = $scheduledPrice->getScheduleStartDate()->format('Y-m-d H:i:s');
                    $onlineFromStr = strtotime($onlineFrom);
                    $onlineFromUTC = gmdate('Y-m-d H:i:s', $onlineFromStr);

                    $productOption['PRICES'][] = array(
                        'MARKET_ID' => $territoryUsed,
                        'CURRENCY_ISO' => $currencyUsed,
                        'VALUE' => $scheduledPrice->getSchedulePrices()->getValue(),
                        'WAS_PRICE' => $wasPrice,
                        'ONLINE_FROM' => $onlineFromUTC
                    );
                }

                if (!isset($productOption['PRICES'])) {
                    $productOption['PRICES'] = array();
                }

                $productPrices[] = $productOption;
            }
            $finalPriceArray = array('PRODUCT_PRICES' => $productPrices);

            return new JsonModel($finalPriceArray);

        } catch (\Exception $ex) {
            return $this->errorHappend($ex->getMessage());
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|mixed|void
     * @throws \Exception
     */
    public function update($id, $data)
    {
        return $this->methodNotAllowed();
    }

    /**
     * @param mixed $id
     * @return array|mixed|void
     * @throws \Exception
     */
    public function delete($id)
    {
        return $this->methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function methodNotAllowed()
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        return $response;
    }

    /**
     * @throws \Exception
     */
    protected function notAuthorized()
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_401);
        return $response;
    }

    /**
     * @return Response
     */
    protected function teapot()
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_418);
        return $response;
    }

    /**
     * @param $error
     * @return Response
     */
    protected function errorHappend($error)
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_500);
        $response->setContent($error);
        return $response;
    }
}