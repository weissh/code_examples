<?php
namespace PrismProductsManager\Controller\RestfulAPI;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use PrismProductsManager\Model\ProductsMapper;
use PrismProductsManager\Service\CommonAttributesService;
use PrismProductsManager\Service\ScheduleService;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class StandardProductDataAPIController extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * @var ProductsMapper
     */
    protected $productMapper;
    /**
     * @var array
     */
    protected $config;
    /**
     * @var array
     */
    protected $languages;
    /**
     * @var ScheduleService
     */
    protected $scheduleService;

    /**
     * @var CommonAttributesService
     */
    protected $commonAttributesService;

    /**
     * @param array $config
     * @param ProductsMapper $productMapper
     * @param ScheduleService $scheduleService
     * @param CommonAttributesService $commonAttributesService
     */
    public function __construct(
        array $config,
        ProductsMapper $productMapper,
        ScheduleService $scheduleService,
        CommonAttributesService $commonAttributesService
    )
    {
        $this->config = $config;
        $this->languages = $config['siteConfig']['languages']['site-languages'];
        $this->productMapper = $productMapper;
        $this->scheduleService = $scheduleService;
        $this->commonAttributesService = $commonAttributesService;
    }

    /**
     * @param mixed $id
     * @return Response
     */
    public function get($id)
    {
        return $this->_methodNotAllowed();
    }

    /**
     * @return Response
     */
    public function getList()
    {
        return $this->_methodNotAllowed();
    }


    /**
     * @param mixed $data
     * @return Response|JsonModel
     */
    public function create($data)
    {
        try {

            $sharedSecret = !empty($data['sharedSecret']) ? $data['sharedSecret'] : '';
            $variantIds = !empty($data['variantIds']) ? $data['variantIds'] : array();

            /** If the shared secret does not match with the stored version kick him out */
            if ($this->config['sipAPI']['sharedSecret'] !== $sharedSecret) {
                return $this->notAuthorized();
            }

            /** Validate variantIds */
            if (empty($variantIds)) {
                throw new \Exception('Variant Ids array is empty');
            }

            $variantsArray = array();

            /** Loop Variants and build product info array */
            foreach ($variantIds as $variantId) {
                /** Check if the variant exists */
                $productId = $this->productMapper->getProductIdFromVariantId($variantId);
                $productsDetails = $this->productMapper->getMinimumProductsDetails($productId, true);
                $variantDetails = $this->productMapper->getVariantFullDetails($variantId);
                $spectrumId = $this->productMapper->getSpectrumId($variantId, 'PRODUCTS_R_ATTRIBUTES');
                $originalCommonAttributesArray = $this->productMapper->getCommonAttributesFor($variantId, 'PRODUCTS_R_ATTRIBUTES');
                $commonAttributesHolder = $this->productMapper->processAndGroupAttributes($originalCommonAttributesArray);
                $colourUsedDetails = $this->productMapper->getColourForVariantId($variantId);
                $sizeUsed = $this->productMapper->getSizeUsedForVariantId($variantId, true);



                $attributesArray = array();
                $attributeImages = array();
                foreach ($commonAttributesHolder as $commonAttributes) {
                    foreach ($commonAttributes as $commonAttribute) {
                        if (empty($commonAttribute['mapping'])) {
                            $commonAttribute['mapping'] = str_replace(' ', '', $commonAttribute['commonAttributeName']);
                        }
                        switch ($commonAttribute['mapping']) {
                            case 'highRes':
                                $attributeImages = explode("\n", $commonAttribute['values']);
                                foreach ($attributeImages as &$image) {
                                    $image = trim($image);
                                }
                                break;
                            case 'lowRes':
                                break;
                            default:
                                $attributesArray[$commonAttribute['mapping']] = $commonAttribute['values'];
                                break;
                        }
                    }
                }


                if (!empty($this->config['clientConfig']['settings']['mediaServer']) && empty($attributeImages[0])) {
                    $imagesArray = $this->productMapper->getHighResImageForProduct($variantId, 1);
                    $images = $imagesArray['images'];
                    $defaultImage = $imagesArray['default'];
                } else {
                    $images = array();
                    $defaultImage = array();
                }

                $colourArray = array();
                if (!empty($colourUsedDetails)) {
                    $colourArray = array(
                        'code' => isset($colourUsedDetails['groupCode']) ? $colourUsedDetails['groupCode'] : '',
                        'name' => isset($colourUsedDetails['name']) ? $colourUsedDetails['name'] : '',
                    );
                }

                $sizeArray = array();
                if (!empty($sizeUsed)) {
                    $sizeArray = array(
                        'value' => isset($sizeUsed['value']) ? $sizeUsed['value'] : '',
                        'skuCode' => isset($sizeUsed['commonAttributeValueSkuCode']) ? $sizeUsed['commonAttributeValueSkuCode'] : '',
                    );
                }


                $variantArray = array(
                    'variantId' => $variantId,
                    'productId' => $productId,
                    'productType' => isset($productsDetails['name']) ? $productsDetails['name'] : 'STANDARD',
                    'shortProductName' => isset($variantDetails['shortProductName']) ? $variantDetails['shortProductName'] : '',
                    'productName' => isset($variantDetails['productName']) ? $variantDetails['productName'] : '',
                    'shortDescription' => isset($variantDetails['shortDescription']) ? $variantDetails['shortDescription'] : '',
                    'longDescription' => isset($variantDetails['longDescription']) ? $variantDetails['longDescription'] : '',
                    'active' => $variantDetails['status'] == 'ACTIVE' ? 1 : 0,
                    'sku' => $variantDetails['sku'],
                    'spectrumId' => $spectrumId,
                    'style' => $productsDetails['style'],
                    'barcode' => $variantDetails['ean13'],
                    'colour' => $colourArray,
                    'size' => $sizeArray,
                    'attributes' => $attributesArray,
                    'images' => !empty($attributeImages) ? $attributeImages : $images,
                    'defaultImage' => !empty($attributeImages[0]) ? $attributeImages[0]: $defaultImage
                );
                $variantsArray[$variantId] = $variantArray;
            }


            return new JsonModel($variantsArray);
        } catch (\Exception $ex) {
            return $this->errorHappend($ex->getMessage());
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return Response
     */
    public function update($id, $data)
    {
        return $this->_methodNotAllowed();
    }

    /**
     * @param mixed $id
     * @return Response
     */
    public function delete($id)
    {
        return $this->_methodNotAllowed();
    }

    /**
     * @param $error
     * @return Response
     */
    protected function errorHappend($error)
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_500);
        $response->setContent($error);
        return $response;
    }

    /**
     * @return Response
     */
    protected function notAuthorized()
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_401);
        return $response;
    }
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        return $response;
    }
}