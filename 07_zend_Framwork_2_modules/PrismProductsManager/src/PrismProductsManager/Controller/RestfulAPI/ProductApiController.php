<?php

namespace PrismProductsManager\Controller\RestfulAPI;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use PrismProductsManager\Model\ProductsMapper;
use PrismProductsManager\Validator\ProductData;
use Respect\Validation\Validator as v;

/**
 * Class ProductApiController
 *
 * @package PrismProductsManager\Controller\RestfulAPI
 */
class ProductApiController extends AbstractApiController implements ControllerApiAwareInterface
{
    /**
     * @var \PrismQueueManager\Model\ProductMapper
     */
    private $productMapper;

    /**
     * ProductApiController constructor.
     *
     * @param string                                     $sharedSecret
     * @param \PrismProductsManager\Model\ProductsMapper $productMapper
     */
    public function __construct($sharedSecret, ProductsMapper $productMapper)
    {
        parent::__construct($sharedSecret);
        $this->productMapper = $productMapper;
    }

    /**
     * @param $id
     * @return \Zend\Http\Response
     */
    public function get($id)
    {
        $sku = $_GET['sku'];
        if (empty($sku)) {
            return $this->_jsonResponse([
                'success'    => false,
            ]);
        }

        try {
            $variantDetails = $this->productMapper->getVariantDetailsByStyle($sku, true);
            return $this->_jsonResponse([
                'success'    => false,
                'variant_id' => array(
                    'id' => isset($variantDetails['productAttributeId']) ? $variantDetails['productAttributeId'] : false ,
                    'barcode' => isset($variantDetails['ean13']) ? $variantDetails['ean13'] : false
                )
            ]);

        } catch (\Exception $ex) {
            return $this->_jsonResponse([
                'success'    => false,
                'error' => $ex->getMessage()
            ]);
        }
    }

    /**
     * @param array $data
     *
     * @return \Zend\Http\Response
     */
    public function create($data)
    {
        $params = $this->_getParams($data);

        v::with(ProductData::getNamespace() . '\\');
        /** @var \Respect\Validation\Validator $validator */
        $validator = v::productData();

        $validator->assert($params);

        $variantId = $this->productMapper->createVariantFromDropship($params);

        return $this->_jsonResponse([
            'success'    => true,
            'variant_id' => $variantId,
        ]);
    }

    /**
     * @param mixed $id
     * @param mixed $data
     *
     * @return \Zend\Http\Response
     */
    public function update($id, $data)
    {
        $params = $this->_getParams($data);

        v::with(ProductData::getNamespace() . '\\');
        /** @var \Respect\Validation\Validator $validator */
        $validator = v::productData();

        $validator->assert($params);

        $variant = $this->productMapper->getVariantFullDetails($id);

        if ($variant === false) {
            return $this->_jsonNotFound('Variant not found');
        }

        $this->productMapper->updateVariantFromDropship($id, $params, false);

        return $this->_jsonResponse(['success' => true]);
    }
}
