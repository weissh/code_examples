<?php
namespace PrismProductsManager\Controller\RestfulAPI;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use PrismProductsManager\Model\ProductsMapper;
use PrismProductsManager\Service\CommonAttributesService;
use PrismProductsManager\Service\ScheduleService;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class StandardProductPriceAPIController extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * @var ProductsMapper
     */
    protected $productMapper;
    /**
     * @var array
     */
    protected $config;
    /**
     * @var array
     */
    protected $languages;
    /**
     * @var ScheduleService
     */
    protected $scheduleService;

    /**
     * @param array $config
     * @param ProductsMapper $productMapper
     * @param ScheduleService $scheduleService
     */
    public function __construct(
        array $config,
        ProductsMapper $productMapper,
        ScheduleService $scheduleService
    )
    {
        $this->config = $config;
        $this->languages = $config['siteConfig']['languages']['site-languages'];
        $this->productMapper = $productMapper;
        $this->scheduleService = $scheduleService;
    }

    /**
     * @param mixed $id
     * @return Response
     */
    public function get($id)
    {
        return $this->_methodNotAllowed();
    }

    /**
     * @return Response
     */
    public function getList()
    {
        return $this->_methodNotAllowed();
    }


    /**
     * @param mixed $data
     * @return Response|JsonModel
     */
    public function create($data)
    {
        try {
            $sharedSecret = !empty($data['sharedSecret']) ? $data['sharedSecret'] : '';
            $variantIds = !empty($data['variantIds']) ? $data['variantIds'] : array();

            /** If the shared secret does not match with the stored version kick him out */
            if ($this->config['sipAPI']['sharedSecret'] !== $sharedSecret) {
                return $this->notAuthorized();
            }
    
            /** Validate variantIds */
            if (empty($variantIds)) {
                throw new \Exception('Variant Ids array is empty');
            }
            
            $variantsArray = array();

            /** Loop Variants and build product info array */
            foreach ($variantIds as $variantId) {
                $prices = $this->productMapper->getAllProductPrices($variantId, "PRODUCTS_R_ATTRIBUTES");
                $variantDetails = $this->productMapper->getVariantFullDetails($variantId);
                $pricesArray = array();
                foreach ($prices as $price) {
                    $currency = $this->productMapper->getCurrencyCodeById($price['currencyId']);

                    $pricesArray[$currency['isoCode']] = array(
                        'nowPrice' => $price['nowPrice'],
                        'wasPrice' => $price['wasPrice'],
                        'costPrice' => $price['costPrice']
                    );
                }
                $variantsArray[$variantId] = array(
                    'variantId' => $variantId,
                    'sku' => $variantDetails['sku'],
                    'prices' => $pricesArray
                );
            }

            return new JsonModel($variantsArray);
        } catch (\Exception $ex) {
            return $this->errorHappend($ex->getMessage());
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return Response
     */
    public function update($id, $data)
    {
        return $this->_methodNotAllowed();
    }

    /**
     * @param mixed $id
     * @return Response
     */
    public function delete($id)
    {
        return $this->_methodNotAllowed();
    }

    /**
     * @param $error
     * @return Response
     */
    protected function errorHappend($error)
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_500);
        $response->setContent($error);
        return $response;
    }

    /**
     * @return Response
     */
    protected function notAuthorized()
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_401);
        return $response;
    }
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        return $response;
    }
}
