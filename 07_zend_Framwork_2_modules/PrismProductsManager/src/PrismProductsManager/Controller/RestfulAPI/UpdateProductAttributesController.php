<?php
namespace PrismProductsManager\Controller\RestfulAPI;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use PrismProductsManager\Model\ProductsMapper;
use PrismProductsManager\Service\CommonAttributesService;
use PrismProductsManager\Service\ScheduleService;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class UpdateProductAttributesController extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * @var ProductsMapper
     */
    protected $productMapper;
    /**
     * @var array
     */
    protected $config;
    /**
     * @var array
     */
    protected $languages;
    /**
     * @var ScheduleService
     */
    protected $scheduleService;

    /**
     * @var CommonAttributesService
     */
    protected $commonAttributesService;

    /**
     * @param array $config
     * @param ProductsMapper $productMapper
     * @param ScheduleService $scheduleService
     * @param CommonAttributesService $commonAttributesService
     */
    public function __construct(
        array $config,
        ProductsMapper $productMapper,
        CommonAttributesService $commonAttributesService
    )
    {
        $this->config = $config;
        $this->languages = $config['siteConfig']['languages']['site-languages'];
        $this->productMapper = $productMapper;
        $this->commonAttributesService = $commonAttributesService;
    }

    /**
     * @param mixed $id
     * @return Response
     */
    public function get($id)
    {
        return $this->_methodNotAllowed();
    }

    /**
     * @return Response
     */
    public function getList()
    {
        return $this->_methodNotAllowed();
    }


    /**
     * @param mixed $data
     * @return Response|JsonModel
     */
    public function create($data)
    {

        try {
            $sharedSecret = !empty($data['sharedSecret']) ? $data['sharedSecret'] : '';
            $requestParams = !empty($data['requestParams']) ? $data['requestParams'] : array();

            /** If the shared secret does not match with the stored version kick him out */
            if ($this->config['sipAPI']['sharedSecret'] !== $sharedSecret) {
                return $this->notAuthorized();
            }

            /** Validate variantIds */
            if (empty($requestParams)) {
                throw new \Exception('requestParams array is empty');
            }

            $response = array();

            /** Loop Variants and build product info array */
            foreach ($requestParams as $variant) {

                /** Check if the variant exists */
                $variantId = $this->productMapper->getVariantIdFromSku($variant['sku']);

                foreach ($variant['attributes'] as $attribute) {
                    foreach ($attribute as $mapping => $attributeValue) {
                        try {
                            $commonAttributeId = $this->productMapper->getCommonAttributeIdFromMapping($mapping);
                            if ($commonAttributeId) {

                                $attributeUsed = $this->productMapper->getCommonAttributesUsedForProduct($variantId, 'PRODUCTS_R_ATTRIBUTES', $commonAttributeId);
                                if ($attributeUsed) {
                                    // Update the attribute
                                    $this->commonAttributesService->updateCommonAttbuteValueOnStyleLevel($variantId, $commonAttributeId, $attributeValue, "PRODUCTS_R_ATTRIBUTES");
                                } else {
                                    // Create new attribute
                                    $this->commonAttributesService->insertNewAttributeValueForProduct($variantId, $commonAttributeId, $attributeValue, "PRODUCTS_R_ATTRIBUTES");
                                }
                            }
                            $this->productMapper->syncToSpectrum($variantId);
                            $response[$variant['sku']][$mapping] = 'Success';
                        } catch (\Exception $ex) {
                            $response[$variant['sku']][$mapping] = 'Failed';
                        }
                    }
                }
            }


            return new JsonModel($response);
        } catch (\Exception $ex) {
            return $this->errorHappend($ex->getMessage());
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return Response
     */
    public function update($id, $data)
    {
        return $this->_methodNotAllowed();
    }

    /**
     * @param mixed $id
     * @return Response
     */
    public function delete($id)
    {
        return $this->_methodNotAllowed();
    }

    /**
     * @param $error
     * @return Response
     */
    protected function errorHappend($error)
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_500);
        $response->setContent($error);
        return $response;
    }

    /**
     * @return Response
     */
    protected function notAuthorized()
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_401);
        return $response;
    }
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        return $response;
    }
}