<?php
namespace PrismProductsManager\Controller\RestfulAPI;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use PrismProductsManager\Model\ProductsMapper;
use PrismProductsManager\Service\CommonAttributesService;
use PrismProductsManager\Service\ScheduleService;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 *
 * @author <hani.weiss@whistl.co.uk> Hani Weiss
 */
class ProductDataAPIController extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * @var ProductsMapper
     */
    protected $productMapper;
    /**
     * @var array
     */
    protected $config;
    /**
     * @var array
     */
    protected $languages;
    /**
     * @var ScheduleService
     */
    protected $scheduleService;

    /**
     * @var CommonAttributesService
     */
    protected $commonAttributesService;

    /**
     * @param array $config
     * @param ProductsMapper $productMapper
     * @param ScheduleService $scheduleService
     * @param CommonAttributesService $commonAttributesService
     */
    public function __construct(
        array $config,
        ProductsMapper $productMapper,
        ScheduleService $scheduleService,
        CommonAttributesService $commonAttributesService
    )
    {
        $this->config = $config;
        $this->languages = $config['siteConfig']['languages']['site-languages'];
        $this->productMapper = $productMapper;
        $this->scheduleService = $scheduleService;
        $this->commonAttributesService = $commonAttributesService;
    }

    /**
     * @param mixed $id
     * @return Response
     */
    public function get($id)
    {
        return $this->_methodNotAllowed();
    }

    /**
     * @return Response
     */
    public function getList()
    {
        return $this->_methodNotAllowed();
    }

    /**
     * @param mixed $data
     * @return Response
     */
    public function create($data)
    {

        try {

            $sharedSecret = !empty($data['sharedSecret']) ? $data['sharedSecret'] : '';
            $variantIds = !empty($data['variantIds']) ? $data['variantIds'] : array();

            /** If the shared secret does not match with the stored version kick him out */
            if ($this->config['sipAPI']['sharedSecret'] !== $sharedSecret) {
                return $this->notAuthorized();
            }

            /** Validate variantIds */
            if (empty($variantIds)) {
                throw new \Exception('Variant Ids array is empty');
            }

            /** Declare empty parts of content */
            $products = array();
            $options = array();
            $skus = array();

            /** Iterate variants and build data */
            foreach ($variantIds as $variantId) {
                $productId = $this->productMapper->getProductIdFromVariantId($variantId);
                $variantDetails = $this->productMapper->getVariantFullDetails($variantId);
                $spectrumId = $this->productMapper->getSpectrumId($variantId, 'PRODUCTS_R_ATTRIBUTES');
                $productDetails = $this->productMapper->getProductDetailsById($productId, 1);
                $originalCommonAttributesArray = $this->productMapper->getCommonAttributesFor($variantId, 'PRODUCTS_R_ATTRIBUTES');

                $commonAttributes = $this->productMapper->processAndGroupAttributes($originalCommonAttributesArray);
                $optionCodeTmp = $this->productMapper->getOptionCodeForVariantId($variantId);
                $readyForWebTmp = $this->productMapper->isReadyForWeb($optionCodeTmp);

                if ($readyForWebTmp === false) {
                    $tmpData = [
                        'time' => date('Y-m-d H:i:s'),
                        'variant' => $variantId,
                        'readyForWeb' => $readyForWebTmp
                    ];
                    file_put_contents('/tmp/ready_for_web_not_allowed.log', var_export($tmpData, true), FILE_APPEND);
                    continue;
                }

                $usedStyles = array();
                /** BUILD DATA FOR STYLE LEVEL ARRAY */
                if (is_array($productDetails)) {

                    if (!empty($commonAttributes['master_style'][0]['values'])) {
                        $masterStyle = $commonAttributes['master_style'][0]['values'];
                        $mainStyleDetails = $this->productMapper->getMainProductDetailsByStyle($masterStyle);
                        $mainProductId = $mainStyleDetails['productId'];
                        $mainProductDetails = $this->productMapper->getProductDetailsById($mainProductId, 1);
                        $mainProductVariants = $this->productMapper->getVariants($mainProductId);
                        $mainProductDetailsVariantId = !empty($mainProductVariants['data'][0]['productAttributeId']) ? $mainProductVariants['data'][0]['productAttributeId'] : 0;

                        if (empty($mainProductDetailsVariantId)) {
                            // jump to the next variant
                            continue;
                        } else {
                            $mainProductOriginalCommonAttributesArray = $this->productMapper->getCommonAttributesFor($mainProductDetailsVariantId, 'PRODUCTS_R_ATTRIBUTES');
                            $mainProductCommonAttributes = $this->productMapper->processAndGroupAttributes($mainProductOriginalCommonAttributesArray);
                        }

                        $productDetails = $productDetails[key($productDetails)];
                        $mainProductDetails = $mainProductDetails[key($mainProductDetails)];

                        if (!isset($products[$productId]) && !in_array($mainProductDetails['style'], $usedStyles)) {
                            $usedStyles[] = $mainProductDetails['style'];
                            $currentProductDetails = array(
                                'PRODUCT_CODE' => $mainProductDetails['style']
                            );

                            if (!empty($mainProductCommonAttributes['style'])) {
                                foreach ($mainProductCommonAttributes['style'] as $styleLevelAttributes) {
                                    /** Check if string ends with / */
                                    if (strpos($styleLevelAttributes['mapping'], 'BY_LANGUAGE') === 0) {
                                        // we have a value by language
                                        $explodedMappingString = explode('/', $styleLevelAttributes['mapping']);
                                        $currentProductDetails[$explodedMappingString[1]][] = array(
                                            'LANGUAGE' => $explodedMappingString[2],
                                            'VALUE' => count($styleLevelAttributes['values']) > 1 ? $styleLevelAttributes['values'][0] : $styleLevelAttributes['values']
                                        );
                                    } else {
                                        $currentProductDetails[$styleLevelAttributes['mapping']] = $styleLevelAttributes['values'];
                                    }
                                }
                            }
                            $products[$productId] = $currentProductDetails;
                        }

                        unset($commonAttributes['master_style']);
                    } else {
                        $mainProductDetails = array();
                        $productDetails = $productDetails[key($productDetails)];
                        if (!isset($products[$productId]) && !in_array($productDetails['style'], $usedStyles)) {
                            $usedStyles[] = $productDetails['style'];
                            $currentProductDetails = array(
                                'PRODUCT_CODE' => $productDetails['style']
                            );

                            if (!empty($commonAttributes['style'])) {
                                foreach ($commonAttributes['style'] as $styleLevelAttributes) {
                                    /** Check if string ends with / */
                                    if (strpos($styleLevelAttributes['mapping'], 'BY_LANGUAGE') === 0) {
                                        // we have a value by language
                                        $explodedMappingString = explode('/', $styleLevelAttributes['mapping']);
                                        $currentProductDetails[$explodedMappingString[1]][] = array(
                                            'LANGUAGE' => $explodedMappingString[2],
                                            'VALUE' => count($styleLevelAttributes['values']) > 1 ? $styleLevelAttributes['values'][0] : $styleLevelAttributes['values']
                                        );
                                    } else {
                                        $currentProductDetails[$styleLevelAttributes['mapping']] = $styleLevelAttributes['values'];
                                    }
                                }
                            }
                            $products[$productId] = $currentProductDetails;
                        }
                    }
                }
                /** END OF : BUILD DATA FOR STYLE LEVEL ARRAY */

                /** BUILD DATA FOR OPTION LEVEL ARRAY */
                $colourUsedDetails = $this->productMapper->getColourForVariantId($variantId);

                if (isset($colourUsedDetails['groupCode']) && isset($productDetails['style'])) {
                    $optionCode = $productDetails['style'] . $colourUsedDetails['groupCode'];
                    if (!isset($options[$optionCode])) {
                        $currentOptionDetails = array(
                            'OPTION_CODE' => $optionCode,
                            'BASE_PRODUCT' => !empty($mainProductDetails['style']) ? $mainProductDetails['style'] : $productDetails['style'],
                            'COLOUR_CODE' => $colourUsedDetails['groupCode'],
                        );

                        if (!empty($commonAttributes['option'])) {
                            $statuses = array();

                            foreach ($commonAttributes['option'] as $optionLevelAttributes) {
                                if (strpos($optionLevelAttributes['commonAttributeName'], 'Status ') !== false) {
                                    /** Logic custom for statuses */
                                    /** Current status */
                                    $currentStatus = array();
                                    $marketId = str_replace('Status ', '', $optionLevelAttributes['commonAttributeName']);
                                    $onlineFrom = $optionLevelAttributes['modified'];
                                    $onlineFromStr = strtotime($onlineFrom);
                                    $onlineFromUTC = gmdate('Y-m-d H:i:s', $onlineFromStr);

                                    switch ($marketId) {
                                        case 'UK':
                                            $marketId = 'GB';
                                            break;
                                        default:
                                            break;
                                    }
                                    $currentStatus[] = array(
                                        'MARKET_ID' => strtolower($marketId),
                                        'START_DATE' => $onlineFromUTC,
                                        'CODE' => $optionLevelAttributes['values'][0]
                                    );
                                    /** Other Scheduled values for this status */
                                    $otherScheduledValues = $this->scheduleService->getAllScheduledEventsForProductAndAttribute(
                                        $variantId,
                                        'PRODUCTS_R_ATTRIBUTES',
                                        $optionLevelAttributes['commonAttributeId'],
                                        2,
                                        $marketId
                                    );
                                    $statuses = array_merge($currentStatus, $otherScheduledValues, $statuses);
                                    $currentOptionDetails[$optionLevelAttributes['mapping']] = $statuses;
                                } else {
                                    $currentOptionDetails[$optionLevelAttributes['mapping']] = $optionLevelAttributes['values'];
                                }
                            }
                        }
                        $options[$optionCode] = $currentOptionDetails;
                    }
                }

                /** END OF : BUILD DATA FOR OPTION LEVEL ARRAY */

                /** BUILD DATA FOR SKU LEVEL ARRAY */
                if (!empty($variantDetails) && !empty($optionCode)) {
                    $currentSkuDetails = array(
                        //'PRODUCT_CODE' => $variantDetails['sku'],
                        'SKU_CODE' => $spectrumId,
                        //'PIM_PRODUCT_ID' => $variantDetails['productAttributeId'],
                        //'SPECTRUM_PRODUCT_ID' => $spectrumId,
                        'OPTION_CODE' => $optionCode,
                        'EAN' => $variantDetails['ean13'],
                        'SANITY_NAME' => $variantDetails['sku'],
                        'BASE_SIZE_CODE' => $this->productMapper->getBaseSizeUsedForVariantId($variantId)
                    );
                    $skus[] = $currentSkuDetails;
                }

                /** END OF : BUILD DATA FOR SKU LEVEL ARRAY */
            }
            $returnArray = array(
                'PRODUCTS' => $products,
                'COLOUR_VARIANTS' => $options,
                'SIZE_VARIANTS' => $skus
            );

            return new JsonModel($returnArray);
        } catch (\Exception $ex) {
            return $this->errorHappend($ex->getMessage());
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return Response
     */
    public function update($id, $data)
    {
        return $this->_methodNotAllowed();
    }

    /**
     * @param mixed $id
     * @return Response
     */
    public function delete($id)
    {
        return $this->_methodNotAllowed();
    }

    /**
     * @param $error
     * @return Response
     */
    protected function errorHappend($error)
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_500);
        $response->setContent($error);
        return $response;
    }

    /**
     * @return Response
     */
    protected function notAuthorized()
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_401);
        return $response;
    }
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $response = new Response();
        $response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        return $response;
    }
}