<?php

namespace PrismProductsManager\Controller\RestfulAPI;

use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;
use PrismProductsManager\Entity\CommonAttributesValues;
use PrismProductsManager\Service\CommonAttributesService;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as v;
use Zend\Http\PhpEnvironment\Response as ResponseStatus;

/**
 * Class WebCategoryAPIController
 *
 * @package PrismProductsManager\Controller\RestfulAPI
 */
class WebCategoryAPIController extends AbstractApiController implements ControllerApiAwareInterface
{
    /**
     * @var string
     */
    private $webCategoryField;

    /**
     * @var \PrismProductsManager\Service\CommonAttributesService
     */
    private $commonAttributesService;

    /**
     * WebCategoryAPIController constructor.
     *
     * @param string                                                $sharedSecret
     * @param string                                                $webCategoryField
     * @param \PrismProductsManager\Service\CommonAttributesService $commonAttributesService
     */
    public function __construct($sharedSecret, $webCategoryField, CommonAttributesService $commonAttributesService)
    {
        parent::__construct($sharedSecret);
        $this->webCategoryField = $webCategoryField;
        $this->commonAttributesService = $commonAttributesService;
    }

    /**
     * @param array $request
     *
     * @return \Zend\Http\Response
     */
    public function create($request)
    {
        if (!$this->_isAuthorized($request)) {
            return $this->_jsonResponse(null, ResponseStatus::STATUS_CODE_403);
        }

        $itemValidator = v::allOf(
            v::arrayType(),
            v::key('id', v::intVal(), true),
            v::key('name', v::stringType(), true)
        );

        $singleValidator = v::allOf(
            v::arrayType(),
            v::key('requestParams', $itemValidator, true)
        );

        $isCollection = true;

        try {
            $singleValidator->assert($request);
            $isCollection = false;
            $insertData = [
                [
                    CommonAttributesValues::FIELD_ABBREV => $request['requestParams']['id'],
                    'value'                              => $request['requestParams']['name'],
                ],
            ];
            $this->commonAttributesService->upsertCommandAttributeOptions(
                $this->webCategoryField,
                CommonAttributesValues::FIELD_ABBREV,
                $insertData
            );
        } catch (NestedValidationException $e) {
            // try collection
        }

        if ($isCollection) {
            $collectionValidator = v::allOf(
                v::arrayType(),
                v::key('requestParams', v::allOf(
                    v::arrayType(),
                    v::each($itemValidator)
                ), true)
            );

            $collectionValidator->assert($request);
            $insertData = [];
            foreach ($request['requestParams'] as $item) {
                $insertData[] = [
                    CommonAttributesValues::FIELD_ABBREV => $item['id'],
                    'value'                              => $item['name'],
                ];
            }
            $this->commonAttributesService->upsertCommandAttributeOptions(
                $this->webCategoryField,
                CommonAttributesValues::FIELD_ABBREV,
                $insertData
            );
        }

        return $this->_jsonResponse(['success' => true, 'request' => $request]);
    }

    /**
     * @param int|string $id
     * @param array      $request
     *
     * @return \Zend\Http\Response
     */
    public function delete($id, $request)
    {
        if (!$this->_isAuthorized($request)) {
            return $this->_jsonResponse(null, ResponseStatus::STATUS_CODE_403);
        }

        $validator = v::intVal();

        $validator->assert($id);
        $this->commonAttributesService->deleteCommonAttributeOption(
            $this->webCategoryField,
            CommonAttributesValues::FIELD_ABBREV,
            [$id]
        );

        return $this->_jsonResponse(['success' => true, 'id' => $id, 'request' => $request]);
    }

    /**
     * @param array $request
     *
     * @return \Zend\Http\Response
     */
    public function deleteList($request)
    {
        if (!$this->_isAuthorized($request)) {
            return $this->_jsonResponse(null, ResponseStatus::STATUS_CODE_403);
        }

        $validator = v::allOf(
            v::arrayType(),
            v::key('requestParams', v::allOf(
                v::arrayType(),
                v::each(v::intVal())
            ), true)
        );

        $validator->assert($request);
        $this->commonAttributesService->deleteCommonAttributeOption(
            $this->webCategoryField,
            CommonAttributesValues::FIELD_ABBREV,
            $request['requestParams']
        );

        return $this->_jsonResponse(['success' => true, 'request' => $request]);
    }

    /**
     * @param array $request
     *
     * @return \Zend\Http\Response
     */
    public function replaceList($request)
    {
        return $this->create($request);
    }

    /**
     * @param int|string $id
     * @param array      $request
     *
     * @return \Zend\Http\Response
     */
    public function update($id, $request)
    {
        if (!$this->_isAuthorized($request)) {
            return $this->_jsonResponse(null, ResponseStatus::STATUS_CODE_403);
        }

        $itemValidator = v::allOf(
            v::arrayType(),
            v::key('requestParams', v::allOf(
                v::arrayType(),
                v::key('name', v::stringType(), true)
            ), true)
        );

        $itemValidator->assert($request);
        $insertData = [
            [
                CommonAttributesValues::FIELD_ABBREV => $id,
                'value'                              => $request['requestParams']['name'],
            ],
        ];
        $this->commonAttributesService->upsertCommandAttributeOptions(
            $this->webCategoryField,
            CommonAttributesValues::FIELD_ABBREV,
            $insertData
        );

        return $this->_jsonResponse(['success' => true, 'id' => $id, 'request' => $request]);
    }
}
