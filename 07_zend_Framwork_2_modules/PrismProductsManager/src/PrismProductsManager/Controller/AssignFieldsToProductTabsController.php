<?php

namespace PrismProductsManager\Controller;

use PrismProductsManager\Form\AssignInputFieldsForm;
use PrismProductsManager\Form\InputFieldValidationForm;
use PrismProductsManager\Form\SearchForm;
use PrismProductsManager\Service\AssignFieldToTabsService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\View;

/**
 * Class AssignFieldsToProductTabsController
 * @package PrismProductsManager\Controller
 * @author Hani Weiss <hani.weiss@whistl.co.uk>
 */
class AssignFieldsToProductTabsController extends AbstractActionController
{
    /**
     * @var
     */
    protected $commonAttributesService;
    /**
     * @var
     */
    protected $viewRenderer;
    /**
     * @var bool
     */
    protected $errorFlag = false;
    /**
     * @var SearchForm
     */
    protected $searchForm;

    /**
     * @var AssignInputFieldsForm
     */
    protected $assignInputFieldsForm;

    /**
     * @param $config
     * @param AssignFieldToTabsService $assignFieldsToTabsService
     * @param $viewRenderer
     * @param SearchForm $searchForm
     * @param AssignInputFieldsForm $assignInputFieldsForm
     * @param InputFieldValidationForm $inputFieldValidationForm
     */
    public function __construct(
        $config,
        AssignFieldToTabsService $assignFieldsToTabsService,
        $viewRenderer,
        SearchForm $searchForm,
        AssignInputFieldsForm $assignInputFieldsForm,
        InputFieldValidationForm $inputFieldValidationForm
    )
    {
        /** Services */
        $this->assignFieldsToTabsService = $assignFieldsToTabsService;
        /** Forms */
        $this->assignInputFieldsForm = $assignInputFieldsForm;
        $this->searchForm = $searchForm;
        $this->inputFieldValidationForm = $inputFieldValidationForm;
        /** Other */
        $this->viewRenderer = $viewRenderer;
        $this->clientConfig = $config['clientConfig'];
    }

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $this->isCmsUserAuthorized(array('view'));

        /**
         *  Set the values for the errorFlag to false
         *  If true Popup opens on page load
         */
        $attributeInputFields = $this->assignFieldsToTabsService->getAttributesInputFieldsByCriteria();

        if ($this->isFeatureEnabled() && $this->checkIfUserIsAuthorized()) {
            return new ViewModel(array(
                'allowed' => true,
                'attributeInputFields' => $attributeInputFields,
                'searchForm' => $this->searchForm
            ));
        } else {
            return new ViewModel(array(
                'allowed' => false,
            ));
        }

    }

    /**
     * @return ViewModel
     */
    public function createNewFieldAction()
    {
        $this->isCmsUserAuthorized(array('add'));
        $request = $this->getRequest();

        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            /** Set up data for the form */
            $this->assignInputFieldsForm->setData($data);
            $this->extraValidationOnForm($data);

            if (isset($data['display-only-value'])) {
                $this->assignInputFieldsForm->get('display-only-value[]')->setValue(
                    $this->assignFieldsToTabsService->getCommonAttributesValues($data['display-only-value'])
                );
            }
            $this->assignInputFieldsForm->get('inputFieldsets')->setValue($data['inputFieldsets']);
            /** Validate data */
            $formMessages = $this->assignInputFieldsForm->getMessages();
            if ($this->assignInputFieldsForm->isValid() && empty($formMessages)) {
                $sanitizedData = $this->assignInputFieldsForm->getData();
                $sanitizedData['display-only-value'] = !empty($data['display-only-value']) ? $data['display-only-value'] : array();
                /** Save data */
                $successFlag = $this->assignFieldsToTabsService->createNewField($sanitizedData);
                if ($successFlag) {
                    /** Redirect to fields listing */
                    return $this->redirectToPage('Field has been created on the specified product tab');
                } else {
                    /** Set up error to show on the page */
                }
            }
        }

        if ($this->isFeatureEnabled() && $this->checkIfUserIsAuthorized()) {
            return new ViewModel(array(
                'allowed' => true,
                'assignInputFieldsForm' => $this->assignInputFieldsForm
            ));
        } else {
            return new ViewModel(array(
                'allowed' => false,
            ));
        }
    }

    /**
     * @return ViewModel
     * @throws \Exception
     */
    public function editInputFieldAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $inputFieldId = $this->params()->fromRoute('inputFieldId', null);
        $request = $this->getRequest();

        /** Set up data for the form */
        $inputFieldData = $this->assignFieldsToTabsService->getInputFieldDetails($inputFieldId);

        $this->assignInputFieldsForm->setData($inputFieldData);
        $this->assignInputFieldsForm->get('inputFieldsets')->setValue($inputFieldData['inputFieldsets']);
        $this->assignInputFieldsForm->get('editableAtLevel')->setValue($inputFieldData['editableAtLevel']);
        $this->assignInputFieldsForm->get('isHidden')->setValue($inputFieldData['isHidden']);

        if ($request->isPost()) {
            /** Set up data for the form */
            $data = $request->getPost()->toArray();
            $this->assignInputFieldsForm->setData($data);
            $this->extraValidationOnForm($data);
            $this->assignInputFieldsForm->get('display-only-value[]')->setValue(
                $this->assignFieldsToTabsService->getCommonAttributesValues($data['display-only-value'])
            );
            $this->assignInputFieldsForm->get('inputFieldsets')->setValue($data['inputFieldsets']);
            /** Validate data */
            $formMessages = $this->assignInputFieldsForm->getMessages();
            if ($this->assignInputFieldsForm->isValid() && empty($formMessages)) {
                $sanitizedData = $this->assignInputFieldsForm->getData();
                $sanitizedData['display-only-value'] = !empty($data['display-only-value']) ? $data['display-only-value'] : array();

                /** Save data */
                $successFlag = $this->assignFieldsToTabsService->editInputField($sanitizedData);
                if ($successFlag) {
                    /** Redirect to fields listing */
                    return $this->redirectToPage('Field has been edited on the specified product tab');
                } else {
                    /** Set up error to show on the page */
                    return $this->redirectToPage(
                        'An error has occurred. Please try again or contact PRISM.' ,
                        'index',
                        'AssignFieldsToProductTabs',
                        'error'
                    );
                }
            }
        }

        if ($this->isFeatureEnabled() && $this->checkIfUserIsAuthorized()) {
            return new ViewModel(array(
                'allowed' => true,
                'assignInputFieldsForm' => $this->assignInputFieldsForm
            ));
        } else {
            return new ViewModel(array(
                'allowed' => false,
            ));
        }
    }

    /**
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function getInputFieldOrderOnFieldsetAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $inputFieldsetId = $this->params()->fromPost('inputFieldsetId', null);
        $inputPageId = $this->params()->fromPost('inputPageId', null);
        $request = $this->getRequest();

        if ($request->isPost()) {
            $inputFields = $this->assignFieldsToTabsService->getInputFieldsAssignedToFieldset($inputFieldsetId, $inputPageId);

            $inputFieldsetModal = new ViewModel();
            $inputFieldsetModal->setTerminal(true);
            $inputFieldsetModal->setTemplate('prism-products-manager/assign-fields-to-product-tabs/partials/fieldset-order');
            $inputFieldsetModal->setVariable('inputFields', $inputFields);

            $inputFieldsetModalOutput = $this->viewRenderer->render($inputFieldsetModal);
            $inputFieldsetModalOutput = utf8_encode($inputFieldsetModalOutput);

            $response = $this->getResponse();
            $response->setContent(json_encode(array('html' => $inputFieldsetModalOutput)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }

        $response = $this->getResponse();
        $response->setStatusCode(401);
        $response->setContent('Unauthorized');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }

    /**
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function saveInputFieldOrderAction()
    {
        $this->isCmsUserAuthorized(array('edit'));
        $inputArray = $this->params()->fromPost('inputArray', null);
        $request = $this->getRequest();
        $result = false;
        if ($request->isPost() && !is_null($inputArray)) {
            $result = $this->assignFieldsToTabsService->saveInputFieldsOrder($inputArray);
        }
        $response = $this->getResponse();
        $response->setContent(json_encode(array('result' => $result)));
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        return $response;
    }
    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function retrieveWorkFlowsAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $inputFieldId = $this->params()->fromPost('inputFieldId', null);
        $request = $this->getRequest();

        if ($request->isPost()){
            /** Set the already selected work-flows */
            $inputFieldEntity = $this->assignFieldsToTabsService->getInputField($inputFieldId);
            $selectedWorkflows = array();
            foreach ($inputFieldEntity->getInputFieldRWorkflows() as $selectedWorkflow) {
                $selectedWorkflows[$selectedWorkflow->getWorkflowId()->getWorkflowId()] = $selectedWorkflow->getWorkflowId()->getWorkflowDescription();
            }
            /** Set all work-flows */
            $workflows = $this->assignFieldsToTabsService->getAllWorkflows();
            $workflowsList = array();
            foreach ($workflows as $workflow) {
                $workflowsList[$workflow->getWorkflowId()] = $workflow->getWorkflowDescription();
            }

            $workflowsModal = new ViewModel();
            $workflowsModal->setTerminal(true);
            $workflowsModal->setTemplate('prism-products-manager/assign-fields-to-product-tabs/partials/workflows');
            $workflowsModal->setVariable('inputFieldEntity', $inputFieldEntity);
            $workflowsModal->setVariable('selectedWorkflows', $selectedWorkflows);
            $workflowsModal->setVariable('workflowsList', $workflowsList);

            $workflowsModalOutput = $this->viewRenderer->render($workflowsModal);
            $workflowsModalOutput = utf8_encode($workflowsModalOutput);

            $response = $this->getResponse();
            $response->setContent(json_encode(array('html' => $workflowsModalOutput)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }
        $response = $this->getResponse();
        $response->setStatusCode(401);
        $response->setContent('Unauthorized');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }

    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function saveWorkFlowsAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $inputFieldId = $this->params()->fromPost('inputFieldId', null);
        $selectedWorkflows = $this->params()->fromPost('selectedWorkflows', null);
        $request = $this->getRequest();

        $successFlag = false;
        if ($request->isPost()) {
            if ($this->assignFieldsToTabsService->deleteInputFieldRelationToWorkflows($inputFieldId)) {
                $successFlag = $this->assignFieldsToTabsService->createInputFieldRelationToWorkflows($selectedWorkflows, $inputFieldId);
            }
        }

        $response = $this->getResponse();
        $response->setContent(json_encode(array('success' => $successFlag)));
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        return $response;
    }
    /**
     * @return null
     */
    public function deleteInputFieldAction()
    {
        $this->isCmsUserAuthorized(array('delete'));
        $inputFieldId = $this->params()->fromRoute('inputFieldId', null);
        if (!is_null($inputFieldId)) {
            /** Check if field is attribute type in order  */
            if ($this->assignFieldsToTabsService->fieldIsAttributeType($inputFieldId)) {
                return $this->redirectToPage(
                    'Attribute cannot be deleted. Please contact PRISM quoting ID 213' ,
                    'index',
                    'AssignFieldsToProductTabs',
                    'error'
                );
            }
            /** Delete input field */
            $successFlag = $this->assignFieldsToTabsService->deleteInputField($inputFieldId);
            if ($successFlag) {
                return $this->redirectToPage('Field has been deleted.');
            }
        }
        /** Set up error to show on the page */
        return $this->redirectToPage(
            'An error has occurred. Please try again or contact PRISM.' ,
            'index',
            'AssignFieldsToProductTabs',
            'error'
        );
    }

    /**
     * @param $data
     */
    private function extraValidationOnForm($data)
    {
        /** check if attributeType is selected */
        if ($data['attributeType'] == '0') {
            $this->assignInputFieldsForm->get('attributeType')->setMessages(array('Please select the attribute type'));
        }
        /** check if the attribute has been selected */
        switch ($data['attributeType']) {
            case 'COMMON_ATTRIBUTES':
                if (empty($data['commonAttributes'])) {
                    $this->assignInputFieldsForm->get('commonAttributes')->setMessages(array('Please select a common attribute.'));
                }
                break;
            case 'CORE_ATTRIBUTES':
                if (empty($data['coreAttributes'])) {
                    $this->assignInputFieldsForm->get('coreAttributes')->setMessages(array('Please select a core attribute.'));
                }
                break;
            case 'MARKETING_ATTRIBUTES':
                if (empty($data['marketingAttributes'])) {
                    $this->assignInputFieldsForm->get('marketingAttributes')->setMessages(array('Please select a marketing attribute.'));
                }
                break;
            case 'MERCHANT_ATTRIBUTES':
                if (empty($data['merchantAttributes'])) {
                    $this->assignInputFieldsForm->get('merchantAttributes')->setMessages(array('Please select a merchant attribute.'));
                }
                break;
        }
    }
    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function retrieveFieldsetsForProductTabAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $productTabId = $this->params()->fromPost('productTab', null);
        $request = $this->getRequest();

        if ($request->isPost() && !is_null($productTabId)) {
            $inputFieldsets = $this->assignFieldsToTabsService->retrieveAllFieldsetsFromTab($productTabId);
            /** Return Response */
            $response = $this->getResponse();
            $response->setContent(json_encode(array('fieldsets' => $inputFieldsets)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }

        $response = $this->getResponse();
        $response->setStatusCode(401);
        $response->setContent('Unauthorized');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }

    /**
     * @return \Zend\Http\PhpEnvironment\Response|\Zend\Stdlib\ResponseInterface
     */
    public function retrieveInputFieldValidationAction()
    {
        $this->isCmsUserAuthorized(array('view'));
        $inputFieldId = $this->params()->fromPost('inputFieldId', null);
        $request = $this->getRequest();

        if ($request->isPost()){
            /** Retrieve validation saved data */
            $inputFieldValidation = $this->assignFieldsToTabsService->retrieveValidationForInputField($inputFieldId);
            if (!is_null($inputFieldValidation)) {
                /** Hydrate form */
                $this->inputFieldValidationForm->get('inputValidationType')->setValue(
                    $inputFieldValidation->getInputValidationType()->getInputValidationTypeId()
                );
                $this->inputFieldValidationForm->get('inputValidationId')->setValue(
                    $inputFieldValidation->getInputValidationId()
                );
                $this->inputFieldValidationForm->get('minValue')->setValue(
                    $inputFieldValidation->getInputValidationMinValue()
                );
                $this->inputFieldValidationForm->get('maxValue')->setValue(
                    $inputFieldValidation->getInputValidationMaxValue()
                );
                $this->inputFieldValidationForm->get('pattern')->setValue(
                    $inputFieldValidation->getPattern()
                );
            }
            $this->inputFieldValidationForm->get('inputFieldId')->setValue($inputFieldId);

            $inputFieldValidationModal = new ViewModel();
            $inputFieldValidationModal->setTerminal(true);
            $inputFieldValidationModal->setTemplate('prism-products-manager/assign-fields-to-product-tabs/partials/validation-management');

            $inputFieldValidationModal->setVariable('inputFieldValidationForm', $this->inputFieldValidationForm);
            $inputFieldValidationModalOutput = $this->viewRenderer->render($inputFieldValidationModal);
            $inputFieldValidationModalOutput = utf8_encode($inputFieldValidationModalOutput);

            $response = $this->getResponse();
            $response->setContent(json_encode(array('html' => $inputFieldValidationModalOutput)));
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            return $response;
        }
        $response = $this->getResponse();
        $response->setStatusCode(401);
        $response->setContent('Unauthorized');
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/html');
        return $response;
    }

    /**
     * @return null
     */
    public function saveValidationAction()
    {
        $this->isCmsUserAuthorized(array('view'));

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $this->inputFieldValidationForm->setData($data);
            if ($this->inputFieldValidationForm->isValid()) {
                $successFlag = $this->assignFieldsToTabsService->saveInputFieldValidation($this->inputFieldValidationForm->getData());
                if ($successFlag) {
                    return $this->redirectToPage('Validation has been saved.');
                }
            }
        }
        return $this->redirectToPage(
            'An error has occurred. Please try again or contact PRISM.' ,
            'index',
            'AssignFieldsToProductTabs',
            'error'
        );
    }



    /**
     * @return bool
     */
    private function isFeatureEnabled()
    {
        if (isset($this->clientConfig['features']['dynamicInputs']) &&
            $this->clientConfig['features']['dynamicInputs']
        ) {
            return true;
        }
        return false;
    }

    /**
     * Method used to check if user is administrator
     * Only administrators should have access to common attributes if config flag is set up
     * @return bool
     */
    private function checkIfUserIsAuthorized()
    {
        if ($this->clientConfig['settings']['commonAttributesForAdmin']) {
            $groupDetails = $this->getCurrentUser();
            if (!empty($groupDetails)) {
                if ($groupDetails->groupName == 'Administrator') {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    /**
     * @param string $message
     * @param string $action
     * @param string $controller
     * @param string $type
     * @return null
     */
    private function redirectToPage(
        $message = '',
        $action = 'index',
        $controller = 'AssignFieldsToProductTabs',
        $type = 'success'
    )
    {
        /**
         * Redirect to main page with message.
         * */
        $route = array(
            'controller' => $controller,
            'action' => $action
        );

        return $this->_errorValue($route, $type, $message);
    }
    /**
     * Redirect in case of error
     * @param $route
     * @param $type
     * @param $message
     *
     * @param string $alert
     * @return null
     */
    protected function _errorValue($route, $type, $message, $alert = [])
    {
        $alert['type'] = $type;
        $alert['bold'] = ucfirst($type).':';
        $alert['messages'] = array();
        $alert['messages'][0] = $message;

        $this->flashmessenger()->setNamespace('alert')
            ->addMessage($alert);

        return $this->redirect()->toRoute(null ,$route);
    }


}