<?php

namespace PrismProductsManager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ColoursController extends AbstractActionController
{
    protected $_coloursMapper;
    protected $_coloursForm;
    /**
     * @var Boolean
     */
    protected $errorFlag = false;

    public function __construct($coloursMapper, $coloursForm)
    {
        $this->_coloursMapper = $coloursMapper;
        $this->_coloursForm = $coloursForm;
    }

    public function indexAction()
    {
        $this->isCmsUserAuthorized(array('view'));

        $colourForm = $this->_coloursForm;
        $colourForm->get('submit')->setValue('Save');

        $valid['add'] = false;
        $valid['edit'] = false;

        $request = $this->getRequest();

        if ($request->isPost()) {
            $this->isCmsUserAuthorized(array('add', 'edit'));
            $submitedFormData = $this->params()->fromPost();

            // For the edit we need to check the values from the form and see
            $colourForm->setData($submitedFormData);

            $colourForm->setSubmittedData($submitedFormData);

            if ($colourForm->isValid()) {
                $processedData = $colourForm->getData();

                //Return true for adding a new colour
                //Return false for editing an existing colour
                // TODO: check if another group with similar name, group code and don;t allow creation if duplicates
                if ($this->_coloursMapper->saveColour($processedData)) {
                    $message = 'The colour has been added';
                } else {
                    $message = 'The colour has been edited';
                }
                $route = array(
                    'controller' => 'Colours',
                    'action' => 'index',
                );
                // Redirect based on the message and route
                return $this->_errorValue($route, 'success', $message);
            } else {
                // Set the valid variable to true
                // Used to open modal after post
                if (empty($submitedFormData['colourId'])) {
                    $valid['add'] = true;
                } else {
                    $valid['edit'] = true;
                }

                $this->errorFlag = true;
            }
        }

        $results = $this->_coloursMapper->fetchAll();

        $coloursArray = array();
        foreach ($results as $result) {
            $coloursArray[] = $result;
        }
        // reverse the result FEFO last in > fist out
        $coloursArray = array_reverse($coloursArray);

        return new ViewModel(array(
            'colours' => $coloursArray,
            'colourForm' => $colourForm,
            'valid' => $valid,
        ));
    }

    public function deleteAction()
    {
        $this->isCmsUserAuthorized(array('delete'));
        $colourId = (int) $this->params()->fromRoute('id');

        $delete = $this->_coloursMapper->deleteColour($colourId);

        if ($delete['error'] == false) {

            $message = 'The colour has been deleted';
            $type = 'success';

        } else {

            $message = $delete['message'];
            $type = 'error';
        }
        $route = array(
            'controller' => 'Colours',
            'action' => 'index',
        );

        // Redirect based on the message and route
        return $this->_errorValue($route, $type, $message);
    }

    /**
     * Redirect in case of error
     * @param $route
     * @param $type
     * @param $message
     *
     * @param string $alert
     * @return null
     */
    protected function _errorValue($route, $type, $message, $alert = [])
    {
        $alert['type'] = $type;
        $alert['bold'] = ucfirst($type).':';
        $alert['messages'] = array();
        $alert['messages'][0] = $message;

        $this->flashmessenger()->setNamespace('alert')
            ->addMessage($alert);

        return $this->redirect()->toRoute(null ,$route);
    }
}

