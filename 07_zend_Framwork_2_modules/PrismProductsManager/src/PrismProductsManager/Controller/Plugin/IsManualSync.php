<?php

namespace PrismProductsManager\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;


/**
 * Class IsManualSync
 * @package PrismProductsManager\Controller\Plugin
 */
class IsManualSync extends AbstractPlugin
{

    /**
     * @return bool
     */
    public function __invoke()
    {
        $config = $this->getController()->getServiceLocator()->get('Config');
        $clientConfig = $config['clientConfig'];
        if (!empty($clientConfig['settings']['manualSync'])) {
            return true;
        }
        return false;
    }
}