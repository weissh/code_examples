<?php

namespace PrismProductsManager;

use PrismProductsManager\Controller\HistoryController;
use PrismProductsManager\Controller\RestfulAPI\ProductApiController;
use PrismProductsManager\Controller\RestfulAPI\WebCategoryAPIController;
use PrismProductsManager\Factory\Controller\HistoryControllerFactory;
use PrismProductsManager\Factory\Controller\RestfulAPI\ProductApiControllerFactory;
use PrismProductsManager\Factory\Controller\RestfulAPI\WebCategoryAPIControllerFactory;
use Zend\Mvc\Router\Http\Segment;

return [
    'controllers' => [
        'invokables' => [
            'PrismProductsManager\Controller\ApiAdminCategories'     => 'PrismProductsManager\Controller\ApiAdminCategoriesController',
            'PrismProductsManager\Controller\ApiAttributes'          => 'PrismProductsManager\Controller\ApiAttributesController',
            'PrismProductsManager\Controller\CurrencyConverter'      => 'PrismProductsManager\Controller\CurrencyConverterController',
            'PrismProductsManager\Controller\ApiProductByUrlName'    => 'PrismProductsManager\Controller\ApiProductByUrlNameController',
            'PrismProductsManager\Command\CleanupAttachmentsCommand' => 'PrismProductsManager\Command\CleanupAttachmentsCommand',
        ],
        'factories'  => [
            'PrismProductsManager\Controller\ApiProducts'                                  => 'PrismProductsManager\Controller\ApiProductsController',
            'PrismProductsManager\Controller\Categories'                                   => 'PrismProductsManager\Factory\Controller\CategoriesControllerFactory',
            'PrismProductsManager\Controller\MerchantAttributes'                           => 'PrismProductsManager\Factory\Controller\MerchantAttributesControllerFactory',
            'PrismProductsManager\Controller\MarketingAttributes'                          => 'PrismProductsManager\Factory\Controller\MarketingAttributesControllerFactory',
            'PrismProductsManager\Controller\CoreAttributes'                               => 'PrismProductsManager\Factory\Controller\CoreAttributesControllerFactory',
            'PrismProductsManager\Controller\CommonAttributes'                             => 'PrismProductsManager\Factory\Controller\CommonAttributesControllerFactory',
            'PrismProductsManager\Controller\AssignFieldsToProductTabs'                    => 'PrismProductsManager\Factory\Controller\AssignFieldsToProductTabsControllerFactory',
            'PrismProductsManager\Controller\Colours'                                      => 'PrismProductsManager\Factory\Controller\ColoursControllerFactory',
            'PrismProductsManager\Controller\Products'                                     => 'PrismProductsManager\Factory\Controller\ProductsControllerFactory',
            'PrismProductsManager\Controller\MerchantCategories'                           => 'PrismProductsManager\Factory\Controller\MerchantCategoriesControllerFactory',
            'PrismProductsManager\Controller\ApiProductsByCategoriesId'                    => 'PrismProductsManager\Factory\Controller\ApiProductsByCategoriesIdFactory',
            'PrismProductsManager\Controller\ApiSitemap'                                   => 'PrismProductsManager\Factory\Controller\ApiSitemapControllerFactory',
            'PrismProductsManager\Controller\ApiProductsByCategoriesName'                  => 'PrismProductsManager\Factory\Controller\ApiProductsByCategoriesNameFactory',
            'PrismProductsManager\Controller\ApiCategoriesById'                            => 'PrismProductsManager\Factory\Controller\ApiCategoriesByIdFactory',
            'PrismProductsManager\Controller\ApiCategoriesLeftSidebar'                     => 'PrismProductsManager\Factory\Controller\ApiCategoriesLeftSidebarFactory',
            'PrismProductsManager\Controller\ApiCategoriesByName'                          => 'PrismProductsManager\Factory\Controller\ApiCategoriesByNameFactory',
            'PrismProductsManager\Controller\ApiCategories'                                => 'PrismProductsManager\Factory\Controller\ApiCategoriesFactory',
            'PrismProductsManager\Controller\ApiSizeGuide'                                 => 'PrismProductsManager\Factory\Controller\ApiSizeGuideFactory',
            'PrismProductsManager\Controller\ApiStockLevel'                                => 'PrismProductsManager\Factory\Controller\ApiStockLevelFactory',
            'PrismProductsManager\Controller\Tax'                                          => 'PrismProductsManager\Factory\Controller\TaxControllerFactory',
            'PrismProductsManager\Controller\ProductsMigration'                            => 'PrismProductsManager\Factory\Controller\ProductsMigrationControllerFactory',
            'PrismProductsManager\Controller\NauticaliaProductsMigration'                  => 'PrismProductsManager\Factory\Controller\NauticaliaProductsMigrationControllerFactory',
            'PrismProductsManager\Controller\BarcodeUpload'                                => 'PrismProductsManager\Factory\Controller\BarcodeUploadControllerFactory',
            'PrismProductsManager\Controller\ApiVoucherProduct'                            => 'PrismProductsManager\Factory\Controller\ApiVoucherProductFactory',
            'PrismProductsManager\Controller\ApiVoucherProductBySpectrumId'                => 'PrismProductsManager\Factory\Controller\ApiVoucherProductBySpectrumIdFactory',
            'PrismProductsManager\Controller\BulkActions'                                  => 'PrismProductsManager\Factory\Controller\BulkActionsControllerFactory',
            'PrismProductsManager\Controller\BulkUploadController'                         => 'PrismProductsManager\Factory\Controller\BulkUploadControllerFactory',
            'PrismProductsManager\Controller\Schedule'                                     => 'PrismProductsManager\Factory\Controller\ScheduleControllerFactory',
            'PrismProductsManager\Controller\RestfulAPI\ProductDataAPIController'          => 'PrismProductsManager\Factory\Controller\RestfulAPI\ProductDataAPIControllerFactory',
            'PrismProductsManager\Controller\RestfulAPI\StandardProductDataAPIController'  => 'PrismProductsManager\Factory\Controller\RestfulAPI\StandardProductDataAPIControllerFactory',
            'PrismProductsManager\Controller\RestfulAPI\ProductPriceAPIController'         => 'PrismProductsManager\Factory\Controller\RestfulAPI\ProductPriceAPIControllerFactory',
            'PrismProductsManager\Controller\RestfulAPI\StandardProductPriceAPIController' => 'PrismProductsManager\Factory\Controller\RestfulAPI\StandardProductPriceAPIControllerFactory',
            'PrismProductsManager\Controller\RestfulAPI\UpdateProductAttributesController' => 'PrismProductsManager\Factory\Controller\RestfulAPI\UpdateProductAttributesControllerFactory',
            'PrismProductsManager\Command\ProductImportCommand'                            => 'PrismProductsManager\Factory\Command\ProductImportCommandFactory',
            'PrismProductsManager\Controller\DataTables\DataTables'                        => 'PrismProductsManager\Factory\Controller\DataTables\DataTablesControllerFactory',
            WebCategoryAPIController::class                                                => WebCategoryAPIControllerFactory::class,
            ProductApiController::class                                                    => ProductApiControllerFactory::class,
            Controller\BulkProcessesController::class                                      => Factory\Controller\BulkProcessesControllerFactory::class,
            'PrismProductsManager\Command\ProcessBulkRecordsCommand'                       => 'PrismProductsManager\Factory\Command\ProcessBulkRecordsCommandFactory',
            'PrismProductsManager\Command\BulkErrorReportCommand'                          => 'PrismProductsManager\Factory\Command\BulkErrorReportCommandFactory',
            'PrismProductsManager\Command\AutomaticFromDateCommand'                         => 'PrismProductsManager\Factory\Command\AutomaticFromDateCommandFactory',
            HistoryController::class => HistoryControllerFactory::class
        ],
    ],

    'service_manager'    => [
        'invokables' => [
            'PrismProductsManager\Form\AddCategoryForm'                     => 'PrismProductsManager\Form\AddCategoryForm',
            'PrismProductsManager\Form\VariantsBasicInformationForm'        => 'PrismProductsManager\Form\VariantsBasicInformationForm',
            'PrismProductsManager\Form\AddVariantToCategoryForm'            => 'PrismProductsManager\Form\AddVariantToCategoryForm',
            'PrismProductsManager\Form\BulkProductActionForm'               => 'PrismProductsManager\Form\BulkProductActionForm',
            'PrismProductsManager\Form\ProductsCreateCollationVariantsForm' => 'PrismProductsManager\Form\ProductsCreateCollationVariantsForm',
            'PrismProductsManager\Form\ProductsTabsForm'                    => 'PrismProductsManager\Form\ProductsTabsForm',
        ],
        'factories'  => [
            'NauticaliaProductsMigrationMapper'                      => 'PrismProductsManager\Factory\Model\NauticaliaProductsMigrationMapperFactory',
            'PrismProductsManager\Form\AddAttributesMarketingForm'   => 'PrismProductsManager\Factory\Form\AddAttributesMarketingFormFactory',
            'PrismProductsManager\Form\AddMerchantCategoryForm'      => 'PrismProductsManager\Factory\Form\AddMerchantCategoryFormFactory',
            'PrismProductsManager\Form\AddNewTaxForm'                => 'PrismProductsManager\Factory\Form\AddNewTaxFormFactory',
            'PrismProductsManager\Form\AssignInputFieldsForm'        => 'PrismProductsManager\Factory\Form\AssignInputFieldsFormFactory',
            'PrismProductsManager\Form\ColoursForm'                  => 'PrismProductsManager\Factory\Form\ColoursFormFactory',
            'PrismProductsManager\Form\CommonAttributesForm'         => 'PrismProductsManager\Factory\Form\CommonAttributesFormFactory',
            'PrismProductsManager\Form\CommonAttributesValuesForm'   => 'PrismProductsManager\Factory\Form\CommonAttributesValuesFormFactory',
            'PrismProductsManager\Form\InputFieldValidationForm'     => 'PrismProductsManager\Factory\Form\InputFieldValidationFormFactory',
            'PrismProductsManager\Form\MarketingCategorySelectForm'  => 'PrismProductsManager\Factory\Form\MarketingCategorySelectFormFactory',
            'PrismProductsManager\Form\MerchantCategorySelectForm'   => 'PrismProductsManager\Factory\Form\MerchantCategorySelectFormFactory',
            'PrismProductsManager\Form\ProductAttributesSelectForm'  => 'PrismProductsManager\Factory\Form\ProductAttributesSelectFormFactory',
            'PrismProductsManager\Form\ProductPricesSelectForm'      => 'PrismProductsManager\Factory\Form\ProductPricesSelectFormFactory',
            'PrismProductsManager\Form\ProductsActivationForm'       => 'PrismProductsManager\Factory\Form\ProductsActivationFormFactory',
            'PrismProductsManager\Form\ProductsAddOnForm'            => 'PrismProductsManager\Factory\Form\ProductsAddOnFormFactory',
            'PrismProductsManager\Form\ProductsBasicInformationForm' => 'PrismProductsManager\Factory\Form\ProductsBasicInformationFormFactory',
            'PrismProductsManager\Form\ProductsRelatedProductForm'   => 'PrismProductsManager\Factory\Form\ProductsRelatedProductFormFactory',
            'PrismProductsManager\Form\ProductSupplierPriceForm'     => 'PrismProductsManager\Factory\Form\ProductSupplierPriceFormFactory',
            'PrismProductsManager\Form\ProductVariantCreationForm'   => 'PrismProductsManager\Factory\Form\ProductVariantCreationFormFactory',
            'PrismProductsManager\Form\TerritoryOverrideForm'        => 'PrismProductsManager\Factory\Form\TerritoryOverrideFormFactory',
            'PrismProductsManager\Model\ApiCategoriesMapper'         => 'PrismProductsManager\Factory\Model\ApiCategoriesMapperFactory',
            'PrismProductsManager\Model\ApiProductsMapper'           => 'PrismProductsManager\Factory\Model\ApiProductsMapperFactory',
            'PrismProductsManager\Model\AttributesGroupMapper'       => 'PrismProductsManager\Factory\Model\AttributesGroupMapperFactory',
            'PrismProductsManager\Model\BulkActionsMapper'           => 'PrismProductsManager\Factory\Model\BulkActionsMapperFactory',
            'PrismProductsManager\Model\CategoriesDefinitionsMapper' => 'PrismProductsManager\Factory\Model\CategoriesDefinitionsMapper',
            'PrismProductsManager\Model\CategoriesMapper'            => 'PrismProductsManager\Factory\Model\CategoriesMapperFactory',
            'PrismProductsManager\Model\CategoriesTable'             => 'PrismProductsManager\Factory\Model\CategoriesTableFactory',
            'PrismProductsManager\Model\ColoursMapper'               => 'PrismProductsManager\Factory\Model\ColoursMapperFactory',
            'PrismProductsManager\Model\CommonAttributesMapper'      => 'PrismProductsManager\Factory\Model\CommonAttributesMapperFactory',
            'PrismProductsManager\Model\CurrencyMapper'              => 'PrismProductsManager\Factory\Model\CurrencyMapperFactory',
            'PrismProductsManager\Model\MerchantCategoriesMapper'    => 'PrismProductsManager\Factory\Model\MerchantCategoriesMapperFactory',
            'PrismProductsManager\Model\ProductsMapper'              => 'PrismProductsManager\Factory\Model\ProductsMapperFactory',
            'PrismProductsManager\Model\SizesShoesMapper'            => 'PrismProductsManager\Factory\Model\SizesShoesMapperFactory',
            'PrismProductsManager\Model\TaxMapper'                   => 'PrismProductsManager\Factory\Model\TaxMapperFactory',
            'PrismProductsManager\Service\AssignFieldToTabsService'  => 'PrismProductsManager\Factory\Service\AssignFieldToTabsServiceFactory',
            'PrismProductsManager\Service\CommonAttributesService'   => 'PrismProductsManager\Factory\Service\CommonAttributesServiceFactory',
            'PrismProductsManager\Service\InputService'              => 'PrismProductsManager\Factory\Service\InputServiceFactory',
            'PrismProductsManager\Service\ProductsUpdateService'     => 'PrismProductsManager\Factory\Service\ProductsUpdateServiceFactory',
            'PrismProductsManager\Service\ScheduleService'           => 'PrismProductsManager\Factory\Service\ScheduleServiceFactory',
            'PrismProductsManager\Service\WorkflowService'           => 'PrismProductsManager\Factory\Service\WorkflowServiceFactory',
            'ProductCategoriesMapper'                                => 'PrismProductsManager\Factory\Model\ProductCategoriesMapperFactory',
            'ProductImport'                                          => 'PrismProductsManager\Factory\Service\ProductImportServiceFactory',
            'ProductsMigrationMapper'                                => 'PrismProductsManager\Factory\Model\ProductsMigrationMapperFactory',
            'PrismProductsManager\Service\HistoricalService'         => 'PrismProductsManager\Factory\Service\HistoricalServiceFactory',
            'LoggingService'                                         => 'PrismProductsManager\Factory\Service\LoggingServiceFactory',

        ],
    ],
    'form_elements'      => [
        'invokables' => [
            'Zend\Form\Element\Number' => 'PrismProductsManager\Form\Element\Number',
        ],
        'factories'  => [
            'PrismProductsManager\Form\InputFieldsForm' => 'PrismProductsManager\Factory\Form\InputFieldsFormFactory',
        ],
    ],
    'controller_plugins' => [
        'invokables' => [
            'IsManualSync' => 'PrismProductsManager\Controller\Plugin\IsManualSync',
        ],
    ],

    'view_helpers' => [
        'invokables' => [
            'formElementErrors' => 'PrismProductsManager\Form\View\Helper\FormElementErrors',
        ],
        'factories'  => [
            'getUserById'       => 'PrismProductsManager\Factory\View\GetUserByIdFactory',
            'manualSync'        => 'PrismProductsManager\Factory\View\ManualSyncViewHelperFactory',
            'skuIsEditable'     => 'PrismProductsManager\Factory\View\SkuIsEditableViewHelperFactory',
            'outputTableHeader' => 'PrismProductsManager\Factory\View\OutputTableHeaderViewHelperFactory',
        ],
    ],
    // The following section is new and should be added to your file
    'router'       => [
        'routes' => [
            // api call to get product data in style/option format
            'api-product-data'                   => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/api/product-data[/]',
                    'defaults' => [
                        'controller' => 'PrismProductsManager\Controller\RestfulAPI\ProductDataAPIController',
                    ],
                ],
            ],
            'api-update-product-attributes'      => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/api/update-product-attributes[/]',
                    'defaults' => [
                        'controller' => 'PrismProductsManager\Controller\RestfulAPI\UpdateProductAttributesController',
                    ],
                ],
            ],
            'standard-api-product-data'          => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/api/standard-product-data[/]',
                    'defaults' => [
                        'controller' => 'PrismProductsManager\Controller\RestfulAPI\StandardProductDataAPIController',
                    ],
                ],
            ],
            // api call to get product data in style/option format
            'api-price-data'                     => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/api/price-data[/]',
                    'defaults' => [
                        'controller' => 'PrismProductsManager\Controller\RestfulAPI\ProductPriceAPIController',
                    ],
                ],
            ],
            // api call to get product data in style/option format
            'api-standard-price-data'            => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/api/standard-price-data[/]',
                    'defaults' => [
                        'controller' => 'PrismProductsManager\Controller\RestfulAPI\StandardProductPriceAPIController',
                    ],
                ],
            ],
            // api calls to get all categories
            'api-categories'                     => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/api/categories[/:hierarchy][/]',
                    'defaults' => [
                        'controller' => 'PrismProductsManager\Controller\ApiCategories',
                    ],
                ],
            ],
            // api calls to get size guide
            'api-size-guide'                     => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'       => '/api/size-guide[/:category][/:table][/]',
                    'constraints' => [
                        'category' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'table'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\ApiSizeGuide',
                    ],
                ],
            ],
            // api calls to receive stock level from spectrum
            'api-stock-update'                   => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'       => '/api/stock-update[/]',
                    'constraints' => [
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\ApiStockLevel',
                    ],
                ],
            ],
            // api calls to get sitemap data
            'site-map'                           => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'       => '/api/get-sitemap-data[/]',
                    'constraints' => [
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\ApiSitemap',
                    ],
                ],
            ],
            // api call to get the evoucher product
            'api-voucher-product'                => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'       => '/api/voucher-product[/:websiteId][/:languageId][/]',
                    'constraints' => [
                        'websiteId'  => '[0-9]+',
                        'languageId' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\ApiVoucherProduct',
                    ],
                ],
            ],
            // api call to get the evoucher product
            'api-voucher-product-by-spectrum-id' => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'       => '/api/voucher-product-by-spectrum-id[/:spectrumId][/]',
                    'constraints' => [
                        'spectrumId' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\ApiVoucherProductBySpectrumId',
                    ],
                ],
            ],

            // api calls to get categories by id
            'api-categories-id'                  => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'       => '/api/categories/by-id[/:id][/:website][/:language]',
                    'constraints' => [
                        'id' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\ApiCategoriesById',
                    ],
                ],
            ],
            // api calls to left sidebar
            'api-left-sidebar'                   => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/api/categories/by-left-side-bar[/:id][/:website][/:language][/]',
                    'defaults' => [
                        'controller' => 'PrismProductsManager\Controller\ApiCategoriesLeftSidebar',
                    ],
                ],
            ],
            // api calls to get categories by name
            'api-categories-name'                => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/api/categories/by-url-name[/:id][/:website][/:language][/]',
                    'defaults' => [
                        'controller' => 'PrismProductsManager\Controller\ApiCategoriesByName',
                    ],
                ],
            ],

            // api calls to get products categories id
            'api-products-by-category-id'        => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'       => '/api/products/by-category-id[/:id][/:website][/:language][/]',
                    'constraints' => [
                        'id' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\ApiProductsByCategoriesId',
                    ],
                ],
            ],
            // api calls to get products categories name
            'api-products-by-category-name'      => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'    => '/api/products/by-category-name[/:id][/:website][/:language][/:limit][/:page][/]',
                    'defaults' => [
                        'controller' => 'PrismProductsManager\Controller\ApiProductsByCategoriesName',
                    ],
                ],
            ],

            // api calls to get products
            'api-attributes'                     => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'       => '/api/attributes[/:id][/]',
                    'constraints' => [
                        'id' => '\w+' //'[0-9]+'
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\ApiAttributes',
                    ],
                ],
            ],

            //API call to get the product details by rulName
            'api-product-by-url-name'            => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'       => '/api/product/url[/:urlName][/:websiteId][/:languageId][/:id][/]',
                    'constraints' => [
                        'id'         => '[0-9]+',
                        'websiteId'  => '[0-9]+',
                        'languageId' => '[0-9]+',
                        'urlName'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\ApiProductByUrlName',
                    ],
                ],
            ],

            //API call to update product prices and status
            'api-update-product'                 => [
                'type'    => 'Zend\Mvc\Router\Http\Segment',
                'options' => [
                    'route'       => '/api/product-update[/:id][/]',
                    'constraints' => [
                        'id' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\ApiProducts',
                    ],
                ],
            ],

            // manage products
            'products'                           => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/products[/:action][/:id][/:stage][/:displayPage][/style/:style][/option/:option][/]',
                    'constraints' => [
                        'action'      => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'          => '[0-9]+',
                        'stage'       => '[0-9]+',
                        'displayPage' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\Products',
                        'action'     => 'index',
                    ],
                ],
            ],

            // Schedule route events
            'schedule'                           => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/schedule[/:action][/display/:displayPage][/]',
                    'constraints' => [
                        'action'      => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'displayPage' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\Schedule',
                        'action'     => 'index',
                    ],
                ],
            ],


            'bulk-product-variants' => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/products/bulk[/:task][/:id][/:variants][/]',
                    'constraints' => [
                        'task'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'       => '[0-9]+',
                        'variants' => '[0-9_-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\Products',
                        'action'     => 'bulk',
                    ],
                ],
            ],

            'products_add_edit' => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/products[/:param][/:id][/:stage][/success/:success][/variantId/:variantId][/showcategorypopup/:showcategorypopup][/]',
                    'constraints' => [
                        'param'             => '(\badd\b)|(\bedit\b)',
                        'id'                => '[0-9]+',
                        'stage'             => '[0-9]+',
                        'success'           => '[0-1]{1}',
                        'variantId'         => '[0-9]+',
                        'showcategorypopup' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\Products',
                        'action'     => 'addEdit',
                    ],
                ],
            ],

            'get_all_saved_variants' => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/products/get-all-saved-variants[/:id]',
                    'constraints' => [
                        'id'       => '[0-9]+',
                        'viewOnly' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\Products',
                        'action'     => 'getAllSavedVariants',
                    ],
                ],
            ],


            'get_variant_attributes' => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/products/get-variant-attributes[/:id][/:variantId][/][/:viewOnly]',
                    'constraints' => [
                        'id'        => '[0-9]+',
                        'viewOnly'  => '[0-9]+',
                        'variantId' => '[0-9]+[0-9-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\Products',
                        'action'     => 'getVariantAttributes',
                    ],
                ],
            ],


            'get-variant-marketing-categories' => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/products/get-variant-marketing-categories[/:id][/:variantId][/][/:viewOnly]',
                    'constraints' => [
                        'id'        => '[0-9]+',
                        'viewOnly'  => '[0-9]+',
                        'variantId' => '[0-9]+[0-9-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\Products',
                        'action'     => 'getVariantMarketingCategories',
                    ],
                ],
            ],
            'searchProductsAction'             => [
                'type'    => 'segment',
                'options' => [
                    'route'    => '/products/searchProducts[/:searchTerms][/]',
                    'defaults' => [
                        'controller' => 'PrismProductsManager\Controller\Products',
                        'action'     => 'searchProducts',
                    ],
                ],
            ],
            'get_variant_prices'               => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/products/get-variant-prices[/:id][/:variantId][/][/:viewOnly]',
                    'constraints' => [
                        'id'        => '[0-9]+',
                        'viewOnly'  => '[0-9]+',
                        'variantId' => '[0-9]+[0-9-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\Products',
                        'action'     => 'getVariantPrices',
                    ],
                ],
            ],
            'change_variant_tax'               => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/products/change-variant-tax[/:id][/:variantId][/][/:viewOnly]',
                    'constraints' => [
                        'id'        => '[0-9]+',
                        'viewOnly'  => '[0-9]+',
                        'variantId' => '[0-9]+[0-9-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\Products',
                        'action'     => 'changeVariantTax',
                    ],
                ],
            ],
            'products_index'                   => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/products[/search/:search][/page/:page][/orderby/:orderby][/:orderbyoption][/]',
                    'constraints' => [
                        'page'          => '[0-9]+',
                        'orderby'       => '(productName|status)',
                        'orderbyoption' => '(asc|desc)',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\Products',
                        'action'     => 'index',
                    ],
                ],
            ],
            // manage taxes
            'tax'                              => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/tax[/][:action][/:id][/search/:search][/page/:page]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'page'   => '[0-9]+',
                        'id'     => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\Tax',
                        'action'     => 'index',
                    ],
                ],
            ],
            // manage colours
            'colours'                          => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/colours[/:action][/:id][/]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\Colours',
                        'action'     => 'index',
                    ],
                ],
            ],

            //Add route for marketing categories (product groups).
            'marketing-categories'             => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/marketing-categories[/:action][/:id][/page/:page][/search/:search][/orderby/:orderby][/:orderbyoption][/categoryid/:categoryid][/:position][/]',
                    'constraints' => [
                        'action'        => '(?!\bpage\b)(?!\bsearch\b)(?!\borderby\b)[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'            => '[0-9]+',
                        'position'      => '[0-9]+',
                        'page'          => '[0-9]+',
                        'orderby'       => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'orderbyoption' => 'ASC|DESC',
                        'categoryid'    => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\Categories',
                        'action'     => 'index',
                    ],
                ],
            ],
            //Add route for merchant categories (product groups).
            'merchant-categories'              => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/merchant-categories[/:action][/:id][/page/:page][/orderby/:orderby][/:orderbyoption][/search/:search][/]',
                    'constraints' => [
                        'action'        => '(?!\bpage\b)(?!\bsearch\b)(?!\borderby\b)(?!\borderbyoption\b)[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'            => '[0-9]+',
                        'page'          => '[0-9]+',
                        'orderby'       => '(categoryId|categoryName|longDescription|locked|status)',
                        'orderbyoption' => '(asc|desc)',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\MerchantCategories',
                        'action'     => 'index',
                    ],
                ],
            ],

            //Add route for merchant attributes.
            'merchant-attributes'              => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/merchant-attributes[/:action][/page/:page][/orderby/:orderby][/:orderbyoption][/search/:search][/:id][/:delete][/:from][/]',
                    'constraints' => [
                        'action'        => '(?!\bpage\b)(?!\bsearch\b)(?!\borderby\b)(?!\borderbyoption\b)[a-zA-Z][a-zA-Z0-9_-]*',
                        'delete'        => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'orderby'       => '(name|viewAs|groupType|usedForFiltering|groupCode)',
                        'orderbyoption' => '(asc|desc)',
                        'page'          => '[0-9]+',
                        'id'            => '[0-9]+',
                        'from'          => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\MerchantAttributes',
                        'action'     => 'index',
                    ],
                ],
            ],
            //Add route for marketing attributes.
            'marketing-attributes'             => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/marketing-attributes[/:action][/page/:page][/orderby/:orderby][/:orderbyoption][/search/:search][/:id][/:delete][/:from][/]',
                    'constraints' => [
                        'action'        => '(?!\bpage\b)(?!\borderby\b)(?!\borderbyoption\b)(?!\bsearch\b)(?!\border_by\b)[a-zA-Z][a-zA-Z0-9_-]*',
                        'delete'        => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'orderby'       => '(name|viewAs|groupType|usedForFiltering)',
                        'orderbyoption' => '(asc|desc)',
                        'page'          => '[0-9]+',
                        'id'            => '[0-9]+',
                        'from'          => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\MarketingAttributes',
                        'action'     => 'index',
                    ],
                ],
            ],
            //Add route for core attributes.
            'core-attributes'                  => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/core-attributes[/:action][/page/:page][/orderby/:orderby][/:orderbyoption][/search/:search][/:id][/]',
                    'constraints' => [
                        'action'        => '(?!\bpage\b)(?!\borderby\b)(?!\borderbyoption\b)(?!\bsearch\b)[a-zA-Z][a-zA-Z0-9_-]*',
                        'delete'        => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'orderby'       => '(name|viewAs|groupType|usedForFiltering)',
                        'orderbyoption' => '(asc|desc)',
                        'page'          => '[0-9]+',
                        'id'            => '[0-9]+',
                        'from'          => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\CoreAttributes',
                        'action'     => 'index',
                    ],
                ],
            ],
            //Add route for common attributes.
            'common-attributes'                => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/common-attributes[/:action][/search/:search][/page/:page][/attributeId/:attributeId][/valueId/:valueId][/search/:search][/set-default/:set-default][/]',
                    'constraints' => [
                        'action'      => '(?!\bpage\b)(?!\bsearch\b)[a-zA-Z][a-zA-Z0-9_-]*',
                        'search'      => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'page'        => '[0-9]+',
                        'attributeId' => '[0-9]+',
                        'valueId'     => '[0-9]+',
                        'set-default' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\CommonAttributes',
                        'action'     => 'index',
                    ],
                ],
            ],

            //Add route to assign fields to product tabs.
            'assign-fields-to-tabs'            => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/assign-fields-to-tabs[/:action][/:inputFieldId][/]',
                    'constraints' => [
                        'action'       => '(?!\bpage\b)(?!\bsearch\b)[a-zA-Z][a-zA-Z0-9_-]*',
                        'inputFieldId' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\AssignFieldsToProductTabs',
                        'action'     => 'index',
                    ],
                ],
            ],

            //Add route for attributes.
            'currency-convert'                 => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/currency/converter[/:action][/page/:page][/]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'delete' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'page'   => '[0-9]+',
                        'id'     => '[0-9]+',
                        'from'   => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\CurrencyConverter',
                        'action'     => 'index',
                    ],
                ],
            ],

            //Add route for attributes.
            'product-migration'                => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/product-migration[/:action][/]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\ProductsMigration',
                        'action'     => 'index',
                    ],
                ],
            ],

            //Add route for attributes.
            'nauticalia-product-migration'     => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/nauticalia-product-migration[/:action][/]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\NauticaliaProductsMigration',
                        'action'     => 'index',
                    ],
                ],
            ],

            'barcode-upload'                     => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/barcode-upload[/:action][/]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\BarcodeUpload',
                        'action'     => 'index',
                    ],
                ],
            ],
            // Bulk Actions
            'bulk-actions'                       => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/bulk-actions[/:action][/]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\BulkActions',
                        'action'     => 'index',
                    ],
                ],
            ],
            // Bulk Upload
            'bulk-upload'                        => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/bulk-upload[/:action][/]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\BulkUploadController',
                        'action'     => 'index',
                    ],
                ],
            ],
            // Server Side processing for data tables
            'server-side-processing-data-tables' => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/products/server-side-processing',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\DataTables\DataTables',
                        'action'     => 'serverSideProcessing',
                    ],
                ],
            ],

            // Server Side processing for data tables
            'products-migration-from-feed'       => [
                'type'    => 'segment',
                'options' => [
                    'route'       => '/migrate-attributes-from-feed',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults'    => [
                        'controller' => 'PrismProductsManager\Controller\ProductsMigration',
                        'action'     => 'migrateAttributesFromFeed',
                    ],
                ],
            ],

            'api-web-category' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/api/web-category[/:id]',
                    'constraints' => [
                        'id' => '[1-9][0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => WebCategoryAPIController::class,
                    ],
                ],
            ],

            'api-product' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/api/products[/:id]',
                    'constraints' => [
                        'id' => '[1-9][0-9]*',
                    ],
                    'defaults'    => [
                        'controller' => ProductApiController::class,
                    ],
                ],
            ],
            
            'bulk-processes' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/bulk-processes[/][:action][/page/:page]',
                    'defaults'    => [
                        'controller' => Controller\BulkProcessesController::class,
                        'action'     => 'index',
                        'constraints' => [
                            'page'        => '[0-9]+',
                        ],
                    ],
                ],
            ],

            'history' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/history[/][:action]',
                    'defaults'    => [
                        'controller' => HistoryController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

        ],
    ],
    'console'      => [
        'router' => [
            'routes' => [
                'schedule-events-process'          => [
                    'options' => [
                        'route'    => 'schedule-events-process',
                        'defaults' => [
                            'controller' => 'PrismProductsManager\Controller\Schedule',
                            'action'     => 'scheduleEventsProcess',
                        ],
                    ],
                ],
                'product-import-command'           => [
                    'options' => [
                        'route'    => 'products-import <filePath>',
                        'defaults' => [
                            'controller' => 'PrismProductsManager\Command\ProductImportCommand',
                            'action'     => 'productImport',
                        ],
                    ],
                ],
                'product-truncate-command'         => [
                    'options' => [
                        'route'    => 'products-import-truncate <confirm>',
                        'defaults' => [
                            'controller' => 'PrismProductsManager\Command\ProductImportCommand',
                            'action'     => 'productImportTruncate',
                        ],
                    ],
                ],
                'import-colours'                   => [
                    'options' => [
                        'route'    => 'import-colours',
                        'defaults' => [
                            'controller' => 'PrismProductsManager\Command\ProductImportCommand',
                            'action'     => 'coloursImport',
                        ],
                    ],
                ],
                'update-abbrev-values'             => [
                    'options' => [
                        'route'    => 'updateAbbrevCode',
                        'defaults' => [
                            'controller' => 'PrismProductsManager\Command\ProductImportCommand',
                            'action'     => 'updateAbbrevCode',
                        ],
                    ],
                ],
                'clean-up-attachments'             => [
                    'options' => [
                        'route'    => 'clean-up-attachments',
                        'defaults' => [
                            'controller' => 'PrismProductsManager\Command\CleanupAttachmentsCommand',
                            'action'     => 'bulk',
                        ],
                    ],
                ],
                'products-migration-from-spectrum' => [
                    'options' => [
                        'route'    => 'migrate-spectrum-products',
                        'defaults' => [
                            'controller' => 'PrismProductsManager\Controller\ProductsMigration',
                            'action'     => 'migrateProductsFromSpectrum',
                        ],
                    ],
                ],
                'process-bulk-records' => [
                    'options' => [
                        'route'    => 'process-bulk-records',
                        'defaults' => [
                            'controller' => 'PrismProductsManager\Command\ProcessBulkRecordsCommand',
                            'action'     => 'process',
                        ],
                    ],
                ],
                'process-notify-sip-and-spectrum' => [
                    'options' => [
                        'route'    => 'process-notify-sip-and-spectrum',
                        'defaults' => [
                            'controller' => 'PrismProductsManager\Command\ProcessBulkRecordsCommand',
                            'action'     => 'processNotifySipAndSpectrum',
                        ],
                    ],
                ],
                'bulk-error-report' => [
                    'options' => [
                        'route'    => 'bulk-error-report',
                        'defaults' => [
                            'controller' => 'PrismProductsManager\Command\BulkErrorReportCommand',
                            'action'     => 'send',
                        ],
                    ],
                ],
                'automatic-from-date' => [
                    'options' => [
                        'route'    => 'automatic-from-date',
                        'defaults' => [
                            'controller' => 'PrismProductsManager\Command\AutomaticFromDateCommand',
                            'action'     => 'process',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'PrismProductsManager' => __DIR__ . '/../view',
        ],
    ],

    'doctrine' => [
        'driver' => [
            'ProductsMangerDriver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/PrismProductsManager/Entity'],
            ],
            'orm_default'          => [
                'drivers' => [
                    'PrismProductsManager\Entity' => 'ProductsMangerDriver',
                ],
            ],
        ],
    ],
];
