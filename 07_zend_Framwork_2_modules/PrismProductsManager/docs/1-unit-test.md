Unit Test
=========

Introduction
------------

<code>PrismProductsManager</code> is a module that live in PrismCMS repository. To test <code>PrismProductsManager</code>, we added some dependencies in root application's composer.json under require-dev:

```
// ...
"require-dev": {
  // ...
  "phpunit/phpunit": "4.*",
  "phpunit/phpcov": "~2.0"
  // ...
},
// ...
```
On the dependencies above, in test's Bootstrap, we added them <code>test/PHPUnit/Bootstrap.php::init()</code>

```php
public static function init()
{
    // ...
    $config = [
        'module_listener_options' => [
            'module_paths' => $zf2ModulePaths,
        ],
        'modules' => [
            'PrismCommon',
            'PrismAuth',
            'PrismAuthorize',
            'PrismContentManager',
            'PrismQueueManager',
            'PrismMediaManager',
            'PrismSpectrumApiClient',
            'PrismElasticSearch',
            'PrismProductsManager',
        ],
    ];
    // ...
}
```

Fixture Test
------------
To work with fixture, we need assets for "TEST" db (new DB for test) up called when running test and db down called tearDown, we can define in <code>test/PHPUnit/ZendDbFixture/assets</code> so when running test, we can do :

```
public function testGetPaginatorForSelect($nrPerPage)
{
    $this->fixtureManager->execute('users-up.sql');

    $this->service->method();
}

protected function tearDown()
{
    $this->fixtureManager->execute('users-down.sql');
}
```

Run Test
--------

As there are SQLs execution to be tested, we need a real DB for fixture, we can prepare "TEST db", for example :
```
$ mysql -u root
$ create database ADMINPANELTEST
```
After that, we can copy <code>test/PHPUnit/config/autoload/db.config.php.dist</code>, to <code>test/PHPUnit/config/autoload/db.config.php</code> under PrismProductsManager:

```
$ cd /path/to/root/app/module/PrismProductsManager
$ cp test/PHPUnit/config/autoload/db.local.php.dist test/PHPUnit/config/autoload/db.local.php
```

and configure <code>test/PHPUnit/config/autoload/db.local.php</code> based on our "TEST db".

And run :

```
$ cd /path/to/root/app
$ composer update
```

After that, we can run phpunit :
```
$ vendor/bin/phpunit -c module/PrismProductsManager/test/PHPUnit/phpunit.xml module/PrismProductsManager/test/PHPUnit/
```

After run phpunit, we can see html coverage on <code>data/test-html-report/prismproductsmanager/index.html</code>
