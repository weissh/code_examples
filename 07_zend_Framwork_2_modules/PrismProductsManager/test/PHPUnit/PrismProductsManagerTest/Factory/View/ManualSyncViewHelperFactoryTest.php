<?php

namespace PrismProductsManagerTest\Factory\View;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\View\ManualSyncViewHelperFactory;
use PrismProductsManager\View\Helper\ManualSyncViewHelper;
use Zend\View\HelperPluginManager;

/**
 * @author haniw
 */
class ManualSyncViewHelperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ManualSyncViewHelperFactory */
    protected $factory;

    /** @var \Prophecy\Prophecy\ObjectProphecy */
    protected $serviceLocator;

    /** @var \Prophecy\Prophecy\ObjectProphecy */
    protected $helperManager;

    public function setUp()
    {
        $this->helperManager = $this->prophesize(HelperPluginManager::class);
        $this->serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');

        $factory = new ManualSyncViewHelperFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $this->serviceLocator->get('Config')
                             ->willReturn([])
                             ->shouldBeCalled();

        $this->helperManager->getServiceLocator()
                                ->willReturn($this->serviceLocator)
                                ->shouldBeCalled();

        $result = $this->factory->createService($this->helperManager->reveal());
        $this->assertInstanceOf(ManualSyncViewHelper::class, $result);
    }
}
