<?php

namespace PrismProductsManagerTest\Factory\View;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\View\GetUserByIdFactory;
use PrismProductsManager\View\Helper\GetUserById;
use Zend\View\HelperPluginManager;

/**
 * @author haniw
 */
class GetUserByIdFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var GetUserByIdFactory */
    protected $factory;

    /** @var \Prophecy\Prophecy\ObjectProphecy */
    protected $serviceLocator;

    /** @var \Prophecy\Prophecy\ObjectProphecy */
    protected $helperManager;

    public function setUp()
    {
        $this->helperManager = $this->prophesize(HelperPluginManager::class);
        $this->serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');

        $factory = new GetUserByIdFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $usersTable = $this->prophesize('PrismUsersManager\Model\UsersTable');
        $this->serviceLocator->get('PrismUsersManager\Model\UsersTable')
                             ->willReturn($usersTable)
                             ->shouldBeCalled();

        $this->helperManager->getServiceLocator()
                                ->willReturn($this->serviceLocator)
                                ->shouldBeCalled();

        $result = $this->factory->createService($this->helperManager->reveal());
        $this->assertInstanceOf(GetUserById::class, $result);
    }
}
