<?php

namespace PrismProductsManagerTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Model\ApiProductsMapperFactory;
use Zend\Db\Adapter\Platform\Mysql;

/**
 * @author haniw
 */
class ApiProductsMapperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiProductsMapperFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ApiProductsMapperFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                      ->disableOriginalConstructor()
                      ->getMock();
        $dbPms->expects($this->once())
                             ->method('getPlatform')
                             ->willReturn(new Mysql());
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('db-pms')
                        ->willReturn($dbPms);

        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $this->serviceLocator->expects($this->at(1))
                             ->method('get')
                             ->with('PrismProductsManager\Model\ProductsMapper')
                             ->willReturn($productsMapper);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Model\ApiProductsMapper', $result);
    }
}
