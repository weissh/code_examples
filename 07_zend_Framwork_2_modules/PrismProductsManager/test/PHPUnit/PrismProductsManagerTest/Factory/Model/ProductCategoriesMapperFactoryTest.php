<?php

namespace PrismProductsManagerTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Model\ProductCategoriesMapperFactory;
use PrismProductsManager\Model\CategoriesTable;

/**
 * @author haniw
 */
class ProductCategoriesMapperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductCategoriesMapperFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProductCategoriesMapperFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $tableGateway = $this->getMockBuilder('Zend\Db\TableGateway\TableGateway')
                             ->disableOriginalConstructor()
                             ->getMock();

        $categoriesTable = new CategoriesTable($tableGateway);
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('PrismProductsManager\Model\CategoriesTable')
                        ->willReturn($categoriesTable);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Model\ProductCategoriesMapper', $result);
    }
}
