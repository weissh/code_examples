<?php

namespace PrismProductsManagerTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Model\CategoriesMapperFactory;
use Zend\Db\Adapter\Platform\Mysql;

/**
 * @author haniw
 */
class CategoriesMapperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var CategoriesMapperFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new CategoriesMapperFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
            ->disableOriginalConstructor()
            ->getMock();
        $dbPms->expects($this->once())
            ->method('getPlatform')
            ->willReturn(new Mysql());
        $this->serviceLocator->expects($this->at(0))
            ->method('get')
            ->with('db-pms')
            ->willReturn($dbPms);

        $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
            ->disableOriginalConstructor()
            ->getMock();
        $config = [
            'languages' => [
                'default-language' => 1,
                'current-language' => 1,
                'site-languages' => [1],
            ],
            'websites' => [
                'current-website-id' => 1,
                'default-website' => [
                    1 => 'Uk Website',
                ],
                'websites' => [
                    1 => 'Uk Website',
                    2 => 'US Website',
                    3 => 'Europe Website',
                ],
            ],
            'pagination' => [
                'records-per-page' => 1,
            ],
        ];
        $commonStorage->expects($this->at(0))
            ->method('read')
            ->with('siteConfig')
            ->willReturn($config);
        $commonStorage->expects($this->at(1))
            ->method('read')
            ->with('clientConfig')
            ->willReturn([]);
        $this->serviceLocator->expects($this->at(1))
            ->method('get')
            ->with('CommonStorage')
            ->willReturn($commonStorage);

        $queryPagination = $this->getMockBuilder('PrismCommon\Service\QueryPagination')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(2))
            ->method('get')
            ->with('QueryPagination')
            ->wilLReturn($queryPagination);

        $updateNavigationService = $this->getMockBuilder('PrismContentManager\Service\UpdateNavigation')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(3))
            ->method('get')
            ->with('PrismContentManager\Service\UpdateNavigation')
            ->willReturn($updateNavigationService);

        $queueService = $this->getMockBuilder('PrismQueueManager\Service\QueueService')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(4))
            ->method('get')
            ->with('QueueService')
            ->willReturn($queueService);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Model\CategoriesMapper', $result);
    }
}
