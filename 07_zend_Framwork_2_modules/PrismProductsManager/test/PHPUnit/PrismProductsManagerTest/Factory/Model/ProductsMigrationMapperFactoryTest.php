<?php

namespace PrismProductsManagerTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Model\ProductsMigrationMapperFactory;
use Zend\Db\Adapter\Platform\Mysql;

/**
 * @author haniw
 */
class ProductsMigrationMapperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiCategoriesMapperFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProductsMigrationMapperFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $config = [
            'siteConfig' => [
                'languages' => [
                    'site-languages' => [1],
                ],
                'websites' => [
                    'websites' => [
                        1 => 'Uk Website',
                        2 => 'US Website',
                        3 => 'Europe Website',
                    ],
                    'current-website-id' => 1,
                    'website-url' => [
                        '1' => 'http://nauticalia-website.local/',
                        '2' => 'http://nauticalia-website.local/',
                        '3' => 'http://nauticalia-website.local/',
                    ],
                ],
                'currencies' => [
                    'site-currencies' => [1],
                ],
            ],
        ];
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('Config')
                        ->willReturn($config);

        $db = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                      ->disableOriginalConstructor()
                      ->getMock();
        $db->expects($this->any())
                             ->method('getPlatform')
                             ->willReturn(new Mysql());

        $this->serviceLocator->expects($this->at(1))
                        ->method('get')
                        ->with('db-pms')
                        ->willReturn($db);

        $this->serviceLocator->expects($this->at(2))
                        ->method('get')
                        ->with('old-pms')
                        ->willReturn($db);

        $this->serviceLocator->expects($this->at(3))
                        ->method('get')
                        ->with('db-admin')
                        ->willReturn($db);

        $this->serviceLocator->expects($this->at(4))
                        ->method('get')
                        ->with('db-spectrum')
                        ->willReturn($db);

        $productFolderMapper = $this->getMockBuilder('PrismMediaManager\Model\ProductFolderMapper')
                                  ->disableOriginalConstructor()
                                  ->getMock();
        $this->serviceLocator->expects($this->at(5))
                        ->method('get')
                        ->with('PrismMediaManager\Model\ProductFolderMapper')
                        ->willReturn($productFolderMapper);


        $customerService = $this->getMockBuilder('PrismSpectrumApiClient\Service\Customer')
                                  ->disableOriginalConstructor()
                                  ->getMock();
        $this->serviceLocator->expects($this->at(6))
                        ->method('get')
                        ->with('PrismSpectrumApiClient\Service\Customer')
                        ->willReturn($customerService);
                        
        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                                  ->disableOriginalConstructor()
                                  ->getMock();
        $this->serviceLocator->expects($this->at(7))
                        ->method('get')
                        ->with('PrismProductsManager\Model\ProductsMapper')
                        ->willReturn($productsMapper);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Model\ProductsMigrationMapper', $result);
    }
}
