<?php

namespace PrismProductsManagerTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Model\ApiCategoriesMapperFactory;
use Zend\Db\Adapter\Platform\Mysql;

/**
 * @author haniw
 */
class ApiCategoriesMapperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiCategoriesMapperFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ApiCategoriesMapperFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                      ->disableOriginalConstructor()
                      ->getMock();
        $dbPms->expects($this->once())
                             ->method('getPlatform')
                             ->willReturn(new Mysql());
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('db-pms')
                        ->willReturn($dbPms);

        $imageMapper = $this->getMockBuilder('PrismMediaManager\Model\ImageMapper')
                            ->disableOriginalConstructor()
                            ->getMock();
        $this->serviceLocator->expects($this->at(1))
                             ->method('get')
                             ->with('PrismMediaManager\Model\ImageMapper')
                             ->willReturn($imageMapper);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Model\ApiCategoriesMapper', $result);
    }
}
