<?php

namespace PrismProductsManagerTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Model\BulkActionsMapperFactory;
use Zend\Db\Adapter\Platform\Mysql;

/**
 * @author haniw
 */
class BulkActionsMapperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiCategoriesMapperFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new BulkActionsMapperFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                      ->disableOriginalConstructor()
                      ->getMock();
        $dbPms->expects($this->any())
                             ->method('getPlatform')
                             ->willReturn(new Mysql());
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('db-pms')
                        ->willReturn($dbPms);

        $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
                            ->disableOriginalConstructor()
                            ->getMock();
        $config = [
            'languages' => [
                'current-language' => 1,
                'site-languages' => [1],
            ],
        ];
        $commonStorage->expects($this->once())
                    ->method('read')
                    ->with('siteConfig')
                    ->willReturn($config);
        $this->serviceLocator->expects($this->at(1))
                         ->method('get')
                         ->with('CommonStorage')
                         ->willReturn($commonStorage);

        $queueService = $this->getMockBuilder('PrismQueueManager\Service\QueueService')
                           ->disableOriginalConstructor()
                           ->getMock();
        $this->serviceLocator->expects($this->at(2))
                       ->method('get')
                       ->with('QueueService')
                       ->willReturn($queueService);

        $commonAttributesService = $this->getMockBuilder('PrismProductsManager\Service\CommonAttributesService')
                                        ->disableOriginalConstructor()
                                        ->getMock();
        $this->serviceLocator->expects($this->at(3))
                             ->method('get')
                             ->with('PrismProductsManager\Service\CommonAttributesService')
                             ->willReturn($commonAttributesService);

        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(4))
            ->method('get')
            ->with()
            ->willReturn($productsMapper);

        $scheduleService = $this->getMockBuilder('PrismProductsManager\Service\ScheduleService')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(5))
            ->method('get')
            ->with()
            ->willReturn($scheduleService);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Model\BulkActionsMapper', $result);
    }
}
