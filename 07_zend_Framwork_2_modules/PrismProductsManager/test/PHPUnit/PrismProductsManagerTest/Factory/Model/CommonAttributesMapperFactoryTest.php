<?php

namespace PrismProductsManagerTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Model\CommonAttributesMapperFactory;
use Zend\Db\Adapter\Platform\Mysql;

/**
 * @author haniw
 */
class CommonAttributesMapperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var CommonAttributesMapperFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new CommonAttributesMapperFactory();
        $this->factory = $factory;
    }

    /**
     * @runInSeparateProcess
     */
    public function testCreateService()
    {
        $dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
            ->disableOriginalConstructor()
            ->getMock();
        $dbPms->expects($this->once())
            ->method('getPlatform')
            ->willReturn(new Mysql());
        $this->serviceLocator->expects($this->at(0))
            ->method('get')
            ->with('db-pms')
            ->willReturn($dbPms);

        $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
            ->disableOriginalConstructor()
            ->getMock();
        $config = [
            'languages' => [
                'current-language' => 1,
                'site-languages' => [1],
            ],
            'websites' => [
                'current-website-id' => 1,
            ],
            'pagination' => [
                'records-per-page' => 1,
            ],
            'currencies' => [
                'currencies-list' => [
                    1 => [
                        'currency-code' => 'GBP'
                    ],
                ],
                'default-currency' => 1,
                'site-currencies' => [1],
                'allow-legacy-trade-currency-map-translation' => true, //flag to determine if currency code should be translated to codes other systems e.g SPECTRUM understands
                'legacy-trade-currency-map' => [ //Codes other systems e.g SPECTRUM understands
                    'gbp' => 'curr0',
                ],
            ],
        ];
        $commonStorage->expects($this->at(0))
            ->method('read')
            ->with('siteConfig')
            ->willReturn($config);
        $commonStorage->expects($this->at(1))
            ->method('read')
            ->with('clientConfig')
            ->willReturn([]);
        $this->serviceLocator->expects($this->at(1))
            ->method('get')
            ->with('CommonStorage')
            ->willReturn($commonStorage);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Model\CommonAttributesMapper', $result);
    }
}
