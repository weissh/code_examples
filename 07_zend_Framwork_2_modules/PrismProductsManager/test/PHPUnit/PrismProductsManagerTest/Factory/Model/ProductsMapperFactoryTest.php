<?php

namespace PrismProductsManagerTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Model\ProductsMapperFactory;
use Zend\Db\Adapter\Platform\Mysql;

/**
 * @author haniw
 */
class ProductsMapperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsMapperFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProductsMapperFactory();
        $this->factory = $factory;
    }

    /**
     * @runInSeparateProcess
     */
    public function testCreateService()
    {
        $dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                      ->disableOriginalConstructor()
                      ->getMock();
        $dbPms->expects($this->once())
                             ->method('getPlatform')
                             ->willReturn(new Mysql());
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('db-pms')
                        ->willReturn($dbPms);

        $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
                            ->disableOriginalConstructor()
                            ->getMock();
        $config = [
            'languages' => [
                'current-language' => 1,
                'site-languages' => [1],
            ],
            'websites' => [
                'current-website-id' => 1,
            ],
            'pagination' => [
                'records-per-page' => 1,
            ],
            'currencies' => [
                'currencies-list' => [
                    1 => [
                        'currency-code' => 'GBP'
                    ],
                ],
                'default-currency' => 1,
                'site-currencies' => [1],
                'allow-legacy-trade-currency-map-translation' => true, //flag to determine if currency code should be translated to codes other systems e.g SPECTRUM understands
                'legacy-trade-currency-map' => [ //Codes other systems e.g SPECTRUM understands
                    'gbp' => 'curr0',
                ],
            ],
        ];
        $commonStorage->expects($this->at(0))
            ->method('read')
            ->with('siteConfig')
            ->willReturn($config);
        $commonStorage->expects($this->at(1))
            ->method('read')
            ->with('clientConfig')
            ->willReturn([]);
        $this->serviceLocator->expects($this->at(1))
                         ->method('get')
                         ->with('CommonStorage')
                         ->willReturn($commonStorage);

        $attributesGroupMapper = $this->getMockBuilder('PrismProductsManager\Model\AttributesGroupMapper')
                              ->disableOriginalConstructor()
                              ->getMock();
        $this->serviceLocator->expects($this->at(2))
                          ->method('get')
                          ->with('PrismProductsManager\Model\AttributesGroupMapper')
                          ->willReturn($attributesGroupMapper);

        $categoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\CategoriesMapper')
                            ->disableOriginalConstructor()
                            ->getMock();
        $this->serviceLocator->expects($this->at(3))
                        ->method('get')
                        ->with('PrismProductsManager\Model\CategoriesMapper')
                        ->willReturn($categoriesMapper);

        $actionLogger = $this->getMockBuilder('PrismCommon\Service\ActionLogger')
                            ->disableOriginalConstructor()
                            ->getMock();
        $this->serviceLocator->expects($this->at(4))
                        ->method('get')
                        ->with('PrismCommon\Factory\Service\ActionLoggerFactory')
                        ->willReturn($actionLogger);

        $merchantCategoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\MerchantCategoriesMapper')
                ->disableOriginalConstructor()
               ->getMock();
        $this->serviceLocator->expects($this->at(5))
                 ->method('get')
                 ->with('PrismProductsManager\Model\MerchantCategoriesMapper')
                 ->willReturn($merchantCategoriesMapper);

        $queryPagination = $this->getMockBuilder('PrismCommon\Service\QueryPagination')
                             ->disableOriginalConstructor()
                             ->getMock();
        $this->serviceLocator->expects($this->at(6))
                          ->method('get')
                          ->with('QueryPagination')
                          ->wilLReturn($queryPagination);

        $taxMapper = $this->getMockBuilder('PrismProductsManager\Model\TaxMapper')
           ->disableOriginalConstructor()
          ->getMock();
        $this->serviceLocator->expects($this->at(7))
            ->method('get')
            ->with('PrismProductsManager\Model\TaxMapper')
            ->willReturn($taxMapper);

        $productFolderMapper = $this->getMockBuilder('PrismMediaManager\Model\ProductFolderMapper')
                                  ->disableOriginalConstructor()
                                  ->getMock();
        $this->serviceLocator->expects($this->at(8))
                        ->method('get')
                        ->with('PrismMediaManager\Model\ProductFolderMapper')
                        ->willReturn($productFolderMapper);

        $rsMapper = $this->getMockBuilder('PrismMediaManager\Service\RSMapper')
                                  ->disableOriginalConstructor()
                                  ->getMock();
        $this->serviceLocator->expects($this->at(9))
                        ->method('get')
                        ->with('PrismMediaManager\Service\RSMapper')
                        ->willReturn($rsMapper);

        $this->serviceLocator->expects($this->at(10))
            ->method('get')
            ->with('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable')
                     ->disableOriginalConstructor()
                     ->getMock()
            );

        $this->serviceLocator->expects($this->at(11))
            ->method('get')
            ->with('PrismProductsManager\Service\InputService')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Service\InputService')
                    ->disableOriginalConstructor()
                    ->getMock()
            );

        $this->serviceLocator->expects($this->at(12))
            ->method('get')
            ->with('PrismProductsManager\Model\ColoursMapper')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Model\ColoursMapper')
                    ->disableOriginalConstructor()
                    ->getMock()
            );

        $this->serviceLocator->expects($this->at(13))
            ->method('get')
            ->with('PrismProductsManager\Service\CommonAttributesService')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Service\CommonAttributesService')
                    ->disableOriginalConstructor()
                    ->getMock()
            );

        $this->serviceLocator->expects($this->at(14))
            ->method('get')
            ->with('QueueService')
            ->willReturn(
                $this->getMockBuilder('PrismQueueManager\Service\QueueService')
                    ->disableOriginalConstructor()
                    ->getMock()
            );
            
        $this->serviceLocator->expects($this->at(15))
            ->method('get')
            ->with('MailSender')
            ->willReturn(
                $this->getMockBuilder('PrismCommon\Service\MailSender')
                    ->disableOriginalConstructor()
                    ->getMock()
            );
            
        $this->serviceLocator->expects($this->at(16))
            ->method('get')
            ->with('config')
            ->willReturn([]);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Model\ProductsMapper', $result);
    }
}
