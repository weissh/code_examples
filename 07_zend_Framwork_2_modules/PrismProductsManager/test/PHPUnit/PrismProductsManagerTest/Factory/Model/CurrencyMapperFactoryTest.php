<?php

namespace PrismProductsManagerTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Model\CurrencyMapperFactory;
use PrismProductsManagerTest\ZendDbFixture\FixtureManager;

/**
 * @author haniw
 */
class CurrencyMapperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var CurrencyMapperFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new CurrencyMapperFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');
        $this->fixtureManager->execute('CURRENCY-up.sql');
        $dbAdapter            = $this->fixtureManager->getDbAdapter();

        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('db-pms')
                        ->willReturn($dbAdapter);

        $queryPagination = $this->getMockBuilder('PrismCommon\Service\QueryPagination')
                                ->disableOriginalConstructor()
                                ->getMock();
        $this->serviceLocator->expects($this->at(1))
                             ->method('get')
                             ->with('QueryPagination')
                             ->wilLReturn($queryPagination);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Model\CurrencyMapper', $result);
        $this->fixtureManager->execute('CURRENCY-down.sql');
    }
}
