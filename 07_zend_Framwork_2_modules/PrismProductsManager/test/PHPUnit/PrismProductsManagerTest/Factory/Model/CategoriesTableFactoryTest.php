<?php

namespace PrismProductsManagerTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Model\CategoriesTableFactory;
use Zend\Db\Adapter\Platform\Mysql;

/**
 * @author haniw
 */
class CategoriesTableFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var CategoriesTableFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new CategoriesTableFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                      ->disableOriginalConstructor()
                      ->getMock();
        $dbPms->expects($this->once())
                             ->method('getPlatform')
                             ->willReturn(new Mysql());
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('db-pms')
                        ->willReturn($dbPms);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Model\CategoriesTable', $result);
    }
}
