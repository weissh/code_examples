<?php

namespace PrismProductsManagerTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Model\MerchantCategoriesMapperFactory;
use Zend\Db\Adapter\Platform\Mysql;

/**
 * @author haniw
 */
class MerchantCategoriesMapperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var MerchantCategoriesMapperFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new MerchantCategoriesMapperFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                      ->disableOriginalConstructor()
                      ->getMock();
        $dbPms->expects($this->once())
                             ->method('getPlatform')
                             ->willReturn(new Mysql());
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('db-pms')
                        ->willReturn($dbPms);

        $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
                            ->disableOriginalConstructor()
                            ->getMock();
        $config = [
            'defaultOneSizeCode' => 'USX',   // this is used to build the sku for one size product
            'languages' => [
                'current-language' => 1,
                'site-languages' => [1],
            ],
        ];
        $commonStorage->expects($this->once())
                    ->method('read')
                    ->with('siteConfig')
                    ->willReturn($config);
        $this->serviceLocator->expects($this->at(1))
                         ->method('get')
                         ->with('CommonStorage')
                         ->willReturn($commonStorage);

        $attributesGroupMapper = $this->getMockBuilder('PrismProductsManager\Model\AttributesGroupMapper')
                               ->disableOriginalConstructor()
                               ->getMock();
        $this->serviceLocator->expects($this->at(2))
                     ->method('get')
                     ->with('PrismProductsManager\Model\AttributesGroupMapper')
                     ->willReturn($attributesGroupMapper);

        $commonAttributesTable = $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesTable')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(3))
            ->method('get')
            ->with('PrismProductsManager\Model\CommonAttributes\CommonAttributesTable')
            ->willReturn($commonAttributesTable);

        $commonAttributesValuesTable = $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(4))
            ->method('get')
            ->with('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable')
            ->willReturn($commonAttributesValuesTable);

        $commonAttributesValuesDefinitionsTable = $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesDefinitionsTable')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(5))
            ->method('get')
            ->with('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesDefinitionsTable')
            ->willReturn($commonAttributesValuesDefinitionsTable);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Model\MerchantCategoriesMapper', $result);
    }
}
