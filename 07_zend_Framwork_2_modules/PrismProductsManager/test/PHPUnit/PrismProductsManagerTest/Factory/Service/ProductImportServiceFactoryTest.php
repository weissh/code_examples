<?php

namespace PrismProductsManagerTest\Factory\Service;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Service\ProductImportServiceFactory;
use PrismProductsManager\Service\ProductImportService;
use Prophecy\Argument;

/**
 * @author haniw
 */
class ProductImportServiceFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductImportServiceFactory */
    protected $factory;

    /** @var \Prophecy\prophecy\ObjectProphecy */
    protected $serviceLocator;

    public function setUp()
    {
        $this->serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');;
        $this->factory = new ProductImportServiceFactory();;
    }

    public function testCreateService()
    {
        $db = $this->prophesize('Zend\Db\Adapter\Adapter');
        $driver = $this->prophesize('Zend\Db\Adapter\Driver\Pdo\Pdo');
        $statement = $this->prophesize('Zend\Db\Adapter\Driver\Pdo\Statement');
        $driver->createStatement()
               ->willReturn($statement)
               ->shouldBeCalled();
        $db->getDriver()
           ->willReturn($driver)
           ->shouldBeCalled();

        $platform = new \Zend\Db\Adapter\Platform\Mysql();
        $db->getPlatform()
           ->wilLReturn($platform)
           ->shouldBeCalled();

        $result = $this->prophesize('Zend\Db\ResultSet\ResultSet');
        $result->count()
               ->willReturn(0)
               ->shouldBeCalled();

        $parameterContainer = $this->prophesize('Zend\Db\Adapter\ParameterContainer');

        $statement->setSql(Argument::type('string'))->shouldBeCalled();
        $statement->getParameterContainer()
          ->willReturn($parameterContainer)
          ->shouldBeCalled();

        $statement->execute()
                  ->willReturn($result)
                  ->shouldBeCalled();

        $db->query(Argument::type('string'))
           ->willReturn($statement)
           ->shouldBeCalled();
        $this->serviceLocator->get('db-pms')
                             ->willReturn($db)
                             ->shouldBeCalled();

        $commonAttributesService = $this->prophesize('PrismProductsManager\Service\CommonAttributesService');
        $this->serviceLocator->get('PrismProductsManager\Service\CommonAttributesService')
                             ->willReturn($commonAttributesService)
                             ->shouldBeCalled();

        $coloursMapper = $this->prophesize('PrismProductsManager\Model\ColoursMapper');
        $this->serviceLocator->get('PrismProductsManager\Model\ColoursMapper')
                             ->willReturn($coloursMapper)
                             ->shouldBeCalled();

        $result = $this->factory->createService($this->serviceLocator->reveal());
        $this->assertInstanceOf(ProductImportService::class, $result);
    }
}
