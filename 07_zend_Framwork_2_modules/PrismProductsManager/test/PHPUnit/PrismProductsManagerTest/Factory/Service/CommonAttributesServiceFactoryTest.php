<?php

namespace PrismProductsManagerTest\Factory\Service;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Service\CommonAttributesServiceFactory;
use PrismProductsManager\Model;
use PrismProductsManager\Service\CommonAttributesService;

/**
 * @author haniw
 */
class CommonAttributesServiceFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var CommonAttributesServiceFactory */
    protected $factory;

    /** @var \Prophecy\prophecy\ObjectProphecy */
    protected $serviceLocator;

    public function setUp()
    {
        $this->serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');;
        $this->factory = new CommonAttributesServiceFactory();;
    }

    public function testCreateService()
    {
        $commonStorage = $this->prophesize('PrismAuth\Storage\CommonStorage');

        $config = [
            'languages' => [
                'current-language' => 1,
            ],
        ];
        $commonStorage->read('siteConfig')->willReturn($config)
            ->shouldBeCalled();
        $this->serviceLocator->get('CommonStorage')->willReturn($commonStorage)
                                                   ->shouldBeCalled();

        $doctrineEM = $this->prophesize('Doctrine\ORM\EntityManager');
        $doctrineEM->getRepository('PrismProductsManager\Entity\CommonAttributes')
                   ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
                   ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\CommonAttributesViewType')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\CommonAttributesValues')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\CommonAttributesValuesGuiRepresentation')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\CommonAttributesValuesEquivalence')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\CommonAttributesValuesChain')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\CommonAttributesDefinitions')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\Territories')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();

        $this->serviceLocator->get('doctrine.entitymanager.orm_default')
                             ->willReturn($doctrineEM)
                             ->shouldBeCalled();

        $commonAttributeMapper = $this->prophesize(Model\CommonAttributesMapper::class);
        $this->serviceLocator->get(Model\CommonAttributesMapper::class)->willReturn($commonAttributeMapper)
            ->shouldBeCalled();

        $dbAdapter = $this->prophesize(\Zend\Db\Adapter\Adapter::class);
        $this->serviceLocator->get('db-pms')->willReturn($dbAdapter)
            ->shouldBeCalled();

        $this->assertInstanceOf(CommonAttributesService::class, $this->factory->createService($this->serviceLocator->reveal()));
    }
}
