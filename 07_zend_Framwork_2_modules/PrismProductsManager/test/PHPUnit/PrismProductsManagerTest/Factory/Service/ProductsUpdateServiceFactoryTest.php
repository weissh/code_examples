<?php

namespace PrismProductsManagerTest\Factory\Service;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Service\ProductsUpdateServiceFactory;

/**
 * @author haniw
 */
class ProductsUpdateServiceFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsUpdateServiceFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProductsUpdateServiceFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
                           ->disableOriginalConstructor()
                           ->getMock();

        $commonStorage->expects($this->once())
                   ->method('read')
                   ->with('siteConfig')
                   ->willReturn([]);
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('CommonStorage')
                        ->willReturn($commonStorage);
                        
        $this->serviceLocator->expects($this->at(1))
                        ->method('get')
                        ->with('config')
                        ->willReturn(
                        [
                            'clientConfig' => [
                                'settings' => [
                                    'newSpectrumAndSipSynch' => true,
                                ]
                            ],
                        ]
                        );

        $queueService = $this->getMockBuilder('PrismQueueManager\Service\QueueService')
                             ->disableOriginalConstructor()
                             ->getMock();
        $this->serviceLocator->expects($this->at(2))
                     ->method('get')
                     ->with('QueueService')
                     ->willReturn($queueService);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Service\ProductsUpdateService', $result);
    }
}
