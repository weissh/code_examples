<?php

namespace PrismProductsManagerTest\Factory\Service;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Service\InputServiceFactory;
use PrismProductsManager\Model;
use PrismProductsManager\Service\InputService;

/**
 * @author haniw
 */
class InputServiceFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var InputServiceFactory */
    protected $factory;

    /** @var \Prophecy\prophecy\ObjectProphecy */
    protected $serviceLocator;

    public function setUp()
    {
        $this->serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');;
        $this->factory = new InputServiceFactory();;
    }

    public function testCreateService()
    {
        $dbAdapter = $this->prophesize(\Zend\Db\Adapter\Adapter::class);
        $this->serviceLocator->get('db-pms')->willReturn($dbAdapter)
            ->shouldBeCalled();
        $commonStorage = $this->prophesize('PrismAuth\Storage\CommonStorage');

        $config = [
            'languages' => [
                'current-language' => 1,
            ],
        ];
        $commonStorage->read('siteConfig')->willReturn($config)
            ->shouldBeCalled();
        $this->serviceLocator->get('CommonStorage')->willReturn($commonStorage)
            ->shouldBeCalled();

        $doctrineEM = $this->prophesize('Doctrine\ORM\EntityManager');
        $this->serviceLocator->get('doctrine.entitymanager.orm_default')
                             ->willReturn($doctrineEM)
                             ->shouldBeCalled();

        $mapper = $this->prophesize(Model\CommonAttributesMapper::class);
        $this->serviceLocator->get(Model\CommonAttributesMapper::class)->willReturn($mapper)
            ->shouldBeCalled();

        $mapper = $this->prophesize(Model\AttributesGroupMapper::class);
        $this->serviceLocator->get(Model\AttributesGroupMapper::class)->willReturn($mapper)
            ->shouldBeCalled();

        $this->assertInstanceOf(InputService::class, $this->factory->createService($this->serviceLocator->reveal()));
    }
}
