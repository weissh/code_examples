<?php

namespace PrismProductsManagerTest\Factory\Service;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Service\ScheduleServiceFactory;
use PrismProductsManager\Model;
use PrismProductsManager\Service;
use PrismProductsManager\Service\ScheduleService;

/**
 * @author haniw
 */
class ScheduleServiceFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ScheduleServiceFactory */
    protected $factory;

    /** @var \Prophecy\prophecy\ObjectProphecy */
    protected $serviceLocator;

    public function setUp()
    {
        $this->serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');;
        $this->factory = new ScheduleServiceFactory();;
    }

    public function testCreateService()
    {
        $doctrineEM = $this->prophesize('Doctrine\ORM\EntityManager');
        $this->serviceLocator->get('doctrine.entitymanager.orm_default')
                             ->willReturn($doctrineEM)
                             ->shouldBeCalled();

        $doctrineEM->getRepository('PrismProductsManager\Entity\Schedules')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();

        $doctrineEM->getRepository('PrismProductsManager\Entity\SchedulePrices')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();

        $doctrineEM->getRepository('PrismProductsManager\Entity\ScheduleTrail')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();

        $doctrineEM->getRepository('PrismProductsManager\Entity\ScheduleTypes')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();

        $doctrineEM->getRepository('PrismProductsManager\Entity\Territories')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();

        $doctrineEM->getRepository('PrismProductsManager\Entity\Currency')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();

        $mapper = $this->prophesize(Model\ProductsMapper::class);
        $this->serviceLocator->get(Model\ProductsMapper::class)->willReturn($mapper)
            ->shouldBeCalled();

        $mapper = $this->prophesize(Model\AttributesGroupMapper::class);
        $this->serviceLocator->get(Model\AttributesGroupMapper::class)->willReturn($mapper)
            ->shouldBeCalled();

        $service = $this->prophesize(Service\InputService::class);
        $this->serviceLocator->get(Service\InputService::class)->willReturn($service)
            ->shouldBeCalled();

        $service = $this->prophesize(Service\CommonAttributesService::class);
        $this->serviceLocator->get(Service\CommonAttributesService::class)->willReturn($service)
            ->shouldBeCalled();

        $service = $this->prophesize('PrismQueueManager\Service\QueueService');
        $this->serviceLocator->get('QueueService')->willReturn($service)
            ->shouldBeCalled();

        $dbAdapter = $this->prophesize(\Zend\Db\Adapter\Adapter::class);
        $this->serviceLocator->get('db-pms')->willReturn($dbAdapter)
            ->shouldBeCalled();

        $this->assertInstanceOf(ScheduleService::class, $this->factory->createService($this->serviceLocator->reveal()));
    }
}
