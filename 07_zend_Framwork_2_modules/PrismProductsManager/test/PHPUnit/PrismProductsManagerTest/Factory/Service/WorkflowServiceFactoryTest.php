<?php

namespace PrismProductsManagerTest\Factory\Service;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Service\WorkflowServiceFactory;
use PrismProductsManager\Model;
use PrismProductsManager\Service;
use PrismProductsManager\Service\WorkflowService;

/**
 * @author haniw
 */
class WorkflowServiceFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var WorkflowServiceFactory */
    protected $factory;

    /** @var \Prophecy\prophecy\ObjectProphecy */
    protected $serviceLocator;

    public function setUp()
    {
        $this->serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');;
        $this->factory = new WorkflowServiceFactory();;
    }

    public function testCreateService()
    {
        $commonStorage = $this->prophesize('PrismAuth\Storage\CommonStorage');

        $config = [
            'languages' => [
                'current-language' => 1,
            ],
        ];
        $commonStorage->read('siteConfig')->willReturn($config)
            ->shouldBeCalled();
        $this->serviceLocator->get('CommonStorage')->willReturn($commonStorage)
                                                   ->shouldBeCalled();

        $doctrineEM = $this->prophesize('Doctrine\ORM\EntityManager');
        $this->serviceLocator->get('doctrine.entitymanager.orm_default')
                             ->willReturn($doctrineEM)
                             ->shouldBeCalled();

        $mapper = $this->prophesize(Model\ProductsMapper::class);
        $this->serviceLocator->get(Model\ProductsMapper::class)->willReturn($mapper)
            ->shouldBeCalled();

        $service = $this->prophesize(Service\AssignFieldToTabsService::class);
        $this->serviceLocator->get(Service\AssignFieldToTabsService::class)->willReturn($service)
            ->shouldBeCalled();

        $service = $this->prophesize(Service\InputService::class);
        $this->serviceLocator->get(Service\InputService::class)->willReturn($service)
            ->shouldBeCalled();

        $dbAdapter = $this->prophesize(\Zend\Db\Adapter\Adapter::class);
        $this->serviceLocator->get('db-pms')->willReturn($dbAdapter)
            ->shouldBeCalled();

        $this->assertInstanceOf(WorkflowService::class, $this->factory->createService($this->serviceLocator->reveal()));
    }
}
