<?php

namespace PrismProductsManagerTest\Factory\Service;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Service\AssignFieldToTabsServiceFactory;
use PrismProductsManager\Model;
use PrismProductsManager\Service\AssignFieldToTabsService;
use PrismProductsManager\Service\CommonAttributesService;

/**
 * @author haniw
 */
class AssignFieldToTabsServiceFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var AssignFieldToTabsServiceFactory */
    protected $factory;

    /** @var \Prophecy\prophecy\ObjectProphecy */
    protected $serviceLocator;

    public function setUp()
    {
        $this->serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');;
        $this->factory = new AssignFieldToTabsServiceFactory();;
    }

    public function testCreateService()
    {
        $commonStorage = $this->prophesize('PrismAuth\Storage\CommonStorage');

        $config = [
            'languages' => [
                'current-language' => 1,
            ],
        ];
        $commonStorage->read('siteConfig')->willReturn($config)
            ->shouldBeCalled();
        $this->serviceLocator->get('CommonStorage')->willReturn($commonStorage)
                                                   ->shouldBeCalled();

        $doctrineEM = $this->prophesize('Doctrine\ORM\EntityManager');
        $doctrineEM->getRepository('PrismProductsManager\Entity\InputFields')
                   ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
                   ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\InputFieldTypes')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\InputPages')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\InputFieldsets')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\InputFieldRAttributes')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\AttributesGroups')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\InputValidations')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\InputValidationTypes')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\Workflows')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();
        $doctrineEM->getRepository('PrismProductsManager\Entity\InputFieldRPages')
            ->willReturn($this->prophesize('Doctrine\Common\Persistence\ObjectRepository')->reveal())
            ->shouldBeCalled();

        $this->serviceLocator->get('doctrine.entitymanager.orm_default')
                             ->willReturn($doctrineEM)
                             ->shouldBeCalled();

        $commonAttributesService = $this->prophesize(CommonAttributesService::class);
        $this->serviceLocator->get(CommonAttributesService::class)->willReturn($commonAttributesService)
            ->shouldBeCalled();

        $commonAttributeMapper = $this->prophesize(Model\AttributesGroupMapper::class);
        $this->serviceLocator->get(Model\AttributesGroupMapper::class)->willReturn($commonAttributeMapper)
            ->shouldBeCalled();

        $this->assertInstanceOf(AssignFieldToTabsService::class, $this->factory->createService($this->serviceLocator->reveal()));
    }
}
