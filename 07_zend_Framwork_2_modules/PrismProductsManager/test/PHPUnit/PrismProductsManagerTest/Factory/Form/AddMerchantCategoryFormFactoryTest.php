<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\AddMerchantCategoryFormFactory;

/**
 * @author haniw
 */
class AddMerchantCategoryFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var AddMerchantCategoryFormFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new AddMerchantCategoryFormFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $categoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\CategoriesMapper')
            ->disableOriginalConstructor()
            ->getMock();
        $categoriesMapper->expects($this->once())
            ->method('fetchAllMerchantCategories')
            ->willReturn([]);
        $this->serviceLocator->expects($this->at(0))
            ->method('get')
            ->with('PrismProductsManager\Model\CategoriesMapper')
            ->willReturn($categoriesMapper);

        $attributesGroupMapper = $this->getMockBuilder('PrismProductsManager\Model\AttributesGroupMapper')
            ->disableOriginalConstructor()
            ->getMock();
        $attributesGroupMapper->expects($this->any())
            ->method('fetchTopMerchantGroups')
            ->willReturn([]);
        $this->serviceLocator->expects($this->at(1))
            ->method('get')
            ->with('PrismProductsManager\Model\AttributesGroupMapper')
            ->willReturn($attributesGroupMapper);

        $commonAttributesTable = $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesTable')
            ->disableOriginalConstructor()
            ->getMock();
        $commonAttributesTable->expects($this->any())
            ->method('getCommonAttributesDetailsBy')
            ->willReturn([]);
        $commonAttributesTable->expects($this->any())
            ->method('listResultsWithKeyAs')
            ->willReturn([]);

        $this->serviceLocator->expects($this->at(2))
            ->method('get')
            ->with('PrismProductsManager\Model\CommonAttributes\CommonAttributesTable')
            ->willReturn($commonAttributesTable);

        $dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(3))
            ->method('get')
            ->with('db-pms')
            ->willReturn($dbPms);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Form\AddMerchantCategoryForm', $result);
    }
}
