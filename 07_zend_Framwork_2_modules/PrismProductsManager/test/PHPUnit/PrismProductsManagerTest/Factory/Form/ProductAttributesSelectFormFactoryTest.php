<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\ProductAttributesSelectFormFactory;

/**
 * @author haniw
 */
class ProductAttributesSelectFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductAttributesSelectFormFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProductAttributesSelectFormFactory();
        $this->factory = $factory;
    }

    public function provideParams()
    {
        return [
            [false, false, [], []],
            [false, '1_1', [], []],
            [1, false, [], []],
            [1, '1_1', [], []],
            [1, '1_1', [
                'foo' => [
                    0 => [
                        'attributeGroupId' => 1,
                        'publicName' => 'foo',
                        'label' => 'Foo',
                        'viewAs' => 'foo',
                        'values' => [],
                    ],
                ]
            ], [
                1,
                2,
            ]],
            [1, '1_1', [
                'foo' => [
                    0 => [
                        'attributeGroupId' => 1,
                        'publicName' => 'foo',
                        'label' => 'Foo',
                        'viewAs' => 'foo',
                        'values' => [],
                    ],
                ]
            ], [
                5,
                6,
            ]],
            [1, '1_1', [
                'foo' => [
                    0 => [
                        'attributeGroupId' => 1,
                        'publicName' => 'foo',
                        'label' => 'Foo',
                        'viewAs' => 'foo',
                        'values' => [
                            1,
                            2,
                        ],
                    ],
                ]
            ], [
                5,
                6,
            ]],
        ];
    }

    /**
     * @dataProvider provideParams
     */
    public function testCreateService($productId, $variantId, $returnFetchAllAttributesGroups, $fetchAllAttributeGroupsForMarketingCategory)
    {
        $attributesGroupMapper = $this->getMockBuilder('PrismProductsManager\Model\AttributesGroupMapper')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        if ($productId !== false && $variantId !== false) {
            $attributesGroupMapper->expects($this->once())
                                  ->method('fetchAllAttributeGroups')
                                  ->with(1)
                                  ->willReturn($returnFetchAllAttributesGroups);

            $attributesGroupMapper->expects($this->once())
                                ->method('fetchAllAttributeGroupsForMarketingCategory')
                                ->with(1)
                                ->willReturn($fetchAllAttributeGroupsForMarketingCategory);
        }

        $this->serviceLocator->expects($this->at(0))
                             ->method('get')
                             ->with('PrismProductsManager\Model\AttributesGroupMapper')
                             ->willReturn($attributesGroupMapper);

        $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
                           ->disableOriginalConstructor()
                           ->getMock();

        $config = [
           'languages' => [
              'current-language' => 1,
           ],
        ];
        $commonStorage->expects($this->once())
                ->method('read')
               ->with('siteConfig')
               ->willReturn($config);
        $this->serviceLocator->expects($this->at(1))
                    ->method('get')
                    ->with('CommonStorage')
                    ->willReturn($commonStorage);

        $application = $this->getMockBuilder('Zend\Mvc\Application')
            ->disableOriginalConstructor()
            ->getMock();
        $mvcEvent = $this->getMock('Zend\Mvc\MvcEvent');
        $routeMatch = $this->getMockBuilder('Zend\Mvc\Router\RouteMatch')
                           ->disableOriginalConstructor()
                           ->getMock();
        $application->expects($this->once())
                    ->method('getMvcEvent')
                    ->willReturn($mvcEvent);
        $routeMatch->expects($this->at(0))
                   ->method('getParam')
                   ->with('id')
                   ->willReturn($productId);
        $routeMatch->expects($this->at(1))
                  ->method('getParam')
                  ->with('variantId')
                  ->willReturn($variantId);
        $mvcEvent->expects($this->once())
                 ->method('getRouteMatch')
                 ->willReturn($routeMatch);

        $this->serviceLocator->expects($this->at(2))
                             ->method('get')
                             ->with('Application')
                             ->willReturn($application);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Form\ProductAttributesSelectForm', $result);
    }
}
