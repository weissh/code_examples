<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\ProductVariantCreationFormFactory;

/**
 * @author haniw
 */
class ProductVariantCreationFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductVariantCreationFormFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProductVariantCreationFormFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                      ->disableOriginalConstructor()
                      ->getMock();
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('PrismProductsManager\Model\ProductsMapper')
                        ->willReturn($productsMapper);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Form\ProductVariantsCreationForm', $result);
    }
}
