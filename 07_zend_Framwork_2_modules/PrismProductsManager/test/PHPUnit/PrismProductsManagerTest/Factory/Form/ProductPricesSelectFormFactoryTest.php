<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\ProductPricesSelectFormFactory;

/**
 * @author haniw
 */
class ProductPricesSelectFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductPricesSelectFormFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProductPricesSelectFormFactory();
        $this->factory = $factory;
    }

    public function provideProductIdAndVariantIds()
    {
        return [
            [false, false, false],
            [false, '1_1', false],
            [1, false, false],
            [1, '1_1', true]
        ];
    }

    /**
     * @dataProvider provideProductIdAndVariantIds
     */
    public function testCreateService($productId, $variantId, $return)
    {
        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                      ->disableOriginalConstructor()
                      ->getMock();
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('PrismProductsManager\Model\ProductsMapper')
                        ->willReturn($productsMapper);

        $application = $this->getMockBuilder('Zend\Mvc\Application')
            ->disableOriginalConstructor()
            ->getMock();
        $mvcEvent = $this->getMock('Zend\Mvc\MvcEvent');
        $routeMatch = $this->getMockBuilder('Zend\Mvc\Router\RouteMatch')
                           ->disableOriginalConstructor()
                           ->getMock();
        $application->expects($this->once())
                    ->method('getMvcEvent')
                    ->willReturn($mvcEvent);
        $routeMatch->expects($this->at(0))
                   ->method('getParam')
                   ->with('id')
                   ->willReturn($productId);
        $routeMatch->expects($this->at(1))
                  ->method('getParam')
                  ->with('variantId')
                  ->willReturn($variantId);
        $mvcEvent->expects($this->once())
                 ->method('getRouteMatch')
                 ->willReturn($routeMatch);

        $this->serviceLocator->expects($this->at(1))
                             ->method('get')
                             ->with('Application')
                             ->willReturn($application);

        if ($return) {
            $productsMapper->expects($this->once())
                           ->method('getProductPrices')
                           ->with($variantId, false, 1)
                           ->willReturn([]);
        }

        $result = $this->factory->createService($this->serviceLocator);
        if (!$return) {
            $this->assertEquals($return, $result);
        } else {
            $this->assertInstanceOf('PrismProductsManager\Form\ProductPricesSelectForm', $result);
        }
    }
}
