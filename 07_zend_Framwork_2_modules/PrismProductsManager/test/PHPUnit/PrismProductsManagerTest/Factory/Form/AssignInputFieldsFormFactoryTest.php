<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\AssignInputFieldsFormFactory;
use PrismProductsManager\Form\AssignInputFieldsForm;
use PrismProductsManager\Model;
use PrismProductsManager\Service;

/**
 * @author haniw
 */
class AssignInputFieldsFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var AssignInputFieldsFormFactory */
    protected $factory;

    /** @var  \Prophecy\Prophecy\ObjectProphecy */
    protected $serviceLocator;

    public function setUp()
    {
        $this->serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');
        $this->factory = new AssignInputFieldsFormFactory();
    }

    public function testCreateService()
    {
        $assignFieldToTabsService = $this->prophesize(Service\AssignFieldToTabsService::class);
        $this->serviceLocator->get(Service\AssignFieldToTabsService::class)->willReturn($assignFieldToTabsService)
            ->shouldBeCalled();

        $commonAttributesService = $this->prophesize(Service\CommonAttributesService::class);
        $this->serviceLocator->get(Service\CommonAttributesService::class)->willReturn($commonAttributesService)
            ->shouldBeCalled();

        $attributesGroupMapper = $this->prophesize(Model\AttributesGroupMapper::class);
        $this->serviceLocator->get(Model\AttributesGroupMapper::class)->willReturn($attributesGroupMapper)
            ->shouldBeCalled();

        $result = $this->factory->createService($this->serviceLocator->reveal());
        $this->assertInstanceOf(AssignInputFieldsForm::class, $result);
    }
}
