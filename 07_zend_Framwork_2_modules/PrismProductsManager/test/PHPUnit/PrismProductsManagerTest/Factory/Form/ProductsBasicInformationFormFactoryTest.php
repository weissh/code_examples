<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\ProductsBasicInformationFormFactory;

/**
 * @author haniw
 */
class ProductsBasicInformationFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsBasicInformationFormFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProductsBasicInformationFormFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                      ->disableOriginalConstructor()
                      ->getMock();
        // need test when getProductTypes return empty array,
        // but currently _productTypes property not defined in class property
        $productsMapper->expects($this->once())
                       ->method('getProductTypes')
                       ->willReturn([
            0 => [
                'productTypeId' => 1,
                'name' => 'foo',
            ],
        ]);
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('PrismProductsManager\Model\ProductsMapper')
                        ->willReturn($productsMapper);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Form\ProductsBasicInformationForm', $result);
    }
}
