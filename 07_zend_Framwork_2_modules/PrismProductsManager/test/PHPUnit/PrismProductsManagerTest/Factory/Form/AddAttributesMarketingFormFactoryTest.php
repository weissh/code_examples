<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\AddAttributesMarketingFormFactory;

/**
 * @author haniw
 */
class AddAttributesMarketingFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var AddMerchantCategoryFormFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new AddAttributesMarketingFormFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                      ->disableOriginalConstructor()
                      ->getMock();
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('db-pms')
                        ->willReturn($dbPms);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Form\AddAttributesMarketingForm', $result);
    }
}
