<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\Repository;
use PrismProductsManager\Factory\Form\InputFieldsFormFactory;
use PrismProductsManager\Form\InputFieldsForm;
use PrismProductsManager\Model;
use PrismProductsManager\Service;

/**
 * @author haniw
 */
class InputFieldsFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var InputFieldsFormFactory */
    protected $factory;

    /** @var  \Prophecy\Prophecy\ObjectProphecy */
    protected $serviceLocator;

    public function setUp()
    {
        $this->formElementManager = $this->prophesize('Zend\Form\FormElementManager');
        $this->factory = new InputFieldsFormFactory();
    }

    public function provideCreateService()
    {
        return [
            [1, 1, 1],
            [1, 0, 1],
            [1, 0, false],
            [0, 0, false],
            [0, 1, false],
            [0, 1, true],
        ];
    }

    /**
     * @dataProvider provideCreateService
     */
    public function testCreateService($style, $option, $pageUrl)
    {
        $serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');
        $this->formElementManager->getServiceLocator()->willReturn($serviceLocator)
                                                      ->shouldBeCalled();

        $inputService = $this->prophesize('PrismProductsManager\Service\InputService');
        $serviceLocator->get('PrismProductsManager\Service\InputService')
                       ->willReturn($inputService)
                       ->shouldBeCalled();

        $productsMapper = $this->prophesize('PrismProductsManager\Model\ProductsMapper');
        $serviceLocator->get('PrismProductsManager\Model\ProductsMapper')
                       ->willReturn($productsMapper)
                       ->shouldBeCalled();

        $application = $this->prophesize('Zend\Mvc\Application');
        $serviceLocator->get('Application')->willReturn($application)
                                           ->shouldBeCalled();

        $mvcEvent = $this->prophesize('Zend\Mvc\MvcEvent');
        $application->getMvcEvent()->willReturn($mvcEvent)
                                   ->shouldBeCalled();

        $routeMatch = $this->prophesize('Zend\Mvc\Router\RouteMatch');
        $routeMatch->getParam('displayPage', false)->willReturn($pageUrl)
                                                   ->shouldBeCalled();
        $routeMatch->getParam('style', null)->willReturn($style)
                                            ->shouldBeCalled();
        $routeMatch->getParam('option', null)->willReturn($option)
                                            ->shouldBeCalled();

        $mvcEvent->getRouteMatch()->willReturn($routeMatch)
                                  ->shouldBeCalled();
                                  
        if (!empty($style) && empty($option)) {
            $productsMapper->getAllOptionsForStyle($style, true)->willReturn([])
                                                                ->shouldBeCalled();
        }

        if ($pageUrl !== false) {
            $inputService->getPageInputFields($pageUrl, $style, $option, [])
                         ->willReturn([])
                         ->shouldBeCalled();
        }

        $doctrineOrm = $this->prophesize('Doctrine\ORM\EntityManager');
        $serviceLocator->get('doctrine.entitymanager.orm_default')->willReturn($doctrineOrm)
                                                                  ->shouldBeCalled();

        $inputPagesRepository = $this->prophesize(Repository\InputPagesRepository::class);
        $doctrineOrm->getRepository('PrismProductsManager\Entity\InputPages')
                    ->willReturn($inputPagesRepository)
                    ->shouldBeCalled();

        $inputPagesRepository->findOneBy(array('pageurl' => $pageUrl))
                   ->shouldBeCalled();

        $config = [
            'siteConfig' => [
                'languages' => [
                    'current-language' => 1
                ],
            ],
        ];
        $serviceLocator->get('config')->willReturn($config)
                                      ->shouldBeCalled();

        if ($style) {
            $basicDetails = [
                  'websites' => [],
                  'defaultWebsite' => '',
                  'channels' => [],
                  'manufacturers' => [],
                  'currenciesPriceTypeMatrix' => '',
                  'careInformation' => '',
            ];
            $productsMapper->fetchBasicDetails()->willReturn($basicDetails)->shouldBeCalled();
            $productsMapper->getSuppliers($style)->shouldBeCalled();
            $productsMapper->getSuppliersCurrency(true)->shouldBeCalled();
            if (! $option) {
                $productsMapper->getProductPricesForStyleOrOption($style, null)->shouldBeCalled();
            }
        }

        $result = $this->factory->createService($this->formElementManager->reveal());
        $this->assertInstanceOf(InputFieldsForm::class, $result);
    }
}
