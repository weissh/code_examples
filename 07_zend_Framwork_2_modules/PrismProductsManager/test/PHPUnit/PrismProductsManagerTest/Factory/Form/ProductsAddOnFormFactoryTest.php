<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\ProductsAddOnFormFactory;

/**
 * @author haniw
 */
class ProductsAddOnFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsAddOnFormFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProductsAddOnFormFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Form\ProductsAddOnForm', $result);
    }
}
