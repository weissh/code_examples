<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\CommonAttributesFormFactory;
use PrismProductsManager\Form\CommonAttributesForm;
use PrismProductsManager\Service\CommonAttributesService;

/**
 * @author haniw
 */
class CommonAttributesFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var CommonAttributesFormFactory */
    protected $factory;

    /** @var  \Prophecy\Prophecy\ObjectProphecy */
    protected $serviceLocator;

    public function setUp()
    {
        $this->serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');
        $this->factory = new CommonAttributesFormFactory();
    }

    public function testCreateService()
    {
        $commonStorage = $this->prophesize('PrismAuth\Storage\CommonStorage');

        $config = [
            'languages' => [
                'current-language' => 1,
            ],
        ];
        $commonStorage->read('siteConfig')->willReturn($config)
                                          ->shouldBeCalled();
        $this->serviceLocator->get('CommonStorage')->willReturn($commonStorage)
                            ->shouldBeCalled();

        $commonAttributesService = $this->prophesize(CommonAttributesService::class);
        $this->serviceLocator->get(CommonAttributesService::class)->willReturn($commonAttributesService)
            ->shouldBeCalled();

        $result = $this->factory->createService($this->serviceLocator->reveal());
        $this->assertInstanceOf(CommonAttributesForm::class, $result);
    }
}
