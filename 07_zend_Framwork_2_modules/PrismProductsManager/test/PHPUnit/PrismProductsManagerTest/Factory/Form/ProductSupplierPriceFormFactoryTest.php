<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\ProductSupplierPriceFormFactory;

/**
 * @author haniw
 */
class ProductSupplierPriceFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductSupplierPriceFormFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProductSupplierPriceFormFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                      ->disableOriginalConstructor()
                      ->getMock();
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('PrismProductsManager\Model\ProductsMapper')
                        ->willReturn($productsMapper);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Form\ProductSupplierPriceForm', $result);
    }
}
