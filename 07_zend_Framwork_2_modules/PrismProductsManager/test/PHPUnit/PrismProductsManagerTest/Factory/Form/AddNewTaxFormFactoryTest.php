<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\AddNewTaxFormFactory;

/**
 * @author haniw
 */
class AddNewTaxFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var AddNewTaxFormFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new AddNewTaxFormFactory();
        $this->factory = $factory;
    }

    public function provideGetList()
    {
        $stdClass = new \stdClass();
        $stdClass->success = [];

        return [
            [[]],
            [$stdClass],
        ];
    }

    /**
     * @dataProvider provideGetList
     */
    public function testCreateService($getList)
    {
        $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
                          ->disableOriginalConstructor()
                          ->getMock();

        $config = [
            'languages' => [
                'current-language' => 1,
            ],
        ];
        $commonStorage->expects($this->once())
                   ->method('read')
                  ->with('siteConfig')
                  ->willReturn($config);
        $this->serviceLocator->expects($this->at(0))
                       ->method('get')
                       ->with('CommonStorage')
                       ->willReturn($commonStorage);

        $taxMapper = $this->getMockBuilder('PrismProductsManager\Model\TaxMapper')
          ->disableOriginalConstructor()
         ->getMock();
        $this->serviceLocator->expects($this->at(1))
           ->method('get')
           ->with('PrismProductsManager\Model\TaxMapper')
           ->willReturn($taxMapper);

        $countryService = $this->getMockBuilder('PrismSpectrumApiClient\Service\Country')
             ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(2))
              ->method('get')
              ->with('PrismSpectrumApiClient\Service\Country')
              ->willReturn($countryService);

        $countryService->expects($this->once())
                       ->method('getList')
                       ->willReturn($getList);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Form\AddNewTaxForm', $result);
    }
}
