<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\MarketingCategorySelectFormFactory;

/**
 * @author haniw
 */
class MarketingCategorySelectFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var MarketingCategorySelectFormFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new MarketingCategorySelectFormFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $categoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\CategoriesMapper')
                      ->disableOriginalConstructor()
                      ->getMock();
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('PrismProductsManager\Model\CategoriesMapper')
                        ->willReturn($categoriesMapper);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Form\MarketingCategorySelectForm', $result);
    }
}
