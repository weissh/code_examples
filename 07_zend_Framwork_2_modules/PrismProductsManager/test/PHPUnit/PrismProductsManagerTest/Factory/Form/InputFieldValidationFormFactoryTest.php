<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\InputFieldValidationFormFactory;
use PrismProductsManager\Form\InputFieldValidationForm;
use PrismProductsManager\Service\AssignFieldToTabsService;

/**
 * @author haniw
 */
class InputFieldValidationFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var InputFieldValidationFormFactory */
    protected $factory;

    /** @var  \Prophecy\Prophecy\ObjectProphecy */
    protected $serviceLocator;

    public function setUp()
    {
        $this->serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');
        $this->factory = new InputFieldValidationFormFactory();
    }

    public function testCreateService()
    {
        $assignFieldToTabsService = $this->prophesize(AssignFieldToTabsService::class);
        $this->serviceLocator->get(AssignFieldToTabsService::class)->willReturn($assignFieldToTabsService)
            ->shouldBeCalled();

        $result = $this->factory->createService($this->serviceLocator->reveal());
        $this->assertInstanceOf(InputFieldValidationForm::class, $result);
    }
}
