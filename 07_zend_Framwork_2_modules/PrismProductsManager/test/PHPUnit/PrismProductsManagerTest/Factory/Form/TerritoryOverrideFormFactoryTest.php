<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\TerritoryOverrideFormFactory;
use PrismProductsManager\Form\TerritoryOverrideForm;
use PrismProductsManager\Service\CommonAttributesService;

/**
 * @author haniw
 */
class TerritoryOverrideFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var TerritoryOverrideFormFactory */
    protected $factory;

    /** @var  \Prophecy\Prophecy\ObjectProphecy */
    protected $serviceLocator;

    public function setUp()
    {
        $this->serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');
        $this->factory = new TerritoryOverrideFormFactory();
    }

    public function testCreateService()
    {
        $commonAttributesService = $this->prophesize(CommonAttributesService::class);
        $commonAttributesService->getAllTerritories(true)->willReturn([
            0 => [
                'territoryId' => 1,
                'iso2Code' => 'iso2Code'
            ]
        ])->shouldBeCalled();
        $this->serviceLocator->get(CommonAttributesService::class)->willReturn($commonAttributesService)
            ->shouldBeCalled();

        $result = $this->factory->createService($this->serviceLocator->reveal());
        $this->assertInstanceOf(TerritoryOverrideForm::class, $result);
    }
}
