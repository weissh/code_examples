<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\ProductsRelatedProductFormFactory;

/**
 * @author haniw
 */
class ProductsRelatedProductFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsRelatedProductFormFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProductsRelatedProductFormFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Form\ProductsRelatedProductForm', $result);
    }
}
