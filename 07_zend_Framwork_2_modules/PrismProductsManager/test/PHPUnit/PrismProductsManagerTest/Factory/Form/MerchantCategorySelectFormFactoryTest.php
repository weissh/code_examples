<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\MerchantCategorySelectFormFactory;

/**
 * @author haniw
 */
class MerchantCategorySelectFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var MerchantCategorySelectFormFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new MerchantCategorySelectFormFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $merchantCategoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\MerchantCategoriesMapper')
                      ->disableOriginalConstructor()
                      ->getMock();
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('PrismProductsManager\Model\MerchantCategoriesMapper')
                        ->willReturn($merchantCategoriesMapper);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismProductsManager\Form\MerchantCategorySelectForm', $result);
    }
}
