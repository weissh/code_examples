<?php

namespace PrismProductsManagerTest\Factory\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Form\CommonAttributesValuesFormFactory;
use PrismProductsManager\Form\CommonAttributesValuesForm;
use Zend\Db\Adapter\Adapter;

/**
 * @author haniw
 */
class CommonAttributesValuesFormFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var CommonAttributesValuesFormFactory */
    protected $factory;

    /** @var  \Prophecy\Prophecy\ObjectProphecy */
    protected $serviceLocator;

    public function setUp()
    {
        $this->serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');
        $this->factory = new CommonAttributesValuesFormFactory();
    }

    public function testCreateService()
    {
        $dbAdapter = $this->prophesize(Adapter::class);
        $this->serviceLocator->get('db-pms')->willReturn($dbAdapter)
            ->shouldBeCalled();

        $result = $this->factory->createService($this->serviceLocator->reveal());
        $this->assertInstanceOf(CommonAttributesValuesForm::class, $result);
    }
}
