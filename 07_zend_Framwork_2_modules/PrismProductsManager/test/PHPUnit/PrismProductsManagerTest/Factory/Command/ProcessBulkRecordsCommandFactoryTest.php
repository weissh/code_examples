<?php

namespace PrismProductsManagerTest\Factory\Command;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Command\ProcessBulkRecordsCommand;
use PrismProductsManager\Factory\Command\ProcessBulkRecordsCommandFactory;
use PrismProductsManager\Model\BulkActionsMapper;

/**
 * @author haniw
 */
class ProcessBulkRecordsCommandFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProcessBulkRecordsCommandFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->prophesize('Zend\Mvc\Controller\ControllerManager');

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProcessBulkRecordsCommandFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $bulkActionsMapper = $this->prophesize(BulkActionsMapper::class);
        $this->serviceLocator->get(BulkActionsMapper::class)
                             ->willReturn($bulkActionsMapper)
                             ->shouldBeCalled();

        $this->controllerManager->getServiceLocator()
                                ->willReturn($this->serviceLocator)
                                ->shouldBeCalled();

        $result = $this->factory->createService($this->controllerManager->reveal());
        $this->assertInstanceOf('PrismProductsManager\Command\ProcessBulkRecordsCommand', $result);
    }
}
