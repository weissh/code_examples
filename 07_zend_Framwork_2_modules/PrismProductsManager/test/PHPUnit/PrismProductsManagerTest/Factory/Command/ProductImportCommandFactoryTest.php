<?php

namespace PrismProductsManagerTest\Factory\Command;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Command\ProductImportCommandFactory;

/**
 * @author haniw
 */
class ProductImportCommandFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductImportCommandFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->prophesize('Zend\Mvc\Controller\ControllerManager');

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProductImportCommandFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $productImport = $this->prophesize('PrismProductsManager\Service\ProductImportService');
        $this->serviceLocator->get('ProductImport')
                             ->willReturn($productImport)
                             ->shouldBeCalled();

        $this->controllerManager->getServiceLocator()
                                ->willReturn($this->serviceLocator)
                                ->shouldBeCalled();

        $result = $this->factory->createService($this->controllerManager->reveal());
        $this->assertInstanceOf('PrismProductsManager\Command\ProductImportCommand', $result);
    }
}
