<?php

namespace PrismProductsManagerTest\Factory\Command;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Command\BulkErrorReportCommand;
use PrismProductsManager\Factory\Command\BulkErrorReportCommandFactory;
use PrismProductsManager\Model\BulkActionsMapper;

/**
 * @author haniw
 */
class BulkErrorReportCommandFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var BulkErrorReportCommandFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->prophesize('Zend\Mvc\Controller\ControllerManager');

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new BulkErrorReportCommandFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $entityManager = $this->prophesize('Doctrine\ORM\EntityManager');
        $repository = $this->prophesize('PrismProductsManager\Entity\Repository\BulkProcessingRepository');
        $entityManager->getRepository('PrismProductsManager\Entity\BulkProcessing')
                      ->willReturn($repository);
                       
        $this->serviceLocator->get('Doctrine\ORM\EntityManager')
                             ->willReturn($entityManager)
                             ->shouldBeCalled();

        $mailSender = $this->prophesize('PrismCommon\Service\MailSender');
        $this->serviceLocator->get('MailSender')
                             ->willReturn($mailSender)
                             ->shouldBeCalled();

        $config = [
            'bulk-error-report' => [
                'email_to_send' => 'Alerts-PIM-Blukupload-Errors@longtallsally.com',
                'subject'       => 'PIM Bulk Upload Error Report - QA Test',
            ],
        ];
        $this->serviceLocator->get('config')
                             ->willReturn($config)
                             ->shouldBeCalled();
                                                                                                    
        $this->controllerManager->getServiceLocator()
                                ->willReturn($this->serviceLocator)
                                ->shouldBeCalled();

        $result = $this->factory->createService($this->controllerManager->reveal());
        $this->assertInstanceOf('PrismProductsManager\Command\BulkErrorReportCommand', $result);
    }
}
