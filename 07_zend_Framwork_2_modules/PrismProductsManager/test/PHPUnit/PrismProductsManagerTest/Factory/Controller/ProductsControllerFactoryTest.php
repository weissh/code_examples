<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\ProductsControllerFactory;

/**
 * @author haniw
 */
class ProductsControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProductsControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
                           ->disableOriginalConstructor()
                           ->getMock();
        $config = [
           'languages' => [
               'current-language' => 1,
           ],
           'websites' => [
               'current-website-id' => 1,
               'website-url' => [
                   '1' => 'http://nauticalia-website.local/',
                   '2' => 'http://nauticalia-website.local/',
                   '3' => 'http://nauticalia-website.local/',
               ],
               'websites-currencies' => [
                   1 => 1,
                   2 => 2,
                   3 => 3,
               ],
           ],
           'db' => [
                'adapters' => [
                    'db-pms' => [
                        'dsn'      => 'mysql:dbname=prism_pms_TEST;host=localhost',
                        'username' => 'root',
                        'password' => '',
                        'driver' => 'Pdo',
                        'driver_options' => [
                            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
                        ],
                    ],
                ],
           ],
        ];

        $this->serviceLocator->expects($this->at(0))
            ->method('get')
            ->with('Config')
            ->willReturn($config);

        $commonStorage->expects($this->at(0))
                      ->method('read')
                      ->with('siteConfig')
                      ->willReturn(['languages' => [
        'current-language' => 1,
    ],
           'websites' => [
        'current-website-id' => 1,
        'website-url' => [
            '1' => 'http://nauticalia-website.local/',
            '2' => 'http://nauticalia-website.local/',
            '3' => 'http://nauticalia-website.local/',
        ],
        'websites-currencies' => [
            1 => 1,
            2 => 2,
            3 => 3,
        ],
    ]]);
        $commonStorage->expects($this->at(1))
            ->method('read')
            ->with('clientConfig')
            ->willReturn(array(
                'clientCode' => 'lts',
                'features' => array(
                    'editableSku' => false,
                    'productAddOn' => false,
                    'productTabs' => false,
                    'category_image_banner' => false,
                    'commonAttributes' => true,
                    'dynamicInputs' => true,
                    'menu' => array(
                        'cms' => array(
                            'main-display' => false,
                            'pages' => array(
                                'main-display' => false,
                                'new-page' => false,
                                'manage-pages' => false
                            ),
                            'headers' => array(
                                'main-display' => false,
                                'manage-headers' => false
                            ),
                            'navigation' => array(
                                'main-display' => false,
                                'new-navigation' => false,
                                'manage-navigations' => false
                            ),
                            'template-manager' => array(
                                'main-display' => false,
                                'new-template' => false,
                                'manage-templates' => false
                            ),
                            'carousel' => array(
                                'main-display' => false,
                                'new-carousel' => false,
                                'manage-carousels' => false
                            ),
                            'popup-windows' => array(
                                'main-display' => false,
                                'new-popup' => false,
                                'manage-popups' => false
                            ),
                            'tag-manager' => array(
                                'main-display' => false,
                                'view-tag-manager' => false,
                                'data-layer' => false,
                                'new-data-element' => false,
                                'data-elements' => false,
                                'edit-data-elements' => false
                            ),
                            'emails' => array(
                                'main-display' => false,
                                'new-emailer' => false,
                                'manage-emails' => false
                            ),
                            'blog' => array(
                                'main-display' => false,
                                'blog-page' => false,
                                'admin-page' => false
                            )
                        ),
                        'pms' => array(
                            'main-display' => true,
                            'products' => array(
                                'main-display' => true,
                                'manage-products' => true,
                                'new-product' => true,
                                'assign-fields-to-tabs' => true
                            ),
                            'product-categories' => array(
                                'main-display' => false,
                                'manage-marketing-categories' => false,
                                'manage-merchant-categories' => false
                            ),
                            'product-attributes' => array(
                                'main-display' => true,
                                'manage-merchant-attributes' => false,
                                'bulk-upload-merchant-attributes' => true,
                                'manage-marketing-attributes' => false,
                                'manage-core-attributes' => false,
                                'manage-common-attributes' => true,
                                'manage-colours' => true
                            ),
                            'tools' => array(
                                'main-display' => true,
                                'bulk-actions' => false,
                                'bulk-upload' => true,
                                'tax' => false,
                                'barcode-upload' => true,
                                'bulk-currency-conversion' => false
                            )
                        ),
                        'media' => array(
                            'main-display' => false,
                            'media-server-management' => array(
                                'main-display' => false,
                                'upload-file' => false,
                                'manage-media-library' => false,
                                'media-server-admin' => false
                            )
                        ),
                        'promotions' => array(
                            'main-display' => false,
                            'promotions-management' => array(
                                'main-display' => false,
                                'new-promotion' => false,
                                'manage-promotions' => false
                            )
                        ),
                        'admin' => array(
                            'main-display' => true,
                            'user-management' => array(
                                'main-display' => true,
                                'new-user' => true,
                                'manage-users' => true,
                                'create-permission' => true,
                                'manage-permission' => true
                            ),
                            'languages' => array(
                                'main-display' => false,
                                'add-new-language' => false,
                                'new-language-from-current' => false,
                                'manage-languages' => false,
                                'manage-placeholders' => false,
                                'manage-translations' => false
                            ),
                            'search-manager' => array(
                                'main-display' => false,
                                'search-priorities' => false
                            ),
                            'options' => array(
                                'main-display' => false,
                                'settings' => false,
                                'event-log' => false,
                                'reporting' => false,
                                'logout' => false
                            ),
                            'reports' => array(
                                'main-display' => false,
                                'products-comparison-report' => false,
                                'users-comparison-report' => false,
                                'product-report' => false
                            ),
                            'files' => array(
                                'main-display' => false,
                                'upload-file' => false,
                                'list-files' => false
                            )
                        ),
                        'logout' => array(
                            'main-display' => true
                        )
                    ),
                    'firstWorkflow' => true
                ),
                'settings' => array(
                    'manualSync' => false,
                    'default_addBcc' => array(
                        'cristian.popescu@whistl.co.uk',
                        'vilius.vaitiekunas@whistl.co.uk'
                    ),
                    'carousel_slider' => 'bxSlider',
                    'commonAttributesForAdmin' => true, // Flag used to define if common attributes are manageable by administrators or all users
                    'showOptionsWhenSavingAtStyleLevel' => false,
                    'standardStylePrice' => 0
                )
            ));


        $this->serviceLocator->expects($this->at(1))
                        ->method('get')
                        ->with('CommonStorage')
                        ->willReturn($commonStorage);

        $curencyMapper = $this->getMockBuilder('PrismProductsManager\Model\CurrencyMapper')
                      ->disableOriginalConstructor()
                      ->getMock();
        $curencyMapper->expects($this->once())
                      ->method('getAllCurrencies')
                      ->willReturn([
            1 => [
                'currencyId' => 1,
                'currency' => 'EUR',
            ],
            2 => [
                'currencyId' => 2,
                'currency' => 'EUR',
            ],
            3 => [
                'currencyId' => 3,
                'currency' => 'EUR',
            ],
        ]);
        $this->serviceLocator->expects($this->at(2))
                        ->method('get')
                        ->with('PrismProductsManager\Model\CurrencyMapper')
                        ->willReturn($curencyMapper);

        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $this->serviceLocator->expects($this->at(3))
                             ->method('get')
                             ->with('PrismProductsManager\Model\ProductsMapper')
                             ->willReturn($productsMapper);

        $categoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\CategoriesMapper')
                           ->disableOriginalConstructor()
                           ->getMock();
        $this->serviceLocator->expects($this->at(4))
                       ->method('get')
                       ->with('PrismProductsManager\Model\CategoriesMapper')
                       ->willReturn($categoriesMapper);

        $merchantCategoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\MerchantCategoriesMapper')
                ->disableOriginalConstructor()
               ->getMock();
        $this->serviceLocator->expects($this->at(5))
                 ->method('get')
                 ->with('PrismProductsManager\Model\MerchantCategoriesMapper')
                 ->willReturn($merchantCategoriesMapper);

        $taxMapper = $this->getMockBuilder('PrismProductsManager\Model\TaxMapper')
             ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(6))
              ->method('get')
              ->with('PrismProductsManager\Model\TaxMapper')
              ->willReturn($taxMapper);


        $this->serviceLocator->expects($this->at(7))
            ->method('get')
            ->with('PrismProductsManager\Form\ProductAttributesSelectForm')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Form\ProductAttributesSelectForm')
                     ->disableOriginalConstructor()
                     ->getMock()
        );

        $this->serviceLocator->expects($this->at(8))
            ->method('get')
            ->with('PrismProductsManager\Form\MarketingCategorySelectForm')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Form\MarketingCategorySelectForm')
                     ->disableOriginalConstructor()
                     ->getMock()
        );

        $this->serviceLocator->expects($this->at(9))
            ->method('get')
            ->with('PrismProductsManager\Form\MerchantCategorySelectForm')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Form\MerchantCategorySelectForm')
                     ->disableOriginalConstructor()
                     ->getMock()
        );

        $this->serviceLocator->expects($this->at(10))
            ->method('get')
            ->with('PrismProductsManager\Form\ProductsBasicInformationForm')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Form\ProductsBasicInformationForm')
                     ->disableOriginalConstructor()
                     ->getMock()
        );

        $this->serviceLocator->expects($this->at(11))
            ->method('get')
            ->with('PrismProductsManager\Form\ProductsActivationForm')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Form\ProductsActivationForm')
                     ->disableOriginalConstructor()
                     ->getMock()
        );

        $this->serviceLocator->expects($this->at(12))
            ->method('get')
            ->with('PrismProductsManager\Form\ProductVariantCreationForm')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Form\ProductVariantsCreationForm')
                     ->disableOriginalConstructor()
                     ->getMock()
        );

        $this->serviceLocator->expects($this->at(13))
            ->method('get')
            ->with('PrismProductsManager\Form\ProductsRelatedProductForm')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Form\ProductsRelatedProductForm')
                     ->disableOriginalConstructor()
                     ->getMock()
        );

        $this->serviceLocator->expects($this->at(14))
            ->method('get')
            ->with('PrismProductsManager\Form\ProductPricesSelectForm')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Form\ProductPricesSelectForm')
                     ->disableOriginalConstructor()
                     ->getMock()
        );

        $this->serviceLocator->expects($this->at(15))
            ->method('get')
            ->with('PrismProductsManager\Form\ProductSupplierPriceForm')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Form\ProductSupplierPriceForm')
                     ->disableOriginalConstructor()
                     ->getMock()
        );

        $this->serviceLocator->expects($this->at(16))
            ->method('get')
            ->with('PrismProductsManager\Form\BulkProductActionForm')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Form\BulkProductActionForm')
                     ->disableOriginalConstructor()
                     ->getMock()
        );

        $productCreateCollationVariantsForm = $this->getMockBuilder('PrismProductsManager\Form\ProductsCreateCollationVariantsForm')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(17))
            ->method('get')
            ->with('PrismProductsManager\Form\ProductsCreateCollationVariantsForm')
            ->willReturn($productCreateCollationVariantsForm);

        $productsAddOnForm = $this->getMockBuilder('PrismProductsManager\Form\ProductsAddOnForm')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(18))
            ->method('get')
            ->with('PrismProductsManager\Form\ProductsAddOnForm')
            ->willReturn($productsAddOnForm);

        $productsTabsForm = $this->getMockBuilder('PrismProductsManager\Form\ProductsTabsForm')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(19))
            ->method('get')
            ->with('PrismProductsManager\Form\ProductsTabsForm')
            ->willReturn($productsTabsForm);

        $formElementManager = $this->getMockBuilder('Zend\Form\FormElementManager')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $searchForm = $this->getMockBuilder('PrismProductsManager\Form\SearchForm')
                                    ->disableOriginalConstructor()
                                    ->getMock();
        $formElementManager->expects($this->exactly(1))
                           ->method('get')
                           ->with('PrismProductsManager\Form\SearchForm')
                           ->willReturn($searchForm);

        $this->serviceLocator->expects($this->at(20))
            ->method('get')
            ->with('FormElementManager')
            ->willReturn($formElementManager);



        $queueService = $this->getMockBuilder('PrismQueueManager\Service\QueueService')
                          ->disableOriginalConstructor()
                          ->getMock();
        $this->serviceLocator->expects($this->at(21))
                      ->method('get')
                      ->with('QueueService')
                      ->willReturn($queueService);


        $this->serviceLocator->expects($this->at(22))
            ->method('get')
            ->with('PrismAuth\Model\ApiWishlistMapper')
            ->willReturn(
                $this->getMockBuilder('PrismAuth\Model\ApiWishlistMapper')
                    ->disableOriginalConstructor()
                    ->getMock()
            );

        $this->serviceLocator->expects($this->at(23))
          ->method('get')
          ->with('PrismProductsManager\Form\VariantsBasicInformationForm')
          ->willReturn(
              $this->getMockBuilder('PrismProductsManager\Form\VariantsBasicInformationForm')
                   ->disableOriginalConstructor()
                   ->getMock()
        );

        $this->serviceLocator->expects($this->at(24))
            ->method('get')
            ->with('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesDefinitionsTable')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesDefinitionsTable')
                    ->disableOriginalConstructor()
                    ->getMock()
            );

        $this->serviceLocator->expects($this->at(25))
            ->method('get')
            ->with('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable')
                    ->disableOriginalConstructor()
                    ->getMock()
            );

        $this->serviceLocator->expects($this->at(26))
            ->method('get')
            ->with('PrismProductsManager\Service\InputService')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Service\InputService')
                    ->disableOriginalConstructor()
                    ->getMock()
            );

        $this->serviceLocator->expects($this->at(27))
            ->method('get')
            ->with('PrismProductsManager\Service\CommonAttributesService')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Service\CommonAttributesService')
                    ->disableOriginalConstructor()
                    ->getMock()
            );
        $this->serviceLocator->expects($this->at(28))
            ->method('get')
            ->with('PrismProductsManager\Service\WorkflowService')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Service\WorkflowService')
                    ->disableOriginalConstructor()
                    ->getMock()
            );

        $formElementManager = $this->getMockBuilder('Zend\Form\FormElementManager')
            ->disableOriginalConstructor()
            ->getMock();

        $this->serviceLocator->expects($this->at(29))
            ->method('get')
            ->with('FormElementManager')
            ->willReturn(
                $formElementManager
            );

        $formElementManager->expects($this->exactly(1))
                            ->method('get')
                            ->with('PrismProductsManager\Form\InputFieldsForm')
                            ->willReturn(
                                $this->getMockBuilder('PrismProductsManager\Form\InputFieldsForm')
                                    ->disableOriginalConstructor()
                                    ->getMock()
                            );

        $this->serviceLocator->expects($this->at(30))
            ->method('get')
            ->with('PrismProductsManager\Model\ColoursMapper')
            ->willReturn(
                $this->getMockBuilder('PrismProductsManager\Model\ColoursMapper')
                    ->disableOriginalConstructor()
                    ->getMock()
            );


        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\ProductsController', $result);
    }
}
