<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\ScheduleControllerFactory;

/**
 * @author haniw
 */
class ScheduleControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ScheduleControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->prophesize('Zend\Mvc\Controller\ControllerManager');

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ScheduleControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $scheduleService = $this->prophesize('PrismProductsManager\Service\ScheduleService');
        $this->serviceLocator->get('PrismProductsManager\Service\ScheduleService')
                             ->willReturn($scheduleService)
                             ->shouldBeCalled();

        $viewRenderer = $this->prophesize('Zend\View\Renderer\PhpRenderer');
        $this->serviceLocator->get('viewrenderer')
            ->willReturn($viewRenderer)
            ->shouldBeCalled();

        $inputService = $this->prophesize('PrismProductsManager\Service\InputService');
        $this->serviceLocator->get('PrismProductsManager\Service\InputService')
            ->willReturn($inputService)
            ->shouldBeCalled();
            
        $bulkActionsMapper = $this->prophesize('PrismProductsManager\Model\BulkActionsMapper');
        $this->serviceLocator->get('PrismProductsManager\Model\BulkActionsMapper')
                             ->willReturn($bulkActionsMapper)
                             ->shouldBeCalled();

        $this->controllerManager->getServiceLocator()
                                ->willReturn($this->serviceLocator)
                                ->shouldBeCalled();

        $result = $this->factory->createService($this->controllerManager->reveal());
        $this->assertInstanceOf('PrismProductsManager\Controller\ScheduleController', $result);
    }
}
