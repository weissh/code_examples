<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\BulkUploadControllerFactory;
use PrismProductsManager\Controller\BulkUploadController;
use PrismProductsManager\Model\BulkActionsMapper;
use PrismProductsManager\Form\BulkUploadForm;
use Zend\Form\FormElementManager;

/**
 * @author haniw
 */
class BulkUploadControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var BulkUploadControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->prophesize('Zend\Mvc\Controller\ControllerManager');

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new BulkUploadControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $formElementManager = $this->prophesize(FormElementManager::class);
        $this->serviceLocator->get('FormElementManager')
                             ->willReturn($formElementManager)
                             ->shouldBeCalled();

        $bulkUploadForm = $this->prophesize(BulkUploadForm::class);
        $formElementManager->get(BulkUploadForm::class)
                           ->willReturn($bulkUploadForm)
                           ->shouldBeCalled();

        $bulkActionsMapper = $this->prophesize(BulkActionsMapper::class);
        $this->serviceLocator->get(BulkActionsMapper::class)
                             ->willReturn($bulkActionsMapper)
                             ->shouldBeCalled();

        $this->controllerManager->getServiceLocator()
                                ->willReturn($this->serviceLocator)
                                ->shouldBeCalled();

        $result = $this->factory->createService($this->controllerManager->reveal());
        $this->assertInstanceOf(BulkUploadController::class, $result);
    }
}
