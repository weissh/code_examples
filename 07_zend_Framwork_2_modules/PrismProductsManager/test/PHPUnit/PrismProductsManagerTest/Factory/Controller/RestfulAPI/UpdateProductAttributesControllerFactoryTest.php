<?php

namespace PrismProductsManagerTest\Factory\Controller\RestfulAPI;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Controller\RestfulAPI\UpdateProductAttributesController;
use PrismProductsManager\Factory\Controller\RestfulAPI\UpdateProductAttributesControllerFactory;
use PrismProductsManager\Model\ProductsMapper;
use PrismProductsManager\Service\CommonAttributesService;
use Zend\Mvc\Controller\ControllerManager;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @author haniw
 */
class UpdateProductAttributesControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var UpdateProductAttributesControllerFactory */
    protected $factory;

    /** @var \Prophecy\Prophecy\ObjectProphecy */
    protected $serviceLocator;

    /** @var \Prophecy\Prophecy\ObjectProphecy */
    protected $controllerManager;

    protected function setUp()
    {
        $this->serviceLocator = $this->prophesize(ServiceLocatorInterface::class);
        $this->controllerManager = $this->prophesize(ControllerManager::class);

        $this->factory = new UpdateProductAttributesControllerFactory();
    }

    public function testCreateService()
    {
        $productsMapper = $this->prophesize(ProductsMapper::class);
        $this->serviceLocator->get(ProductsMapper::class)->willReturn($productsMapper)
                                                         ->shouldBeCalled();

        $config = [
            'siteConfig' => [
                'languages' =>[
                    'site-languages' => [
                        1
                    ]
                ],
            ],
        ];
        $this->serviceLocator->get('Config')->willReturn($config)
            ->shouldBeCalled();

        $commonAttributesConfig = $this->prophesize(CommonAttributesService::class);
        $this->serviceLocator->get(CommonAttributesService::class)->willReturn($commonAttributesConfig)
                             ->shouldBeCalled();

        $this->controllerManager->getServiceLocator()->willReturn($this->serviceLocator)
                                                     ->shouldBeCalled();

        $this->assertInstanceOf(UpdateProductAttributesController::class, $this->factory->createService($this->controllerManager->reveal()));
    }
}
