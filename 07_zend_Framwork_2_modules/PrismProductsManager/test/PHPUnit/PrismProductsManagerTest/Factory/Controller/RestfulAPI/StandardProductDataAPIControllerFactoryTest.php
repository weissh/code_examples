<?php

namespace PrismProductsManagerTest\Factory\Controller\RestfulAPI;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Controller\RestfulAPI\StandardProductDataAPIController;
use PrismProductsManager\Factory\Controller\RestfulAPI\StandardProductDataAPIControllerFactory;
use PrismProductsManager\Model\ProductsMapper;
use PrismProductsManager\Service\CommonAttributesService;
use PrismProductsManager\Service\ScheduleService;
use Zend\Mvc\Controller\ControllerManager;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @author haniw
 */
class StandardProductDataAPIControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductDataAPIControllerFactory */
    protected $factory;

    /** @var \Prophecy\Prophecy\ObjectProphecy */
    protected $serviceLocator;

    /** @var \Prophecy\Prophecy\ObjectProphecy */
    protected $controllerManager;

    protected function setUp()
    {
        $this->serviceLocator = $this->prophesize(ServiceLocatorInterface::class);
        $this->controllerManager = $this->prophesize(ControllerManager::class);

        $this->factory = new StandardProductDataAPIControllerFactory();
    }

    public function testCreateService()
    {
        $productsMapper = $this->prophesize(ProductsMapper::class);
        $this->serviceLocator->get(ProductsMapper::class)->willReturn($productsMapper)
                                                         ->shouldBeCalled();

        $scheduleService = $this->prophesize(ScheduleService::class);
        $this->serviceLocator->get(ScheduleService::class)->willReturn($scheduleService)
            ->shouldBeCalled();

        $config = [
            'siteConfig' => [
                'languages' =>[
                    'site-languages' => [
                        1
                    ]
                ],
            ],
        ];
        $this->serviceLocator->get('Config')->willReturn($config)
            ->shouldBeCalled();

        $commonAttributesService = $this->prophesize(CommonAttributesService::class);
        $this->serviceLocator->get(CommonAttributesService::class)->willReturn($commonAttributesService)
            ->shouldBeCalled();

        $this->controllerManager->getServiceLocator()->willReturn($this->serviceLocator)
                                                     ->shouldBeCalled();

        $this->assertInstanceOf(StandardProductDataAPIController::class, $this->factory->createService($this->controllerManager->reveal()));
    }
}
