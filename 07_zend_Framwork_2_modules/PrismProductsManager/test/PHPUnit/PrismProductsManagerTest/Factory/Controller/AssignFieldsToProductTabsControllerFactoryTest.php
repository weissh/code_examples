<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\AssignFieldsToProductTabsControllerFactory;
use PrismProductsManager\Controller\AssignFieldsToProductTabsController;
use Zend\Form\FormElementManager;

/**
 * @author haniw
 */
class AssignFieldsToProductTabsControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var AssignFieldsToProductTabsControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->prophesize('Zend\Mvc\Controller\ControllerManager');

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new AssignFieldsToProductTabsControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $config = ['clientConfig' => []];
        $this->serviceLocator->get('config')
            ->willReturn($config)
            ->shouldBeCalled();

        $assignToTabService = $this->prophesize('PrismProductsManager\Service\AssignFieldToTabsService');
        $this->serviceLocator->get('PrismProductsManager\Service\AssignFieldToTabsService')
                             ->willReturn($assignToTabService)
                             ->shouldBeCalled();

        $viewRenderer = $this->prophesize('Zend\View\Renderer\PhpRenderer');
        $this->serviceLocator->get('viewrenderer')
            ->willReturn($viewRenderer)
            ->shouldBeCalled();

        $formElementManager = $this->prophesize(FormElementManager::class);
        $searchForm         = $this->prophesize('PrismProductsManager\Form\SearchForm');
        $formElementManager->get('PrismProductsManager\Form\SearchForm')
                           ->willReturn($searchForm)
                           ->shouldBeCalled() ;

        $this->serviceLocator->get('FormElementManager')->willReturn($formElementManager)
                                                        ->shouldBeCalled();

        $assignInputFieldsForm         = $this->prophesize('PrismProductsManager\Form\AssignInputFieldsForm');
        $formElementManager->get('PrismProductsManager\Form\AssignInputFieldsForm')
            ->willReturn($assignInputFieldsForm)
            ->shouldBeCalled();

        $this->serviceLocator->get('FormElementManager')->willReturn($formElementManager)
            ->shouldBeCalled();

        $inputFieldValidationForm         = $this->prophesize('PrismProductsManager\Form\InputFieldValidationForm');
        $formElementManager->get('PrismProductsManager\Form\InputFieldValidationForm')
            ->willReturn($inputFieldValidationForm)
            ->shouldBeCalled();

        $this->serviceLocator->get('FormElementManager')->willReturn($formElementManager)
            ->shouldBeCalled();


        $this->controllerManager->getServiceLocator()
            ->willReturn($this->serviceLocator)
            ->shouldBeCalled();

        $result = $this->factory->createService($this->controllerManager->reveal());
        $this->assertInstanceOf(AssignFieldsToProductTabsController::class, $result);
    }
}
