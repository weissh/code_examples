<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\CategoriesControllerFactory;

/**
 * @author haniw
 */
class CategoriesControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var CategoriesControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
            ->disableOriginalConstructor()
            ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new CategoriesControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $formElementManager = $this->getMockBuilder('Zend\Form\FormElementManager')
            ->disableOriginalConstructor()
            ->getMock();
        $addCategoryForm = $this->getMockBuilder('PrismProductsManager\Form\AddCategoryForm')
            ->disableOriginalConstructor()
            ->getMock();
        $formElementManager->expects($this->at(0))
            ->method('get')
            ->with('PrismProductsManager\Form\AddCategoryForm')
            ->willReturn($addCategoryForm);

        $searchForm = $this->getMockBuilder('PrismProductsManager\Form\SearchForm')
            ->disableOriginalConstructor()
            ->getMock();
        $formElementManager->expects($this->at(1))
            ->method('get')
            ->with('PrismProductsManager\Form\SearchForm')
            ->willReturn($searchForm);

        $addVariantToCategoryForm = $this->getMockBuilder('PrismProductsManager\Form\AddVariantToCategoryForm')
            ->disableOriginalConstructor()
            ->getMock();
        $formElementManager->expects($this->at(2))
            ->method('get')
            ->with('PrismProductsManager\Form\AddVariantToCategoryForm')
            ->willReturn($addVariantToCategoryForm);

        $this->serviceLocator->expects($this->at(0))
            ->method('get')
            ->with('FormElementManager')
            ->willReturn($formElementManager);

        $categoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\CategoriesMapper')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(1))
            ->method('get')
            ->with('PrismProductsManager\Model\CategoriesMapper')
            ->willReturn($categoriesMapper);

        $simplePagination = $this->getMockBuilder('PrismCommon\Service\SimplePagination')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(2))
            ->method('get')
            ->with('SimplePagination')
            ->willReturn($simplePagination);

        $dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(3))
            ->method('get')
            ->with('db-pms')
            ->willReturn($dbPms);

        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(4))
            ->method('get')
            ->with('PrismProductsManager\Model\ProductsMapper')
            ->willReturn($productsMapper);

        $updateNavigationService = $this->getMockBuilder('PrismContentManager\Service\UpdateNavigation')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(5))
            ->method('get')
            ->with('PrismContentManager\Service\UpdateNavigation')
            ->willReturn($updateNavigationService);

        $queueService = $this->getMockBuilder('PrismQueueManager\Service\QueueService')
            ->disableOriginalConstructor()
            ->getMock();
        $this->serviceLocator->expects($this->at(6))
            ->method('get')
            ->with('QueueService')
            ->willReturn($queueService);

        $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
            ->disableOriginalConstructor()
            ->getMock();

        $config = [
            'languages' => [
                'current-language' => 1,
            ],
            'websites' => [
                'current-website-id' => 1,
                'website-url' => [
                    '1' => 'http://nauticalia-website.local/',
                    '2' => 'http://nauticalia-website.local/',
                    '3' => 'http://nauticalia-website.local/',
                ],
            ],
        ];
        $commonStorage->expects($this->at(0))
            ->method('read')
            ->with('siteConfig')
            ->willReturn($config);
        $commonStorage->expects($this->at(1))
            ->method('read')
            ->with('clientConfig')
            ->willReturn([]);
        $this->serviceLocator->expects($this->at(7))
            ->method('get')
            ->with('CommonStorage')
            ->willReturn($commonStorage);

        $this->controllerManager->expects($this->exactly(1))
            ->method('getServiceLocator')
            ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\CategoriesController', $result);
    }
}
