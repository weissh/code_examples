<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\MerchantAttributesControllerFactory;

/**
 * @author haniw
 */
class MerchantAttributesControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var MerchantAttributesControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new MerchantAttributesControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                      ->disableOriginalConstructor()
                      ->getMock();
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('db-pms')
                        ->willReturn($dbPms);

        $formElementManager = $this->getMockBuilder('Zend\Form\FormElementManager')
                                   ->disableOriginalConstructor()
                                   ->getMock();

        $addAttributesMerchantGroupForm = $this->getMockBuilder('PrismProductsManager\Form\AddAttributesMerchantGroupForm')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $formElementManager->expects($this->at(0))
                          ->method('get')
                          ->with('PrismProductsManager\Form\AddAttributesMerchantGroupForm')
                          ->willReturn($addAttributesMerchantGroupForm);

        $downloadBulkForm = $this->getMockBuilder('PrismProductsManager\Form\DownloadBulkForm')
                                    ->disableOriginalConstructor()
                                    ->getMock();
        $formElementManager->expects($this->at(1))
                           ->method('get')
                           ->with('PrismProductsManager\Form\DownloadBulkForm')
                           ->willReturn($downloadBulkForm);

        $uploadBulkForm = $this->getMockBuilder('PrismProductsManager\Form\UploadBulkForm')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $formElementManager->expects($this->at(2))
                          ->method('get')
                          ->with('PrismProductsManager\Form\UploadBulkForm')
                          ->willReturn($uploadBulkForm);

        $addAttributesColourGroupsForm = $this->getMockBuilder('PrismProductsManager\Form\AddAttributesColourGroupsForm')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $formElementManager->expects($this->at(3))
                        ->method('get')
                        ->with('PrismProductsManager\Form\AddAttributesColourGroupsForm')
                        ->willReturn($addAttributesColourGroupsForm);

        $addAttributesExistingColoursGroupForm = $this->getMockBuilder('PrismProductsManager\Form\AddAttributesExistingColoursGroupForm')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $formElementManager->expects($this->at(4))
                        ->method('get')
                        ->with('PrismProductsManager\Form\AddAttributesExistingColoursGroupForm')
                        ->willReturn($addAttributesExistingColoursGroupForm);

        $searchForm = $this->getMockBuilder('PrismProductsManager\Form\SearchForm')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $formElementManager->expects($this->at(5))
                        ->method('get')
                        ->with('PrismProductsManager\Form\SearchForm')
                        ->willReturn($searchForm);

        $this->serviceLocator->expects($this->at(1))
                             ->method('get')
                             ->with('FormElementManager')
                             ->willReturn($formElementManager);

        $simplePagination = $this->getMockBuilder('PrismCommon\Service\SimplePagination')
                  ->disableOriginalConstructor()
                  ->getMock();
        $this->serviceLocator->expects($this->at(2))
                    ->method('get')
                    ->with('SimplePagination')
                    ->willReturn($simplePagination);

        $attributesGroupMapper = $this->getMockBuilder('PrismProductsManager\Model\AttributesGroupMapper')
                  ->disableOriginalConstructor()
                  ->getMock();
        $this->serviceLocator->expects($this->at(3))
                    ->method('get')
                    ->with('PrismProductsManager\Model\AttributesGroupMapper')
                    ->willReturn($attributesGroupMapper);

        $coloursMapper = $this->getMockBuilder('PrismProductsManager\Model\ColoursMapper')
                  ->disableOriginalConstructor()
                  ->getMock();
        $this->serviceLocator->expects($this->at(4))
                    ->method('get')
                    ->with('PrismProductsManager\Model\ColoursMapper')
                    ->willReturn($coloursMapper);

        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $this->serviceLocator->expects($this->at(5))
                             ->method('get')
                             ->with('PrismProductsManager\Model\ProductsMapper')
                             ->willReturn($productsMapper);

        $queueService = $this->getMockBuilder('PrismQueueManager\Service\QueueService')
                           ->disableOriginalConstructor()
                           ->getMock();
        $this->serviceLocator->expects($this->at(6))
                       ->method('get')
                       ->with('QueueService')
                       ->willReturn($queueService);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\MerchantAttributesController', $result);
    }
}
