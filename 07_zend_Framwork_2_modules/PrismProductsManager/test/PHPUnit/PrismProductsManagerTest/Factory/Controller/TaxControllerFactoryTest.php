<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\TaxControllerFactory;

/**
 * @author haniw
 */
class TaxControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var TaxControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new TaxControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $taxMapper = $this->getMockBuilder('PrismProductsManager\Model\TaxMapper')
                      ->disableOriginalConstructor()
                      ->getMock();
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('PrismProductsManager\Model\TaxMapper')
                        ->willReturn($taxMapper);

        $formElementManager = $this->getMockBuilder('Zend\Form\FormElementManager')
                                   ->disableOriginalConstructor()
                                   ->getMock();

        $addNewTaxForm = $this->getMockBuilder('PrismProductsManager\Form\AddNewTaxForm')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $formElementManager->expects($this->at(0))
                          ->method('get')
                          ->with('PrismProductsManager\Form\AddNewTaxForm')
                          ->willReturn($addNewTaxForm);

        $this->serviceLocator->expects($this->at(1))
                             ->method('get')
                             ->with('FormElementManager')
                             ->willReturn($formElementManager);

        $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
                            ->disableOriginalConstructor()
                            ->getMock();

        $config = [
            'languages' => [
                'current-language' => 1,
            ],
        ];
        $commonStorage->expects($this->once())
                    ->method('read')
                    ->with('siteConfig')
                    ->willReturn($config);
        $this->serviceLocator->expects($this->at(2))
                         ->method('get')
                         ->with('CommonStorage')
                         ->willReturn($commonStorage);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\TaxController', $result);
    }
}
