<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\BarcodeUploadControllerFactory;

/**
 * @author haniw
 */
class BarcodeUploadControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var BarcodeUploadControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new BarcodeUploadControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                                  ->disableOriginalConstructor()
                                  ->getMock();
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('PrismProductsManager\Model\ProductsMapper')
                        ->willReturn($productsMapper);

        $formElementManager = $this->getMockBuilder('Zend\Form\FormElementManager')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $uploadBarcodesForm = $this->getMockBuilder('PrismProductsManager\Form\UploadBarcodesForm')
                                    ->disableOriginalConstructor()
                                    ->getMock();
        $formElementManager->expects($this->exactly(1))
                           ->method('get')
                           ->with('PrismProductsManager\Form\UploadBarcodesForm')
                           ->willReturn($uploadBarcodesForm);

        $this->serviceLocator->expects($this->at(1))
                       ->method('get')
                       ->with('FormElementManager')
                       ->willReturn($formElementManager);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\BarcodeUploadController', $result);
    }
}
