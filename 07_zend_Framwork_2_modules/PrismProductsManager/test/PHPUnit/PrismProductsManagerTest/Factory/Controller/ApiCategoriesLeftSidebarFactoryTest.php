<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\ApiCategoriesLeftSidebarFactory;

/**
 * @author haniw
 */
class ApiCategoriesLeftSidebarFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiCategoriesLeftSidebarFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ApiCategoriesLeftSidebarFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $apiCategoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\ApiCategoriesMapper')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $this->serviceLocator->expects($this->at(0))
                             ->method('get')
                             ->with('PrismProductsManager\Model\ApiCategoriesMapper')
                             ->willReturn($apiCategoriesMapper);

        $categoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\CategoriesMapper')
                              ->disableOriginalConstructor()
                              ->getMock();
        $this->serviceLocator->expects($this->at(1))
                          ->method('get')
                          ->with('PrismProductsManager\Model\CategoriesMapper')
                          ->willReturn($categoriesMapper);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\ApiCategoriesLeftSidebarController', $result);
    }
}
