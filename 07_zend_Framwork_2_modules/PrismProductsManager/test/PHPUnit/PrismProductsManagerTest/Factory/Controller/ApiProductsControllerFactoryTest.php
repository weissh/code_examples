<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\ApiProductsControllerFactory;

/**
 * @author haniw
 */
class ApiProductsControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiProductsControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ApiProductsControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $this->serviceLocator->expects($this->at(0))
                             ->method('get')
                             ->with('PrismProductsManager\Model\ProductsMapper')
                             ->willReturn($productsMapper);

        $queueService = $this->getMockBuilder('PrismQueueManager\Service\QueueService')
                              ->disableOriginalConstructor()
                              ->getMock();
        $this->serviceLocator->expects($this->at(1))
                          ->method('get')
                          ->with('QueueService')
                          ->willReturn($queueService);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\ApiProductsController', $result);
    }
}
