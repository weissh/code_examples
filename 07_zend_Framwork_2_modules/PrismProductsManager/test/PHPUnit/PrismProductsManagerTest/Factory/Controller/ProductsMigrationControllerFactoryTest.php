<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\ProductsMigrationControllerFactory;

/**
 * @author haniw
 */
class ProductsMigrationControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsMigrationControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ProductsMigrationControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $productsMigrationMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMigrationMapper')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $this->serviceLocator->expects($this->at(0))
                             ->method('get')
                             ->with('ProductsMigrationMapper')
                             ->willReturn($productsMigrationMapper);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\ProductsMigrationController', $result);
    }
}
