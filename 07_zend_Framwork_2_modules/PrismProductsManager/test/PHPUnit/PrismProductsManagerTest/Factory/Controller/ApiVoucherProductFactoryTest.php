<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\ApiVoucherProductFactory;

/**
 * @author haniw
 */
class ApiVoucherProductFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiVoucherProductFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ApiVoucherProductFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $apiProductsMapper = $this->getMockBuilder('PrismProductsManager\Model\ApiProductsMapper')
                                  ->disableOriginalConstructor()
                                  ->getMock();
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('PrismProductsManager\Model\ApiProductsMapper')
                        ->willReturn($apiProductsMapper);

        $productFolderMapper = $this->getMockBuilder('PrismMediaManager\Model\ProductFolderMapper')
                                  ->disableOriginalConstructor()
                                  ->getMock();
        $this->serviceLocator->expects($this->at(1))
                        ->method('get')
                        ->with('PrismMediaManager\Model\ProductFolderMapper')
                        ->willReturn($productFolderMapper);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\ApiVoucherProduct', $result);
    }
}
