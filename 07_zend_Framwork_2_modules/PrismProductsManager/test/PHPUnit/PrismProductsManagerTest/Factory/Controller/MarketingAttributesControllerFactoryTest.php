<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\MarketingAttributesControllerFactory;

/**
 * @author haniw
 */
class MarketingAttributesControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var MarketingAttributesControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new MarketingAttributesControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $formElementManager = $this->getMockBuilder('Zend\Form\FormElementManager')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $addAttributesMarketingForm = $this->getMockBuilder('PrismProductsManager\Form\AddAttributesMarketingGroupForm')
                                    ->disableOriginalConstructor()
                                    ->getMock();
        $formElementManager->expects($this->at(0))
                           ->method('get')
                           ->with('PrismProductsManager\Form\AddAttributesMarketingGroupForm')
                           ->willReturn($addAttributesMarketingForm);

        $addAttributesMarketingForm = $this->getMockBuilder('PrismProductsManager\Form\AddAttributesMarketingForm')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $formElementManager->expects($this->at(1))
                          ->method('get')
                          ->with('PrismProductsManager\Form\AddAttributesMarketingForm')
                          ->willReturn($addAttributesMarketingForm);

        $searchForm = $this->getMockBuilder('PrismProductsManager\Form\SearchForm')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $formElementManager->expects($this->at(2))
                        ->method('get')
                        ->with('PrismProductsManager\Form\SearchForm')
                        ->willReturn($searchForm);

        $this->serviceLocator->expects($this->at(0))
                             ->method('get')
                             ->with('FormElementManager')
                             ->willReturn($formElementManager);

        $simplePagination = $this->getMockBuilder('PrismCommon\Service\SimplePagination')
                  ->disableOriginalConstructor()
                  ->getMock();
        $this->serviceLocator->expects($this->at(1))
                    ->method('get')
                    ->with('SimplePagination')
                    ->willReturn($simplePagination);

        $attributesGroupMapper = $this->getMockBuilder('PrismProductsManager\Model\AttributesGroupMapper')
                  ->disableOriginalConstructor()
                  ->getMock();
        $this->serviceLocator->expects($this->at(2))
                    ->method('get')
                    ->with('PrismProductsManager\Model\AttributesGroupMapper')
                    ->willReturn($attributesGroupMapper);

        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $this->serviceLocator->expects($this->at(3))
                             ->method('get')
                             ->with('PrismProductsManager\Model\ProductsMapper')
                             ->willReturn($productsMapper);

        $queueService = $this->getMockBuilder('PrismQueueManager\Service\QueueService')
                           ->disableOriginalConstructor()
                           ->getMock();
        $this->serviceLocator->expects($this->at(4))
                       ->method('get')
                       ->with('QueueService')
                       ->willReturn($queueService);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\MarketingAttributesController', $result);
    }
}
