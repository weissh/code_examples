<?php

namespace PrismProductsManagerTest\Factory\Controller\DataTables;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Controller\DataTables\DataTablesController;
use PrismProductsManager\Factory\Controller\DataTables\DataTablesControllerFactory;
use PrismProductsManager\Model\ProductsMapper;
use Zend\Mvc\Controller\ControllerManager;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @author haniw
 */
class DataTablesControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var DataTablesControllerFactory */
    protected $factory;

    /** @var \Prophecy\Prophecy\ObjectProphecy */
    protected $serviceLocator;

    /** @var \Prophecy\Prophecy\ObjectProphecy */
    protected $controllerManager;

    protected function setUp()
    {
        $this->serviceLocator = $this->prophesize(ServiceLocatorInterface::class);
        $this->controllerManager = $this->prophesize(ControllerManager::class);

        $this->factory = new DataTablesControllerFactory();
    }

    public function testCreateService()
    {
        $config = [
            'db' => [
                'adapters' => [
                    'db-pms' => [
                        'dsn'      => 'mysql:dbname=lts_prism_pms;host=localhost',
                        'username' => 'root',
                        'password' => ''
                    ]
                ]
            ],
            'clientConfig' => [
                'clientCode' => 'lts',
                'features' => [
                    'editableSku' => false,
                    'productAddOn' => false,
                    'productTabs' => false,
                    'category_image_banner' => false,
                    'commonAttributes' => true,
                    'dynamicInputs' => true,
                    'menu' => [
                        'cms' => [
                            'main-display' => false,
                            'pages' => [
                                'main-display' => false,
                                'new-page' => false,
                                'manage-pages' => false
                            ],
                            'headers' => [
                                'main-display' => false,
                                'manage-headers' => false
                            ],
                            'navigation' => [
                                'main-display' => false,
                                'new-navigation' => false,
                                'manage-navigations' => false
                            ],
                            'template-manager' => [
                                'main-display' => false,
                                'new-template' => false,
                                'manage-templates' => false
                            ],
                            'carousel' => [
                                'main-display' => false,
                                'new-carousel' => false,
                                'manage-carousels' => false
                            ],
                            'popup-windows' => [
                                'main-display' => false,
                                'new-popup' => false,
                                'manage-popups' => false
                            ],
                            'tag-manager' => [
                                'main-display' => false,
                                'view-tag-manager' => false,
                                'data-layer' => false,
                                'new-data-element' => false,
                                'data-elements' => false,
                                'edit-data-elements' => false
                            ],
                            'emails' => [
                                'main-display' => false,
                                'new-emailer' => false,
                                'manage-emails' => false
                            ],
                            'blog' => [
                                'main-display' => false,
                                'blog-page' => false,
                                'admin-page' => false
                            ]
                        ],
                        'pms' => [
                            'main-display' => true,
                            'products' => [
                                'main-display' => true,
                                'manage-products' => true,
                                'new-product' => true,
                                'assign-fields-to-tabs' => true
                            ],
                            'product-categories' => [
                                'main-display' => false,
                                'manage-marketing-categories' => false,
                                'manage-merchant-categories' => false
                            ],
                            'product-attributes' => [
                                'main-display' => true,
                                'manage-merchant-attributes' => false,
                                'bulk-upload-merchant-attributes' => true,
                                'manage-marketing-attributes' => false,
                                'manage-core-attributes' => false,
                                'manage-common-attributes' => true,
                                'manage-colours' => true
                            ],
                            'tools' => [
                                'main-display' => true,
                                'bulk-actions' => false,
                                'bulk-upload' => true,
                                'tax' => false,
                                'barcode-upload' => true,
                                'bulk-currency-conversion' => false
                            ]
                        ],
                        'media' => [
                            'main-display' => false,
                            'media-server-management' => [
                                'main-display' => false,
                                'upload-file' => false,
                                'manage-media-library' => false,
                                'media-server-admin' => false
                            ]
                        ],
                        'promotions' => [
                            'main-display' => false,
                            'promotions-management' => [
                                'main-display' => false,
                                'new-promotion' => false,
                                'manage-promotions' => false
                            ]
                        ],
                        'admin' => [
                            'main-display' => true,
                            'user-management' => [
                                'main-display' => true,
                                'new-user' => true,
                                'manage-users' => true,
                                'create-permission' => true,
                                'manage-permission' => true
                            ],
                            'languages' => [
                                'main-display' => false,
                                'add-new-language' => false,
                                'new-language-from-current' => false,
                                'manage-languages' => false,
                                'manage-placeholders' => false,
                                'manage-translations' => false
                            ],
                            'search-manager' => [
                                'main-display' => false,
                                'search-priorities' => false
                            ],
                            'options' => [
                                'main-display' => false,
                                'settings' => false,
                                'event-log' => false,
                                'reporting' => false,
                                'logout' => false
                            ],
                            'reports' => [
                                'main-display' => false,
                                'products-comparison-report' => false,
                                'users-comparison-report' => false,
                                'product-report' => false
                            ],
                            'files' => [
                                'main-display' => false,
                                'upload-file' => false,
                                'list-files' => false
                            ]
                        ],
                        'logout' => [
                            'main-display' => true
                        ]
                    ],
                    'firstWorkflow' => true
                ],
                'settings' => [
                    'manualSync' => false,
                    'default_addBcc' => [
                        'hani.weiss@whistl.co.uk',
                        'vilius.vaitiekunas@whistl.co.uk'
                    ],
                    'carousel_slider' => 'bxSlider',
                    'commonAttributesForAdmin' => true, // Flag used to define if common attributes are manageable by administrators or all users
                    'showOptionsWhenSavingAtStyleLevel' => false,
                    'standardStylePrice' => 0,
                    'enableWorkflows' => true,
                    'automaticStyleGeneration' => true,
                    'skuGenerationSeparator' => '',
                    'checkReadyForWeb' => true,
                    'checkMasterStyle' => true
                ],
            ],
        ];

        $this->serviceLocator->get('Config')->willReturn($config)
                                            ->shouldBeCalled();

        $productsMapper = $this->prophesize(ProductsMapper::class);
        $this->serviceLocator->get(ProductsMapper::class)->willReturn($productsMapper)
                                                         ->shouldBeCalled();

        $this->controllerManager->getServiceLocator()->willReturn($this->serviceLocator)
                                                     ->shouldBeCalled();

        $this->assertInstanceOf(DataTablesController::class, $this->factory->createService($this->controllerManager->reveal()));
    }
}
