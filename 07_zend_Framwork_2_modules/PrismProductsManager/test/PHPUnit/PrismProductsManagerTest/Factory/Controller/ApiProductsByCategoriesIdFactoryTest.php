<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\ApiProductsByCategoriesIdFactory;

/**
 * @author haniw
 */
class ApiProductsByCategoriesIdFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiProductsByCategoriesIdFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ApiProductsByCategoriesIdFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $apiProductsMapper = $this->getMockBuilder('PrismProductsManager\Model\ApiProductsMapper')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $this->serviceLocator->expects($this->at(0))
                             ->method('get')
                             ->with('PrismProductsManager\Model\ApiProductsMapper')
                             ->willReturn($apiProductsMapper);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\ApiProductsByCategoriesIdController', $result);
    }
}
