<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\MerchantCategoriesControllerFactory;

/**
 * @author haniw
 */
class MerchantCategoriesControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var MerchantCategoriesControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new MerchantCategoriesControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                      ->disableOriginalConstructor()
                      ->getMock();
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('db-pms')
                        ->willReturn($dbPms);

        $formElementManager = $this->getMockBuilder('Zend\Form\FormElementManager')
                                   ->disableOriginalConstructor()
                                   ->getMock();

        $addMerchantCategoryForm = $this->getMockBuilder('PrismProductsManager\Form\AddMerchantCategoryForm')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $formElementManager->expects($this->at(0))
                          ->method('get')
                          ->with('PrismProductsManager\Form\AddMerchantCategoryForm')
                          ->willReturn($addMerchantCategoryForm);

        $searchForm = $this->getMockBuilder('PrismProductsManager\Form\SearchForm')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $formElementManager->expects($this->at(1))
                        ->method('get')
                        ->with('PrismProductsManager\Form\SearchForm')
                        ->willReturn($searchForm);

        $this->serviceLocator->expects($this->at(1))
                             ->method('get')
                             ->with('FormElementManager')
                             ->willReturn($formElementManager);

        $merchantCategoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\MerchantCategoriesMapper')
                ->disableOriginalConstructor()
               ->getMock();
        $this->serviceLocator->expects($this->at(2))
                 ->method('get')
                 ->with('PrismProductsManager\Model\MerchantCategoriesMapper')
                 ->willReturn($merchantCategoriesMapper);

        $simplePagination = $this->getMockBuilder('PrismCommon\Service\SimplePagination')
                  ->disableOriginalConstructor()
                  ->getMock();
        $this->serviceLocator->expects($this->at(3))
                    ->method('get')
                    ->with('SimplePagination')
                    ->willReturn($simplePagination);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\MerchantCategoriesController', $result);
    }
}
