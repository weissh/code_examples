<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\ColoursControllerFactory;

/**
 * @author haniw
 */
class ColoursControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ColoursControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ColoursControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $coloursMapper = $this->getMockBuilder('PrismProductsManager\Model\ColoursMapper')
                      ->disableOriginalConstructor()
                      ->getMock();
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('PrismProductsManager\Model\ColoursMapper')
                        ->willReturn($coloursMapper);

        $formElementManager = $this->getMockBuilder('Zend\Form\FormElementManager')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $coloursForm = $this->getMockBuilder('PrismProductsManager\Form\ColoursForm')
                                    ->disableOriginalConstructor()
                                    ->getMock();
        $formElementManager->expects($this->at(0))
                           ->method('get')
                           ->with('PrismProductsManager\Form\ColoursForm')
                           ->willReturn($coloursForm);

        $this->serviceLocator->expects($this->at(1))
                             ->method('get')
                             ->with('FormElementManager')
                             ->willReturn($formElementManager);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\ColoursController', $result);
    }
}
