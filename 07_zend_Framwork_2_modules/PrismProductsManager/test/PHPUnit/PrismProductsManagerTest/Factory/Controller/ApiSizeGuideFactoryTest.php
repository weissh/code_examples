<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\ApiSizeGuideFactory;

/**
 * @author haniw
 */
class ApiSizeGuideFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiSizeGuideFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ApiSizeGuideFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $attributesGroupMapper = $this->getMockBuilder('PrismProductsManager\Model\AttributesGroupMapper')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $this->serviceLocator->expects($this->at(0))
                             ->method('get')
                             ->with('PrismProductsManager\Model\AttributesGroupMapper')
                             ->willReturn($attributesGroupMapper);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\ApiSizeGuideController', $result);
    }
}
