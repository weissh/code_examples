<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\BulkActionsControllerFactory;

/**
 * @author haniw
 */
class BulkActionsControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var BulkActionsControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new BulkActionsControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                      ->disableOriginalConstructor()
                      ->getMock();
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('db-pms')
                        ->willReturn($dbPms);

        $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
                           ->disableOriginalConstructor()
                           ->getMock();

        $commonStorage->expects($this->once())
                   ->method('read')
                   ->with('siteConfig')
                   ->willReturn([]);
        $this->serviceLocator->expects($this->at(1))
                        ->method('get')
                        ->with('CommonStorage')
                        ->willReturn($commonStorage);

        $formElementManager = $this->getMockBuilder('Zend\Form\FormElementManager')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $bulkDownloadForm = $this->getMockBuilder('PrismProductsManager\Form\BulkDownloadForm')
                                    ->disableOriginalConstructor()
                                    ->getMock();
        $formElementManager->expects($this->at(0))
                           ->method('get')
                           ->with('PrismProductsManager\Form\BulkDownloadForm')
                           ->willReturn($bulkDownloadForm);

        $bulkUploadForm = $this->getMockBuilder('PrismProductsManager\Form\BulkUploadForm')
                                   ->disableOriginalConstructor()
                                   ->getMock();

        $formElementManager->expects($this->at(1))
                           ->method('get')
                           ->with('PrismProductsManager\Form\BulkUploadForm')
                           ->willReturn($bulkUploadForm);

        $this->serviceLocator->expects($this->at(2))
                       ->method('get')
                       ->with('FormElementManager')
                       ->willReturn($formElementManager);

        $bulkActionsMapper = $this->getMockBuilder('PrismProductsManager\Model\BulkActionsMapper')
                                  ->disableOriginalConstructor()
                                  ->getMock();
        $this->serviceLocator->expects($this->at(3))
                     ->method('get')
                     ->with('PrismProductsManager\Model\BulkActionsMapper')
                     ->willReturn($bulkActionsMapper);

        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                               ->disableOriginalConstructor()
                               ->getMock();
        $this->serviceLocator->expects($this->at(4))
                  ->method('get')
                  ->with('PrismProductsManager\Model\ProductsMapper')
                  ->willReturn($productsMapper);

        $queueService = $this->getMockBuilder('PrismQueueManager\Service\QueueService')
                             ->disableOriginalConstructor()
                             ->getMock();
        $this->serviceLocator->expects($this->at(5))
                ->method('get')
                ->with('QueueService')
                ->willReturn($queueService);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\BulkActionsController', $result);
    }
}
