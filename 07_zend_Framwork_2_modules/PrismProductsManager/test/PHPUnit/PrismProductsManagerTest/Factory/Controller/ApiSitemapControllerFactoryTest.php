<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\ApiSitemapControllerFactory;

/**
 * @author haniw
 */
class ApiSitemapControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiSitemapControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ApiSitemapControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $categoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\CategoriesMapper')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $this->serviceLocator->expects($this->at(0))
                             ->method('get')
                             ->with('PrismProductsManager\Model\CategoriesMapper')
                             ->willReturn($categoriesMapper);

        $pagesMapper = $this->getMockBuilder('PrismContentManager\Model\PagesMapper')
                              ->disableOriginalConstructor()
                              ->getMock();
        $this->serviceLocator->expects($this->at(1))
                          ->method('get')
                          ->with('PrismContentManager\Model\PagesMapper')
                          ->willReturn($pagesMapper);

        $config = [
           'siteConfig' => [
               'websites' => [
                   'website-url' => [
                       '1' => 'http://nauticalia-website.local/',
                       '2' => 'http://nauticalia-website.local/',
                       '3' => 'http://nauticalia-website.local/',
                   ],
               ],
               'sitemap' => [
                   'home' => '1',
                   'category' => '0.8',
                   'pages' => '0.6',
                   'product' => '1',
               ],
            ]
        ];
        $this->serviceLocator->expects($this->at(2))
                        ->method('get')
                        ->with('Config')
                        ->willReturn($config);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\ApiSitemapController', $result);
    }
}
