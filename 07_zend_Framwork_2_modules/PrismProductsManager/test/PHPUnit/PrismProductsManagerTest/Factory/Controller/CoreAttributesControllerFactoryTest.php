<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\CoreAttributesControllerFactory;

/**
 * @author haniw
 */
class CoreAttributesControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var CoreAttributesControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new CoreAttributesControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $attributesMapper = $this->getMockBuilder('PrismProductsManager\Model\AttributesGroupMapper')
                      ->disableOriginalConstructor()
                      ->getMock();
        $this->serviceLocator->expects($this->at(0))
                        ->method('get')
                        ->with('PrismProductsManager\Model\AttributesGroupMapper')
                        ->willReturn($attributesMapper);

        $formElementManager = $this->getMockBuilder('Zend\Form\FormElementManager')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $editCoreAttributesForm = $this->getMockBuilder('PrismProductsManager\Form\EditCoreAttributeGroupForm')
                                    ->disableOriginalConstructor()
                                    ->getMock();
        $formElementManager->expects($this->at(0))
                           ->method('get')
                           ->with('PrismProductsManager\Form\EditCoreAttributeGroupForm')
                           ->willReturn($editCoreAttributesForm);

        $searchForm = $this->getMockBuilder('PrismProductsManager\Form\SearchForm')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $formElementManager->expects($this->at(1))
                          ->method('get')
                          ->with('PrismProductsManager\Form\SearchForm')
                          ->willReturn($searchForm);

        $this->serviceLocator->expects($this->at(1))
                             ->method('get')
                             ->with('FormElementManager')
                             ->willReturn($formElementManager);

        $simplePagination = $this->getMockBuilder('PrismCommon\Service\SimplePagination')
                   ->disableOriginalConstructor()
                   ->getMock();
        $this->serviceLocator->expects($this->at(2))
                     ->method('get')
                     ->with('SimplePagination')
                     ->willReturn($simplePagination);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\CoreAttributesController', $result);
    }
}
