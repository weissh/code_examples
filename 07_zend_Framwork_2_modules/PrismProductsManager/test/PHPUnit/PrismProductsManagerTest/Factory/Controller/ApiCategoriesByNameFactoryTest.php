<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Factory\Controller\ApiCategoriesByNameFactory;

/**
 * @author haniw
 */
class ApiCategoriesByNameFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiCategoriesByNameFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ApiCategoriesByNameFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $apiCategoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\ApiCategoriesMapper')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $this->serviceLocator->expects($this->at(0))
                             ->method('get')
                             ->with('PrismProductsManager\Model\ApiCategoriesMapper')
                             ->willReturn($apiCategoriesMapper);

        $pagesMapper = $this->getMockBuilder('PrismContentManager\Model\PagesMapper')
                              ->disableOriginalConstructor()
                              ->getMock();
        $this->serviceLocator->expects($this->at(1))
                          ->method('get')
                          ->with('PrismContentManager\Model\PagesMapper')
                          ->willReturn($pagesMapper);

        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismProductsManager\Controller\ApiCategoriesByNameController', $result);
    }
}
