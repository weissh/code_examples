<?php

namespace PrismProductsManagerTest\Factory\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Controller\CommonAttributesController;
use PrismProductsManager\Factory\Controller\CommonAttributesControllerFactory;
use PrismProductsManager\Form;
use PrismProductsManager\Model;
use PrismProductsManager\Service;
use Zend\Db\Adapter\Adapter;
use Zend\Form\FormElementManager;
use Zend\Mvc\Controller\ControllerManager;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @author haniw
 */
class CommonAttributesControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var CommonAttributesControllerFactory */
    protected $factory;

    /** @var \Prophecy\Prophecy\ObjectProphecy */
    protected $serviceLocator;

    /** @var \Prophecy\Prophecy\ObjectProphecy */
    protected $controllerManager;

    protected function setUp()
    {
        $this->serviceLocator = $this->prophesize(ServiceLocatorInterface::class);
        $this->controllerManager = $this->prophesize(ControllerManager::class);

        $this->factory = new CommonAttributesControllerFactory();
    }

    public function testCreateService()
    {
        $simplePagination = $this->prophesize('PrismCommon\Service\SimplePagination');
        $this->serviceLocator->get('SimplePagination')->willReturn($simplePagination)
            ->shouldBeCalled();

        $config = [
            'clientConfig' => []
        ];

        $this->serviceLocator->get('config')->willReturn($config)
                                            ->shouldBeCalled();

        $dbPms = $this->prophesize(Adapter::class);
        $this->serviceLocator->get('db-pms')->willReturn($dbPms)
                                            ->shouldBeCalled();

        $commonAttributeMapper = $this->prophesize(Model\CommonAttributesMapper::class);
        $this->serviceLocator->get(Model\CommonAttributesMapper::class)->willReturn($commonAttributeMapper)
            ->shouldBeCalled();

        $productsMapper = $this->prophesize(Model\ProductsMapper::class);
        $this->serviceLocator->get(Model\ProductsMapper::class)->willReturn($productsMapper)
                                                         ->shouldBeCalled();

        $formElementManager = $this->prophesize(FormElementManager::class);
        $searchForm = $this->prophesize(Form\SearchForm::class);
        $formElementManager->get(Form\SearchForm::class)->willReturn($searchForm)
                                                  ->shouldBeCalled();
        $this->serviceLocator->get('FormElementManager')->willReturn($formElementManager)
                                                        ->shouldBeCalled();

        $commonAttributeForm = $this->prophesize(Form\CommonAttributesForm::class);
        $this->serviceLocator->get(Form\CommonAttributesForm::class)->willReturn($commonAttributeForm)
            ->shouldBeCalled();

        $commonAttributesValuesForm = $this->prophesize(Form\CommonAttributesValuesForm::class);
        $this->serviceLocator->get(Form\CommonAttributesValuesForm::class)->willReturn($commonAttributesValuesForm)
            ->shouldBeCalled();

        $territoryOverrideForm = $this->prophesize(Form\TerritoryOverrideForm::class);
        $this->serviceLocator->get(Form\TerritoryOverrideForm::class)->willReturn($territoryOverrideForm)
            ->shouldBeCalled();

        $commonAttributesService = $this->prophesize(Service\CommonAttributesService::class);
        $this->serviceLocator->get(Service\CommonAttributesService::class)->willReturn($commonAttributesService)
            ->shouldBeCalled();

        $viewRenderer = $this->prophesize('Zend\View\Renderer\PhpRenderer');
        $this->serviceLocator->get('viewrenderer')->willReturn($viewRenderer)
            ->shouldBeCalled();

        $assignFieldsToTabsService = $this->prophesize(Service\AssignFieldToTabsService::class);
        $this->serviceLocator->get(Service\AssignFieldToTabsService::class)->willReturn($assignFieldsToTabsService)
            ->shouldBeCalled();

        $this->controllerManager->getServiceLocator()->willReturn($this->serviceLocator)
                                                     ->shouldBeCalled();

        $this->assertInstanceOf(CommonAttributesController::class, $this->factory->createService($this->controllerManager->reveal()));
    }
}
