<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\UploadBarcodesForm;

/**
 * @author haniw
 */
class UploadBarcodesFormTest extends PHPUnit_Framework_TestCase
{
    /** @var UploadBarcodesForm */
    protected $form;

    public function setUp()
    {
        $this->form = new UploadBarcodesForm();
    }

    public function testInit()
    {
        $this->form->init();
    }

    public function testInstanceOfFormEventManagerAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }
}
