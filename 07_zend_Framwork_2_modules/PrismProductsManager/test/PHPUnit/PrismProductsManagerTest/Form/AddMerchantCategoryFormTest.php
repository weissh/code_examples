<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\AddMerchantCategoryForm;

/**
 * @author haniw
 */
class AddMerchantCategoryFormTest extends PHPUnit_Framework_TestCase
{
    /** @var \PrismProductsManager\Model\CategoriesMapper */
    private $categoriesMapper;

    /** @var \PrismProductsManager\Model\AttributesGroupMapper */
    private $attributesGroupMapper;

    /** @var \Zend\Db\Adapter\Adapter */
    private $dbPms;

    /** @var AddMerchantCategoryForm */
    private $form;

    public function setUp()
    {
        $this->categoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\CategoriesMapper')
            ->disableOriginalConstructor()
            ->getMock();
        $this->categoriesMapper->expects($this->any())
            ->method('fetchAllMerchantCategories')
            ->willReturn([]);

        $this->attributesGroupMapper = $this->getMockBuilder('PrismProductsManager\Model\AttributesGroupMapper')
            ->disableOriginalConstructor()
            ->getMock();
        $this->attributesGroupMapper->expects($this->any())
            ->method('fetchTopMerchantGroups')
            ->willReturn([]);

        $this->commonAttributesTable = $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesTable')
            ->disableOriginalConstructor()
            ->getMock();
        $this->commonAttributesTable->expects($this->any())
            ->method('getCommonAttributesDetailsBy')
            ->willReturn([]);
        $this->commonAttributesTable->expects($this->any())
            ->method('listResultsWithKeyAs')
            ->willReturn([]);

        $this->dbPms = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
            ->disableOriginalConstructor()
            ->getMock();

        $this->form = new AddMerchantCategoryForm(
            $this->categoriesMapper,
            $this->attributesGroupMapper,
            $this->dbPms,
            $this->commonAttributesTable
        );
    }

    public function testInit()
    {
        $this->form->init();
    }

    public function testInstanceOfFormEventManagerAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }

    public function provideValues()
    {
        return [
            [true],
            [false],
        ];
    }

    /**
     * @dataProvider provideValues
     */
    public function testGetInputFilterSpecificationWithUniqueNameSet($value)
    {
        $this->form->setUniqueName($value);
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }

    /**
     * @dataProvider provideValues
     */
    public function testGetInputFilterSpecificationWithSetParentFlag($value)
    {
        $this->form->parentFlag = $value;
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }
}
