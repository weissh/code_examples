<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\ProductsCreateCollationVariantsForm;

/**
 * @author haniw
 */
class ProductsCreateCollationVariantsFormTest extends PHPUnit_Framework_TestCase
{
    /** @var ColoursForm */
    protected $form;

    public function setUp()
    {
        $this->form = new ProductsCreateCollationVariantsForm();
    }

    public function testInit()
    {
        $this->form->init();
    }

    public function testInstanceOfFormEventManagerAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }
}
