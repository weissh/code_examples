<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\CommonAttributesForm;

/**
 * @author haniw
 */
class CommonAttributesFormTest extends PHPUnit_Framework_TestCase
{
    /** @var CommonAttributesForm */
    protected $form;

    public function setUp()
    {
        $commonAttributeService = $this->prophesize('PrismProductsManager\Service\CommonAttributesService');
        $commonAttributeService->getCommonAttributesDisplayType()
                               ->willReturn([])
                               ->shouldBeCalled();

        $this->form = new CommonAttributesForm($commonAttributeService->reveal());
    }

    public function testInit()
    {
        $this->form->init();
    }

    public function testInstanceOfFormAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }
}
