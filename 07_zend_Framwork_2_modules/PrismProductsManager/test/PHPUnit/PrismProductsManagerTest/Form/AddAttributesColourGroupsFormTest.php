<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\AddAttributesColourGroupsForm;

/**
 * @author haniw
 */
class AddAttributesColourGroupsFormTest extends PHPUnit_Framework_TestCase
{
    /** @var AddAttributesColourGroupsForm */
    protected $form;

    public function setUp()
    {
        $this->form = new AddAttributesColourGroupsForm();
    }

    public function testInit()
    {
        $this->form->init();
    }

    public function testInstanceOfFormEventManagerAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }

    public function testGetInputFilterSpecificationWithSubmittedHexData()
    {
        $this->form->setSubmitedData(['hex' => '#9781C3']);
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }

    public function testSetDbAdapter()
    {
        $this->form->setDbAdapter(
            $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                 ->disableOriginalConstructor()
                 ->getMock()
        );
    }
}
