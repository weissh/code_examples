<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\CurrencyConverterForm;

/**
 * @author haniw
 */
class CurrencyConverterFormTest extends PHPUnit_Framework_TestCase
{
    /** @var CurrencyConverterForm */
    protected $form;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceManager');
        $this->serviceLocator = $serviceLocator;

        $this->form = new CurrencyConverterForm();
    }

    public function testInit()
    {
        $this->form->setServiceManager($this->serviceLocator);
        $this->serviceLocator->expects($this->once())
                       ->method('get')
                       ->with('PrismProductsManager\Model\CurrencyMapper')
                       ->willReturn(
            $this->getMockBuilder('PrismProductsManager\Model\CurrencyMapper')
                 ->disableOriginalConstructor()
                 ->getMock()
        );
        $this->form->init();
    }

    public function testInstanceOfFormEventManagerAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\ServiceManager\ServiceManagerAwareInterface', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }

    public function testSetGetServiceManager()
    {
        $this->form->setServiceManager($this->serviceLocator);
        $this->assertInstanceOf('Zend\ServiceManager\ServiceManager', $this->form->getServiceManager());
    }
}
