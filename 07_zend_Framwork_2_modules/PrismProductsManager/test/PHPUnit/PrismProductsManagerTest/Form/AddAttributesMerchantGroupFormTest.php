<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\AddAttributesMerchantGroupForm;

/**
 * @author haniw
 */
class AddAttributesMerchantGroupFormTest extends PHPUnit_Framework_TestCase
{
    /** @var AddAttributesMarketingGroupForm */
    protected $form;

    public function setUp()
    {
        $this->form = new AddAttributesMerchantGroupForm();
    }

    public function testInit()
    {
        $this->form->init();
    }

    public function testInstanceOfFormEventManagerAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }

    public function testSetDbAdapter()
    {
        $this->form->setDbAdapter(
            $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                 ->disableOriginalConstructor()
                 ->getMock()
        );
    }

    public function provideAttributeTypeAndAttributeIds()
    {
        return [
            [false],
            [0],
            [1],
        ];
    }

    /**
     * @dataProvider provideAttributeTypeAndAttributeIds
     */
    public function testGetInputFilterSpecificationWithFormData($attributeGroupId)
    {
        $this->form->formData = ['attributeGroupId' => $attributeGroupId];
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }
}
