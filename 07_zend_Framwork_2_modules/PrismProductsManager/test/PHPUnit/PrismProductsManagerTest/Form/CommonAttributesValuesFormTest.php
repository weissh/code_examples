<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\CommonAttributesValuesForm;

/**
 * @author haniw
 */
class CommonAttributesValuesFormTest extends PHPUnit_Framework_TestCase
{
    /** @var CommonAttributesValuesForm */
    protected $form;

    public function setUp()
    {
        $adapter = $this->prophesize('Zend\Db\Adapter\Adapter');
        $this->form = new CommonAttributesValuesForm($adapter->reveal());
    }

    public function testInit()
    {
        $this->form->init();
    }

    public function testInstanceOfFormAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }

    public function testSetDbAdapter()
    {
        $adapter = $this->prophesize('Zend\Db\Adapter\Adapter');
        $this->form->setDbAdapter($adapter->reveal());
    }
}
