<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\ProductsActivationForm;

/**
 * @author haniw
 */
class ProductsActivationFormTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsActivationForm */
    protected $form;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                      ->disableOriginalConstructor()
                      ->getMock();

        $this->form = new ProductsActivationForm($this->serviceLocator, $productsMapper);
    }

    public function testInit()
    {
        $this->form->init();
    }

    public function testInstanceOfFormEventManagerAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\EventManager\EventManagerAwareInterface', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }

    public function testSetGetEventManager()
    {
        // call first when setEventManager didn't called yet
        $this->assertInstanceOf('Zend\EventManager\EventManagerInterface', $this->form->getEventManager());

        $eventManager = $this->getMock('Zend\EventManager\EventManagerInterface');
        $this->form->setEventManager($eventManager);

        $this->assertInstanceOf('Zend\EventManager\EventManagerInterface', $this->form->getEventManager());
    }
}
