<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\BulkProductActionForm;

/**
 * @author haniw
 */
class BulkProductActionFormTest extends PHPUnit_Framework_TestCase
{
    /** @var BulkProductActionForm */
    protected $form;

    public function setUp()
    {
        $this->form = new BulkProductActionForm();
    }

    public function testInit()
    {
        $this->form->init();
    }

    public function testInstanceOfFormEventManagerAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }
}
