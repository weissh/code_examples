<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\AddAttributesMarketingForm;

/**
 * @author haniw
 */
class AddAttributesMarketingFormTest extends PHPUnit_Framework_TestCase
{
    /** @var AddAttributesColourGroupsForm */
    protected $form;

    public function setUp()
    {
        $this->form = new AddAttributesMarketingForm(
            $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                 ->disableOriginalConstructor()
                 ->getMock()
        );
    }

    public function testInit()
    {
        $this->form->init();
    }

    public function testInstanceOfFormEventManagerAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }
}
