<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\TerritoryOverrideForm;
use PrismProductsManager\Service\CommonAttributesService;

/**
 * @author haniw
 */
class TerritoryOverrideFormTest extends PHPUnit_Framework_TestCase
{
    /** @var ColoursForm */
    protected $form;

    public function setUp()
    {
        $this->commonAttributesService = $this->prophesize(CommonAttributesService::class);
        $this->commonAttributesService->getAllTerritories(true)->willReturn([
            0 => [
                'territoryId' => 1,
                'iso2Code' => 'iso2Code'
            ]
        ])->shouldBeCalled();

        $this->form = new TerritoryOverrideForm($this->commonAttributesService->reveal());
    }

    public function testInit()
    {
        $this->form->init();
    }

    public function testInstanceOfFormEventManagerAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }
}
