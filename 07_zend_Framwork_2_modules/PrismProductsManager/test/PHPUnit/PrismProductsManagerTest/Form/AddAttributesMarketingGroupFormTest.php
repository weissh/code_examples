<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\AddAttributesMarketingGroupForm;

/**
 * @author haniw
 */
class AddAttributesMarketingGroupFormTest extends PHPUnit_Framework_TestCase
{
    /** @var AddAttributesMarketingGroupForm */
    protected $form;

    public function setUp()
    {
        $this->form = new AddAttributesMarketingGroupForm();
    }

    public function testInit()
    {
        $this->form->init();
    }

    public function testInstanceOfFormEventManagerAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }

    public function provideAttributeTypeAndAttributeIds()
    {
        return [
            ['TEXT', ''],
            ['TEXT', 1],
            ['CHECKBOX', ''],
            ['CHECKBOX', 1],
            ['OPTIONS', ''],
            ['OPTIONS', 1],
        ];
    }

    /**
     * @dataProvider provideAttributeTypeAndAttributeIds
     */
    public function testGetInputFilterSpecificationWithFormData($attributeType, $attributeId)
    {
        $this->form->formData = ['attributeType' => $attributeType, 'attributeId' => $attributeId];
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }
}
