<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\AddCategoryForm;

/**
 * @author haniw
 */
class AddCategoryFormTest extends PHPUnit_Framework_TestCase
{
    /** @var SearchForm */
    protected $form;

    public function setUp()
    {
        $this->form = new AddCategoryForm();
    }

    public function testInit()
    {
        $this->form->init();
    }

    public function testInstanceOfFormEventManagerAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }

    public function provideDataAndFlags()
    {
        return [
            [
                ['categoryId' => 1], false, false,
            ],
            [
                ['categoryId' => 1], true, false,
            ],
            [
                ['categoryId' => 1], true, true,
            ],
            [
                [], false, false,
            ],
            [
                [], true, false,
            ],
            [
                [], true, true,
            ],
        ];
    }

    /**
     * @dataProvider provideDataAndFlags
     */
    public function testGetInputFilterSpecificationWithVariousSubmmitedData($submittedData, $duplicateFlag, $validateCategoryNameFlag)
    {
        $this->form->setSubmittedData($submittedData, $duplicateFlag);
        $this->form->setValidateCategoryNameFlag($validateCategoryNameFlag);
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }

    public function testSetGetDbAdapter()
    {
        $this->form->setDbAdapter(
            $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                 ->disableOriginalConstructor()
                 ->getMock()
        );

        $this->assertInstanceOf('Zend\Db\Adapter\Adapter', $this->form->getDbAdapter());
    }

    public function provideFlags()
    {
        return [
            [true],
            [false],
        ];
    }

    /**
     * @dataProvider provideFlags
     */
    public function testSetValidateCategoryNameFlag($flag)
    {
        $this->form->setValidateCategoryNameFlag($flag);
    }

    /**
     * @dataProvider provideFlags
     */
    public function testSetDuplicateFlag($flag)
    {
        $this->form->setDuplicateFlag($flag);
    }

    /**
     * @dataProvider provideFlags
     */
    public function testSetDuplicateUrlFlag($flag)
    {
        $this->form->setDuplicateUrlFlag($flag);
    }

    /**
     * @dataProvider provideFlags
     */
    public function testSetSubmittedData($flag)
    {
        $this->form->setSubmittedData([], $flag);
    }
}
