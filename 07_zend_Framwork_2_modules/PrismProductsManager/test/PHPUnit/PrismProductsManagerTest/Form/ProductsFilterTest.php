<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\ProductsFilter;

/**
 * @author haniw
 */
class ProductsFilterTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsFilter */
    protected $filter;

    public function setUp()
    {
        $this->filter = new ProductsFilter();
    }

    public function testInstanceOfFormEventManagerAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->filter);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->filter->getInputFilterSpecification());
    }
}
