<?php

namespace PrismProductsManagerTest\Form;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Form\AddAttributesExistingColoursGroupForm;

/**
 * @author haniw
 */
class AddAttributesExistingColoursGroupFormTest extends PHPUnit_Framework_TestCase
{
    /** @var SearchForm */
    protected $form;

    public function setUp()
    {
        $this->form = new AddAttributesExistingColoursGroupForm();
    }

    public function testInit()
    {
        $this->form->init();
    }

    public function testInstanceOfFormEventManagerAndInputFilterProvider()
    {
        $this->assertInstanceOf('Zend\Form\Form', $this->form);
        $this->assertInstanceOf('Zend\InputFilter\InputFilterProviderInterface', $this->form);
    }

    public function testGetInputFilterSpecification()
    {
        $this->assertInternalType('array', $this->form->getInputFilterSpecification());
    }

    public function testSetDbAdapter()
    {
        $this->form->setDbAdapter(
            $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                 ->disableOriginalConstructor()
                 ->getMock()
        );
    }
}
