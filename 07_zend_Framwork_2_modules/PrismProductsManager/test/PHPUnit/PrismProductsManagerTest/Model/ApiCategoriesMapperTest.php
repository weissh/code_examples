<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ApiCategoriesMapper;
use PrismProductsManagerTest\ZendDbFixture\FixtureManager;

class ApiCategoriesMapperTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiCategoriesMapper */
    private $model;

    /** @var \PrismProductsmanagerTest\ZendDbFixture\FixtureManager */
    private $fixtureManager;

    private function init()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');

        $this->fixtureManager->execute('CATEGORIES_Definitions-up.sql');
        $this->fixtureManager->execute('CATEGORIES-up.sql');
        $this->fixtureManager->execute('CATEGORIES_Tags-up.sql');
        $this->fixtureManager->execute('CATEGORIES_R_CLIENTS_Websites-up.sql');
        $this->fixtureManager->execute('CATEGORIES_Media-up.sql');
    }

    protected function setUp()
    {
        $this->init();
        $this->imageMapper = $this->getMockBuilder('PrismMediaManager\Model\ImageMapper')
            ->disableOriginalConstructor()
            ->getMock();

        $this->model = new ApiCategoriesMapper(
            $this->fixtureManager->getDbAdapter(),
            $this->imageMapper
        );
    }

    public function testGetCategoryById()
    {
        $this->assertInternalType('array', $this->model->getCategoryById(1, 1, 1));
    }

    public function testGetCategoryByName()
    {
        $this->assertInternalType('array', $this->model->getCategoryByName('home', 1, 1));
    }

    protected function tearDown()
    {
        $this->fixtureManager->execute('CATEGORIES_R_CLIENTS_Websites-down.sql');
        $this->fixtureManager->execute('CATEGORIES_Tags-down.sql');
        $this->fixtureManager->execute('CATEGORIES-down.sql');
        $this->fixtureManager->execute('CATEGORIES_Definitions-down.sql');
        $this->fixtureManager->execute('CATEGORIES_Media-down.sql');
    }
}
