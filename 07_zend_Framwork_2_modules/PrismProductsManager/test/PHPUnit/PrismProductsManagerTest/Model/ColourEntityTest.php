<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ColourEntity;
use Zend\Db\Sql\Expression;

/**
 * @author haniw
 */
class ColourEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ColourEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ColourEntity();
    }

    public function testSetGetModified()
    {
        $this->model->setModified(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());
    }

    public function testSetGetCode()
    {
        $this->model->setCode('XyZ');
        $this->assertEquals('XyZ', $this->model->getCode());
    }

    public function testSetGetColourId()
    {
        $this->model->setColourId(1);
        $this->assertEquals(1, $this->model->getColourId());
    }

    public function testSetGetEnabled()
    {
        $this->model->setEnabled(1);
        $this->assertEquals(1, $this->model->getEnabled());
    }

    public function testSetGetHex()
    {
        $this->model->setHex('#5F0090');
        $this->assertEquals('#5F0090', $this->model->getHex());
    }
}
