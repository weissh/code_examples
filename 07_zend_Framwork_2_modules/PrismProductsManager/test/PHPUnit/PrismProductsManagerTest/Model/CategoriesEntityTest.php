<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\CategoriesEntity;

/**
 * @author haniw
 */
class CategoriesEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var CategoriesEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new CategoriesEntity();
    }
    public function testSetGetStatus()
    {
        // default from constructor
        $this->assertEquals('INACTIVE', $this->model->getStatus());

        $this->model->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->model->getStatus());
    }

    public function testSetGetCreated()
    {
        $created = date('Y-m-d H:i:s');
        $this->model->setCreated($created);
        $this->assertEquals($created, $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        $modified = date('Y-m-d H:i:s');
        $this->model->setModified($modified);
        $this->assertEquals($modified, $this->model->getModified());
    }

    public function testSetGetCategoryId()
    {
        $this->model->setCategoryId(1);
        $this->assertEquals(1, $this->model->getCategoryId());
    }

    public function testSetGetPosition()
    {
        $this->model->setPosition(1);
        $this->assertEquals(1, $this->model->getPosition());
    }

    public function testSetGetCategoryParentId()
    {
        $this->model->setCategoryParentId(1);
        $this->assertEquals(1, $this->model->getCategoryParentId());
    }

    public function testSetGetTreeDepth()
    {
        $this->model->setTreeDepth('[]');
        $this->assertEquals('[]', $this->model->getTreeDepth());
    }
}
