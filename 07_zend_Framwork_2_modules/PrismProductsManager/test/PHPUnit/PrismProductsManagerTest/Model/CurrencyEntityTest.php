<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\CurrencyEntity;

/**
 * @author haniw
 */
class CurrencyEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var CurrencyEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new CurrencyEntity();
    }

    public function testSetGetName()
    {
        $this->model->setName('foo');
        $this->assertEquals('foo', $this->model->getName());
    }

    public function testSetGetStatus()
    {
        $this->model->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->model->getStatus());
    }

    public function testSetGetCurrencyId()
    {
        $this->model->setCurrencyId(1);
        $this->assertEquals(1, $this->model->getCurrencyId());
    }

    public function testSetGetIsDefault()
    {
        $this->model->setIsDefault(true);
        $this->assertEquals(true, $this->model->getIsDefault());
    }

    public function testSetGetIsoCode()
    {
        $this->model->setIsoCode(true);
        $this->assertEquals(true, $this->model->getIsoCode());
    }

    public function testSetGetHtmlCode()
    {
        $this->model->setHtmlCode('<br/>');
        $this->assertEquals('<br/>', $this->model->getHtmlCode());
    }

    public function testSetGetBlank()
    {
        $this->model->setBlank(true);
        $this->assertEquals(true, $this->model->getBlank());
    }

    public function testSetGetFormat()
    {
        $this->model->setFormat('2,".","."');
        $this->assertEquals('2,".","."', $this->model->getFormat());
    }

    public function testSetGetDecimals()
    {
        $this->model->setDecimals(2);
        $this->assertEquals(2, $this->model->getDecimals());
    }

    public function testSetGetConversionRate()
    {
        $this->model->setConversionRate(0.5);
        $this->assertEquals(0.5, $this->model->getConversionRate());
    }
}
