<?php

namespace PrismProductsManagerTest\Model;

use PHPExcel;
use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity;
use PrismProductsManager\Model as PrismProductsManagerModel;
use PrismProductsManagerTest\ZendDbFixture\FixtureManager;
use PrismQueueManager\Service as PrismQueueManagerService;
use PrismProductsManager\Service as PrismProductsManagerService;
use ReflectionMethod;
use ReflectionProperty;

class BulkActionsMapperTest extends PHPUnit_Framework_TestCase
{
    /** @var PrismProductsManagerModel\BulkActionsMapper */
    private $model;

    /** @var \PrismProductsmanagerTest\ZendDbFixture\FixtureManager */
    private $fixtureManager;

    private function init()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');
    }
    
    protected function setUp()
    {
        $this->init();
        
        $this->dbAdapter = $this->fixtureManager->getDbAdapter();
        $this->languages = [
            'current-language' => 1,
            'site-languages' => [1],
        ];
        $this->phpExcel = $this->prophesize(PHPExcel::class);
        $this->siteConfig = [
            'languages' => [
                'current-language' => 1,
                'site-languages' => [1],
            ],
        ];
        $this->productsMapperPrices = $this->prophesize(PrismProductsManagerModel\ProductsMapperPrices::class);
        $this->queueService = $this->prophesize(PrismQueueManagerService\QueueService::class);
        $this->commonAttributesService = $this->prophesize(PrismProductsManagerService\CommonAttributesService::class);
        $this->productsMapper = $this->prophesize(PrismProductsManagerModel\ProductsMapper::class);
        $this->scheduleService = $this->prophesize(PrismProductsManagerService\ScheduleService::class);
        
        $this->model = new PrismProductsManagerModel\BulkActionsMapper(
            $this->dbAdapter,
            $this->languages,
            $this->phpExcel->reveal(),
            $this->siteConfig,
            $this->productsMapperPrices->reveal(),
            $this->queueService->reveal(),
            $this->commonAttributesService->reveal(),
            $this->productsMapper->reveal(),
            $this->scheduleService->reveal()
        );
    }
    
    public function provideValidatePricesHeading()
    {
        return [
            [
                [
                    'Style',
                ],
                'Price_cascade',
                [
                    'error' => false,
                    'message' => [],
                ]
            ],
            [
                [
                    'Style',
                ],
                'Price',
                [
                    'error' => true,
                    'message' => [
                        0 => 'Header Style is not match',
                    ],
                ]
            ],
            [
                [
                    'Option (Style + Color)',
                ],
                'Price',
                [
                    'error' => false,
                    'message' => [],
                ]
            ],
            [
                [
                    'Option (Style + Color)',
                ],
                'Price_cascade',
                [
                    'error' => true,
                    'message' => [
                        0 =>  'Header Option (Style + Color) is not match',
                    ],
                ]
            ]
        ];
    }
    
    /**
     * @dataProvider provideValidatePricesHeading
     * @param  [type]                    $fileHeading
     * @param  [type]                    $uploadType
     */
    public function testvalidatePricesHeading($fileHeading, $uploadType, $return)
    {
        $result = $this->model->validatePricesHeading($fileHeading, $uploadType);
        $this->assertSame($return, $result);
    }
    
    public function provideValidateCommonAttributesHeading()
    {
        return [
            [
                [
                    'Style',
                ],
                'Common_Attributes_Cascade',
                [
                    'error' => false,
                    'message' => [],
                ]
            ],
            [
                [
                    'Style',
                ],
                'All_Common_Attributes',
                [
                    'error' => true,
                    'message' => [
                        0 => 'Header Style is not match',
                    ],
                ]
            ],
            [
                [
                    'Option (Style + Color)',
                ],
                'All_Common_Attributes',
                [
                    'error' => false,
                    'message' => [],
                ]
            ],
            [
                [
                    'Foo',
                ],
                'Common_Attributes_Cascade',
                [
                    'error' => true,
                    'message' => [
                        0 =>  'Header Foo is not match',
                    ],
                ]
            ]
        ];
    }
    
    /**
     * @dataProvider provideValidateCommonAttributesHeading
     * @param  [type]                    $fileHeading
     * @param  [type]                    $uploadType
     */
    public function testvalidateCommonAttributesHeading($fileHeading, $uploadType, $return)
    {
        $commonAttributesDefinitions = new Entity\CommonAttributesDefinitions();
        if ($uploadType === 'All_Common_Attributes') {
            
            $commonAttributesDefinitions->setDescription('test');
            
            $header = new Entity\CommonAttributes();
            $header->setIssize(false);
            $header->setCommonAttributeDefinitions($commonAttributesDefinitions);
            
            $commonAttributes = [
                0 => $header
            ];
            $this->commonAttributesService->getAllCommonAttributes()
                                           ->willReturn($commonAttributes)
                                           ->shouldBeCalled();                    
        } else {
            $commonAttributesDefinitions->setDescription('test');
            
            $header = new Entity\CommonAttributes();
            $header->setIssize(false);
            $header->setCommonAttributeDefinitions($commonAttributesDefinitions);
            
            $commonAttributes = [
                0 => $header
            ];
            
            $this->commonAttributesService->getCascadeCommonAttributes()
                                           ->willReturn($commonAttributes)
                                           ->shouldBeCalled();                                
        }
        
        $result = $this->model->validateCommonAttributesHeading($fileHeading, $uploadType);
        $this->assertSame($return, $result);
    }
    
    public function provideSetProductsFrom()
    {
        return [
            ['STYLE', 'PRODUCTS'],
            ['OPTION', 'PRODUCTS_R_ATTRIBUTES'],
            ['Foo', null],
        ];
    }
    
    /**
     * @dataProvider provideSetProductsFrom
     * @method testsetProductFrom
     * @param  boolean            $isValidStyleOrVariant
     */
    public function testsetProductFrom($isValidStyleOrVariant, $return)
    {
        $r = new ReflectionMethod($this->model, 'setProductFrom');
        $r->setAccessible(true);
        
        $this->assertSame($return, $r->invokeArgs($this->model, [$isValidStyleOrVariant]));
    }
    
    public function testsetBulkProcessingId()
    {
        $r = new ReflectionMethod($this->model, 'setBulkProcessingId');
        $r->setAccessible(true);
        $r->invokeArgs($this->model, [1]);
        
        $rp = new ReflectionProperty($this->model, 'bulkProcessingId');
        $rp->setAccessible(true);
        $this->assertSame(1, $rp->getValue($this->model));
    }

    public function testIsAssoc()
    {
        $this->assertFalse($this->model->is_assoc([]));
    }
    
    public function testIsReadyForWeb()
    {
        $r = new ReflectionMethod($this->model, 'isReadyForWeb');
        $r->setAccessible(true);
        
        $this->assertTrue($r->invokeArgs($this->model, ['anything']));
    }
    
    public function provideGetFileHeading()
    {
        return [
            [
                [
                    'rows' => [
                        0 => [
                            0 => 'Style',
                        ],
                    ]
                ],
                ['Style']
            ],
            [
                [
                    'rows' => [
                        0 => [
                            0 => 'Style',
                            1 => '',
                        ],
                    ]
                ],
                ['Style']
            ]
        ];
    }
    
    /**
     * @dataProvider provideGetFileHeading
     * @method testgetFileHeading
     * @param  array             $uploadData
     */
    public function testgetFileHeading($uploadData, $return)
    {
        $r = new ReflectionMethod($this->model, 'getFileHeading');
        $r->setAccessible(true);
        
        $this->assertSame($return, $r->invokeArgs($this->model, [$uploadData]));
    }
    
    public function provideGetPocessType()
    {
        return [
            ['All_Common_Attributes', 'COMMON_ALL'],
            ['Common_Attributes_Cascade', 'COMMON_CASCADE'],
            ['Price', 'PRICE_ALL'],
            ['Price_cascade', 'PRICE_CASCADE'],
        ];
    }
    
    /**
     * @dataProvider provideGetPocessType
     * @method testGetPocessType
     * @param string   $uploadType
     * @param string   $return
     * @return [type]
     */
    public function testGetPocessType($uploadType, $return)
    {
        $this->assertSame($return, $this->model->getPocessType($uploadType));
    }

    protected function tearDown()
    {
    }
}
