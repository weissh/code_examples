<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsAttributesCombinationEntity;
use Zend\Db\Sql\Expression;

/**
 * @author haniw
 */
class ProductsAttributesCombinationEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsAttributesCombinationEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductsAttributesCombinationEntity();
    }

    public function testSetGetStatus()
    {
        $this->assertEquals('ACTIVE', $this->model->getStatus());

        $this->model->setStatus('INACTIVE');
        $this->assertEquals('INACTIVE', $this->model->getStatus());
    }

    public function testSetGetAttributeValue()
    {
        $this->model->setAttributeValue('foo');
        $this->assertEquals('foo', $this->model->getAttributeValue());
    }

    public function testSetGetAttributeGroupId()
    {
        $this->model->setAttributeGroupId(1);
        $this->assertEquals(1, $this->model->getAttributeGroupId());
    }

    public function testSetGetProductAttributeCombinationId()
    {
        $this->model->setProductAttributeCombinationId(1);
        $this->assertEquals(1, $this->model->getProductAttributeCombinationId());
    }

    public function testSetGetIsVariant()
    {
        $this->model->setIsVariant(true);
        $this->assertEquals(true, $this->model->getIsVariant());
    }

    public function testSetGetProductOrVariantId()
    {
        $this->model->setProductOrVariantId(1);
        $this->assertEquals(1, $this->model->getProductOrVariantId());
    }

    public function testSetGetCreated()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());

        $this->model->setCreated(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());

        $this->model->setModified(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());
    }
}
