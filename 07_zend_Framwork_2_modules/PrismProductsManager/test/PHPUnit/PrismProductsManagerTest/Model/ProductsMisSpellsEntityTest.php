<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsMisSpellsEntity;

/**
 * @author haniw
 */
class ProductsMisSpellsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsMisSpellsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductsMisSpellsEntity();
    }

    public function testSetGetProductId()
    {
        $this->model->setProductId(1);
        $this->assertEquals(1, $this->model->getProductId());
    }

    public function testSetGetLanguageId()
    {
        $this->model->setLanguageId(1);
        $this->assertEquals(1, $this->model->getLanguageId());
    }

    public function testSetGetProductMisSpellId()
    {
        $this->model->setProductMisSpellId(1);
        $this->assertEquals(1, $this->model->getProductMisSpellId());
    }

    public function testSetGetMisSpell()
    {
        $this->model->setMisSpell(1);
        $this->assertEquals(1, $this->model->getMisSpell());
    }

    public function testSetGetCreated()
    {
        $created = date('Y-m-d H:i:s');
        $this->model->setCreated($created);
        $this->assertEquals($created, $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        $modified = date('Y-m-d H:i:s');
        $this->model->setModified($modified);
        $this->assertEquals($modified, $this->model->getModified());
    }
}
