<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\CategoriesDefinitionsEntity;

/**
 * @author haniw
 */
class CategoriesDefinitionsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var CategoriesDefinitionsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new CategoriesDefinitionsEntity();
    }

    public function testSetGetModified()
    {
        $this->model->setModified('2015-01-01 00:00:00');
        $this->assertEquals('2015-01-01 00:00:00', $this->model->getModified());
    }

    public function testSetGetCreated()
    {
        $this->model->setCreated('2015-01-01 00:00:00');
        $this->assertEquals('2015-01-01 00:00:00', $this->model->getCreated());
    }

    public function testSetGetCategoryId()
    {
        $this->model->setCategoryId(1);
        $this->assertEquals(1, $this->model->getCategoryId());
    }

    public function testSetGetLanguageId()
    {
        $this->model->setLanguageId(1);
        $this->assertEquals(1, $this->model->getLanguageId());
    }

    public function testSetGetVersionId()
    {
        $this->model->setVersionId(1);
        $this->assertEquals(1, $this->model->getVersionId());
    }

    public function testSetGetVersionDescription()
    {
        $this->model->setVersionDescription('foo');
        $this->assertEquals('foo', $this->model->getVersionDescription());
    }

    public function testSetGetCategoryName()
    {
        $this->model->setCategoryName('foo');
        $this->assertEquals('foo', $this->model->getCategoryName());
    }


    public function testSetGetUrlName()
    {
        $this->model->setUrlName('foo');
        $this->assertEquals('foo', $this->model->getUrlName());
    }

    public function testSetGetMetaTitle()
    {
        $this->model->setMetaTitle('foo');
        $this->assertEquals('foo', $this->model->getMetaTitle());
    }

    public function testSetGetMetaDescription()
    {
        $this->model->setMetaDescription('foo');
        $this->assertEquals('foo', $this->model->getMetaDescription());
    }

    public function testSetGetShortDescription()
    {
        $this->model->setShortDescription('foo');
        $this->assertEquals('foo', $this->model->getShortDescription());
    }

    public function testSetGetLongDescription()
    {
        $this->model->setLongDescription('foo');
        $this->assertEquals('foo', $this->model->getLongDescription());
    }

    public function testSetGetmetaKeyword()
    {
        $this->model->setmetaKeyword('foo');
        $this->assertEquals('foo', $this->model->getmetaKeyword());
    }

    public function testSetGetStatus()
    {
        $this->model->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->model->getStatus());
    }

    public function testSetGetCategoryDefinitionsId()
    {
        $this->model->setCategoryDefinitionsId(1);
        $this->assertEquals(1, $this->model->getCategoryDefinitionsId());
    }

    public function testSetGetMakeLive()
    {
        $this->model->setMakeLive('foo');
        $this->assertEquals('foo', $this->model->getMakeLive());
    }
}
