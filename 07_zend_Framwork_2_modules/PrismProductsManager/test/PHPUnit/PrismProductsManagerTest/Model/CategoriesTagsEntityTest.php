<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\CategoriesTagsEntity;

/**
 * @author haniw
 */
class CategoriesTagsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var CategoriesTagsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new CategoriesTagsEntity();
    }

    public function testSetGetAlias()
    {
        $this->model->setAlias('foo');
        $this->assertEquals('foo', $this->model->getAlias());
    }

    public function testSetGetStatus()
    {
        $this->model->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->model->getStatus());
    }

    public function testSetGetModified()
    {
        $this->model->setModified('2015-01-01 00:00:00');
        $this->assertEquals('2015-01-01 00:00:00', $this->model->getModified());
    }

    public function testSetGetCreated()
    {
        $this->model->setCreated('2015-01-01 00:00:00');
        $this->assertEquals('2015-01-01 00:00:00', $this->model->getCreated());
    }

    public function testSetGetCategoriesDefinitionsId()
    {
        $this->model->setCategoriesDefinitionsId(1);
        $this->assertEquals(1, $this->model->getCategoriesDefinitionsId());
    }

    public function testSetGetCategoriesTagId()
    {
        $this->model->setCategoriesTagId(1);
        $this->assertEquals(1, $this->model->getCategoriesTagId());
    }


}
