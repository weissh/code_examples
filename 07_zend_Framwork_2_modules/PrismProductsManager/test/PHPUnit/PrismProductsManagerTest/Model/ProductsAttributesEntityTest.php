<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsAttributesEntity;
use Zend\Db\Sql\Expression;

/**
 * @author haniw
 */
class ProductsAttributesEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsAttributesEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductsAttributesEntity();
    }

    public function testSetGetProductId()
    {
        $this->model->setProductId(1);
        $this->assertEquals(1, $this->model->getProductId());
    }

    public function testSetGetStatus()
    {
        // default from constructor
        $this->assertEquals('INACTIVE', $this->model->getStatus());

        $this->model->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->model->getStatus());
    }

    public function testSetGetCreated()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());

        $this->model->setCreated(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());

        $this->model->setModified(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());
    }

    public function testSetGetEcoTax()
    {
        $this->model->setEcoTax('0.00');
        $this->assertEquals('0.00', $this->model->getEcoTax());
    }

    public function testSetGetSku()
    {
        $this->model->setSku('ALESDEBRW060');
        $this->assertEquals('ALESDEBRW060', $this->model->getSku());
    }

    public function testSetGetEan13()
    {
        $this->model->setEan13('5055347347580');
        $this->assertEquals('5055347347580', $this->model->getEan13());
    }

    public function testSetGetsupplierreference()
    {
        $this->model->setsupplierreference('BER01');
        $this->assertEquals('BER01', $this->model->getsupplierreference());
    }

    public function testSetGetIndexed()
    {
        $this->model->setIndexed(1);
        $this->assertEquals(1, $this->model->getIndexed());
    }

    public function testSetGetIsDefaultProduct()
    {
        $this->model->setIsDefaultProduct(1);
        $this->assertEquals(1, $this->model->getIsDefaultProduct());
    }

    public function testSetGetProductAttributeId()
    {
        $this->model->setProductAttributeId(1);
        $this->assertEquals(1, $this->model->getProductAttributeId());
    }

    public function testSetGetManufacturedId()
    {
        $this->model->setManufacturerId(1);
        $this->assertEquals(1, $this->model->getManufacturerId());
    }
}
