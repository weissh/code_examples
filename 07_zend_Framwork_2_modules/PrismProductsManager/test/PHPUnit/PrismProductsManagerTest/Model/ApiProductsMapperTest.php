<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ApiProductsMapper;
use PrismProductsManagerTest\ZendDbFixture\FixtureManager;

class ApiProductsMapperTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiProductsMapper */
    private $model;

    /** @var \PrismProductsmanagerTest\ZendDbFixture\FixtureManager */
    private $fixtureManager;

    private function init()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');

        $this->fixtureManager->execute('PRODUCTS-up.sql');
        $this->fixtureManager->execute('PRODUCTS_Definitions-up.sql');
        $this->fixtureManager->execute('PRODUCTS_R_CATEGORIES-up.sql');
        $this->fixtureManager->execute('PRODUCTS_R_SPECTRUM-up.sql');
        $this->fixtureManager->execute('CATEGORIES_Definitions-up.sql');
        $this->fixtureManager->execute('CATEGORIES_R_CLIENTS_Websites-up.sql');
        $this->fixtureManager->execute('PRODUCTS_Types-up.sql');
        $this->fixtureManager->execute('PRODUCTS_Types_Definitions-up.sql');
        $this->fixtureManager->execute('PRODUCTS_R_CLIENTS_Websites-up.sql');

    }

    protected function setUp()
    {
        $this->init();
        $this->productsMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                                     ->disableOriginalConstructor()
                                     ->getMock();

        $this->model = new ApiProductsMapper(
            $this->fixtureManager->getDbAdapter(),
            $this->productsMapper
        );
    }

    public function provideLimitAndPages()
    {
        return [
            [null, null],
            [1, null],
            [null, 1],
            [1, 1],
        ];
    }

    /**
     * @dataProvider provideLimitAndPages
     */
    public function testGetProductsByCategoryId($limit, $page)
    {
        $this->assertInternalType('array', $this->model->getProductsByCategoryId(1, 1, 1, $limit, $page));
    }

    /**
     * @dataProvider provideLimitAndPages
     */
    public function testGetProductsByCategoryName($limit, $page)
    {
        $this->assertInternalType('array', $this->model->getProductsByCategoryName(1, 1, 1, $limit, $page));
    }

    public function testGetVoucherProduct()
    {
        $this->productsMapper->expects($this->once())
                             ->method('getVoucherTypeId')
                             ->with(1)
                             ->willReturn(['productTypeId' => 1]);
        $this->model->getVoucherProduct(1, 1);
    }

    public function testGetVoucherProductBySpectrumId()
    {
        $this->model->getVoucherProductBySpectrumId(1);
    }

    protected function tearDown()
    {
        $this->fixtureManager->execute('PRODUCTS_R_CLIENTS_Websites-down.sql');
        $this->fixtureManager->execute('PRODUCTS_Types_Definitions-down.sql');
        $this->fixtureManager->execute('PRODUCTS_Types-down.sql');
        $this->fixtureManager->execute('CATEGORIES_R_CLIENTS_Websites-down.sql');
        $this->fixtureManager->execute('CATEGORIES_Definitions-down.sql');
        $this->fixtureManager->execute('PRODUCTS_R_SPECTRUM-down.sql');
        $this->fixtureManager->execute('PRODUCTS_R_CATEGORIES-down.sql');
        $this->fixtureManager->execute('PRODUCTS_Definitions-down.sql');
        $this->fixtureManager->execute('PRODUCTS-down.sql');
    }
}
