<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\MerchantCategoriesMapper;
use PrismProductsManager\Model\MerchantCategoriesDefinitionsEntity;
use PrismProductsManager\Model\MerchantCategoriesEntity;
use PrismProductsManager\Model\SkuRulesEntity;
use PrismProductsManagerTest\ZendDbFixture\FixtureManager;

class MerchantCategoriesMapperTest extends PHPUnit_Framework_TestCase
{
    /** @var TaxMapper */
    private $model;

    /** @var \PrismProductsmanagerTest\ZendDbFixture\FixtureManager */
    private $fixtureManager;

    private function init()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');

        $this->fixtureManager->execute('PRODUCTS_Merchant_Categories-up.sql');
        $this->fixtureManager->execute('PRODUCTS_Merchant_Categories_Definitions-up.sql');
        $this->fixtureManager->execute('SKU_Rules-up.sql');
        $this->fixtureManager->execute('ATTRIBUTES_Groups-up.sql');
        $this->fixtureManager->execute('ATTRIBUTES_Group_Definitions-up.sql');
        $this->fixtureManager->execute('ATTRIBUTES-up.sql');
        $this->fixtureManager->execute('ATTRIBUTES_Definitions-up.sql');
        $this->fixtureManager->execute('COMMON_ATTRIBUTES_Definitions-up.sql');
    }

    protected function setUp()
    {
        $this->init();

        $this->attributesGroupMapper = $this->getMockBuilder('PrismProductsManager\Model\AttributesGroupMapper')
                               ->disableOriginalConstructor()
                               ->getMock();

        $this->commonAttributesTable = $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesTable')
            ->disableOriginalConstructor()
            ->getMock();

        $this->commonAttributesValuesTable = $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable')
            ->disableOriginalConstructor()
            ->getMock();

        $this->commonAttributesValuesDefinitionsTable = $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesDefinitionsTable')
            ->disableOriginalConstructor()
            ->getMock();

        $this->model = new MerchantCategoriesMapper(
            $this->fixtureManager->getDbAdapter(),
            [
                'defaultOneSizeCode' => 'USX',   // this is used to build the sku for one size product
                'languages' => [
                    'current-language' => 1,
                    'site-languages' => [1],
                ],
            ],
            $this->attributesGroupMapper,
            new MerchantCategoriesDefinitionsEntity(),
            new MerchantCategoriesEntity(),
            new SkuRulesEntity(),
            $this->commonAttributesTable,
            $this->commonAttributesValuesTable,
            $this->commonAttributesValuesDefinitionsTable
        );
    }

    public function provideParentIds()
    {
        return [
            [0],
            [1],
        ];
    }

    /**
     * @dataProvider provideParentIds
     */
    public function testFetchMerchantCategories($parentId)
    {
        $this->assertInternalType('array', $this->model->fetchMerchantCategories($parentId, 1));
    }

    /**
     * @dataProvider provideParentIds
     */
    public function testCategoryChildrenCount($parentId)
    {
        $this->assertInternalType('int', $this->model->categoryChildrenCount($parentId));
    }

    public function provideFetchAllMerchantCategories()
    {
        return [
            ['list', null, null, null],
            ['list', 'foo', null, null],
            ['list', 'foo', 'status', null],
            ['list', 'foo', 'created', null],
            ['list', 'foo', 'modified', 'asc'],

            ['other', null, null, null],
            ['other', 'foo', null, null],
            ['other', 'foo', 'status', null],
            ['other', 'foo', 'created', null],
            ['other', 'foo', 'modified', 'asc'],
        ];
    }

    /**
     * @dataProvider provideFetchAllMerchantCategories
     */
    public function testFetchAllMerchantCategories($type, $search, $orderBy, $orderbyOption)
    {
        $this->assertInternalType('array', $this->model->fetchAllMerchantCategories($type, $search, $orderBy, $orderbyOption));
    }

    public function provideEnableCategory()
    {
        return [
            [['checked' => '0', 'id' => 1]],
            [['checked' => '1', 'id' => 1]]
        ];
    }

    /**
     * @dataProvider provideEnableCategory
     */
    public function testEnableCategory($params)
    {
        $this->model->enableCategory($params);

        $this->tearDown();
        $this->init();
    }

    public function testfetchSkuBuildingAttribute()
    {
        $this->assertInternalType('array', $this->model->fetchSkuBuildingAttribute(1, 1));
    }

    public function provideSkuRules()
    {
        return [
            [[]],
            [[
                0 => [
                    'skuRuleId' => 1,
                    'merchantCategoryId' => 1,
                    'merchantAttributeGroupId' => 1,
                    'position' => 1,
                ],
            ]],
        ];
    }

    /**
     * @dataProvider provideSkuRules
     */
    public function testSaveMerchantCategory($skuRules)
    {
        $this->model->saveMerchantCategory([
            'parentId' => 1,
            'status' => 'ACTIVE',
            'categoryName' => 'foo',
            'shortDescription' => 'foo',
            'fullDescription' => 'foo is a foo',
            'skuRules' => [
                1,
                2
            ],
        ]);

        $this->tearDown();
        $this->init();
    }

    public function testDeleteMerchantCategory()
    {
        $this->model->deleteMerchantCategory(1);

        $this->tearDown();
        $this->init();
    }

    public function testRebuildMerchantCategoryByMerchantID()
    {
        $this->model->rebuildMerchantCategoryByMerchantID(1, 1);

        $this->tearDown();
        $this->init();
    }

    protected function tearDown()
    {
        $this->fixtureManager->execute('ATTRIBUTES_Definitions-down.sql');
        $this->fixtureManager->execute('ATTRIBUTES-down.sql');
        $this->fixtureManager->execute('ATTRIBUTES_Group_Definitions-down.sql');
        $this->fixtureManager->execute('ATTRIBUTES_Groups-down.sql');
        $this->fixtureManager->execute('SKU_Rules-down.sql');
        $this->fixtureManager->execute('PRODUCTS_Merchant_Categories_Definitions-down.sql');
        $this->fixtureManager->execute('PRODUCTS_Merchant_Categories-down.sql');
        $this->fixtureManager->execute('COMMON_ATTRIBUTES_Definitions-down.sql');
    }
}
