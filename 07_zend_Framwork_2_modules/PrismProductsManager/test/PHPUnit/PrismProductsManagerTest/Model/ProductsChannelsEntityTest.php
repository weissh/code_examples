<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsChannelsEntity;
use Zend\Db\Sql\Expression;

/**
 * @author haniw
 */
class ProductsChannelsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ChannelsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductsChannelsEntity();
    }
    public function testSetGetStatus()
    {
        // default from constructor
        $this->assertEquals('ACTIVE', $this->model->getStatus());

        $this->model->setStatus('INACTIVE');
        $this->assertEquals('INACTIVE', $this->model->getStatus());
    }

    public function testSetGetCreated()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());

        $this->model->setCreated(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());

        $this->model->setModified(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());
    }

    public function testSetGetProductId()
    {
        $this->model->setProductId(1);
        $this->assertEquals(1, $this->model->getproductId());
    }

    public function testSetGetChannelId()
    {
        $this->model->setChannelId(1);
        $this->assertEquals(1, $this->model->getChannelId());
    }
}
