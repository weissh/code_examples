<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductPricesEntity;

/**
 * @author haniw
 */
class ProductPricesEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductPricesEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductPricesEntity();
    }
    public function testSetGetStatus()
    {
        $this->model->setStatus('INACTIVE');
        $this->assertEquals('INACTIVE', $this->model->getStatus());
    }

    public function testSetGetCreated()
    {
        $created = date('Y-m-d H:i:s');
        $this->model->setCreated($created);
        $this->assertEquals($created, $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        $modified = date('Y-m-d H:i:s');
        $this->model->setModified($modified);
        $this->assertEquals($modified, $this->model->getModified());
    }

    public function testSetGetProductIdFrom()
    {
        $this->model->setProductIdFrom('NOT_PRODUCTS');
        $this->assertEquals('NOT_PRODUCTS', $this->model->getProductIdFrom());
    }

    public function testSetGetOfferPrice()
    {
        $this->model->setOfferPrice(1000);
        $this->assertEquals(1000, $this->model->getOfferPrice());
    }

    public function testSetGetProductId()
    {
        $this->model->setProductId(1);
        $this->assertEquals(1, $this->model->getProductId());
    }

    public function testSetGetRegionId()
    {
        $this->model->setRegionId(1);
        $this->assertEquals(1, $this->model->getRegionId());
    }

    public function testSetGetCurrencyId()
    {
        $this->model->setCurrencyId(1);
        $this->assertEquals(1, $this->model->getCurrencyId());
    }

    public function testSetGetPrice()
    {
        $this->model->setPrice(1000);
        $this->assertEquals(1000, $this->model->getPrice());
    }

    public function testSetGetNowPrice()
    {
        $this->model->setNowPrice(1000);
        $this->assertEquals(1000, $this->model->getNowPrice());
    }

    public function testSetGetWasPrice()
    {
        $this->model->setWasPrice(1000);
        $this->assertEquals(1000, $this->model->getWasPrice());
    }

    public function testSetGetCostPrice()
    {
        $this->model->setCostPrice(1000);
        $this->assertEquals(1000, $this->model->getCostPrice());
    }

    public function testSetGetTradingCostPrice()
    {
        $this->model->setTradingCostPrice(1000);
        $this->assertEquals(1000, $this->model->getTradingCostPrice());
    }

    public function testSetGetWholesalePrice()
    {
        $this->model->setWholesalePrice(1000);
        $this->assertEquals(1000, $this->model->getWholesalePrice());
    }

    public function testGetArrayCopyReturnsAnArrayWithPropertyValues()
    {
        $data  = [
            'productPriceId' => 1,
            'productIdFrom' => 'PRODUCTS',
            'productId' => 1,
            'regionId' => 1,
            'currencyId' => 1,
            'price' => 11111,
            'nowPrice' => 121211,
            'wasPrice' => 121233,
            'costPrice' => 11,
            'tradingCostPrice' => 112,
            'wholesalePrice' => 1333333,
            'status' => 'ACTIVE',
            'modified' => '2015-01-01 00:00:00',
            'created' => '2015-01-01 00:00:00',
        ];
        $this->model->exchangeArray($data);
        $copyArray = $this->model->getArrayCopy();

        $this->assertEquals($data['productPriceId'], $copyArray['productPriceId']);
        $this->assertEquals($data['productIdFrom'], $copyArray['productIdFrom']);
        $this->assertEquals($data['productId'], $copyArray['productId']);
        $this->assertEquals($data['regionId'], $copyArray['regionId']);
        $this->assertEquals($data['currencyId'], $copyArray['currencyId']);
        $this->assertEquals($data['price'], $copyArray['price']);
        $this->assertEquals($data['nowPrice'], $copyArray['nowPrice']);
        $this->assertEquals($data['wasPrice'], $copyArray['wasPrice']);
        $this->assertEquals($data['costPrice'], $copyArray['costPrice']);
        $this->assertEquals($data['tradingCostPrice'], $copyArray['tradingCostPrice']);
        $this->assertEquals($data['wholesalePrice'], $copyArray['wholesalePrice']);
        $this->assertEquals($data['status'], $copyArray['status']);
        $this->assertEquals($data['modified'], $copyArray['modified']);
        $this->assertEquals($data['created'], $copyArray['created']);

        $this->assertEquals($data['productPriceId'], $this->model->getProductPriceId());
        $this->assertEquals($data['productIdFrom'], $this->model->getProductIdFrom());
        $this->assertEquals($data['productId'], $this->model->getProductId());
        $this->assertEquals($data['regionId'], $this->model->getRegionId());
        $this->assertEquals($data['currencyId'], $this->model->getCurrencyId());
        $this->assertEquals($data['price'], $this->model->getPrice());
        $this->assertEquals($data['nowPrice'], $this->model->getNowPrice());
        $this->assertEquals($data['wasPrice'], $this->model->getWasPrice());
        $this->assertEquals($data['costPrice'], $this->model->getCostPrice());
        $this->assertEquals($data['tradingCostPrice'], $this->model->getTradingCostPrice());
        $this->assertEquals($data['wholesalePrice'], $this->model->getWholesalePrice());
        $this->assertEquals($data['status'], $this->model->getStatus());
        $this->assertEquals($data['modified'], $this->model->getModified());
        $this->assertEquals($data['created'], $this->model->getCreated());

    }
}
