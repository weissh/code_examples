<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsPricesEntity;

/**
 * @author haniw
 */
class ProductsPricesEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsPricesEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductsPricesEntity();
    }

    public function testSetGetStatus()
    {
        $this->model->setStatus('INACTIVE');
        $this->assertEquals('INACTIVE', $this->model->getStatus());
    }

    public function testSetGetCreated()
    {
        $created = date('Y-m-d H:i:s');
        $this->model->setCreated($created);
        $this->assertEquals($created, $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        $modified = date('Y-m-d H:i:s');
        $this->model->setModified($modified);
        $this->assertEquals($modified, $this->model->getModified());
    }

    public function testSetGetDesignPrice()
    {
        $this->model->setDesignPrice(10);
        $this->assertEquals(10, $this->model->getDesignPrice());
    }

    public function testSetGetDutyPrice()
    {
        $this->model->setDutyPrice(10);
        $this->assertEquals(10, $this->model->getDutyPrice());
    }

    public function testSetGetOfferPrice()
    {
        $this->model->setOfferPrice(1000);
        $this->assertEquals(1000, $this->model->getOfferPrice());
    }

    public function testSetGetFreightPrice()
    {
        $this->model->setFreightPrice(1000);
        $this->assertEquals(1000, $this->model->getFreightPrice());
    }

    public function testSetGetPackagingPrice()
    {
        $this->model->setPackagingPrice(1000);
        $this->assertEquals(1000, $this->model->getPackagingPrice());
    }

    public function testSetGetProductPriceId()
    {
        $this->model->setProductPriceId(1);
        $this->assertEquals(1, $this->model->getProductPriceId());
    }

    public function testSetGetProductIdFrom()
    {
        $this->model->setProductIdFrom(1);
        $this->assertEquals(1, $this->model->getProductIdFrom());
    }

    public function testSetGetProductOrVariantId()
    {
        $this->model->setProductOrVariantId(1);
        $this->assertEquals(1, $this->model->getProductOrVariantId());
    }

    public function testSetGetRegionId()
    {
        $this->model->setRegionId(1);
        $this->assertEquals(1, $this->model->getRegionId());
    }

    public function testSetGetCurrencyId()
    {
        $this->model->setCurrencyId(1);
        $this->assertEquals(1, $this->model->getCurrencyId());
    }

    public function testSetGetPrice()
    {
        $this->model->setPrice(1000);
        $this->assertEquals(1000, $this->model->getPrice());
    }

    public function testSetGetNowPrice()
    {
        $this->model->setNowPrice(1000);
        $this->assertEquals(1000, $this->model->getNowPrice());
    }

    public function testSetGetWasPrice()
    {
        $this->model->setWasPrice(1000);
        $this->assertEquals(1000, $this->model->getWasPrice());
    }

    public function testSetGetCostPrice()
    {
        $this->model->setCostPrice(1000);
        $this->assertEquals(1000, $this->model->getCostPrice());
    }

    public function testSetGetTradingCostPrice()
    {
        $this->model->setTradingCostPrice(1000);
        $this->assertEquals(1000, $this->model->getTradingCostPrice());
    }

    public function testSetGetWholesalePrice()
    {
        $this->model->setWholesalePrice(1000);
        $this->assertEquals(1000, $this->model->getWholesalePrice());
    }
}
