<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\Categories;

/**
 * @author haniw
 */
class CategoriesTest extends PHPUnit_Framework_TestCase
{
    /** @var Categories */
    protected $model;

    protected function setUp()
    {
        $this->model = new Categories();
    }

    public function testModelInitialState()
    {
       $this->assertNull($this->model->categoryId);
       $this->assertNull($this->model->parentId);
       $this->assertNull($this->model->overviewImageId);
       $this->assertNull($this->model->position);
       $this->assertNull($this->model->enabled);
       $this->assertNull($this->model->created);
       $this->assertNull($this->model->modified);
   }

   public function testExchangeArraySetsPropertiesCorrectly()
   {
        $data  = [
            'categoryId' => 1,
            'parentId' => 0,
            'overviewImageId' => 1,
            'position' => 1,
            'enabled' => true,
            'created' => '2015-01-01',
            'modified' => '2015-01-01',
        ];
        $this->model->exchangeArray($data);

        $this->assertEquals($data['categoryId'], $this->model->categoryId);
        $this->assertEquals($data['parentId'], $this->model->parentId);
        $this->assertEquals($data['overviewImageId'], $this->model->overviewImageId);
        $this->assertEquals($data['position'], $this->model->position);
        $this->assertEquals($data['enabled'], $this->model->enabled);
        $this->assertEquals($data['created'], $this->model->created);
        $this->assertEquals($data['modified'], $this->model->modified);
   }

   public function testGetArrayCopyReturnsAnArrayWithPropertyValues()
   {
       $data  = [
           'categoryId' => 1,
           'parentId' => 0,
           'overviewImageId' => 1,
           'position' => 1,
           'enabled' => true,
           'created' => '2015-01-01',
           'modified' => '2015-01-01',
       ];
       $this->model->exchangeArray($data);
       $copyArray = $this->model->getArrayCopy();

       $this->assertEquals($data['categoryId'], $copyArray['categoryId']);
       $this->assertEquals($data['parentId'], $copyArray['parentId']);
       $this->assertEquals($data['overviewImageId'], $copyArray['overviewImageId']);
       $this->assertEquals($data['position'], $copyArray['position']);
       $this->assertEquals($data['enabled'], $copyArray['enabled']);
       $this->assertEquals($data['created'], $copyArray['created']);
       $this->assertEquals($data['modified'], $copyArray['modified']);
   }
}
