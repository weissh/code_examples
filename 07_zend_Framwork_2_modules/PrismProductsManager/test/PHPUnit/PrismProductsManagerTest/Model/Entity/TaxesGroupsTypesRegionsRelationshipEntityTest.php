<?php

namespace PrismProductsManagerTest\Model\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\Entity\TaxesGroupsTypesRegionsRelationshipEntity;
use Zend\Db\Sql\Expression;

/**
 * @author haniw
 */
class TaxesGroupsTypesRegionsRelationshipEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var TaxesExternalProvidersEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new TaxesGroupsTypesRegionsRelationshipEntity();
    }

    public function testSetGetTaxGroupTypeRegionId()
    {
        $this->model->setTaxGroupTypeRegionId(1);
        $this->assertEquals(1, $this->model->getTaxGroupTypeRegionId());
    }

    public function testSetGetTaxGroupId()
    {
        $this->model->setTaxGroupId(1);
        $this->assertEquals(1, $this->model->getTaxGroupId());
    }

    public function testSetGetTaxTypeid()
    {
        $this->model->setTaxTypeid(1);
        $this->assertEquals(1, $this->model->getTaxTypeid());
    }

    public function testSetGetTaxRegionId()
    {
        $this->model->setTaxRegionId(1);
        $this->assertEquals(1, $this->model->getTaxRegionId());
    }

    public function testSetGetValue()
    {
        $this->model->setValue(1);
        $this->assertEquals(1, $this->model->getValue());
    }

    public function testSetGetPriority()
    {
        $this->model->setPriority(0);
        $this->assertEquals(0, $this->model->getPriority());
    }

    public function testSetGetStatus()
    {
        // default from constructor
        $this->assertEquals('ACTIVE', $this->model->getStatus());

        $this->model->setStatus('INACTIVE');
        $this->assertEquals('INACTIVE', $this->model->getStatus());
    }

    public function testSetGetCreated()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());

        $this->model->setCreated(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());

        $this->model->setModified(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());
    }
}
