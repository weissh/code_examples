<?php

namespace PrismProductsManagerTest\Model\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\Entity\TaxesTypesEntity;
use Zend\Db\Sql\Expression;

/**
 * @author haniw
 */
class TaxesTypesEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var TaxesTypesEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new TaxesTypesEntity();
    }

    public function testSetGetTaxTypeid()
    {
        $this->model->setTaxTypeid(1);
        $this->assertEquals(1, $this->model->getTaxTypeid());
    }

    public function testSetGetCalculateForm()
    {
        $this->model->setCalculateFrom(1);
        $this->assertEquals(1, $this->model->getCalculateFrom());
    }

    public function testSetGetStatus()
    {
        // default from constructor
        $this->assertEquals('ACTIVE', $this->model->getStatus());

        $this->model->setStatus('INACTIVE');
        $this->assertEquals('INACTIVE', $this->model->getStatus());
    }

    public function testSetGetCreated()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());

        $this->model->setCreated(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());

        $this->model->setModified(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());
    }
}
