<?php

namespace PrismProductsManagerTest\Model\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\Entity\ProductsTabsEntity;
use Zend\Db\Sql\Expression;

/**
 * @author haniw
 */
class ProductsTabsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsTabsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductsTabsEntity();
    }

    public function testSetGetProductId()
    {
        $this->model->setProductId(1);
        $this->assertEquals(1, $this->model->getProductId());
    }

    public function testSetGetTabId()
    {
        $this->model->setTabId(1);
        $this->assertEquals(1, $this->model->getTabId());
    }

    public function testSetGetTabGroupId()
    {
        $this->model->setTabGroupId(1);
        $this->assertEquals(1, $this->model->getTabGroupId());
    }

    public function testSetGetStatus()
    {
        // default from constructor
        $this->assertEquals('ACTIVE', $this->model->getStatus());

        $this->model->setStatus('INACTIVE');
        $this->assertEquals('INACTIVE', $this->model->getStatus());
    }

    public function testSetGetPriority()
    {
        // default from constructor
        $this->assertEquals(0, $this->model->getPriority());

        $this->model->setPriority(1);
        $this->assertEquals(1, $this->model->getPriority());
    }


    public function testSetGetCreated()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());

        $this->model->setCreated(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());

        $this->model->setModified(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());
    }
}
