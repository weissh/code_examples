<?php

namespace PrismProductsManagerTest\Model\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\Entity\TaxesExternalProvidersEntity;
use Zend\Db\Sql\Expression;

/**
 * @author haniw
 */
class TaxesExternalProvidersEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var TaxesExternalProvidersEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new TaxesExternalProvidersEntity();
    }

    public function testSetGetTaxExternalProviderId()
    {
        $this->model->setTaxExternalProviderId(1);
        $this->assertEquals(1, $this->model->getTaxExternalProviderId());
    }

    public function testSetGetName()
    {
        $this->model->setName('foo');
        $this->assertEquals('foo', $this->model->getName());
    }

    public function testSetGetDescription()
    {
        $this->model->setDescription('bar baz bat');
        $this->assertEquals('bar baz bat', $this->model->getDescription());
    }

    public function testSetGetStatus()
    {
        // default from constructor
        $this->assertEquals('ACTIVE', $this->model->getStatus());

        $this->model->setStatus('INACTIVE');
        $this->assertEquals('INACTIVE', $this->model->getStatus());
    }
 
    public function testSetGetCreated()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());

        $this->model->setCreated(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());

        $this->model->setModified(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());
    }
}
