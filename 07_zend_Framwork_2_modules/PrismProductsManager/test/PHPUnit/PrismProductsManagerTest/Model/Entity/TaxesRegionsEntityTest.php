<?php

namespace PrismProductsManagerTest\Model\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\Entity\TaxesRegionsEntity;
use Zend\Db\Sql\Expression;

/**
 * @author haniw
 */
class TaxesRegionsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var TaxesRegionsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new TaxesRegionsEntity();
    }

    public function testSetGetTaxRegionId()
    {
        $this->model->setTaxRegionId(1);
        $this->assertEquals(1, $this->model->getTaxRegionId());
    }

    public function testSetGetTaxRegion()
    {
        $this->model->setTaxRegion('foo');
        $this->assertEquals('foo', $this->model->getTaxRegion());
    }

    public function testSetGetStatus()
    {
        // default from constructor
        $this->assertEquals('ACTIVE', $this->model->getStatus());

        $this->model->setStatus('INACTIVE');
        $this->assertEquals('INACTIVE', $this->model->getStatus());
    }

    public function testSetGetCreated()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());

        $this->model->setCreated(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());

        $this->model->setModified(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());
    }
}
