<?php

namespace PrismProductsManagerTest\Model\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\Entity\ProductsRelatedProductsEntity;
use Zend\Db\Sql\Expression;

/**
 * @author haniw
 */
class ProductsRelatedProductsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsRelatedProductsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductsRelatedProductsEntity();
    }

    public function testSetGetProductProductId()
    {
        $this->model->setProductProductId(1);
        $this->assertEquals(1, $this->model->getProductProductId());
    }

    public function testSetGetProductIdFrom()
    {
        $this->model->setProductIdFrom(1);
        $this->assertEquals(1, $this->model->getProductIdFrom());
    }

    public function testSetGetProductId()
    {
        $this->model->setProductId(1);
        $this->assertEquals(1, $this->model->getProductId());
    }

    public function testSetGetRelatedProductId()
    {
        $this->model->setRelatedProductId(1);
        $this->assertEquals(1, $this->model->getRelatedProductId());
    }

    public function testSetGetStatus()
    {
        // default from constructor
        $this->assertEquals('ACTIVE', $this->model->getStatus());

        $this->model->setStatus('INACTIVE');
        $this->assertEquals('INACTIVE', $this->model->getStatus());
    }

    public function testSetGetCreated()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());

        $this->model->setCreated(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());

        $this->model->setModified(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());
    }
}
