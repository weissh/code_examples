<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsDefinitionsEntity;

/**
 * @author haniw
 */
class ProductsDefinitionsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsDefinitionsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductsDefinitionsEntity();
    }

    public function testSetGetMisSpells()
    {
        $this->model->setMisSpells('foo');
        $this->assertEquals('foo', $this->model->getMisSpells());
    }

    public function testSetGetTags()
    {
        $this->model->setTags('foo');
        $this->assertEquals('foo', $this->model->getTags());
    }

    public function testSetGetShortProductName()
    {
        $this->model->setShortProductName('foo');
        $this->assertEquals('foo', $this->model->getShortProductName());
    }

    public function testSetGetProductDefinitionsId()
    {
        $this->model->setProductDefinitionsId(1);
        $this->assertEquals(1, $this->model->getProductDefinitionsId());
    }

    public function testSetGetProductId()
    {
        $this->model->setProductId(1);
        $this->assertEquals(1, $this->model->getProductId());
    }

    public function testSetGetLanguageId()
    {
        $this->model->setLanguageId(1);
        $this->assertEquals(1, $this->model->getLanguageId());
    }

    public function testSetGetVersionId()
    {
        $this->model->setVersionId(1);
        $this->assertEquals(1, $this->model->getVersionId());
    }

    public function testSetGetVersionDescription()
    {
        $this->model->setVersionDescription('foo');
        $this->assertEquals('foo', $this->model->getVersionDescription());
    }

    public function testSetGetProductName()
    {
        $this->model->setProductName('foo');
        $this->assertEquals('foo', $this->model->getProductName());
    }

    public function testSetGetUrlName()
    {
        $this->model->setUrlName('foo');
        $this->assertEquals('foo', $this->model->getUrlName());
    }

    public function testSetGetMetaTitle()
    {
        $this->model->setMetaTitle('foo');
        $this->assertEquals('foo', $this->model->getMetaTitle());
    }

    public function testSetGetMetaDescription()
    {
        $this->model->setMetaDescription('foo');
        $this->assertEquals('foo', $this->model->getMetaDescription());
    }

    public function testSetGetShortDescription()
    {
        $this->model->setShortDescription('foo');
        $this->assertEquals('foo', $this->model->getShortDescription());
    }

    public function testSetGetLongDescription()
    {
        $this->model->setLongDescription('foo');
        $this->assertEquals('foo', $this->model->getLongDescription());
    }

    public function testSetGetMakeLive()
    {
        $this->model->setMakeLive('foo');
        $this->assertEquals('foo', $this->model->getMakeLive());
    }

    public function testSetGetStatus()
    {
        // default from constructor
        $this->assertEquals('INACTIVE', $this->model->getStatus());

        $this->model->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->model->getStatus());
    }

    public function testSetGetCreated()
    {
        $created = date('Y-m-d H:i:s');
        $this->model->setCreated($created);
        $this->assertEquals($created, $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        $modified = date('Y-m-d H:i:s');
        $this->model->setModified($modified);
        $this->assertEquals($modified, $this->model->getModified());
    }
}
