<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsCategoriesEntity;

/**
 * @author haniw
 */
class ProductsCategoriesEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsCategoriesEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductsCategoriesEntity();
    }

    public function testSetGetPriority()
    {
        // default from constructor
        $this->assertEquals(1, $this->model->getPriority());

        $this->model->setPriority(0);
        $this->assertEquals(0, $this->model->getPriority());
    }

    public function testSetGetProductIdFrom()
    {
        $this->assertEquals('PRODUCTS', $this->model->getProductIdFrom());

        $this->model->setProductIdFrom('NOT_PRODUCTS');
        $this->assertEquals('NOT_PRODUCTS', $this->model->getProductIdFrom());
    }

    public function testSetGetProductCategoryId()
    {
        $this->model->setProductCategoryId(1);
        $this->assertEquals(1, $this->model->getProductCategoryId());
    }

    public function testSetGetProductId()
    {
        $this->model->setProductId(1);
        $this->assertEquals(1, $this->model->getProductId());
    }

    public function testSetGetCategoryId()
    {
        $this->model->setCategoryId(1);
        $this->assertEquals(1, $this->model->getCategoryId());
    }

    public function testSetGetIsDefault()
    {
        // default from constructor
        $this->assertEquals(0, $this->model->getIsDefault());

        $this->model->setIsDefault(1);
        $this->assertEquals(1, $this->model->getIsDefault());
    }

    public function testSetGetStatus()
    {
        // default from constructor
        $this->assertEquals('ACTIVE', $this->model->getStatus());

        $this->model->setStatus('INACTIVE');
        $this->assertEquals('INACTIVE', $this->model->getStatus());
    }

    public function testSetGetCreated()
    {
        $created = date('Y-m-d H:i:s');
        $this->model->setCreated($created);
        $this->assertEquals($created, $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        $modified = date('Y-m-d H:i:s');
        $this->model->setModified($modified);
        $this->assertEquals($modified, $this->model->getModified());
    }
}
