<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsEntity;
use Zend\Db\Sql\Expression;

/**
 * @author haniw
 */
class ProductsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ChannelsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductsEntity();
    }
    public function testSetGetStatus()
    {
        // default from constructor
        $this->assertEquals('INACTIVE', $this->model->getStatus());

        $this->model->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->model->getStatus());
    }

    public function testSetGetCreated()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());

        $this->model->setCreated(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());

        $this->model->setModified(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());
    }

    public function testSetGetStyle()
    {
        $this->model->setStyle('london');
        $this->assertEquals('london', $this->model->getStyle());
    }

    public function testSetGetProductId()
    {
        $this->model->setProductId(1);
        $this->assertEquals(1, $this->model->getProductId());
    }

    public function testSetGetProductMerchantcategoryId()
    {
        $this->model->setProductMerchantcategoryId(1);
        $this->assertEquals(1, $this->model->getProductMerchantcategoryId());
    }

    public function testSetGetProductTypeId()
    {
        $this->model->setProductTypeId(1);
        $this->assertEquals(1, $this->model->getProductTypeId());
    }

    public function testSetGetManufacturedId()
    {
        $this->model->setManufacturerId(1);
        $this->assertEquals(1, $this->model->getManufacturerId());
    }

    public function testSetGetEan13()
    {
        $this->model->setEan13('5055347347580');
        $this->assertEquals('5055347347580', $this->model->getEan13());
    }

    public function testSetGetEcoTax()
    {
        $this->model->setEcoTax('0.00');
        $this->assertEquals('0.00', $this->model->getEcoTax());
    }

    public function testSetGetQuantity()
    {
        $this->model->setQuantity(1);
        $this->assertEquals(1, $this->model->getQuantity());
    }

    public function testSetGetSku()
    {
        $this->model->setSku('ALESDEBRW060');
        $this->assertEquals('ALESDEBRW060', $this->model->getSku());
    }

    public function testSetGetsupplierreference()
    {
        $this->model->setsupplierreference('BER01');
        $this->assertEquals('BER01', $this->model->getsupplierreference());
    }
}
