<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsTagsEntity;

/**
 * @author haniw
 */
class ProductsTagsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsTagsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductsTagsEntity();
    }

    public function testSetGetProductId()
    {
        $this->model->setProductId(1);
        $this->assertEquals(1, $this->model->getProductId());
    }

    public function testSetGetProductTagId()
    {
        $this->model->setProductTagId(1);
        $this->assertEquals(1, $this->model->getProductTagId());
    }

    public function testSetGetAlias()
    {
        $this->model->setAlias('foo');
        $this->assertEquals('foo', $this->model->getAlias());
    }

    public function testSetGetLanguageId()
    {
        $this->model->setLanguageId(1);
        $this->assertEquals(1, $this->model->getLanguageId());
    }

    public function testSetGetStatus()
    {
        // default from constructor
        $this->assertEquals('ACTIVE', $this->model->getStatus());

        $this->model->setStatus('INACTIVE');
        $this->assertEquals('INACTIVE', $this->model->getStatus());
    }

    public function testSetGetCreated()
    {
        $created = date('Y-m-d H:i:s');
        $this->model->setCreated($created);
        $this->assertEquals($created, $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        $modified = date('Y-m-d H:i:s');
        $this->model->setModified($modified);
        $this->assertEquals($modified, $this->model->getModified());
    }
}
