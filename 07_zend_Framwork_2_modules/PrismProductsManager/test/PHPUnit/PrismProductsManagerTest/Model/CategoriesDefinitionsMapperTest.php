<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\CategoriesEntity;
use PrismProductsManager\Model\CategoriesDefinitionsMapper;
use PrismProductsManagerTest\ZendDbFixture\FixtureManager;
use Zend\Db\Sql\Sql;

class CategoriesDefinitionsMapperTest extends PHPUnit_Framework_TestCase
{
    /** @var CategoriesDefinitionsMapper */
    private $model;

    /** @var \PrismProductsmanagerTest\ZendDbFixture\FixtureManager */
    private $fixtureManager;

    private function init()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');

        $this->fixtureManager->execute('CATEGORIES_Definitions-up.sql');
    }

    protected function setUp()
    {
        $this->init();
        $this->model = new CategoriesDefinitionsMapper(
            $this->fixtureManager->getDbAdapter()
        );
    }

    public function testSaveCategoryDefinitions()
    {
        $entity = new CategoriesEntity();
        $entity->setCategoryId(1);
        $entity->setPosition(1);

        $this->model->saveCategoryDefinitions($entity);

        $this->tearDown();
        $this->init();
    }

    protected function tearDown()
    {
        $this->fixtureManager->execute('CATEGORIES_Definitions-down.sql');
    }
}
