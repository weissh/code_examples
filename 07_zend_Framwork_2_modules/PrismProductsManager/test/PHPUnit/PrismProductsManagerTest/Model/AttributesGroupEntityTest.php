<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\AttributesGroupEntity;

/**
 * @author haniw
 */
class AttributesGroupEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var AttributesGroupEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new AttributesGroupEntity();
    }

    public function testSetGetAttributeGroupId()
    {
        $this->model->setAttributeGroupId(1);
        $this->assertEquals(1, $this->model->getAttributeGroupId());
    }

    public function testSetGetGroupCode()
    {
        $this->model->setGroupCode('foo');
        $this->assertEquals('foo', $this->model->getGroupCode());
    }

    public function testSetGetGroupParentId()
    {
        $this->model->setGroupParentId(1);
        $this->assertEquals(1, $this->model->getGroupParentId());
    }

    public function testSetGetGroupType()
    {
        $this->model->setGroupType('foo');
        $this->assertEquals('foo', $this->model->getGroupType());
    }

    public function testSetGetSourceTable()
    {
        $this->model->setSourceTable('foo');
        $this->assertEquals('foo', $this->model->getSourceTable());
    }

    public function testSetGetViewAs()
    {
        $this->model->setViewAs('foo');
        $this->assertEquals('foo', $this->model->getViewAs());
    }
}
