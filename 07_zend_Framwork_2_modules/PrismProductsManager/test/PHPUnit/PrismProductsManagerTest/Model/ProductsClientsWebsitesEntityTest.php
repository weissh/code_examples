<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsClientsWebsitesEntity;

/**
 * @author haniw
 */
class ProductsClientsWebsitesEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsClientsWebsitesEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductsClientsWebsitesEntity();
    }

    public function testSetGetProductIdFrom()
    {
        $this->model->setProductIdFrom(1);
        $this->assertEquals(1, $this->model->getProductIdFrom());
    }

    public function testSetGetProductId()
    {
        $this->model->setProductId(1);
        $this->assertEquals(1, $this->model->getProductId());
    }

    public function testSetGetWebsiteId()
    {
        $this->model->setWebsiteId(1);
        $this->assertEquals(1, $this->model->getWebsiteId());
    }

    public function testSetGetProductWebsiteId()
    {
        $this->model->setProductWebsiteId(1);
        $this->assertEquals(1, $this->model->getProductWebsiteId());
    }

    public function testSetGetElasticSearchIndexId()
    {
        // default from constructor
        $this->assertEquals(0, $this->model->getElasticSearchIndexId());

        $this->model->setElasticSearchIndexId(1);
        $this->assertEquals(1, $this->model->getElasticSearchIndexId());
    }
}
