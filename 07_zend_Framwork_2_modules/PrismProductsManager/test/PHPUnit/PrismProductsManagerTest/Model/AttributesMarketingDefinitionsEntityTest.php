<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\AttributesMarketingDefinitionsEntity;

/**
 * @author haniw
 */
class AttributesMarketingDefinitionsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var AttributesMarketingDefinitionsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new AttributesMarketingDefinitionsEntity();
    }

    public function testSetGetAttributeId()
    {
        $this->model->setAttributeId(1);
        $this->assertEquals(1, $this->model->getAttributeId());
    }

    public function testSetGetAttributeValue()
    {
        $this->model->setAttributeValue('foo');
        $this->assertEquals('foo', $this->model->getAttributeValue());
    }

    public function testSetGetAttributesDefinitionsId()
    {
        $this->model->setAttributesDefinitionsId(1);
        $this->assertEquals(1, $this->model->getAttributesDefinitionsId());
    }

    public function testSetGetLanguageId()
    {
        $this->model->setLanguageId(1);
        $this->assertEquals(1, $this->model->getLanguageId());
    }
}
