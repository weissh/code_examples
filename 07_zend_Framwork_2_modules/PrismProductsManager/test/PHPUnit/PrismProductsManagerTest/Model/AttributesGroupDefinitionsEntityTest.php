<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\AttributesGroupDefinitionsEntity;

/**
 * @author haniw
 */
class AttributesGroupDefinitionsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var AttributesGroupDefinitionsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new AttributesGroupDefinitionsEntity();
    }

    public function testSetGetAttributeGroupId()
    {
        $this->model->setAttributeGroupId(1);
        $this->assertEquals(1, $this->model->getAttributeGroupId());
    }

    public function testSetGetAttributeGroupDefinitionId()
    {
        $this->model->setAttributeGroupDefinitionId(1);
        $this->assertEquals(1, $this->model->getAttributeGroupDefinitionId());
    }

    public function testSetGetLanguageId()
    {
        $this->model->setLanguageId(1);
        $this->assertEquals(1, $this->model->getLanguageId());
    }

    public function testSetGetName()
    {
        $this->model->setName('foo');
        $this->assertEquals('foo', $this->model->getName());
    }

    public function testSetGetPublicName()
    {
        $this->model->setPublicName('foo');
        $this->assertEquals('foo', $this->model->getPublicName());
    }
}
