<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductCategoriesMapper;

/**
 * @author haniw
 */
class ProductCategoriesMapperTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductCategoriesMapper */
    private $model;

    /** @var \PrismProductsManager\Model\CategoriesTable */
    private $categoriesTable;

    protected function setUp()
    {
        $this->categoriesTable = $this->getMockBuilder('PrismProductsManager\Model\CategoriesTable')
                                    ->disableOriginalConstructor()
                                    ->getMock();
        $this->model = new ProductCategoriesMapper($this->categoriesTable);
    }

    public function testGetCategories()
    {
        $tableGateway = $this->getMockBuilder('Zend\Db\TableGateway\TableGateway')
                             ->disableOriginalConstructor()
                             ->getMock();
        $sql = $this->getMockBuilder('Zend\Db\Sql\Sql')
                    ->disableOriginalConstructor()
                    ->getMock();
        $select = $this->getMockBuilder('Zend\Db\Sql\Select')
                       ->disableOriginalConstructor()
                       ->getMock();
        $sql->expects($this->once())
            ->method('select')
            ->willReturn($select);

        $select->expects($this->once())
               ->method('where')
               ->with(['enabled' => 1])
               ->willReturn($select);

        $tableGateway->expects($this->once())
                     ->method('getSql')
                     ->willReturn($sql);

        $rowset = $this->getMockBuilder('Zend\Db\Resultset\ResultSet')
                       ->disableOriginalConstructor()
                       ->getMock();
        $tableGateway->expects($this->once())
                     ->method('selectWith')
                     ->with($select)
                     ->willReturn($rowset);

        $this->categoriesTable->expects($this->exactly(2))
                              ->method('getTableGateway')
                              ->willReturn($tableGateway);

        $this->assertInstanceOf('Zend\Db\Resultset\ResultSet', $this->model->getCategories());
    }

    public function provideReturnSave()
    {
        return [
            [0],
            [1],
        ];
    }

    /**
     * @dataProvider provideReturnSave
     */
    public function testSaveCategory($return)
    {
        $tableGateway = $this->getMockBuilder('Zend\Db\TableGateway\TableGateway')
                             ->disableOriginalConstructor()
                             ->getMock();

        $data['created']  = '2015-01-01 00:00:00';
        $data['modified'] = '2015-01-01 00:00:00';

        $tableGateway->expects($this->any())
                  ->method('insert')
                  ->with($data)
                  ->willReturn($return);

        $this->categoriesTable->expects($this->exactly(1))
                            ->method('getTableGateway')
                            ->willReturn($tableGateway);

        $this->model->saveCategory($data);
    }

    public function provideUpdateParams()
    {
        return [
            [0],
            [-1],
            [1],
        ];
    }

    /**
     * @dataProvider provideUpdateParams
     */
    public function testUpdateExistingCategory($id)
    {
        if ($id > 0) {
            $tableGateway = $this->getMockBuilder('Zend\Db\TableGateway\TableGateway')
                                 ->disableOriginalConstructor()
                                 ->getMock();

            $tableGateway->expects($this->any())
                      ->method('update')
                      ->with([], ['id' => $id])
                      ->willReturn(1);

            $this->categoriesTable->expects($this->exactly(1))
                                ->method('getTableGateway')
                                ->willReturn($tableGateway);

            $this->model->updateExistingCategory($id, []);
        }
    }

    /**
     * @dataProvider provideUpdateParams
     */
    public function testUpdateExistingCategoryReturnZero($id)
    {
        if ($id > 0) {
            $tableGateway = $this->getMockBuilder('Zend\Db\TableGateway\TableGateway')
                                 ->disableOriginalConstructor()
                                 ->getMock();

            $tableGateway->expects($this->any())
                      ->method('update')
                      ->with([], ['id' => $id])
                      ->willReturn(0);

            $this->categoriesTable->expects($this->exactly(1))
                                ->method('getTableGateway')
                                ->willReturn($tableGateway);

            $this->model->updateExistingCategory($id, []);
        }
    }
}
