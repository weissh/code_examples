<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\MerchantCategoriesDefinitionsEntity;

/**
 * @author haniw
 */
class MerchantCategoriesDefinitionsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var MerchantCategoriesDefinitionsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new MerchantCategoriesDefinitionsEntity();
    }

    public function testSetGetCategoryName()
    {
        $this->model->setCategoryName('foo');
        $this->assertEquals('foo', $this->model->getCategoryName());
    }

    public function testSetGetLanguageId()
    {
        $this->model->setLanguageId(1);
        $this->assertEquals(1, $this->model->getLanguageId());
    }

    public function testSetGetProductMerchantcategoryId()
    {
        $this->model->setProductMerchantcategoryId(1);
        $this->assertEquals(1, $this->model->getProductMerchantcategoryId());
    }

    public function testSetGetShortDescription()
    {
        $this->model->setShortDescription('foo');
        $this->assertEquals('foo', $this->model->getShortDescription());
    }

    public function testSetGetFullDescription()
    {
        $this->model->setFullDescription('foo');
        $this->assertEquals('foo', $this->model->getFullDescription());
    }

    public function testSetGetCreated()
    {
        $created = date('Y-m-d H:i:s');
        $this->model->setCreated($created);
        $this->assertEquals($created, $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        $modified = date('Y-m-d H:i:s');
        $this->model->setModified($modified);
        $this->assertEquals($modified, $this->model->getModified());
    }
}
