<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\SizesShoesMapper;
use PrismProductsManagerTest\ZendDbFixture\FixtureManager;

class SizesShoesMapperTest extends PHPUnit_Framework_TestCase
{
    /** @var SizesShoesMapper */
    private $model;

    /** @var \PrismProductsmanagerTest\ZendDbFixture\FixtureManager */
    private $fixtureManager;

    private function init()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');

        $this->fixtureManager->execute('SIZES_Shoes-up.sql');
    }

    protected function setUp()
    {
        $this->init();
        $this->model = new SizesShoesMapper(
            $this->fixtureManager->getDbAdapter()
        );
    }

    public function testFetchAll()
    {
        $this->assertInstanceOf('Zend\Db\Adapter\Driver\Pdo\Result', $this->model->fetchAll());
    }

    public function provideIds()
    {
        return [
            [11],
            [100000],
        ];
    }

    /**
     * @dataProvider provideIds
     */
    public function testgetValueById($id)
    {
        $this->assertInternalType('array', $this->model->getValueById($id));
    }

    protected function tearDown()
    {
        $this->fixtureManager->execute('SIZES_Shoes-down.sql');
    }
}
