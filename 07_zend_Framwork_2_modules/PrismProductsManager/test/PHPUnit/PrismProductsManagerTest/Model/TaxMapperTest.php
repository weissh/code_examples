<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\TaxMapper;
use PrismProductsManagerTest\ZendDbFixture\FixtureManager;

class TaxMapperTest extends PHPUnit_Framework_TestCase
{
    /** @var TaxMapper */
    private $model;

    /** @var \PrismProductsmanagerTest\ZendDbFixture\FixtureManager */
    private $fixtureManager;

    private function init()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');

        $this->fixtureManager->execute('TAXES-up.sql');
        $this->fixtureManager->execute('TAXES_Definitions-up.sql');
        $this->fixtureManager->execute('TAXES_Types-up.sql');
        $this->fixtureManager->execute('TAXES_Types_Definitions-up.sql');
        $this->fixtureManager->execute('PRODUCTS_R_TAXES-up.sql');
    }

    protected function setUp()
    {
        $this->init();
        $this->model = new TaxMapper(
            $this->fixtureManager->getDbAdapter(),
            $this->getMock('Zend\ServiceManager\ServiceLocatorInterface'),
            [1]
        );
    }

    public function provideTaxLists()
    {
        return [
            [1, true],
            [1, false],
            [2, true],
            [2, false],
        ];
    }

    /**
     * @dataProvider provideTaxLists
     */
    public function testGetTaxLists($languageId, $onlyActive)
    {
        $this->assertInternalType('array', $this->model->getTaxLists($languageId, $onlyActive));
    }

    public function provideProductTaxTerritoriesUsed()
    {
        return [
            [1, 0, 1],
            [1, 1, 1],
        ];
    }

    /**
     * @dataProvider provideProductTaxTerritoriesUsed
     */
    public function testGetProductTaxTerritoriesUsed($productOrVariantId, $isVariant, $languageId)
    {
        $this->assertInternalType('array', $this->model->getProductTaxTerritoriesUsed($productOrVariantId, $isVariant, $languageId));
    }

    public function provideLanguages()
    {
        return [
            [1],
            [2],
        ];
    }

    /**
     * @dataProvider provideLanguages
     */
    public function testGetTaxTypes($languageId)
    {
        $this->assertInternalType('array', $this->model->getTaxTypes($languageId));
    }

    public function provideIsVariant()
    {
        return [
            [0],
            [1],
        ];
    }

    /**
     * @dataProvider provideIsVariant
     */
    public function testGetTaxForProduct($isVariant)
    {
        $this->assertInternalType('array', $this->model->getTaxForProduct(1, $isVariant));
    }

    /**
     * @dataProvider provideIsVariant
     */
    public function testGetTaxValueForProduct($isVariant)
    {
        $this->assertInternalType('array', $this->model->getTaxValueForProduct(1, $isVariant));
    }

    public function testDeleteTax()
    {
        $this->model->deleteTax(1);

        $this->tearDown();
        $this->init();
    }

    public function provideDeactive()
    {
        return [
            [true],
            [false],
        ];
    }

    /**
     * @dataProvider provideDeactive
     */
    public function testGoggleTax($deactive)
    {
        $this->model->toggleTax(1, $deactive);

        $this->tearDown();
        $this->init();
    }

    public function provideSaveTax()
    {
        return [
            [1, 1],
            [0, 1],
        ];
    }

    /**
     * @dataProvider provideSaveTax
     */
    public function testSaveTax($id, $currentLanguage)
    {
        $this->model->saveTax(
            [
                'tax_type' => 1,
                'tax_country' => 'ID',
                'tax_value' => 20000,
                'name' => 'fooo',
                'description' => 'foo bar baz',
            ],
            $id,
            $currentLanguage
        );

        $this->tearDown();
        $this->init();
    }

    protected function tearDown()
    {
        $this->fixtureManager->execute('PRODUCTS_R_TAXES-down.sql');
        $this->fixtureManager->execute('TAXES_Types_Definitions-down.sql');
        $this->fixtureManager->execute('TAXES_Types-down.sql');
        $this->fixtureManager->execute('TAXES_Definitions-down.sql');
        $this->fixtureManager->execute('TAXES-down.sql');
    }
}
