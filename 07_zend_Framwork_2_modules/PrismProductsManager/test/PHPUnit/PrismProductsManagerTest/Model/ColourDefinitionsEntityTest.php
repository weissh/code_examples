<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ColourDefinitionsEntity;

/**
 * @author haniw
 */
class ColourDefinitionsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ColourDefinitionsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ColourDefinitionsEntity();
    }

    public function testSetGetColourId()
    {
        $this->model->setColourId(1);
        $this->assertEquals(1, $this->model->getColourId());
    }

    public function testSetGetColourDefinitionId()
    {
        $this->model->setColourDefinitionId(1);
        $this->assertEquals(1, $this->model->getColourDefinitionId());
    }

    public function testSetGetColourDescription()
    {
        $this->model->setColourDescription('foo');
        $this->assertEquals('foo', $this->model->getColourDescription());
    }

    public function testSetGetColourName()
    {
        $this->model->setColourName(1);
        $this->assertEquals(1, $this->model->getColourName());
    }

    public function testSetGetLanguageId()
    {
        $this->model->setLanguageId(1);
        $this->assertEquals(1, $this->model->getLanguageId());
    }
}
