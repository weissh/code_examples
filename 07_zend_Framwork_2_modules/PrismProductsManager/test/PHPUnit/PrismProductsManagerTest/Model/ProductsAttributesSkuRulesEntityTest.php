<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsAttributesSkuRulesEntity;
use Zend\Db\Sql\Expression;

/**
 * @author haniw
 */
class ProductsAttributesSkuRulesEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsAttributesSkuRulesEntity */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new ProductsAttributesSkuRulesEntity();
    }

    public function testsetgetProductAttributeSkuRuleId()
    {
        $this->entity->setProductAttributeSkuRuleId(1);
        $this->assertEquals(1, $this->entity->getProductAttributeSkuRuleId());
    }

    public function testsetgetProductAttributeId()
    {
        $this->entity->setProductAttributeId(1);
        $this->assertEquals(1, $this->entity->getProductAttributeId());
    }

    public function testsetgetSkuRuleId()
    {
        $this->entity->setSkuRuleId(1);
        $this->assertEquals(1, $this->entity->getSkuRuleId());
    }

    public function testsetgetSkuRuleTable()
    {
        $this->entity->setSkuRuleTable('SKU_Rules');
        $this->assertEquals('SKU_Rules', $this->entity->getSkuRuleTable());
    }

    public function testSetGetCreated()
    {
        $this->assertInstanceOf(Expression::class, $this->entity->getCreated());
        $this->entity->setCreated(new Expression("NOW()"));

        $this->assertInstanceOf(Expression::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->assertInstanceOf(Expression::class, $this->entity->getModified());
        $this->entity->setModified(new Expression("NOW()"));

        $this->assertInstanceOf(Expression::class, $this->entity->getModified());
    }
}
