<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\AttributesMerchantsEntity;

/**
 * @author haniw
 */
class AttributesMerchantsEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var AttributesMerchantsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new AttributesMerchantsEntity();
    }

    public function testSetGetMerchantAttributeId()
    {
        $this->model->setMerchantAttributeId(1);
        $this->assertEquals(1, $this->model->getMerchantAttributeId());
    }

    public function testSetGetAttributeId()
    {
        $this->model->setAttributeId(1);
        $this->assertEquals(1, $this->model->getAttributeId());
    }

    public function testSetGetAttributeGroupId()
    {
        $this->model->setAttributeGroupId(1);
        $this->assertEquals(1, $this->model->getAttributeGroupId());
    }
}
