<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\SkuRulesEntity;

/**
 * @author haniw
 */
class SkuRulesEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var SkuRulesEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new SkuRulesEntity();
    }

    public function testSetGetisCommonAttribute()
    {
        // before set, bring default value
        $this->assertEquals(0, $this->model->getIsCommonAttribute());

        // after set, bring setted value
        $this->model->setIsCommonAttribute(1);
        $this->assertEquals(1, $this->model->getIsCommonAttribute());
    }

    public function testSetGetSkuRuleId()
    {
        $this->model->setSkuRuleId(1);
        $this->assertEquals(1, $this->model->getSkuRuleId());
    }

    public function testSetGetMerchantcategoryId()
    {
        $this->model->setMerchantcategoryId(1);
        $this->assertEquals(1, $this->model->getMerchantcategoryId());
    }

    public function testSetGetPosition()
    {
        $this->model->setPosition(1);
        $this->assertEquals(1, $this->model->getPosition());
    }

    public function testSetGetMerchantAttributeGroupId()
    {
        $this->model->setMerchantAttributeGroupId(1);
        $this->assertEquals(1, $this->model->getMerchantAttributeGroupId());
    }
}
