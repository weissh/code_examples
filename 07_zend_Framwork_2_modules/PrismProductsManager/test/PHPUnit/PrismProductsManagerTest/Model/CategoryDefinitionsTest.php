<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\CategoryDefinitions;

/**
 * @author haniw
 */
class CategoryDefinitionsTest extends PHPUnit_Framework_TestCase
{
    /** @var CategoryDefinitions */
    protected $model;

    protected function setUp()
    {
        $this->model = new CategoryDefinitions();
    }

    public function testModelInitialState()
    {
       $this->assertNull($this->model->categoryDefinitionId);
       $this->assertNull($this->model->categoryId);
       $this->assertNull($this->model->languageId);
       $this->assertNull($this->model->versionId);
       $this->assertNull($this->model->versionDescription);
       $this->assertNull($this->model->categoryName);
       $this->assertNull($this->model->urlName);
       $this->assertNull($this->model->metaTitle);
       $this->assertNull($this->model->metaDescription);
       $this->assertNull($this->model->metaKeyword);
       $this->assertNull($this->model->shortDescription);
       $this->assertNull($this->model->longDescription);
       $this->assertNull($this->model->status);
       $this->assertNull($this->model->makeLive);
       $this->assertNull($this->model->created);
       $this->assertNull($this->model->modified);
       $this->assertNull($this->model->locked);
   }

   public function testExchangeArraySetsPropertiesCorrectly()
   {
        $data  = [
            'categoryDefinitionId' => 1,
            'categoryId' => 1,
            'languageId' => 1,
            'versionId' => 1,
            'versionDescription' => 'foo',
            'categoryName' => 'foo',
            'urlName' => 'foo',
            'metaTitle' => 'foo',
            'metaDescription' => 'foo',
            'metaKeyword' => 'foo',
            'shortDescription' => 'foo',
            'longDescription' => 'foo',
            'status' => 'INACTIVE',
            'makeLive' => '2015-01-01',
            'created' => '2015-01-01',
            'modified' => '2015-01-01',
            'locked' => 1,
        ];
        $this->model->exchangeArray($data);

        $this->assertEquals($data['categoryDefinitionId'], $this->model->categoryDefinitionId);
        $this->assertEquals($data['categoryId'], $this->model->categoryId);
        $this->assertEquals($data['languageId'], $this->model->languageId);
        $this->assertEquals($data['versionId'], $this->model->versionId);
        $this->assertEquals($data['versionDescription'], $this->model->versionDescription);
        $this->assertEquals($data['categoryName'], $this->model->categoryName);
        $this->assertEquals($data['urlName'], $this->model->urlName);
        $this->assertEquals($data['metaTitle'], $this->model->metaTitle);
        $this->assertEquals($data['metaDescription'], $this->model->metaDescription);
        $this->assertEquals($data['metaKeyword'], $this->model->metaKeyword);
        $this->assertEquals($data['shortDescription'], $this->model->shortDescription);
        $this->assertEquals($data['longDescription'], $this->model->longDescription);
        $this->assertEquals($data['status'], $this->model->status);
        $this->assertEquals($data['makeLive'], $this->model->makeLive);
        $this->assertEquals($data['created'], $this->model->created);
        $this->assertEquals($data['modified'], $this->model->modified);
        $this->assertEquals($data['locked'], $this->model->locked);
   }

   public function testGetArrayCopyReturnsAnArrayWithPropertyValues()
   {
       $data  = [
           'categoryDefinitionId' => 1,
           'categoryId' => 1,
           'languageId' => 1,
           'versionId' => 1,
           'versionDescription' => 'foo',
           'categoryName' => 'foo',
           'urlName' => 'foo',
           'metaTitle' => 'foo',
           'metaDescription' => 'foo',
           'metaKeyword' => 'foo',
           'shortDescription' => 'foo',
           'longDescription' => 'foo',
           'status' => 'INACTIVE',
           'makeLive' => '2015-01-01',
           'created' => '2015-01-01',
           'modified' => '2015-01-01',
           'locked' => 1,
       ];
       $this->model->exchangeArray($data);
       $copyArray = $this->model->getArrayCopy();

       $this->assertEquals($data['categoryDefinitionId'], $copyArray['categoryDefinitionId']);
       $this->assertEquals($data['categoryId'], $copyArray['categoryId']);
       $this->assertEquals($data['languageId'], $copyArray['languageId']);
       $this->assertEquals($data['versionId'], $copyArray['versionId']);
       $this->assertEquals($data['versionDescription'], $copyArray['versionDescription']);
       $this->assertEquals($data['categoryName'], $copyArray['categoryName']);
       $this->assertEquals($data['urlName'], $copyArray['urlName']);
       $this->assertEquals($data['metaTitle'], $copyArray['metaTitle']);
       $this->assertEquals($data['metaDescription'], $copyArray['metaDescription']);
       $this->assertEquals($data['metaKeyword'], $copyArray['metaKeyword']);
       $this->assertEquals($data['shortDescription'], $copyArray['shortDescription']);
       $this->assertEquals($data['longDescription'], $copyArray['longDescription']);
       $this->assertEquals($data['status'], $copyArray['status']);
       $this->assertEquals($data['makeLive'], $copyArray['makeLive']);
       $this->assertEquals($data['created'], $copyArray['created']);
       $this->assertEquals($data['modified'], $copyArray['modified']);
       $this->assertEquals($data['locked'], $copyArray['locked']);
    }
}
