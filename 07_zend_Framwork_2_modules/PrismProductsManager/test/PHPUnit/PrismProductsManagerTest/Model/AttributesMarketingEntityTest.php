<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\AttributesMarketingEntity;

/**
 * @author haniw
 */
class AttributesMarketingEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var AttributesMarketingEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new AttributesMarketingEntity();
    }

    public function testSetGetAttributeGroupId()
    {
        $this->model->setAttributeGroupId(1);
        $this->assertEquals(1, $this->model->getAttributeGroupId());
    }
    public function testSetGetAttributeId()
    {
        $this->model->setAttributeId(1);
        $this->assertEquals(1, $this->model->getAttributeId());
    }
}
