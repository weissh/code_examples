<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsMerchantCategoriesEntity;

/**
 * @author haniw
 */
class ProductsMerchantCategoriesEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsMerchantCategoriesEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductsMerchantCategoriesEntity();
    }

    public function testSetGetParentId()
    {
        $this->model->setParentId(1);
        $this->assertEquals(1, $this->model->getParentId());
    }

    public function testSetGetProductMerchantcategoryId()
    {
        $this->model->setProductMerchantcategoryId(1);
        $this->assertEquals(1, $this->model->getProductMerchantcategoryId());
    }

    public function testSetGetStatus()
    {
        $this->model->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->model->getStatus());
    }

    public function testSetGetCreated()
    {
        $created = date('Y-m-d H:i:s');
        $this->model->setCreated($created);
        $this->assertEquals($created, $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        $modified = date('Y-m-d H:i:s');
        $this->model->setModified($modified);
        $this->assertEquals($modified, $this->model->getModified());
    }
}
