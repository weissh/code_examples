<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\CategoryDefinitionsTable;

/**
 * @author haniw
 */
class CategoryDefinitionsTableTest extends PHPUnit_Framework_TestCase
{
    /** @var CategoryDefinitionsTable */
    private $model;

    protected function setUp()
    {
        $this->model = new CategoryDefinitionsTable();
    }

    public function testSetGetTableGateway()
    {
        $this->model->setTableGateway(
            $this->getMockBuilder('Zend\Db\TableGateway\TableGateway')
                  ->disableOriginalConstructor()
                  ->getMock()
        );

        $this->assertInstanceOf('Zend\Db\TableGateway\TableGateway', $this->model->getTableGateway());
    }

}
