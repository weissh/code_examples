<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsSpectrumEntity;
use Zend\Db\Sql\Expression;

/**
 * @author haniw
 */
class ProductsSpectrumEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var ChannelsEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new ProductsSpectrumEntity();
    }

    public function testSetGetProductId()
    {
        $this->model->setProductId(1);
        $this->assertEquals(1, $this->model->getProductId());
    }

    public function testDeprecatedSetGeSpectrumId()
    {
        $this->model->setSpectrumId(1);
        $this->assertEquals(1, $this->model->geSpectrumId());
    }

    public function testSetGetSpectrumId()
    {
        $this->model->setSpectrumId(1);
        $this->assertEquals(1, $this->model->getSpectrumId());
    }

    public function testSetGeProductSpectrumId()
    {
        $this->model->setProductSpectrumId(1);
        $this->assertEquals(1, $this->model->geProductSpectrumId());
    }

    public function testSetGetProductIdFrom()
    {
        $this->model->setProductIdFrom('NOT_PRODUCTS');
        $this->assertEquals('NOT_PRODUCTS', $this->model->getProductIdFrom());
    }

    public function testSetGetCreated()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());

        $this->model->setCreated(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        // default from constructor
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());

        $this->model->setModified(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());
    }

    public function testSetGetStyle()
    {
        $this->model->setStyle('london');
        $this->assertEquals('london', $this->model->getStyle());
    }

    public function testSetCrossBorder()
    {
        $this->model->setCrossBorder('#');
        $this->assertEquals('#', $this->model->getCrossBorder());
    }
}
