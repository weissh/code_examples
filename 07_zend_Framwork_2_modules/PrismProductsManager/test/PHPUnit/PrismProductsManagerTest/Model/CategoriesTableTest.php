<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\CategoriesTable;

/**
 * @author haniw
 */
class CategoriesTableTest extends PHPUnit_Framework_TestCase
{
    /** @var CategoriesTable */
    private $model;

    /** @var \Zend\Db\TableGateway\TableGateway */
    private $tableGateway;

    protected function setUp()
    {
        $this->tableGateway = $this->getMockBuilder('Zend\Db\TableGateway\TableGateway')
                                    ->disableOriginalConstructor()
                                    ->getMock();
        $this->model = new CategoriesTable($this->tableGateway);
    }

    public function testFetchAll()
    {
        $rowset = $this->getMockBuilder('Zend\Db\ResultSet\ResultSet')
                                    ->disableOriginalConstructor()
                                    ->getMock();
        $this->tableGateway->expects($this->once())
                           ->method('select')
                           ->willReturn($rowset);

        $this->assertInstanceOf('Zend\Db\ResultSet\ResultSet', $this->model->fetchAll());
    }

    public function provideResults()
    {
        return [
            [new \stdClass()],
            [false],
        ];
    }

    /**
     * @dataProvider provideResults
     */
    public function testGetCategories($result)
    {
        $rowset = $this->getMockBuilder('Zend\Db\ResultSet\ResultSet')
                                    ->disableOriginalConstructor()
                                    ->getMock();
        $rowset->expects($this->once())
               ->method('current')
               ->willReturn($result);

        $this->tableGateway->expects($this->once())
                           ->method('select')
                           ->with(['categoryId' => 1])
                           ->willReturn($rowset);
        if (! $result) {
            $this->setExpectedException('\Exception', 'Could not find row 1');
        }
        $this->model->getCategories(1);
    }

    public function testDeleteCategories()
    {
        $this->tableGateway->expects($this->once())
                           ->method('delete')
                           ->with(['categoryId' => 1]);

        $this->model->deleteCategories(1);
    }

}
