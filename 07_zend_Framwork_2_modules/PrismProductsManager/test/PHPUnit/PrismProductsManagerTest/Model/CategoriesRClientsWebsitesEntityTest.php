<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\CategoriesRClientsWebsitesEntity;

/**
 * @author haniw
 */
class CategoriesRClientsWebsitesEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var CategoriesRClientsWebsitesEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new CategoriesRClientsWebsitesEntity();
    }

    public function testSetGet()
    {
        $this->model->setCategoryId(1);
        $this->model->setCategoryWebsiteId(1);
        $this->model->setWebsiteId(1);

        $this->assertEquals(1, $this->model->getCategoryId());
        $this->assertEquals(1, $this->model->getCategoryWebsiteId());
        $this->assertEquals(1, $this->model->getWebsiteId());
    }
}
