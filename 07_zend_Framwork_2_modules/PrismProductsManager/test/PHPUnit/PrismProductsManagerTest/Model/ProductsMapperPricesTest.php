<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsMapperPrices;
use PrismProductsManagerTest\ZendDbFixture\FixtureManager;
use Zend\Db\Sql\Sql;
use Zend\Stdlib\Hydrator\ClassMethods;

class ProductsMapperPricesTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsMapperPrices */
    private $model;

    /** @var \PrismProductsmanagerTest\ZendDbFixture\FixtureManager */
    private $fixtureManager;

    /** @var Sql */
    private $sql;

    /** @var ClassMethods */
    private $hydrator;

    /** @var \Zend\Db\Adapter\Adapter */
    private $dbAdapter;

    private function init()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');

        $this->fixtureManager->execute('PRODUCTS_Prices-up.sql');
    }

    protected function setUp()
    {
        $this->init();

        $this->sql       = new Sql($this->fixtureManager->getDbAdapter());
        $this->hydrator  = new ClassMethods();
        $this->dbAdapter = $this->fixtureManager->getDbAdapter();

        $this->model = new ProductsMapperPrices(
            $this->sql,
            $this->hydrator,
            $this->dbAdapter
        );
    }

    public function test_deactivateProductPrices()
    {
        $this->assertEquals(1, $this->model->_deactivateProductPrices(1, 1, 'PRODUCTS'));

        $this->tearDown();
        $this->init();
    }

    /** pending as SQL error: - Field 'created' doesn't have a default value
    public function testInsertNewVariantPrices()
    {
        $this->assertEquals(1, $this->model->insertNewVariantPrices(1, 1, ['regionId' => 1]));

        $this->tearDown();
        $this->init();
    }

    public function testUpdateProductPricesByPriceID()
    {
        $this->model->updateProductPricesByPriceID(1, []);

        $this->tearDown();
        $this->init();
    }
    **/

    public function testDeactivatePricesByProductId()
    {
        $this->assertTrue($this->model->deactivatePricesByProductId(1));

        $this->tearDown();
        $this->init();
    }

    protected function tearDown()
    {
        $this->fixtureManager->execute('PRODUCTS_Prices-down.sql');
    }
}
