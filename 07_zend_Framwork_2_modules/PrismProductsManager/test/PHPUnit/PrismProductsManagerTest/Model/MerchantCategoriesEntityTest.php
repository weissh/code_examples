<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\MerchantCategoriesEntity;
use Zend\Db\Sql\Expression;

/**
 * @author haniw
 */
class MerchantCategoriesEntityTest extends PHPUnit_Framework_TestCase
{
    /** @var MerchantCategoriesEntity */
    protected $model;

    protected function setUp()
    {
        $this->model = new MerchantCategoriesEntity();
    }

    public function testSetGetStatus()
    {
        $this->model->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->model->getStatus());
    }

    public function testSetGetParentId()
    {
        $this->model->setParentId(1);
        $this->assertEquals(1, $this->model->getParentId());
    }

    public function testSetGetCreated()
    {
        $this->model->setCreated(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getCreated());
    }

    public function testSetGetModified()
    {
        $this->model->setModified(new Expression('NOW'));
        $this->assertInstanceOf('Zend\Db\Sql\Expression', $this->model->getModified());
    }
}
