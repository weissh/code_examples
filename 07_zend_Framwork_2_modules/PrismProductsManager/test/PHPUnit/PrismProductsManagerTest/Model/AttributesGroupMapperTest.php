<?php

namespace PrismProductsManagerTest\Model;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\AttributesGroupMapper;
use PrismProductsManager\Model\AttributesGroupEntity;
use PrismProductsManager\Model\AttributesGroupDefinitionsEntity;
use PrismProductsManager\Model\AttributesMarketingEntity;
use PrismProductsManager\Model\AttributesMarketingDefinitionsEntity;
use PrismProductsManager\Model\AttributesMerchantsEntity;
use PrismProductsManagerTest\ZendDbFixture\FixtureManager;

class AttributesGroupMapperTest extends PHPUnit_Framework_TestCase
{
    /** @var TaxMapper */
    private $model;

    /** @var \PrismProductsmanagerTest\ZendDbFixture\FixtureManager */
    private $fixtureManager;

    private function init()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');

        $this->fixtureManager->execute('ATTRIBUTES_Groups-up.sql');
        $this->fixtureManager->execute('ATTRIBUTES-up.sql');
        $this->fixtureManager->execute('ATTRIBUTES_Definitions-up.sql');
        $this->fixtureManager->execute('ATTRIBUTES_Groups_Validations-up.sql');
        $this->fixtureManager->execute('ATTRIBUTES_Merchants-up.sql');
        $this->fixtureManager->execute('ATTRIBUTES_Group_Definitions-up.sql');
        $this->fixtureManager->execute('PRODUCTS_R_ATTRIBUTES_R_SKU_Rules-up.sql');
        $this->fixtureManager->execute('PRODUCTS_ATTRIBUTES_Combination-up.sql');
        $this->fixtureManager->execute('PRODUCTS_ATTRIBUTES_Combination_Definitions-up.sql');
        $this->fixtureManager->execute('PRODUCTS_R_CATEGORIES-up.sql');
        $this->fixtureManager->execute('PRODUCTS_R_ATTRIBUTES-up.sql');
        $this->fixtureManager->execute('CATEGORIES_R_ATTRIBUTES-up.sql');

        $this->fixtureManager->execute('COLOURS_Groups-up.sql');
        $this->fixtureManager->execute('COLOURS_Groups_Definitions-up.sql');
        $this->fixtureManager->execute('COLOURS-up.sql');
        $this->fixtureManager->execute('COLOURS_Definitions-up.sql');
        $this->fixtureManager->execute('COLOURS_R_GROUPS-up.sql');
    }

    protected function setUp()
    {
        $this->init();

        $this->rsMapper = $this->getMockBuilder('PrismMediaManager\Service\RSMapper')
                               ->disableOriginalConstructor()
                               ->getMock();
        $this->model = new AttributesGroupMapper(
            new AttributesGroupEntity(),
            new AttributesGroupDefinitionsEntity(),
            new AttributesMarketingEntity(),
            new AttributesMarketingDefinitionsEntity(),
            new AttributesMerchantsEntity(),
            $this->fixtureManager->getDbAdapter(),
            [
                'default-language' => 1,
                'current-language' => 1,
                'site-languages' => [1],
            ],
            new \PHPExcel(),
            $this->rsMapper
        );
    }

    public function provideSearchOptions()
    {
        return [
            [null, null, null],
            ['foo', null, null],
            ['foo', 'agd.modified', null],
            ['foo', 'agd.modified', 'asc'],
        ];
    }

    /**
     * @dataProvider provideSearchOptions
     */
    public function testFetchAllCoreAttributes($search, $orderBy, $orderByOption)
    {
        $this->assertInternalType('array', $this->model->fetchAllCoreAttributes($search, $orderBy, $orderByOption));
    }

    /**
     * @dataProvider provideSearchOptions
     */
    public function testFetchAllMarketingAttributes($search, $orderBy, $orderByOption)
    {
        $this->assertInternalType('array', $this->model->fetchAllMarketingAttributes($search, $orderBy, $orderByOption));
    }

    public function provideFetchAllParams()
    {
        return [
            [true, []],
            [false, []],
            [true, ['name']],
            [false, ['name']],
        ];
    }

    /**
     * @dataProvider provideFetchAllParams
     */
    public function testFetchAll($asArray)
    {
        if ($asArray) {
            $this->assertInternalType('array', $this->model->fetchAll(true));
        } else {
            $this->assertInternalType('object', $this->model->fetchAll(false));
        }
    }

    public function testGetAllProductsWithThisAttribute()
    {
        $this->assertInternalType('array', $this->model->getAllProductsWithThisAttribute(1));
    }

    /**
     * @dataProvider provideSearchOptions
     */
    public function testFetchAllMerchantsAttributes($search = null, $orderBy, $orderbyOption)
    {
        $this->assertInternalType('array', $this->model->fetchAllMerchantsAttributes($search = null, $orderBy, $orderbyOption));
    }

    public function testGroupById()
    {
        $this->assertInternalType('array', $this->model->getGroupById(1));
    }

    public function testFetchAllAttributeGroups()
    {
        $this->assertInternalType('array', $this->model->fetchAllAttributeGroups(1));
    }

    public function testGetAttributeValues()
    {
        $this->assertInternalType('array', $this->model->getAttributeValues(1, 1));
    }

    public function testFetchTopMerchantGroups()
    {
        $this->assertInternalType('array', $this->model->fetchTopMerchantGroups());
    }

    public function testFetchAllAttributeGroupsForMarketingCategory()
    {
        $this->assertInternalType('array', $this->model->fetchAllAttributeGroupsForMarketingCategory(1));
    }

    public function provideDeleteData()
    {
        return [
            [['delete' => 'group', 'id' => 1]],
            [['delete' => 'attribute', 'id' => 1]]
        ];
    }

    /**
     * @dataProvider provideDeleteData
     */
    public function testDelete($data)
    {
        $this->model->delete($data);

        $this->tearDown();
        $this->init();
    }

    public function provideIsVariant()
    {
        return [
            [0],
            [1],
        ];
    }

    /**
     * @dataProvider provideIsVariant
     */
    public function testFetchCoreAttributesForProduct($isVariant)
    {
        $this->assertInternalType('array', $this->model->fetchCoreAttributesForProduct(1, $isVariant, 1));
    }

    public function provideGroupCode()
    {
        return [
            ['COLOR'],
            ['TEST_GROUP'],
        ];
    }

    /**
     * @dataProvider provideGroupCode
     */
    public function testGetMerchantAttributeGroupByGroupCode($groupCode)
    {
        if ($groupCode !== 'TEST_GROUP') {
            $this->assertInternalType('array',$this->model->getMerchantAttributeGroupByGroupCode($groupCode));
        } else {
            $this->assertFalse($this->model->getMerchantAttributeGroupByGroupCode($groupCode));
        }
    }

    public function testGetGroupSourceTable()
    {
        $this->assertInternalType('array', $this->model->getGroupSourceTable(1));
    }

    public function provideParams()
    {
        return [
            [['checked' => 1, 'id' => 1]],
            [['checked' => 0, 'id' => 1]],
        ];
    }

    /**
     * @dataProvider provideParams
     */
    public function testEnableColours($params)
    {
        $this->model->enableColours($params);

        $this->tearDown();
        $this->init();
    }

    /**
     * @dataProvider provideParams
     */
    public function tetSetFilter($params)
    {
        $this->model->setFilter($params);

        $this->tearDown();
        $this->init();
    }

    public function testActivateAttributeValue()
    {
        $this->model->activateAttributeValue(1);

        $this->tearDown();
        $this->init();
    }

    /**
     * @dataProvider provideIsVariant
     */
    public function testFetchExistingAttributeGroupsIdsForProduct($isVariant)
    {
        $this->assertInternalType('array', $this->model->fetchExistingAttributeGroupsIdsForProduct(1, $isVariant));
    }

    protected function tearDown()
    {
        $this->fixtureManager->execute('COLOURS_R_GROUPS-down.sql');
        $this->fixtureManager->execute('COLOURS_Definitions-down.sql');
        $this->fixtureManager->execute('COLOURS-down.sql');
        $this->fixtureManager->execute('COLOURS_Groups_Definitions-down.sql');
        $this->fixtureManager->execute('COLOURS_Groups-down.sql');

        $this->fixtureManager->execute('CATEGORIES_R_ATTRIBUTES-down.sql');
        $this->fixtureManager->execute('PRODUCTS_R_ATTRIBUTES-down.sql');
        $this->fixtureManager->execute('PRODUCTS_R_CATEGORIES-down.sql');
        $this->fixtureManager->execute('PRODUCTS_ATTRIBUTES_Combination_Definitions-down.sql');
        $this->fixtureManager->execute('PRODUCTS_ATTRIBUTES_Combination-down.sql');
        $this->fixtureManager->execute('PRODUCTS_R_ATTRIBUTES_R_SKU_Rules-down.sql');
        $this->fixtureManager->execute('ATTRIBUTES_Group_Definitions-down.sql');
        $this->fixtureManager->execute('ATTRIBUTES_Merchants-down.sql');
        $this->fixtureManager->execute('ATTRIBUTES_Groups_Validations-down.sql');
        $this->fixtureManager->execute('ATTRIBUTES_Definitions-down.sql');
        $this->fixtureManager->execute('ATTRIBUTES-down.sql');
        $this->fixtureManager->execute('ATTRIBUTES_Groups-down.sql');
    }
}
