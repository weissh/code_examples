<?php

namespace PrismProductsManagerTest\Service;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity;
use PrismProductsManager\Entity\Repository;
use PrismProductsManager\Model;
use PrismProductsManager\Service;
use PrismProductsManager\Service\ScheduleService;
use PrismQueueManager\Service\QueueService;

/**
 * @author haniw
 */
class ScheduleServiceTest extends PHPUnit_Framework_TestCase
{
    /** @var CommonAttributesService */
    protected $service;

    protected function setUp()
    {
        $this->siteConfig = [
            'languages' => [
                'current-language' => 1,
            ],
        ];

        $this->doctrineEM = $this->prophesize('Doctrine\ORM\EntityManager');
        $this->productsMapper = $this->prophesize(Model\ProductsMapper::class);
        $this->attributesGroupMapper = $this->prophesize(Model\AttributesGroupMapper::class);
        $this->schedulesRepository = $this->prophesize(Repository\SchedulesRepository::class);

        $this->schedulePricesRepository = $this->prophesize('Doctrine\Common\Persistence\ObjectRepository');
        $this->scheduleTrailRepository = $this->prophesize('Doctrine\Common\Persistence\ObjectRepository');
        $this->scheduleTypesRepository = $this->prophesize('Doctrine\Common\Persistence\ObjectRepository');

        $this->territoriesRepository = $this->prophesize('Doctrine\Common\Persistence\ObjectRepository');
        $this->currencyRepository = $this->prophesize('Doctrine\Common\Persistence\ObjectRepository');

        $this->inputService = $this->prophesize(Service\InputService::class);
        $this->commonAttributesService = $this->prophesize(Service\CommonAttributesService::class);
        $this->queueService = $this->prophesize(QueueService::class);

        $dbAdapter = $this->prophesize(\Zend\Db\Adapter\Adapter::class);

        $this->service = new ScheduleService(
            $this->doctrineEM->reveal(),
            $this->productsMapper->reveal(),
            $this->attributesGroupMapper->reveal(),
            $this->schedulesRepository->reveal(),
            $this->schedulePricesRepository->reveal(),
            $this->scheduleTrailRepository->reveal(),
            $this->scheduleTypesRepository->reveal(),
            $this->territoriesRepository->reveal(),
            $this->currencyRepository->reveal(),
            new Entity\Schedules,
            new Entity\SchedulePrices,
            new Entity\ScheduleTrail,
            new Entity\ScheduleTypes,
            new Entity\ScheduleAttributes,
            $this->inputService->reveal(),
            $this->commonAttributesService->reveal(),
            $this->queueService->reveal(),
            $dbAdapter->reveal()
        );
    }

    public function testGetWasPriceValidAtDate()
    {
        $this->schedulesRepository->getPriceSchedulesForWasPriceUpToDate('2016-01-01', 1, 'PRODUCTS', 1, 1)
                                  ->shouldBeCalled()  ;
        $this->service->getWasPriceValidAtDate('2016-01-01', 1, 'PRODUCTS', 1, 1);
    }
}
