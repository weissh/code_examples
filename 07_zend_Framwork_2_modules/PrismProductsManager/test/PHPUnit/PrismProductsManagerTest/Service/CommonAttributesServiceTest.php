<?php

namespace PrismProductsManagerTest\Service;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity;
use PrismProductsManager\Entity\Repository;
use PrismProductsManager\Model;
use PrismProductsManager\Service\CommonAttributesService;
use Prophecy\Argument;
use Zend\Db\Sql\Sql;

/**
 * @author haniw
 */
class CommonAttributesServiceTest extends PHPUnit_Framework_TestCase
{
    /** @var CommonAttributesService */
    protected $service;

    protected function setUp()
    {
        $this->siteConfig = [
            'languages' => [
                'current-language' => 1,
            ],
        ];

        $this->doctrineEM = $this->prophesize('Doctrine\ORM\EntityManager');

        $this->commonAttributeMapper = $this->prophesize(Model\CommonAttributesMapper::class);
        $this->commonAttributesEntityRepository = $this->prophesize(Repository\CommonAttributesRepository::class);
        $this->commonAttributesViewTypeEntityRepository = $this->prophesize('Doctrine\Common\Persistence\ObjectRepository');
        $this->commonAttributesValuesRepository = $this->prophesize('Doctrine\Common\Persistence\ObjectRepository');
        $this->commonAttributesValuesGuiRepresentationRepository = $this->prophesize('Doctrine\Common\Persistence\ObjectRepository');
        $this->commonAttributesValuesEquivalenceRepository = $this->prophesize('Doctrine\Common\Persistence\ObjectRepository');
        $this->commonAttributesValuesChainRepository = $this->prophesize('Doctrine\Common\Persistence\ObjectRepository');
        $this->commonAttributesDefinitionsRepository = $this->prophesize('Doctrine\Common\Persistence\ObjectRepository');
        $this->territoriesRepository = $this->prophesize('Doctrine\Common\Persistence\ObjectRepository');

        $dbAdapter = $this->prophesize(\Zend\Db\Adapter\Adapter::class);
        $this->sql = new Sql($dbAdapter->reveal());

        $this->service = new CommonAttributesService(
            $this->siteConfig,
            $this->doctrineEM->reveal(),
            $this->commonAttributeMapper->reveal(),
            $this->commonAttributesEntityRepository->reveal(),
            $this->commonAttributesViewTypeEntityRepository->reveal(),
            new Entity\CommonAttributes,
            new Entity\CommonAttributesDefinitions,
            new Entity\CommonAttributesGuiRepresentation,
            new Entity\CommonAttributesValues,
            new Entity\CommonAttributesValuesDefinitions,
            new Entity\CommonAttributesValuesEquivalence,
            new Entity\CommonAttributesValuesGuiRepresentation,
            new Entity\CommonAttributesMapping,
            $this->commonAttributesValuesRepository->reveal(),
            $this->commonAttributesValuesGuiRepresentationRepository->reveal(),
            $this->commonAttributesValuesEquivalenceRepository->reveal(),
            new Entity\CommonAttributesValuesChain,
            $this->commonAttributesValuesChainRepository->reveal(),
            $this->commonAttributesDefinitionsRepository->reveal(),
            $this->territoriesRepository->reveal(),
            $this->sql
        );
    }

    public function testGetAllCommonAttributes()
    {
        $this->commonAttributesEntityRepository->getAllCommonAttributes()->shouldBeCalled();
        $this->service->getAllCommonAttributes();
    }

    public function testGetCommonAttributeByName()
    {
        $this->commonAttributesDefinitionsRepository->findOneBy([
            'description' => 'desc', 'languageid' => 1
        ])->shouldBeCalled();

        $this->service->getCommonAttributeByName('desc');
    }

    public function testGetCommonAttributeValueBy()
    {
        $this->commonAttributesValuesRepository->findOneBy([])->shouldBeCalled();
        $this->service->getCommonAttributeValueBy([]);
    }

    public function testRemoveChain()
    {
        $entity = new Entity\CommonAttributesValuesChain();
        $this->commonAttributesValuesChainRepository->findOneBy([
            'sourcevalueid' => 1, 'destinationvalueid' => 1
        ])->willReturn($entity)
          ->shouldBeCalled();

        $this->doctrineEM->remove(Argument::type(Entity\CommonAttributesValuesChain::class))
                         ->shouldBeCalled();
        $this->doctrineEM->flush()
                         ->shouldBeCalled();

        $this->assertTrue($this->service->removeChain(1, 1));
    }

    public function testRemoveChainGotException()
    {
        $entity = new Entity\CommonAttributesValuesChain();
        $this->commonAttributesValuesChainRepository->findOneBy([
            'sourcevalueid' => 1, 'destinationvalueid' => 1
        ])->willReturn($entity)
            ->shouldBeCalled();

        $this->doctrineEM->remove(Argument::type(Entity\CommonAttributesValuesChain::class))
            ->shouldBeCalled();
        $this->doctrineEM->flush()
                ->willThrow(new \Exception())
            ->shouldBeCalled();

        $this->assertFalse($this->service->removeChain(1, 1));
    }

    public function testGetCommonAttribute()
    {
        $this->commonAttributesEntityRepository->findOneBy(['commonattributeid' => 1])
                                               ->willReturn(new Entity\CommonAttributes())
                                               ->shouldBeCalled()
                                ;

        $this->assertInstanceOf(Entity\CommonAttributes::class, $this->service->getCommonAttribute(1));
    }

    public function testDoctrineFlushSucceed()
    {
        $this->doctrineEM->flush()->shouldBeCalled();
        $this->assertTrue($this->service->doctrineFlush());
    }

    public function testDoctrineFlushWithException()
    {
        $this->doctrineEM->flush()->willThrow(new \Exception());
        $this->assertFalse($this->service->doctrineFlush());
    }

    public function testGetCommonAttributeValuesByIdGetArrayOfCommonAttributeValues()
    {
        $result = [
            new Entity\CommonAttributesValues(),
            new Entity\CommonAttributesValues(),
        ];

        $this->commonAttributesValuesRepository->findBy([
            'commonattributeid' => 1
        ])->willReturn($result)->shouldBeCalled();

        $this->assertEquals($result, $this->service->getCommonAttributeValuesById(1));
    }

    public function testGetEquivalenceByGroupGetArrayOfCommonAttributesEquivalency()
    {
        $result = [
            new Entity\CommonAttributesValuesEquivalence(),
            new Entity\CommonAttributesValuesEquivalence(),
        ];

        $this->commonAttributesValuesEquivalenceRepository->findBy([
            'uniquegroupid' => 1
        ])->willReturn($result)->shouldBeCalled();

        $this->assertEquals($result, $this->service->getEquivalenceByGroup(1));
    }
}
