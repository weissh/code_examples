<?php

namespace PrismProductsManagerTest\Service;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Service\ProductsUpdateService;

/**
 * @author haniw
 */
class ProductsUpdateServiceTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsUpdateService */
    protected $service;

    protected function setUp()
    {
        $this->queueService = $this->getMockBuilder('PrismQueueManager\Service\QueueService')
                             ->disableOriginalConstructor()
                             ->getMock();
        $this->siteConfig = [
            'siteConfig' => [
                'websites' => [
                    'clear-cache' => [
                        '1' => 'http://bar.os.dev/',
                        '2' => 'http://foo.os.dev/',
                    ],
                ],
            ],
        ];

        $this->service = new ProductsUpdateService(
            $this->queueService,
            $this->siteConfig,
            [
                'clientConfig' => [
                    'settings' => [
                        'newSpectrumAndSipSynch' => false,
                    ]
                ],
            ]
        );
    }

    public function provideVariantIds()
    {
        return [
            ['1_1'],
            [false],
        ];
    }

    /**
     * @dataProvider provideVariantIds
     */
    public function testUpdateProductAndVariantId($variantId)
    {
        $this->queueService->expects($this->once())
                           ->method('addSpectrumEntryToQueue')
                           ->with(1);
        $this->queueService->expects($this->once())
                           ->method('addIndexEntryToQueue')
                           ->with('1_' . $variantId, 1);

        $this->service->updateProductAndVariantId(1, $variantId);
    }

    public function testUpdateProductId()
    {
        $this->queueService->expects($this->once())
                           ->method('addSpectrumEntryToQueue')
                           ->with(1);
        $this->queueService->expects($this->once())
                           ->method('addIndexEntryToQueue')
                           ->with('1_0', 1);

        $this->service->updateProductId(1);
    }
}
