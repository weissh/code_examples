<?php

namespace PrismProductsManagerTest\Service;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity;
use PrismProductsManager\Entity\Repository;
use PrismProductsManager\Model;
use PrismProductsManager\Service;
use PrismProductsManager\Service\InputService;
use Zend\Db\Sql\Sql;

/**
 * @author haniw
 */
class InputServiceTest extends PHPUnit_Framework_TestCase
{
    /** @var CommonAttributesService */
    protected $service;

    protected function setUp()
    {
        $this->dbAdapter = $this->prophesize(\Zend\Db\Adapter\Adapter::class);
        $this->doctrineEM = $this->prophesize('Doctrine\ORM\EntityManager');
        $this->commonAttributesMapper = $this->prophesize(Model\CommonAttributesMapper::class);
        $this->attributesGroupMapper = $this->prophesize(Model\AttributesGroupMapper::class);

        $this->service = new InputService(
            $this->dbAdapter->reveal(),
            [
                'languages' => [
                    'current-language' => 1,
                ],
            ],
            $this->doctrineEM->reveal(),
            new Sql($this->dbAdapter->reveal()),
            $this->commonAttributesMapper->reveal(),
            $this->attributesGroupMapper->reveal()
        );
    }

    public function testGetAllPages()
    {
        $result = [
            new Entity\InputPages(),
            new Entity\InputPages()
        ];
        $inputPagesRepository = $this->prophesize(Repository\InputPagesRepository::class);
        $inputPagesRepository->findBy(array(),array('pageOrder'=>'asc'))
                             ->willReturn($result)
                             ->shouldBeCalled();

        $this->doctrineEM->getRepository('PrismProductsManager\Entity\InputPages')
                         ->willReturn($inputPagesRepository)
                         ->shouldBeCalled();

        $this->assertEquals($result, $this->service->getAllPages());
    }
}
