<?php

namespace PrismProductsManagerTest\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Controller\ColoursController;
use ReflectionProperty;

/**
 * @author haniw
 */
class ColoursControllerTest extends PHPUnit_Framework_TestCase
{
    /** @var ColoursController */
    protected $controller;

    /** @var \Zend\Mvc\Controller\PluginManager */
    protected $pluginManager;

    /** @var \Zend\EventManager\EventManager */
    protected $eventManager;

    /** @var array **/
    public $pluginManagerPlugins = [];

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    protected function setUp()
    {
        $this->coloursMapper = $this->getMockBuilder('PrismProductsManager\Model\ColoursMapper')
                      ->disableOriginalConstructor()
                      ->getMock();

        $this->coloursForm = $this->getMockBuilder('PrismProductsManager\Form\ColoursForm')
                                                  ->disableOriginalConstructor()
                                                  ->getMock();

        $controller = new ColoursController(
            $this->coloursMapper,
            $this->coloursForm
        );

        // pluginManager and event manager setted after controller created
        $pluginManager = $this->getMock('Zend\Mvc\Controller\PluginManager', ['get']);
        $pluginManager->expects($this->any())
                      ->method('get')
                      ->will($this->returnCallback([$this, 'helperMockCallbackPluginManagerGet']));
        $this->pluginManager = $pluginManager;

        $eventManager = $this->getMock('Zend\EventManager\EventManager');
        $this->eventManager = $eventManager;

        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $controller->setPluginManager($pluginManager);
        $controller->setEventManager($eventManager);
        $controller->setServiceLocator($serviceLocator);

        $this->controller = $controller;
    }

    private function setIsCMSAuthorized(array $access)
    {
        $isCmsUserAuthorized = $this->getMock('PrismAuthorize\Controller\Plugin\IsCmsUserAuthorized');
        $isCmsUserAuthorized->expects($this->once())
             ->method('__invoke')
             ->with($access);
        $this->pluginManagerPlugins['isCmsUserAuthorized'] = $isCmsUserAuthorized;
    }

    private function setRouteWithQuery($query)
    {
        $params = $this->getMock('Zend\Mvc\Controller\Plugin\Params');
        $params->expects($this->any())
             ->method('__invoke')
             ->will($this->returnSelf());
        $params->expects($this->exactly(1))
                ->method('fromRoute')
                ->will($this->returnCallback(function ($key) use ($query) {
                 return $query;
        }));
        $this->pluginManagerPlugins['params'] = $params;
    }

    private function errorValue($route, $type, $message)
    {
        $flashMessenger = $this->getMock('Zend\Mvc\Controller\Plugin\FlashMessenger');
        $flashMessenger->expects($this->any())
             ->method('__invoke')
             ->willReturn($flashMessenger);
        $flashMessenger->expects($this->any())
                ->method('setNamespace')
                ->with('alert')
                ->willReturn($flashMessenger);

        $alert['type'] = $type;
        $alert['bold'] = ucfirst($type).':';
        $alert['messages'] = array();
        $alert['messages'][0] = $message;

        $flashMessenger->expects($this->any())
                       ->method('addMessage')
                       ->with($alert)
                       ->willReturn($flashMessenger);

        $this->pluginManagerPlugins['flashmessenger'] = $flashMessenger;

        $redirect = $this->getMock('Zend\Mvc\Controller\Plugin\Redirect');
        $redirect->expects($this->once())
               ->method('toRoute')
               ->with(null ,$route);

        $this->pluginManagerPlugins['redirect'] = $redirect;
    }

    public function testDeleteAction()
    {
        $this->setIsCMSAuthorized(['delete']);
        $this->setRouteWithQuery(['id' => 1]);

        $type = 'success';
        $message = 'The colour has been deleted';

        $this->coloursMapper->expects($this->once())
                        ->method('deleteColour')
                        ->with(1)->willReturn(true);

        $this->errorValue([
            'controller'=>'Colours',
            'action' => 'index'
        ], $type, $message);

        $this->controller->deleteAction();
    }

    public function testDeleteActionReturnFalse()
    {
        $this->setIsCMSAuthorized(['delete']);
        $this->setRouteWithQuery(['id' => 1]);

        $type = 'error';
        $message = 'The colour could not be deleted';

        $this->coloursMapper->expects($this->once())
                        ->method('deleteColour')
                        ->with(1)->willReturn(false);
        $this->errorValue([
            'controller'=>'Colours',
            'action' => 'index'
        ], $type, $message);

        $this->controller->deleteAction();
    }

    public function helperMockCallbackPluginManagerGet($key)
    {
        return (array_key_exists($key, $this->pluginManagerPlugins))
            ? $this->pluginManagerPlugins[$key]
            : null;
    }

    /**
     *
     * @param mixed $objectOrClass
     * @param string $property
     * @param mixed $value = null
     * @return \ReflectionProperty
     */
    public function helperMakePropertyAccessable($objectOrClass, $property, $value = null)
    {
        $reflectionProperty = new ReflectionProperty($objectOrClass, $property);
        $reflectionProperty->setAccessible(true);

        if ($value !== null) {
            $reflectionProperty->setValue($objectOrClass, $value);
        }
        return $reflectionProperty;
    }
}
