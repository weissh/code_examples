<?php

namespace PrismProductsManagerTest\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Controller\CoreAttributesController;
use ReflectionProperty;

/**
 * @author haniw
 */
class CoreAttributesControllerTest extends PHPUnit_Framework_TestCase
{
    /** @var CoreAttributesController */
    protected $controller;

    /** @var \Zend\Mvc\Controller\PluginManager */
    protected $pluginManager;

    /** @var \Zend\EventManager\EventManager */
    protected $eventManager;

    /** @var array **/
    public $pluginManagerPlugins = [];

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    protected function setUp()
    {
        $this->attributesMapper = $this->getMockBuilder('PrismProductsManager\Model\AttributesGroupMapper')
                      ->disableOriginalConstructor()
                      ->getMock();
        $this->editCoreAttributesForm = $this->getMockBuilder('PrismProductsManager\Form\EditCoreAttributeGroupForm')
                                  ->disableOriginalConstructor()
                                  ->getMock();

        $this->searchForm = $this->getMockBuilder('PrismProductsManager\Form\SearchForm')
                                 ->disableOriginalConstructor()
                                 ->getMock();

        $this->simplePagination = $this->getMockBuilder('PrismCommon\Service\SimplePagination')
                ->disableOriginalConstructor()
                ->getMock();

        $controller = new CoreAttributesController(
            $this->attributesMapper,
            $this->editCoreAttributesForm,
            $this->searchForm,
            $this->simplePagination
        );

        // pluginManager and event manager setted after controller created
        $pluginManager = $this->getMock('Zend\Mvc\Controller\PluginManager', ['get']);
        $pluginManager->expects($this->any())
                      ->method('get')
                      ->will($this->returnCallback([$this, 'helperMockCallbackPluginManagerGet']));
        $this->pluginManager = $pluginManager;

        $eventManager = $this->getMock('Zend\EventManager\EventManager');
        $this->eventManager = $eventManager;

        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $controller->setPluginManager($pluginManager);
        $controller->setEventManager($eventManager);
        $controller->setServiceLocator($serviceLocator);

        $this->controller = $controller;
    }

    private function setIsCMSAuthorized(array $access)
    {
        $isCmsUserAuthorized = $this->getMock('PrismAuthorize\Controller\Plugin\IsCmsUserAuthorized');
        $isCmsUserAuthorized->expects($this->once())
             ->method('__invoke')
             ->with($access);
        $this->pluginManagerPlugins['isCmsUserAuthorized'] = $isCmsUserAuthorized;
    }

    private function setRouteWithQuery($key, $result)
    {
        $params = $this->getMock('Zend\Mvc\Controller\Plugin\Params');
        $params->expects($this->any())
             ->method('__invoke')
             ->will($this->returnSelf());
        $params->expects($this->exactly(1))
                ->method('fromRoute')
                ->with($key)
                ->willReturn($result);
        $this->pluginManagerPlugins['params'] = $params;
    }

    private function errorValue($route, $type, $message)
    {
        $flashMessenger = $this->getMock('Zend\Mvc\Controller\Plugin\FlashMessenger');
        $flashMessenger->expects($this->any())
             ->method('__invoke')
             ->willReturn($flashMessenger);
        $flashMessenger->expects($this->any())
                ->method('setNamespace')
                ->with('alert')
                ->willReturn($flashMessenger);

        $alert['type'] = $type;
        $alert['bold'] = ucfirst($type).':';
        $alert['messages'] = array();
        $alert['messages'][0] = $message;

        $flashMessenger->expects($this->any())
                       ->method('addMessage')
                       ->with($alert)
                       ->willReturn($flashMessenger);

        $this->pluginManagerPlugins['flashmessenger'] = $flashMessenger;

        $redirect = $this->getMock('Zend\Mvc\Controller\Plugin\Redirect');
        $redirect->expects($this->once())
               ->method('toRoute')
               ->with(null ,$route);

        $this->pluginManagerPlugins['redirect'] = $redirect;
    }

    public function provideIds()
    {
        return [
//            [null],
            [1],
        ];
    }

    /**
     * @dataProvider provideIds
     */
    public function testValuesActionWithEmptyResult($id)
    {
        $this->setIsCMSAuthorized(['view']);
        $this->setRouteWithQuery('id', $id);

        $type = 'success';
        $message = 'The colour has been deleted';

        $result = [];
        $this->attributesMapper->expects($this->once())
                        ->method('getGroupById')
                        ->with($id)
                        ->willReturn($result);

        if ($id === null || empty($result)) {
            $this->errorValue('attributes', 'error', 'Group was not found in the database');
        }

        $this->controller->valuesAction();
    }

    public function testValuesActionWithHasResult()
    {
        $this->setIsCMSAuthorized(['view']);
        $this->setRouteWithQuery('id', 1);

        $type = 'success';
        $message = 'The colour has been deleted';

        $result = [
            0 => [

            ],
        ];
        $this->attributesMapper->expects($this->once())
                        ->method('getGroupById')
                        ->with(1)
                        ->willReturn($result);

        $this->assertInstanceOf('Zend\View\Model\ViewModel', $this->controller->valuesAction());
    }

    public function helperMockCallbackPluginManagerGet($key)
    {
        return (array_key_exists($key, $this->pluginManagerPlugins))
            ? $this->pluginManagerPlugins[$key]
            : null;
    }

    /**
     *
     * @param mixed $objectOrClass
     * @param string $property
     * @param mixed $value = null
     * @return \ReflectionProperty
     */
    public function helperMakePropertyAccessable($objectOrClass, $property, $value = null)
    {
        $reflectionProperty = new ReflectionProperty($objectOrClass, $property);
        $reflectionProperty->setAccessible(true);

        if ($value !== null) {
            $reflectionProperty->setValue($objectOrClass, $value);
        }
        return $reflectionProperty;
    }
}
