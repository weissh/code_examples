<?php

namespace PrismProductsManagerTest\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Controller\CurrencyConverterController;
use ReflectionMethod;
use ReflectionProperty;

/**
 * @author haniw
 */
class CurrencyConverterControllerTest extends PHPUnit_Framework_TestCase
{
    /** @var CurrencyConverterController */
    protected $controller;

    /** @var \Zend\Mvc\Controller\PluginManager */
    protected $pluginManager;

    /** @var \Zend\EventManager\EventManager */
    protected $eventManager;

    /** @var array **/
    public $pluginManagerPlugins = [];

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    protected function setUp()
    {
        $controller = new CurrencyConverterController();

        // pluginManager and event manager setted after controller created
        $pluginManager = $this->getMock('Zend\Mvc\Controller\PluginManager', ['get']);
        $pluginManager->expects($this->any())
                      ->method('get')
                      ->will($this->returnCallback([$this, 'helperMockCallbackPluginManagerGet']));
        $this->pluginManager = $pluginManager;

        $eventManager = $this->getMock('Zend\EventManager\EventManager');
        $this->eventManager = $eventManager;

        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $controller->setPluginManager($pluginManager);
        $controller->setEventManager($eventManager);
        $controller->setServiceLocator($serviceLocator);

        $this->controller = $controller;
    }

    public function testcsvExportAction()
    {
        $this->setIsCMSAuthorized(['view']);

        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $currencyMapper = $this->getMockBuilder('PrismProductsManager\Model\CurrencyMapper')
                               ->disableOriginalConstructor()
                               ->getMock();
        $currencyMapper->expects($this->at(0))
                        ->method('getProductList')
                        ->with(false);

        $currencyMapper->expects($this->at(1))
                       ->method('exportDataArray');

        $serviceLocator->expects($this->any())
                       ->method('get')
                       ->with('PrismProductsManager\Model\CurrencyMapper')
                       ->willReturn($currencyMapper);

        $this->controller->setServiceLocator($serviceLocator);

        $this->controller->csvExportAction();
    }

    private function setIsCMSAuthorized(array $access)
    {
        $isCmsUserAuthorized = $this->getMock('PrismAuthorize\Controller\Plugin\IsCmsUserAuthorized');
        $isCmsUserAuthorized->expects($this->once())
             ->method('__invoke')
             ->with($access);
        $this->pluginManagerPlugins['isCmsUserAuthorized'] = $isCmsUserAuthorized;
    }

    public function helperMockCallbackPluginManagerGet($key)
    {
        return (array_key_exists($key, $this->pluginManagerPlugins))
            ? $this->pluginManagerPlugins[$key]
            : null;
    }

    /**
     *
     * @param mixed $objectOrClass
     * @param string $property
     * @param mixed $value = null
     * @return \ReflectionProperty
     */
    public function helperMakePropertyAccessable($objectOrClass, $property, $value = null)
    {
        $reflectionProperty = new ReflectionProperty($objectOrClass, $property);
        $reflectionProperty->setAccessible(true);

        if ($value !== null) {
            $reflectionProperty->setValue($objectOrClass, $value);
        }
        return $reflectionProperty;
    }
}
