<?php

namespace PrismProductsManagerTest\Controller\Plugin;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Controller\Plugin\IsManualSync;
use ReflectionMethod;
use ReflectionProperty;

/**
 * @author haniw
 */
class IsManualSyncTest extends PHPUnit_Framework_TestCase
{
    /** @var IsManualSync */
    private $plugin;

    protected function setUp()
    {
        $this->plugin = new IsManualSync();
    }

    public function provideConfigAndReturn()
    {
        return [
            [[], false],
            [['settings' => ['manualSync' => true ]], true],
        ];
    }

    /**
     * @dataProvider provideConfigAndReturn
     */
    public function testInvoke($config, $return)
    {
        $controller = $this->prophesize('Zend\Mvc\Controller\AbstractActionController');
        $this->plugin->setController($controller->reveal());

        $serviceLocator = $this->prophesize('Zend\ServiceManager\ServiceLocatorInterface');
        $controller->getServiceLocator()->willReturn($serviceLocator)
                                        ->shouldBeCalled();

        $serviceLocator->get('Config')->willReturn(['clientConfig' => $config])
                                      ->shouldBeCalled();

        $this->assertEquals($return, $this->plugin->__invoke());
    }
}
