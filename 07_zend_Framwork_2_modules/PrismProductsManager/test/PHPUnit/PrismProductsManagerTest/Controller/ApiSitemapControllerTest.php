<?php

namespace PrismProductsManagerTest\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Controller\ApiSitemapController;
use ReflectionMethod;
use ReflectionProperty;

/**
 * @author haniw
 */
class ApiSitemapControllerTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiSitemapController */
    protected $controller;

    /** @var \Zend\Mvc\Controller\PluginManager */
    protected $pluginManager;

    /** @var \Zend\EventManager\EventManager */
    protected $eventManager;

    /** @var array **/
    public $pluginManagerPlugins = [];

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    protected function setUp()
    {
        $this->categoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\CategoriesMapper')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $this->pagesMapper = $this->getMockBuilder('PrismContentManager\Model\PagesMapper')
                           ->disableOriginalConstructor()
                           ->getMock();

        $config = [
           'siteConfig' => [
               'websites' => [
                   'website-url' => [
                       '1' => 'http://nauticalia-website.local/',
                       '2' => 'http://nauticalia-website.local/',
                       '3' => 'http://nauticalia-website.local/',
                   ],
               ],
               'sitemap' => [
                   'home' => '1',
                   'category' => '0.8',
                   'pages' => '0.6',
                   'product' => '1',
               ],
            ],
        ];
        $controller = new ApiSitemapController(
            $this->categoriesMapper,
            $this->pagesMapper,
            $config
        );

        // pluginManager and event manager setted after controller created
        $pluginManager = $this->getMock('Zend\Mvc\Controller\PluginManager', ['get']);
        $pluginManager->expects($this->any())
                      ->method('get')
                      ->will($this->returnCallback([$this, 'helperMockCallbackPluginManagerGet']));
        $this->pluginManager = $pluginManager;

        $eventManager = $this->getMock('Zend\EventManager\EventManager');
        $this->eventManager = $eventManager;

        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $controller->setPluginManager($pluginManager);
        $controller->setEventManager($eventManager);
        $controller->setServiceLocator($serviceLocator);

        $this->controller = $controller;
    }

    public function setNotAllowed()
    {
        $response = $this->getMockBuilder('Zend\Http\PhpEnvironment\Response')
                        ->disableOriginalConstructor()
                        ->getMock();
        $response->expects($this->once())
            ->method('setStatusCode')
            ->with(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        $this->helperMakePropertyAccessable($this->controller, 'response', $response);
        $this->setExpectedException('\Exception');
    }

    public function test_methodNotAllowed()
    {
        $this->setNotAllowed();
        $reflectionMethod = new ReflectionMethod($this->controller, '_methodNotAllowed');
        $reflectionMethod->setAccessible(true);

        $reflectionMethod->invoke($this->controller);
    }

    public function testGetList()
    {
        $this->categoriesMapper->expects($this->once())
                               ->method('fetchAllMarketingCategories')
                               ->with('', null, true, true)
                               ->willReturn([
            0 => [
               'categoryName' => 'shoes',
               'urlName' =>    'shoes',
            ]
        ]);
        $this->pagesMapper->expects($this->once())
                          ->method('getAllPages')
                          ->willReturn([
            0 => [
               'isLandingPage' => '1',
               'contentName' =>    'adidas',
               'urlName' => 'adidas',
           ],
           0 => [
              'isLandingPage' => '0',
              'contentName' => 'batta',
              'urlName' => 'batta',
           ]
        ]);

        $this->assertInstanceOf('Zend\View\Model\JsonModel', $this->controller->getList());
    }

    public function testGet()
    {
        $this->setNotAllowed();
        $this->controller->get(1);
    }

    public function testCreate()
    {
        $this->setNotAllowed();
        $this->controller->create([]);
    }

    public function testUpdate()
    {
        $this->setNotAllowed();
        $this->controller->update(1, ['foo' => 'fooValue']);
    }

    public function testDelete()
    {
        $this->setNotAllowed();
        $this->controller->delete(1);
    }

    public function helperMockCallbackPluginManagerGet($key)
    {
        return (array_key_exists($key, $this->pluginManagerPlugins))
            ? $this->pluginManagerPlugins[$key]
            : null;
    }

    /**
     *
     * @param mixed $objectOrClass
     * @param string $property
     * @param mixed $value = null
     * @return \ReflectionProperty
     */
    public function helperMakePropertyAccessable($objectOrClass, $property, $value = null)
    {
        $reflectionProperty = new ReflectionProperty($objectOrClass, $property);
        $reflectionProperty->setAccessible(true);

        if ($value !== null) {
            $reflectionProperty->setValue($objectOrClass, $value);
        }
        return $reflectionProperty;
    }
}
