<?php

namespace PrismProductsManagerTest\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Controller\ApiProductByUrlNameController;
use ReflectionMethod;
use ReflectionProperty;

/**
 * @author haniw
 */
class ApiProductByUrlNameControllerTest extends PHPUnit_Framework_TestCase
{
    /** @var ApiProductByUrlNameController */
    protected $controller;

    /** @var \Zend\Mvc\Controller\PluginManager */
    protected $pluginManager;

    /** @var \Zend\EventManager\EventManager */
    protected $eventManager;

    /** @var array **/
    public $pluginManagerPlugins = [];

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    protected function setUp()
    {
        $controller = new ApiProductByUrlNameController();

        // pluginManager and event manager setted after controller created
        $pluginManager = $this->getMock('Zend\Mvc\Controller\PluginManager', ['get']);
        $pluginManager->expects($this->any())
                      ->method('get')
                      ->will($this->returnCallback([$this, 'helperMockCallbackPluginManagerGet']));
        $this->pluginManager = $pluginManager;

        $eventManager = $this->getMock('Zend\EventManager\EventManager');
        $this->eventManager = $eventManager;

        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $controller->setPluginManager($pluginManager);
        $controller->setEventManager($eventManager);
        $controller->setServiceLocator($serviceLocator);

        $this->controller = $controller;
    }

    public function getMapper()
    {
        $reflectionMethod = new ReflectionMethod($this->controller, '_getProductMapper');
        $reflectionMethod->setAccessible(true);

        $reflectionProperty = new ReflectionProperty($this->controller, '_productsMapper');
        $reflectionProperty->setAccessible(true);

        $pagesMapper = $reflectionProperty->getValue($this->controller);
        if ($pagesMapper === null) {
            $pagesMapper = $this->getMockBuilder('PrismProductsManager\Model\ProductsMapper')
                                ->disableOriginalConstructor()
                                ->getMock();
            $this->serviceLocator->expects($this->at(0))
                                 ->method('get')
                                 ->with('PrismProductsManager\Model\ProductsMapper')
                                 ->willReturn($pagesMapper);
        }

        $reflectionMethod->invoke($this->controller);
        return $pagesMapper;
    }

    public function testLazyLoadGetProductMapper()
    {
        // call twice to make it idempotent
        $this->getMapper();
        $this->getMapper();
    }

    private function setRouteWithQuery($query)
    {
        $params = $this->getMock('Zend\Mvc\Controller\Plugin\Params');
        $params->expects($this->any())
             ->method('__invoke')
             ->will($this->returnSelf());
        $params->expects($this->any())
                ->method('fromRoute')
                ->will($this->returnCallback(function ($key) use ($query) {
            return $query;
        }));
        $this->pluginManagerPlugins['params'] = $params;
    }

    public function setNotAllowed()
    {
        $response = $this->getMockBuilder('Zend\Http\PhpEnvironment\Response')
                        ->disableOriginalConstructor()
                        ->getMock();
        $response->expects($this->once())
            ->method('setStatusCode')
            ->with(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        $this->helperMakePropertyAccessable($this->controller, 'response', $response);
        $this->setExpectedException('\Exception');
    }

    public function test_methodNotAllowed()
    {
        $this->setNotAllowed();
        $reflectionMethod = new ReflectionMethod($this->controller, '_methodNotAllowed');
        $reflectionMethod->setAccessible(true);

        $reflectionMethod->invoke($this->controller);
    }

    public function testGet()
    {
        $this->setNotAllowed();
        $this->controller->get(1);
    }

    public function helperMockCallbackPluginManagerGet($key)
    {
        return (array_key_exists($key, $this->pluginManagerPlugins))
            ? $this->pluginManagerPlugins[$key]
            : null;
    }

    /**
     *
     * @param mixed $objectOrClass
     * @param string $property
     * @param mixed $value = null
     * @return \ReflectionProperty
     */
    public function helperMakePropertyAccessable($objectOrClass, $property, $value = null)
    {
        $reflectionProperty = new ReflectionProperty($objectOrClass, $property);
        $reflectionProperty->setAccessible(true);

        if ($value !== null) {
            $reflectionProperty->setValue($objectOrClass, $value);
        }
        return $reflectionProperty;
    }
}
