<?php

namespace PrismProductsManagerTest\Controller;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Controller\TaxController;
use ReflectionProperty;

/**
 * @author haniw
 */
class TaxControllerTest extends PHPUnit_Framework_TestCase
{
    /** @var TaxController */
    protected $controller;

    /** @var \Zend\Mvc\Controller\PluginManager */
    protected $pluginManager;

    /** @var \Zend\EventManager\EventManager */
    protected $eventManager;

    /** @var array **/
    public $pluginManagerPlugins = [];

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    protected function setUp()
    {
        $this->taxMapper = $this->getMockBuilder('PrismProductsManager\Model\TaxMapper')
                      ->disableOriginalConstructor()
                      ->getMock();
        $this->addNewTaxForm = $this->getMockBuilder('PrismProductsManager\Form\AddNewTaxForm')
                                 ->disableOriginalConstructor()
                                 ->getMock();


        $controller = new TaxController(
            $this->taxMapper,
            $this->addNewTaxForm,
            [
                'current-language' => 1,
            ]
        );

        // pluginManager and event manager setted after controller created
        $pluginManager = $this->getMock('Zend\Mvc\Controller\PluginManager', ['get']);
        $pluginManager->expects($this->any())
                      ->method('get')
                      ->will($this->returnCallback([$this, 'helperMockCallbackPluginManagerGet']));
        $this->pluginManager = $pluginManager;

        $eventManager = $this->getMock('Zend\EventManager\EventManager');
        $this->eventManager = $eventManager;

        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $controller->setPluginManager($pluginManager);
        $controller->setEventManager($eventManager);
        $controller->setServiceLocator($serviceLocator);

        $this->controller = $controller;
    }

    private function setIsCMSAuthorized(array $access)
    {
        $isCmsUserAuthorized = $this->getMock('PrismAuthorize\Controller\Plugin\IsCmsUserAuthorized');
        $isCmsUserAuthorized->expects($this->once())
             ->method('__invoke')
             ->with($access);
        $this->pluginManagerPlugins['isCmsUserAuthorized'] = $isCmsUserAuthorized;
    }

    private function setRouteMatchParam($key, $defaultValue, $return)
    {
        $event = $this->getMockBuilder('Zend\Mvc\MvcEvent')
                      ->disableOriginalConstructor()
                      ->getMock();
        $routeMatch = $this->getMockBuilder('Zend\Mvc\Router\RouteMatch')
                           ->disableOriginalConstructor()
                           ->getMock();
        $routeMatch->expects($this->once())
                   ->method('getParam')
                   ->with($key, $defaultValue)
                   ->willReturn($return);
        $event->expects($this->once())
              ->method('getRouteMatch')
              ->willReturn($routeMatch);
        $mvcEvent = $this->controller->setEvent($event);
    }

    private function errorValue($route, $type, $message)
    {
        $flashMessenger = $this->getMock('Zend\Mvc\Controller\Plugin\FlashMessenger');
        $flashMessenger->expects($this->any())
             ->method('__invoke')
             ->willReturn($flashMessenger);
        $flashMessenger->expects($this->any())
                ->method('setNamespace')
                ->with('alert')
                ->willReturn($flashMessenger);

        $alert['type'] = $type;
        $alert['bold'] = ucfirst($type).':';
        $alert['messages'] = array();
        $alert['messages'][0] = $message;

        $flashMessenger->expects($this->any())
                       ->method('addMessage')
                       ->with($alert)
                       ->willReturn($flashMessenger);

        $this->pluginManagerPlugins['flashmessenger'] = $flashMessenger;

        $redirect = $this->getMock('Zend\Mvc\Controller\Plugin\Redirect');
        $redirect->expects($this->once())
               ->method('toRoute')
               ->with(null ,$route);

        $this->pluginManagerPlugins['redirect'] = $redirect;
    }

    public function testIndexAction()
    {
        $this->setIsCMSAuthorized(['view']);
        $this->setRouteMatchParam('page', 1, 1);

        $this->taxMapper->expects($this->once())
                        ->method('getTaxLists')
                        ->with(1)->willReturn([]);

        $this->assertInstanceOf('Zend\View\Model\ViewModel', $this->controller->indexAction());
    }

    public function provideIds()
    {
        return [
            [0],
            [1],
        ];
    }

    /**
     * @dataProvider provideIds
     */
    public function testDeactivateAction($id)
    {
        $this->setIsCMSAuthorized(['edit']);
        $this->setRouteMatchParam('id', 0, $id);

        if ($id == 0) {
            $type = 'error';
            $message = 'Entry id not specified!';
        } else {
            $type = 'success';
            $message = 'Taxed deactivated successfully!';

            $this->taxMapper->expects($this->once())
                            ->method('toggleTax')
                            ->with($id, true)->willReturn(true);
        }

        $this->errorValue([
	        'controller'=>'Tax',
	        'action' => 'index'
        ], $type, $message);

        $this->controller->deactivateAction();
    }

    public function testDeactivateActionReturnFalse()
    {
        $this->setIsCMSAuthorized(['edit']);
        $this->setRouteMatchParam('id', 0, 1);

        $type = 'error';
        $message = 'Could not deactivate the tax!';

        $this->taxMapper->expects($this->once())
                        ->method('toggleTax')
                        ->with(1, true)->willReturn(false);
        $this->errorValue([
            'controller'=>'Tax',
            'action' => 'index'
        ], $type, $message);

        $this->controller->deactivateAction();
    }

    /**
     * @dataProvider provideIds
     */
    public function testActivateAction($id)
    {
        $this->setIsCMSAuthorized(['edit']);
        $this->setRouteMatchParam('id', 0, $id);

        if ($id == 0) {
            $type = 'error';
            $message = 'Entry id not specified!';
        } else {
            $type = 'success';
            $message = 'Taxed activated successfully!';

            $this->taxMapper->expects($this->once())
                            ->method('toggleTax')
                            ->with($id, false)->willReturn(true);
        }

        $this->errorValue([
            'controller'=>'Tax',
            'action' => 'index'
        ], $type, $message);

        $this->controller->activateAction();
    }

    public function testActivateActionReturnFalse()
    {
        $this->setIsCMSAuthorized(['edit']);
        $this->setRouteMatchParam('id', 0, 1);

        $type = 'error';
        $message = 'Could not activate the tax!';

        $this->taxMapper->expects($this->once())
                        ->method('toggleTax')
                        ->with(1, false)->willReturn(false);
        $this->errorValue([
            'controller'=>'Tax',
            'action' => 'index'
        ], $type, $message);

        $this->controller->activateAction();
    }

    /**
     * @dataProvider provideIds
     */
    public function testDeleteAction($id)
    {
        $this->setIsCMSAuthorized(['delete']);
        $this->setRouteMatchParam('id', 0, $id);

        if ($id == 0) {
            $type = 'error';
            $message = 'Entry id not specified!';
        } else {
            $type = 'success';
            $message = 'Taxed deleted successfully!';

            $this->taxMapper->expects($this->once())
                            ->method('deleteTax')
                            ->with($id)->willReturn(true);
        }

        $this->errorValue([
            'controller'=>'Tax',
            'action' => 'index'
        ], $type, $message);

        $this->controller->deleteAction();
    }

    public function testDeleteActionReturnFalse()
    {
        $this->setIsCMSAuthorized(['delete']);
        $this->setRouteMatchParam('id', 0, 1);

        $type = 'error';
        $message = 'Could not delete the tax!';

        $this->taxMapper->expects($this->once())
                        ->method('deleteTax')
                        ->with(1)->willReturn(false);
        $this->errorValue([
            'controller'=>'Tax',
            'action' => 'index'
        ], $type, $message);

        $this->controller->deleteAction();
    }

    public function helperMockCallbackPluginManagerGet($key)
    {
        return (array_key_exists($key, $this->pluginManagerPlugins))
            ? $this->pluginManagerPlugins[$key]
            : null;
    }

    /**
     *
     * @param mixed $objectOrClass
     * @param string $property
     * @param mixed $value = null
     * @return \ReflectionProperty
     */
    public function helperMakePropertyAccessable($objectOrClass, $property, $value = null)
    {
        $reflectionProperty = new ReflectionProperty($objectOrClass, $property);
        $reflectionProperty->setAccessible(true);

        if ($value !== null) {
            $reflectionProperty->setValue($objectOrClass, $value);
        }
        return $reflectionProperty;
    }
}
