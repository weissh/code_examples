<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\AttributesGroups;

/**
 * @author haniw
 */
class AttributesGroupsTest extends PHPUnit_Framework_TestCase
{
    /** @var AttributesGroups */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new AttributesGroups();
    }

    public function testGetAttributegroupid()
    {
        $this->assertNull($this->entity->getAttributegroupid());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
