<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\Products;

/**
 * @author haniw
 */
class ProductsTest extends PHPUnit_Framework_TestCase
{
    /** @var Products */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new Products();
    }
    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetStyle()
    {
        $this->entity->setStyle('london');
        $this->assertEquals('london', $this->entity->getStyle());
    }

    public function testGetProductId()
    {
        $this->assertNull($this->entity->getProductId());
    }

    public function testSetGetManufacturedId()
    {
        $this->entity->setManufacturerId(1);
        $this->assertEquals(1, $this->entity->getManufacturerId());
    }

    public function testSetGetEan13()
    {
        $this->entity->setEan13('5055347347580');
        $this->assertEquals('5055347347580', $this->entity->getEan13());
    }

    public function testSetGetEcoTax()
    {
        $this->entity->setEcoTax('0.00');
        $this->assertEquals('0.00', $this->entity->getEcoTax());
    }

    public function testSetGetQuantity()
    {
        $this->entity->setQuantity(1);
        $this->assertEquals(1, $this->entity->getQuantity());
    }

    public function testSetGetSku()
    {
        $this->entity->setSku('ALESDEBRW060');
        $this->assertEquals('ALESDEBRW060', $this->entity->getSku());
    }

    public function testSetGetsupplierreference()
    {
        $this->entity->setsupplierreference('BER01');
        $this->assertEquals('BER01', $this->entity->getsupplierreference());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
