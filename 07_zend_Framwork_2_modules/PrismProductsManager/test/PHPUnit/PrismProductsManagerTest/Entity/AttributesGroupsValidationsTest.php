<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\AttributesGroupsValidations;

/**
 * @author haniw
 */
class AttributesGroupsValidationsTest extends PHPUnit_Framework_TestCase
{
    /** @var AttributesGroupsValidations */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new AttributesGroupsValidations();
    }

    public function testGetAttributegroupvalidationid()
    {
        $this->assertNull($this->entity->getAttributegroupvalidationid());
    }
}
