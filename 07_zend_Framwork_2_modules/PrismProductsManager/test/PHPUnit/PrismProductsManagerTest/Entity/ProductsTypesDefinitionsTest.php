<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\ProductsTypesDefinitions;

/**
 * @author haniw
 */
class ProductsTypesDefinitionsTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsTypesDefinitions */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new ProductsTypesDefinitions();
    }

    public function testGetProducttypedefinitionid()
    {
        $this->assertNull($this->entity->getProducttypedefinitionid());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
