<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\BundlesPrices;

/**
 * @author haniw
 */
class BundlesPricesTest extends PHPUnit_Framework_TestCase
{
    /** @var BundlesPrices */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new BundlesPrices();
    }

    public function testGetBundlepriceid()
    {
        $this->assertNull($this->entity->getBundlepriceid());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
