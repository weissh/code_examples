<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\Attributes;

/**
 * @author haniw
 */
class AttributesTest extends PHPUnit_Framework_TestCase
{
    /** @var Attributes */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new Attributes();
    }

    public function testGetAttributeId()
    {
        $this->assertNull($this->entity->getAttributeId());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
