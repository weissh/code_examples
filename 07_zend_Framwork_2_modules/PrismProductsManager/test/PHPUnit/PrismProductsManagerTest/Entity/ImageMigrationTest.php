<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\ImageMigration;

/**
 * @author haniw
 */
class ImageMigrationTest extends PHPUnit_Framework_TestCase
{
    /** @var ImageMigration */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new ImageMigration();
    }

    public function testGetConvertionid()
    {
        $this->assertNull($this->entity->getConvertionid());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }
}
