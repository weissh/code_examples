<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\ProductsDefinitions;

/**
 * @author haniw
 */
class ProductsDefinitionsTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductsDefinitions */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new ProductsDefinitions();
    }

    public function testGetProductdefinitionsid()
    {
        $this->assertNull($this->entity->getProductdefinitionsid());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
