<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\Promotions;

/**
 * @author haniw
 */
class PromotionsTest extends PHPUnit_Framework_TestCase
{
    /** @var LogsActionHistory */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new Promotions();
    }

    public function testGetPrmotionid()
    {
        $this->assertNull($this->entity->getPrmotionid());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
