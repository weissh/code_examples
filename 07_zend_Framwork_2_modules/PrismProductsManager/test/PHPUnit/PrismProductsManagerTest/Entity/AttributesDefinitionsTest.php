<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\AttributesDefinitions;

/**
 * @author haniw
 */
class AttributesDefinitionsTest extends PHPUnit_Framework_TestCase
{
    /** @var AttributesDefinitions */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new AttributesDefinitions();
    }

    public function testGetAttributesdefinitionsid()
    {
        $this->assertNull($this->entity->getAttributesdefinitionsid());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
