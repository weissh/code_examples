<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\LogsActionHistory;

/**
 * @author haniw
 */
class LogsActionHistoryTest extends PHPUnit_Framework_TestCase
{
    /** @var LogsActionHistory */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new LogsActionHistory();
    }

    public function testGetLogid()
    {
        $this->assertNull($this->entity->getLogid());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
