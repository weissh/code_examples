<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\TaxesRegions;

/**
 * @author haniw
 */
class TaxesRegionsTest extends PHPUnit_Framework_TestCase
{
    /** @var TaxesRegions */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new TaxesRegions();
    }

    public function testGetTaxregionid()
    {
        $this->assertNull($this->entity->getTaxregionid());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
