<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\Taxes;

/**
 * @author haniw
 */
class TaxesTest extends PHPUnit_Framework_TestCase
{
    /** @var Taxes */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new Taxes();
    }

    public function testGetTaxid()
    {
        $this->assertNull($this->entity->getTaxid());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
