<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\BulkProcessing;

/**
 * @author haniw
 */
class BulkProcessingTest extends PHPUnit_Framework_TestCase
{
    /** @var BulkProcessing */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new BulkProcessing();
    }
    
    public function testSetGetSubmittedBy()
    {
        $this->entity->setSubmittedBy(1);
        $this->assertEquals(1, $this->entity->getSubmittedBy());
    }
    
    public function testSetGetProcesseValue()
    {
        $this->entity->setProcessValue('UK');
        $this->assertEquals('UK', $this->entity->getProcessValue());
    }
    
    public function testSetGetProcessedKey()
    {
        $this->entity->setProcessKey('Bought By Territory');
        $this->assertEquals('Bought By Territory', $this->entity->getProcessKey());
    }
    
    public function testSetGetProcessedResponse()
    {
        $this->entity->setProcessedResponse('failure');
        $this->assertEquals('failure', $this->entity->getProcessedResponse());
    }
    
    public function testSetGetSchedule()
    {
        $this->entity->setSchedule(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getSchedule());
    }
    
    public function testSetGetProcessed()
    {
        $this->entity->setProcessed(0);
        $this->assertEquals(0, $this->entity->getProcessed());
    }
    
    public function testSetGetProcessing()
    {
        $this->entity->setProcessing(0);
        $this->assertEquals(0, $this->entity->getProcessing());
    }
    
    public function testSetGetStyleoroption()
    {
        $this->entity->setStyleoroption('03AG001BLA');
        $this->assertEquals('03AG001BLA', $this->entity->getStyleoroption());
    }
    
    public function testSetGetChangedFileName()
    {
        $this->entity->setChangedFileName('foo.xlsx');
        $this->assertEquals('foo.xlsx', $this->entity->getChangedFileName());
    }
    
    public function testSetGetOrginalFileName()
    {
        $this->entity->setOrginalFileName('foo.xlsx');
        $this->assertEquals('foo.xlsx', $this->entity->getOrginalFileName());
    }
    
    public function testSetGetProcessType()
    {
        $this->entity->setProcessType('COMMON_ALL');
        $this->assertEquals('COMMON_ALL', $this->entity->getProcessType());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
