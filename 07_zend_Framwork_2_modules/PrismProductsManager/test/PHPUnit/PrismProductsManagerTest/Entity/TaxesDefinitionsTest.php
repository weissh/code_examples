<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\TaxesDefinitions;

/**
 * @author haniw
 */
class TaxesDefinitionsTest extends PHPUnit_Framework_TestCase
{
    /** @var Taxes */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new TaxesDefinitions();
    }

    public function testGetTaxdefinitionid()
    {
        $this->assertNull($this->entity->getTaxdefinitionid());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
