<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\QueueManagerBulkLogs;

/**
 * @author haniw
 */
class QueueManagerBulkLogsTest extends PHPUnit_Framework_TestCase
{
    /** @var QueueManagerBulkLogs */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new QueueManagerBulkLogs();
    }

    public function testGetQueuebulklogsid()
    {
        $this->assertNull($this->entity->getQueuebulklogsid());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
