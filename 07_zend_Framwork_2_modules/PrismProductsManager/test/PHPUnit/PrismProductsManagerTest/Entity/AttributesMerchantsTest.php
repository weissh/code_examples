<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\AttributesMerchants;

/**
 * @author haniw
 */
class AttributesMerchantsTest extends PHPUnit_Framework_TestCase
{
    /** @var AttributesMerchants */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new AttributesMerchants();
    }

    public function testGetAttributeid()
    {
        $this->assertNull($this->entity->getAttributeid());
    }
}
