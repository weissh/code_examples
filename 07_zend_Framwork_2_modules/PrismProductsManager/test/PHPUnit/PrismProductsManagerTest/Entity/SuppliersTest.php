<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\Suppliers;

/**
 * @author haniw
 */
class SuppliersTest extends PHPUnit_Framework_TestCase
{
    /** @var Suppliers */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new Suppliers();
    }

    public function testGetSupplierid()
    {
        $this->assertNull($this->entity->getSupplierid());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
