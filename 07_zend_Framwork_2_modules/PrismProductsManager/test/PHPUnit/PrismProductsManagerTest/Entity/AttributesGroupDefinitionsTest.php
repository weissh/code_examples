<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\AttributesGroupDefinitions;

/**
 * @author haniw
 */
class AttributesGroupDefinitionsTest extends PHPUnit_Framework_TestCase
{
    /** @var AttributesGroupDefinitions */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new AttributesGroupDefinitions();
    }

    public function testGetAttributegroupdefinitionid()
    {
        $this->assertNull($this->entity->getAttributegroupdefinitionid());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }

    public function testSetGetName()
    {
        $this->entity->setName('name');
        $this->assertEquals('name', $this->entity->getName());
    }
}
