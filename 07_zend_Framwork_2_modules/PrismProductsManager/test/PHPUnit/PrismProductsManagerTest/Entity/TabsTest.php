<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\Tabs;

/**
 * @author haniw
 */
class TabsTest extends PHPUnit_Framework_TestCase
{
    /** @var Tabs */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new Tabs();
    }

    public function testGetTabid()
    {
        $this->assertNull($this->entity->getTabid());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
