<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\BundlesDefinitions;

/**
 * @author haniw
 */
class BundlesDefinitionsTest extends PHPUnit_Framework_TestCase
{
    /** @var BundlesDefinitions */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new BundlesDefinitions();
    }

    public function testgetBundledefinitionid()
    {
        $this->assertNull($this->entity->getBundledefinitionid());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
