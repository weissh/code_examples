<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\Colours;

/**
 * @author haniw
 */
class ColoursTest extends PHPUnit_Framework_TestCase
{
    /** @var Colours */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new Colours();
    }

    public function testSetGetHex()
    {
        $this->entity->setHex('#5F0090');
        $this->assertEquals('#5F0090', $this->entity->getHex());
    }
}
