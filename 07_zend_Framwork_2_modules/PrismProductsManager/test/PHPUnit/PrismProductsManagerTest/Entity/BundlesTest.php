<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\Bundles;

/**
 * @author haniw
 */
class BundlesTest extends PHPUnit_Framework_TestCase
{
    /** @var Bundles */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new Bundles();
    }

    public function testGetBundleid()
    {
        $this->assertNull($this->entity->getBundleid());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testSetGetCreated()
    {
        $this->entity->setCreated(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getCreated());
    }

    public function testSetGetModified()
    {
        $this->entity->setModified(new \DateTime('NOW'));
        $this->assertInstanceOf(\DateTime::class, $this->entity->getModified());
    }
}
