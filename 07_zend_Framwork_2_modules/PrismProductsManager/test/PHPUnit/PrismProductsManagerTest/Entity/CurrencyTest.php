<?php

namespace PrismProductsManagerTest\Entity;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Entity\Currency;

/**
 * @author haniw
 */
class CurrencyTest extends PHPUnit_Framework_TestCase
{
    /** @var Currency */
    protected $entity;

    protected function setUp()
    {
        $this->entity = new Currency();
    }

    public function testSetGetName()
    {
        $this->entity->setName('foo');
        $this->assertEquals('foo', $this->entity->getName());
    }

    public function testSetGetStatus()
    {
        $this->entity->setStatus('ACTIVE');
        $this->assertEquals('ACTIVE', $this->entity->getStatus());
    }

    public function testGetCurrencyId()
    {
        $this->assertNull($this->entity->getCurrencyId());
    }

    public function testSetGetIsDefault()
    {
        $this->entity->setIsDefault(true);
        $this->assertEquals(true, $this->entity->getIsDefault());
    }

    public function testSetGetIsoCode()
    {
        $this->entity->setIsoCode(true);
        $this->assertEquals(true, $this->entity->getIsoCode());
    }

    public function testSetGetHtmlCode()
    {
        $this->entity->setHtmlCode('<br/>');
        $this->assertEquals('<br/>', $this->entity->getHtmlCode());
    }

    public function testSetGetBlank()
    {
        $this->entity->setBlank(true);
        $this->assertEquals(true, $this->entity->getBlank());
    }

    public function testSetGetFormat()
    {
        $this->entity->setFormat('2,".","."');
        $this->assertEquals('2,".","."', $this->entity->getFormat());
    }

    public function testSetGetDecimals()
    {
        $this->entity->setDecimals(2);
        $this->assertEquals(2, $this->entity->getDecimals());
    }

    public function testSetGetConversionRate()
    {
        $this->entity->setConversionRate(0.5);
        $this->assertEquals(0.5, $this->entity->getConversionRate());
    }
}
