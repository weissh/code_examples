<?php

namespace PrismProductsManagerTest\Command;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\BulkActionsMapper;
use PrismProductsManager\Command\ProcessBulkRecordsCommand;
use Zend\View\Model\ConsoleModel;

/**
 * @author haniw
 */
class ProcessBulkRecordsCommandTest extends PHPUnit_Framework_TestCase
{
    /** @var ProcessBulkRecordsCommand */
    protected $command;

    protected function setUp()
    {
        $this->model   = $this->prophesize(BulkActionsMapper::class);
        $this->command = new ProcessBulkRecordsCommand(
            $this->model->reveal()
        );
    }
    
    public function testProcessAction()
    {
        $this->model->processBulkRecords()->shouldBeCalled();
        
        $result = $this->command->processAction();
        $this->assertInstanceOf(ConsoleModel::class, $result);
    }
    
    public function testProcessNotifySipAndSpectrumAction()
    {
        $this->model->notifySipAndSpectrum()->shouldBeCalled();
        
        $result = $this->command->processNotifySipAndSpectrumAction();
        $this->assertInstanceOf(ConsoleModel::class, $result);
    }
}
