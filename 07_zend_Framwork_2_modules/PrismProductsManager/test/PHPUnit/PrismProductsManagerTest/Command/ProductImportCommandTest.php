<?php

namespace PrismProductsManagerTest\Command;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Command\ProductImportCommand;

/**
 * @author haniw
 */
class ProductImportCommandTest extends PHPUnit_Framework_TestCase
{
    /** @var ProductImportCommand */
    protected $command;

    protected function setUp()
    {
        $this->productImportService = $this->prophesize('PrismProductsManager\Service\ProductImportService');
        $this->command       = new ProductImportCommand($this->productImportService->reveal());
    }

    public function testproductImportActionWithRequestFromConsoleRequestAndfilePathParameterisExistsAndImportAble()
    {
        $consoleRequest = $this->prophesize('Zend\Console\Request');
        $consoleRequest->getParam('filePath')->willReturn('filePath');

        $r = new \ReflectionProperty($this->command, 'request');
        $r->setAccessible(true);
        $r->setValue($this->command, $consoleRequest->reveal());

        $this->productImportService->verbose(true)->shouldBeCalled();
        $this->productImportService->validateExcelFile('filePath')->willReturn(true)->shouldBeCalled();
        $this->productImportService->import('filePath')->willReturn(true)->shouldBeCalled();

        ob_start();
        $this->command->productImportAction();
        $content = ob_get_clean();

        $this->assertEquals(
<<<content
Starting validation \nValidation passed \nStarting import \nImport was successful \n
content
            , $content);

    }

    public function testproductImportActionWithRequestFromConsoleRequestAndfilePathParameterisExistsAndPartialSucceed()
    {
        $consoleRequest = $this->prophesize('Zend\Console\Request');
        $consoleRequest->getParam('filePath')->willReturn('filePath');

        $r = new \ReflectionProperty($this->command, 'request');
        $r->setAccessible(true);
        $r->setValue($this->command, $consoleRequest->reveal());

        $this->productImportService->verbose(true)->shouldBeCalled();
        $this->productImportService->validateExcelFile('filePath')->willReturn(2)->shouldBeCalled();
        $this->productImportService->import('filePath')->willReturn(true)->shouldBeCalled();

        ob_start();
        $this->command->productImportAction();
        $content = ob_get_clean();

        $this->assertEquals(
<<<content
Starting validation \nValidation partially passed. 2 products will be imported \nStarting import \nImport was successful \n
content
            , $content);

    }

    public function testproductImportActionWithRequestFromConsoleRequestAndfilePathParameterisExistsAndPartialSucceedWithFailureImport()
    {
        $consoleRequest = $this->prophesize('Zend\Console\Request');
        $consoleRequest->getParam('filePath')->willReturn('filePath');

        $r = new \ReflectionProperty($this->command, 'request');
        $r->setAccessible(true);
        $r->setValue($this->command, $consoleRequest->reveal());

        $this->productImportService->verbose(true)->shouldBeCalled();
        $this->productImportService->validateExcelFile('filePath')->willReturn(2)->shouldBeCalled();
        $this->productImportService->import('filePath')->willReturn(false)->shouldBeCalled();

        ob_start();
        $this->command->productImportAction();
        $content = ob_get_clean();

        $this->assertEquals(
<<<content
Starting validation \nValidation partially passed. 2 products will be imported \nStarting import \n\033[0;31m [!] Import failed!... \033[0m\n
content
            , $content);

    }

    public function testproductImportActionWithRequestFromHttpRequest()
    {
        $this->setExpectedException('RuntimeException');

        $request = $this->prophesize('Zend\Http\PhpEnvironment\Request');

        $r = new \ReflectionProperty($this->command, 'request');
        $r->setAccessible(true);
        $r->setValue($this->command, $request->reveal());

        $this->command->productImportAction();
    }

    public function testproductImportTruncateActionWithRequestFromConsoleRequestAndConfirmIsNo()
    {
        $consoleRequest = $this->prophesize('Zend\Console\Request');
        $consoleRequest->getParam('confirm')->willReturn('no');

        $r = new \ReflectionProperty($this->command, 'request');
        $r->setAccessible(true);
        $r->setValue($this->command, $consoleRequest->reveal());

        ob_start();
        $this->command->productImportTruncateAction();
        $content = ob_get_clean();

        $this->assertEquals('Please confirm script to truncate tables using "confirm" as a parameter.' . "\n", $content);
    }

    public function testproductImportTruncateActionWithRequestFromConsoleRequestAndConfirmIsYes()
    {
        $consoleRequest = $this->prophesize('Zend\Console\Request');
        $consoleRequest->getParam('confirm')->willReturn('yes');

        $r = new \ReflectionProperty($this->command, 'request');
        $r->setAccessible(true);
        $r->setValue($this->command, $consoleRequest->reveal());

        $this->productImportService->truncateTablesForImport()->shouldBeCalled();
        $this->command->productImportTruncateAction();
    }

    public function testproductImportTruncateActionWithRequestFromHttpRequest()
    {
        $this->setExpectedException('RuntimeException');

        $request = $this->prophesize('Zend\Http\PhpEnvironment\Request');

        $r = new \ReflectionProperty($this->command, 'request');
        $r->setAccessible(true);
        $r->setValue($this->command, $request->reveal());

        $this->command->productImportTruncateAction();
    }

    public function testcoloursImportActionWithRequestFromConsoleRequest()
    {
        $consoleRequest = $this->prophesize('Zend\Console\Request');

        $r = new \ReflectionProperty($this->command, 'request');
        $r->setAccessible(true);
        $r->setValue($this->command, $consoleRequest->reveal());

        $this->productImportService->importColours()->shouldBeCalled();
        $this->command->coloursImportAction();
    }

    public function testcoloursImportActionWithRequestFromHttpRequest()
    {
        $this->setExpectedException('RuntimeException');

        $request = $this->prophesize('Zend\Http\PhpEnvironment\Request');

        $r = new \ReflectionProperty($this->command, 'request');
        $r->setAccessible(true);
        $r->setValue($this->command, $request->reveal());

        $this->command->coloursImportAction();
    }

    public function testUpdateAbbrevCodeActionWithRequestFromConsoleRequest()
    {
        $consoleRequest = $this->prophesize('Zend\Console\Request');

        $r = new \ReflectionProperty($this->command, 'request');
        $r->setAccessible(true);
        $r->setValue($this->command, $consoleRequest->reveal());

        $this->productImportService->updateAbbrevCodeValues()->shouldBeCalled();
        $this->command->updateAbbrevCodeAction();
    }

    public function testUpdateAbbrevCodeActionWithRequestFromHttpRequest()
    {
        $this->setExpectedException('RuntimeException');

        $request = $this->prophesize('Zend\Http\PhpEnvironment\Request');

        $r = new \ReflectionProperty($this->command, 'request');
        $r->setAccessible(true);
        $r->setValue($this->command, $request->reveal());

        $this->command->updateAbbrevCodeAction();
    }
}
