<?php

namespace PrismProductsManagerTest\Command;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Command\CleanupAttachmentsCommand;
use Zend\View\Model\ConsoleModel;

/**
 * @author haniw
 */
class CleanupAttachmentsCommandTest extends PHPUnit_Framework_TestCase
{
    /** @var CleanupAttachmentsCommand */
    protected $command;

    protected function setUp()
    {
        $this->command = new CleanupAttachmentsCommand();
    }

    public function testBulkAction()
    {
        $oldCwd = getcwd();

        chdir(__DIR__);

        // backup first
        $this->full_copy('./public/uploads', './public/uploadsbackup');

        $this->assertInstanceOf(ConsoleModel::class, $this->command->bulkAction());

        // rollback
        $this->full_copy('./public/uploadsbackup', './public/uploads');
        @unlink('./public/uploads/.gitignore');

        chdir($oldCwd);
    }

    private function full_copy( $source, $target ) {
        if ( is_dir( $source ) ) {
            @mkdir( $target );
            $d = dir( $source );
            while ( FALSE !== ( $entry = $d->read() ) ) {
                if ( $entry == '.' || $entry == '..' ) {
                    continue;
                }
                $Entry = $source . '/' . $entry;
                if ( is_dir( $Entry ) ) {
                    full_copy( $Entry, $target . '/' . $entry );
                    continue;
                }
                copy( $Entry, $target . '/' . $entry );
            }

            $d->close();
        }else {
            copy( $source, $target );
        }
    }
}
