<?php

namespace PrismProductsManagerTest\ZendDbFixture;

use PrismProductsManagerTest\Bootstrap;

/**
 * @author haniw
 */
class FixtureManager
{
    protected $dbAdapter;

    public function __construct($adapterName)
    {
        $this->dbAdapter = Bootstrap::getServiceManager()->get($adapterName);
    }

    public function getDbAdapter()
    {
        return $this->dbAdapter;
    }

    public function execute($file)
    {
        $this->getDbAdapter()->query("SET FOREIGN_KEY_CHECKS=0;", 'execute');
        $this->getDbAdapter()->query(file_get_contents(__DIR__.'/assets/'.$file), 'execute');
        $this->getDbAdapter()->query("SET FOREIGN_KEY_CHECKS=1;", 'execute');
    }
}
