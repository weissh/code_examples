DROP TABLE IF EXISTS `PRODUCTS_Types_Definitions`;
CREATE TABLE IF NOT EXISTS `PRODUCTS_Types_Definitions` (
  `productTypeDefinitionId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `languageId` int(11) NOT NULL,
  `productTypeId` int(11) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`productTypeDefinitionId`),
  KEY `languageId` (`languageId`),
  KEY `name` (`name`),
  KEY `productTypeId` (`productTypeId`),
  KEY `languageId_productTypeId_name` (`languageId`,`productTypeId`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `PRODUCTS_Types_Definitions`
--

INSERT INTO `PRODUCTS_Types_Definitions` (`productTypeDefinitionId`, `languageId`, `productTypeId`, `name`, `description`, `created`, `modified`) VALUES
(1, 1, 1, 'STANDARD', 'Our standard products', '2014-08-08 18:58:30', '2014-08-08 12:01:44'),
(2, 1, 2, 'ECARDS', 'Voucher product', '2014-08-08 18:59:49', '2014-11-21 02:44:14'),
(3, 1, 3, 'EVOUCHER', 'A virtual product', '2014-09-08 12:36:58', '2015-02-18 03:20:14'),
(5, 2, 1, 'STANDARD', 'Our standard products', '2014-08-08 18:58:30', '2014-11-21 02:44:27'),
(6, 2, 2, 'ECARDS', 'Voucher product', '2014-08-08 18:59:49', '2014-11-21 02:44:24'),
(7, 2, 3, 'EVOUCHER', 'A virtual product', '2014-09-08 12:36:58', '2015-02-18 03:20:14');
