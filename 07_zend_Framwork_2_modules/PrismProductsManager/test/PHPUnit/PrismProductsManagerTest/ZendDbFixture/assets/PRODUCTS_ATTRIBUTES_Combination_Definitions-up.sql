DROP TABLE IF EXISTS `PRODUCTS_ATTRIBUTES_Combination_Definitions`;
CREATE TABLE IF NOT EXISTS `PRODUCTS_ATTRIBUTES_Combination_Definitions` (
  `productAttributesCombinationDefinitionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productAttributeCombinationId` int(10) unsigned NOT NULL,
  `attributeValue` varchar(100) NOT NULL,
  `languageId` int(10) unsigned NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`productAttributesCombinationDefinitionId`),
  KEY `status` (`status`) USING BTREE,
  KEY `Index 3` (`languageId`) USING BTREE,
  KEY `productAttributeCombinationId` (`productAttributeCombinationId`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=128884 ;

--
-- Dumping data for table `PRODUCTS_ATTRIBUTES_Combination_Definitions`
--

INSERT INTO `PRODUCTS_ATTRIBUTES_Combination_Definitions` (`productAttributesCombinationDefinitionId`, `productAttributeCombinationId`, `attributeValue`, `languageId`, `status`, `created`, `modified`) VALUES
(1, 4, '1', 1, 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(2, 5, '13', 1, 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(3, 6, '34', 1, 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(4, 7, '0', 1, 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(5, 9, 'FREE DELIVERY & RETURNS IN UK', 1, 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(6, 10, 'WGB', 1, 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(7, 11, '20', 1, 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(8, 12, '', 1, 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(9, 16, 'FREE INTERNATIONAL DELIVERIES ON ORDERS OVER &pound;100, $100, &euro;100', 1, 'ACTIVE', '2015-01-28 19:00:48', '2015-02-25 06:06:32'),
(10, 20, '1', 1, 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(11, 21, '13', 1, 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00');


ALTER TABLE `PRODUCTS_ATTRIBUTES_Combination_Definitions`
  ADD CONSTRAINT `productAttributeCombinationId` FOREIGN KEY (`productAttributeCombinationId`) REFERENCES `PRODUCTS_ATTRIBUTES_Combination` (`productAttributeCombinationId`) ON DELETE NO ACTION ON UPDATE NO ACTION;
