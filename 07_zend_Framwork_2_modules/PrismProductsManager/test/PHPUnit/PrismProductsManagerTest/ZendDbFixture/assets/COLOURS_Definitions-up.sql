DROP TABLE IF EXISTS `COLOURS_Definitions`;
CREATE TABLE IF NOT EXISTS `COLOURS_Definitions` (
  `colourDefinitionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `colourId` int(10) unsigned NOT NULL,
  `languageId` int(10) unsigned NOT NULL,
  `colourName` varchar(45) NOT NULL,
  PRIMARY KEY (`colourDefinitionId`),
  KEY `colourDescription` (`colourName`) USING BTREE,
  KEY `languageId` (`languageId`) USING BTREE,
  KEY `colourId` (`colourId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=158 ;

--
-- Dumping data for table `COLOURS_Definitions`
--

INSERT INTO `COLOURS_Definitions` (`colourDefinitionId`, `colourId`, `languageId`, `colourName`) VALUES
(2, 2, 1, 'Aubergine'),
(3, 3, 1, 'Beige'),
(4, 4, 1, 'Black'),
(5, 5, 1, 'Blue'),
(6, 6, 1, 'Brown'),
(7, 7, 1, 'Burgundy'),
(8, 8, 1, 'Camel'),
(9, 9, 1, 'Charcoal'),
(10, 10, 1, 'Clear'),
(11, 11, 1, 'Cognac');

ALTER TABLE `COLOURS_Definitions`
  ADD CONSTRAINT `COLOURS_Definitions_ibfk_1` FOREIGN KEY (`colourId`) REFERENCES `COLOURS` (`colourId`) ON DELETE NO ACTION ON UPDATE NO ACTION;
