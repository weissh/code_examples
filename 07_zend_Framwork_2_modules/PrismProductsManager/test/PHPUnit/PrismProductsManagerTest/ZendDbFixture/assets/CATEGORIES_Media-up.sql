DROP TABLE IF EXISTS `CATEGORIES_Media`;
CREATE TABLE `CATEGORIES_Media` (
  `categoryMediaId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) unsigned NOT NULL,
  `imageId` varchar(45) NOT NULL,
  `mediaPriority` int(11) unsigned NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`categoryMediaId`),
  KEY `mediaPriority` (`mediaPriority`),
  KEY `fk_key` (`categoryId`) USING BTREE,
  CONSTRAINT `fk_category_id` FOREIGN KEY (`categoryId`) REFERENCES `CATEGORIES` (`categoryId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;