DROP TABLE IF EXISTS `CURRENCY`;
CREATE TABLE IF NOT EXISTS `CURRENCY` (
  `currencyId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `isDefault` tinyint(1) NOT NULL DEFAULT '0',
  `isoCode` varchar(3) NOT NULL DEFAULT '0',
  `htmlCode` varchar(7) NOT NULL DEFAULT '0',
  `sign` varchar(8) NOT NULL,
  `blank` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `format` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `decimals` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `conversionRate` decimal(13,6) NOT NULL,
  `plusFixed` decimal(13,6) DEFAULT NULL,
  `threshold` decimal(13,0) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`currencyId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `CURRENCY`
--

INSERT INTO `CURRENCY` (`currencyId`, `name`, `isDefault`, `isoCode`, `htmlCode`, `sign`, `blank`, `format`, `decimals`, `conversionRate`, `plusFixed`, `threshold`, `status`) VALUES
(3, 'Euro', 0, 'EUR', '&#8364;', 'ÃƒÆ’Ã†â€', 1, 2, 1, '2.200000', '5.000000', '50', 'ACTIVE'),
(2, 'Dollar', 0, 'USD', '&#36;', '$', 0, 1, 1, '1.660000', '5.000000', '300', 'ACTIVE'),
(1, 'Pound', 1, 'GBP', '&#163;', 'ÃƒÆ’Ã†â€', 0, 1, 1, '0.800000', NULL, NULL, 'ACTIVE'),
(4, 'Euro', 0, 'EUR', '&#8364;', 'ÃƒÆ’Ã†â€', 1, 2, 1, '2.200000', '5.000000', '50', 'ACTIVE'),
(5, 'Euro', 0, 'EUR', '&#8364;', 'ÃƒÆ’Ã†â€', 1, 2, 1, '2.200000', '5.000000', '50', 'ACTIVE');
