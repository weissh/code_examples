DROP TABLE IF EXISTS `CATEGORIES_R_CLIENTS_Websites`;
CREATE TABLE IF NOT EXISTS `CATEGORIES_R_CLIENTS_Websites` (
  `categoryWebsiteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) unsigned NOT NULL,
  `websiteId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`categoryWebsiteId`),
  KEY `Index 2` (`websiteId`),
  KEY `FK_CATEGORIES_R_CLIENTS_Websites_CATEGORIES` (`categoryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1611 ;

--
-- Dumping data for table `CATEGORIES_R_CLIENTS_Websites`
--

INSERT INTO `CATEGORIES_R_CLIENTS_Websites` (`categoryWebsiteId`, `categoryId`, `websiteId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 1),
(7, 2, 2),
(8, 2, 3),
(9, 2, 4),
(10, 2, 5),
(11, 3, 1),
(12, 3, 2);
