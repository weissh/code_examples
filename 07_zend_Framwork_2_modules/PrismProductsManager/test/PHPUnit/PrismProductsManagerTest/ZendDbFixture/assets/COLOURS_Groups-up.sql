DROP TABLE IF EXISTS `COLOURS_Groups`;
CREATE TABLE IF NOT EXISTS `COLOURS_Groups` (
  `colourGroupId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupCode` char(30) NOT NULL,
  `hasImage` tinyint(1) NOT NULL DEFAULT '0',
  `hex` varchar(7) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `imageId` int(11) DEFAULT NULL,
  PRIMARY KEY (`colourGroupId`),
  UNIQUE KEY `colourGroupId` (`colourGroupId`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=155 ;

--
-- Dumping data for table `COLOURS_Groups`
--

INSERT INTO `COLOURS_Groups` (`colourGroupId`, `groupCode`, `hasImage`, `hex`, `enabled`, `imageId`) VALUES
(1, 'NOC', 0, '#FFFFFF', 1, 0),
(2, 'AUB', 0, '#4D013C', 1, 0),
(3, 'BEI', 0, '#D1A248', 1, 0),
(4, 'BLK', 0, '#000000', 1, 0),
(5, 'BLU', 0, '#1C277C', 1, 0),
(6, 'BRW', 0, '#4D300A', 1, 0),
(7, 'BUR', 0, '#7A1212', 1, 0),
(8, 'CAM', 0, '#B4783E', 1, 0),
(9, 'CHA', 0, '#1A1C22', 1, 0),
(10, 'CLE', 0, '#FFFFFF', 1, 0),
(11, 'COG', 0, '#C66A13', 1, 0);
