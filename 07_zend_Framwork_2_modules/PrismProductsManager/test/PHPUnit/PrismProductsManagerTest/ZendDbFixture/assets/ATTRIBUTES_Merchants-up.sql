DROP TABLE IF EXISTS `ATTRIBUTES_Merchants`;
CREATE TABLE IF NOT EXISTS `ATTRIBUTES_Merchants` (
  `attributeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attributeGroupId` int(10) unsigned NOT NULL,
  `merchantAttributeId` int(10) NOT NULL,
  PRIMARY KEY (`attributeId`),
  KEY `attributeGroupId` (`attributeGroupId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=634 ;

--
-- Dumping data for table `ATTRIBUTES_Merchants`
--

INSERT INTO `ATTRIBUTES_Merchants` (`attributeId`, `attributeGroupId`, `merchantAttributeId`) VALUES
(205, 146, 1),
(206, 146, 2),
(207, 146, 3),
(208, 146, 4),
(209, 146, 5),
(210, 146, 6),
(211, 146, 7),
(212, 146, 8),
(213, 146, 9),
(214, 146, 10);


ALTER TABLE `ATTRIBUTES_Merchants`
  ADD CONSTRAINT `ATTRIBUTES_Merchants_ibfk_1` FOREIGN KEY (`attributeGroupId`) REFERENCES `ATTRIBUTES_Groups` (`attributeGroupId`) ON DELETE NO ACTION ON UPDATE NO ACTION;
