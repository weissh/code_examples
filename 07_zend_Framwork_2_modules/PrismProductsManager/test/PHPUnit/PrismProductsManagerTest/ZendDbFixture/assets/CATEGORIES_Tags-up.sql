DROP TABLE IF EXISTS `CATEGORIES_Tags`;
CREATE TABLE IF NOT EXISTS `CATEGORIES_Tags` (
  `categoriesTagId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoriesDefinitionsId` int(10) unsigned NOT NULL DEFAULT '0',
  `alias` varchar(50) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL DEFAULT 'ACTIVE',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`categoriesTagId`),
  KEY `FK_CATEGORIES_Tags_CATEGORIES_Definitions` (`categoriesDefinitionsId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `CATEGORIES_Tags`
--

INSERT INTO `CATEGORIES_Tags` (`categoriesTagId`, `categoriesDefinitionsId`, `alias`, `status`, `created`, `modified`) VALUES
(4, 267, 'New Arrivals Shoes', 'ACTIVE', '2015-02-12 13:56:10', '2015-02-12 06:56:10'),
(5, 268, 'spring summer 2015 boots', 'ACTIVE', '2015-02-12 13:56:24', '2015-02-12 06:56:24'),
(8, 266, 'New Arrivals', 'ACTIVE', '2015-02-20 14:59:20', '2015-02-20 07:59:20'),
(9, 271, 'paukl', 'INACTIVE', '2015-03-18 12:15:29', '2015-03-18 05:15:29'),
(11, 282, 'asfsafsaf', 'ACTIVE', '2015-04-13 15:53:17', '2015-04-13 08:53:17'),
(12, 283, 'safsa', 'INACTIVE', '2015-04-14 08:28:51', '2015-04-14 01:28:51'),
(13, 284, 'QA MAR CAT 1', 'ACTIVE', '2015-04-23 12:24:37', '2015-04-23 05:24:37'),
(14, 285, 'Child QA MAR', 'ACTIVE', '2015-04-23 12:25:22', '2015-04-23 05:25:22');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `CATEGORIES_Tags`
--
ALTER TABLE `CATEGORIES_Tags`
  ADD CONSTRAINT `FK_CATEGORIES_Tags_CATEGORIES_Definitions` FOREIGN KEY (`categoriesDefinitionsId`) REFERENCES `CATEGORIES_Definitions` (`categoryDefinitionsId`) ON DELETE NO ACTION;
