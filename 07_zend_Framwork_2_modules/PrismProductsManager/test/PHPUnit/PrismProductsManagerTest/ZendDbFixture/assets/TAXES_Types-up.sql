DROP TABLE IF EXISTS `TAXES_Types`;
CREATE TABLE IF NOT EXISTS `TAXES_Types` (
  `taxTypeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `calculateFrom` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Taxes could be calculated from prices or external source',
  `type` enum('NUMBER','PERCENT') NOT NULL DEFAULT 'PERCENT',
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`taxTypeId`),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `TAXES_Types`
--

INSERT INTO `TAXES_Types` (`taxTypeId`, `calculateFrom`, `type`, `status`, `created`, `modified`) VALUES
(1, 0, 'PERCENT', 'ACTIVE', '2014-09-09 11:39:35', '2014-09-29 10:21:39'),
(2, 0, 'NUMBER', 'ACTIVE', '2014-09-09 13:20:58', '2014-09-29 08:41:10');
