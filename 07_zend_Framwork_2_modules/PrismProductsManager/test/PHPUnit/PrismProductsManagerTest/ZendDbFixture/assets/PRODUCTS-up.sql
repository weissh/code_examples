DROP TABLE IF EXISTS `PRODUCTS`;
CREATE TABLE IF NOT EXISTS `PRODUCTS` (
  `productId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `manufacturerId` int(10) unsigned DEFAULT NULL,
  `productTypeId` int(11) unsigned NOT NULL,
  `productMerchantCategoryId` int(11) unsigned DEFAULT '0',
  `taxId` int(10) unsigned NOT NULL,
  `ean13` varchar(30) DEFAULT NULL,
  `ecoTax` decimal(17,2) NOT NULL DEFAULT '0.00',
  `quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `style` varchar(30) NOT NULL,
  `sku` varchar(32) DEFAULT NULL,
  `supplierReference` varchar(32) DEFAULT NULL,
  `weight` int(10) DEFAULT NULL,
  `onSale` int(10) DEFAULT NULL,
  `outOfStock` int(10) DEFAULT NULL,
  `allowBackOrder` int(10) DEFAULT NULL,
  `customizable` int(10) DEFAULT NULL,
  `uploadableFiles` int(10) DEFAULT NULL,
  `applyPostage` int(10) DEFAULT NULL,
  `quantityDiscount` int(10) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL DEFAULT 'INACTIVE',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`productId`),
  KEY `manufacturerId` (`manufacturerId`),
  KEY `taxId` (`taxId`),
  KEY `created` (`created`),
  KEY `FK_PRODUCTS_PRODUCTS_Merchant_Categories` (`productMerchantCategoryId`),
  KEY `FK_PRODUCTS_PRODUCTS_Types` (`productTypeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `PRODUCTS`
--

INSERT INTO `PRODUCTS` (`productId`, `manufacturerId`, `productTypeId`, `productMerchantCategoryId`, `taxId`, `ean13`, `ecoTax`, `quantity`, `style`, `sku`, `supplierReference`, `weight`, `onSale`, `outOfStock`, `allowBackOrder`, `customizable`, `uploadableFiles`, `applyPostage`, `quantityDiscount`, `status`, `created`, `modified`) VALUES
(1, 1, 1, 31, 0, '5055347347580', '0.00', 0, 'ALESDE', 'ALESDEBRW060', 'BER01', 0, NULL, NULL, 0, 1, NULL, NULL, NULL, 'INACTIVE', '0000-00-00 00:00:00', '2015-03-16 04:11:55'),
(2, 1, 1, 29, 0, '5055347368769', '0.00', 0, 'ALEXGL', 'ALEXGLTAN060', 'BER01', 0, NULL, NULL, 0, 1, NULL, NULL, NULL, 'INACTIVE', '0000-00-00 00:00:00', '2015-03-16 04:11:50'),
(3, 1, 1, 31, 0, '5055347374500', '0.00', 0, 'ARPSBL', 'ARPSBLFUO060', 'IND01', 0, NULL, NULL, 0, 1, NULL, NULL, NULL, 'INACTIVE', '0000-00-00 00:00:00', '2015-03-16 04:11:55'),
(4, 1, 1, 29, 0, '5055347371004', '0.00', 0, 'BALHSD', 'BALHSDGRY060', 'KEN01', 0, NULL, NULL, 0, 1, NULL, NULL, NULL, 'INACTIVE', '0000-00-00 00:00:00', '2015-03-16 04:11:50'),
(5, 1, 1, 4, 0, '5055347319167', '0.00', 0, 'BENSST', 'BENSSTBLK001', 'JPI01', 0, NULL, NULL, 0, 1, NULL, NULL, NULL, 'INACTIVE', '0000-00-00 00:00:00', '2015-03-20 09:08:52'),
(6, 1, 1, 11, 0, '5055347356148', '0.00', 0, 'BLOCXX', 'BLOCXXCHA001', 'ERD01', 0, NULL, NULL, 0, 1, NULL, NULL, NULL, 'INACTIVE', '0000-00-00 00:00:00', '2015-03-16 04:11:46'),
(7, 1, 1, 31, 0, '5055347302664', '0.00', 0, 'BOLOGL', 'BOLOGLBLK060', 'BER01', 0, NULL, NULL, 0, 1, NULL, NULL, NULL, 'INACTIVE', '0000-00-00 00:00:00', '2015-03-16 04:11:55');
