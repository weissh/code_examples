DROP TABLE IF EXISTS `COLOURS`;
CREATE TABLE IF NOT EXISTS `COLOURS` (
  `colourId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `colourCode` varchar(5) NOT NULL,
  `hex` varchar(7) NOT NULL,
  PRIMARY KEY (`colourId`),
  UNIQUE KEY `colourId` (`colourId`) USING BTREE,
  UNIQUE KEY `colourCode` (`colourCode`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=161 ;

--
-- Dumping data for table `COLOURS`
--

INSERT INTO `COLOURS` (`colourId`, `colourCode`, `hex`) VALUES
(2, 'AUB', '#5F0090'),
(3, 'BEI', '#D5AA81'),
(4, 'BLK', '#000000'),
(5, 'BLU', '#0D00FF'),
(6, 'BRW', '#4D300A'),
(7, 'BUR', '#8C001A'),
(8, 'CAM', '#C19A6B'),
(9, 'CHA', '#36454F'),
(10, 'CLE', '#FFFFFF');
