DROP TABLE IF EXISTS `ATTRIBUTES_Definitions`;
CREATE TABLE IF NOT EXISTS `ATTRIBUTES_Definitions` (
  `attributesDefinitionsId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `languageId` int(10) unsigned NOT NULL,
  `attributeId` int(10) unsigned NOT NULL,
  `attributeValue` varchar(150) NOT NULL,
  `attributeCode` varchar(50) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL DEFAULT 'ACTIVE',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`attributesDefinitionsId`),
  KEY `Index 2` (`languageId`),
  KEY `Index 3` (`attributeId`),
  KEY `attributeValue` (`attributeValue`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=962 ;

--
-- Dumping data for table `ATTRIBUTES_Definitions`
--

INSERT INTO `ATTRIBUTES_Definitions` (`attributesDefinitionsId`, `languageId`, `attributeId`, `attributeValue`, `status`, `created`, `modified`) VALUES
(1, 1, 1, 'Allow Express', 'ACTIVE', '0000-00-00 00:00:00', '2014-09-18 09:16:38'),
(9, 1, 5, 'Allow Back Orders', 'ACTIVE', '0000-00-00 00:00:00', '2014-09-17 02:53:50'),
(11, 1, 6, 'Suppress From Sale', 'ACTIVE', '0000-00-00 00:00:00', '2014-09-17 02:54:24'),
(13, 1, 7, 'Screening Qty', 'ACTIVE', '0000-00-00 00:00:00', '2014-09-17 02:55:22'),
(17, 1, 9, 'Height', 'ACTIVE', '0000-00-00 00:00:00', '2014-09-17 02:57:10'),
(19, 1, 10, 'Width', 'ACTIVE', '0000-00-00 00:00:00', '2014-09-17 02:57:32');

--
-- Constraints for table `ATTRIBUTES_Definitions`
--
ALTER TABLE `ATTRIBUTES_Definitions`
  ADD CONSTRAINT `ATTRIBUTES_Definitions_ibfk_1` FOREIGN KEY (`attributeId`) REFERENCES `ATTRIBUTES` (`attributeId`) ON DELETE NO ACTION ON UPDATE NO ACTION;
