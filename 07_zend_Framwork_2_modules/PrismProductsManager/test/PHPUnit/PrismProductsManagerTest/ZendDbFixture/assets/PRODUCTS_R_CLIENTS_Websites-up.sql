DROP TABLE IF EXISTS `PRODUCTS_R_CLIENTS_Websites`;
CREATE TABLE IF NOT EXISTS `PRODUCTS_R_CLIENTS_Websites` (
  `productWebsiteId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productIdFrom` enum('PRODUCTS','PRODUCTS_R_ATTRIBUTES') NOT NULL,
  `productId` int(11) unsigned NOT NULL,
  `websiteId` int(10) unsigned NOT NULL,
  `elasticSearchIndexId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`productWebsiteId`),
  KEY `Index 2` (`websiteId`),
  KEY `Index 3` (`productId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5426 ;

--
-- Dumping data for table `PRODUCTS_R_CLIENTS_Websites`
--

INSERT INTO `PRODUCTS_R_CLIENTS_Websites` (`productWebsiteId`, `productIdFrom`, `productId`, `websiteId`, `elasticSearchIndexId`) VALUES
(1, 'PRODUCTS', 1, 1, 0),
(2, 'PRODUCTS', 1, 2, 0),
(3, 'PRODUCTS', 1, 3, 0),
(4, 'PRODUCTS', 1, 4, 0),
(5, 'PRODUCTS', 1, 5, 0),
(6, 'PRODUCTS', 2, 1, 0),
(7, 'PRODUCTS', 2, 2, 0),
(8, 'PRODUCTS', 2, 3, 0),
(9, 'PRODUCTS', 2, 4, 0);
