DROP TABLE IF EXISTS `TAXES`;
CREATE TABLE IF NOT EXISTS `TAXES` (
  `taxId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `taxType` int(11) unsigned NOT NULL,
  `taxTerritory` varchar(4) NOT NULL DEFAULT '',
  `taxValue` double(10,2) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`taxId`),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `TAXES`
--

INSERT INTO `TAXES` (`taxId`, `taxType`, `taxTerritory`, `taxValue`, `status`, `created`, `modified`) VALUES
(1, 1, 'GB', 20.00, 'ACTIVE', '0000-00-00 00:00:00', '2014-10-03 04:00:37'),
(2, 1, 'US', 20.00, 'INACTIVE', '0000-00-00 00:00:00', '2015-04-23 05:17:37'),
(3, 1, 'DE', 20.00, 'ACTIVE', '0000-00-00 00:00:00', '2014-10-07 07:31:23'),
(4, 1, 'DE', 20.00, 'ACTIVE', '0000-00-00 00:00:00', '2014-10-10 04:31:27');
