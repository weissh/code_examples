DROP TABLE IF EXISTS `TAXES_Types_Definitions`;
CREATE TABLE IF NOT EXISTS `TAXES_Types_Definitions` (
  `taxTypeDefinitionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `languageId` int(10) unsigned NOT NULL,
  `taxTypeId` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL DEFAULT 'INACTIVE',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`taxTypeDefinitionId`),
  KEY `languageId` (`languageId`),
  KEY `name` (`name`),
  KEY `languageId_name` (`languageId`,`name`),
  KEY `taxTypeId` (`taxTypeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `TAXES_Types_Definitions`
--

INSERT INTO `TAXES_Types_Definitions` (`taxTypeDefinitionId`, `languageId`, `taxTypeId`, `name`, `description`, `status`, `created`, `modified`) VALUES
(1, 1, 1, 'VAT', 'Value added tax: a tax on goods and services', 'ACTIVE', '2014-09-09 11:37:51', '2014-09-09 06:22:19'),
(2, 1, 2, 'Duty', 'Tax that you must pay on something that you buy, or on something that you bring into one country from another country', 'INACTIVE', '2014-09-09 12:06:58', '2014-09-09 06:22:21');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `TAXES_Types_Definitions`
--
ALTER TABLE `TAXES_Types_Definitions`
  ADD CONSTRAINT `FK_TAXES_Types_Definitions_TAXES_Types` FOREIGN KEY (`taxTypeId`) REFERENCES `TAXES_Types` (`taxTypeId`) ON DELETE NO ACTION ON UPDATE NO ACTION;
