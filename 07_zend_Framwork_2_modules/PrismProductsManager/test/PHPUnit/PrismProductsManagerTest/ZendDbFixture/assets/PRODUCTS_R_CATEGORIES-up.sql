DROP TABLE IF EXISTS `PRODUCTS_R_CATEGORIES`;
CREATE TABLE IF NOT EXISTS `PRODUCTS_R_CATEGORIES` (
  `productCategoryId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productId` int(10) unsigned NOT NULL,
  `categoryId` int(10) unsigned NOT NULL,
  `isDefault` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `priority` int(10) unsigned NOT NULL DEFAULT '1',
  `productIdFrom` enum('PRODUCTS','PRODUCTS_R_ATTRIBUTES') NOT NULL DEFAULT 'PRODUCTS',
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL DEFAULT 'ACTIVE',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`productCategoryId`),
  KEY `FK_PRODUCTS_R_CATEGORIES_PRODUCTS` (`productId`),
  KEY `FK_PRODUCTS_R_CATEGORIES_CATEGORIES` (`categoryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=86137 ;

--
-- Dumping data for table `PRODUCTS_R_CATEGORIES`
--

INSERT INTO `PRODUCTS_R_CATEGORIES` (`productCategoryId`, `productId`, `categoryId`, `isDefault`, `priority`, `productIdFrom`, `status`, `created`, `modified`) VALUES
(1, 1, 183, 0, 1, 'PRODUCTS', 'DELETED', '2015-01-28 17:33:26', '2015-02-17 11:36:27'),
(2, 1, 141, 0, 1, 'PRODUCTS', 'DELETED', '2015-01-28 17:33:26', '2015-02-17 11:36:27'),
(3, 1, 142, 1, 1, 'PRODUCTS', 'ACTIVE', '2015-01-28 17:33:26', '2015-02-17 11:36:27'),
(4, 1, 183, 0, 1, 'PRODUCTS_R_ATTRIBUTES', 'DELETED', '2015-01-28 17:33:26', '2015-02-17 11:36:27'),
(5, 1, 141, 0, 1, 'PRODUCTS_R_ATTRIBUTES', 'DELETED', '2015-01-28 17:33:26', '2015-02-17 11:36:27'),
(6, 1, 142, 1, 1, 'PRODUCTS_R_ATTRIBUTES', 'ACTIVE', '2015-01-28 17:33:26', '2015-02-17 11:36:27'),
(7, 2, 183, 0, 1, 'PRODUCTS_R_ATTRIBUTES', 'DELETED', '2015-01-28 17:33:26', '2015-02-17 11:36:27'),
(8, 2, 141, 0, 1, 'PRODUCTS_R_ATTRIBUTES', 'DELETED', '2015-01-28 17:33:26', '2015-02-17 11:36:27'),
(9, 2, 142, 1, 1, 'PRODUCTS_R_ATTRIBUTES', 'ACTIVE', '2015-01-28 17:33:26', '2015-02-17 11:36:27'),
(10, 3, 183, 0, 1, 'PRODUCTS_R_ATTRIBUTES', 'DELETED', '2015-01-28 17:33:26', '2015-02-17 11:36:27'),
(11, 3, 141, 0, 1, 'PRODUCTS_R_ATTRIBUTES', 'DELETED', '2015-01-28 17:33:26', '2015-02-17 11:36:27');
