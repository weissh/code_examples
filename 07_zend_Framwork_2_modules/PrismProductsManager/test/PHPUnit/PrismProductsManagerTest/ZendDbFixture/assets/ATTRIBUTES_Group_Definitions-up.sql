DROP TABLE IF EXISTS `ATTRIBUTES_Group_Definitions`;
CREATE TABLE IF NOT EXISTS `ATTRIBUTES_Group_Definitions` (
  `attributeGroupDefinitionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attributeGroupId` int(10) unsigned NOT NULL DEFAULT '0',
  `languageId` int(10) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `publicName` varchar(64) NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL DEFAULT 'ACTIVE',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`attributeGroupDefinitionId`),
  KEY `Index 2` (`languageId`),
  KEY `Index 3` (`name`),
  KEY `attributeGroupId` (`attributeGroupId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=316 ;

--
-- Dumping data for table `ATTRIBUTES_Group_Definitions`
--

INSERT INTO `ATTRIBUTES_Group_Definitions` (`attributeGroupDefinitionId`, `attributeGroupId`, `languageId`, `name`, `publicName`, `status`, `created`, `modified`) VALUES
(1, 1, 1, 'Colours', 'Colours', 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, 1, 'Shoe Sizes', 'Shoe Sizes', 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, 1, 'Clothes Sizes', 'Clothes Sizes', 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, 1, 'Accessories Sizes', 'Accessories Sizes', 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 134, 1, 'Shirts Sizes', 'Shirts Sizes', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 07:10:21'),
(6, 146, 1, 'Colours', 'Colours', 'ACTIVE', '0000-00-00 00:00:00', '2015-02-10 09:45:10'),
(7, 5, 1, 'Allow Express', 'Allow Express', 'ACTIVE', '0000-00-00 00:00:00', '2014-09-17 02:50:41'),
(8, 9, 1, 'Allow Back Orders', 'Allow Back Orders', 'ACTIVE', '0000-00-00 00:00:00', '2014-09-17 02:53:50'),
(9, 10, 1, 'Suppress From Sale', 'Suppress From Sale', 'ACTIVE', '0000-00-00 00:00:00', '2014-09-17 02:54:24'),
(10, 11, 1, 'Screening Qty', 'Screening Qty', 'ACTIVE', '0000-00-00 00:00:00', '2014-09-17 02:55:22'),
(11, 13, 1, 'Height', 'Height In CM', 'ACTIVE', '0000-00-00 00:00:00', '2014-09-17 03:05:56'),
(12, 14, 1, 'Width', 'Width', 'ACTIVE', '0000-00-00 00:00:00', '2014-09-17 02:57:32'),
(13, 15, 1, 'Weight', 'Weight In Grammes', 'ACTIVE', '0000-00-00 00:00:00', '2014-09-17 03:06:30'),
(14, 17, 1, 'UK Delivery Only', 'UK Delivery Only', 'ACTIVE', '0000-00-00 00:00:00', '2014-09-17 03:08:24'),
(15, 145, 1, 'Free Delivery Text 1', 'Free Delivery Text 1', 'ACTIVE', '0000-00-00 00:00:00', '2014-10-31 07:39:47'),
(17, 147, 1, 'Despatch House', 'Despatch House', 'ACTIVE', '0000-00-00 00:00:00', '2014-10-30 04:22:00'),
(18, 148, 1, 'Length', 'Length', 'ACTIVE', '0000-00-00 00:00:00', '2014-10-30 06:43:33'),
(19, 149, 1, 'Commodity', 'Commodity', 'ACTIVE', '0000-00-00 00:00:00', '2014-10-30 04:24:11'),
(20, 150, 1, 'Country Of Origin', 'Country Of Origin', 'ACTIVE', '0000-00-00 00:00:00', '2014-10-30 04:25:16'),
(21, 151, 1, 'Heavy Item', 'Heavy Item', 'ACTIVE', '0000-00-00 00:00:00', '2014-10-30 04:48:55'),
(22, 152, 1, 'Free Postage', 'Free Postage', 'ACTIVE', '0000-00-00 00:00:00', '2014-10-30 04:52:56'),
(23, 153, 1, 'Free Delivery Text 2', 'Free Delivery Text 2', 'ACTIVE', '0000-00-00 00:00:00', '2014-10-30 07:06:47'),
(24, 155, 1, 'Belts Sizes', 'Belts Sizes', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 11:31:38'),
(293, 160, 1, 'All Belts', 'All Belts', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 11:40:30'),
(294, 161, 1, 'Formal', 'Formal', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 11:45:01'),
(295, 162, 1, 'All Accessories', 'All Accessories', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 11:46:10'),
(296, 163, 1, 'All Clothes', 'All Clothes', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 11:46:43'),
(297, 164, 1, 'All Shoes', 'All Shoes', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 11:47:13'),
(298, 165, 1, 'Brand', 'Brand', 'ACTIVE', '0000-00-00 00:00:00', '2015-02-17 03:42:51'),
(299, 166, 1, 'Material', 'Material', 'ACTIVE', '0000-00-00 00:00:00', '2015-02-17 03:47:30'),
(300, 167, 1, 'Fastening', 'Fastening', 'ACTIVE', '0000-00-00 00:00:00', '2015-02-17 03:48:21'),
(301, 168, 1, 'Sole Material', 'Sole Material', 'DELETED', '0000-00-00 00:00:00', '2015-02-17 03:49:19'),
(302, 169, 1, 'Sole Material', 'Sole Material', 'ACTIVE', '0000-00-00 00:00:00', '2015-02-17 03:50:27'),
(303, 170, 1, 'Style', 'Style', 'ACTIVE', '0000-00-00 00:00:00', '2015-02-17 08:59:59'),
(304, 171, 1, 'Supplier', 'Supplier', 'ACTIVE', '0000-00-00 00:00:00', '2015-02-18 04:37:27'),
(305, 172, 1, 'Age Restricted', 'Age Restricted', 'ACTIVE', '0000-00-00 00:00:00', '2015-02-18 06:25:40'),
(306, 173, 1, 'Is Johnnie Walker Product', 'Is Johnnie Walker Product', 'ACTIVE', '0000-00-00 00:00:00', '2015-02-18 06:26:12'),
(307, 174, 1, 'Jackets Sizes', 'Jackets Sizes', 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(308, 175, 1, 'All Jackets', 'All Jackets', 'ACTIVE', '0000-00-00 00:00:00', '2015-03-10 05:04:04'),
(309, 176, 1, 'Trousers Sizes', 'Trousers Sizes', 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(310, 178, 1, 'All Trousers', 'All Trousers', 'ACTIVE', '0000-00-00 00:00:00', '2015-03-10 05:04:04'),
(311, 179, 1, 'All Swim Shorts', 'All Swim Shorts', 'ACTIVE', '0000-00-00 00:00:00', '2015-03-10 05:04:04'),
(312, 180, 1, 'All Swim Shorts', 'All Swim Shorts', 'ACTIVE', '0000-00-00 00:00:00', '2015-04-20 03:08:56'),
(313, 181, 1, 'Sleeve Length', 'Sleeve Length', 'ACTIVE', '0000-00-00 00:00:00', '2015-04-23 05:28:24'),
(314, 182, 1, 'QA Merch group', 'QA Merch group', 'ACTIVE', '0000-00-00 00:00:00', '2015-04-23 05:33:12'),
(315, 183, 1, 'Base colours', 'Base colours', 'ACTIVE', '0000-00-00 00:00:00', '2015-04-23 05:34:44');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ATTRIBUTES_Group_Definitions`
--
ALTER TABLE `ATTRIBUTES_Group_Definitions`
  ADD CONSTRAINT `ATTRIBUTES_Group_Definitions_ibfk_1` FOREIGN KEY (`attributeGroupId`) REFERENCES `ATTRIBUTES_Groups` (`attributeGroupId`) ON DELETE NO ACTION ON UPDATE NO ACTION;
