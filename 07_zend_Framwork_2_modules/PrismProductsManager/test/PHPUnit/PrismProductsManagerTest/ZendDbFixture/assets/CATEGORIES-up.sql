DROP TABLE IF EXISTS `CATEGORIES`;
CREATE TABLE IF NOT EXISTS `CATEGORIES` (
  `categoryId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `categoryParentId` int(11) unsigned NOT NULL DEFAULT '0',
  `treeDepth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `position` int(11) unsigned NOT NULL DEFAULT '0',
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL DEFAULT 'ACTIVE',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `oldCategoryId` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`categoryId`),
  KEY `categoryAndParentCategory` (`categoryParentId`,`categoryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=286 ;

--
-- Dumping data for table `CATEGORIES`
--

INSERT INTO `CATEGORIES` (`categoryId`, `categoryParentId`, `treeDepth`, `position`, `status`, `created`, `modified`, `oldCategoryId`) VALUES
(1, 1, 0, 1, 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 06:53:24', 0),
(2, 1, 1, 1, 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 06:53:24', 1),
(3, 2, 2, 1, 'DELETED', '0000-00-00 00:00:00', '2015-02-12 06:44:55', 42),
(4, 2, 2, 1, 'DELETED', '0000-00-00 00:00:00', '2015-02-09 03:16:53', 46),
(5, 2, 3, 1, 'ACTIVE', '0000-00-00 00:00:00', '2015-02-09 03:16:53', 47),
(6, 2, 2, 1, 'DELETED', '0000-00-00 00:00:00', '2015-02-09 03:18:08', 72),
(7, 2, 3, 1, 'DELETED', '0000-00-00 00:00:00', '2015-02-12 07:43:50', 76),
(8, 2, 2, 1, 'DELETED', '0000-00-00 00:00:00', '2015-02-12 07:44:01', 73);
