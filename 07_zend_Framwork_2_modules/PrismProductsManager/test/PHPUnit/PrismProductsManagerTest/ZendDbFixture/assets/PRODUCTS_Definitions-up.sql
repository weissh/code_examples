DROP TABLE IF EXISTS `PRODUCTS_Definitions`;
CREATE TABLE IF NOT EXISTS `PRODUCTS_Definitions` (
  `productDefinitionsId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productId` int(10) unsigned NOT NULL,
  `productIdFrom` enum('PRODUCTS_R_ATTRIBUTES','PRODUCTS') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PRODUCTS',
  `languageId` int(10) unsigned NOT NULL,
  `versionId` int(10) unsigned NOT NULL,
  `versionDescription` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shortProductName` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `productName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `urlName` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `metaTitle` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `metaDescription` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `metaKeyword` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `shortDescription` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `longDescription` text COLLATE utf8_unicode_ci NOT NULL,
  `misSpells` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') COLLATE utf8_unicode_ci NOT NULL,
  `makeLive` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`productDefinitionsId`),
  KEY `FK_PRODUCTS_Definitions_PRODUCTS` (`productId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16115 ;

--
-- Dumping data for table `PRODUCTS_Definitions`
--

INSERT INTO `PRODUCTS_Definitions` (`productDefinitionsId`, `productId`, `productIdFrom`, `languageId`, `versionId`, `versionDescription`, `shortProductName`, `productName`, `urlName`, `metaTitle`, `metaDescription`, `metaKeyword`, `shortDescription`, `longDescription`, `misSpells`, `tags`, `status`, `makeLive`, `created`, `modified`) VALUES
(1, 1, 'PRODUCTS', 1, 1, '', 'Alessi Brown', 'Alessi Suede Shoe', 'alessi-suede-shoe', 'Alessi Suede Shoe', 'Alessi Suede Shoe', '', 'Alessi Brown', 'A 5-eyelet toe cap shoe. Available in oiled suede or leather, either option is a step in the right direction for a great casual look', '', '', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 17:33:26', '2015-02-18 04:09:39'),
(2, 1, 'PRODUCTS_R_ATTRIBUTES', 1, 1, '', 'Alessi Brown', 'Alessi Suede Shoe', 'alessi-suede-shoe', 'Alessi Suede Shoe', 'Alessi Suede Shoe', '', 'Alessi Brown', 'A 5-eyelet toe cap shoe. Available in oiled suede or leather, either option is a step in the right direction for a great casual look.', '', '', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 17:33:26', '2015-02-24 04:54:19'),
(3, 2, 'PRODUCTS_R_ATTRIBUTES', 1, 1, '', 'Alessi Brown', 'Alessi Suede Shoe', 'alessi-suede-shoe', 'Alessi Suede Shoe', 'Alessi Suede Shoe', '', 'Alessi Brown', 'A 5-eyelet toe cap shoe. Available in oiled suede or leather, either option is a step in the right direction for a great casual look.', '', '', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 17:33:26', '2015-02-24 04:54:19'),
(4, 3, 'PRODUCTS_R_ATTRIBUTES', 1, 1, '', 'Alessi Brown', 'Alessi Suede Shoe', 'alessi-suede-shoe', 'Alessi Suede Shoe', 'Alessi Suede Shoe', '', 'Alessi Brown', 'A 5-eyelet toe cap shoe. Available in oiled suede or leather, either option is a step in the right direction for a great casual look.', '', '', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 17:33:26', '2015-02-24 04:54:19'),
(5, 4, 'PRODUCTS_R_ATTRIBUTES', 1, 1, '', 'Alessi Brown', 'Alessi Suede Shoe', 'alessi-suede-shoe', 'Alessi Suede Shoe', 'Alessi Suede Shoe', '', 'Alessi Brown', 'A 5-eyelet toe cap shoe. Available in oiled suede or leather, either option is a step in the right direction for a great casual look.', '', '', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 17:33:26', '2015-02-24 04:54:19'),
(6, 5, 'PRODUCTS_R_ATTRIBUTES', 1, 1, '', 'Alessi Brown', 'Alessi Suede Shoe', 'alessi-suede-shoe', 'Alessi Suede Shoe', 'Alessi Suede Shoe', '', 'Alessi Brown', 'A 5-eyelet toe cap shoe. Available in oiled suede or leather, either option is a step in the right direction for a great casual look.', '', '', 'INACTIVE', '0000-00-00 00:00:00', '2015-01-28 17:33:26', '2015-03-20 09:09:22'),
(7, 6, 'PRODUCTS_R_ATTRIBUTES', 1, 1, '', 'Alessi Brown', 'Alessi Suede Shoe', 'alessi-suede-shoe', 'Alessi Suede Shoe', 'Alessi Suede Shoe', '', 'Alessi Brown', 'A 5-eyelet toe cap shoe. Available in oiled suede or leather, either option is a step in the right direction for a great casual look.', '', '', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 17:33:26', '2015-02-24 04:54:19'),
(8, 7, 'PRODUCTS_R_ATTRIBUTES', 1, 1, '', 'Alessi Brown', 'Alessi Suede Shoe', 'alessi-suede-shoe', 'Alessi Suede Shoe', 'Alessi Suede Shoe', '', 'Alessi Brown', 'A 5-eyelet toe cap shoe. Available in oiled suede or leather, either option is a step in the right direction for a great casual look.', '', '', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 17:33:26', '2015-02-24 04:54:19'),
(9, 8, 'PRODUCTS_R_ATTRIBUTES', 1, 1, '', 'Alessi Brown', 'Alessi Suede Shoe', 'alessi-suede-shoe', 'Alessi Suede Shoe', 'Alessi Suede Shoe', '', 'Alessi Brown', 'A 5-eyelet toe cap shoe. Available in oiled suede or leather, either option is a step in the right direction for a great casual look.', '', '', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 17:33:26', '2015-02-24 04:54:19'),
(10, 9, 'PRODUCTS_R_ATTRIBUTES', 1, 1, '', 'Alessi Brown', 'Alessi Suede Shoe', 'alessi-suede-shoe', 'Alessi Suede Shoe', 'Alessi Suede Shoe', '', 'Alessi Brown', 'A 5-eyelet toe cap shoe. Available in oiled suede or leather, either option is a step in the right direction for a great casual look.', '', '', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 17:33:26', '2015-02-24 04:54:19'),
(11, 10, 'PRODUCTS_R_ATTRIBUTES', 1, 1, '', 'Alessi Brown', 'Alessi Suede Shoe', 'alessi-suede-shoe', 'Alessi Suede Shoe', 'Alessi Suede Shoe', '', 'Alessi Brown', 'A 5-eyelet toe cap shoe. Available in oiled suede or leather, either option is a step in the right direction for a great casual look.', '', '', 'ACTIVE', '0000-00-00 00:00:00', '2015-01-28 17:33:27', '2015-02-24 04:54:19');
