DROP TABLE IF EXISTS `PRODUCTS_Merchant_Categories_Definitions`;
CREATE TABLE IF NOT EXISTS `PRODUCTS_Merchant_Categories_Definitions` (
  `productMerchantCategoryDefinitionId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `productMerchantCategoryId` int(3) unsigned NOT NULL,
  `languageId` tinyint(1) unsigned NOT NULL,
  `categoryName` varchar(30) NOT NULL,
  `shortDescription` varchar(150) DEFAULT NULL,
  `fullDescription` text,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`productMerchantCategoryDefinitionId`),
  KEY `productsMerchantCategoriesId` (`productMerchantCategoryId`),
  KEY `languageId` (`languageId`),
  KEY `categoryName` (`categoryName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `PRODUCTS_Merchant_Categories_Definitions`
--

INSERT INTO `PRODUCTS_Merchant_Categories_Definitions` (`productMerchantCategoryDefinitionId`, `productMerchantCategoryId`, `languageId`, `categoryName`, `shortDescription`, `fullDescription`, `created`, `modified`) VALUES
(1, 1, 1, 'Unspecified', 'Unspecified', 'Unspecified', '0000-00-00 00:00:00', '2015-01-28 11:14:07'),
(2, 2, 1, 'Unspecified', 'Unspecified', 'Unspecified', '0000-00-00 00:00:00', '2015-01-28 11:14:22'),
(3, 3, 1, 'Accessories', 'Accessories', 'Accessories', '0000-00-00 00:00:00', '2015-01-28 11:14:58'),
(4, 4, 1, 'Slg', 'Slg', 'Slg', '0000-00-00 00:00:00', '2015-04-01 06:36:10'),
(5, 5, 1, 'Ties', 'Ties', 'Ties', '0000-00-00 00:00:00', '2015-03-17 03:30:47'),
(6, 6, 1, 'Umbrella', 'Umbrella', 'Umbrella', '0000-00-00 00:00:00', '2015-03-12 10:08:33'),
(7, 7, 1, 'Grooming', 'Grooming', 'Grooming', '0000-00-00 00:00:00', '2015-01-28 11:14:58'),
(8, 8, 1, 'Wallet', 'Wallet', 'Wallet', '0000-00-00 00:00:00', '2015-01-28 11:14:58'),
(9, 9, 1, 'Sunglasses', 'Sunglasses', 'Sunglasses', '0000-00-00 00:00:00', '2015-03-13 07:11:52'),
(10, 10, 1, 'Sock', 'Sock', 'Sock', '0000-00-00 00:00:00', '2015-03-05 10:20:54'),
(11, 11, 1, 'Scarf', 'Scarf', 'Scarf', '0000-00-00 00:00:00', '2015-04-01 09:36:13'),
(12, 12, 1, 'Key Ring', 'Key Ring', 'Key Ring', '0000-00-00 00:00:00', '2015-01-28 11:14:58'),
(13, 13, 1, 'Belt', 'Belt', 'Belt', '0000-00-00 00:00:00', '2015-01-28 11:14:58'),
(14, 14, 1, 'Care Product', 'Care Product', 'Care Product', '0000-00-00 00:00:00', '2015-01-28 11:14:58'),
(15, 15, 1, 'Cufflinks', 'Cufflinks', 'Cufflinks', '0000-00-00 00:00:00', '2015-03-31 07:00:45'),
(16, 16, 1, 'Gift', 'Gift', 'Gift', '0000-00-00 00:00:00', '2015-03-26 08:57:49'),
(17, 17, 1, 'Glove', 'Glove', 'Glove', '0000-00-00 00:00:00', '2015-03-04 10:05:31'),
(18, 18, 1, 'Hat', 'Hat', 'Hat', '0000-00-00 00:00:00', '2015-03-31 05:39:52'),
(19, 19, 1, 'Bag', 'Bag', 'Bag', '0000-00-00 00:00:00', '2015-01-28 11:14:58'),
(20, 20, 1, 'Care Products', 'Care Products', 'Care Products', '0000-00-00 00:00:00', '2015-01-28 11:15:00'),
(21, 21, 1, 'Care Product', 'Care Product', 'Care Product', '0000-00-00 00:00:00', '2015-03-05 03:19:19'),
(22, 22, 1, 'Shoe Lace', 'Shoe Lace', 'Shoe Lace', '0000-00-00 00:00:00', '2015-01-28 11:15:00'),
(23, 23, 1, 'Footwear', 'Footwear', 'Footwear', '0000-00-00 00:00:00', '2015-01-28 11:15:00'),
(24, 24, 1, 'Trainer', 'Trainer', 'Trainer', '0000-00-00 00:00:00', '2015-01-28 11:15:00'),
(25, 25, 1, 'Glove', 'Glove', 'Glove', '0000-00-00 00:00:00', '2015-03-04 10:05:16'),
(26, 26, 1, 'Moccasin', 'Moccasin', 'Moccasin', '0000-00-00 00:00:00', '2015-01-28 11:15:00'),
(27, 27, 1, 'Casual', 'Casual', 'Casual', '0000-00-00 00:00:00', '2015-01-28 11:15:00'),
(28, 28, 1, 'Slipper', 'Slipper', 'Slipper', '0000-00-00 00:00:00', '2015-01-28 11:15:00'),
(29, 29, 1, 'Boots', 'Boots', 'Boots', '0000-00-00 00:00:00', '2015-01-28 11:15:00'),
(30, 30, 1, 'Sandal', 'Sandal', 'Sandal', '0000-00-00 00:00:00', '2015-01-28 11:15:00'),
(31, 31, 1, 'Shoe', 'Shoe', 'Shoe', '0000-00-00 00:00:00', '2015-01-28 11:15:00'),
(32, 32, 1, 'not active', 'not active', 'not active', '0000-00-00 00:00:00', '2015-01-28 11:15:00'),
(33, 33, 1, 'Outerwear', 'Outerwear', 'Outerwear', '0000-00-00 00:00:00', '2015-02-10 09:43:36'),
(34, 34, 1, 'Shirts', 'Shirts', 'Shirts', '0000-00-00 00:00:00', '2015-03-12 10:01:55'),
(35, 35, 1, 'Polo &amp; Tees', 'Polo &amp; Tees', 'Polo &amp; Tees', '0000-00-00 00:00:00', '2015-02-27 04:40:28'),
(36, 36, 1, 'Outerwear', 'Outerwear', 'Outerwear', '0000-00-00 00:00:00', '2015-01-28 11:15:01'),
(37, 37, 1, 'Jackets and Coats', 'Jackets and Coats', 'Jackets and Coats', '0000-00-00 00:00:00', '2015-04-02 09:01:52'),
(38, 38, 1, 'Leather', 'Leather', 'Leather', '0000-00-00 00:00:00', '2015-03-05 10:20:30'),
(39, 39, 1, 'Components', 'Components', 'Components', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(40, 40, 1, 'Packaging', 'Packaging', 'Packaging', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(41, 41, 1, 'Shoe Lace', 'Shoe Lace', 'Shoe Lace', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(42, 42, 1, 'Components', 'Components', 'Components', '0000-00-00 00:00:00', '2015-03-25 06:13:11'),
(43, 43, 1, 'DELETED', 'DELETED', 'DELETED', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(44, 44, 1, 'Cufflinks', 'Cufflinks', 'Cufflinks', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(45, 45, 1, 'Coat/Jacket', 'Coat/Jacket', 'Coat/Jacket', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(46, 46, 1, 'DELETED', 'DELETED', 'DELETED', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(47, 47, 1, 'Repairs', 'Repairs', 'Repairs', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(48, 48, 1, 'Repairs', 'Repairs', 'Repairs', '0000-00-00 00:00:00', '2015-03-05 03:18:52'),
(49, 49, 1, 'Samples', 'Samples', 'Samples', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(50, 50, 1, 'Samples', 'Samples', 'Samples', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(51, 51, 1, 'vouchers', 'vouchers', 'vouchers', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(52, 52, 1, 'vouchers', 'vouchers', 'vouchers', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(53, 53, 1, 'Ladies', 'Ladies', 'Ladies', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(54, 54, 1, 'shoes', 'shoes', 'shoes', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(55, 55, 1, 'boots', 'boots', 'boots', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(56, 56, 1, 'Shoe', 'Shoe', 'Shoe', '0000-00-00 00:00:00', '2015-01-28 11:15:04'),
(57, 57, 1, 'Knitwear', 'Knitwear', 'Knitwear', '0000-00-00 00:00:00', '2015-03-25 10:20:46'),
(58, 58, 1, 'Chinos', 'Chinos', 'Chinos', '0000-00-00 00:00:00', '2015-03-22 04:05:45'),
(59, 59, 1, 'Jeans', 'Jeans', 'Jeans', '0000-00-00 00:00:00', '2015-03-23 05:20:31'),
(60, 60, 1, 'Shorts', 'Shorts', 'Shorts', '0000-00-00 00:00:00', '2015-03-22 04:05:56'),
(61, 61, 1, 'Formal Jackets And Coats', 'Formal Jackets and Coats', 'Formal Jackets and Coats', '2015-03-10 12:05:20', '2015-03-10 05:05:34'),
(62, 62, 1, 'Knitwear', 'Knitwear', 'Knitwear', '2015-03-25 17:22:49', '2015-03-25 10:22:49'),
(63, 63, 1, 'Jeans', 'Jeans', 'Jeans', '2015-03-30 10:35:32', '2015-03-30 03:35:32'),
(64, 64, 1, 'Swim Shorts', 'swim shorts', 'swim shorts', '2015-04-20 10:48:10', '2015-04-20 03:48:10'),
(65, 65, 1, 'QA TEST', 'QA TEST', 'QA TEST', '2015-04-23 12:21:26', '2015-04-23 05:21:50');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `PRODUCTS_Merchant_Categories_Definitions`
--
ALTER TABLE `PRODUCTS_Merchant_Categories_Definitions`
  ADD CONSTRAINT `PRODUCTS_Merchant_Categories_Definitions_ibfk_1` FOREIGN KEY (`productMerchantCategoryId`) REFERENCES `PRODUCTS_Merchant_Categories` (`productMerchantCategoryId`) ON DELETE NO ACTION ON UPDATE NO ACTION;
