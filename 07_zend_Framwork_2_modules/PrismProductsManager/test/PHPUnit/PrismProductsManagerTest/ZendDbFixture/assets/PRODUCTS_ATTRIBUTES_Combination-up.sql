DROP TABLE IF EXISTS `PRODUCTS_ATTRIBUTES_Combination`;
CREATE TABLE IF NOT EXISTS `PRODUCTS_ATTRIBUTES_Combination` (
  `productAttributeCombinationId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attributeGroupId` int(11) unsigned NOT NULL,
  `productOrVariantId` int(10) unsigned NOT NULL,
  `attributeValue` varchar(100) NOT NULL,
  `isVariant` tinyint(1) NOT NULL DEFAULT '1',
  `groupType` varchar(255) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`productAttributeCombinationId`),
  KEY `productId` (`productOrVariantId`) USING BTREE,
  KEY `attributeId` (`attributeGroupId`),
  KEY `status` (`status`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=270350 ;

--
-- Dumping data for table `PRODUCTS_ATTRIBUTES_Combination`
--

INSERT INTO `PRODUCTS_ATTRIBUTES_Combination` (`productAttributeCombinationId`, `attributeGroupId`, `productOrVariantId`, `attributeValue`, `isVariant`, `groupType`, `status`, `created`, `modified`) VALUES
(1, 5, 1, '1', 0, 'CHECKBOX', 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(2, 9, 1, '0', 0, 'CHECKBOX', 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(3, 10, 1, '0', 0, 'CHECKBOX', 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(4, 11, 1, '1', 0, 'TEXT', 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(5, 13, 1, '13', 0, 'TEXT', 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(6, 14, 1, '34', 0, 'TEXT', 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(7, 15, 1, '0', 0, 'TEXT', 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(8, 17, 1, '0', 0, 'CHECKBOX', 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(9, 145, 1, 'FREE DELIVERY & RETURNS IN UK', 0, 'TEXT', 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(10, 147, 1, 'WGB', 0, 'TEXT', 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00'),
(11, 148, 1, '20', 0, 'TEXT', 'ACTIVE', '2015-01-28 19:00:48', '0000-00-00 00:00:00');
