DROP TABLE IF EXISTS `PRODUCTS_R_TAXES`;
CREATE TABLE IF NOT EXISTS `PRODUCTS_R_TAXES` (
  `productTaxId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productOrVariantId` int(10) unsigned NOT NULL,
  `taxId` int(10) NOT NULL,
  `isVariant` smallint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`productTaxId`),
  KEY `productId` (`productOrVariantId`),
  KEY `taxId` (`taxId`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2732 ;

--
-- Dumping data for table `PRODUCTS_R_TAXES`
--

INSERT INTO `PRODUCTS_R_TAXES` (`productTaxId`, `productOrVariantId`, `taxId`, `isVariant`) VALUES
(8, 180, 0, 0),
(9, 179, 0, 0),
(10, 178, 0, 0),
(12, 12794, 0, 1),
(13, 12795, 0, 1),
(14, 12796, 0, 1),
(15, 12797, 0, 1),
(16, 12798, 0, 1),
(17, 12799, 0, 1),
(18, 12800, 0, 1),
(19, 12801, 0, 1),
(20, 12802, 0, 1),
(21, 12803, 0, 1),
(22, 12804, 0, 1),
(23, 12805, 0, 1),
(24, 12806, 0, 1),
(25, 12807, 0, 1),
(26, 12808, 0, 1),
(27, 993, 0, 0),
(28, 540, 0, 0),
(32, 994, 0, 0),
(33, 876, 0, 0),
(39, 463, 1, 0),
(42, 918, 0, 0),
(47, 938, 0, 0),
(51, 638, 0, 0),
(52, 637, 0, 0),
(53, 636, 0, 0),
(56, 685, 0, 0),
(57, 845, 0, 0),
(58, 309, 0, 0),
(60, 844, 0, 0),
(71, 12809, 1, 1),
(72, 12810, 1, 1),
(73, 12811, 1, 1),
(74, 12812, 1, 1),
(75, 12813, 1, 1),
(76, 12814, 1, 1);
