DROP TABLE IF EXISTS `TAXES_Definitions`;
CREATE TABLE IF NOT EXISTS `TAXES_Definitions` (
  `taxDefinitionId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `languageId` int(11) unsigned NOT NULL,
  `taxId` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`taxDefinitionId`),
  KEY `languageId` (`languageId`),
  KEY `name` (`name`),
  KEY `taxGroupId` (`taxId`) USING BTREE,
  KEY `languageId_taxGroupId_status` (`languageId`,`taxId`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `TAXES_Definitions`
--

INSERT INTO `TAXES_Definitions` (`taxDefinitionId`, `languageId`, `taxId`, `name`, `description`, `created`, `modified`) VALUES
(1, 1, 1, 'Base UK Tax', 'Base UK Tax', '0000-00-00 00:00:00', '2014-10-03 04:00:37'),
(2, 2, 1, 'Base UK Tax', 'Base UK Tax', '0000-00-00 00:00:00', '2014-10-03 04:00:37'),
(3, 1, 2, 'Base US Tax', 'Base US Tax', '0000-00-00 00:00:00', '2014-10-03 04:00:57'),
(4, 2, 2, 'Base US Tax', 'Base US Tax', '0000-00-00 00:00:00', '2014-10-03 04:00:57'),
(5, 1, 3, 'DE', 'German VAT', '0000-00-00 00:00:00', '2014-10-07 03:57:33'),
(6, 2, 3, 'DE', 'German VAT', '0000-00-00 00:00:00', '2014-10-07 03:57:33'),
(7, 1, 4, 'DE Tax', 'DE Tax Rate', '0000-00-00 00:00:00', '2014-10-10 04:31:27'),
(8, 2, 4, 'DE Tax', 'DE Tax Rate', '0000-00-00 00:00:00', '2014-10-10 04:31:27');
