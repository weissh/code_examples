DROP TABLE IF EXISTS `ATTRIBUTES_Groups_Validations`;
CREATE TABLE IF NOT EXISTS `ATTRIBUTES_Groups_Validations` (
  `attributeGroupValidationId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attributeGroupId` int(10) unsigned NOT NULL DEFAULT '0',
  `isRequired` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Flag used to Determine if the attribute group values are required',
  `validationType` enum('INT','STRING','FLOAT') DEFAULT NULL COMMENT 'Validation type flag for attributes groups',
  PRIMARY KEY (`attributeGroupValidationId`),
  UNIQUE KEY `Index 1` (`attributeGroupId`),
  UNIQUE KEY `Index 3` (`attributeGroupValidationId`),
  KEY `Index 4` (`validationType`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table used to set up validations for attributes Groups' AUTO_INCREMENT=16 ;

--
-- Dumping data for table `ATTRIBUTES_Groups_Validations`
--

INSERT INTO `ATTRIBUTES_Groups_Validations` (`attributeGroupValidationId`, `attributeGroupId`, `isRequired`, `validationType`) VALUES
(6, 165, 1, 'STRING'),
(7, 166, 1, 'STRING'),
(8, 167, 1, 'STRING'),
(9, 168, 0, 'STRING'),
(10, 169, 1, 'STRING'),
(11, 170, 0, 'STRING'),
(12, 171, 0, 'STRING'),
(13, 172, 0, 'STRING'),
(14, 173, 0, 'STRING'),
(15, 181, 0, 'STRING');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ATTRIBUTES_Groups_Validations`
--
ALTER TABLE `ATTRIBUTES_Groups_Validations`
  ADD CONSTRAINT `FK__ATTRIBUTES_Groups` FOREIGN KEY (`attributeGroupId`) REFERENCES `ATTRIBUTES_Groups` (`attributeGroupId`) ON DELETE NO ACTION ON UPDATE NO ACTION;
