DROP TABLE IF EXISTS `CATEGORIES_Definitions`;
CREATE TABLE IF NOT EXISTS `CATEGORIES_Definitions` (
  `categoryDefinitionsId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoryId` int(10) unsigned NOT NULL,
  `languageId` int(10) unsigned NOT NULL,
  `versionId` int(10) unsigned NOT NULL,
  `versionDescription` varchar(255) NOT NULL,
  `categoryName` varchar(75) NOT NULL,
  `urlName` varchar(75) NOT NULL,
  `metaTitle` varchar(75) NOT NULL,
  `metaDescription` varchar(160) NOT NULL,
  `metaKeyword` varchar(160) NOT NULL,
  `shortDescription` varchar(30) NOT NULL,
  `longDescription` text NOT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') DEFAULT 'INACTIVE',
  `makeLive` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `locked` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'flag used to lock a category',
  PRIMARY KEY (`categoryDefinitionsId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=286 ;

--
-- Dumping data for table `CATEGORIES_Definitions`
--

INSERT INTO `CATEGORIES_Definitions` (`categoryDefinitionsId`, `categoryId`, `languageId`, `versionId`, `versionDescription`, `categoryName`, `urlName`, `metaTitle`, `metaDescription`, `metaKeyword`, `shortDescription`, `longDescription`, `status`, `makeLive`, `created`, `modified`, `locked`) VALUES
(1, 1, 1, 1, 'Home', 'Home', 'home', 'Home', 'Home', '', 'Home', 'Home', 'ACTIVE', NULL, NULL, '2015-02-20 09:33:25', 0),
(2, 2, 1, 1, 'Oliver Sweeney', 'Oliver Sweeney', 'oliver-sweeney', 'Oliver Sweeney', 'Oliver Sweeney', '', 'Oliver Sweeney', 'Oliver Sweeney', 'ACTIVE', NULL, NULL, '2015-02-20 09:49:55', 0),
(3, 3, 1, 1, 'New Arrivals', 'New Arrivals', 'new-arrivals', 'New Arrivals', 'New Arrivals', '', 'New Arrivals', 'New Arrivals', 'DELETED', NULL, NULL, '2015-02-20 09:49:55', 0),
(4, 4, 1, 1, 'Components', 'Components', 'components-1', 'Components', 'Components', '', 'Components', 'Components', 'DELETED', NULL, NULL, '2015-02-20 09:49:55', 0),
(5, 5, 1, 1, 'Components', 'Components', 'components-2', 'Components', 'Components', '', 'Components', 'Components', 'ACTIVE', NULL, NULL, '2015-02-20 09:49:55', 0),
(6, 6, 1, 1, 'Women', 'Women', 'women', 'Women', 'Women', '', 'Women', 'Women', 'DELETED', NULL, NULL, '2015-02-20 09:49:55', 0),
(7, 7, 1, 1, 'Boot', 'Boot', 'women-boot', 'Boot', 'Boot', '', 'Boot', 'Boot', 'DELETED', NULL, NULL, '2015-02-20 09:49:55', 0),
(8, 8, 1, 1, 'Men', 'Men', 'men', 'Men', 'Men', '', 'Men', 'Men', 'DELETED', NULL, NULL, '2015-02-20 09:49:55', 0);
