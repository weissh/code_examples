<?php

namespace PrismProductsManagerTest\View\Helper;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Model\ProductsMapper;
use PrismProductsManager\View\Helper\SkuIsEditableViewHelper;

/**
 * @author haniw
 */
class SkuIsEditableViewHelperTest extends PHPUnit_Framework_TestCase
{
    /** @var  array */
    protected $config;

    /** @var  \Prophecy\Prophecy\ObjectProphecy */
    protected $productsMapper;

    /** @var SkuIsEditableViewHelper */
    protected $helper;

    public function provideEditableSkuAndIsSavedInSpectrumAndItsReturn()
    {
        return [
            [true, true, false],
            [true, false, true],
            [false, false, false],
            [false, true, false]
        ];
    }

    /**
     * @dataProvider provideEditableSkuAndIsSavedInSpectrumAndItsReturn
     *
     * @param $editableSku
     * @param $isSavedInSpectrum
     * @param $return
     */
    public function testInvoke($editableSku, $isSavedInSpectrum, $return)
    {
        $this->config = [
            'clientConfig' => [
                'features' => [
                    'editableSku' => $editableSku,
                ]
            ]
        ];
        $this->productsMapper = $this->prophesize(ProductsMapper::class);

        if ($editableSku) {
            $this->productsMapper->variantIsSavedInSpectrum(1)
                ->willReturn($isSavedInSpectrum)
                ->shouldBeCalled();
        }

        $this->helper = new SkuIsEditableViewHelper($this->config, $this->productsMapper->reveal());

        $this->assertSame($return, $this->helper->__invoke(1));
    }
}
