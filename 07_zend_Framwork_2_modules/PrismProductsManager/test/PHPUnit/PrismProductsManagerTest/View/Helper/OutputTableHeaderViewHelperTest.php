<?php

namespace PrismProductsManagerTest\View\Helper;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\View\Helper\OutputTableHeaderViewHelper;

/**
 * @author haniw
 */
class OutputTableHeaderViewHelperTest extends PHPUnit_Framework_TestCase
{
    /** @var  array */
    protected $config;

    /** @var ManualSyncViewHelper */
    protected $helper;

    public function provideConfigAndReturn()
    {
        return [
            [
                [
                    'clientConfig' => [
                        'table-header' => <<<table
<th>Product Name</th>
table
                    ]
                ],
                <<<table
<th>Product Name</th>
table
            ],
            [
                [
                    'clientConfig' => [
                    ]
                ],
                <<<table
<th>Product Name</th>
                           <th>Season</th>
                           <th>Category</th>
                           <th>Sub-Category</th>
                           <th>Web-Department</th>
                           <th>Web-SubDepartment</th>
                           <th>Style/Option Code</th>
                           <th>Nr Of Options</th>
                           <th>WorkFlow</th>
                           <th>Modified Date</th>
                           <th>Created Date</th>
                           <th>Actions</th>
                           <th></th>
                           <th style="display:none">Completed-Workflows</th>
                           <th style="display:none">Not-Completed-Workflows</th>
table
            ],
        ];
    }

    /**
     * @dataProvider provideConfigAndReturn
     *
     * @param array $config
     * @param bool $return
     */
    public function testInvoke($config, $return)
    {
        $this->helper = new OutputTableHeaderViewHelper($config);
        $this->assertSame($return, $this->helper->__invoke());
    }
}
