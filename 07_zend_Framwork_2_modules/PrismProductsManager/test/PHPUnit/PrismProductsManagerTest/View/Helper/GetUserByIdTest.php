<?php

namespace PrismProductsManagerTest\View\Helper;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\View\Helper\GetUserById;

/**
 * @author haniw
 */
class GetUserByIdTest extends PHPUnit_Framework_TestCase
{
    /** @var  \PrismUsersManager\Model\UsersTable */
    protected $users;

    /** @var GetUserById */
    protected $helper;
    
    protected function setUp()
    {
        $this->users = $this->prophesize('PrismUsersManager\Model\UsersTable');
        $this->helper = new GetUserById(
            $this->users->reveal()
        );
    }
    
    public function testInvoke()
    {
        $resultset = new \Zend\Db\ResultSet\HydratingResultSet();            
        $this->users->getUserBy('userId', 1)
                    ->willReturn(
                        $resultset
                    )
                    ->shouldBeCalled();
                     
        $this->helper->__invoke(1);
    }
}
