<?php

namespace PrismProductsManagerTest\View\Helper;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\View\Helper\ManualSyncViewHelper;

/**
 * @author haniw
 */
class ManualSyncViewHelperTest extends PHPUnit_Framework_TestCase
{
    /** @var  array */
    protected $config;

    /** @var ManualSyncViewHelper */
    protected $helper;

    public function provideConfigAndReturn()
    {
        return [
            [
                [
                    'clientConfig' => [
                        'settings' => [
                             'manualSync' => true,
                         ]
                    ]
                ],
                true
            ],
            [
                [
                    'clientConfig' => [
                        'settings' => [
                            'manualSync' => false,
                        ]
                    ]
                ],
                false
            ],
            [
                [
                ],
                false
            ],
        ];
    }

    /**
     * @dataProvider provideConfigAndReturn
     *
     * @param array $config
     * @param bool $return
     */
    public function testInvoke($config, $return)
    {
        $this->helper = new ManualSyncViewHelper($config);
        $this->assertSame($return, $this->helper->__invoke());
    }
}
