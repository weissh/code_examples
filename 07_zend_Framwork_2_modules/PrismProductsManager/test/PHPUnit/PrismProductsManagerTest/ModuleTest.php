<?php

namespace PrismProductsManagerTest;

use PrismProductsManager\Module;
use PHPUnit_Framework_TestCase;

/**
 * @author haniw
 */
class ModuleTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Module
     */
    protected $module;

    public function setUp()
    {
        $this->module = new Module();
    }

    public function testGetConfig()
    {
        $config = $this->module->getConfig();
        $expected = include __DIR__.'/../../../config/module.config.php';

        $this->assertEquals($expected, $config);
    }

    public function testGetAutoloaderConfig()
    {
        $config = $this->module->getAutoloaderConfig();

        $this->assertTrue(is_array($config));
        $this->assertArrayHasKey('Zend\Loader\StandardAutoloader', $config);
    }

    public function testGetConsoleUsage()
    {
        $consoleAdapter = $this->prophesize('Zend\Console\Adapter\AbstractAdapter');
        $consoleUsage = $this->module->getConsoleUsage($consoleAdapter->reveal());

        $this->assertEquals(
            [
            ],
            $consoleUsage
        );
    }
}
