<?php

namespace PrismProductsManagerTest\Validator;

use PHPUnit_Framework_TestCase;
use PrismProductsManager\Validator\Step;

/**
 * @author haniw
 */
class StepTest extends PHPUnit_Framework_TestCase
{
    /** @var Step */
    protected $validator;

    public function provideValueAndReturn()
    {
        return [
            [false, false],
            [1, true],
            [2, true],
            [0, true],
            [20.06, false]
        ];
    }

    /**
     * @dataProvider provideValueAndReturn
     *
     * @param $value
     * @param $return
     */
    public function testIsValid($value, $return)
    {
        $this->validator = new Step();
        $this->assertSame($return, $this->validator->isValid($value));
    }
}
