<?php

namespace PrismElasticSearch\Controller;

use PrismElasticSearch\Enum\ElasticSearchTypes;
use PrismElasticSearch\Model\ElasticSearchMappingDetailsMapper;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Http\Client as HttpClient;
use Zend\Http\Request;

/**
 * Class SearchManagerController
 * @package PrismElasticSearch\Controller
 */
class SearchManagerController extends AbstractActionController
{
    /**
     * @var ElasticSearchMappingDetailsMapper
     */
    protected $_elasticSearchMappingDetailsMapper;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @param ElasticSearchMappingDetailsMapper $elasticSearchMappingDetailsMapper
     */
    public function __construct(ElasticSearchMappingDetailsMapper $elasticSearchMappingDetailsMapper, $config, HttpClient $client)
    {
        $this->_elasticSearchMappingDetailsMapper = $elasticSearchMappingDetailsMapper;
        $this->_config = $config;
        $this->client = $client;
    }

    /**
     * @return ViewModel
     */
    public function searchPrioritiesAction()
    {
        $productsPriorities = $this->_elasticSearchMappingDetailsMapper->getMappingPrioritiesArray(ElasticSearchTypes::PRODUCTS);
        $pagesPriorities = $this->_elasticSearchMappingDetailsMapper->getMappingPrioritiesArray(ElasticSearchTypes::WEBSITE_CONTENT);

        return new ViewModel(array(
            'productsPriorities' => $productsPriorities,
            'pagesPriorities' => $pagesPriorities,
        ));
    }

    /**
     * @return JsonModel
     */
    public function updateSearchPrioritiesAction()
    {
        $responseArray = array(
            'success' => false
        );
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $inputName = $this->params()->fromPost('inputName', false);
            $inputValue = $this->params()->fromPost('inputValue', false);

            if ($inputName && $inputValue) {
                $inputValue = (int) $inputValue;
                if ($inputValue < 1) {
                    return new JsonModel($responseArray);
                }
                $explodedInputName = explode('_', $inputName);
                $elasticSearchType = (int) $explodedInputName[0];
                $fieldId = (int) $explodedInputName[1];
                $responseArray['success'] = $this->_elasticSearchMappingDetailsMapper->updateFieldPriority($elasticSearchType, $fieldId, $inputValue);
                $this->_clearSearchPrioritiesCache($elasticSearchType);
                return new JsonModel($responseArray);
            } else {
                return new JsonModel($responseArray);
            }
        } else {
            return new JsonModel($responseArray);
        }
    }

    protected function _clearSearchPrioritiesCache($elasticSearchType)
    {
        $websites =  $this->_config['siteConfig']['websites']['clear-cache'];

        foreach ($websites as $website) {
            $url = sprintf("%sadmin-tasks/clear-search-cache/" . $elasticSearchType, $website);
            $this->client->setEncType(HttpClient::ENC_URLENCODED);
            $this->client->setUri($url);
            $this->client->setMethod(Request::METHOD_GET);
            $this->client->setOptions(array('curloptions' => array(CURLOPT_SSL_VERIFYPEER => false)));

            $response = $this->client->send();
            if ($response->isSuccess()) {
                $result = $response->getBody();
            } else {
                //@TODO add the action to cron so the cache will be updated later
            }
        }

    }
}
