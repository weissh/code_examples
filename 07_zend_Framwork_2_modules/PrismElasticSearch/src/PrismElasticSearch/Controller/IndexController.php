<?php

namespace PrismElasticSearch\Controller;

use PrismElasticSearch\Enum\ElasticSearchTypes;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class IndexController
 * @package PrismElasticSearch\Controller
 */
class IndexController extends AbstractActionController
{
    /**
     * @throws \Exception
     */
    public function indexAction()
    {
        //throw new \Exception("Access denied!");
        /** @var \PrismElasticSearch\Model\ElasticSearchProductsMapper $productsMapper */
        //$productService = $this->getServiceLocator()->get('PrismSpectrumApiClient\Service\Products');
        //$productService->addProductInSpectrum(987, 0);
        //$productsMapper = $this->getServiceLocator()->get('ElasticSearchProductsMapper');
        //$contentMapper = $this->getServiceLocator()->get('ElasticSearchWebsiteContentMapper');
        //$productsMapper->getIndexLayout('2_0');
        /** @var $elasticIndexingService \PrismElasticSearch\Service\Manager\ElasticSearchIndexingManager */

        // Create new products index
        //$elasticIndexingService = $this->getServiceLocator()->get('ElasticSearchIndexingManager');
        //$elasticIndexingService->setupIndexCategory(ElasticSearchTypes::PRODUCTS);
        ///** @var $elasticIndexingService \PrismElasticSearch\Service\ElasticSearchProductsIndexer */
        //$indexArray = array(
        //    '4' => array(
        //        1 => 'products_4_1_1434976244'
        //    ),
        //    '5' => array(
        //        1 => 'products_5_1_1434976244'
        //    )
        //);
        //print_r($elasticIndexingService->createNewIndexes($indexArray));die;

        // Create new website index
        //$elasticIndexingService = $this->getServiceLocator()->get('ElasticSearchIndexingManager');
        //$elasticIndexingService->setupIndexCategory(ElasticSearchTypes::WEBSITE_CONTENT);
        ///** @var $elasticIndexingService \PrismElasticSearch\Service\ElasticSearchProductsIndexer */
        //$indexArray = array(
        //    '2' => array(
        //        1 => 'website_content_2_1_1412863883'
        //    ),
        //    '4' => array(
        //        1 => 'website_content_4_1_1412863883'
        //    ),
        //    '5' => array(
        //        1 => 'website_content_5_1_1412863883'
        //    ),
        //);
        //print_r($elasticIndexingService->createNewIndexes($indexArray));die;

        //echo '<pre>';
        //print_r($productsMapper->getIndexLayout('2_0'));die;
        //echo '<pre>';print_r($contentBatch);die;
        //$elasticIndexingService->upsert(array('987_0'));
        //die;

        //echo '<pre>';print_r($elasticIndexingService->delete('12_22', array(1)));
        /** @var \PrismElasticSearch\Model\ElasticSearchLastIndexMapper $elasticSearchLastIndexMapper */
        //$elasticSearchLastIndexMapper = $this->getServiceLocator()->get('ElasticSearchLastIndexMapper');

        /*
         * Create indexes
        $timestamp = date('U');
        $elasticSearchLastIndexMapper->setLastTimestamp($timestamp);

        $elasticIndexingService->createIndexes($timestamp);
        */

        /** @var $elasticSearchingService \PrismElasticSearch\Service\Manager\ElasticSearchSearchingManager */
        //$elasticSearchingService = $this->getServiceLocator()->get('ElasticSearchSearchingManager');
        //$elasticSearchingService->setupIndexCategory(ElasticSearchTypes::PRODUCTS);
        /** @var $elasticSearchingService \PrismElasticSearch\Service\ElasticSearchProductsSearching */

        /*
         * Search
         */
        //$elasticSearchingService->setSearchLocation(1, 1);
        //$elasticSearchingService->applyUrlFilter(array('nandy-black_navy-reversible-formal-belt'));
        //$elasticSearchingService->applyCategoryFilter(130);
        //$elasticSearchingService->applyColourFilter(array('red'));
        //$elasticSearchingService->applyStyleFilter('FELINO');
        /*$elasticSearchingService->applySizeFilter(array(
            'gt' => 6,
            'lte' => 13
        ));*/
        /*$elasticSearchingService->applyPriceFilter(array(
            'gte' => 100,
            'lte' => 250
        ));*/
        //$elasticSearchingService->applyCustomAttributeFilter('material', 'leather');
        //$elasticSearchingService->applyCustomAttributeRangeFilter('material', array('exact'=>'leather'));
        //$elasticSearchingService->buildFinalFilter();
        //echo '<pre>'; print_r($elasticSearchingService->doSearch(""));

        // echo'<pre>'; print_r($elasticIndexingService->updateProduct(3));
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function reindexAction()
    {
        //throw new \Exception("Access denied!");
        ini_set('max_execution_time', 7200);
        // Get user email from console and check if the user used --verbose or -v flag
        $entityType   = (int) $this->params('entityType');
        $batchSize     = $this->params('batchSize', 100);

        switch($entityType) {
            case ElasticSearchTypes::PRODUCTS:
            /*case ElasticSearchTypes::BLOG_POSTS:*/
            case ElasticSearchTypes::WEBSITE_CONTENT:
                break;
            default:
                throw new \Exception("Invalid entity type specified!");
                break;
        }

        /** @var $elasticIndexingService \PrismElasticSearch\Service\Manager\ElasticSearchIndexingManager */
        $elasticIndexingService = $this->getServiceLocator()->get('ElasticSearchIndexingManager');
        $elasticIndexingService->setupIndexCategory($entityType);
        /** @var $elasticIndexingService \PrismElasticSearch\Service\Base\ElasticSearchBaseIndexer */

        // CHECK IF THE LOCK FILE ALLOWS FOR RE-INDEXING
        if (!$elasticIndexingService->getIndexLockedStatus()) {
            /**
             * Process flow:
             *   1) stop all the indexing operations
             *   2) resolve current indexes names
             *   3) resolve new indexes names
             *   4) create new indexes
             *   5) apply index settings+mappings
             *   6) populate new index
             *   7) add new index to alias
             *   8) remove old index from alias
             *   9) drop old index
             *   10) override the lock file and allow indexing the new products created meantime
             *   11) start all the indexing operations
             */

            /** ################################################################################################################## */
            /** ------------------------------------------------------------------------------------------------------------------ */
                /**
                 * 1) stop all the indexing operations
                 */
                if (!$elasticIndexingService->setIndexLockedStatus(true)) {
                    return false;
                }
            /** ------------------------------------------------------------------------------------------------------------------ */
            /** ################################################################################################################## */
            /** ------------------------------------------------------------------------------------------------------------------ */
                /**
                 * 2) resolve current indexes names
                 * 3) resolve new indexes names
                 *   -- the arrays will be build in the following format:
                 *   --   $currentIndexesNamesArray = array(
                 *              WEBSITE_ID1 => array(
                 *                  LANGUAGE_ID1 => INDEX_NAME1,
                 *                  LANGUAGE_ID2 => INDEX_NAME2
                 *              ),
                 *              WEBSITE_ID2 => array(
                 *                  LANGUAGE_ID1 => INDEX_NAME1,
                 *                  LANGUAGE_ID2 => INDEX_NAME2
                 *              ),
                 *        );
                 *   --   $newIndexesNamesArray = array(
                 *              WEBSITE_ID1 => array(
                 *                  LANGUAGE_ID1 => INDEX_NAME1,
                 *                  LANGUAGE_ID2 => INDEX_NAME2
                 *              ),
                 *              WEBSITE_ID2 => array(
                 *                  LANGUAGE_ID1 => INDEX_NAME1,
                 *                  LANGUAGE_ID2 => INDEX_NAME2
                 *              ),
                 *        );
                 */
                $listOfOldAndNewIndexes = $elasticIndexingService->determineOldAndNewIndexesNames();
                if($listOfOldAndNewIndexes === false) {
                    return false;
                } else {
                    list($currentIndexesNamesArray, $newIndexesNamesArray) = $listOfOldAndNewIndexes;
                }

            /** ------------------------------------------------------------------------------------------------------------------ */
            /** ################################################################################################################## */
            /** ------------------------------------------------------------------------------------------------------------------ */
                /**
                 * 4) create new indexes
                 * 5) apply indexes settings+mappings -- will be applied automatically by the create function
                 */
                if (!$elasticIndexingService->createNewIndexes($newIndexesNamesArray)) {
                    return false;
                }
            /** ------------------------------------------------------------------------------------------------------------------ */
            /** ################################################################################################################## */
            /** ------------------------------------------------------------------------------------------------------------------ */
                /**
                 * 6) populate new indexes
                 */
                if (!$elasticIndexingService->reIndexProducts($newIndexesNamesArray, $batchSize)) {
                    return false;
                }
            /** ------------------------------------------------------------------------------------------------------------------ */
            /** ################################################################################################################## */
            /** ------------------------------------------------------------------------------------------------------------------ */
                /**
                 * 7) add new indexes to aliases
                 * 8) remove old indexes from aliases
                 */
                if (!$elasticIndexingService->switchAliases($currentIndexesNamesArray, $newIndexesNamesArray)) {
                    return false;
                }
            /** ------------------------------------------------------------------------------------------------------------------ */
            /** ################################################################################################################## */
            /** ------------------------------------------------------------------------------------------------------------------ */
                /**
                 * 9) drop old indexes
                 */
                /*if (!$elasticIndexingService->dropIndexes($currentIndexesNamesArray)) {
                    return false;
                }*/
            /** ------------------------------------------------------------------------------------------------------------------ */
            /** ################################################################################################################## */
            /** ------------------------------------------------------------------------------------------------------------------ */
                /**
                 * 10) override the lock file and allow indexing the new products created meantime
                 */
                /** @var \PrismElasticSearch\Model\ElasticSearchIndexQueueMapper $elasticSearchIndexQueueMapper */
                /*$elasticSearchIndexQueueMapper = $this->getServiceLocator()->get('ElasticSearchIndexQueueMapper');
                $productsIdsArray = $elasticSearchIndexQueueMapper->getProductsInQueue($entityType);
                if ($elasticIndexingService->indexProducts($productsIdsArray, $batchSize, true)) {
                    $elasticSearchLogsMapper->createNewLog($entityType, 'Indexed new products from queue', $processId);
                } else {
                    return;
                }*/
            /** ------------------------------------------------------------------------------------------------------------------ */
            /** ################################################################################################################## */
            /** ------------------------------------------------------------------------------------------------------------------ */
                /**
                 * 11) start all the indexing operations
                 */
                if (!$elasticIndexingService->setIndexLockedStatus(false)) {
                    return false;
                }
            /** ------------------------------------------------------------------------------------------------------------------ */
            /** ################################################################################################################## */
        }

        return true;
    }
}
