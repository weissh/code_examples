<?php
namespace PrismElasticSearch\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;

/**
 * Api Elastic Search Priorities Controller
 *
 * @author <cristian.popescu@whistl.co.uk> Cristian Popescu
 */
class ApiElasticSearchTypeController extends AbstractRestfulController implements ControllerApiAwareInterface
{

    /**
     * @throws \Exception
     */
    public function getList()
    {
        $params = $this->params()->fromRoute();
        $elasticSearchType = $params['elasticSearchType'];
        $result = $this->_getElasticSearchMappingDetailsMapper()->getSearchPriorities($elasticSearchType);

        if ($result != FALSE) {
            return new JsonModel($result);
        } else {
            throw new \Exception('I could not get the priorities for elastic search', 400);
        }
    }

    /**
     * @param mixed $id
     * @return array|mixed|void
     * @throws \Exception
     */
    public function get($id)
    {
        $this->_methodNotAllowed();
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|mixed|void
     * @throws \Exception
     */
    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    /**
     * @param mixed $id
     * @return array|mixed|void
     * @throws \Exception
     */
    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }

    /**
     * @return array|object
     */
    private function _getElasticSearchMappingDetailsMapper()
    {
        return $this->getServiceLocator()->get('ElasticSearchMappingDetailsMapper');
    }
}