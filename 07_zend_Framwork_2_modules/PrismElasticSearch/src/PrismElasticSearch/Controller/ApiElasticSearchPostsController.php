<?php
namespace PrismElasticSearch\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use PrismElasticSearch\Enum\ElasticSearchTypes;
use PrismAuthorize\Controller\Api\ControllerApiAwareInterface;

/**
 * Class ApiElasticSearchPostsController
 * @package PrismElasticSearch\Controller
 */
class ApiElasticSearchPostsController extends AbstractRestfulController implements ControllerApiAwareInterface
{
    /**
     * @param mixed $data
     * @return array|mixed|void
     * @throws \Exception
     */
    public function create($data)
    {
        // initialize Elastic-Search Searching Service
        /** @var \PrismElasticSearch\Service\Manager\ElasticSearchSearchingManager $elasticSearchingService */
        $elasticSearchingService = $this->getServiceLocator()->get('ElasticSearchSearchingManager');
        $elasticSearchingService->setupIndexCategory(ElasticSearchTypes::BLOG_POSTS);
        /** @var \PrismElasticSearch\Service\ElasticSearchProductsSearching $elasticSearchingService */
        try {
            return new JsonModel($elasticSearchingService->searchByParams($data));
        } catch (\Exception $e) {
            throw new \Exception($e, 400);
        }
    }

    /**
     * @param mixed $id
     * @return array|mixed|void
     * @throws \Exception
     */
    public function get($id)
    {
        $this->_methodNotAllowed();
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|mixed|void
     * @throws \Exception
     */
    public function update($id, $data)
    {
        $this->_methodNotAllowed();
    }

    /**
     * @param mixed $id
     * @return array|mixed|void
     * @throws \Exception
     */
    public function delete($id)
    {
        $this->_methodNotAllowed();
    }
    
    /**
     * Response with error if the method is not allowed
     */
    protected function _methodNotAllowed()
    {
        $this->response->setStatusCode(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        throw new \Exception('Method Not Allowed', 405);
    }
}