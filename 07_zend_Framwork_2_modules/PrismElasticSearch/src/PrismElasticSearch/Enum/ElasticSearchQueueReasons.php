<?php

namespace PrismElasticSearch\Enum;

/**
 * Class ElasticSearchQueueReasons
 * @package PrismElasticSearch\Enum
 */
class ElasticSearchQueueReasons {
    /**
     *
     */
    const WRONG_MAPPING_INDEX = 'WRONG_MAPPING_INDEX';
    /**
     *
     */
    const WRONG_MAPPING_UPDATE = 'WRONG_MAPPING_UPDATE';
    /**
     *
     */
    const WRONG_MAPPING_REINDEX = 'WRONG_MAPPING_REINDEX';
    /**
     *
     */
    const REINDEX_LOCK = 'REINDEX_LOCK';
}