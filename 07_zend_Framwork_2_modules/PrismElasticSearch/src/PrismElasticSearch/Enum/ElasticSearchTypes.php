<?php

namespace PrismElasticSearch\Enum;

/**
 * Class ElasticSearchTypes
 * @package PrismElasticSearch\Enum
 */
class ElasticSearchTypes {
    /**
     *
     */
    const PRODUCTS = 1;
    /**
     *
     */
    const BLOG_POSTS = 2;
    /**
     *
     */
    const WEBSITE_CONTENT = 3;
}