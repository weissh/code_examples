<?php

namespace PrismElasticSearch\Service\Base;

/**
 * Class ElasticSearchBaseSearcher
 * @package PrismElasticSearch\Service\Base
 */
abstract class ElasticSearchBaseSearcher
{
    /**
     * @var \Elasticsearch\Client
     */
    protected $_elasticSearchClient;
    /**
     * @var
     */
    protected $_indexes;
    /**
     * @var
     */
    protected $_mappingType;
    /**
     * @var
     */
    protected $_searchParams;
    /**
     * @var bool
     */
    protected $_filters = false;
    /**
     * @var array
     */
    protected $_termFilter = array();
    /**
     * @var array
     */
    protected $_rangeFilter = array();

    // filters
    /**
     * @var int
     */
    protected $_filterCount = 0;
    /**
     * @var bool
     */
    protected $_excludeCriteria = false;
    /**
     * @var bool
     */
    protected $_priorities = false;
    /**
     * @var bool
     */
    protected $_aggregationsArray = false;
    /**
     * @var bool
     */
    protected $_customTermFieldFilter = array();

    /**
     * Set Configuration with Prism CMS Config
     */
    public function __construct(\Elasticsearch\Client $elasticSearchClient, $indexesArray, $mappingType, $prioritiesArray = false)
    {
        $this->_elasticSearchClient = $elasticSearchClient;
        $this->_indexes = $indexesArray;
        $this->_mappingType = $mappingType;
        $this->_priorities = $prioritiesArray;
    }

    /**
     * @return bool
     */
    public function hasFilters()
    {
        return ($this->_filterCount > 0) ? true : false;
    }

    /**
     * @param $fieldName
     * @param $value
     */
    public function applyCustomAttributeFilter($fieldName, $value)
    {
        $this->_termFilter[$fieldName] = $value;
    }

    /**
     * @param $fieldName
     * @param $value
     */
    public function applyCustomTermFieldFilter($fieldName, $value)
    {
        $this->_customTermFieldFilter[$fieldName] = $value;
    }

    /**
     * The range array passed must be in this format
     *  array(
     *      'gt' => value or false, -> greater than
     *      'gte' => value or false, -> greater than or equal to
     *      'lt' => value or false, -> less than
     *      'lte' => value or false, -> less than or equal to
     *      'exact' => value or false -> equal to
     *  )
     *
     * @param $fieldName
     * @param $rangeArray
     */
    public function applyCustomAttributeRangeFilter($fieldName, $rangeArray)
    {
        $this->_rangeFilter[$fieldName] = $rangeArray;
    }

    /**
     * @param $rangeArray
     * @throws \Exception
     * @returns array
     */
    public function validateRangeArrayFormatAndExtractValues($rangeArray)
    {
        // valid formats :
        //    gt set
        //    gte set
        //    lt set
        //    lte set
        //    gt and lt set
        //    gt and lte set
        //    gte and lt set
        //    gte and lte set
        //    exact set
        $finalArray = array();
        if (!empty($rangeArray['gt']) && empty($rangeArray['gte']) && empty($rangeArray['lt'])
            && empty($rangeArray['lte']) && empty($rangeArray['exact'])) {
            $finalArray['range']['gt'] = $rangeArray['gt'];
        } else if (empty($rangeArray['gt']) && !empty($rangeArray['gte']) && empty($rangeArray['lt'])
            && empty($rangeArray['lte']) && empty($rangeArray['exact'])) {
            $finalArray['range']['gte'] = $rangeArray['gte'];
        } else if (empty($rangeArray['gt']) && empty($rangeArray['gte']) && !empty($rangeArray['lt'])
            && empty($rangeArray['lte']) && empty($rangeArray['exact'])) {
            $finalArray['range']['lt'] = $rangeArray['lt'];
        } else if (empty($rangeArray['gt']) && empty($rangeArray['gte']) && empty($rangeArray['lt'])
            && !empty($rangeArray['lte']) && empty($rangeArray['exact'])) {
            $finalArray['range']['lte'] = $rangeArray['lte'];
        } else if (empty($rangeArray['gt']) && empty($rangeArray['gte']) && empty($rangeArray['lt'])
            && empty($rangeArray['lte']) && !empty($rangeArray['exact'])) {
            $finalArray['term'] = $rangeArray['exact'];
        } else if (!empty($rangeArray['gt']) && empty($rangeArray['gte']) && !empty($rangeArray['lt'])
            && empty($rangeArray['lte']) && empty($rangeArray['exact'])) {
            if($rangeArray['gt'] < $rangeArray['lt']) {
                $finalArray['range']['gt'] = $rangeArray['gt'];
                $finalArray['range']['lt'] = $rangeArray['lt'];
            }
        } else if (!empty($rangeArray['gt']) && empty($rangeArray['gte']) && empty($rangeArray['lt'])
            && !empty($rangeArray['lte']) && empty($rangeArray['exact'])) {
            if($rangeArray['gt'] < $rangeArray['lte']) {
                $finalArray['range']['gt'] = $rangeArray['gt'];
                $finalArray['range']['lte'] = $rangeArray['lte'];
            }
        } else if (empty($rangeArray['gt']) && !empty($rangeArray['gte']) && !empty($rangeArray['lt'])
            && empty($rangeArray['lte']) && empty($rangeArray['exact'])) {
            if($rangeArray['gte'] < $rangeArray['lt']) {
                $finalArray['range']['gte'] = $rangeArray['gte'];
                $finalArray['range']['lt'] = $rangeArray['lt'];
            }
        } else if (empty($rangeArray['gt']) && !empty($rangeArray['gte']) && empty($rangeArray['lt'])
            && !empty($rangeArray['lte']) && empty($rangeArray['exact'])) {
            if($rangeArray['gte'] < $rangeArray['lte']) {
                $finalArray['range']['gte'] = $rangeArray['gte'];
                $finalArray['range']['lte'] = $rangeArray['lte'];
            }
        }

        if (count($finalArray) > 0) {
            return $finalArray;
        } else {
            throw new \Exception("Invalid configuration specified for the range array!");
        }
    }

    /**
     * This will need to be set according to the website/language the search is made from
     *
     * @param int $websiteId
     * @param int $languageId
     * @throws \Exception
     */
    public function setSearchLocation($websiteId, $languageId)
    {
        // TODO: once the services for websites and languages are set use those to validate the parameters passed
        if (isset($this->_indexes[$websiteId][$languageId])) {
            $this->_searchParams['index'] = $this->_indexes[$websiteId][$languageId];
            $this->_searchParams['type'] = $this->_mappingType;
        } else {
            throw new \Exception("The search location is not valid!");
        }
    }

    /**
     * @param $elasticSearchIdsArray
     * @return array
     */
    public function getByIds($elasticSearchIdsArray)
    {
        $searchArray = array();
        foreach ($elasticSearchIdsArray as $elasticSearchId) {
            $searchArray[] = array('_id' => $elasticSearchId);
        }
        $this->_searchParams['body'] = array(
            'docs' => $searchArray
        );
        return $this->_elasticSearchClient->mget($this->_searchParams);
    }

    /**
     * @param bool $searchText
     * @param int $from
     * @param int $size
     * @param array|bool $customOrder
     * @param array|bool $customAggregations
     * @param array|bool $customSearch
     * @return array
     */
    public function doSearch($searchText = false, $from = 0, $size = 20, $customOrder = false, $customAggregations = false, $customSearch = false)
    {

        $fieldsArray = array();
        if(isset($customSearch['customFields']) && !empty($customSearch['customFields'])){
            $fieldsArray = $customSearch['customFields'];
        } else {
        if (!empty($this->_priorities)) {
            foreach($this->_priorities as $attribute => $boost) {
                $fieldsArray[] = $attribute.'^'.$boost;
            }
        }
        $fieldsArray[] = '_all';
        }
        if (empty($searchText) && !empty($this->_filters)) {
            // only filter
            $this->_searchParams['body']['query']['filtered']['query'] = array(
                'match_all' => new \stdClass()
            );
            if (!empty($this->_excludeCriteria)) {
                foreach ($this->_excludeCriteria as $excludeCriteriaArray) {
                    $this->_filters['and'][] = $excludeCriteriaArray;
                }
            }
            $this->_searchParams['body']['query']['filtered']['filter'] = $this->_filters;
        } else if (!empty($searchText) && empty($this->_filters)) {
            // only search
            $this->_searchParams['body']['query']['filtered']['query'] = array(
                'multi_match' => array(
                    'query' => $searchText,
                    'fields' => $fieldsArray
                )
            );
            if(isset($customSearch['type']) && !empty($customSearch['type'])){
                $this->_searchParams['body']['query']['filtered']['query']['multi_match']['type'] = $customSearch['type'];
            }
            if (!empty($this->_excludeCriteria)) {
                foreach ($this->_excludeCriteria as $excludeCriteriaArray) {
                    $this->_filters['and'][] = $excludeCriteriaArray;
                }
                $this->_searchParams['body']['query']['filtered']['filter'] = $this->_filters;
            }
        } else if (!empty($searchText) && !empty($this->_filters)) {
            // search and filter
            $this->_searchParams['body']['query']['filtered']['query'] = array(
                'multi_match' => array(
                    'query' => $searchText,
                    'fields' => $fieldsArray
                )
            );
            if(isset($customSearch['type']) && !empty($customSearch['type'])){
                $this->_searchParams['body']['query']['filtered']['query']['multi_match']['type'] = $customSearch['type'];
            }
            if (!empty($this->_excludeCriteria)) {
                foreach ($this->_excludeCriteria as $excludeCriteriaArray) {
                    $this->_filters['and'][] = $excludeCriteriaArray;
                }
            }
            $this->_searchParams['body']['query']['filtered']['filter'] = $this->_filters;
        } else {
            $this->_searchParams['body']['query']['match_all'] = new \stdClass();
        }

        if ($customOrder !== false && is_array($customOrder)) {
            reset($customOrder);
            if (is_scalar($customOrder[key($customOrder)])) {
                $customOrder[key($customOrder)] = (array) $customOrder[key($customOrder)];
            }
            $customOrder[key($customOrder)]['missing'] = PHP_INT_MAX -1;
            $this->_searchParams['body']['sort'] = array('categories_priorities.' . key($customOrder) => $customOrder[key($customOrder)]);

        }

        if($size != 0) {
            $this->_searchParams['body']['from'] = $from;
            $this->_searchParams['body']['size'] = $size;
        }
        if (!empty($this->_aggregationsArray)) {
            if (!empty($customAggregations)) {
                foreach ($customAggregations as $aggregationKey => $customAggregationArray) {
                    $this->_aggregationsArray[$aggregationKey] = $customAggregationArray;
                }
            }
            $this->_searchParams['body']['aggs'] = $this->_aggregationsArray;
        }

        if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != '') {
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if ($ip_address == '62.254.216.62') {
                error_log(json_encode($this->_searchParams));
            }
        }


        return $this->_elasticSearchClient->search($this->_searchParams);
    }

    public function doAutoCompleteSearch($searchText = false, $size = 5)
    {
        $this->_searchParams['body']['from'] = 0;
        $this->_searchParams['body']['size'] = $size * 5;

        $this->_searchParams['body']['query'] = array(
            'multi_match' => array(
                'query' => $searchText,
                'fields' => array("*.autocomplete")
            )
        );

        return $this->_elasticSearchClient->search($this->_searchParams);
    }

    /**
     * @return mixed
     */
    abstract public function buildFinalFilter();

    /**
     * @param $websiteId
     * @param $languageId
     * @param $searchString
     * @param $filtersArray
     * @param $from
     * @param $size
     * @param $customOrder
     * @return mixed
     */
    abstract public function buildParamsArray($websiteId, $languageId, $searchString, $filtersArray, $from, $size, $customOrder);

    abstract public function resetFilters();

    /**
     * @param $paramsArray
     * @return mixed
     */
    abstract public function searchByParams($paramsArray);

    /**
     * @param $paramsArray
     * @return mixed
     */
    abstract public function getAutoCompleteResults($paramsArray);
}
