<?php

namespace PrismElasticSearch\Service\Base;

use PrismElasticSearch\Enum\ElasticSearchQueueReasons;

/**
 * Class ElasticSearchBaseIndexer
 * @package PrismElasticSearch\Service\Base
 */
class ElasticSearchBaseIndexer
{
    /**
     * @var \Elasticsearch\Client
     */
    protected $_elasticSearchClient;
    /**
     * @var
     */
    protected $_indexes;
    /**
     * @var
     */
    protected $_mappingType;
    /**
     * @var
     */
    protected $_numberOfShards;
    /**
     * @var
     */
    protected $_numberOfReplicas;
    /**
     * @var
     */
    protected $_lockFile;
    /**
     * @var \PrismElasticSearch\Model\ElasticSearchLogsMapper
     */
    protected $_logsMapper;
    /**
     * @var \PrismElasticSearch\Model\ElasticSearchLastIndexMapper
     */
    protected $_lastIndexMapper;
    /**
     * @var \PrismElasticSearch\Model\ElasticSearchIndexQueueMapper
     */
    protected $_queueMapper;
    /**
     * @var \PrismElasticSearch\Model\ElasticSearchAbstractMapper
     */
    protected $_entityMapper;
    /**
     * @var
     */
    protected $_logsProcessId;
    /**
     * @var
     */
    protected $_indexPrefix;
    /**
     * @var bool
     */
    protected $_newIndexNamesArray = false;
    /**
     * @var bool
     */
    private $_newTimestamp = false;

    /**
     * This is the mapping array which must be defined in each entity indexer that extends this class
     */
    protected $_mappingArray = array();

    /**
     * Set Configuration with Prism CMS Config
     */
    public function __construct(
        \Elasticsearch\Client $elasticSearchPhpClient,
        \PrismElasticSearch\Model\ElasticSearchLogsMapper $elasticSearchLogsMapper,
        \PrismElasticSearch\Model\ElasticSearchIndexQueueMapper $elasticSearchIndexQueueMapper,
        \PrismElasticSearch\Model\ElasticSearchLastIndexMapper $elasticSearchLastIndexMapper,
        \PrismElasticSearch\Model\ElasticSearchAbstractMapper $entityMapper,
        $elasticSearchType,
        $indexesArray,
        $elasticSearchVars,
        $mappingArray
    )
    {
        $this->_elasticSearchClient = $elasticSearchPhpClient;
        $this->_elasticSearchType = $elasticSearchType;
        $this->_logsMapper = $elasticSearchLogsMapper;
        $this->_queueMapper = $elasticSearchIndexQueueMapper;
        $this->_lastIndexMapper = $elasticSearchLastIndexMapper;
        $this->_entityMapper = $entityMapper;
        $this->_mappingArray = $mappingArray;
        /**
         * $this->_indexes must be an array like:
         *      array(
         *          websiteId1 => array(
         *              languageId1 => ##indexName##,
         *              languageId2 => ##indexName##,
         *              ...
         *          ),
         *          websiteId2 => array(
         *              languageId1 => ##indexName##,
         *              languageId2 => ##indexName##,
         *              ...
         *          ),
         *          ...
         *      )
         */
        $this->_indexes = $indexesArray;
        $this->_indexPrefix = $elasticSearchVars['indexPrefix'];
        $this->_mappingType = $elasticSearchVars['mappingType'];
        $this->_numberOfShards = $elasticSearchVars['numberOfShards'];
        $this->_numberOfReplicas = $elasticSearchVars['numberOfReplicas'];
        $this->_lockFile = $elasticSearchVars['lockFile'];
    }

    // -----------------------------------------
    // Index status methods
    // -----------------------------------------

    /**
     * @return mixed
     */
    public function getAliasesNames()
    {
        return $this->_indexes;
    }

    /**
     * Return the index details
     * If no index is specified, return the details for all the indexes
     *
     * @param string|bool $indexName
     * @return array - index|indexes details
     */
    public function getIndexDetails($indexName = false)
    {
        $params = array();
        if ($indexName !== false) {
            $params['index'][] = $indexName;
        }

        if (!empty($params['index'])) {
            return $this->_elasticSearchClient->indices()->getSettings($params);
        }

        return false;
    }

    /**
     * Return the mapping for an index type
     * @return array - mapping
     */
    public function getMappingDetails()
    {
        $params = array();
        foreach ($this->_indexes as $indexArray) {
            foreach ($indexArray as $indexName) {
                $params['index'][] = $indexName;
            }
        }

        if (!empty($params['index'])) {
            $params['type'] = $this->_mappingType;
            return $this->_elasticSearchClient->indices()->getMapping($params);
        }

        return false;
    }

    // -----------------------------------------
    // Core methods
    // -----------------------------------------

    /**
     * This is the main method used for indexing new created entities
     * It accepts as a parameter an array of entities IDs and a batchSize
     *
     * @param array $elasticSearchIdsArray
     * @param array|bool $websitesIdsArray
     * @param int $batchSize - build the bulk array on batches in order to reduce the memory usage
     * @param bool $ignoreLockFlag - ignores the index locked status - only used by re-indexing
     * @returns bool - success or failure
     * @throws \Exception
     */
    public function index($elasticSearchIdsArray, $websitesIdsArray = false, $batchSize = 100, $ignoreLockFlag = false)
    {
        if (!$this->getIndexLockedStatus() || $ignoreLockFlag === true) {
            if (is_array($elasticSearchIdsArray)) {
                $indexArray = array();
                // build the index names arrays
                /** @var array $indicesToUse */
                $indicesToUse = $this->_newIndexNamesArray !== false ? $this->_newIndexNamesArray : $this->getAliasesNames();
                foreach ($indicesToUse as $websiteId => $indexPerLanguageArray) {
                    foreach ($indexPerLanguageArray as $languageId => $indexName) {
                        $indexArray[$websiteId][$languageId]['index'] = $indexName;
                        $indexArray[$websiteId][$languageId]['type'] = $this->_mappingType;
                    }
                }
                try {
                    foreach ($elasticSearchIdsArray as $elasticSearchId) {
                        /**
                         * Build indexing document for product
                         *  The indexing document should look like:
                         *      array(
                         *          WEBSITE_ID1 => array(
                         *              LANGUAGE_ID1 => ## PRODUCT ARRAY ## -- if it is not available for this website it will not be set,
                         *              LANGUAGE_ID2 => ## PRODUCT ARRAY ##,
                         *          ),
                         *          WEBSITE_ID2 => array(
                         *              LANGUAGE_ID1 => ## PRODUCT ARRAY ##,
                         *              LANGUAGE_ID2 => ## PRODUCT ARRAY ##,
                         *          ),
                         *          ....
                         *      )
                         */
                        $returnedProductsMapperResponse = $this->_entityMapper->getIndexLayout($elasticSearchId);
                        if ($returnedProductsMapperResponse['success'] === false && $returnedProductsMapperResponse['results'] == 'EVOUCHER' ) {
                            print_r('Product is evoucher');
                            return false;
                        } elseif ($returnedProductsMapperResponse['success'] === false && $returnedProductsMapperResponse['results'] != 'EVOUCHER' ) {
                            // something failed while fetching the mapper for the product
                            // we will queue the product and try debug it and index it later
                            // maybe send an email also
                            $this->_queueMapper->addProductInQueue($elasticSearchId, $this->_elasticSearchType, ElasticSearchQueueReasons::WRONG_MAPPING_INDEX);
                            // loop to the next elastic-search-id if any
                            continue;
                        } else {
                            $returnedProductsArray = $returnedProductsMapperResponse['results'];
                        }
                        if ($returnedProductsArray === false) {
                            // for some reason we failed to return the mapping for this product
                            $this->_queueMapper->addProductInQueue($elasticSearchId, $this->_elasticSearchType, ElasticSearchQueueReasons::WRONG_MAPPING_INDEX);
                        }
                        foreach ($returnedProductsArray as $websiteId => $entitiesGroupedByLanguage) {
                            foreach ($entitiesGroupedByLanguage as $languageId => $toBeIndexedArray) {
                                if (!empty($returnedProductsArray[$websiteId][$languageId])) {
                                    // we have a mapping for this index so we will need to create a new record
                                    $indexParams['index'] = $indexArray[$websiteId][$languageId]['index'];
                                    $indexParams['type'] = $indexArray[$websiteId][$languageId]['type'];
                                    $indexParams['id'] = $elasticSearchId;
                                    $indexParams['body'] = $returnedProductsArray[$websiteId][$languageId];
                                    $response = $this->_elasticSearchClient->index($indexParams);
                                }
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.$e->getMessage(), $this->getLogsProcessId(), true);
                    return false;
                }
                return true;
            } else {
                $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.'Invalid array of elastic-search IDs passed into method '.__METHOD__.'!',
                    $this->getLogsProcessId(), true);
                return false;
            }
        } else {
            try {
                $this->_queueMapper->addProductsInQueue($elasticSearchIdsArray, $this->_elasticSearchType, ElasticSearchQueueReasons::REINDEX_LOCK);
            } catch (\Exception $e) {
                $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.$e->getMessage(), $this->getLogsProcessId(), true);
                return false;
            }

            return true;
        }
    }

    /**
     * This is the main method used for indexing, updating or deleting based on the mapping configuration
     *
     * We need to iterate trough each website and language and check if the product exists in the specific index or not
     * - if the product doesn't exists in a specific index but has the mapping for it create it
     * - if the product exists in a specific index but doesn't have the mapping for it delete it
     * - if the product exists in a specific index and has the mapping for it, update it
     * - if the product doesn't exists in a specific index and it doesn't have the mapping for it leave it alone
     *
     * @param array $elasticSearchIdsArray
     * @returns bool - success or failure
     * @throws \Exception
     */
    public function upsert($elasticSearchIdsArray)
    {
        if (!$this->getIndexLockedStatus()) {
            if (is_array($elasticSearchIdsArray)) {
                $indexArray = array();
                // build the index names arrays
                /** @var array $indicesToUse */
                $indicesToUse = $this->_newIndexNamesArray !== false ? $this->_newIndexNamesArray : $this->getAliasesNames();
                foreach ($indicesToUse as $websiteId => $indexPerLanguageArray) {
                    foreach ($indexPerLanguageArray as $languageId => $indexName) {
                        $indexArray[$websiteId][$languageId]['index'] = $indexName;
                        $indexArray[$websiteId][$languageId]['type'] = $this->_mappingType;
                    }
                }

                try {
                    foreach ($elasticSearchIdsArray as $elasticSearchId) {
                        /**
                         * Build indexing document for product
                         *  The indexing document should look like:
                         *      array(
                         *          WEBSITE_ID1 => array(
                         *              LANGUAGE_ID1 => ## PRODUCT ARRAY ## -- if it is not available for this website it will not be set,
                         *              LANGUAGE_ID2 => ## PRODUCT ARRAY ##,
                         *          ),
                         *          WEBSITE_ID2 => array(
                         *              LANGUAGE_ID1 => ## PRODUCT ARRAY ##,
                         *              LANGUAGE_ID2 => ## PRODUCT ARRAY ##,
                         *          ),
                         *          ....
                         *      )
                         */
                        $returnedProductsMapperResponse = $this->_entityMapper->getIndexLayout($elasticSearchId);

                        if ($returnedProductsMapperResponse['success'] === false) {
                            // something failed while fetching the mapper for the product
                            // we will queue the product and try debug it and index it later
                            // maybe send an email also
                            $this->_queueMapper->addProductInQueue($elasticSearchId, $this->_elasticSearchType, ElasticSearchQueueReasons::WRONG_MAPPING_INDEX);
                            // loop to the next elastic-search-id if any
                            $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.$returnedProductsMapperResponse['results'], $this->getLogsProcessId(), true);
                            continue;
                        } else {
                            $returnedProductsArray = $returnedProductsMapperResponse['results'];
                        }

                        if ($returnedProductsArray === false) {
                            // for some reason we failed to return the mapping for this product
                            $this->_queueMapper->addProductInQueue($elasticSearchId, $this->_elasticSearchType, ElasticSearchQueueReasons::WRONG_MAPPING_INDEX);
                        }

                        foreach ($indexArray as $websiteId => $entitiesGroupedByLanguage) {
                            foreach ($entitiesGroupedByLanguage as $languageId => $entitiesArrayToBeIndexed) {

                                // check if on the current index we have an entry with the current elasticSearchId
                                $documentExists = $this->checkIfDocumentExists($elasticSearchId, $entitiesArrayToBeIndexed['index'], $entitiesArrayToBeIndexed['type']);
                                if ($documentExists === true) {
                                    // document exists in the current index
                                    if (!empty($returnedProductsArray[$websiteId][$languageId])) {
                                        // we need to update the existing document
                                        $updateParams['index'] = $entitiesArrayToBeIndexed['index'];
                                        $updateParams['type'] = $entitiesArrayToBeIndexed['type'];
                                        $updateParams['id'] = $elasticSearchId;
                                        $updateParams['body']['doc'] = $returnedProductsArray[$websiteId][$languageId];
                                        $updateParams['body']['doc_as_upsert'] = true;
                                        $response = $this->_elasticSearchClient->update($updateParams);
                                    } else {
                                        // we need to delete the existing document
                                        $deleteParams = array();
                                        $deleteParams['index'] = $entitiesArrayToBeIndexed['index'];
                                        $deleteParams['type'] = $entitiesArrayToBeIndexed['type'];
                                        $deleteParams['id'] = $elasticSearchId;
                                        $response = $this->_elasticSearchClient->delete($deleteParams);
                                    }
                                } else {
                                    // document does not exist int eh current index
                                    if (!empty($returnedProductsArray[$websiteId][$languageId])) {
                                        // we have a mapping for this index so we will need to create a new record
                                        $indexParams['index'] = $entitiesArrayToBeIndexed['index'];
                                        $indexParams['type'] = $entitiesArrayToBeIndexed['type'];
                                        $indexParams['id'] = $elasticSearchId;
                                        $indexParams['body'] = $returnedProductsArray[$websiteId][$languageId];
                                        $response = $this->_elasticSearchClient->index($indexParams);
                                    }
                                }
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.$e->getMessage(), $this->getLogsProcessId(), true);
                    return false;
                }
                return true;
            } else {
                $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.'Invalid array of elastic-search IDs passed into method '.__METHOD__.'!',
                    $this->getLogsProcessId(), true);
                return false;
            }
        } else {
            try {
                $this->_queueMapper->addProductsInQueue($elasticSearchIdsArray, $this->_elasticSearchType, ElasticSearchQueueReasons::REINDEX_LOCK);
            } catch (\Exception $e) {
                $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.$e->getMessage(), $this->getLogsProcessId(), true);
                return false;
            }

            return true;
        }
    }
    /**
     * @param array $elasticSearchIds - an array of elasticSearchIds to update
     * @return array|bool
     */
    public function update($elasticSearchIds)
    {
        if (!$this->getIndexLockedStatus()) {
            try {
                foreach ($elasticSearchIds as $elasticSearchId) {
                    $updateParams['type']           = $this->_mappingType;
                    $updateParams['id']             = $elasticSearchId;

                    $returnedProductsMapperResponse = $this->_entityMapper->getIndexLayout($elasticSearchId);
                    if ($returnedProductsMapperResponse['success'] === false) {
                        // something failed while fetching the mapper for the product
                        // we will queue the product and try debug it and index it later
                        // maybe send an email also
                        $this->_queueMapper->addProductInQueue($elasticSearchId, $this->_elasticSearchType, ElasticSearchQueueReasons::WRONG_MAPPING_UPDATE);
                        // loop to the next elastic-search-id if any
                        return false;
                    } else {
                        $returnedProductsArray = $returnedProductsMapperResponse['results'];
                    }
                    foreach ($returnedProductsArray as $websiteId => $entitiesGroupedByLanguage) {
                        foreach ($entitiesGroupedByLanguage as $languageId => $toBeIndexedArray) {
                            $updateParams['index'] = $this->_indexPrefix.$websiteId.'_'.$languageId;
                            $updateParams['body']['doc']    = $toBeIndexedArray;
                            $updateParams['body']['doc_as_upsert'] = true;
                            $this->_elasticSearchClient->update($updateParams);
                        }
                    }
                }
                return true;
            } catch (\Exception $e) {
                $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.$e->getMessage(), $this->getLogsProcessId(), true);
            }
        } else {
            $this->_queueMapper->addProductsInQueue($elasticSearchIds, $this->_elasticSearchType, ElasticSearchQueueReasons::REINDEX_LOCK);
        }
    }

    /**
     * @param $elasticSearchId
     * @param array $websitesArray
     * @return array|bool
     */
    public function delete($elasticSearchId, $websitesArray = array())
    {
        $deleteParams = array();
        $deleteParams['type'] = $this->_mappingType;
        $deleteParams['id'] = $elasticSearchId;

        try {
            if(!empty($websitesArray)) {
                // delete from the specified website index
                if (!is_array($websitesArray)) {
                    $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.'Invalid parameter $websitesIdsArray passed to the method '.__METHOD__.'!',
                        $this->getLogsProcessId(), true);
                    return false;
                } else {
                    foreach ($websitesArray as &$website) {
                        // delete from the specified website index
                        if (is_numeric($website) && (int) $website > 0) {
                            $website = (int) $website;
                        } else {
                            $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.'Invalid website ID: '.$website.'!', $this->getLogsProcessId(), true);
                            return false;
                        }
                    }
                }
            }
            foreach ($this->_indexes as $websiteId => $entitiesGroupedByLanguage) {
                foreach ($entitiesGroupedByLanguage as $languageId => $indexName) {
                    if(!empty($websitesArray) && !in_array($websiteId, $websitesArray)) {
                        continue;
                    }
                    $deleteParams['index'] = $indexName;
                    $this->_elasticSearchClient->delete($deleteParams);
                    $this->_logsMapper->createNewLog(
                        $this->_elasticSearchType,
                        __FILE__.':'.__LINE__.' - '."Deleted entry for id: ".$elasticSearchId.", on index ".$indexName,
                        $this->getLogsProcessId()
                    );
                }
            }
            return true;
        } catch (\Exception $e) {
            $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.$e->getMessage(), $this->getLogsProcessId(), true);
            return false;
        }
    }

    // -----------------------------------------
    // Methods used in the re-indexing process
    // -----------------------------------------

    /**
     * This method should be used for creating the indexes in an empty elastic-search server only.
     * It is used only be developers when creating a new elastic-search server
     *
     * @param $timestamp
     * @return bool
     * @throws \Exception
     */
    public function create($timestamp)
    {
        if(empty($this->_mappingArray)) {
            $this->_logsMapper->createNewLog(
                $this->_elasticSearchType,
                __FILE__.':'.__LINE__.' - '.'Mapping array not specified in '.get_class($this).'!',
                $this->getLogsProcessId(), true);
            return false;
        }
        $aliasesArray = $this->getAliasesNames();
        if (is_array($aliasesArray)) {
            try {
                foreach ($aliasesArray as $websiteId => $indexPerLanguageArray) {
                    foreach($indexPerLanguageArray as $languageId => $indexAlias) {
                        $params = array();
                        $params['index'] = $indexAlias.'_'.$timestamp;
                        $params['body']['settings']['number_of_shards']   = $this->_numberOfShards;
                        $params['body']['settings']['number_of_replicas'] = $this->_numberOfReplicas;
                        $params['body']['settings']['analysis'] = array(
                            'filter' => array(
                                'autocomplete_filter' => array(
                                    'type' => 'edge_ngram',
                                    'min_gram' => 3,
                                    'max_gram' => 25,
                                )
                            ),
                            'analyzer' => array(
                                'autocomplete' => array(
                                    'type' => 'custom',
                                    'tokenizer' => 'standard',
                                    'filter' => array(
                                        'standard',
                                        'lowercase',
                                        'stop',
                                        'kstem',
                                        'autocomplete_filter'
                                    )
                                )
                            )
                        );
                        $params['body']['mappings'][$this->_mappingType] = $this->_mappingArray;
                        $this->_elasticSearchClient->indices()->create($params);
                        $params['body'] = array(
                            'actions' => array(
                                array(
                                    'add' => array(
                                        'index' => $indexAlias.'_'.$timestamp,
                                        'alias' => $indexAlias
                                    )
                                )
                            )
                        );
                        $this->_elasticSearchClient->indices()->updateAliases($params);
                        $this->_lastIndexMapper->setLastTimestamp($timestamp, $this->_elasticSearchType);
                        $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '."Fresh new indexes created!", $this->getLogsProcessId());
                    }
                }
            } catch (\Exception $e) {
                $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.$e->getMessage(), $this->getLogsProcessId(), true);
                return false;
            }
        } else {
            $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.'Invalid array of indexes names passed into method '.__METHOD__.'!',
                $this->getLogsProcessId(), true);
            return false;
        }
        return true;
    }

    /**
     * This is the main method used for re-indexing all entities
     * It accepts as a parameter a batchSize
     *
     * @param array $newIndexesArray - need to make sure that this array passed into the method is like:
     *          array(
     *              websiteId1 => array(
     *                  languageId1 => ##indexName##,
     *                  languageId2 => ##indexName##,
     *                  ...
     *              ),
     *              websiteId2 => array(
     *                  languageId1 => ##indexName##,
     *                  languageId2 => ##indexName##,
     *                  ...
     *              ),
     *              ...
     *          )
     * @param int $batchSize - build the bulk array on batches in order to reduce the memory usage
     * @returns bool - success or failure
     * @throws \Exception
     */
    public function reIndexProducts($newIndexesArray, $batchSize = 100)
    {
        $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.'Starting new re-indexing process', $this->getLogsProcessId());
        if (is_array($newIndexesArray)) {
            // get elastic-search IDs in batches
            try {
                $elasticSearchIdsBatch = $this->_entityMapper->getIdsForBatch(false, $batchSize);
                //var_dump($elasticSearchIdsBatch);die;
                $this->_newIndexNamesArray = $newIndexesArray;
                $this->index($elasticSearchIdsBatch, false, $batchSize, true);
                $this->_newIndexNamesArray = false;
            } catch (\Exception $e) {
                $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.$e->getMessage(), $this->getLogsProcessId(), true);
                return false;
            }
        } else {
            $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.'Invalid array of indexes names passed into method '.__METHOD__.'!',
                $this->getLogsProcessId(), true);
            return false;
        }

        $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.'Indexing of the entities into the new indices complete', $this->getLogsProcessId());
        return true;
    }

    /**
     * Main method used for deleting indexes
     *
     * @param $indexArray
     * @return bool
     */
    public function dropIndexes($indexArray)
    {
        if (is_array($indexArray)) {
            foreach ($indexArray as $indexName) {
                try {
                    $params['index'] = $indexName;
                    $this->_elasticSearchClient->indices()->delete($params);
                } catch (\Exception $e) {
                    $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.$e->getMessage(), $this->getLogsProcessId(), true);
                    return false;
                }
            }
        } else {
            $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.'Invalid array of indexes names passed into
                method '.__METHOD__.'!', $this->getLogsProcessId(), true);
            return false;
        }

        $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.'Successfully deleted the old indices', $this->getLogsProcessId());
        return true;
    }

    /**
     * This method is used by the re-indexing process to create the new indexes, on which the entities will be re-indexed on
     *
     * @param array $indexesArray
     * @return bool
     * @throws \Exception
     */
    public function createNewIndexes($indexesArray)
    {
        if(empty($this->_mappingArray)) {
            $this->_logsMapper->createNewLog(
                $this->_elasticSearchType,
                __FILE__.':'.__LINE__.' - '.'Mapping array not specified in '.get_class($this).'!',
                $this->getLogsProcessId(), true);
            return false;
        }
        if (is_array($indexesArray)) {
            try {
                foreach ($indexesArray as $websiteId => $entitiesGroupedByLanguage) {
                    foreach ($entitiesGroupedByLanguage as $languageId => $indexName) {
                        $params = array();
                        $params['index'] = $indexName;
                        $params['body']['settings']['number_of_shards']   = $this->_numberOfShards;
                        $params['body']['settings']['number_of_replicas'] = $this->_numberOfReplicas;
                        $params['body']['settings']['analysis'] = array(
                            'filter' => array(
                                'autocomplete_filter' => array(
                                    'type' => 'edge_ngram',
                                    'min_gram' => 3,
                                    'max_gram' => 25,
                                )
                            ),
                            'analyzer' => array(
                                'autocomplete' => array(
                                    'type' => 'custom',
                                    'tokenizer' => 'standard',
                                    'filter' => array(
                                        'standard',
                                        'lowercase',
                                        'stop',
                                        'kstem',
                                        'autocomplete_filter'
                                    )
                                )
                            )
                        );
                        $params['body']['mappings'][$this->_mappingType] = $this->_mappingArray;

                        $response = $this->_elasticSearchClient->indices()->create($params);
                    }
                }
            } catch (\Exception $e) {
                $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.$e->getMessage(), $this->getLogsProcessId(), true);
                return false;
            }
        } else {
            $this->_logsMapper->createNewLog(
                $this->_elasticSearchType,
                __FILE__.':'.__LINE__.' - '.'Invalid array of indexes names passed into method '.__METHOD__.'!',
                $this->getLogsProcessId(), true);
            return false;
        }
        $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.'Creation of new indexes complete', $this->getLogsProcessId());
        return true;
    }

    /**
     * @return array|bool
     * @throws \Exception
     */
    public function determineOldAndNewIndexesNames()
    {
        try {
            $aliasesArray = $this->getAliasesNames();
            $lastTimestamp = $this->_lastIndexMapper->getLastTimestamp($this->_elasticSearchType);
            $this->_newTimestamp = date('U');
            $currentIndexesNamesArray = array();
            $newIndexesNamesArray = array();
            foreach ($aliasesArray as $websiteId => $aliasNamesArray) {
                foreach ($aliasNamesArray as $languageId => $aliasName) {
                    $currentIndexesNamesArray[$websiteId][$languageId] = $aliasName.'_'.$lastTimestamp;
                    $newIndexesNamesArray[$websiteId][$languageId] = $aliasName.'_'.$this->_newTimestamp;
                }
            }
            if (!empty($aliasesArray) && !empty($currentIndexesNamesArray) && !empty($newIndexesNamesArray)) {
                $this->_logsMapper->createNewLog(
                    $this->_elasticSearchType,
                    __FILE__.':'.__LINE__.' - '.'Setup indexes names and aliases complete',
                    $this->getLogsProcessId()
                );
            } else {
                $this->_logsMapper->createNewLog(
                    $this->_elasticSearchType,
                    __FILE__.':'.__LINE__.' - '.'Failed to setup indexes names and aliases',
                    $this->getLogsProcessId(),
                    true
                );
                return false;
            }
            return array($currentIndexesNamesArray, $newIndexesNamesArray);
        } catch (\Exception $e) {
            $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.$e->getMessage(), $this->getLogsProcessId(), true);
            return false;
        }
    }
    /**
     * The function that switches of the use of the old index once the new index is in place after the re-indexing process
     *
     * @param $currentIndexesNamesArray
     * @param $newIndexesNamesArray
     * @return bool
     */
    public function switchAliases($currentIndexesNamesArray, $newIndexesNamesArray)
    {
        try {
            $aliasesArray = $this->getAliasesNames();
            foreach ($aliasesArray as $websiteId => $indexesGroupedByLanguage) {
                foreach ($indexesGroupedByLanguage as $languageId => $indexAlias) {
                    $params = array();
                    $params['index'] = $indexAlias;
                    $params['body'] = array(
                        'actions' => array(
                            array(
                                'add' => array(
                                    'index' => $newIndexesNamesArray[$websiteId][$languageId],
                                    'alias' => $indexAlias
                                ),
                                'remove' => array(
                                    'index' => $currentIndexesNamesArray[$websiteId][$languageId],
                                    'alias' => $indexAlias
                                )
                            )
                        )
                    );
                    $this->_elasticSearchClient->indices()->updateAliases($params);
                }
            }
            $this->_lastIndexMapper->setLastTimestamp($this->_newTimestamp, $this->_elasticSearchType);
            $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '."Alias switch complete", $this->getLogsProcessId(), true);
            $this->_newTimestamp = false;
            return true;
        } catch (\Exception $e) {
            $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.$e->getMessage(), $this->getLogsProcessId(), true);
            return false;
        }
        return true;
    }

    // -----------------------------------------
    // Methods used for logging and flag setting
    // -----------------------------------------

    /**
     *
     */
    public function setLogsProcessId()
    {
        $newProcessId = $this->_logsMapper->getLastProcessId($this->_elasticSearchType) + 1;
        $this->_logsProcessId = $newProcessId;
    }

    /**
     * @return bool
     */
    public function getLogsProcessId()
    {
        return !empty($this->_logsProcessId) ? $this->_logsProcessId : false;
    }

    /**
     * The main method use to determine if any indexing operation is permitted at the moment or not.
     * The lock file is mainly used by the re-indexing process
     *    to stop the indexing of new documents into the old index
     *
     * @return bool
     */
    public function getIndexLockedStatus()
    {
        return false;

        if (file_exists($this->_lockFile)) {
            $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.'Process stopped by lock file', $this->getLogsProcessId());
            return true;
        } else {
            return false;
        }
    }

    /**
     * The main method use to set if any indexing operation is permitted at the moment or not.
     * This is mainly used by the re-indexing process
     *    to stop/start the indexing of new documents into the index
     *
     * @param bool $flag
     * @return bool
     * @throws \Exception
     */
    public function setIndexLockedStatus($flag = true)
    {
        if ($flag === true) {
            if (!file_exists($this->_lockFile)) {
                if ($lockFileHandler = fopen($this->_lockFile, "w")) {
                    $text = "elastic-search";
                    fwrite($lockFileHandler, $text);
                    fclose($lockFileHandler);
                } else {
                    $this->_logsMapper->createNewLog(
                        $this->_elasticSearchType,
                        __FILE__.':'.__LINE__.' - '."Could not create the lock file: {$this->_lockFile}!
                        Make sure you have the correct access rights!"
                        , $this->getLogsProcessId(), true);
                    return false;
                }
            }
            $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.'Lock file created', $this->getLogsProcessId());
        } else {
            if (file_exists($this->_lockFile)) {
                if (!unlink($this->_lockFile)) {
                    $this->_logsMapper->createNewLog(
                        $this->_elasticSearchType,
                        __FILE__.':'.__LINE__.' - '."Could not remove the lock file: {$this->_lockFile}!
                        Make sure you have the correct access rights!"
                        , $this->getLogsProcessId(), true);
                    return false;
                }
            }
            $this->_logsMapper->createNewLog($this->_elasticSearchType, __FILE__.':'.__LINE__.' - '.'Removed lock file. Re-index completed', $this->getLogsProcessId());
        }
        return true;
    }

    /**
     * @param $elasticSearchId
     * @param $index
     * @param $mappingType
     * @return array
     */
    public function checkIfDocumentExists($elasticSearchId, $index, $mappingType)
    {
        $params = array(
            'id' => $elasticSearchId,
            'index' => $index,
            'type' => $mappingType
        );
        return $this->_elasticSearchClient->exists($params);
    }
}
