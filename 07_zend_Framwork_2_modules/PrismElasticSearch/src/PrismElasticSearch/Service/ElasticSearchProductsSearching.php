<?php

namespace PrismElasticSearch\Service;

use PrismElasticSearch\Service\Base\ElasticSearchBaseSearcher;

/**
 * Class ElasticSearchProductsSearching
 * @package PrismElasticSearch\Service
 */
class ElasticSearchProductsSearching extends ElasticSearchBaseSearcher
{
    /**
     * @var bool
     */
    private $_colourFilter = false;
    /**
     * @var bool
     */
    private $_sizeFilter = false;
    /**
     * @var bool
     */
    private $_styleFilter = false;
    /**
     * @var bool
     */
    private $_categoryFilter = false;
    /**
     * @var bool
     */
    private $_priceFilter = false;
    /**
     * @var bool
     */
    private $_productIdsFilter = false;
    /**
     * @var bool
     */
    private $_urlArrayFilter = false;
    /**
     * @var bool
     */
    private $_spectrumIdFilter = false;
    /**
     * @var array
     */
    private $_channelFilter = array(1);

    /**
     * @var array
     */
    protected $_excludeCriteria = array(
        /*array(
            'range' => array(
                'qty' => array(
                    'gt' => 0
                )
            )
        )*/
        // a new array for each restriction
    );

    /**
     * @var array
     */
    protected $_aggregationsArray = array(
        'colours' => array(
            'terms' => array(
                'size' => 0,
                'field' => 'attributes.colour_groups'
            )
        ),
        'categories' => array(
            'terms' => array(
                'size' => 0,
                'field' => 'categories.filter_name'
            )
        ),
        'personalised_items' => array(
            'terms' => array(
                'size' => 0,
                'field' => 'attributes.personalised_items'
            )
        ),
        'recipients' => array(
            'terms' => array(
                'size' => 0,
                'field' => 'attributes.recipients'
            )
        ),
        'product_id_colour' => array(
            'terms' => array(
                'size' => 0,
                'field' => 'product_id_colour'
            )
        ),
        'sizes' => array(
            'terms' => array(
                'size' => 0,
                'field' => 'attributes.size'
            )
        ),
        'min_price' => array(
            'min' => array(
                'field' => 'attributes.price'
            )
        ),
        'max_price' => array(
            'max' => array(
                'field' => 'attributes.price'
            )
        )
    );

    /**
     * @var bool
     */
    protected $_searchType = false;

    /**
     * @var bool
     */
    protected $_customFields = false;

    /**
     * @param $channelsArray
     */
    public function applyChannelFilter($channelsArray)
    {
        $this->_channelFilter = $channelsArray;
    }

    /**
     * @param $coloursArray
     */
    public function applyColourFilter($coloursArray)
    {
        foreach($coloursArray as &$colour) {
            $colour = strtolower($colour);
        }
        $this->_colourFilter = $coloursArray;
    }

    /**
     * @param $productIdArray
     */
    public function applyProductIdFilter($productIdArray)
    {
        foreach($productIdArray as &$productId) {
            $productId = (int) $productId;
        }
        $this->_productIdsFilter = $productIdArray;
    }

    /**
     * @param $urlArray
     */
    public function applyUrlFilter($urlArray)
    {
        foreach($urlArray as &$url) {
            $url = strtolower($url);
        }
        $this->_urlArrayFilter = $urlArray;
    }

    /**
     * The size array passed must be in this format
     *  array(
     *      'gt' => value or false, -> greater than
     *      'gte' => value or false, -> greater than or equal to
     *      'lt' => value or false, -> less than
     *      'lte' => value or false, -> less than or equal to
     *      'exact' => value or false -> equal to
     *  )
     *
     * @param $sizeArray
     */
    public function applySizeFilter($sizeArray)
    {
        $this->_sizeFilter = $sizeArray;
    }

    /**
     * @param $style
     */
    public function applyStyleFilter($style)
    {
        $this->_styleFilter = $style;
    }

    /**
     * The price array passed must be in this format
     *  array(
     *      'gt' => value or false, -> greater than
     *      'gte' => value or false, -> greater than or equal to
     *      'lt' => value or false, -> less than
     *      'lte' => value or false, -> less than or equal to
     *      'exact' => value or false -> equal to
     *  )
     *
     * @param $priceArray
     */
    public function applyPriceFilter($priceArray)
    {
        $this->_priceFilter = $priceArray;
    }

    /**
     * @param $spectrumIdArray
     */
    public function applySpectrumIdFilter($spectrumIdArray)
    {
        $this->_spectrumIdFilter = $spectrumIdArray;
    }

    /**
     * @param $category
     */
    public function applyCategoryFilter($category)
    {
        $this->_categoryFilter = $category;
    }

    /**
     * @throws \Exception
     */
    public function buildFinalFilter()
    {
        $finalQuery = array();
        // 1. check if a category filter is set
        if ($this->_categoryFilter !== false) {
            $this->_filterCount++;
            $finalQuery['and'][] = array(
                'nested' => array(
                    'path' => 'categories',
                    'filter' => array(
                        'terms' => array('categories.id' => $this->_categoryFilter)
                    )
                )
            );
        }

        // 2. check if a colour filter is set
        if ($this->_channelFilter !== false) {
            $this->_filterCount++;
            $finalQuery['and'][] = array(
                'terms' => array('channel' => $this->_channelFilter)
            );
        }

        // 3. check if a colour filter is set
        if ($this->_colourFilter !== false) {
            $this->_filterCount++;
            $finalQuery['and'][] = array(
                'terms' => array('attributes.colour_groups' => $this->_colourFilter)
            );
        }

        // 4. check if a style filter is set
        if ($this->_styleFilter !== false) {
            $this->_filterCount++;
            $finalQuery['and'][] = array(
                'terms' => array('style' => $this->_styleFilter)
            );
        }

        // 5. check if a size filter is set
        if ($this->_sizeFilter !== false) {
            $this->_filterCount++;
            $finalQuery['and'][] = array(
                'terms' => array('attributes.size' => $this->_sizeFilter)
            );
        }

        // 6. check if a price filter is set
        if ($this->_priceFilter !== false) {
            $priceFilter = $this->validateRangeArrayFormatAndExtractValues($this->_priceFilter);
            if($priceFilter === false) {
                throw new \Exception('Invalid format for the price filter provided to Elastic Search Service');
            }
            if (isset($priceFilter['term'])) {
                // exact search
                $this->_filterCount++;
                $finalQuery['and'][] = array(
                    'term' => array('attributes.price' => $priceFilter['term'])
                );
            } else {
                // range search
                $this->_filterCount++;
                $finalQuery['and'][] = array(
                    'range' => array('attributes.price' => $priceFilter['range'])
                );
            }
        }

        // 7. check if a custom field term filter is set
        if (count($this->_termFilter) > 0) {
            foreach($this->_termFilter as $field => $value) {
                $this->_filterCount++;
                $finalQuery['and'][] = array(
                    'term' => array('attributes.'.$field => $value)
                );
            }
        }


        // Check if custom field term is set with custom field
        // 7. check if a custom field term filter is set
        if (count($this->_customTermFieldFilter) > 0) {
            foreach($this->_customTermFieldFilter as $field => $value) {
                $this->_filterCount++;
                $finalQuery['and'][] = array(
                    'term' => array($field => $value)
                );
            }
        }


        // 8. check if a custom field range filter is set
        if ($this->_rangeFilter !== false) {
            foreach($this->_rangeFilter as $field => $rangeArray) {
                $customRangeFilter = $this->validateRangeArrayFormatAndExtractValues($rangeArray);
                if (isset($customRangeFilter['term'])) {
                    // exact search
                    $finalQuery['and'][] = array(
                        'term' => array('attributes.'.$field => $customRangeFilter['term'])
                    );
                } else {
                    // range search
                    $finalQuery['and'][] = array(
                        'range' => array('attributes.'.$field => $customRangeFilter['range'])
                    );
                }
            }
        }

        // 9. check if a url filter is set
        if ($this->_urlArrayFilter !== false) {
            $this->_filterCount++;
            $finalQuery['and'][] = array(
                'terms' => array('url' => $this->_urlArrayFilter)
            );
        }

        // 10. check if a productId filter is set
        if ($this->_productIdsFilter !== false) {
            $this->_filterCount++;
            $finalQuery['and'][] = array(
                'terms' => array('productId' => $this->_productIdsFilter)
            );
        }

        // 11. check if a spectrumId filter is set
        if ($this->_spectrumIdFilter !== false) {
            $this->_filterCount++;
            $finalQuery['and'][] = array(
                'terms' => array('spectrumId' => $this->_spectrumIdFilter)
            );
        }

        // 12. Check if custom search fields are set

        // 13. Check if type is set


        $this->_filters = $finalQuery;
    }

    public function applySearchType($searchType)
    {
        $this->_searchType = $searchType;
    }

    public function applyCustomFields($customFields)
    {
        $this->_customFields = $customFields;
    }



    /**
     * @param $websiteId
     * @param $languageId
     * @param string $searchString
     * @param array $filtersArray
     * @param int $from
     * @param int $size
     * @param array|bool $customOrder
     * @return array
     */
    public function buildParamsArray($websiteId,
                    $languageId,
                    $searchString = '',
                    $filtersArray = array(),
                    $from = 0,
                    $size = 20,
                    $customOrder = false,
                    $type = '',
                    $customFields = array())
    {
        $parametersArray = array(
            'websiteID' => $websiteId,
            'languageID' => $languageId,
            'from' => $from,
            'size' => $size,
            'searchString' => !empty($searchString) ? $searchString : false,
            'searchType' => !empty($type) ? $type : false,
            'customFields' => !empty($customFields) ? $customFields : false,
            'filters' => array(
                'categories' => !empty($filtersArray['categories']) ? $filtersArray['categories'] : false,
                'colours' => !empty($filtersArray['colours']) ? $filtersArray['colours'] : false,
                'sizes' => !empty($filtersArray['sizes']) ? $filtersArray['sizes'] : false,
                'styles' => !empty($filtersArray['styles']) ? $filtersArray['styles'] : false,
                'price' => !empty($filtersArray['price']) ? $filtersArray['price'] : false,
                'custom' => !empty($filtersArray['custom']) ? $filtersArray['custom'] : false,
                'customFieldFilter' => !empty($filtersArray['customFieldFilter']) ? $filtersArray['customFieldFilter'] : array(),
                'url' => !empty($filtersArray['url']) ? $filtersArray['url'] : false,
                'productId' => !empty($filtersArray['productId']) ? $filtersArray['productId'] : false,
                'spectrumId' => !empty($filtersArray['spectrumId']) ? $filtersArray['spectrumId'] : false,
                'elasticSearchIds' => !empty($filtersArray['elasticSearchIds']) ? $filtersArray['elasticSearchIds'] : false,
                'customOrder' => !empty($customOrder) ? $customOrder : false,
                'customAggregations' => !empty($filtersArray['customAggregations']) ? $filtersArray['customAggregations'] : false
            )
        );

        return $parametersArray;
    }

    public function resetFilters()
    {
        $this->_colourFilter = false;
        $this->_sizeFilter = false;
        $this->_styleFilter = false;
        $this->_categoryFilter = false;
        $this->_priceFilter = false;
        $this->_productIdsFilter = false;
        $this->_urlArrayFilter = false;
        $this->_spectrumIdFilter = false;
        $this->_searchType = false;
        $this->_customFields = false;
        $this->_channelFilter = array(1);
        $this->_customTermFieldFilter = array();
    }

    /**
     * @param $paramsArray
     * @throws \Exception
     * @returns array
     */
    public function searchByParams($paramsArray)
    {
        if ($paramsArray != false) {
            // validate the received array
            switch (true) {
                // validate the incoming data structure
                case is_array($paramsArray)
                    && isset($paramsArray['websiteID'])
                    && isset($paramsArray['languageID'])
                    && isset($paramsArray['from'])
                    && isset($paramsArray['size'])
                    && isset($paramsArray['searchString'])
                    && isset($paramsArray['filters'])
                    && isset($paramsArray['filters']['categories'])
                    && isset($paramsArray['filters']['colours'])
                    && isset($paramsArray['filters']['sizes'])
                    && isset($paramsArray['filters']['styles'])
                    && isset($paramsArray['filters']['price'])
                    && isset($paramsArray['filters']['url'])
                    && isset($paramsArray['filters']['productId'])
                    && isset($paramsArray['filters']['custom'])
                    && isset($paramsArray['filters']['spectrumId'])
                    && isset($paramsArray['filters']['elasticSearchIds'])
                    && isset($paramsArray['filters']['customOrder'])
                    && isset($paramsArray['filters']['customAggregations'])
                    && isset($paramsArray['filters']['customFieldFilter'])
                    && isset($paramsArray['searchType'])
                    && isset($paramsArray['customFields']):

                    $websiteID = (int) $paramsArray['websiteID'];
                    if (empty($websiteID)) {
                        throw new \Exception('Invalid websiteID specified as a parameter!', 400);
                    }

                    $languageID = (int) $paramsArray['languageID'];
                    if (empty($languageID)) {
                        throw new \Exception('Invalid languageID specified as a parameter!', 400);
                    }

                    // set the searching location
                    try {
                        $this->setSearchLocation($websiteID, $languageID);
                    } catch (\Exception $e) {
                        throw new \Exception($e, 400);
                    }

                    if (!empty($paramsArray['filters']['elasticSearchIds']) && is_array($paramsArray['filters']['elasticSearchIds'])) {
                        $elasticSearchResponse = $this->getByIds($paramsArray['filters']['elasticSearchIds']);
                        if (isset($elasticSearchResponse['docs'])) {
                            $response = array('docs' => array());
                            foreach ($elasticSearchResponse['docs'] as $docs) {
                                if ($docs['found'] === true) {
                                    $response['docs'][] = $docs['_source'];
                                }
                            }
                        } else {
                            $response = $elasticSearchResponse;
                        }
                        return $response;
                    }

                    $limitFrom = (int) $paramsArray['from'];
                    if ($limitFrom < 0) {
                        throw new \Exception('Invalid \'from\' parameter specified', 400);
                    }

                    $limitSize = (int) $paramsArray['size'];
                    if ($limitSize < 0) {
                        throw new \Exception('Invalid \'size\' parameter specified', 400);
                    }

                    // validate filters array
                    $categories = $paramsArray['filters']['categories'];
                    if(!empty($categories)) {
                        if (!(is_array($categories) && count($categories) > 0)) {
                            throw new \Exception('Invalid category specified in the filter section!', 400);
                        } else {
                            $this->applyCategoryFilter($categories);
                        }
                    }

                    $colours = $paramsArray['filters']['colours'];
                    if(!empty($colours)) {
                        if (!(is_array($colours) && count($colours) > 0)) {
                            throw new \Exception('Invalid colours parameter specified in the filter section!', 400);
                        } else {
                            $this->applyColourFilter($colours);
                        }
                    }

                    $styles = $paramsArray['filters']['styles'];
                    if(!empty($styles)) {
                        if (!(is_array($styles) && count($styles) > 0)) {
                            throw new \Exception('Invalid styles specified in the filter section!', 400);
                        } else {
                            $this->applyStyleFilter($styles);
                        }
                    }

                    $sizes = $paramsArray['filters']['sizes'];
                    if(!empty($sizes)) {
                        if (!(is_array($sizes) && count($sizes) > 0)) {
                            throw new \Exception('Invalid sizes specified in the filter section!', 400);
                        } else {
                            $this->applySizeFilter($sizes);
                        }
                    }

                    $priceArray = $paramsArray['filters']['price'];
                    if(!empty($priceArray)) {
                        $this->applyPriceFilter($priceArray);
                    }

                    $urlArray = $paramsArray['filters']['url'];
                    if(!empty($urlArray)) {
                        $this->applyUrlFilter($urlArray);
                    }

                    $productIdArray = $paramsArray['filters']['productId'];
                    if(!empty($productIdArray)) {
                        $this->applyProductIdFilter($productIdArray);
                    }

                    $spectrumIdArray = $paramsArray['filters']['spectrumId'];
                    if(!empty($spectrumIdArray)) {
                        $this->applySpectrumIdFilter($spectrumIdArray);
                    }
                    $customOrder = false;
                    if(!empty($paramsArray['filters']['customOrder'])) {
                        $customOrder = $paramsArray['filters']['customOrder'];
                    }

                    $customFilters = $paramsArray['filters']['custom'];
                    if (!empty($customFilters)) {
                        foreach ($customFilters as $filterName => $customFilterArray) {
                            if (empty($filterName)) {
                                throw new \Exception('Custom filter name is empty!', 400);
                            }
                            if (empty($customFilterArray['type'])) {
                                throw new \Exception('No type specified for custom filter: '.$filterName.'!', 400);
                            }
                            if (empty($customFilterArray['value'])) {
                                throw new \Exception('No value specified for custom filter: '.$filterName.'!', 400);
                            }
                            switch ($customFilterArray['type']) {
                                case 'single':
                                    $this->applyCustomAttributeFilter($filterName, $customFilterArray['value']);
                                    break;
                                case 'range':
                                    $this->applyCustomAttributeRangeFilter($filterName, $customFilterArray['value']);
                                    break;
                                default:
                                    throw new \Exception('Invalid type specified for custom filter: '.$filterName.'!', 400);
                            }
                        }
                    }

                    $customFieldFilters = $paramsArray['filters']['customFieldFilter'];
                    if (!empty($customFieldFilters)) {
                        foreach ($customFieldFilters as $filterName => $customFieldFiltersArray) {
                            if (empty($filterName)) {
                                throw new \Exception('Custom filter name is empty!', 400);
                            }
                            if (empty($customFieldFiltersArray['type'])) {
                                throw new \Exception('No type specified for custom filter: '.$filterName.'!', 400);
                            }
                            if (empty($customFieldFiltersArray['value'])) {
                                throw new \Exception('No value specified for custom filter: '.$filterName.'!', 400);
                            }
                            switch ($customFieldFiltersArray['type']) {
                                case 'single':
                                    $this->applyCustomTermFieldFilter($filterName, $customFieldFiltersArray['value']);
                                    break;
                                default:
                                    throw new \Exception('Invalid type specified for custom filter: '.$filterName.'!', 400);
                            }
                        }
                    }




                    try {
                        $this->buildFinalFilter();
                    } catch (\Exception $e) {
                        throw new \Exception($e, 400);
                    }

                    // if filters array is empty then we must at least have a search Param
                    $searchText = empty($paramsArray['searchString']) ? false : $paramsArray['searchString'];
                    if (!$this->hasFilters()) {
                        if (empty($searchText)) {
                            throw new \Exception('You must specify at least a search phrase or a filter!', 400);
                        }
                    }


                    $customAggregations = array();
                    if (!empty($paramsArray['filters']['customAggregations']) && is_array($paramsArray['filters']['customAggregations'])) {
                        $customAggregations = $paramsArray['filters']['customAggregations'];
                    } else if (!empty($paramsArray['filters']['customAggregations'])) {
                        throw new \Exception('The custom aggregations must be an valid array !', 400);
                    }

                    // Check if type & custom fields are set
                    $customSearch = false;
                    if(!empty($paramsArray['searchType']) && !empty($paramsArray['customFields'])) {
                        $customSearch = array(
                                'type' => $paramsArray['searchType'],
                                'customFields' => $paramsArray['customFields']
                        );
                    }

                    try {
                        $elasticSearchResponse = $this->doSearch($searchText, $limitFrom, $limitSize, $customOrder, $customAggregations, $customSearch);
                        $returnArray = array('success' => true, 'response' => $elasticSearchResponse);
                        return $returnArray;
                    } catch (\Exception $e) {
                        throw new \Exception($e, 500);
                    }
                    break;
                default:
                    throw new \Exception('Invalid structure for the document sent!', 400);
            }
        } else {
            throw new \Exception('No data was submitted', 400);
        }
    }

    /**
     * @param $paramsArray
     * @throws \Exception
     * @returns array
     */
    public function getAutoCompleteResults($paramsArray)
    {
        if ($paramsArray != false) {
            // validate the received array
            switch (true) {
                // validate the incoming data structure
                case is_array($paramsArray)
                    && isset($paramsArray['websiteID'])
                    && isset($paramsArray['languageID'])
                    && isset($paramsArray['size'])
                    && isset($paramsArray['searchString']):

                    $websiteID = (int) $paramsArray['websiteID'];
                    if (empty($websiteID)) {
                        throw new \Exception('Invalid websiteID specified as a parameter!', 400);
                    }

                    $languageID = (int) $paramsArray['languageID'];
                    if (empty($languageID)) {
                        throw new \Exception('Invalid languageID specified as a parameter!', 400);
                    }

                    $searchString = $paramsArray['searchString'];
                    if (empty($searchString)) {
                        throw new \Exception('Invalid search string specified as a parameter!', 400);
                    }

                    // set the searching location
                    try {
                        $this->setSearchLocation($websiteID, $languageID);
                    } catch (\Exception $e) {
                        throw new \Exception($e, 400);
                    }

                    $limitSize = (int) $paramsArray['size'];
                    if ($limitSize < 0) {
                        throw new \Exception('Invalid \'size\' parameter specified', 400);
                    }

                    try {
                        $elasticSearchResponse = $this->doAutoCompleteSearch($searchString, $limitSize);
                        $returnArray = array('success' => true, 'response' => $elasticSearchResponse);
                        return $returnArray;
                    } catch (\Exception $e) {
                        throw new \Exception($e, 500);
                    }
                    break;
                default:
                    throw new \Exception('Invalid structure for the document sent!', 400);
            }
        } else {
            throw new \Exception('No data was submitted', 400);
        }
    }
}
