<?php

namespace PrismElasticSearch\Service\Manager;

use PrismElasticSearch\Enum\ElasticSearchTypes;

/**
 * Class ElasticSearchSearchingManager
 * @package PrismElasticSearch\Service\Manager
 */
class ElasticSearchSearchingManager
{
    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    private $_serviceLocator;
    /**
     * @var bool
     */
    private $_indexingService = false;
    /**
     * @var
     */
    private $_indexType;

    /**
     * Set the service manager
     */
    public function __construct(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $this->_serviceLocator = $serviceLocator;
    }

    /**
     * @param int $indexType
     * @throws \Exception
     */
    public function setupIndexCategory($indexType = ElasticSearchTypes::PRODUCTS)
    {
        switch ($indexType) {
            case ElasticSearchTypes::PRODUCTS:
                $this->_indexingService = $this->_serviceLocator->get('ElasticSearchProductsSearching');
                break;
            case ElasticSearchTypes::BLOG_POSTS:
                $this->_indexingService = $this->_serviceLocator->get('ElasticSearchBlogPostsSearching');
                break;
            case ElasticSearchTypes::WEBSITE_CONTENT:
                $this->_indexingService = $this->_serviceLocator->get('ElasticSearchWebsiteContentSearching');
                break;
            default:
                throw new \Exception("Unknown index category: ".$indexType."!");
        }
        $this->_indexType = $indexType;
    }

    /**
     * $method is one of the methods available in the specific service
     * $args is an array of arguments for that method
     *  -- basic methods:
     *      ->index($elasticSearchIdsArray, $batchSize = 100)
     *      ->update($elasticSearchId)
     *      ->delete($elasticSearchId, $websiteId = false)
     * @param $method
     * @param $args
     * @throws \Exception
     * @returns mixed
     */
    public function __call($method, $args)
    {
        if ($this->_indexingService === false) {
            throw new \Exception("You must call the setupIndexCategory method first and specify an index type you want to access the searching operations on!");
        }

        try {
            return call_user_func_array(array($this->_indexingService, $method), $args);
        } catch(\Exception $e) {
            throw new \Exception($e);
        }
    }
}
