<?php

namespace PrismElasticSearch\Service\Manager;

use \PrismElasticSearch\Enum\ElasticSearchTypes;

/**
 * Class ElasticSearchIndexingManager
 * @package PrismElasticSearch\Service\Manager
 */
class ElasticSearchIndexingManager
{
    /**
     * @var bool
     */
    private $_indexingService = false;
    /**
     * @var \PrismElasticSearch\Model\ElasticSearchLogsMapper
     */
    private $_logsMapper;
    /**
     * @var
     */
    private $_indexType;
    /**
     * @var
     */
    private $_productsIndexer;
    /**
     * @var
     */
    private $_contentIndexer;

    /**
     * Set the service manager
     */
    public function __construct(
        \PrismElasticSearch\Model\ElasticSearchLogsMapper $logsMapper,
        $productsIndexer,
        $contentIndexer
    )
    {
        $this->_logsMapper = $logsMapper;
        $this->_productsIndexer = $productsIndexer;
        $this->_contentIndexer = $contentIndexer;
    }

    /**
     * @param int $indexType
     * @throws \Exception
     */
    public function setupIndexCategory($indexType = ElasticSearchTypes::PRODUCTS)
    {
        switch ($indexType) {
            case ElasticSearchTypes::PRODUCTS:
                $this->_indexingService = $this->_productsIndexer;
                break;
            /*
             * Removed because all the indexing operations happen on the blog side
            case ElasticSearchTypes::BLOG_POSTS:
                $this->_indexingService = $this->_serviceLocator->get('ElasticSearchBlogPostsIndexer');
                break;
            */
            case ElasticSearchTypes::WEBSITE_CONTENT:
                $this->_indexingService = $this->_contentIndexer;
                break;
            default:
                throw new \Exception("Unknown index category: ".$indexType."!");
        }
        $this->_indexType = $indexType;
    }

    /**
     * $method is one of the methods available in the specific service
     * $args is an array of arguments for that method
     *  -- basic methods:
     *      ->index($elasticSearchIdsArray, $batchSize = 100)
     *      ->update($elasticSearchId)
     *      ->delete($elasticSearchId, $websiteId = false)
     * @param $method
     * @param $args
     * @throws \Exception
     * @returns mixed
     */
    public function __call($method, $args)
    {
        if ($this->_indexingService === false) {
            throw new \Exception("You must call the setupIndexCategory method first and specify an index type you want to access the index operations on!");
        }

        try {
            $this->_indexingService->setLogsProcessId();
            return call_user_func_array(array($this->_indexingService, $method), $args);
        } catch(\Exception $e) {
            throw new \Exception($e);
        }
    }
}
