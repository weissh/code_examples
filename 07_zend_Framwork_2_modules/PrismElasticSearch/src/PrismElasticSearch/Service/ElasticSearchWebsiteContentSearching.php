<?php

namespace PrismElasticSearch\Service;

use PrismElasticSearch\Service\Base\ElasticSearchBaseSearcher;

/**
 * Class ElasticSearchWebsiteContentSearching
 * @package PrismElasticSearch\Service
 */
class ElasticSearchWebsiteContentSearching extends ElasticSearchBaseSearcher
{
    /**
     *
     */
    public function buildFinalFilter()
    {
        $this->_filters = false;
    }

    /**
     * @param $websiteId
     * @param $languageId
     * @param string $searchString
     * @param array $filtersArray
     * @param int $from
     * @param int $size
     * @param bool $customOrder
     * @return array
     */
    public function buildParamsArray($websiteId, $languageId, $searchString = '', $filtersArray = array(), $from = 0, $size = 20, $customOrder = false)
    {
        $parametersArray = array(
            'websiteID' => $websiteId,
            'languageID' => $languageId,
            'from' => $from,
            'size' => $size,
            'searchString' => !empty($searchString) ? $searchString : false
        );
        return $parametersArray;
    }

    public function resetFilters()
    {
        $this->_filters = false;
    }

    /**
     * @param $paramsArray
     * @throws \Exception
     * @returns array
     */
    public function searchByParams($paramsArray)
    {
        if ($paramsArray != false) {
            // validate the received array
            switch (true) {
                // validate the incoming data structure
                case is_array($paramsArray)
                    && isset($paramsArray['websiteID'])
                    && isset($paramsArray['languageID'])
                    && isset($paramsArray['from'])
                    && isset($paramsArray['size'])
                    && isset($paramsArray['searchString']):

                    $websiteID = (int) $paramsArray['websiteID'];
                    if (empty($websiteID)) {
                        throw new \Exception('Invalid websiteID specified as a parameter!', 400);
                    }

                    $languageID = (int) $paramsArray['languageID'];
                    if (empty($languageID)) {
                        throw new \Exception('Invalid languageID specified as a parameter!', 400);
                    }

                    // set the searching location
                    try {
                        $this->setSearchLocation($websiteID, $languageID);
                    } catch (\Exception $e) {
                        throw new \Exception($e, 400);
                    }

                    $limitFrom = (int) $paramsArray['from'];
                    if ($limitFrom < 0) {
                        throw new \Exception('Invalid \'from\' parameter specified', 400);
                    }

                    $limitSize = (int) $paramsArray['size'];
                    if ($limitSize <= 0) {
                        throw new \Exception('Invalid \'size\' parameter specified', 400);
                    }

                    try {
                        $elasticSearchResponse = $this->doSearch($paramsArray['searchString'], $limitFrom, $limitSize);
                        $returnArray = array('success' => true, 'response' => $elasticSearchResponse);
                        return $returnArray;
                    } catch (\Exception $e) {
                        throw new \Exception($e, 500);
                    }
                    break;
                default:
                    throw new \Exception('Invalid structure for the document sent!', 400);
            }
        } else {
            throw new \Exception('No data was submitted', 400);
        }
    }

    public function getAutoCompleteResults($paramsArray)
    {
        throw new \Exception('Access denied!', 400);
    }
}