<?php

namespace PrismElasticSearch\Factory\Service\Search;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Elasticsearch\Client as ElasticSearchClient;
use PrismElasticSearch\Service\ElasticSearchBlogPostsSearching;

/**
 * Class ElasticSearchBlogPostsSearchingFactory
 * @package PrismElasticSearch\Factory\Service\Search
 */
class ElasticSearchBlogPostsSearchingFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ElasticSearchBlogPostsSearching
     */
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        $elasticSearchConfig = $config['elasticSearchConfig'];
        $elasticSearchVars = $config['elasticSearchBlogPostsIndexVariables'];

        if ($serviceLocator->has('CommonStorage')) {
            $configSession = $serviceLocator->get('CommonStorage');
            $siteConfig['siteConfig'] = $configSession->read('siteConfig');
        } else {
            $siteConfig = $config;
        }
        $languagesConfig = $siteConfig['siteConfig']['languages']['site-languages'];

        $indexesArray = array();
        foreach ($languagesConfig as $languageId) {
            $indexesArray[1][$languageId] = $elasticSearchVars['indexPrefix'].'_'.$languageId;
        }

        unset($languagesConfig);
        unset($config);

        $elasticSearchClient = new ElasticSearchClient($elasticSearchConfig);
        $searchingService = new ElasticSearchBlogPostsSearching($elasticSearchClient, $indexesArray, $elasticSearchVars['mappingType']);

        return $searchingService;
    }
} 