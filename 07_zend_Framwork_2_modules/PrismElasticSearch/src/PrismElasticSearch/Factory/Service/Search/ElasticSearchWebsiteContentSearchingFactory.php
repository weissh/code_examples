<?php

namespace PrismElasticSearch\Factory\Service\Search;

use PrismElasticSearch\Enum\ElasticSearchTypes;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Elasticsearch\Client as ElasticSearchClient;
use PrismElasticSearch\Service\ElasticSearchWebsiteContentSearching;

class ElasticSearchWebsiteContentSearchingFactory implements FactoryInterface
{
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        $elasticSearchConfig = $config['elasticSearchConfig'];
        $elasticSearchVars = $config['elasticSearchWebsiteContentIndexVariables'];

        if ($serviceLocator->has('CommonStorage')) {
            $configSession = $serviceLocator->get('CommonStorage');
            $siteConfig['siteConfig'] = $configSession->read('siteConfig');
        } else {
            $siteConfig = $config;
        }

        $languagesConfig = $siteConfig['siteConfig']['languages']['site-languages'];
        $websitesAvailable = $siteConfig['siteConfig']['websites']['websites'];

        if ($serviceLocator->has('ElasticSearchPrioritiesService')) {
            $elasticSearchPrioritiesService = $serviceLocator->get('ElasticSearchPrioritiesService');
            $prioritiesArray = $elasticSearchPrioritiesService->getSearchPrioritiesApi(ElasticSearchTypes::WEBSITE_CONTENT);
        } else {
            /** @var \PrismElasticSearch\Model\ElasticSearchMappingDetailsMapper $elasticSearchMappingDetailsMapper */
            $elasticSearchMappingDetailsMapper = $serviceLocator->get('ElasticSearchMappingDetailsMapper');
            $prioritiesArray = $elasticSearchMappingDetailsMapper->getSearchPriorities(ElasticSearchTypes::WEBSITE_CONTENT);
        }

        $indexesArray = array();
        foreach($websitesAvailable as $websiteId => $websiteDescription) {
            $currentIndexPrefix = $elasticSearchVars['indexPrefix'].$websiteId;
            foreach($languagesConfig as $languageId) {
                $indexesArray[$websiteId][$languageId] = $currentIndexPrefix.'_'.$languageId;
            }
        }

        unset($websitesAvailable);
        unset($languagesConfig);
        unset($config);

        $elasticSearchClient = new ElasticSearchClient($elasticSearchConfig);
        $searchingService = new ElasticSearchWebsiteContentSearching($elasticSearchClient, $indexesArray, $elasticSearchVars['mappingType'], $prioritiesArray);

        return $searchingService;
    }
} 