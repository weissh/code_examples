<?php

namespace PrismElasticSearch\Factory\Service\Index;

use PrismElasticSearch\Service\Base\ElasticSearchBaseIndexer;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Elasticsearch\Client as ElasticSearchClient;
use PrismElasticSearch\Enum\ElasticSearchTypes;

/**
 * Class ElasticSearchWebsiteContentIndexerFactory
 * @package PrismElasticSearch\Factory\Service\Index
 */
class ElasticSearchWebsiteContentIndexerFactory implements FactoryInterface
{

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ElasticSearchBaseIndexer
     */
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        $elasticSearchConfig = $config['elasticSearchConfig'];
        $elasticSearchVars = $config['elasticSearchVariables'];
        $elasticSearchVars = array_merge($elasticSearchVars, $config['elasticSearchWebsiteContentIndexVariables']);

        $configSession =  $serviceLocator->get('CommonStorage');
        $siteConfig['siteConfig'] = $configSession->read('siteConfig');
        $languagesConfig = $siteConfig['siteConfig']['languages']['site-languages'];
        $websitesAvailable = $siteConfig['siteConfig']['websites']['websites'];

        /** @var \PrismElasticSearch\Model\ElasticSearchLogsMapper $elasticSearchLogsMapper */
        $elasticSearchLogsMapper = $serviceLocator->get('ElasticSearchLogsMapper');
        /** @var \PrismElasticSearch\Model\ElasticSearchIndexQueueMapper $elasticSearchIndexQueueMapper */
        $elasticSearchIndexQueueMapper = $serviceLocator->get('ElasticSearchIndexQueueMapper');
        /** @var \PrismElasticSearch\Model\ElasticSearchLastIndexMapper $elasticSearchLastIndexMapper */
        $elasticSearchLastIndexMapper = $serviceLocator->get('ElasticSearchLastIndexMapper');
        /** @var \PrismElasticSearch\Model\ElasticSearchWebsiteContentMapper $websiteContentMapper */
        $websiteContentMapper = $serviceLocator->get('ElasticSearchWebsiteContentMapper');
        /** @var \PrismElasticSearch\Model\ElasticSearchMappingDetailsMapper $elasticSearchMappingDetailsMapper */
        $elasticSearchMappingDetailsMapper = $serviceLocator->get('ElasticSearchMappingDetailsMapper');

        $elasticSearchType = ElasticSearchTypes::WEBSITE_CONTENT;
        $mappingArray = $elasticSearchMappingDetailsMapper->getMappingDetailsArray($elasticSearchType);

        $indexesArray = array();
        foreach($websitesAvailable as $websiteId => $websiteDescription) {
            $currentIndexPrefix = $elasticSearchVars['indexPrefix'].$websiteId;
            foreach($languagesConfig as $languageId) {
                $indexesArray[$websiteId][$languageId] = $currentIndexPrefix.'_'.$languageId;
            }
        }

        unset($websitesAvailable);
        unset($languagesConfig);
        unset($config);

        $elasticSearchClient = new ElasticSearchClient($elasticSearchConfig);
        $indexingService = new ElasticSearchBaseIndexer(
            $elasticSearchClient,
            $elasticSearchLogsMapper,
            $elasticSearchIndexQueueMapper,
            $elasticSearchLastIndexMapper,
            $websiteContentMapper,
            $elasticSearchType,
            $indexesArray,
            $elasticSearchVars,
            $mappingArray
        );

        return $indexingService;
    }
} 