<?php

namespace PrismElasticSearch\Factory\Service\Index;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Elasticsearch\Client as ElasticSearchClient;
use PrismElasticSearch\Service\Base\ElasticSearchBaseIndexer;
use PrismElasticSearch\Enum\ElasticSearchTypes;

/**
 * Class ElasticSearchProductsIndexerFactory
 * @package PrismElasticSearch\Factory\Service\Index
 */
class ElasticSearchProductsIndexerFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ElasticSearchBaseIndexer
     */
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');
        $elasticSearchConfig = $config['elasticSearchConfig'];
        $elasticSearchVars = $config['elasticSearchVariables'];
        $elasticSearchVars = array_merge($elasticSearchVars, $config['elasticSearchProductsIndexVariables']);

        $configSession =  $serviceLocator->get('CommonStorage');
        $siteConfig['siteConfig'] = $configSession->read('siteConfig');
        $languagesConfig = $siteConfig['siteConfig']['languages']['site-languages'];
        $websitesAvailable = $siteConfig['siteConfig']['websites']['websites'];

        /** @var \PrismElasticSearch\Model\ElasticSearchLogsMapper $elasticSearchLogsMapper */
        $elasticSearchLogsMapper = $serviceLocator->get('ElasticSearchLogsMapper');
        /** @var \PrismElasticSearch\Model\ElasticSearchIndexQueueMapper $elasticSearchIndexQueueMapper */
        $elasticSearchIndexQueueMapper = $serviceLocator->get('ElasticSearchIndexQueueMapper');
        /** @var \PrismElasticSearch\Model\ElasticSearchLastIndexMapper $elasticSearchLastIndexMapper */
        $elasticSearchLastIndexMapper = $serviceLocator->get('ElasticSearchLastIndexMapper');
        /** @var \PrismElasticSearch\Model\ElasticSearchMappingDetailsMapper $elasticSearchMappingDetailsMapper */
        $elasticSearchMappingDetailsMapper = $serviceLocator->get('ElasticSearchMappingDetailsMapper');
        /** @var \PrismElasticSearch\Model\ElasticSearchProductsMapper $productsMapper */
        $productsMapper = $serviceLocator->get('ElasticSearchProductsMapper');

        $elasticSearchType = ElasticSearchTypes::PRODUCTS;
        $mappingArray = $elasticSearchMappingDetailsMapper->getMappingDetailsArray($elasticSearchType);

        $indexesArray = array();
        foreach($websitesAvailable as $websiteId => $websiteDescription) {
            $currentIndexPrefix = $elasticSearchVars['indexPrefix'].$websiteId;
            foreach($languagesConfig as $languageId) {
                $indexesArray[$websiteId][$languageId] = $currentIndexPrefix.'_'.$languageId;
            }
        }

        unset($websitesAvailable);
        unset($languagesConfig);
        unset($config);

        $elasticSearchClient = new ElasticSearchClient($elasticSearchConfig);
        $indexingService = new ElasticSearchBaseIndexer(
            $elasticSearchClient,
            $elasticSearchLogsMapper,
            $elasticSearchIndexQueueMapper,
            $elasticSearchLastIndexMapper,
            $productsMapper,
            $elasticSearchType,
            $indexesArray,
            $elasticSearchVars,
            $mappingArray
        );

        return $indexingService;
    }
} 