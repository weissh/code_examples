<?php

namespace PrismElasticSearch\Factory\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Elasticsearch\Client as ElasticSearchClient;
use PrismElasticSearch\Service\Manager\ElasticSearchSearchingManager;

class ElasticSearchSearchingManagerFactory implements FactoryInterface
{
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $searchingService = new ElasticSearchSearchingManager($serviceLocator);
        return $searchingService;
    }
} 