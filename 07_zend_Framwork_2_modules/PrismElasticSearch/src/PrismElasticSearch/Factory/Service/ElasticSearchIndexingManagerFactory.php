<?php

namespace PrismElasticSearch\Factory\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismElasticSearch\Service\Manager\ElasticSearchIndexingManager;

class ElasticSearchIndexingManagerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \PrismElasticSearch\Model\ElasticSearchLogsMapper $elasticSearchLogsMapper */
        $elasticSearchLogsMapper = $serviceLocator->get('ElasticSearchLogsMapper');
        $productsIndexer = $serviceLocator->get('ElasticSearchProductsIndexer');
        $contentIndexer = $serviceLocator->get('ElasticSearchWebsiteContentIndexer');
        $indexingService = new ElasticSearchIndexingManager($elasticSearchLogsMapper, $productsIndexer, $contentIndexer);
        return $indexingService;
    }
} 