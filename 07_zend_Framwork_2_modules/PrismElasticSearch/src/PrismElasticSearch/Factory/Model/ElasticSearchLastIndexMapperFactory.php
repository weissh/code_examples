<?php

namespace PrismElasticSearch\Factory\Model;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismElasticSearch\Model\ElasticSearchLastIndexMapper;

/**
 * Class ElasticSearchLastIndexMapperFactory
 * @package PrismElasticSearch\Factory\Model
 */
class ElasticSearchLastIndexMapperFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ElasticSearchLastIndexMapper
     */
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('db-pms');

        $elasticSearchLastIndexMapper = new ElasticSearchLastIndexMapper($dbAdapter);
        return $elasticSearchLastIndexMapper;
    }
} 