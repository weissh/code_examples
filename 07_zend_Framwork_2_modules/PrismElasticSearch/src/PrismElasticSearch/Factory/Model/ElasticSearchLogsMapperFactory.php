<?php

namespace PrismElasticSearch\Factory\Model;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismElasticSearch\Model\ElasticSearchLogsMapper;

/**
 * Class ElasticSearchLogsMapperFactory
 * @package PrismElasticSearch\Factory\Model
 */
class ElasticSearchLogsMapperFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ElasticSearchLogsMapper
     */
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('db-pms');

        $elasticSearchLogsMapper = new ElasticSearchLogsMapper($dbAdapter);
        return $elasticSearchLogsMapper;
    }
} 