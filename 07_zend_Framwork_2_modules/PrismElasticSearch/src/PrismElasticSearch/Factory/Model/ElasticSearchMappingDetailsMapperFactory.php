<?php

namespace PrismElasticSearch\Factory\Model;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismElasticSearch\Model\ElasticSearchMappingDetailsMapper;

/**
 * Class ElasticSearchMappingDetailsMapperFactory
 * @package PrismElasticSearch\Factory\Model
 */
class ElasticSearchMappingDetailsMapperFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ElasticSearchMappingDetailsMapper
     */
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = null;
        if ($serviceLocator->has('db-pms')) {
            $dbAdapter = $serviceLocator->get('db-pms');
        }

        $elasticSearchLogsMapper = new ElasticSearchMappingDetailsMapper($dbAdapter);
        return $elasticSearchLogsMapper;
    }
}
