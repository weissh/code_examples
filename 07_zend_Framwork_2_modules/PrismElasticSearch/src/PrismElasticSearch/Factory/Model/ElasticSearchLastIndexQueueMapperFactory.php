<?php

namespace PrismElasticSearch\Factory\Model;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismElasticSearch\Model\ElasticSearchIndexQueueMapper;

/**
 * Class ElasticSearchLastIndexQueueMapperFactory
 * @package PrismElasticSearch\Factory\Model
 */
class ElasticSearchLastIndexQueueMapperFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ElasticSearchIndexQueueMapper
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('db-pms');

        $elasticSearchIndexQueueMapper = new ElasticSearchIndexQueueMapper($dbAdapter);
        return $elasticSearchIndexQueueMapper;
    }
} 