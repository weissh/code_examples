<?php

namespace PrismElasticSearch\Factory\Model;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismElasticSearch\Model\ElasticSearchProductsMapper;

/**
 * Class ElasticSearchProductsMapperFactory
 * @package PrismElasticSearch\Factory\Model
 */
class ElasticSearchProductsMapperFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ElasticSearchProductsMapper
     */
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('db-pms');

        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig['siteConfig'] = $config->read('siteConfig');

        $categoriesMapper = $serviceLocator->get('PrismProductsManager\Model\CategoriesMapper');
        $productsMapper = $serviceLocator->get('PrismProductsManager\Model\ProductsMapper');

        $elasticSearchLogsMapper = new ElasticSearchProductsMapper($dbAdapter, $siteConfig, $categoriesMapper, $productsMapper);
        return $elasticSearchLogsMapper;
    }
} 