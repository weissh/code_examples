<?php

namespace PrismElasticSearch\Factory\Model;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismElasticSearch\Model\ElasticSearchWebsiteContentMapper;

/**
 * Class ElasticSearchWebsiteContentMapperFactory
 * @package PrismElasticSearch\Factory\Model
 */
class ElasticSearchWebsiteContentMapperFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return ElasticSearchWebsiteContentMapper
     */
    public function createService (ServiceLocatorInterface $serviceLocator)
    {
        $dbAdapter = $serviceLocator->get('db-cms');

        $config =  $serviceLocator->get('CommonStorage');
        $siteConfig['siteConfig'] = $config->read('siteConfig');

        $elasticSearchLogsMapper = new ElasticSearchWebsiteContentMapper($dbAdapter, $siteConfig);
        return $elasticSearchLogsMapper;
    }
} 