<?php

namespace PrismElasticSearch\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use PrismElasticSearch\Controller\SearchManagerController;
use Zend\Http\Client;

/**
 * Class SearchManagerControllerFactory
 * @package PrismElasticSearch\Factory\Controller
 */
class SearchManagerControllerFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return SearchManagerController
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $serviceLocator = $serviceLocator->getServiceLocator();

        /** @var \PrismElasticSearch\Model\ElasticSearchMappingDetailsMapper $elasticSearchMappingDetailsMapper */
        $elasticSearchMappingDetailsMapper = $serviceLocator->get('ElasticSearchMappingDetailsMapper');
        $config = $serviceLocator->get('Config');
        $client = new Client();
        return new SearchManagerController($elasticSearchMappingDetailsMapper, $config, $client);
    }
}
