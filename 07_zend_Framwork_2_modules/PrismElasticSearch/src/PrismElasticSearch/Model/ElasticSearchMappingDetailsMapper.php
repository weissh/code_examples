<?php
namespace PrismElasticSearch\Model;

use Zend\Cache\Storage\Adapter\Redis;
use Zend\Db\Adapter\Adapter;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\EventManager\EventManagerInterface;
use Zend\Db\Sql\Select;

/**
 * Class ElasticSearchMappingDetailsMapper
 * @package PrismElasticSearch\Model
 */
class ElasticSearchMappingDetailsMapper
{
    /**
     * @var string
     */
    protected $_tableName = 'ELASTICSEARCH_Mapping_Details';

    /**
     * @var
     */
    protected $_dbAdapter;

    /**
     * @var Sql
     */
    protected $_sql;

    /**
     * @param $dbAdapter
     * @param $redisStorageAdapter
     */
    public function __construct($dbAdapter)
    {
        $this->_dbAdapter = $dbAdapter;
        if ($dbAdapter !== null) {
            $this->_sql = new Sql($dbAdapter);
            $this->_sql->setTable($this->_tableName);
        }
    }

    /**
     * @param $elasticSearchType
     * @return array
     */
    public function getMappingDetailsArray($elasticSearchType) {
        $action = $this->_sql->select()
            ->where("elasticSearchType = '$elasticSearchType'")
            ->order("fieldParent ASC");
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();
        $finalProcessedArray = array('properties' => array());

        foreach ($results as $result) {
            if ($result['fieldParent'] == 0) {
                if (isset($result['autoCompleteEnabled']) && $result['autoCompleteEnabled'] == 1 && $result['indexable'] == 'yes') {
                    // we need to add the autocomplete analizer to this field
                    $finalProcessedArray['properties'][$result['fieldId']] = array(
                        'fieldName' => $result['fieldName'],
                        'type' => 'multi_field',
                        'fields' => array(
                            $result['fieldName'] => array(
                                'type' => 'string'
                            ),
                            'autocomplete' => array(
                                'analyzer' => 'autocomplete',
                                'type' => 'string'
                            )
                        )
                    );
                } else {
                    $finalProcessedArray['properties'][$result['fieldId']] = array(
                        'fieldName' => $result['fieldName'],
                        'type' => $result['fieldType'],
                    );
                    if ($result['indexable'] != 'yes') {
                        $finalProcessedArray['properties'][$result['fieldId']]['index'] = $result['indexable'];
                    }
                    if ($result['includeInParent'] == 1) {
                        $finalProcessedArray['properties'][$result['fieldId']]['include_in_parent'] = true;
                    }
                }
            } else {
                if (isset($finalProcessedArray['properties'][$result['fieldParent']])) {
                    $finalProcessedArray['properties'][$result['fieldParent']]['properties'][$result['fieldName']] = array(
                        'type' => $result['fieldType'],
                    );
                    if ($result['indexable'] != 'yes') {
                        $finalProcessedArray['properties'][$result['fieldParent']]['properties'][$result['fieldName']]['index'] = $result['indexable'];
                    }
                }
            }
        }

        // we have now the final array but with keys instead of names for level 0 elements
        foreach ($finalProcessedArray['properties'] as $fieldId => $value) {
            $fieldName = $value['fieldName'];
            unset($value['fieldName']);
            $finalProcessedArray['properties'][$fieldName] = $value;
            unset($finalProcessedArray['properties'][$fieldId]);
        }

        return $finalProcessedArray;
    }

    /**
     * @param $elasticSearchType
     * @return array
     */
    public function getMappingPrioritiesArray($elasticSearchType) {
        $action = $this->_sql->select()
            ->where("elasticSearchType = '$elasticSearchType'")
            ->order("fieldParent ASC");
        $statement = $this->_sql->prepareStatementForSqlObject($action);
//        echo $this->_sql->getSqlStringForSqlObject($action);
        $results = $statement->execute();

        $finalProcessedArray = array();
        foreach ($results as $result) {
            if ($result['fieldParent'] == 0) {
                if ($result['indexable'] != 'no') {
                    $finalProcessedArray[$result['fieldId']] = array(
                        'fieldName' => $result['fieldName'],
                        'searchPriority' => $result['searchPriority'],
                        'fieldType' => $result['fieldType']
                    );
                }
            } else {
                if (isset($finalProcessedArray[$result['fieldParent']])) {
                    if ($result['indexable'] != 'no') {
                        $finalProcessedArray[$result['fieldParent']]['children'][$result['fieldId']] = array(
                            'fieldName' => $result['fieldName'],
                            'searchPriority' => $result['searchPriority'],
                            'fieldType' => $result['fieldType']
                        );
                    }
                }
            }
        }

        $resultsArray = array();
        foreach ($finalProcessedArray as $parentFieldId => $value) {
            $parentFieldName = $value['fieldName'];
            if (!empty($value['children'])) {
                foreach ($value['children'] as $childFieldId => $childArray) {
                    $childArray['fieldName'] = $parentFieldName.'.'.$childArray['fieldName'];
                    $resultsArray[$childFieldId] = $childArray;
                }
            } else {
                $resultsArray[$parentFieldId] = $value;
            }
        }

        foreach ($resultsArray as $key => $result) {
            if ($result['fieldType'] != 'string') {
                unset ($resultsArray[$key]);
            }
        }

        return $resultsArray;
    }

    /**
     * @param $elasticSearchType
     * @param $fieldId
     * @param $inputValue
     * @return bool
     */
    public function updateFieldPriority($elasticSearchType, $fieldId, $inputValue)
    {
        $update = $this->_sql->update();
        $update->set(array(
            'searchPriority' => $inputValue,
        ));
        $update->where("fieldId = '$fieldId'");
        $update->where("elasticSearchType = '$elasticSearchType'");
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        return ($statement->execute()->getAffectedRows() > 0) ? true : false;
    }

    /**
     * @param $elasticSearchType
     * @return array|bool
     */
    public function getSearchPriorities($elasticSearchType)
    {
        $action = $this->_sql->select()
            ->where("elasticSearchType = '$elasticSearchType'")
            ->where("fieldParent = 0");
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();

        $parentsArray = array();
        foreach ($results as $result) {
            $parentsArray[$result['fieldId']] = $result;
        }

        $action = $this->_sql->select()
            ->where("elasticSearchType = '$elasticSearchType'")
            ->where("searchPriority > 1")
            ->order("searchPriority DESC");
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $results = $statement->execute();

        $finalPrioritiesArray = array();
        foreach ($results as $result) {
            if ($result['fieldParent'] != '0') {
                $fieldName = $parentsArray[$result['fieldParent']]['fieldName'].'.'.$result['fieldName'];
                $finalPrioritiesArray[$fieldName] = $result['searchPriority'];
            } else {
                $finalPrioritiesArray[$result['fieldName']] = $result['searchPriority'];
            }
        }

        return (count($finalPrioritiesArray) == 0) ? false : $finalPrioritiesArray;
    }
}
