<?php
namespace PrismElasticSearch\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\EventManager\EventManagerInterface;
use Zend\Db\Sql\Select;
use Zend\ServiceManager\ServiceLocatorInterface;

class ElasticSearchProductsMapper extends ElasticSearchAbstractMapper
{

    private $_websitesConfig;
    private $_currenciesConfig;
    private $_languages;
    private $_productsTable = 'PRODUCTS';
    private $_productsVariantsTable = 'PRODUCTS_R_ATTRIBUTES';
    private $_productsVariantsSkuRulesTable = 'PRODUCTS_R_ATTRIBUTES_R_SKU_Rules';
    private $_coloursGroupsTable = 'COLOURS_Groups';
    private $_coloursGroupsDefinitionsTable = 'COLOURS_Groups_Definitions';
    private $_coloursGroupsRelationTable = 'COLOURS_R_GROUPS';
    private $_coloursDefinitionsTable = 'COLOURS_Definitions';
    private $_sizesAccessoriesTable = 'SIZES_Accessories';
    private $_sizesShoesTable = 'SIZES_Shoes';
    private $_sizesClothesTable = 'SIZES_Clothes';
    private $_sizesShirtsTable = 'SIZES_Shirts';
    private $_sizesBeltsTable = 'SIZES_Belts';
    private $_sizesJacketsTable = 'SIZES_Jackets';
    private $_sizesTrousersTable = 'SIZES_Trousers';
    private $_sizesSwimShortsTable = 'SIZES_SwimShorts';
    private $_pricesTable = 'PRODUCTS_Prices';

    private $_categoryMapper;
    private $_productsMapper;

    public function __construct(Adapter $dbAdapter, $config, $categoryMapper, $productsMapper)
    {
        parent::__construct($dbAdapter);
        $this->_websitesConfig = $config['siteConfig']['websites'];
        $this->_currenciesConfig = $config['siteConfig']['currencies'];
        $this->_languages = $config['siteConfig']['languages']['site-languages'];
        /** @var \PrismProductsManager\Model\CategoriesMapper $categoryMapper */
        $this->_categoryMapper = $categoryMapper;
        /** @var \PrismProductsManager\Model\ProductsMapper $productsMapper */
        $this->_productsMapper = $productsMapper;
    }

    /**
     * Static function to retrieve products.
     *
     * @param $elasticSearchId
     * @return array
     * @throws \Exception
     */
    public function getIndexLayout($elasticSearchId)
    {
        $productIdAndVidArray = explode('_', $elasticSearchId);
        $productId = (int) $productIdAndVidArray[0];
        $variantId = (int) $productIdAndVidArray[1];

        try {
            $productsDetails = array();
            $categoriesDetails = array();
            $mainProductPrices = array();
            if ($variantId === 0) {
                $productImages = $this->_productsMapper->getImagesForProduct($productId, 0);
            } else {
                $productImages = $this->_productsMapper->getImagesForProduct($variantId, 1);
            }
            $relatedProductsIds = array();
//            $relatedProducts = $this->_productsMapper->_getProductsRelatedProducts($productId);
            $relatedProducts = $this->_productsMapper->getVariantsRelatedVariants($variantId);

            foreach ($relatedProducts as $relatedProduct) {
                if ($relatedProduct['productIdFrom'] == 'PRODUCTS_R_ATTRIBUTES') {
                    $mainProductId = $this->_productsMapper->getProductIdFromVariantId($relatedProduct['relatedProductId']);
                    if ($mainProductId !== false) {
                        $relatedProductsIds[] = $mainProductId.'_'.$relatedProduct['relatedProductId'];
                    }
                } else {
                    $relatedProductsIds[] = $relatedProduct['relatedProductId'].'_0';
                }
            }

            if ($variantId === 0) {
                $productIdForSpectrum = $productId;
                $table = $this->_productsTable;
            } else {
                $productIdForSpectrum = $variantId;
                $table = $this->_productsVariantsTable;
            }
            $spectrumId = $this->_productsMapper->getSpectrumId($productIdForSpectrum, $table);
            if (empty($spectrumId)) {
                // this product has not been syncronized to spectrum yet so return an error
                return array('success' => false, 'results' => "The product cannot be indexed since it hasn't been syncronized to SPECTRUM yet!");
            }
            foreach ($this->_languages as $languageId) {
                $productsDetailsForLanguage = $this->_productsMapper->getProductDetailsById($productId, $languageId, true);

                if (!empty($variantId)) {
                    $variantDetails = $this->_productsMapper->getVariantDetails($variantId, $languageId);
                }

                if ($productsDetailsForLanguage[$productId]['productTypeName'] == 'EVOUCHER') {
                    return array('success' => false, 'results' => 'EVOUCHER');
                }
                if ($variantId === 0) {
                    $categoryProductId = $productId;
                    $isVariant = 0;
                } else {
                    $categoryProductId = $variantId;
                    $isVariant = 1;
                }

                $categoriesArray = $this->_categoryMapper->getProductCategoriesRelationByProductId($categoryProductId, $languageId, $isVariant);
                $priorities = $this->_categoryMapper->getPrioritiesForElasticSearch($categoryProductId, $isVariant);
                if (isset($categoriesArray[$categoryProductId]) && count($categoriesArray[$categoryProductId]) > 0) {
                    foreach ($categoriesArray[$categoryProductId] as $key => $categoryArray) {
                        $categoryWebsites = $this->_categoryMapper->getCategoryWebsites($categoryArray['categoryId']);
                        foreach($categoryWebsites as $websiteId) {
                            $categoryTags = $this->_categoryMapper->getCategoryTags($categoryArray['categoryId'], $languageId);
                            $categoriesDetails[$websiteId][] = array(
                                'id' => !empty($categoryArray['categoryId']) ? (int) $categoryArray['categoryId'] : 0,
                                'name' => !empty($categoryArray['categoryName']) ? ucwords(strtolower($categoryArray['categoryName'])) : '',
                                'url' => !empty($categoryArray['urlName']) ? $categoryArray['urlName'] : '',
                                'filter_name' => (!empty($categoryArray['categoryId']) && !empty($categoryArray['categoryName']))
                                        ? ($categoryArray['categoryId'].'@'.ucwords(strtolower($categoryArray['categoryName']))) : '',
                                'tags' => !empty($categoryTags) ? $categoryTags: '',
                                'default_category' => !empty($categoryArray['isDefault']) ? 1 : 0,
                                'parent_category' => !empty($categoryArray['categoryParentId']) ? (int) $categoryArray['categoryParentId'] : 0,
                                'parent_categories_url' => $this->_categoryMapper->getParentCategories($categoryArray, 'url', $languageId),
                                'parent_categories_ids' => $this->_categoryMapper->getParentCategories($categoryArray, 'id', $languageId),
                                'parent_categories_names' => $this->_categoryMapper->getParentCategories($categoryArray, 'name', $languageId),
                                'isHeroProduct' => !empty($categoryArray['isHeroProduct']) ? (int) $categoryArray['isHeroProduct'] : 0,
                            );
                        }
                    }
                }

                if (isset($productsDetailsForLanguage[$productId]['status']) && $productsDetailsForLanguage[$productId]['status'] == 'ACTIVE') {
                    $productsDetailsForLanguage = $productsDetailsForLanguage[$productId];

                    foreach ($productsDetailsForLanguage as $key => $value) {
                        if ((strpos($key, 'nowPrice') === 0) || (strpos($key, 'wasPrice') === 0) || (strpos($key, 'packagingPrice') === 0) || (strpos($key, 'offerPrice') === 0)) {
                            $mainProductPrices[$key] = $value;
                        }
                    }
                    foreach ($productsDetailsForLanguage['websites'] as $websiteId) {
                        if (!isset($this->_websitesConfig['websites'][$websiteId])) {
                            continue;
                        }
                        $currentCurrencyId = $this->_websitesConfig['websites-currencies'][$websiteId];
                        $currentCurrency = ucfirst(strtolower($this->_currenciesConfig['currencies-list'][$currentCurrencyId]['currency-code']));

                        $variantDescription = !empty($variantDetails['longDescription']) ? $variantDetails['longDescription'] : '';
                        $variantShortDescription = !empty($variantDetails['shortDescription']) ? $variantDetails['shortDescription'] : '';
                        $variantProductName = !empty($variantDetails['productName']) ? $variantDetails['productName'] : '';
                        $variantShortProductName = !empty($variantDetails['shortProductName']) ? $variantDetails['shortProductName'] : '';
                        $variantMetaTitle = !empty($variantDetails['metaTitle']) ? $variantDetails['metaTitle'] : '';
                        $variantMetaDescription = !empty($variantDetails['metaDescription']) ? $variantDetails['metaDescription'] : '';
                        $variantMetaKeyword = !empty($variantDetails['metaKeyword']) ? $variantDetails['metaKeyword'] : '';

                        $productsDetails[$websiteId][$languageId] = array(
                            'productId' => $productId,
                            'variantId' => $variantId,
                            'spectrumId' => $spectrumId,
                            'product_id_colour' => $productId.'_',
                            'related_products' => !empty($relatedProductsIds) ? $relatedProductsIds : array(),
                            'channel' => $productsDetailsForLanguage['channels'],
                            'short_name' => !empty($variantShortProductName) ? $variantShortProductName : (!empty($productsDetailsForLanguage['shortProductName']) ? ucwords(strtolower($productsDetailsForLanguage['shortProductName'])) : ''),
                            'name' => !empty($variantProductName) ? $variantProductName : (!empty($productsDetailsForLanguage['productName']) ? ucwords(strtolower($productsDetailsForLanguage['productName'])) : ''),
                            'short_description' => !empty($variantShortDescription) ? $variantShortDescription :  (!empty($productsDetailsForLanguage['shortDescription']) ? $productsDetailsForLanguage['shortDescription'] : ''),
                            'description' => !empty($variantDescription) ? $variantDescription :  (!empty($productsDetailsForLanguage['longDescription']) ? $productsDetailsForLanguage['longDescription'] : ''),

                            'metaTitle' => !empty($variantMetaTitle) ? $variantMetaTitle : (!empty($productsDetailsForLanguage['metaTitle']) ? $productsDetailsForLanguage['metaTitle'] : ''),
                            'metaDescription' => !empty($variantMetaDescription) ? $variantMetaDescription :  (!empty($productsDetailsForLanguage['metaDescription']) ? $productsDetailsForLanguage['metaDescription'] : ''),
                            'metaKeywords' => !empty($variantMetaKeyword) ? $variantMetaKeyword :  (!empty($productsDetailsForLanguage['metaKeyword']) ? $productsDetailsForLanguage['metaKeyword'] : ''),
                            'care_information' => !empty($productsDetailsForLanguage['careInformationString']) ? $productsDetailsForLanguage['careInformationString'] : '',
                            'style' => !empty($productsDetailsForLanguage['style']) ? $productsDetailsForLanguage['style'] : '',
                            'sku' => !empty($productsDetailsForLanguage['sku']) ? $productsDetailsForLanguage['sku'] : '',
                            'qty' => !empty($productsDetailsForLanguage['quantity']) ? (int) $productsDetailsForLanguage['quantity'] : 0,
                            'mis_spells' => !empty($productsDetailsForLanguage['misSpells']) ? explode(',',$productsDetailsForLanguage['misSpells']) : '',
                            'tags' => !empty($productsDetailsForLanguage['tags']) ? explode(',',$productsDetailsForLanguage['tags']) : '',
                            'url' => !empty($productsDetailsForLanguage['urlName']) ? $productsDetailsForLanguage['urlName'] : '',
                            'images' => $productImages,
                            'categories' => isset($categoriesDetails[$websiteId]) ? $categoriesDetails[$websiteId] : array(),
                            'attributes' => !empty($productsDetailsForLanguage['attributes']) ? $productsDetailsForLanguage['attributes'] : array(),
                            'categories_priorities' => isset($priorities) ? $priorities : array()
                        );

                        $productsDetails[$websiteId][$languageId]['attributes']['price'] = !empty($mainProductPrices['nowPrice'.$currentCurrency]) ? (float) $mainProductPrices['nowPrice'.$currentCurrency] : 0;
                        $productsDetails[$websiteId][$languageId]['attributes']['was_price'] = !empty($mainProductPrices['wasPrice'.$currentCurrency]) ? (float) $mainProductPrices['wasPrice'.$currentCurrency] : 0;
                        $productsDetails[$websiteId][$languageId]['attributes']['packaging_price'] = !empty($mainProductPrices['wasPrice'.$currentCurrency]) ? (float) $mainProductPrices['packagingPrice'.$currentCurrency] : 0;
                        $productsDetails[$websiteId][$languageId]['attributes']['offer_price'] = !empty($mainProductPrices['offerPrice'.$currentCurrency]) ? (float) $mainProductPrices['offerPrice'.$currentCurrency] : 0;
                    }
                }
            }

            // at this point we have the common properties
            // we need now individually, based on the fact that is a variant or not to fetch the attributes
            if ($variantId === 0) {
                // we have a standalone product and we can return the mapping
                return array('success' => true, 'results' => $productsDetails);
            } else {
                // we have a variation of the product
                // we need to update the attributes array with the variant attributes
                // we still need to override the prices and add the colour and size
                $this->_sql->setTable(array('PRA' => $this->_productsVariantsTable));
                $select = $this->_sql->select();
                $select->columns(array('sku', 'quantity', 'status'));
                $select->join(
                    array(
                        'PSR' => $this->_productsVariantsSkuRulesTable
                    ),
                    'PRA.productAttributeId=PSR.productAttributeId',
                    array(
                        'skuRuleId',
                        'skuRuleTable'
                    )
                );
                $select->where('PRA.productAttributeId="'.$variantId.'"');
                $statement = $this->_sql->prepareStatementForSqlObject($select);
                $variantAttributes = $statement->execute();
                $variantSKU = '';
                $variantQty = 0;
                $finalColorArray = array();
                $sizesArray = array();
                $pricesArray = array();
                foreach ($variantAttributes as $attributeArray) {
                    if ($attributeArray['status'] != 'ACTIVE') {
                        // the variant is not active. We should return an empty array for this variant
                        return array('success' => true, 'results' => array());
                    }
                    $variantSKU = $attributeArray['sku'];
                    $variantQty = $attributeArray['quantity'];
                    switch($attributeArray['skuRuleTable']) {
                        case $this->_coloursGroupsTable:
                            $this->_sql->setTable(array('CG' => $this->_coloursGroupsTable));
                            $select = $this->_sql->select();
                            $select->columns(array(
                                'hex',
                                'groupCode',
                                'hasImage',
                                'imageId'
                            ));
                            $select->join(
                                array('CGD' => $this->_coloursGroupsDefinitionsTable),
                                'CG.colourGroupId = CGD.colourGroupId',
                                array(
                                    'colourGroupId',
                                    'languageId',
                                    'groupName' => 'name'
                                )
                            );
                            $select->join(
                                array(
                                    'CRG' => $this->_coloursGroupsRelationTable
                                ),
                                'CRG.colourGroupId=CGD.colourGroupId'
                            );
                            $select->join(
                                array(
                                    'CD' => $this->_coloursDefinitionsTable
                                ),
                                'CD.colourId=CRG.colourId AND CGD.languageId=CD.languageId',
                                array(
                                    'colourName'
                                )
                            );
                            $select->where('CGD.colourGroupId="'.$attributeArray['skuRuleId'].'"');
                            $statement = $this->_sql->prepareStatementForSqlObject($select);
                            $coloursForLanguagesResult = $statement->execute();

                            foreach ($coloursForLanguagesResult as $colourRow) {
                                if (!isset($finalColorArray[$colourRow['languageId']])) {
                                    $finalColorArray[$colourRow['languageId']] = array(
                                        'colour_group_code' => $colourRow['groupCode'],
                                        'colour' => $colourRow['groupName'],
                                        'colourGroups' => array(
                                            $colourRow['colourName']
                                        )
                                    );
                                    if ($colourRow['hasImage'] == 1) {
                                        $finalColorArray[$colourRow['languageId']]['colourImageId'] = $colourRow['imageId'];
                                        $finalColorArray[$colourRow['languageId']]['colourHex'] = '';
                                    } else {
                                        $finalColorArray[$colourRow['languageId']]['colourImageId'] = '';
                                        $finalColorArray[$colourRow['languageId']]['colourHex'] = $colourRow['hex'];
                                    }
                                } else {
                                    $finalColorArray[$colourRow['languageId']]['colourGroups'][] = $colourRow['colourName'];
                                }
                            }
                            break;
                        case $this->_sizesAccessoriesTable:
                        case $this->_sizesShoesTable:
                        case $this->_sizesClothesTable:
                        case $this->_sizesBeltsTable:
                        case $this->_sizesShirtsTable:
                        case $this->_sizesJacketsTable:
                        case $this->_sizesTrousersTable:
                        case $this->_sizesSwimShortsTable:
                            $this->_sql->setTable(array('S' => $attributeArray['skuRuleTable']));
                            $select = $this->_sql->select();
                            $select->columns(array(
                                'type',
                                'uk',
                                'us',
                                'eur',
                                'priority'
                            ));
                            $select->where('S.sizeId="'.$attributeArray['skuRuleId'].'"');
                            $select->limit(1);
                            $statement = $this->_sql->prepareStatementForSqlObject($select);
                            $sizesArray = $statement->execute()->next();
                            $sizesArray['table'] = $attributeArray['skuRuleTable'];
                            break;


                    }
                    $this->_sql->setTable(array('PP' => $this->_pricesTable));
                    $select = $this->_sql->select();
                    $select->columns(array(
                        'currencyId',
                        'nowPrice',
                        'offerPrice',
                        'wasPrice'
                    ));
                    $select->where(array(
                        'PP.productOrVariantId' => $variantId,
                        'PP.productIdFrom' => $this->_productsVariantsTable,
                    ));
                    $statement = $this->_sql->prepareStatementForSqlObject($select);
                    $pricesResult = $statement->execute();
                    foreach ($pricesResult as $priceRow) {
                        $pricesArray[$priceRow['currencyId']] = array(
                            'nowPrice' => $priceRow['nowPrice'],
                            'wasPrice' => $priceRow['wasPrice'],
                            'offerPrice' => $priceRow['offerPrice'],
                        );
                    }
                }
                // if the main products selection query failed to return any results
                if (empty($finalColorArray)) {
                    //COMMENTED OUT BECAUSE THESE ARE NOT MANDATORY ANYMORE
                    //return array('success' => false, 'results' => "Failed to fetch the colour for variant!");
                }
                if (empty($sizesArray)) {
                    //COMMENTED OUT BECAUSE THESE ARE NOT MANDATORY ANYMORE
                    //return array('success' => false, 'results' => "Failed to fetch the size for variant!");
                }
                if (empty($pricesArray)) {
                    //COMMENTED OUT BECAUSE THESE ARE NOT MANDATORY ANYMORE
                    //return array('success' => false, 'results' => "Failed to fetch the price for variant!");
                }
            }
            $variantAttributesArray = array();
            foreach ($productsDetails as $websiteId => $productsPerLanguageArray) {
                if (!isset($this->_websitesConfig['websites'][$websiteId])) {
                    continue;
                }
                foreach ($productsPerLanguageArray as $languageId => $productArray) {
                    if (!empty($finalColorArray[$languageId]['colour'])) {
                        $explodedName = explode(' ', $productsDetails[$websiteId][$languageId]['name']);
                        $name = '';
                        foreach ($explodedName as $key => $value) {
                            if ($key == 0) {
                                $name .= $value.' '.$finalColorArray[$languageId]['colour'];
                            } else {
                                $name .= ' '.$value;
                            }
                        }
                        $productsDetails[$websiteId][$languageId]['name'] = $name;
                    }
                    $variantDetails = $this->_productsMapper->getVariantDetails($variantId, $languageId);
                    $productsDetails[$websiteId][$languageId]['short_name'] = $variantDetails['shortProductName'];
                    $productsDetails[$websiteId][$languageId]['name'] = $variantDetails['productName'];
                    $productsDetails[$websiteId][$languageId]['short_description'] = $variantDetails['shortDescription'];
                    $productsDetails[$websiteId][$languageId]['description'] = $variantDetails['longDescription'];
                    $productsDetails[$websiteId][$languageId]['product_id_colour'] = $productId.'_'.(!empty($finalColorArray[$languageId]['colour']) ? $finalColorArray[$languageId]['colour'] : '');
                    $productsDetails[$websiteId][$languageId]['sku'] = $variantSKU;
                    $productsDetails[$websiteId][$languageId]['qty'] = $variantQty;
                    if (!empty($finalColorArray)) {
                        $productsDetails[$websiteId][$languageId]['attributes']['colour_group_code'] = $finalColorArray[$languageId]['colour_group_code'];
                        $productsDetails[$websiteId][$languageId]['attributes']['colour'] = $finalColorArray[$languageId]['colour'];
                        $productsDetails[$websiteId][$languageId]['attributes']['colour_groups'] = $finalColorArray[$languageId]['colourGroups'];
                        if (!empty($finalColorArray[$languageId]['colourHex'])) {
                            $productsDetails[$websiteId][$languageId]['attributes']['colour_hex'] = !empty($finalColorArray[$languageId]['colourHex']) ? $finalColorArray[$languageId]['colourHex'] : '';
                            $productsDetails[$websiteId][$languageId]['attributes']['colour_image_url'] = 0;
                        } else {
                            $productsDetails[$websiteId][$languageId]['attributes']['colour_hex'] = '';
                            $colourImageId = $this->_productsMapper->getColourImage(!empty($finalColorArray[$languageId]['colourImageId']) ? $finalColorArray[$languageId]['colourImageId'] : 0);
                            $productsDetails[$websiteId][$languageId]['attributes']['colour_image_url'] = $colourImageId;
                        }
                    }
                    if (!empty($sizesArray)) {
                        $productsDetails[$websiteId][$languageId]['attributes']['size_category'] = $sizesArray['type'];
                        $productsDetails[$websiteId][$languageId]['attributes']['size'] = $sizesArray[$this->_websitesConfig['websites-sizes'][$websiteId]];
                        $productsDetails[$websiteId][$languageId]['attributes']['size_table'] = $sizesArray['table'];
                        $productsDetails[$websiteId][$languageId]['attributes']['size_priority'] = $sizesArray['priority'];
                    }
                    if (!empty($pricesArray)) {
                        $productsDetails[$websiteId][$languageId]['attributes']['price'] = !empty($pricesArray[$this->_websitesConfig['websites-currencies'][$websiteId]]['nowPrice']) ? (float) $pricesArray[$this->_websitesConfig['websites-currencies'][$websiteId]]['nowPrice'] : 0;
                        $productsDetails[$websiteId][$languageId]['attributes']['was_price'] = !empty($pricesArray[$this->_websitesConfig['websites-currencies'][$websiteId]]['wasPrice']) ? (float) $pricesArray[$this->_websitesConfig['websites-currencies'][$websiteId]]['wasPrice'] : 0;
                        $productsDetails[$websiteId][$languageId]['attributes']['offer_price'] = !empty($pricesArray[$this->_websitesConfig['websites-currencies'][$websiteId]]['offerPrice']) ? (float) $pricesArray[$this->_websitesConfig['websites-currencies'][$websiteId]]['offerPrice'] : 0;
                    }
                    // override all the other marketing and core attributes
                    if (!isset($variantAttributesArray[$languageId])) {
                        $variantAttributes = $this->_productsMapper->getAllAttributesAssignedToProduct($variantId, 1, $languageId, true);
                        $variantAttributesArray[$languageId] = $variantAttributes;
                    } else {
                        $variantAttributes = $variantAttributesArray[$languageId];
                    }
                    foreach ($productArray['attributes'] as $attributeName => $attributeValue) {
                        if (isset($variantAttributes[$attributeName])) {
                            $productsDetails[$websiteId][$languageId]['attributes'][$attributeName] = $variantAttributes[$attributeName];
                            unset($variantAttributes[$attributeName]);
                        }
                    }
                    foreach ($variantAttributes as $variantAttributeName => $variantAttributeValue) {
                        $productsDetails[$websiteId][$languageId]['attributes'][$variantAttributeName] = $variantAttributeValue;
                    }
                    $a = 'a';
                }
            }
            return array('success' => true, 'results' => $productsDetails);
        } catch(\Exception $e) {
            return array('success' => false, 'results' => $e->getMessage());
        }
    }

    public function getIdsForBatch($from, $limit)
    {
        $voucherType = $this->_productsMapper->getVoucherTypeId();
        $this->_sql->setTable(array('P' => $this->_productsTable));
        $select = $this->_sql->select()
            ->columns(array('productId'))
            ->join(
                array('PA' => $this->_productsVariantsTable),
                'P.productId= PA.productId',
                array('productAttributeId'),
                Select::JOIN_LEFT
            )
            ->where(array('PA.productAttributeId' => null, 'P.status' => 'ACTIVE', 'P.productTypeId !=' . $voucherType['productTypeId']));
        $select1 = $this->_sql->select()
            ->columns(array('productId'))
            ->join(
                array('PA' => $this->_productsVariantsTable),
                'P.productId= PA.productId',
                array('productAttributeId')
            )
            ->where(array('PA.status' => 'ACTIVE'));

        if ($from === false) {
            $finalQuery = sprintf(
                'SELECT * FROM ((%s) UNION (%s))Z',
                $this->_sql->getSqlStringForSqlObject($select),
                $this->_sql->getSqlStringForSqlObject($select1)
            );
        } else {
            $finalQuery = sprintf(
                'SELECT * FROM ((%s) UNION (%s))Z LIMIT %s,%s',
                $this->_sql->getSqlStringForSqlObject($select),
                $this->_sql->getSqlStringForSqlObject($select1),
                $from,
                $limit
            );
        }

        $dbAdapter = $this->_dbAdapter;
        $results = $dbAdapter->query($finalQuery, $dbAdapter::QUERY_MODE_EXECUTE);

        $finalArray = array();
        foreach ($results as $result) {
            $variantId = !empty($result['productAttributeId']) ? $result['productAttributeId'] : 0;
            $finalArray[] = $result['productId'].'_'.$variantId;
        }
        return $finalArray;
    }
}