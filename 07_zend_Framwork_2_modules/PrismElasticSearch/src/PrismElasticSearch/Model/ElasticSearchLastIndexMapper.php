<?php
namespace PrismElasticSearch\Model;

use PrismElasticSearch\Enum\ElasticSearchTypes;
use Zend\Db\Adapter\Adapter;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\EventManager\EventManagerInterface;
use Zend\Db\Sql\Select;

class ElasticSearchLastIndexMapper
{
    protected $_tableName = 'ELASTICSEARCH_LastIndex';

    protected $_dbAdapter;

    protected $_sql;

    public function __construct(Adapter $dbAdapter)
    {
        $this->_dbAdapter = $dbAdapter;
        $this->_sql = new Sql($dbAdapter);
        $this->_sql->setTable($this->_tableName);
    }

    public function getLastTimestamp($elasticSearchType = ElasticSearchTypes::PRODUCTS)
    {
        $action = $this->_sql->select()->where("indexType='$elasticSearchType'")->limit(1);

        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        if ($result->count() === 1) {
            return $result->next()['lastIndexTimestamp'];
        } else {
            return false;
        }
    }

    public function setLastTimestamp($timestamp, $elasticSearchType = ElasticSearchTypes::PRODUCTS)
    {
        if ($this->getLastTimestamp($elasticSearchType) === false) {
            $data = array(
                'indexType' => (int) $elasticSearchType,
                'lastIndexTimestamp' => $timestamp
            );
            try {
                $action = $this->_sql->insert();
                $action->values($data);
                $statement = $this->_sql->prepareStatementForSqlObject($action);
                $statement->execute();
            } catch (\Exception $e) {
                throw new \Exception($e);
                // log any error
            }
        } else {
            $update = $this->_sql->update();
            $update->set(array(
                'lastIndexTimestamp' => $timestamp
            ));
            $update->where(array(
                'indexType' => $elasticSearchType
            ));

            try {
                $statement = $this->_sql->prepareStatementForSqlObject($update);
                $statement->execute();
            } catch (\Exception $e) {
                throw new \Exception($e);
            }
        }
    }
}