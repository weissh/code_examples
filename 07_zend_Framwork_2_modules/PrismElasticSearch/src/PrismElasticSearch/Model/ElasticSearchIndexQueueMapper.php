<?php
namespace PrismElasticSearch\Model;

use Zend\Db\Adapter\Adapter;
use PrismElasticSearch\Enum\ElasticSearchTypes;
use PrismElasticSearch\Enum\ElasticSearchQueueReasons;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\EventManager\EventManagerInterface;
use Zend\Db\Sql\Select;

class ElasticSearchIndexQueueMapper
{
    protected $_tableName = 'ELASTICSEARCH_IndexQueue';

    protected $_dbAdapter;

    protected $_sql;

    public function __construct(Adapter $dbAdapter)
    {
        $this->_dbAdapter = $dbAdapter;
        $this->_sql = new Sql($dbAdapter);
        $this->_sql->setTable($this->_tableName);
    }

    public function getProductsInQueue($elasticSearchType = ElasticSearchTypes::PRODUCTS)
    {
        $action = $this->_sql->select()->where("indexType='$elasticSearchType'");
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        $productIdsArray = array();
        foreach($result as $productInQueue) {
            $productIdsArray[] = $productInQueue['elasticSearchId'];
        }
        return $productIdsArray;
    }

    public function addProductsInQueue($elasticSearchIds, $elasticSearchType = ElasticSearchTypes::PRODUCTS, $elasticSearchQueueReason = ElasticSearchQueueReasons::REINDEX_LOCK)
    {
        foreach($elasticSearchIds as $productIdAndVid) {
            $this->addProductInQueue($productIdAndVid, $elasticSearchType, $elasticSearchQueueReason);
        }
    }

    public function addProductInQueue($elasticSearchId, $elasticSearchType = ElasticSearchTypes::PRODUCTS, $elasticSearchQueueReason = ElasticSearchQueueReasons::REINDEX_LOCK)
    {
        $data = array(
            'indexType' => (int) $elasticSearchType,
            'reason' => $elasticSearchQueueReason,
            'elasticSearchId' => $elasticSearchId,
            'createdAt' => date('Y-m-d H:i:s')
        );
        try {
            $action = $this->_sql->insert();
            $action->values($data);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        } catch (\Exception $e) {
            throw new \Exception($e);
            // log any error
        }
    }
}
