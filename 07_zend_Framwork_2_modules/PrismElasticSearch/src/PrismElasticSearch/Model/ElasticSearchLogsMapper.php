<?php
namespace PrismElasticSearch\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\EventManager\EventManagerInterface;
use Zend\Db\Sql\Select;

class ElasticSearchLogsMapper
{
    protected $_tableName = 'ELASTICSEARCH_Logs';

    protected $_dbAdapter;

    protected $_sql;

    public function __construct(Adapter $dbAdapter)
    {
        $this->_dbAdapter = $dbAdapter;
        $this->_sql = new Sql($dbAdapter);
        $this->_sql->setTable($this->_tableName);
    }

    public function getLastProcessId($indexType)
    {
        $action = $this->_sql->select()
            ->where("indexType = '$indexType'")
            ->order("processId DESC")
            ->limit(1);
        $statement = $this->_sql->prepareStatementForSqlObject($action);
        $result = $statement->execute();
        return $result->next()['processId'];
    }

    public function createNewLog($indexType, $logText, $processId = false, $isError = false)
    {
        if ($isError) {
            //TODO: send email to the it team
        }

        if($processId === false) {
            // generate new process id
            $processId = $this->getLastProcessId($indexType) + 1;
        }

        $data = array(
            'processId' => $processId,
            'indexType' => $indexType,
            'lastStatusUpdate' => $logText,
            'createdAt' => date('Y-m-d H:i:s')
        );
        try {
            $action = $this->_sql->insert();
            $action->values($data);
            $statement = $this->_sql->prepareStatementForSqlObject($action);
            $statement->execute();
        } catch (\Exception $e) {
            throw new \Exception($e);
            // log any error
        }
    }
}