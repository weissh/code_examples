<?php
namespace PrismElasticSearch\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\EventManager\EventManagerInterface;
use Zend\Db\Sql\Select;
use Zend\ServiceManager\ServiceLocatorInterface;

abstract class ElasticSearchAbstractMapper
{
    protected $_dbAdapter;
    protected $_sql;
    protected $_serviceLocator;

    public function __construct(Adapter $dbAdapter)
    {
        $this->_dbAdapter = $dbAdapter;
        $this->_sql = new Sql($dbAdapter);
    }

    /**
     * Static function to retrieve mapped data.
     * The returned array must be:
     *      array(
     *          websiteId1 => array(
     *              languageId1 => array(##entity data for website1 and language1##),
     *              languageId2 => array(##entity data for website1 and language2##),
     *              ...
     *          ),
     *          websiteId2 => array(
     *              languageId1 => array(##entity data for website2 and language1##),
     *              languageId2 => array(##entity data for website2 and language2##),
     *              ...
     *          ),
     *          ...
     *      )
     *
     * @param $elasticSearchId
     * @return array
     */
    abstract public function getIndexLayout($elasticSearchId);

    abstract public function getIdsForBatch($from, $limit);
}