<?php
namespace PrismElasticSearch\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Db\Sql\Sql;
use Zend\EventManager\EventManagerInterface;
use Zend\Db\Sql\Select;
use Zend\ServiceManager\ServiceLocatorInterface;

class ElasticSearchWebsiteContentMapper extends ElasticSearchAbstractMapper
{
    private $_websitesConfig;
    private $_languages;
    private $_contentTable = 'CONTENT';
    private $_contentDefinitionsTable = 'CONTENT_Definitions';
    private $_contentBlocksTable = 'CONTENT_Blocks';

    public function __construct(Adapter $dbAdapter, $config)
    {
        parent::__construct($dbAdapter);
        $this->_websitesConfig = $config['siteConfig']['websites'];
        $this->_languages = $config['siteConfig']['languages']['site-languages'];
    }

    /**
     * Static function to retrieve products.
     *
     * @param $elasticSearchId
     * @return array
     */
    public function getIndexLayout($elasticSearchId)
    {
        $pageId = (int) $elasticSearchId;

        $this->_sql->setTable(array('C' => $this->_contentTable));
        $select = $this->_sql->select()
            ->columns(array('websiteId'))
            ->join(
                array('CD' => $this->_contentDefinitionsTable),
                'C.contentId = CD.contentId',
                array(
                    'languageId',
                    'contentName',
                    'urlName',
                    'metaTitle',
                    'metaDescription',
                    'metaKeyword'
                )
            )
            ->join(
                array('CB' => $this->_contentBlocksTable),
                'CD.contentDefinitionsId = CB.contentDefinitionsId',
                array(
                    'contentBlockElasticSearch'
                )
            )
            ->where(array(
                'C.status' => 'ACTIVE',
                'CD.status' => 'ACTIVE',
                'CB.status' => 'ACTIVE',
                'C.contentId' => $pageId,
            ));
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $contentResult = $statement->execute();

        $toBeIndexedArray = array();
        $websiteId = 0;
        foreach ($contentResult as $contentRow) {
            $websiteId = $contentRow['websiteId'];
            $languageId = $contentRow['languageId'];
            if (!isset($toBeIndexedArray[$languageId])) {
                $toBeIndexedArray[$languageId] = array(
                    'page_id' => $elasticSearchId,
                    'page_url' => $contentRow['urlName'],
                    'page_title' => $contentRow['contentName'],
                    'page_content' => !empty($contentRow['contentBlockElasticSearch']) ? array($contentRow['contentBlockElasticSearch']) : array(),
                    'meta_tags' => array(
                        'meta_keywords' => $contentRow['metaKeyword'],
                        'meta_title' => $contentRow['metaTitle'],
                        'meta_description' => $contentRow['metaDescription']
                    )
                );
            } else {
                if(!empty($contentRow['contentBlockElasticSearch'])) {
                    $toBeIndexedArray[$languageId]['page_content'][] = $contentRow['contentBlockElasticSearch'];
                }
            }
        }
        if ($websiteId == 0) {
            return array('success' => false, 'results' => 'Failed to retrieve any results!');
        } else if(empty($toBeIndexedArray)) {
            return array('success' => false, 'results' => 'Failed to retrieve any results!');
        } else {
            return array('success' => true, 'results' => array($websiteId => $toBeIndexedArray));
        }
    }

    public function getIdsForBatch($from, $limit)
    {
        $this->_sql->setTable(array('C' => $this->_contentTable));
        $select = $this->_sql->select()
            ->join(
                array('CD' => $this->_contentDefinitionsTable),
                'C.contentId = CD.contentId'
            )
            ->where(array('CD.status' => 'ACTIVE'))
            ->group('C.contentId');

        if ($from !== false) {
            $select->limit($limit)
                ->offset($from);
        }

        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $finalArray = array();
        foreach ($results as $result) {
            $finalArray[] = $result['contentId'];
        }
        return $finalArray;
    }
}