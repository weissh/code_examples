<?php
return array(
    'controllers' => array(
        'factories' => array(
            'PrismElasticSearch\Controller\SearchManager' => 'PrismElasticSearch\Factory\Controller\SearchManagerControllerFactory'
        ),
        'invokables' => array(
            'PrismElasticSearch\Controller\Index' => 'PrismElasticSearch\Controller\IndexController',
            'PrismElasticSearch\Controller\ApiElasticSearchProducts' => 'PrismElasticSearch\Controller\ApiElasticSearchProductsController',
            'PrismElasticSearch\Controller\ApiElasticSearchPosts' => 'PrismElasticSearch\Controller\ApiElasticSearchPostsController',
            'PrismElasticSearch\Controller\ApiElasticSearchContent' => 'PrismElasticSearch\Controller\ApiElasticSearchContentController',
            'PrismElasticSearch\Controller\ApiElasticSearchType' => 'PrismElasticSearch\Controller\ApiElasticSearchTypeController'
        )
    ),
    
    'router' => array(
        'routes' => array(
            // api calls to elestic search
            'api-elastic-search-products' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/api/search/products[/][/:id]',
                    'constraints' => array(
                        'id' =>  '\w+' //'[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'PrismElasticSearch\Controller\ApiElasticSearchProducts',
                    ),
                ),
            ),
            // api calls to elestic search
            'api-elastic-search-posts' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/api/search/posts[/][/:id]',
                    'constraints' => array(
                        'id' =>  '\w+' //'[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'PrismElasticSearch\Controller\ApiElasticSearchPosts',
                    ),
                ),
            ),
            // api calls to elestic search
            'api-elastic-search-content' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/api/search/pages[/][/:id]',
                    'constraints' => array(
                        'id' =>  '\w+' //'[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'PrismElasticSearch\Controller\ApiElasticSearchContent',
                    ),
                ),
            ),
            // api calls to elestic search
            'api-elastic-search-priorities' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/api/elastic-search-priorities[/:elasticSearchType][/]',
                    'constraints' => array(
                        'elasticSearchType' =>  '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'PrismElasticSearch\Controller\ApiElasticSearchType',
                    ),
                ),
            ),
            'elasticSearchIndex' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/elastic-search',
                    'defaults' => array(
                        'controller' => 'PrismElasticSearch\Controller\Index',
                        'action' => 'index'
                    ),
                ),
            ),

            'elasticSearchReIndex' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/elastic-search/reindex[/:entityType]',
                    'defaults' => array(
                        'controller' => 'PrismElasticSearch\Controller\Index',
                        'action' => 'reindex'
                    ),
                ),
            ),

            'search-manager' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/search-manager[/:action]',
                    'constraints' => array(
                        'action' =>  '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'PrismElasticSearch\Controller\SearchManager'
                    ),
                ),
            ),
        ),
    ),
    
    'service_manager' => array(
        'factories' => array(
            'ElasticSearchIndexingManager' => 'PrismElasticSearch\Factory\Service\ElasticSearchIndexingManagerFactory',
            'ElasticSearchProductsIndexer' => 'PrismElasticSearch\Factory\Service\Index\ElasticSearchProductsIndexerFactory',
            //'ElasticSearchBlogPostsIndexer' => 'PrismElasticSearch\Factory\Service\Index\ElasticSearchBlogPostsIndexerFactory',
            'ElasticSearchWebsiteContentIndexer' => 'PrismElasticSearch\Factory\Service\Index\ElasticSearchWebsiteContentIndexerFactory',
            'ElasticSearchLastIndexMapper' => 'PrismElasticSearch\Factory\Model\ElasticSearchLastIndexMapperFactory',
            'ElasticSearchIndexQueueMapper' => 'PrismElasticSearch\Factory\Model\ElasticSearchLastIndexQueueMapperFactory',
            'ElasticSearchLogsMapper' => 'PrismElasticSearch\Factory\Model\ElasticSearchLogsMapperFactory',
            'ElasticSearchProductsMapper' => 'PrismElasticSearch\Factory\Model\ElasticSearchProductsMapperFactory',
            //'ElasticSearchBlogPostsMapper' => 'PrismElasticSearch\Factory\Model\ElasticSearchBlogPostsMapperFactory',
            'ElasticSearchWebsiteContentMapper' => 'PrismElasticSearch\Factory\Model\ElasticSearchWebsiteContentMapperFactory',
            'ElasticSearchSearchingManager' => 'PrismElasticSearch\Factory\Service\ElasticSearchSearchingManagerFactory',
            'ElasticSearchProductsSearching' => 'PrismElasticSearch\Factory\Service\Search\ElasticSearchProductsSearchingFactory',
            'ElasticSearchBlogPostsSearching' => 'PrismElasticSearch\Factory\Service\Search\ElasticSearchBlogPostsSearchingFactory',
            'ElasticSearchWebsiteContentSearching' => 'PrismElasticSearch\Factory\Service\Search\ElasticSearchWebsiteContentSearchingFactory',
            'ElasticSearchMappingDetailsMapper' => 'PrismElasticSearch\Factory\Model\ElasticSearchMappingDetailsMapperFactory',
        ),
    ),

    /*'console' => array(
        'router' => array(
            'routes' => array(
                'reindex-process' => array(
                    'options' => array(
                        'route' => 'elasticsearch-reindex [--verbose|-v] <entityType> [<batchSize>]',
                        'defaults' => array(
                            '__NAMESPACE__' => 'PrismElasticSearch\Controller',
                            'controller' => 'index',
                            'action' => 'reindex'
                        ),
                    ),
                ),
            )
        )
    ),*/

    'view_manager' => array(
        'template_path_stack' => array(
            'PrismElasticSearch' => __DIR__ . '/../view'
        )
    )
);