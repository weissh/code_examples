<?php

namespace PrismElasticSearch;

use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\LocatorRegisteredInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;
use Zend\Console\Adapter\AdapterInterface as Console;

class Module  implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    LocatorRegisteredInterface//,
    //ConsoleUsageProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * {@inheritdoc}
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /*
    public function getConsoleUsage(Console $console)
    {
        return array(
            // Describe available commands
            'elasticsearch-reindex [--verbose|-v] <entityType> [<batchSize>]'    => 'Reindex the products across all indices',
            // Describe expected parameters
            array('batchNumber', 'Numbers of products/batch'),
            array('entityType', 'Entity Type'),
            array('--verbose|-v', '(optional) turn on verbose mode')
        );
    }
    */
}
