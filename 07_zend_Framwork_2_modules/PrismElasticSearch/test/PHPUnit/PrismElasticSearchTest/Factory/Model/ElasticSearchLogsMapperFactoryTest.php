<?php

namespace PrismElasticSearchTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Factory\Model\ElasticSearchLogsMapperFactory;
use Zend\Db\Adapter\Platform\Mysql;

/**
 * @author haniw
 */
class ElasticSearchLogsMapperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ElasticSearchLogsMapperFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ElasticSearchLogsMapperFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $dbAdapter = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                             ->disableOriginalConstructor()
                             ->getMock();
        $dbAdapter->expects($this->once())
                               ->method('getPlatform')
                               ->willReturn(new Mysql());
        $this->serviceLocator->expects($this->at(0))
                           ->method('get')
                           ->with('db-pms')
                        ->willReturn($dbAdapter);
        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismElasticSearch\Model\ElasticSearchLogsMapper', $result);
    }
}
