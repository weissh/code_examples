<?php

namespace PrismElasticSearchTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Factory\Model\ElasticSearchLastIndexMapperFactory;
use Zend\Db\Adapter\Platform\Mysql;

/**
 * @author haniw
 */
class ElasticSearchLastIndexMapperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ElasticSearchLastIndexMapperFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ElasticSearchLastIndexMapperFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $dbAdapter = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                             ->disableOriginalConstructor()
                             ->getMock();
        $dbAdapter->expects($this->once())
                               ->method('getPlatform')
                               ->willReturn(new Mysql());
        $this->serviceLocator->expects($this->at(0))
                           ->method('get')
                           ->with('db-pms')
                        ->willReturn($dbAdapter);
        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismElasticSearch\Model\ElasticSearchLastIndexMapper', $result);
    }
}
