<?php

namespace PrismElasticSearchTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Factory\Model\ElasticSearchWebsiteContentMapperFactory;
use Zend\Db\Adapter\Platform\Mysql;

/**
 * @author haniw
 */
class ElasticSearchWebsiteContentMapperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ElasticSearchWebsiteContentMapperFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ElasticSearchWebsiteContentMapperFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $dbAdapter = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                             ->disableOriginalConstructor()
                             ->getMock();
        $dbAdapter->expects($this->once())
                               ->method('getPlatform')
                               ->willReturn(new Mysql());
        $this->serviceLocator->expects($this->at(0))
                           ->method('get')
                           ->with('db-cms')
                        ->willReturn($dbAdapter);

        $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
                              ->disableOriginalConstructor()
                              ->getMock();
        $commonStorage->expects($this->once())
                      ->method('read')
                      ->with('siteConfig')
                      ->willReturn([
            'websites' => 1,
            'currencies' => 1,
            'languages' => [
                'site-languages' => 1,
            ],
        ]);
        $this->serviceLocator->expects($this->at(1))
                         ->method('get')
                         ->with('CommonStorage')
                         ->willReturn($commonStorage);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismElasticSearch\Model\ElasticSearchWebsiteContentMapper', $result);
    }
}
