<?php

namespace PrismElasticSearchTest\Factory\Model;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Factory\Model\ElasticSearchMappingDetailsMapperFactory;
use Zend\Db\Adapter\Platform\Mysql;

/**
 * @author haniw
 */
class ElasticSearchMappingDetailsMapperFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ElasticSearchMappingDetailsMapperFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ElasticSearchMappingDetailsMapperFactory();
        $this->factory = $factory;
    }

    public function provideCreateService()
    {
        return [
            [true],
            [false],
        ];
    }

    /**
     * @dataProvider provideCreateService
     */
    public function testCreateService($hasDb)
    {
        if ($hasDb) {
            $this->serviceLocator->expects($this->once())
                           ->method('has')
                           ->with('db-pms')
                           ->willReturn(true);

            $dbAdapter = $this->getMockBuilder('Zend\Db\Adapter\Adapter')
                                 ->disableOriginalConstructor()
                                 ->getMock();
            $dbAdapter->expects($this->once())
                                   ->method('getPlatform')
                                   ->willReturn(new Mysql());
            $this->serviceLocator->expects($this->once())
                           ->method('get')
                           ->with('db-pms')
                           ->willReturn($dbAdapter);
        }

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismElasticSearch\Model\ElasticSearchMappingDetailsMapper', $result);
    }
}
