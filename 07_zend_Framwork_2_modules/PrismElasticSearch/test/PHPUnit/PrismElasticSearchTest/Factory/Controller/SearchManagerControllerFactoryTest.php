<?php

namespace PrismElasticSearchTest\Factory\Controller\Command;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Factory\Controller\SearchManagerControllerFactory;

/**
 * @author haniw
 */
class SearchManagerControllerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var SearchManagerControllerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    /** @var \Zend\Mvc\Controller\ControllerManager */
    protected $controllerManager;

    public function setUp()
    {
        $this->controllerManager = $this->getMockBuilder('Zend\Mvc\Controller\ControllerManager')
                                        ->disableOriginalConstructor()
                                        ->getMock();

        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new SearchManagerControllerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $elasticSearchMappingDetailsMapper =  $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchMappingDetailsMapper')
                         ->disableOriginalConstructor()
                         ->getMock();
        $this->serviceLocator->expects($this->at(0))
                             ->method('get')
                             ->with('ElasticSearchMappingDetailsMapper')
                             ->willReturn($elasticSearchMappingDetailsMapper);

        $this->serviceLocator->expects($this->at(1))
                          ->method('get')
                          ->with('Config')
                          ->willReturn([]);
        $this->controllerManager->expects($this->exactly(1))
                                ->method('getServiceLocator')
                                ->willReturn($this->serviceLocator);

        $result = $this->factory->createService($this->controllerManager);
        $this->assertInstanceOf('PrismElasticSearch\Controller\SearchManagerController', $result);
    }
}
