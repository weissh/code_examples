<?php

namespace PrismElasticSearchTest\Factory\Service\Search;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Factory\Service\Search\ElasticSearchProductsSearchingFactory;

/**
 * @author haniw
 */
class ElasticSearchProductsSearchingFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ElasticSearchProductsSearchingFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    protected function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ElasticSearchProductsSearchingFactory();
        $this->factory = $factory;
    }

    public function provideCreateService()
    {
        return [
            [true, true],
            [true, false],
            [false, true],
            [false, false],
        ];
    }

    /**
     * @dataProvider provideCreateService
     */
    public function testCreateService($hasCommonStorage, $hasPrioritiesService)
    {
        $config = [
            'elasticSearchConfig' => [
                'hosts'         => ['http://10.128.140.31:9200'],
                'logging'       => false,
                'logPath'       => '/tmp/elasticsearch.log',
                'logLevel'      => \Psr\Log\LogLevel::DEBUG,
            ],
            'elasticSearchProductsIndexVariables' => [
                'indexPrefix'        => 'products_',
                'mappingType'        => 'product',
                'lockFile'           => '/tmp/elasticsearch_products.lck'
            ],
            'siteConfig' => [
                'languages' => [
                    'site-languages' => [
                        1,
                        2,
                    ]
                ],
                'websites' => [
                    'websites' => [
                        'prism website',
                        'oliversweeney website'
                    ],
                ],
            ],
        ];
        $this->serviceLocator->expects($this->at(0))
                             ->method('get')
                             ->with('Config')
                             ->willReturn($config);
        $this->serviceLocator->expects($this->at(1))
                             ->method('has')
                             ->with('CommonStorage')
                             ->willReturn($hasCommonStorage);
        $next = 2;
        if ($hasCommonStorage) {
            $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
                                  ->disableOriginalConstructor()
                                  ->getMock();

            $commonStorage->expects($this->once())
                          ->method('read')
                          ->with('siteConfig')
                          ->willReturn($config['siteConfig']);
            $this->serviceLocator->expects($this->at($next))
                               ->method('get')
                               ->with('CommonStorage')
                               ->willReturn($commonStorage);
            $next = 3;
        }

        $this->serviceLocator->expects($this->at($next))
                             ->method('has')
                             ->with('ElasticSearchPrioritiesService')
                             ->willReturn($hasPrioritiesService);
        $next += 1;
        if ($hasPrioritiesService) {
            $priorityService = $this->getMockBuilder('PrismThemeManager\Service\ElasticSearchPrioritiesService')
                                    ->disableOriginalConstructor()
                                    ->getMock();
            $priorityService->expects($this->once())
                            ->method('getSearchPrioritiesApi')
                            ->with(1)
                            ->willReturn([1, 2]);
            $this->serviceLocator->expects($this->at($next))
                                 ->method('get')
                                 ->with('ElasticSearchPrioritiesService')
                                 ->willReturn($priorityService);
        } else {
            $elasticSearchMappingDetailsMapper = $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchMappingDetailsMapper')
                                    ->disableOriginalConstructor()
                                    ->getMock();
            $elasticSearchMappingDetailsMapper->expects($this->once())
                            ->method('getSearchPriorities')
                            ->with(1)
                            ->willReturn([1, 2]);
            $this->serviceLocator->expects($this->at($next))
                                 ->method('get')
                                 ->with('ElasticSearchMappingDetailsMapper')
                                 ->willReturn($elasticSearchMappingDetailsMapper);
        }

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismElasticSearch\Service\ElasticSearchProductsSearching', $result);
    }
}
