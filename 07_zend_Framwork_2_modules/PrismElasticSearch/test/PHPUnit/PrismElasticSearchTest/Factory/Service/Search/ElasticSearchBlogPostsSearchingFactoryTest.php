<?php

namespace PrismElasticSearchTest\Factory\Service\Search;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Factory\Service\Search\ElasticSearchBlogPostsSearchingFactory;

/**
 * @author haniw
 */
class ElasticSearchBlogPostsSearchingFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ElasticSearchBlogPostsSearchingFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    protected function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ElasticSearchBlogPostsSearchingFactory();
        $this->factory = $factory;
    }

    public function provideCreateService()
    {
        return [
            [true],
            [false],
        ];
    }

    /**
     * @dataProvider provideCreateService
     */
    public function testCreateService($hasCommonStorage)
    {
        $config = [
            'elasticSearchConfig' => [
                'hosts'         => ['http://10.128.140.31:9200'],
                'logging'       => false,
                'logPath'       => '/tmp/elasticsearch.log',
                'logLevel'      => \Psr\Log\LogLevel::DEBUG,
            ],
            'elasticSearchBlogPostsIndexVariables' => [
                'indexPrefix'        => 'blog_posts',
                'mappingType'        => 'post',
                'lockFile'           => '/tmp/elasticsearch_blog_posts.lck',
            ],
            'siteConfig' => [
                'languages' => [
                    'site-languages' => [
                        1,
                        2,
                    ]
                ],
            ],
        ];
        $this->serviceLocator->expects($this->at(0))
                             ->method('get')
                             ->with('Config')
                             ->willReturn($config);
        $this->serviceLocator->expects($this->at(1))
                             ->method('has')
                             ->with('CommonStorage')
                             ->willReturn($hasCommonStorage);

        if ($hasCommonStorage) {
            $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
                                  ->disableOriginalConstructor()
                                  ->getMock();

            $commonStorage->expects($this->once())
                          ->method('read')
                          ->with('siteConfig')
                          ->willReturn($config['siteConfig']);
            $this->serviceLocator->expects($this->at(2))
                               ->method('get')
                               ->with('CommonStorage')
                               ->willReturn($commonStorage);
        }

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismElasticSearch\Service\ElasticSearchBlogPostsSearching', $result);
    }
}
