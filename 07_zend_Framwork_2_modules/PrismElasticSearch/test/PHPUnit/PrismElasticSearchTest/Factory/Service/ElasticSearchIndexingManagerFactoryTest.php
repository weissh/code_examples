<?php

namespace PrismElasticSearchTest\Factory\Service;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Factory\Service\ElasticSearchIndexingManagerFactory;

/**
 * @author haniw
 */
class ElasticSearchIndexingManagerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ElasticSearchIndexingManagerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ElasticSearchIndexingManagerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $elasticSearchLogsMapper = $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchLogsMapper')
                                        ->disableOriginalConstructor()
                                        ->getMock();
        $this->serviceLocator->expects($this->at(0))
                         ->method('get')
                         ->with('ElasticSearchLogsMapper')
                         ->willReturn($elasticSearchLogsMapper);

        $elasticProductIndexer  = $this->getMockBuilder('PrismElasticSearch\Service\Index\ElasticSearchProductsIndexer')
                                       ->disableOriginalConstructor()
                                       ->getMock();
        $this->serviceLocator->expects($this->at(1))
                        ->method('get')
                        ->with('ElasticSearchProductsIndexer')
                        ->willReturn($elasticProductIndexer);

        $elasticSearchWebsiteContentIndexer  = $this->getMockBuilder('PrismElasticSearch\Service\Index\ElasticSearchWebsiteContentIndexer')
                                       ->disableOriginalConstructor()
                                       ->getMock();
        $this->serviceLocator->expects($this->at(2))
                        ->method('get')
                        ->with('ElasticSearchWebsiteContentIndexer')
                        ->willReturn($elasticSearchWebsiteContentIndexer);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismElasticSearch\Service\Manager\ElasticSearchIndexingManager', $result);
    }
}
