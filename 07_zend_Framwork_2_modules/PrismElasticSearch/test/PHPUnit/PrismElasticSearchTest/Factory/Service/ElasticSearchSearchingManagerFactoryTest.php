<?php

namespace PrismElasticSearchTest\Factory\Service;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Factory\Service\ElasticSearchSearchingManagerFactory;

/**
 * @author haniw
 */
class ElasticSearchSearchingManagerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ElasticSearchSearchingManagerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ElasticSearchSearchingManagerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismElasticSearch\Service\Manager\ElasticSearchSearchingManager', $result);
    }
}
