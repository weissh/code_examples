<?php

namespace PrismElasticSearchTest\Factory\Service\Index;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Factory\Service\Index\ElasticSearchProductsIndexerFactory;

/**
 * @author haniw
 */
class ElasticSearchProductsIndexerFactoryTest extends PHPUnit_Framework_TestCase
{
    /** @var ElasticSearchProductsIndexerFactory */
    protected $factory;

    /** @var ServiceLocatorInterface */
    protected $serviceLocator;

    public function setUp()
    {
        /** @var ServiceLocatorInterface $serviceLocator */
        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $factory = new ElasticSearchProductsIndexerFactory();
        $this->factory = $factory;
    }

    public function testCreateService()
    {
        $config = [
            'elasticSearchConfig' => [
                'hosts'         => ['http://10.128.140.31:9200'],
                'logging'       => false,
                'logPath'       => '/tmp/elasticsearch.log',
                'logLevel'      => \Psr\Log\LogLevel::DEBUG,
            ],
            'elasticSearchVariables' => [
                'numberOfShards'     => 5,
                'numberOfReplicas'   => 1
            ],
            'elasticSearchProductsIndexVariables' => [
                'indexPrefix'        => 'products_',
                'mappingType'        => 'product',
                'lockFile'           => '/tmp/elasticsearch_products.lck'
            ],
            'elasticSearchBlogPostsIndexVariables' => [
                'indexPrefix'        => 'blog_posts',
                'mappingType'        => 'post',
                'lockFile'           => '/tmp/elasticsearch_blog_posts.lck',
            ],
            'siteConfig' => [
                'languages' => [
                    'site-languages' => [
                        1,
                        2,
                    ]
                ],
                'websites' => [
                    'websites' => [
                        1 => 'Uk Website',
                        2 => 'US Website',
                        3 => 'Europe Website',
                    ],
                ],
            ],
        ];
        $this->serviceLocator->expects($this->at(0))
                         ->method('get')
                         ->with('Config')
                         ->willReturn($config);

        $commonStorage = $this->getMockBuilder('PrismAuth\Storage\CommonStorage')
                           ->disableOriginalConstructor()
                           ->getMock();

        $commonStorage->expects($this->once())
                   ->method('read')
                   ->with('siteConfig')
                   ->willReturn($config['siteConfig']);
        $this->serviceLocator->expects($this->at(1))
                        ->method('get')
                        ->with('CommonStorage')
                        ->willReturn($commonStorage);

        $elasticSearchLogsMapper = $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchLogsMapper')
                                        ->disableOriginalConstructor()
                                        ->getMock();
        $this->serviceLocator->expects($this->at(2))
                         ->method('get')
                         ->with('ElasticSearchLogsMapper')
                         ->willReturn($elasticSearchLogsMapper);

        $elasticSearchIndexQueueMapper = $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchIndexQueueMapper')
                                     ->disableOriginalConstructor()
                                     ->getMock();
        $this->serviceLocator->expects($this->at(3))
                      ->method('get')
                      ->with('ElasticSearchIndexQueueMapper')
                      ->willReturn($elasticSearchIndexQueueMapper);

        $elasticSearchLastIndexMapper = $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchLastIndexMapper')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $this->serviceLocator->expects($this->at(4))
                    ->method('get')
                    ->with('ElasticSearchLastIndexMapper')
                    ->willReturn($elasticSearchLastIndexMapper);

        $elasticSearchMappingDetailsMapper = $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchMappingDetailsMapper')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $this->serviceLocator->expects($this->at(5))
                    ->method('get')
                    ->with('ElasticSearchMappingDetailsMapper')
                    ->willReturn($elasticSearchMappingDetailsMapper);

        $elasticSearchProductsMapper = $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchProductsMapper')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $elasticSearchProductsMapper->expects($this->any())
                                    ->method('getMappingDetailsArray')
                                    ->with(1)
                                    ->willReturn([]);
        $this->serviceLocator->expects($this->at(6))
                    ->method('get')
                    ->with('ElasticSearchProductsMapper')
                    ->willReturn($elasticSearchProductsMapper);

        $result = $this->factory->createService($this->serviceLocator);
        $this->assertInstanceOf('PrismElasticSearch\Service\Base\ElasticSearchBaseIndexer', $result);
    }
}
