<?php

namespace PrismElasticSearchTest\Controller;

use PrismElasticSearch\Controller\IndexController;
use Zend\Session\Container;

/**
 * @author haniw
 */
class IndexControllerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var IndexController
     */
    protected $controller;

    /** @var \Zend\Mvc\Controller\PluginManager */
    protected $pluginManager;

    /** @var \Zend\EventManager\EventManager */
    protected $eventManager;

    /** @var array **/
    public $pluginManagerPlugins = [];

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;


    protected function setUp()
    {
        $controller = new IndexController();

        // pluginManager and event manager setted after controller created
        $pluginManager = $this->getMock('Zend\Mvc\Controller\PluginManager', ['get']);
        $pluginManager->expects($this->any())
                      ->method('get')
                      ->will($this->returnCallback([$this, 'helperMockCallbackPluginManagerGet']));
        $this->pluginManager = $pluginManager;

        $eventManager = $this->getMock('Zend\EventManager\EventManager');
        $this->eventManager = $eventManager;

        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $controller->setPluginManager($pluginManager);
        $controller->setEventManager($eventManager);
        $controller->setServiceLocator($serviceLocator);

        $this->controller = $controller;
    }

    public function testIndexAction()
    {
        $this->controller->indexAction();
    }

    public function provideReindexAction()
    {
        return [
            [2, 100, true],
            [1, 100, true],
        ];
    }

    /**
     * @dataProvider provideReindexAction
     */
    public function testReIndexAction($entityType, $batchSize, $lockStatus)
    {
        $params = $this->getMock('Zend\Mvc\Controller\Plugin\Params');
        $params->expects($this->at(0))
             ->method('__invoke')
             ->with('entityType')
             ->willReturn($entityType);
        $params->expects($this->at(1))
          ->method('__invoke')
          ->with('batchSize')
          ->willReturn($batchSize);

        $this->pluginManagerPlugins['params'] = $params;

        if ($entityType !== 1 && $entityType !== 3) {
            $this->setExpectedException('\Exception');
        } else {
            $elasticSearchIndexingManager = $this->getMockBuilder('PrismElasticSearch\Service\Manager\ElasticSearchIndexingManager')
                                                 ->disableOriginalConstructor()
                                                 ->getMock();
            $this->serviceLocator->expects($this->at(0))
                                 ->method('get')
                                 ->with('ElasticSearchIndexingManager')
                                 ->willReturn($elasticSearchIndexingManager);

            $elasticSearchIndexingManager->expects($this->at(0))
                                      ->method('setupIndexCategory')
                                      ->with($entityType);

            $elasticSearchIndexingManager->expects($this->at(1))
                                         ->method('getIndexLockedStatus')
                                         ->willReturn($lockStatus);

        }

        $this->controller->reindexAction();
    }

    public function helperMockCallbackPluginManagerGet($key)
    {
        return (array_key_exists($key, $this->pluginManagerPlugins))
            ? $this->pluginManagerPlugins[$key]
            : null;
    }
}
