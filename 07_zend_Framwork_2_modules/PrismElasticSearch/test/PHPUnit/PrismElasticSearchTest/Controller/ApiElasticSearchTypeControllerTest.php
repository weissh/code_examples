<?php

namespace PrismElasticSearchTest\Controller;

use PrismElasticSearch\Controller\ApiElasticSearchTypeController;
use Zend\Session\Container;
use ReflectionProperty;

/**
 * @author haniw
 */
class ApiElasticSearchTypeControllerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ApiElasticSearchTypeController
     */
    protected $controller;

    /** @var \Zend\Mvc\Controller\PluginManager */
    protected $pluginManager;

    /** @var \Zend\EventManager\EventManager */
    protected $eventManager;

    /** @var array **/
    public $pluginManagerPlugins = [];

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;


    protected function setUp()
    {
        $controller = new ApiElasticSearchTypeController();

        // pluginManager and event manager setted after controller created
        $pluginManager = $this->getMock('Zend\Mvc\Controller\PluginManager', ['get']);
        $pluginManager->expects($this->any())
                      ->method('get')
                      ->will($this->returnCallback([$this, 'helperMockCallbackPluginManagerGet']));
        $this->pluginManager = $pluginManager;

        $eventManager = $this->getMock('Zend\EventManager\EventManager');
        $this->eventManager = $eventManager;

        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $controller->setPluginManager($pluginManager);
        $controller->setEventManager($eventManager);
        $controller->setServiceLocator($serviceLocator);

        $this->controller = $controller;
    }

    public function setNotAllowed()
    {
        $response = $this->getMockBuilder('Zend\Http\PhpEnvironment\Response')
                        ->disableOriginalConstructor()
                        ->getMock();
        $response->expects($this->once())
            ->method('setStatusCode')
            ->with(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        $this->helperMakePropertyAccessable($this->controller, 'response', $response);
        $this->setExpectedException('\Exception');
    }

    public function provideGetList()
    {
        return [
            [true],
            [false],
        ];
    }

    /**
     * @dataProvider provideGetList
     */
    public function testGetList($hasPriority)
    {
        $elasticSearchMappingDetailsMapper =  $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchMappingDetailsMapper')
                         ->disableOriginalConstructor()
                         ->getMock();
        $result = ($hasPriority) ? ['priority' => 1] : FALSE;
        $elasticSearchMappingDetailsMapper->expects($this->once())
                                          ->method('getSearchPriorities')
                                          ->with(1)
                                          ->willReturn($result);

        $this->serviceLocator->expects($this->once())
                             ->method('get')
                             ->with('ElasticSearchMappingDetailsMapper')
                             ->willReturn($elasticSearchMappingDetailsMapper);

        $params = $this->getMock('Zend\Mvc\Controller\Plugin\Params');
        $params->expects($this->any())
             ->method('__invoke')
             ->will($this->returnSelf());
        $query = [
             'elasticSearchType' => 1,
         ];
        $params->expects($this->once())
                ->method('fromRoute')
                ->will($this->returnCallback(function ($key) use ($query) {
                 return $query;
         }));
        $this->pluginManagerPlugins['params'] = $params;

        if (!$hasPriority) {
            $this->setExpectedException('\Exception');
        }
        $this->controller->getList();
    }

    public function testGet()
    {
        $this->setNotAllowed();
        $this->controller->get(1);
    }

    public function testUpdate()
    {
        $this->setNotAllowed();
        $this->controller->update(1, ['id' => 1]);
    }

    public function testDelete()
    {
        $this->setNotAllowed();
        $this->controller->delete(1);
    }

    public function helperMockCallbackPluginManagerGet($key)
    {
        return (array_key_exists($key, $this->pluginManagerPlugins))
            ? $this->pluginManagerPlugins[$key]
            : null;
    }

    /**
     *
     * @param mixed $objectOrClass
     * @param string $property
     * @param mixed $value = null
     * @return \ReflectionProperty
     */
    public function helperMakePropertyAccessable($objectOrClass, $property, $value = null)
    {
        $reflectionProperty = new ReflectionProperty($objectOrClass, $property);
        $reflectionProperty->setAccessible(true);

        if ($value !== null) {
            $reflectionProperty->setValue($objectOrClass, $value);
        }
        return $reflectionProperty;
    }
}
