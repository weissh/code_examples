<?php

namespace PrismElasticSearchTest\Controller;

use PrismElasticSearch\Controller\SearchManagerController;
use ReflectionProperty;
use Zend\Http\Client as HttpClient;

/**
 * @author haniw
 */
class SearchManagerControllerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var SearchManagerController
     */
    protected $controller;

    /** @var \Zend\Mvc\Controller\PluginManager */
    protected $pluginManager;

    /** @var \Zend\EventManager\EventManager */
    protected $eventManager;

    /** @var array **/
    public $pluginManagerPlugins = [];

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var \PrismElasticSearch\Model\ElasticSearchMappingDetailsMapper
     */
    protected $elasticSearchMappingDetailsMapper;


    protected function setUp()
    {
        $this->elasticSearchMappingDetailsMapper =  $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchMappingDetailsMapper')
                         ->disableOriginalConstructor()
                         ->getMock();

        $this->client = new HttpClient();
        $this->client->setAdapter('Zend\Http\Client\Adapter\Test');

        $controller = new SearchManagerController(
            $this->elasticSearchMappingDetailsMapper,
            [
                'siteConfig' => [
                    'websites' => [
                        'website-url' => [
                            0 => 'foo.com',
                            1 => 'bar.com',
                        ],
                        'clear-cache' => [
                            '1' => 'http://prism-cms-website.local/',
                        ],
                    ],
                ],
            ],
            $this->client
        );

        // pluginManager and event manager setted after controller created
        $pluginManager = $this->getMock('Zend\Mvc\Controller\PluginManager', ['get']);
        $pluginManager->expects($this->any())
                      ->method('get')
                      ->will($this->returnCallback([$this, 'helperMockCallbackPluginManagerGet']));
        $this->pluginManager = $pluginManager;

        $eventManager = $this->getMock('Zend\EventManager\EventManager');
        $this->eventManager = $eventManager;

        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $controller->setPluginManager($pluginManager);
        $controller->setEventManager($eventManager);
        $controller->setServiceLocator($serviceLocator);

        $this->controller = $controller;
    }

    public function testSearchPrioritiesAction()
    {
        $this->elasticSearchMappingDetailsMapper->expects($this->at(0))
                                                ->method('getMappingPrioritiesArray')
                                                ->with(1);
        $this->elasticSearchMappingDetailsMapper->expects($this->at(1))
                                                ->method('getMappingPrioritiesArray')
                                                ->with(3);

        $this->controller->searchPrioritiesAction();
    }

    public function provideUpdateSearchPriorities()
    {
        return [
            [false, true, ['inputName' => false, 'inputValue' => false]],
            [false, false, ['inputName' => false, 'inputValue' => false]],
            [true, true, ['inputName' => false, 'inputValue' => false]],
            [true, true, ['inputName' => true, 'inputValue' => false]],
            [true, true, ['inputName' => false, 'inputValue' => true]],
            [true, true, ['inputName' => '1_1', 'inputValue' => '1']],
        ];
    }

    /**
     * @dataProvider provideUpdateSearchPriorities
     */
    public function testUpdateSearchPrioritiesAction($isXmlHttpRequest, $isPost, $inputNameAndValues)
    {
        $request = $this->getMockBuilder('Zend\Http\Request')
                        ->disableOriginalConstructor()
                        ->getMock();
        $request->expects($this->at(0))
            ->method('isXmlHttpRequest')
            ->willReturn($isXmlHttpRequest);

        if ($isXmlHttpRequest) {
            $request->expects($this->at(1))
                ->method('isPost')
                ->willReturn($isPost);
            $params = $this->getMock('Zend\Mvc\Controller\Plugin\Params');
            $params->expects($this->exactly(2))
                 ->method('__invoke')
                 ->will($this->returnSelf());
            $query = [
                 'inputName' => $inputNameAndValues['inputName'],
                 'inputValue' => $inputNameAndValues['inputValue'],
             ];
            $params->expects($this->exactly(2))
                    ->method('fromPost')
                    ->will($this->returnCallback(function ($key) use ($query) {
                     return $query[$key];
             }));
            $this->pluginManagerPlugins['params'] = $params;

            if ($inputNameAndValues['inputName'] && $inputNameAndValues['inputValue']) {
                $inputValue = (int) $inputNameAndValues['inputValue'];

                if ($inputValue >= 1) {

                }
            }
        }

        $this->helperMakePropertyAccessable($this->controller, 'request', $request);

        $this->controller->updateSearchPrioritiesAction();
    }

    public function helperMockCallbackPluginManagerGet($key)
    {
        return (array_key_exists($key, $this->pluginManagerPlugins))
            ? $this->pluginManagerPlugins[$key]
            : null;
    }

    /**
     *
     * @param mixed $objectOrClass
     * @param string $property
     * @param mixed $value = null
     * @return \ReflectionProperty
     */
    public function helperMakePropertyAccessable($objectOrClass, $property, $value = null)
    {
        $reflectionProperty = new ReflectionProperty($objectOrClass, $property);
        $reflectionProperty->setAccessible(true);

        if ($value !== null) {
            $reflectionProperty->setValue($objectOrClass, $value);
        }
        return $reflectionProperty;
    }
}
