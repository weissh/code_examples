<?php

namespace PrismElasticSearchTest\Controller;

use PrismElasticSearch\Controller\ApiElasticSearchContentController;
use Zend\Session\Container;
use ReflectionProperty;

/**
 * @author haniw
 */
class ApiElasticSearchContentControllerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ApiElasticSearchContentController
     */
    protected $controller;

    /** @var \Zend\Mvc\Controller\PluginManager */
    protected $pluginManager;

    /** @var \Zend\EventManager\EventManager */
    protected $eventManager;

    /** @var array **/
    public $pluginManagerPlugins = [];

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    protected function setUp()
    {
        $controller = new ApiElasticSearchContentController();

        // pluginManager and event manager setted after controller created
        $pluginManager = $this->getMock('Zend\Mvc\Controller\PluginManager', ['get']);
        $pluginManager->expects($this->any())
                      ->method('get')
                      ->will($this->returnCallback([$this, 'helperMockCallbackPluginManagerGet']));
        $this->pluginManager = $pluginManager;

        $eventManager = $this->getMock('Zend\EventManager\EventManager');
        $this->eventManager = $eventManager;

        $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->serviceLocator = $serviceLocator;

        $controller->setPluginManager($pluginManager);
        $controller->setEventManager($eventManager);
        $controller->setServiceLocator($serviceLocator);

        $this->controller = $controller;
    }

    public function testCreate()
    {
        $elastic =  $this->getMockBuilder('PrismElasticSearch\Service\Manager\ElasticSearchSearchingManager')
            ->setConstructorArgs([$this->serviceLocator])
            ->getMock();
        $elastic->expects($this->once())
              ->method('setupIndexCategory')
              ->with(2);

        $elastic->expects($this->at(1))
                   ->method('__call')
                   ->with('searchByParams', [[]])
                   ->willReturn([
           'response' => [
               'hits' => [
                   'hits' => [],
                   'total' => 0,
               ],
           ],
        ]);

        $this->serviceLocator->expects($this->once())
                              ->method('get')
                              ->with('ElasticSearchSearchingManager')
                           ->willReturn($elastic);

        $this->assertInstanceOf('Zend\View\Model\JsonModel', $this->controller->create([]));
    }

    public function testCreateThrowException()
    {
        $elastic =  $this->getMockBuilder('PrismElasticSearch\Service\Manager\ElasticSearchSearchingManager')
            ->setConstructorArgs([$this->serviceLocator])
            ->getMock();

        try {
            $elastic->expects($this->any())
                           ->method('__call')
                           ->with('searchByParams', [[]])
                           ->will($this->throwException(new \Exception()));
            $this->setExpectedException('\Exception');
        } catch (\Exception $e) {
            return;
        }
        $this->serviceLocator->expects($this->once())
                              ->method('get')
                              ->with('ElasticSearchSearchingManager')
                           ->willReturn($elastic);

        $this->assertInstanceOf('Zend\View\Model\JsonModel', $this->controller->create([]));
    }

    public function setNotAllowed()
    {
        $response = $this->getMockBuilder('Zend\Http\PhpEnvironment\Response')
                        ->disableOriginalConstructor()
                        ->getMock();
        $response->expects($this->once())
            ->method('setStatusCode')
            ->with(\Zend\Http\PhpEnvironment\Response::STATUS_CODE_405);
        $this->helperMakePropertyAccessable($this->controller, 'response', $response);
        $this->setExpectedException('\Exception');
    }

    public function testGet()
    {
        $this->setNotAllowed();
        $this->controller->get(1);
    }

    public function testUpdate()
    {
        $this->setNotAllowed();
        $this->controller->update(1, ['id' => 1]);
    }

    public function testDelete()
    {
        $this->setNotAllowed();
        $this->controller->delete(1);
    }

    public function helperMockCallbackPluginManagerGet($key)
    {
        return (array_key_exists($key, $this->pluginManagerPlugins))
            ? $this->pluginManagerPlugins[$key]
            : null;
    }

    /**
     *
     * @param mixed $objectOrClass
     * @param string $property
     * @param mixed $value = null
     * @return \ReflectionProperty
     */
    public function helperMakePropertyAccessable($objectOrClass, $property, $value = null)
    {
        $reflectionProperty = new ReflectionProperty($objectOrClass, $property);
        $reflectionProperty->setAccessible(true);

        if ($value !== null) {
            $reflectionProperty->setValue($objectOrClass, $value);
        }
        return $reflectionProperty;
    }
}
