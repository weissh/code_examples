<?php

namespace PrismElasticSearchTest\Model;

use PrismElasticSearch\Model\ElasticSearchLastIndexMapper;
use PrismElasticSearchTest\ZendDbFixture\FixtureManager;
use PHPUnit_Framework_TestCase;

/**
 * @author haniw
 */
class ElasticSearchLastIndexMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ElasticSearchLastIndexMapper
     */
    private $model;

    protected function setUp()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');
        $this->model = new ElasticSearchLastIndexMapper($this->fixtureManager->getDbAdapter());
        $this->fixtureManager->execute("ELASTICSEARCH_LastIndex-up.sql");
    }

    public function provideLastTimestampData()
    {
        return [
            [1, '1426766174'],
            [2, '1409254160'],
            [3, '1425036105'],
            [100, false],
            [200, false],
            [300, false],
        ];
    }

    /**
     * @dataProvider provideLastTimestampData
     */
    public function testGetLastTimestamp($elasticSearchType, $result)
    {
        $this->assertEquals($result, $this->model->getLastTimestamp($elasticSearchType));
    }

    public function provideSetLastTimestamp()
    {
        return [
            [true, true],
            [true, false],
            [false, true],
            [false, false],
        ];
    }

    /**
     * @dataProvider provideSetLastTimestamp
     */
    public function testSetLastTimeStamp($found, $exception)
    {
        $elasticSearchType = 1;
        $timeStamp = '1426766174';
        if (!$found) {
            $elasticSearchType = 100;
        }
        if ($exception) {
            $this->setExpectedException('\Exception');
            $timeStamp = new \stdClass();
        }
        $this->model->setLastTimestamp($timeStamp, $elasticSearchType);
    }

    protected function tearDown()
    {
        $this->fixtureManager->execute("ELASTICSEARCH_LastIndex-down.sql");
    }

}
