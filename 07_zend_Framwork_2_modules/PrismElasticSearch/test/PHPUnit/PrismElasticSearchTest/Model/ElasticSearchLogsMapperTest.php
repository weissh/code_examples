<?php

namespace PrismElasticSearchTest\Model;

use PrismElasticSearch\Model\ElasticSearchLogsMapper;
use PrismElasticSearchTest\ZendDbFixture\FixtureManager;
use PHPUnit_Framework_TestCase;

/**
 * @author haniw
 */
class ElasticSearchLogsMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ElasticSearchLogsMapper
     */
    private $model;

    protected function setUp()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');
        $this->model = new ElasticSearchLogsMapper($this->fixtureManager->getDbAdapter());
        $this->fixtureManager->execute("ELASTICSEARCH_Logs-up.sql");
    }

    public function provideLastProcessId()
    {
        return [
            [1, 1],
        ];
    }

    /**
     * @dataProvider provideLastProcessId
     */
    public function testGetLastProcessId($indexType, $result)
    {
        $this->assertEquals($result, $this->model->getLastProcessId($indexType));
    }

    public function provideCreateNewLogParams()
    {
        return [
            [1, 'log', 1, true],
            [1, 'log', false, true],
            [1, 'log', new \stdClass(), true],
        ];
    }

    /**
     * @dataProvider provideCreateNewLogParams
     */
    public function testCreateNewLog($indexType, $logText, $processId, $isError)
    {
        if ($processId !== false && !is_numeric($processId)) {
            $this->setExpectedException('\Exception');
        }
        $this->model->createNewLog($indexType, $logText, $processId, $isError);
    }

    protected function tearDown()
    {
        $this->fixtureManager->execute("ELASTICSEARCH_Logs-down.sql");
    }

}
