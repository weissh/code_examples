<?php

namespace PrismElasticSearchTest\Model;

use PrismElasticSearch\Model\ElasticSearchMappingDetailsMapper;
use PrismElasticSearchTest\ZendDbFixture\FixtureManager;
use PHPUnit_Framework_TestCase;

/**
 * @author haniw
 */
class ElasticSearchMappingDetailsMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ElasticSearchMappingDetailsMapper
     */
    private $model;

    protected function setUp()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');
        $this->model = new ElasticSearchMappingDetailsMapper($this->fixtureManager->getDbAdapter());
    }

    public function provideUpdateFieldPriority()
    {
        return [
            ['1', 1, 1, true, 0],
            ['1', 1, 2, true, 1],
            ['1', 100, 2, false, 0],
        ];
    }

    /**
     * @dataProvider provideUpdateFieldPriority
     */
    public function testUpdateFieldPriority($elasticSearchType, $fieldId, $inputValue, $found, $affected)
    {
        $this->fixtureManager->execute("ELASTICSEARCH_Mapping_Details-up.sql");
        $this->assertEquals($affected, $this->model->updateFieldPriority($elasticSearchType, $fieldId, $inputValue));
    }

    public function testGetMappingDetailsArray()
    {
        $this->fixtureManager->execute("ELASTICSEARCH_Mapping_Details-up.sql");
        $this->model->getMappingDetailsArray(1);
    }

    public function testGetSearchPriorities()
    {
        $this->fixtureManager->execute("ELASTICSEARCH_Mapping_Details-up.sql");
        $this->model->getSearchPriorities(1);
    }

    public function testGetMappingPrioritiesArray()
    {
        $this->fixtureManager->execute("ELASTICSEARCH_Mapping_Details-up.sql");
        $this->model->getMappingPrioritiesArray(1);
    }

    protected function tearDown()
    {
        $this->fixtureManager->execute("ELASTICSEARCH_Mapping_Details-down.sql");
    }

}
