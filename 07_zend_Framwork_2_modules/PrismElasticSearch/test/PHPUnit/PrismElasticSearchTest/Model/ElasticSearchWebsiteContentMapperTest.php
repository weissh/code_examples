<?php

namespace PrismElasticSearchTest\Model;

use PrismElasticSearch\Model\ElasticSearchWebsiteContentMapper;
use PrismElasticSearchTest\ZendDbFixture\FixtureManager;
use PHPUnit_Framework_TestCase;

/**
 * @author haniw
 */
class ElasticSearchWebsiteContentMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ElasticSearchLogsMapper
     */
    private $model;

    protected function setUp()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');
        $this->model = new ElasticSearchWebsiteContentMapper($this->fixtureManager->getDbAdapter(), [
            'siteConfig' => [
                'websites' => [
                    'www.oliversweeney.com'
                ],
                'languages' => [
                    'site-languages' => [
                            1,
                            2,
                            3,
                    ],
                ],
            ],
        ]);

        $this->fixtureManager->execute("CONTENT-up.sql");
        $this->fixtureManager->execute("CONTENT_Definitions-up.sql");
        $this->fixtureManager->execute("CONTENT_Blocks-up.sql");
    }

    public function provideGetIdsForBatch()
    {
        return [
            [1, 1],
            [false, 1],
        ];
    }

    /**
     * @dataProvider provideGetIdsForBatch
     */
    public function testGetIdsForBatch($from, $limit)
    {
        $this->model->getIdsForBatch($from, $limit);
    }

    public function provideElasticSearch()
    {
        return [
            [1, true],
            [10101, false],
            [1, false],
            [0, false],
        ];
    }

    /**
     * @dataProvider provideElasticSearch
     */
    public function testGetIndexLayout($elasticSearchId, $tobeIndexed)
    {
        if (! $tobeIndexed) {
            $this->fixtureManager->getDbAdapter()->query("DELETE FROM CONTENT", 'execute');
            $this->fixtureManager->getDbAdapter()->query("DELETE FROM CONTENT_Definitions", 'execute');
            $this->fixtureManager->getDbAdapter()->query("DELETE FROM CONTENT_Blocks", 'execute');
        } else {
            $this->fixtureManager->execute("CONTENT-up.sql");
            $this->fixtureManager->execute("CONTENT_Definitions-up.sql");
            $this->fixtureManager->execute("CONTENT_Blocks-up.sql");
        }

        $this->model->getIndexLayout($elasticSearchId);
    }

    protected function tearDown()
    {
        $this->fixtureManager->execute("CONTENT-down.sql");
        $this->fixtureManager->execute("CONTENT_Definitions-down.sql");
        $this->fixtureManager->execute("CONTENT_Blocks-down.sql");
    }

}
