<?php

namespace PrismElasticSearchTest\Model;

use PrismElasticSearch\Model\ElasticSearchProductsMapper;
use PrismProductsManager\Model\ProductsMapper;
use PrismElasticSearchTest\ZendDbFixture\FixtureManager;
use PHPUnit_Framework_TestCase;

/**
 * @author haniw
 */
class ElasticSearchProductsMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ElasticSearchProductsMapper
     */
    private $model;

    /**
     * @var \PrismProductsManager\Model\CategoriesMapper
     */
    private $categoryMapper;

    /**
     * @var \PrismProductsManager\Model\ProductsMapper
     */
    private $productsMapper;

    protected function setUp()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');
        $this->categoriesMapper = $this->getMockBuilder('PrismProductsManager\Model\CategoriesMapper')
                                 ->disableOriginalConstructor()
                                 ->getMock();
        $this->productsMapper = new ProductsMapper(
            $this->fixtureManager->getDbAdapter(),
            [
                'languages' => [
                  'site-languages' => 1,
                  'current-language' => 1,
                ],
                'websites' => [
                    'current-website-id' => 1,
                ],
                'pagination' => [
                    'records-per-page' => 10,
                ],
                'currencies' => [
                    'GBP',
                ],
            ],
            $this->getMockBuilder('PrismProductsManager\Model\AttributesGroupMapper')->disableOriginalConstructor()->getMock(),
            null,
            $this->getMockBuilder('PrismProductsManager\Model\MerchantCategoriesMapper')->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder('PrismCommon\Service\ActionLogger')->disableOriginalConstructor()->getMock(),
            null,
            $this->getMockBuilder('PrismProductsManager\Model\TaxMapper')->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder('PrismMediaManager\Model\ProductFolderMapper')->disableOriginalConstructor()->getMock(),
            null,
            1,
            $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable')->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder('PrismProductsManager\Service\InputService')->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder('PrismProductsManager\Model\ColoursMapper')->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder('PrismProductsManager\Service\CommonAttributesService')->disableOriginalConstructor()->getMock(),
            'LTS',
            $this->getMockBuilder('PrismQueueManager\Service\QueueService')->disableOriginalConstructor()->getMock(),
            [
                'customHeader' => true,
                'headerSettings' => [
                    'default' => 'Default',
                    'sweeneyLondon' => 'Sweeney London'
                ],
            ],
            $this->getMockBuilder('PrismCommon\Service\MailSender')
                ->disableOriginalConstructor()
                ->getMock()
            ,
            []
        );

        $this->model = new ElasticSearchProductsMapper(
            $this->fixtureManager->getDbAdapter(),
            ['siteConfig' =>
                [
                    'websites' => 1,
                    'currencies' => 1,
                    'languages' => [
                      'site-languages' => 1,
                    ],
                ],
            ],
            $this->categoriesMapper,
            $this->productsMapper,
            $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesDefinitionsTable')->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesValuesTable')->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesTable')->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder('PrismProductsManager\Model\CommonAttributes\CommonAttributesDefinitionsTable')->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder('PrismProductsManager\Model\CommonAttributesMapper')->disableOriginalConstructor()->getMock()
        );

        $this->fixtureManager->execute('PRODUCTS-up.sql');
        $this->fixtureManager->execute('PRODUCTS_Types-up.sql');
        $this->fixtureManager->execute('PRODUCTS_Types_Definitions-up.sql');
        $this->fixtureManager->execute('PRODUCTS_R_ATTRIBUTES-up.sql');
        $this->fixtureManager->execute('PRODUCTS_R_ATTRIBUTES_R_SKU_Rules-up.sql');
        $this->fixtureManager->execute('COLOURS_Groups-up.sql');
        $this->fixtureManager->execute('COLOURS_Groups_Definitions-up.sql');
        $this->fixtureManager->execute('COLOURS_R_GROUPS-up.sql');
        $this->fixtureManager->execute('COLOURS_Definitions-up.sql');
        $this->fixtureManager->execute('SIZES_Accessories-up.sql');
        $this->fixtureManager->execute('SIZES_Shoes-up.sql');
        $this->fixtureManager->execute('SIZES_Clothes-up.sql');
        $this->fixtureManager->execute('SIZES_Shirts-up.sql');
        $this->fixtureManager->execute('SIZES_Belts-up.sql');
        $this->fixtureManager->execute('SIZES_Jackets-up.sql');
        $this->fixtureManager->execute('SIZES_Trousers-up.sql');
        $this->fixtureManager->execute('SIZES_SwimShorts-up.sql');
        $this->fixtureManager->execute('PRODUCTS_Prices-up.sql');
        $this->fixtureManager->execute('PRODUCTS_Definitions-up.sql');
    }

    public function provideGetIndexLayout()
    {
        return [
            ['1_1'],
            ['1_0'],
        ];
    }

    /**
     * @dataProvider provideGetIndexLayout
     */
    public function testGetIndexLayout($elasticSearchId)
    {
        $this->model->getIndexLayout($elasticSearchId);
    }

    public function testGetIdsForBatch()
    {
        $this->model->getIdsForBatch(1, 1);
    }

    protected function tearDown()
    {
        $this->fixtureManager->execute('PRODUCTS-down.sql');
        $this->fixtureManager->execute('PRODUCTS_Types-down.sql');
        $this->fixtureManager->execute('PRODUCTS_Types_Definitions-down.sql');
        $this->fixtureManager->execute('PRODUCTS_R_ATTRIBUTES-down.sql');
        $this->fixtureManager->execute('PRODUCTS_R_ATTRIBUTES_R_SKU_Rules-down.sql');
        $this->fixtureManager->execute('COLOURS_Groups-down.sql');
        $this->fixtureManager->execute('COLOURS_Groups_Definitions-down.sql');
        $this->fixtureManager->execute('COLOURS_R_GROUPS-down.sql');
        $this->fixtureManager->execute('COLOURS_Definitions-down.sql');
        $this->fixtureManager->execute('SIZES_Accessories-down.sql');
        $this->fixtureManager->execute('SIZES_Shoes-down.sql');
        $this->fixtureManager->execute('SIZES_Clothes-down.sql');
        $this->fixtureManager->execute('SIZES_Shirts-down.sql');
        $this->fixtureManager->execute('SIZES_Belts-down.sql');
        $this->fixtureManager->execute('SIZES_Jackets-down.sql');
        $this->fixtureManager->execute('SIZES_Trousers-down.sql');
        $this->fixtureManager->execute('SIZES_SwimShorts-down.sql');
        $this->fixtureManager->execute('PRODUCTS_Prices-down.sql');
        $this->fixtureManager->execute('PRODUCTS_Definitions-down.sql');
    }
}
