<?php

namespace PrismElasticSearchTest\Model;

use PrismElasticSearch\Model\ElasticSearchIndexQueueMapper;
use PrismElasticSearchTest\ZendDbFixture\FixtureManager;
use PHPUnit_Framework_TestCase;

/**
 * @author haniw
 */
class ElasticSearchIndexQueueMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ElasticSearchLastIndexMapper
     */
    private $model;

    protected function setUp()
    {
        $this->fixtureManager = new FixtureManager('Zend\Db\Adapter\Adapter');
        $this->model = new ElasticSearchIndexQueueMapper($this->fixtureManager->getDbAdapter());
        $this->fixtureManager->execute("ELASTICSEARCH_IndexQueue-up.sql");
    }

    public function provideLastTimestampData()
    {
        return [
            [1],
            [3],
        ];
    }

    /**
     * @dataProvider provideLastTimestampData
     */
    public function testGetLastTimestamp($elasticSearchType)
    {
        $this->model->getProductsInQueue($elasticSearchType);
    }

    public function testAddProductsInQueue()
    {
        $elasticSearchIds = [60];
        $this->model->addProductsInQueue($elasticSearchIds);
    }

    public function testAddProductsInQueueThrowException()
    {
        try {
            $elasticSearchIds = ['foo'];
            $this->model->addProductsInQueue($elasticSearchIds);
        } catch (\Exception $e) {

        }
    }

    protected function tearDown()
    {
        $this->fixtureManager->execute("ELASTICSEARCH_IndexQueue-down.sql");
    }

}
