<?php

namespace PrismElasticSearchTest\Service;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Service\ElasticSearchProductsSearching;
use ReflectionClass;

/**
 * @author haniw
 */
class ElasticSearchProductsSearchingTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ElasticSearchBlogPostsSearching
     */
    private $service;

    /**
     * @var \Elasticsearch\Client
     */
    private $elastic;

    protected function setUp()
    {
        $indexesArray = [
            1 => [
                1 => 'products_1',
                2 => 'products_2',
            ],
        ];
        $this->elastic = $this->getMockBuilder('Elasticsearch\Client')
                        ->disableOriginalConstructor()
                        ->getMock();
        $this->service = new ElasticSearchProductsSearching($this->elastic, $indexesArray, 'product');
    }

    public function testApplyChannelFilter()
    {
        $this->service->applyChannelFilter([1]);
    }

    public function testApplyColourFilter()
    {
        $this->service->applyColourFilter(['Red', 'BLUe']);
    }

    public function testApplyProductIdFilter()
    {
        $this->service->applyProductIdFilter([1, '2']);
    }

    public function testApplyUrlFilter()
    {
        $this->service->applyUrlFilter(['www.GooGle.com', 'www.YAHOO.com']);
    }

    public function testApplyPriceFilter()
    {
        $this->service->applyPriceFilter(['gt' => 100]);
    }

    public function testApplySpectrumIdFilter()
    {
        $this->service->applySpectrumIdFilter([100, 1000]);
    }

    public function testApplySizeFilter()
    {
        $this->service->applySizeFilter([
            'gt' => false,
        ]);
    }

    public function testApplystyleFilter()
    {
        $this->service->applyStyleFilter(true);
    }

    public function provideBuildParamsArray()
    {
        return [
            [null, [
                'websiteID' => 1,
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => false,
                'filters' => [
                    'categories' => false,
                    'colours' => false,
                    'sizes' => false,
                    'styles' => false,
                    'price' => false,
                    'custom' => false,
                    'url' => false,
                    'productId' => false,
                    'spectrumId' => false,
                    'elasticSearchIds' => false,
                    'customOrder' => true,
                    'customFieldFilter' => array(),
                    'customAggregations' => false,
                ],
                'searchType' => false,
                'customFields' => false,
            ]],
            ['', [
                'websiteID' => 1,
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => false,
                'filters' => [
                    'categories' => false,
                    'colours' => false,
                    'sizes' => false,
                    'styles' => false,
                    'price' => false,
                    'custom' => false,
                    'url' => false,
                    'productId' => false,
                    'spectrumId' => false,
                    'elasticSearchIds' => false,
                    'customOrder' => true,
                    'customFieldFilter' => array(),
                    'customAggregations' => false,
                ],
                'searchType' => false,
                'customFields' => false,
            ]],
            [0, [
                'websiteID' => 1,
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => false,
                'filters' => [
                    'categories' => false,
                    'colours' => false,
                    'sizes' => false,
                    'styles' => false,
                    'price' => false,
                    'custom' => false,
                    'url' => false,
                    'productId' => false,
                    'spectrumId' => false,
                    'elasticSearchIds' => false,
                    'customOrder' => true,
                    'customFieldFilter' => array(),
                    'customAggregations' => false,
                ],
                'searchType' => false,
                'customFields' => false,
            ]],
            [false, [
                'websiteID' => 1,
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => false,
                'filters' => [
                    'categories' => false,
                    'colours' => false,
                    'sizes' => false,
                    'styles' => false,
                    'price' => false,
                    'custom' => false,
                    'url' => false,
                    'productId' => false,
                    'spectrumId' => false,
                    'elasticSearchIds' => false,
                    'customOrder' => true,
                    'customFieldFilter' => array(),
                    'customAggregations' => false,
                ],
                'searchType' => false,
                'customFields' => false,
            ]],
            ['foo', [
                'websiteID' => 1,
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => 'foo',
                'filters' => [
                    'categories' => false,
                    'colours' => false,
                    'sizes' => false,
                    'styles' => false,
                    'price' => false,
                    'custom' => false,
                    'url' => false,
                    'productId' => false,
                    'spectrumId' => false,
                    'elasticSearchIds' => false,
                    'customOrder' => true,
                    'customFieldFilter' => array(),
                    'customAggregations' => false,
                ],
                'searchType' => false,
                'customFields' => false,
            ]]
        ];
    }

    /**
     * @dataProvider provideBuildParamsArray
     */
    public function testBuildParamsArray($searchString, $result)
    {
        $this->assertEquals($result, $this->service->buildParamsArray(1, 1, $searchString, [], 0, 20, true));
    }

    public function testResetFilters()
    {
        $this->service->resetFilters();
    }

    public function testApplycategoryFilter()
    {
        $this->service->applyCategoryFilter(true);
    }

    public function provideBuildFinalFilter()
    {
        return [
            [[
                '_categoryFilter' => true,
                '_channelFilter' => [1],
                '_colourFilter' => ['red'],
                '_styleFilter' => true,
                '_sizeFilter' => ['gt' => false],
                '_priceFilter' => ['gt' => 100],
                '_spectrumIdFilter' => [100, 200],
                '_productIdsFilter' => [1, '2'],
            ]],
            [[
                '_categoryFilter' => true,
                '_channelFilter' => [1],
                '_colourFilter' => ['red'],
                '_styleFilter' => true,
                '_sizeFilter' => ['gte' => false],
                '_priceFilter' => ['gte' => 100],
                '_spectrumIdFilter' => [100, 200],
                '_productIdsFilter' => [1, '2'],
            ]],
            [[
                '_categoryFilter' => true,
                '_channelFilter' => [1],
                '_colourFilter' => ['red'],
                '_styleFilter' => true,
                '_sizeFilter' => ['lt' => false],
                '_priceFilter' => ['lt' => 100],
                '_spectrumIdFilter' => [100, 200],
                '_productIdsFilter' => [1, '2'],
            ]],
            [[
                '_categoryFilter' => true,
                '_channelFilter' => [1],
                '_colourFilter' => ['red'],
                '_styleFilter' => true,
                '_sizeFilter' => ['lte' => false],
                '_priceFilter' => ['lte' => 100],
                '_spectrumIdFilter' => [100, 200],
                '_productIdsFilter' => [1, '2'],
            ]],
            [[
                '_categoryFilter' => true,
                '_channelFilter' => [1],
                '_colourFilter' => ['red'],
                '_styleFilter' => true,
                '_sizeFilter' => ['exact' => false],
                '_priceFilter' => ['exact' => 100],
                '_spectrumIdFilter' => [100, 200],
                '_productIdsFilter' => [1, '2'],
            ]],
            [[
                '_categoryFilter' => true,
                '_channelFilter' => [1],
                '_colourFilter' => ['red'],
                '_styleFilter' => true,
                '_sizeFilter' => ['gt' => 10, 'lt' => 188],
                '_priceFilter' => ['gt' => 8, 'lt' => 180],
                '_spectrumIdFilter' => [100, 200],
                '_productIdsFilter' => [1, '2'],
            ]],
            [[
                '_categoryFilter' => true,
                '_channelFilter' => [1],
                '_colourFilter' => ['red'],
                '_styleFilter' => true,
                '_sizeFilter' => ['gt' => 10, 'lte' => 188],
                '_priceFilter' => ['gt' => 8, 'lte' => 180],
                '_spectrumIdFilter' => [100, 200],
                '_productIdsFilter' => [1, '2'],
            ]],
            [[
                '_categoryFilter' => true,
                '_channelFilter' => [1],
                '_colourFilter' => ['red'],
                '_styleFilter' => true,
                '_sizeFilter' => ['gte' => 10, 'lte' => 188],
                '_priceFilter' => ['gte' => 8, 'lte' => 180],
                '_spectrumIdFilter' => [100, 200],
                '_productIdsFilter' => [1, '2'],
            ]],
            [[
                '_categoryFilter' => true,
                '_channelFilter' => [1],
                '_colourFilter' => ['red'],
                '_styleFilter' => true,
                '_sizeFilter' => ['gte' => 10, 'lt' => 188],
                '_priceFilter' => ['gte' => 8, 'lt' => 180],
                '_spectrumIdFilter' => [100, 200],
                '_productIdsFilter' => [1, '2'],
            ]],
            [
                [
                    // empty
                ]
            ],
        ];
    }

    /**
     * @dataProvider provideBuildFinalFilter
     */
    public function testBuildFinalFilter($options)
    {
        if (!empty($options)) {
            if (isset($options['_categoryFilter'])) {
                $this->service->applyCategoryFilter($options['_categoryFilter']);
            }
            if (isset($options['_channelFilter'])) {
                $this->service->applyChannelFilter($options['_channelFilter']);
            }
            if (isset($options['_colourFilter'])) {
                $this->service->applyColourFilter($options['_colourFilter']);
            }
            if (isset($options['_styleFilter'])) {
                $this->service->applyStyleFilter($options['_styleFilter']);
            }
            if (isset($options['_sizeFilter'])) {
                $this->service->applySizeFilter($options['_sizeFilter']);
            }
            if (isset($options['_priceFilter'])) {
                $this->service->applyPriceFilter($options['_priceFilter']);
            }
            if (isset($options['_spectrumIdFilter'])) {
                $this->service->applySpectrumIdFilter($options['_spectrumIdFilter']);
            }
            if (isset($options['_productIdsFilter'])) {
                $this->service->applyProductIdFilter($options['_productIdsFilter']);
            }
        }
        $this->service->buildFinalFilter();
    }

    public function provideSearchByParams()
    {
        return [
            [false],
            [
                [
                    'websiteID' => 1,
                    'languageID' => 1,
                    'from' => 0,
                    'size' => 20,
                    'searchString' => 'foo',
                    'filters' => [
                        'categories' => false,
                        'colours' => false,
                        'sizes' => false,
                        'styles' => false,
                        'price' => false,
                        'custom' => false,
                        'url' => false,
                        'productId' => false,
                        'spectrumId' => false,
                        'elasticSearchIds' => false,
                        'customOrder' => true,
                    ],
                ]
            ],
            [
                [
                    'websiteID' => 1,
                    'languageID' => 1,
                    'from' => -1,
                    'size' => 20,
                    'searchString' => 'foo',
                    'filters' => [
                        'categories' => false,
                        'colours' => false,
                        'sizes' => false,
                        'styles' => false,
                        'price' => false,
                        'custom' => false,
                        'url' => false,
                        'productId' => false,
                        'spectrumId' => false,
                        'elasticSearchIds' => false,
                        'customOrder' => true,
                    ],
                ]
            ],
            [
                [
                    'websiteID' => 1,
                    'languageID' => 1,
                    'from' => 0,
                    'size' => -1,
                    'searchString' => 'foo',
                    'filters' => [
                        'categories' => false,
                        'colours' => false,
                        'sizes' => false,
                        'styles' => false,
                        'price' => false,
                        'custom' => false,
                        'url' => false,
                        'productId' => false,
                        'spectrumId' => false,
                        'elasticSearchIds' => false,
                        'customOrder' => true,
                    ],
                ]
            ],
            [
                [
                    'websiteID' => 0,
                    'languageID' => 1,
                    'from' => 0,
                    'size' => 20,
                    'searchString' => 'foo',
                    'filters' => [
                        'categories' => false,
                        'colours' => false,
                        'sizes' => false,
                        'styles' => false,
                        'price' => false,
                        'custom' => false,
                        'url' => false,
                        'productId' => false,
                        'spectrumId' => false,
                        'elasticSearchIds' => false,
                        'customOrder' => true,
                    ],
                ]
            ],
            [
                [
                    'websiteID' => 1,
                    'languageID' => 0,
                    'from' => 0,
                    'size' => 20,
                    'searchString' => 'foo',
                    'filters' => [
                        'categories' => false,
                        'colours' => false,
                        'sizes' => false,
                        'styles' => false,
                        'price' => false,
                        'custom' => false,
                        'url' => false,
                        'productId' => false,
                        'spectrumId' => false,
                        'elasticSearchIds' => false,
                        'customOrder' => true,
                    ],
                ]
            ],
            [
                [
                    'websiteID' => 1,
                    'languageID' => 1,
                    'from' => 0,
                    'size' => 20,
                    'searchString' => 'foo',
                    'filters' => [
                        'categories' => false,
                        'colours' => false,
                        'sizes' => false,
                        'styles' => false,
                        'price' => false,
                        'custom' => false,
                        'url' => false,
                        'productId' => false,
                        'spectrumId' => false,
                        'elasticSearchIds' => [],
                        'customOrder' => true,
                    ],
                ]
            ],
            [
                [
                    'websiteID' => 1,
                    'languageID' => 1,
                    'from' => 0,
                    'size' => 20,
                    'searchString' => 'foo',
                    'filters' => [
                        'categories' => false,
                        'colours' => false,
                        'sizes' => false,
                        'styles' => false,
                        'price' => false,
                        'custom' => false,
                        'url' => false,
                        'productId' => false,
                        'spectrumId' => false,
                        'elasticSearchIds' => [1, 2],
                        'customOrder' => true,
                    ],
                ]
            ],
            [
                [
                    'websiteID' => 1,
                    'languageID' => 1,
                    'from' => 0,
                    'size' => 20,
                    'searchString' => 'foo',
                    'filters' => [
                        'categories' => false,
                        'colours' => false,
                        'sizes' => false,
                        'styles' => false,
                        'price' => false,
                        'custom' => false,
                        'url' => ['www.oliversweeney.com'],
                        'productId' => false,
                        'spectrumId' => false,
                        'elasticSearchIds' => false,
                        'customOrder' => true,
                    ],
                ]
            ],
            [
                [
                    'websiteID' => 1,
                    'languageID' => 1,
                    'from' => 0,
                    'size' => 20,
                    'searchString' => 'foo',
                    'filters' => [
                        'categories' => false,
                        'colours' => false,
                        'sizes' => false,
                        'styles' => false,
                        'price' => false,
                        'custom' => false,
                        'url' => ['www.oliversweeney.com'],
                        'productId' => [1, 2],
                        'spectrumId' => false,
                        'elasticSearchIds' => false,
                        'customOrder' => true,
                    ],
                ]
            ],
            [
                [
                    'websiteID' => 1,
                    'languageID' => 1,
                    'from' => 0,
                    'size' => 20,
                    'searchString' => 'foo',
                    'filters' => [
                        'categories' => false,
                        'colours' => false,
                        'sizes' => false,
                        'styles' => false,
                        'price' => false,
                        'custom' => false,
                        'url' => ['www.oliversweeney.com'],
                        'productId' => [1, 2],
                        'spectrumId' => [1, 2],
                        'elasticSearchIds' => false,
                        'customOrder' => true,
                    ],
                ]
            ],
            [
                [
                    'websiteID' => 1,
                    'languageID' => 1,
                    'from' => 0,
                    'size' => 20,
                    'searchString' => 'foo',
                    'filters' => [
                        'categories' => false,
                        'colours' => false,
                        'sizes' => ['gt' => false],
                        'styles' => false,
                        'price' => false,
                        'custom' => false,
                        'url' => ['www.oliversweeney.com'],
                        'productId' => [1, 2],
                        'spectrumId' => [1, 2],
                        'elasticSearchIds' => false,
                        'customOrder' => true,
                    ],
                ]
            ],
            [
                [
                    'websiteID' => 1,
                    'languageID' => 1,
                    'from' => 0,
                    'size' => 20,
                    'searchString' => 'foo',
                    'filters' => [
                        'categories' => false,
                        'colours' => false,
                        'sizes' => ['gt' => false],
                        'styles' => false,
                        'price' => ['gt' => 100],
                        'custom' => false,
                        'url' => ['www.oliversweeney.com'],
                        'productId' => [1, 2],
                        'spectrumId' => [1, 2],
                        'elasticSearchIds' => false,
                        'customOrder' => true,
                    ],
                ]
            ],
            [
                [
                    'websiteID' => 1,
                    'languageID' => 1,
                    'from' => 0,
                    'size' => 20,
                    'searchString' => 'foo',
                    'filters' => [
                        'categories' => false,
                        'colours' => ['red'],
                        'sizes' => ['gt' => false],
                        'styles' => false,
                        'price' => ['gt' => 100],
                        'custom' => false,
                        'url' => ['www.oliversweeney.com'],
                        'productId' => [1, 2],
                        'spectrumId' => [1, 2],
                        'elasticSearchIds' => false,
                        'customOrder' => true,
                    ],
                ]
            ],
            [
                [
                    'websiteID' => 1,
                    'languageID' => 1,
                    'from' => 0,
                    'size' => 20,
                    'searchString' => 'foo',
                    'filters' => [
                        // empty for tests Invalid structure exception
                    ],
                ]
            ],
        ];
    }

    /**
     * @dataProvider provideSearchByParams
     */
    public function testSearchByParams($paramsArray)
    {
        if ($paramsArray === false) {
            $this->setExpectedException('\Exception', 'No data was submitted');
        }  else {
            $this->setExpectedException('\Exception', 'Invalid structure for the document sent!');
        }

        $this->service->searchByParams($paramsArray);
    }
}
