<?php

namespace PrismElasticSearchTest\Service\Base;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Service\Base\ElasticSearchBaseIndexer;

/**
 * @author haniw
 */
class ElasticSearchBaseIndexerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ElasticSearchBaseIndexer
     */
    private $service;

    /**
     * @var \Elasticsearch\Client
     */
    private $elastic;

    protected function setUp()
    {
        $this->elastic = $this->getMockBuilder('Elasticsearch\Client')
                        ->disableOriginalConstructor()
                        ->getMock();
        $this->elasticSearchLogsMapper = $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchLogsMapper')
                                        ->disableOriginalConstructor()
                                        ->getMock();
        $this->elasticSearchIndexQueueMapper = $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchIndexQueueMapper')
                                     ->disableOriginalConstructor()
                                     ->getMock();
        $this->elasticSearchLastIndexMapper = $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchLastIndexMapper')
                                ->disableOriginalConstructor()
                                ->getMock();
        $this->abstractMapper = $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchAbstractMapper')
                                   ->disableOriginalConstructor()
                                   ->getMock();
        $this->_indexes = [
            1 => [
                1 => 'products_1',
                2 => 'products_2',
            ],
        ];
        $this->service = new ElasticSearchBaseIndexer(
            $this->elastic,
            $this->elasticSearchLogsMapper,
            $this->elasticSearchIndexQueueMapper,
            $this->elasticSearchLastIndexMapper,
            $this->abstractMapper,
            1,
            $this->_indexes,
            [
                'indexPrefix' => 'products_',
                'mappingType' => 'product',
                'numberOfShards' => 5,
                'numberOfReplicas' => 1,
                'lockFile' => '/tmp/elasticsearch_products.lck'
            ],
            []
        );
    }

    public function testGetAliasesNames()
    {
        $this->assertEquals([
            1 => [
                1 => 'products_1',
                2 => 'products_2',
            ],
        ], $this->service->getAliasesNames());
    }

    public function provideGetIndexDetails()
    {
        return [
            [false],
            ['products']
        ];
    }

    /**
     * @dataProvider provideGetIndexDetails
     */
    public function testGetIndexDetails($indexName)
    {
        if ($indexName !== false) {
            $params['index'][] = $indexName;
            $indices = $this->getMockBuilder('Elasticsearch\Namespaces\IndicesNamespace')
                            ->disableOriginalConstructor()
                            ->getMock();
            $this->elastic->expects($this->once())
                          ->method('indices')
                          ->willReturn($indices);

            $indices->expects($this->once())
                    ->method('getSettings')
                    ->with($params);
        }
        $this->service->getIndexDetails($indexName);
    }

    public function provideGetMappingDetails()
    {
        return [
            [false],
            [true],
        ];
    }

    /**
     * @dataProvider provideGetMappingDetails
     */
    public function testGetMappingDetails($empty)
    {
        $params = array();
        if ($empty) {
            foreach ($this->_indexes as $indexArray) {
                foreach ($indexArray as $indexName) {
                    $params['index'][] = $indexName;
                }
            }
        } else {
            $class = new \ReflectionClass('PrismElasticSearch\Service\Base\ElasticSearchBaseIndexer');
            $property = $class->getProperty('_indexes');
            $property->setAccessible(true);
            $property->setValue($this->service, []);
        }

        if (!empty($params['index'])) {
            $params['type'] = 'product';
            $indices = $this->getMockBuilder('Elasticsearch\Namespaces\IndicesNamespace')
                            ->disableOriginalConstructor()
                            ->getMock();
            $this->elastic->expects($this->once())
                          ->method('indices')
                          ->willReturn($indices);

            $indices->expects($this->once())
                    ->method('getMapping')
                    ->with($params);
        }

        $this->service->getMappingDetails();
    }

    public function provideSetGetLogsProcessId()
    {
        return [
            [1, 2],
            [-1, false],
        ];
    }

    /**
     * @dataProvider provideSetGetLogsProcessId
     */
    public function testSetGetLogsProcessId($lastprocessId, $result)
    {
        $this->elasticSearchLogsMapper->expects($this->once())
                                      ->method('getLastProcessId')
                                      ->with(1)
                                      ->willReturn($lastprocessId);
        $this->service->setLogsProcessId();
        $this->assertEquals($result, $this->service->getLogsProcessId());
    }

    public function testGetIndexLockedStatus()
    {
        $this->assertFalse($this->service->getIndexLockedStatus());
    }

    public function testDropIndexes()
    {
        $indexArray = $this->_indexes[1];
        if (is_array($indexArray)) {
            $key = 0;
            foreach ($indexArray as $indexName) {
                $params['index'] = $indexName;
                $indices = $this->getMockBuilder('Elasticsearch\Namespaces\IndicesNamespace')
                                ->disableOriginalConstructor()
                                ->getMock();
                $this->elastic->expects($this->at($key))
                              ->method('indices')
                              ->willReturn($indices);

                $indices->expects($this->once())
                        ->method('delete')
                        ->with($params);

                $key++;
            }
        }
        $this->service->dropIndexes($indexArray);
    }

    public function provideCheckIfDocumentExists()
    {
        return [
            [true],
            [false],
        ];
    }

    /**
     * @dataProvider provideCheckIfDocumentExists
     */
    public function testCheckIfDocumentExists($exists)
    {
        $this->elastic->expects($this->once())
                                   ->method('exists')
                                   ->with([
            'id' => 1,
            'index' => 1,
            'type' => 'product',
        ])->willReturn($exists);

        $this->assertEquals($exists, $this->service->checkIfDocumentExists(1, 1, 'product'));
    }
}
