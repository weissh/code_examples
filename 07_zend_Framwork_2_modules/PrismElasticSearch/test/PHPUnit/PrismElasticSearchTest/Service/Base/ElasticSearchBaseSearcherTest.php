<?php

namespace PrismElasticSearchTest\Service\Base;

use PrismElasticSearch\Service\Base\ElasticSearchBaseSearcher;
use PHPUnit_Framework_TestCase;

class ElasticSearchBaseSearcherTest extends PHPUnit_Framework_TestCase
{
    /** @var ElasticSearchBaseSearcher */
    private $abstractClass;

    /** @var \Elasticsearch\Client */
    private $elastic;

    public function setUp()
    {
        $this->elastic = $this->getMockBuilder('Elasticsearch\Client')
                        ->disableOriginalConstructor()
                        ->getMock();

        $indexesArray = [
            1 => [
                1 => 'blog_posts_1',
                2 => 'blog_posts_2',
            ],
        ];
        $this->abstractClass = $this->getMockForAbstractClass('PrismElasticSearch\Service\Base\ElasticSearchBaseSearcher', [$this->elastic, $indexesArray, 'blog', false]);
    }

    public function testInstance()
    {
        $this->assertInstanceOf('PrismElasticSearch\Service\Base\ElasticSearchBaseSearcher', $this->abstractClass);
    }

    public function testApplyCustomAttributeFilter()
    {
        $this->abstractClass->applyCustomAttributeFilter('size', 100);
    }

    public function testApplyCustomAttributeRangeFilter()
    {
        $this->abstractClass->applyCustomAttributeRangeFilter('size', [
            'gt' => false,
        ]);
    }

    public function provideSetSearchLocation()
    {
        return [
            [1, 1],
            [2, 2],
        ];
    }

    /**
     * @dataProvider provideSetSearchLocation
     */
    public function testSetSearchLocation($websiteId, $languageId)
    {
        if ($websiteId !== 1 && $languageId !== 1) { // check based on __construct setup
            $this->setExpectedException('\Exception');
        }
        $this->abstractClass->setSearchLocation($websiteId, $languageId);
    }

    public function provideDoSearch()
    {
        return [
            ['foo', 0, 20, true],
            ['foo', 0, 20, [1 => 1, 2 => 2]],
        ];
    }

    /**
     * @dataProvider provideDoSearch
     */
    public function testDoSearch($searchText, $from, $size, $customOrder)
    {
        $this->abstractClass->doSearch($searchText, $from, $size, $customOrder);
    }

    public function provideDoSearchForIpAddressSupplies()
    {
        return [
            ['62.254.216.61'],
            ['62.254.216.62'],
        ];
    }

    /**
     * @dataProvider provideDoSearchForIpAddressSupplies
     */
    public function testDoSearchWithHTTP_X_FORWARDED_FOR($ipaddress)
    {
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $ipaddress;
        $this->abstractClass->doSearch('foo', 0, 20, false);
    }
}
