<?php

namespace PrismElasticSearchTest\Service\Manager;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Service\Manager\ElasticSearchIndexingManager;
use PrismElasticSearch\Enum\ElasticSearchTypes;
use ReflectionClass;

/**
 * @author haniw
 */
class ElasticSearchIndexingManagerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ElasticSearchIndexingManager
     */
    private $service;

    /**
     * @var \PrismElasticSearch\Model\ElasticSearchLogsMapper
     */
    private $elasticSearchLogsMapper;

    /**
     * @var \PrismElasticSearch\Service\Index\ElasticSearchProductsIndexer
     */
    private $elasticProductIndexer;

    /**
     * @var \PrismElasticSearch\Service\Index\ElasticSearchWebsiteContentIndexer
     */
    private $elasticSearchWebsiteContentIndexer;

    protected function setUp()
    {
        $this->elasticSearchLogsMapper = $this->getMockBuilder('PrismElasticSearch\Model\ElasticSearchLogsMapper')
                                              ->disableOriginalConstructor()
                                              ->getMock();
        $this->elasticProductIndexer  = $this->getMockBuilder('PrismElasticSearch\Service\Base\ElasticSearchBaseIndexer')
                                     ->disableOriginalConstructor()
                                     ->getMock();
        $this->elasticSearchWebsiteContentIndexer  = $this->getMockBuilder('PrismElasticSearch\Service\Base\ElasticSearchBaseIndexer')
                                    ->disableOriginalConstructor()
                                    ->getMock();

        $this->service = new ElasticSearchIndexingManager($this->elasticSearchLogsMapper, $this->elasticProductIndexer, $this->elasticSearchWebsiteContentIndexer);
    }

    public function provideSetupCategory()
    {
        return [
            [ElasticSearchTypes::PRODUCTS],
            [ElasticSearchTypes::WEBSITE_CONTENT],
            ['foo'],
            [0],
        ];
    }

    /**
     * @dataProvider provideSetupCategory
     */
    public function testSetupCategory($indexType)
    {
        if ($indexType !== ElasticSearchTypes::PRODUCTS && $indexType !== ElasticSearchTypes::WEBSITE_CONTENT) {
            $this->setExpectedException('\Exception', "Unknown index category: ".$indexType."!");
        }
        $this->service->setupIndexCategory($indexType);
    }

    public function provideSetupIndexService()
    {
        return [
            [true],
            [false],
        ];
    }

    /**
     * @dataProvider provideSetupIndexService
     */
    public function test__call($setup)
    {
        if (! $setup) {
            $this->setExpectedException('\Exception', 'You must call the setupIndexCategory method first and specify an index type you want to access the index operations on!');
        } else {
            $class = new ReflectionClass('PrismElasticSearch\Service\Manager\ElasticSearchIndexingManager');
            $indexingProperty = $class->getProperty('_indexingService');
            $indexingProperty->setAccessible(true);
            $indexingProperty->setValue($this->service, $this->elasticProductIndexer);

            $this->service->setupIndexCategory(ElasticSearchTypes::PRODUCTS);

            $indexingProperty->getValue($this->service)
                  ->expects($this->any())
                  ->method('setLogsProcessId');
            $indexingProperty->getValue($this->service)->expects($this->once())
                                        ->method('setIndexLockedStatus')
                                        ->with(true);
        }

        $this->service->setIndexLockedStatus(true);
    }

    /**
     * @dataProvider provideSetupIndexService
     */
    public function test__callThrowException($setup)
    {
        if (! $setup) {
            $this->setExpectedException('\Exception', 'You must call the setupIndexCategory method first and specify an index type you want to access the index operations on!');
        } else {
            $class = new ReflectionClass('PrismElasticSearch\Service\Manager\ElasticSearchIndexingManager');
            $indexingProperty = $class->getProperty('_indexingService');
            $indexingProperty->setAccessible(true);
            $indexingProperty->setValue($this->service, $this->elasticProductIndexer);

            $this->service->setupIndexCategory(ElasticSearchTypes::PRODUCTS);

            try {
                $indexingProperty->getValue($this->service)
                      ->expects($this->any())
                      ->method('setLogsProcessId')
                      ->will($this->throwException(new \Exception()));
                $this->setExpectedException('\Exception');
            } catch (\Exception $e) {
                return;
            }
            $indexingProperty->getValue($this->service)->expects($this->any())
                                        ->method('setIndexLockedStatus')
                                        ->with(true);
        }

        $this->service->setIndexLockedStatus(true);
    }
}
