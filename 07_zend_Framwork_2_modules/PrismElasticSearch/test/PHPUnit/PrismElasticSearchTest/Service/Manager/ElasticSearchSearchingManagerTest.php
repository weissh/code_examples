<?php

namespace PrismElasticSearchTest\Service\Manager;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Service\Manager\ElasticSearchSearchingManager;
use PrismElasticSearch\Enum\ElasticSearchTypes;
use ReflectionClass;

/**
 * @author haniw
 */
class ElasticSearchSearchingManagerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ElasticSearchSearchingManager
     */
    private $service;

    /**
     * @var \Zend\ServiceLocator\ServiceLocatorInterface
     */
    private $serviceLocator;

    protected function setUp()
    {
        $this->elasticProductIndexer  = $this->getMockBuilder('PrismElasticSearch\Service\Base\ElasticSearchBaseIndexer')
                                     ->disableOriginalConstructor()
                                     ->getMock();
        $this->elasticSearchWebsiteContentIndexer  = $this->getMockBuilder('PrismElasticSearch\Service\Base\ElasticSearchBaseIndexer')
                                    ->disableOriginalConstructor()
                                    ->getMock();

        $this->serviceLocator = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface');
        $this->service = new ElasticSearchSearchingManager($this->serviceLocator);
    }

    public function provideSetupCategory()
    {
        return [
            [ElasticSearchTypes::PRODUCTS],
            [ElasticSearchTypes::WEBSITE_CONTENT],
            [ElasticSearchTypes::BLOG_POSTS],
            [0],
        ];
    }

    /**
     * @dataProvider provideSetupCategory
     */
    public function testSetupCategory($indexType)
    {
        if ($indexType !== ElasticSearchTypes::PRODUCTS
            && $indexType !== ElasticSearchTypes::WEBSITE_CONTENT
            && $indexType !== ElasticSearchTypes::BLOG_POSTS
        ) {
            $this->setExpectedException('\Exception', "Unknown index category: ".$indexType."!");
        }
        $this->service->setupIndexCategory($indexType);
    }

    public function provideSetupIndexService()
    {
        return [
            [true],
            [false],
        ];
    }

    /**
     * @dataProvider provideSetupIndexService
     */
    public function test__call($setup)
    {
        if (! $setup) {
            $this->setExpectedException('\Exception', 'You must call the setupIndexCategory method first and specify an index type you want to access the searching operations on!');
        } else {
            $this->serviceLocator->method('get')
                                 ->with('ElasticSearchProductsSearching')
                                 ->willReturn($this->elasticProductIndexer);

            $class = new ReflectionClass('PrismElasticSearch\Service\Manager\ElasticSearchSearchingManager');
            $indexingProperty = $class->getProperty('_indexingService');
            $indexingProperty->setAccessible(true);
            $indexingProperty->setValue($this->service, $this->elasticProductIndexer);

            $this->service->setupIndexCategory(ElasticSearchTypes::PRODUCTS);
            $indexingProperty->getValue($this->service)->expects($this->once())
                                        ->method('setIndexLockedStatus')
                                        ->with(true);
        }

        $this->service->setIndexLockedStatus(true);
    }

    /**
     * @dataProvider provideSetupIndexService
     */
    public function test__callThrowException($setup)
    {
        if (! $setup) {
            $this->setExpectedException('\Exception', 'You must call the setupIndexCategory method first and specify an index type you want to access the searching operations on!');
        } else {
            $this->serviceLocator->method('get')
                                 ->with('ElasticSearchProductsSearching')
                                 ->willReturn($this->elasticProductIndexer);

            $class = new ReflectionClass('PrismElasticSearch\Service\Manager\ElasticSearchSearchingManager');
            $indexingProperty = $class->getProperty('_indexingService');
            $indexingProperty->setAccessible(true);
            $indexingProperty->setValue($this->service, $this->elasticProductIndexer);

            $this->service->setupIndexCategory(ElasticSearchTypes::PRODUCTS);

            try {
                $indexingProperty->getValue($this->service)->expects($this->any())
                                            ->method('setIndexLockedStatus')
                                            ->with(true)
                                            ->will($this->throwException(new \Exception()));
                $this->setExpectedException('\Exception');
            } catch (\Exception $e) {
                return;
            }
        }

        $this->service->setIndexLockedStatus(true);
    }
}
