<?php

namespace PrismElasticSearchTest\Service;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Service\ElasticSearchWebsiteContentSearching;
use ReflectionClass;

/**
 * @author haniw
 */
class ElasticSearchWebsiteContentSearchingTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ElasticSearchBlogPostsSearching
     */
    private $service;

    /**
     * @var \Elasticsearch\Client
     */
    private $elastic;

    protected function setUp()
    {
        $indexesArray = [
            1 => [
                1 => 'website_content_2',
                2 => 'website_content_2',
            ],
        ];
        $this->elastic = $this->getMockBuilder('Elasticsearch\Client')
                        ->disableOriginalConstructor()
                        ->getMock();
        $this->service = new ElasticSearchWebsiteContentSearching($this->elastic, $indexesArray, 'content');
    }


    public function provideBuildParamsArray()
    {
        return [
            [null, [
                'websiteID' => 1,
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => false,
            ]],
            ['', [
                'websiteID' => 1,
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => false,
            ]],
            [0, [
                'websiteID' => 1,
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => false,
            ]],
            [false, [
                'websiteID' => 1,
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => false,
            ]],
            ['foo', [
                'websiteID' => 1,
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => 'foo',
            ]]
        ];
    }

    /**
     * @dataProvider provideBuildParamsArray
     */
    public function testBuildParamsArray($searchString, $result)
    {
        $this->assertEquals($result, $this->service->buildParamsArray(1, 1, $searchString, [], 0, 20, true));
    }


    public function testBuildFinalFilter()
    {
        $class = new ReflectionClass('PrismElasticSearch\Service\ElasticSearchWebsiteContentSearching');
        $property = $class->getProperty('_filters');
        $property->setAccessible(true);
        $property->setValue($this->service, true);

        $this->assertTrue($property->getValue($this->service));
        $this->service->buildFinalFilter();
        $this->assertFalse($property->getValue($this->service));
    }

    public function testResetFilters()
    {
        $class = new ReflectionClass('PrismElasticSearch\Service\ElasticSearchWebsiteContentSearching');
        $property = $class->getProperty('_filters');
        $property->setAccessible(true);
        $property->setValue($this->service, true);

        $this->assertTrue($property->getValue($this->service));
        $this->service->resetFilters();
        $this->assertFalse($property->getValue($this->service));
    }

    public function provideSearchByParams()
    {
        return [
            [false],
            [[
                'websiteID' => 1,
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => 'foo',
            ]],
            [[
                'websiteID' => 1,
                'languageID' => 0,
                'from' => 0,
                'size' => 20,
                'searchString' => 'foo',
            ]],
            [
                'fail'
            ],
            [[
                'websiteID' => 0,
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => 'foo',
            ]],
            [[
                'websiteID' => 1,
                'languageID' => 1,
                'from' => -1,
                'size' => 20,
                'searchString' => 'foo',
            ]],
            [[
                'websiteID' => 1,
                'languageID' => 1,
                'from' => 1,
                'size' => 0,
                'searchString' => 'foo',
            ]],
            [[
                'websiteID' => 1,
                'languageID' => 1,
                'from' => 0,
                'size' => -1,
                'searchString' => 'foo',
            ]]
        ];
    }

    /**
     * @dataProvider provideSearchByParams
     */
    public function testSearchByParams($paramsArray)
    {
        if ($paramsArray === false) {
            $this->setExpectedException('\Exception', 'No data was submitted');
        } elseif (!is_array($paramsArray)
                || (
                is_array($paramsArray) && (
                    !isset($paramsArray['websiteID']) ||
                    !isset($paramsArray['languageID']) ||
                    !isset($paramsArray['from']) ||
                    !isset($paramsArray['size']) ||
                    !isset($paramsArray['searchString'])
                )
        )) {
            $this->setExpectedException('\Exception', 'Invalid structure for the document sent!');
        } else {
            if (empty($paramsArray['websiteID'])) {
                $this->setExpectedException('\Exception', 'Invalid websiteID specified as a parameter!');
            }

            if (empty($paramsArray['languageID'])) {
                $this->setExpectedException('\Exception', 'Invalid languageID specified as a parameter!');
            }

            if ($paramsArray['from'] < 0) {
                $this->setExpectedException('\Exception', 'Invalid \'from\' parameter specified');
            }
            if ($paramsArray['size'] <= 0) {
                $this->setExpectedException('\Exception', 'Invalid \'size\' parameter specified');
            }
        }

        $this->service->searchByParams($paramsArray);
    }
}
