<?php

namespace PrismElasticSearchTest\Service;

use PHPUnit_Framework_TestCase;
use PrismElasticSearch\Service\ElasticSearchBlogPostsSearching;
use ReflectionClass;

/**
 * @author haniw
 */
class ElasticSearchBlogPostsSearchingTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ElasticSearchBlogPostsSearching
     */
    private $service;

    /**
     * @var \Elasticsearch\Client
     */
    private $elastic;

    protected function setUp()
    {
        $indexesArray = [
            1 => [
                1 => 'blog_posts_1',
                2 => 'blog_posts_2',
            ],
        ];
        $this->elastic = $this->getMockBuilder('Elasticsearch\Client')
                        ->disableOriginalConstructor()
                        ->getMock();
        $this->service = new ElasticSearchBlogPostsSearching($this->elastic, $indexesArray, 'post');
    }

    public function provideBuildParamsArray()
    {
        return [
            [null, 1, [
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => false,
            ]],
            ['', 1, [
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => false,
            ]],
            [0, 1, [
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => false,
            ]],
            [false, 1, [
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => false,
            ]],
            ['foo', 1, [
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => 'foo',
            ]],
            ['foo', 2, [
                'languageID' => 2,
                'from' => 0,
                'size' => 20,
                'searchString' => 'foo',
            ]]
        ];
    }

    /**
     * @dataProvider provideBuildParamsArray
     */
    public function testBuildParamsArray($searchString, $languageId, $result)
    {
        $this->assertEquals($result, $this->service->buildParamsArray(1, $languageId, $searchString, [], 0, 20, true));
    }

    public function testBuildFinalFilter()
    {
        $class = new ReflectionClass('PrismElasticSearch\Service\ElasticSearchBlogPostsSearching');
        $property = $class->getProperty('_filters');
        $property->setAccessible(true);
        $property->setValue($this->service, true);

        $this->assertTrue($property->getValue($this->service));
        $this->service->buildFinalFilter();
        $this->assertFalse($property->getValue($this->service));
    }

    public function testResetFilters()
    {
        $class = new ReflectionClass('PrismElasticSearch\Service\ElasticSearchBlogPostsSearching');
        $property = $class->getProperty('_filters');
        $property->setAccessible(true);
        $property->setValue($this->service, true);

        $this->assertTrue($property->getValue($this->service));
        $this->service->resetFilters();
        $this->assertFalse($property->getValue($this->service));
    }

    public function provideSearchByParams()
    {
        return [
            [false],
            [[
                'languageID' => 1,
                'from' => 0,
                'size' => 20,
                'searchString' => 'foo',
            ]],
            [[
                'languageID' => 2,
                'from' => 0,
                'size' => 20,
                'searchString' => 'foo',
            ]],
            [
                'fail'
            ],
            [[
                'languageID' => 0,
                'from' => 0,
                'size' => 20,
                'searchString' => 'foo',
            ]],
            [[
                'languageID' => 1,
                'from' => -1,
                'size' => 20,
                'searchString' => 'foo',
            ]],
            [[
                'languageID' => 1,
                'from' => 1,
                'size' => 0,
                'searchString' => 'foo',
            ]],
            [[
                'languageID' => 1,
                'from' => 0,
                'size' => -1,
                'searchString' => 'foo',
            ]]
        ];
    }

    /**
     * @dataProvider provideSearchByParams
     */
    public function testSearchByParams($paramsArray)
    {
        if ($paramsArray === false) {
            $this->setExpectedException('\Exception', 'No data was submitted');
        } elseif (!is_array($paramsArray)
                || (
                is_array($paramsArray) && (
                    !isset($paramsArray['languageID']) ||
                    !isset($paramsArray['from']) ||
                    !isset($paramsArray['size']) ||
                    !isset($paramsArray['searchString'])
                )
        )) {
            $this->setExpectedException('\Exception', 'Invalid structure for the document sent!');
        } else {
            if (empty($paramsArray['languageID'])) {
                $this->setExpectedException('\Exception', 'Invalid languageID specified as a parameter!');
            }

            if ($paramsArray['from'] < 0) {
                $this->setExpectedException('\Exception', 'Invalid \'from\' parameter specified');
            }
            if ($paramsArray['size'] <= 0) {
                $this->setExpectedException('\Exception', 'Invalid \'size\' parameter specified');
            }
        }

        $this->service->searchByParams($paramsArray);
    }
}
