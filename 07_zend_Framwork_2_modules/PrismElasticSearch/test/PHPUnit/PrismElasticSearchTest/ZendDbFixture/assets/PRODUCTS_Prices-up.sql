DROP TABLE IF EXISTS `PRODUCTS_Prices`;
CREATE TABLE IF NOT EXISTS `PRODUCTS_Prices` (
  `productPriceId` int(11) NOT NULL AUTO_INCREMENT,
  `productIdFrom` enum('PRODUCTS','PRODUCTS_R_ATTRIBUTES') DEFAULT NULL,
  `productOrVariantId` int(10) unsigned DEFAULT NULL,
  `regionId` int(10) unsigned NOT NULL DEFAULT '0',
  `currencyId` int(10) unsigned NOT NULL DEFAULT '0',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `nowPrice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `wasPrice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `costPrice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tradingCostPrice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `wholesalePrice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `freightPrice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `dutyPrice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `designPrice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `packagingPrice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL DEFAULT 'ACTIVE',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`productPriceId`),
  KEY `FK_PRODUCTS_Prices_PRODUCTS` (`productIdFrom`),
  KEY `regionId` (`regionId`),
  KEY `currencyId` (`currencyId`),
  KEY `productAttributeId` (`productOrVariantId`),
  KEY `productOrVariantId` (`productOrVariantId`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=82657 ;

--
-- Dumping data for table `PRODUCTS_Prices`
--

INSERT INTO `PRODUCTS_Prices` (`productPriceId`, `productIdFrom`, `productOrVariantId`, `regionId`, `currencyId`, `price`, `nowPrice`, `wasPrice`, `costPrice`, `tradingCostPrice`, `wholesalePrice`, `freightPrice`, `dutyPrice`, `designPrice`, `packagingPrice`, `status`, `created`, `modified`) VALUES
(1, 'PRODUCTS', 1, 1, 1, '79.00', '79.00', '249.00', '62.94', '64.50', '64.50', '1.00', '0.00', '0.00', '3.30', 'ACTIVE', '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(2, 'PRODUCTS', 1, 1, 2, '79.00', '135.00', '411.00', '62.94', '64.50', '64.50', '1.00', '0.00', '0.00', '3.30', 'ACTIVE', '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(3, 'PRODUCTS', 1, 1, 3, '79.00', '95.00', '299.00', '62.94', '64.50', '64.50', '1.00', '0.00', '0.00', '3.30', 'ACTIVE', '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(4, 'PRODUCTS', 1, 1, 4, '79.00', '95.00', '299.00', '62.94', '64.50', '64.50', '1.00', '0.00', '0.00', '3.30', 'ACTIVE', '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(5, 'PRODUCTS', 1, 1, 5, '79.00', '95.00', '299.00', '62.94', '64.50', '64.50', '1.00', '0.00', '0.00', '3.30', 'ACTIVE', '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(6, 'PRODUCTS_R_ATTRIBUTES', 1, 1, 1, '79.00', '79.00', '249.00', '62.94', '64.50', '64.50', '1.00', '0.00', '0.00', '3.30', 'ACTIVE', '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(7, 'PRODUCTS_R_ATTRIBUTES', 1, 1, 2, '79.00', '135.00', '411.00', '62.94', '64.50', '64.50', '1.00', '0.00', '0.00', '3.30', 'ACTIVE', '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(8, 'PRODUCTS_R_ATTRIBUTES', 1, 1, 3, '79.00', '95.00', '299.00', '62.94', '64.50', '64.50', '1.00', '0.00', '0.00', '3.30', 'ACTIVE', '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(9, 'PRODUCTS_R_ATTRIBUTES', 1, 1, 4, '79.00', '95.00', '299.00', '62.94', '64.50', '64.50', '1.00', '0.00', '0.00', '3.30', 'ACTIVE', '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(10, 'PRODUCTS_R_ATTRIBUTES', 1, 1, 5, '79.00', '95.00', '299.00', '62.94', '64.50', '64.50', '1.00', '0.00', '0.00', '3.30', 'ACTIVE', '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(11, 'PRODUCTS_R_ATTRIBUTES', 2, 1, 1, '79.00', '79.00', '249.00', '62.94', '64.50', '64.50', '1.00', '0.00', '0.00', '3.30', 'ACTIVE', '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(12, 'PRODUCTS_R_ATTRIBUTES', 2, 1, 2, '79.00', '135.00', '411.00', '62.94', '64.50', '64.50', '1.00', '0.00', '0.00', '3.30', 'ACTIVE', '2015-01-28 17:33:26', '2015-01-28 10:33:30');
