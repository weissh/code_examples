DROP TABLE IF EXISTS `SIZES_Accessories`;
CREATE TABLE IF NOT EXISTS `SIZES_Accessories` (
  `sizeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('KIDS','WOMEN','MEN') COLLATE utf8_unicode_ci NOT NULL,
  `uk` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `us` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eur` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sizeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `SIZES_Accessories`
--

INSERT INTO `SIZES_Accessories` (`sizeId`, `type`, `uk`, `us`, `eur`, `priority`) VALUES
(1, 'MEN', 'S', 'S', '6', 1),
(2, 'MEN', 'M', 'M', '8', 2),
(3, 'MEN', 'L', 'L', '11', 3),
(4, 'MEN', 'XL', 'XL', '12', 4),
(5, 'MEN', 'XXL', 'XXL', '14', 5),
(6, 'MEN', 'XXXL', 'XXXL', '16', 6),
(7, 'MEN', 'S/M', 'S/M', '6-8', 7),
(8, 'MEN', 'M/L', 'M/L', '9-11', 8),
(9, 'MEN', 'L/XL', 'L/XL', '10-12', 9),
(15, 'MEN', 'ONE SIZE', 'ONE SIZE', 'ONE SIZE', 10);
