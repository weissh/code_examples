DROP TABLE IF EXISTS `ELASTICSEARCH_Logs`;

CREATE TABLE IF NOT EXISTS `ELASTICSEARCH_Logs` (
  `logId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `indexType` int(11) NOT NULL,
  `processId` int(10) unsigned NOT NULL,
  `lastStatusUpdate` text NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`logId`),
  UNIQUE KEY `productId` (`logId`) USING BTREE,
  KEY `indexType` (`indexType`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `ELASTICSEARCH_Logs` (`logId`, `indexType`, `processId`, `lastStatusUpdate`, `createdAt`) VALUES
    (1, 1, 1, '/var/www/prism-cms/releases/20150130150547/module/PrismElasticSearch/src/PrismElasticSearch/Service/Base/ElasticSearchBaseIndexer.php:807 - Lock file created', '2015-01-30 15:44:13');

INSERT INTO `ELASTICSEARCH_Logs` (`logId`, `indexType`, `processId`, `lastStatusUpdate`, `createdAt`) VALUES
    (1, 3, 4, '/var/www/prism-cms/releases/20150203153520/module/PrismElasticSearch/src/PrismElasticSearch/Service/Base/ElasticSearchBaseIndexer.php:323 - Failed to retrieve any results!', '2015-02-05 14:38:02');
