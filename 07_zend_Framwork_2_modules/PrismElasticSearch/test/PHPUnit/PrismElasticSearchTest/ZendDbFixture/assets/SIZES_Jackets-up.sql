DROP TABLE IF EXISTS `SIZES_Jackets`;
CREATE TABLE IF NOT EXISTS `SIZES_Jackets` (
  `sizeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('KIDS','WOMEN','MEN') COLLATE utf8_unicode_ci NOT NULL,
  `uk` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `us` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eur` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sizeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=10 ;

--
-- Dumping data for table `SIZES_Jackets`
--

INSERT INTO `SIZES_Jackets` (`sizeId`, `type`, `uk`, `us`, `eur`, `priority`) VALUES
(1, 'MEN', '38', '38', '38', 1),
(2, 'MEN', '39', '39', '39', 2),
(3, 'MEN', '40', '40', '40', 3),
(4, 'MEN', '41', '41', '41', 4),
(5, 'MEN', '42', '42', '42', 5),
(6, 'MEN', '43', '43', '43', 6),
(7, 'MEN', '44', '44', '44', 7),
(8, 'MEN', '45', '45', '45', 8),
(9, 'MEN', '46', '46', '46', 9);
