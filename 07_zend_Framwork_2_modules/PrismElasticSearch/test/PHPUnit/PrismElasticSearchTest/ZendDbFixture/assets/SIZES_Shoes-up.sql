DROP TABLE IF EXISTS `SIZES_Shoes`;
CREATE TABLE IF NOT EXISTS `SIZES_Shoes` (
  `sizeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('KIDS','WOMEN','MEN','UNISEX') COLLATE utf8_unicode_ci NOT NULL,
  `uk` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `us` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eur` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sizeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=84 ;

--
-- Dumping data for table `SIZES_Shoes`
--

INSERT INTO `SIZES_Shoes` (`sizeId`, `type`, `uk`, `us`, `eur`, `priority`) VALUES
(11, 'MEN', '2.0', '3.0', '34.0', 1),
(13, 'MEN', '2.5', '3.5', '34.5', 2),
(15, 'MEN', '3.0', '4.0', '35.0', 3),
(17, 'MEN', '3.5', '4.5', '35.5', 4),
(19, 'MEN', '4.0', '5.0', '36.5', 5),
(21, 'MEN', '4.5', '5.5', '37.0', 6),
(23, 'MEN', '5.0', '6.0', '37.5', 7),
(25, 'MEN', '5.5', '6.5', '38.0', 8),
(27, 'MEN', '6.0', '7.0', '40.0', 9);
