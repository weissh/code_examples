DROP TABLE IF EXISTS `ELASTICSEARCH_Mapping_Details`;
CREATE TABLE IF NOT EXISTS `ELASTICSEARCH_Mapping_Details` (
  `elasticSearchType` enum('3','2','1') NOT NULL DEFAULT '1',
  `fieldId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fieldParent` int(11) unsigned NOT NULL DEFAULT '0',
  `fieldName` varchar(60) NOT NULL,
  `fieldType` enum('nested','object','integer','float','string') NOT NULL DEFAULT 'string',
  `indexable` enum('not_analyzed','yes','no') DEFAULT 'yes',
  `includeInParent` tinyint(1) NOT NULL DEFAULT '0',
  `searchPriority` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`fieldId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ELASTICSEARCH_Mapping_Details`
--

INSERT INTO `ELASTICSEARCH_Mapping_Details` (`elasticSearchType`, `fieldId`, `fieldParent`, `fieldName`, `fieldType`, `indexable`, `includeInParent`, `searchPriority`) VALUES
    ('1', 1, 0, 'productId', 'integer', 'not_analyzed', 0, 1),
    ('1', 2, 0, 'variantId', 'integer', 'no', 0, 1),
    ('1', 3, 0, 'spectrumId', 'integer', 'not_analyzed', 0, 1),
    ('1', 4, 0, 'channel', 'string', 'not_analyzed', 0, 1),
    ('1', 5, 0, 'categories', 'nested', 'yes', 1, 1),
    ('1', 6, 5, 'id', 'integer', 'yes', 0, 1),
    ('1', 7, 5, 'name', 'string', 'yes', 0, 1),
    ('1', 8, 5, 'tags', 'string', 'yes', 0, 1),
    ('1', 9, 5, 'tags', 'string', 'no', 1, 1),
    ('1', 10, 5, 'tags', 'string', 'no', 1, 2),
    ('1', 11, 0, 'productId', 'integer', 'not_analyzed', 0, 2);
