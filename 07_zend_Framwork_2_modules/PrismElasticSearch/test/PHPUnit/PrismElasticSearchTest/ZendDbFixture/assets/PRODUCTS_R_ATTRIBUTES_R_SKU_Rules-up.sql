DROP TABLE IF EXISTS `PRODUCTS_R_ATTRIBUTES_R_SKU_Rules`;
CREATE TABLE IF NOT EXISTS `PRODUCTS_R_ATTRIBUTES_R_SKU_Rules` (
  `productAttributeSkuRuleId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `productAttributeId` int(10) unsigned NOT NULL,
  `skuRuleId` int(10) unsigned NOT NULL,
  `skuRuleTable` varchar(70) NOT NULL COMMENT 'Table to get the sku details from',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`productAttributeSkuRuleId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29995 ;

--
-- Dumping data for table `PRODUCTS_R_ATTRIBUTES_R_SKU_Rules`
--

INSERT INTO `PRODUCTS_R_ATTRIBUTES_R_SKU_Rules` (`productAttributeSkuRuleId`, `productAttributeId`, `skuRuleId`, `skuRuleTable`, `created`, `modified`) VALUES
(1, 1, 6, 'COLOURS_Groups', '2015-01-28 17:33:26', '0000-00-00 00:00:00'),
(2, 1, 27, 'SIZES_Shoes', '2015-01-28 17:33:26', '0000-00-00 00:00:00'),
(3, 2, 6, 'COLOURS_Groups', '2015-01-28 17:33:26', '0000-00-00 00:00:00'),
(4, 2, 29, 'SIZES_Shoes', '2015-01-28 17:33:26', '0000-00-00 00:00:00'),
(5, 3, 6, 'COLOURS_Groups', '2015-01-28 17:33:26', '0000-00-00 00:00:00'),
(6, 3, 31, 'SIZES_Shoes', '2015-01-28 17:33:26', '0000-00-00 00:00:00'),
(7, 4, 6, 'COLOURS_Groups', '2015-01-28 17:33:26', '0000-00-00 00:00:00'),
(8, 4, 33, 'SIZES_Shoes', '2015-01-28 17:33:26', '0000-00-00 00:00:00');
