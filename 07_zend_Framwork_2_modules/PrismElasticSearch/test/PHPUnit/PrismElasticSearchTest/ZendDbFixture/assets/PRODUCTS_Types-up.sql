DROP TABLE IF EXISTS `PRODUCTS_Types`;
CREATE TABLE IF NOT EXISTS `PRODUCTS_Types` (
  `productTypeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` enum('INACTIVE','ACTIVE','DELETED') NOT NULL DEFAULT 'INACTIVE',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`productTypeId`),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `PRODUCTS_Types`
--

INSERT INTO `PRODUCTS_Types` (`productTypeId`, `status`, `created`, `modified`) VALUES
(1, 'ACTIVE', '2014-08-08 18:57:49', '2014-08-08 12:00:05'),
(2, 'ACTIVE', '2014-08-08 18:57:53', '2014-10-01 11:15:24'),
(3, 'ACTIVE', '2014-09-08 12:35:33', '2014-09-08 05:32:50');
