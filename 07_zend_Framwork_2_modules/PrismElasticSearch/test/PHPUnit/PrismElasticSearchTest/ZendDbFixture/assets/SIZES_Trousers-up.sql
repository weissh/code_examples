
DROP TABLE IF EXISTS `SIZES_Trousers`;
CREATE TABLE IF NOT EXISTS `SIZES_Trousers` (
  `sizeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('KIDS','WOMEN','MEN') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'MEN',
  `uk` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `us` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `eur` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `priority` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`sizeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=73 ;

--
-- Dumping data for table `SIZES_Trousers`
--

INSERT INTO `SIZES_Trousers` (`sizeId`, `type`, `uk`, `us`, `eur`, `priority`) VALUES
(1, 'MEN', 'W28" 30L"', 'W28" 30L"', 'W28" 30L"', 1),
(2, 'MEN', 'W29" 30L"', 'W29" 30L"', 'W29" 30L"', 1),
(3, 'MEN', 'W30" 30L"', 'W30" 30L"', 'W30" 30L"', 1),
(4, 'MEN', 'W31" 30L"', 'W31" 30L"', 'W31" 30L"', 1),
(5, 'MEN', 'W32" 30L"', 'W32" 30L"', 'W32" 30L"', 1),
(6, 'MEN', 'W33" 30L"', 'W33" 30L"', 'W33" 30L"', 1),
(7, 'MEN', 'W34" 30L"', 'W34" 30L"', 'W34" 30L"', 1),
(8, 'MEN', 'W35" 30L"', 'W35" 30L"', 'W35" 30L"', 1),
(9, 'MEN', 'W36" 30L"', 'W36" 30L"', 'W36" 30L"', 1),
(10, 'MEN', 'W37" 30L"', 'W37" 30L"', 'W37" 30L"', 1),
(11, 'MEN', 'W38" 30L"', 'W38" 30L"', 'W38" 30L"', 1),
(12, 'MEN', 'W39" 30L"', 'W39" 30L"', 'W39" 30L"', 1);
