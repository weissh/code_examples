DROP TABLE IF EXISTS `COLOURS_Groups`;

CREATE TABLE IF NOT EXISTS `COLOURS_Groups` (
  `colourGroupId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupCode` char(30) NOT NULL,
  `hasImage` tinyint(1) NOT NULL DEFAULT '0',
  `hex` varchar(7) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `imageId` int(11) DEFAULT NULL,
  PRIMARY KEY (`colourGroupId`),
  UNIQUE KEY `colourGroupId` (`colourGroupId`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `COLOURS_Groups`
--

INSERT INTO `COLOURS_Groups` (`colourGroupId`, `groupCode`, `hasImage`, `hex`, `enabled`, `imageId`) VALUES
(1, 'NOC', 0, '#FFFFFF', 1, 0),
(2, 'AUB', 0, '#4D013C', 1, 0),
(3, 'BEI', 0, '#D1A248', 1, 0),
(4, 'BLK', 0, '#000000', 1, 0);
