DROP TABLE IF EXISTS `SIZES_Belts`;
CREATE TABLE IF NOT EXISTS `SIZES_Belts` (
  `sizeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('KIDS','WOMEN','MEN') COLLATE utf8_unicode_ci NOT NULL,
  `uk` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `us` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eur` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sizeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=8 ;

--
-- Dumping data for table `SIZES_Belts`
--

INSERT INTO `SIZES_Belts` (`sizeId`, `type`, `uk`, `us`, `eur`, `priority`) VALUES
(1, 'MEN', 'S', 'S', 'S', 1),
(2, 'MEN', 'M', 'M', 'M', 2),
(3, 'MEN', 'L', 'L', 'L', 3),
(4, 'MEN', 'XL', 'XL', 'XL', 4),
(5, 'MEN', 'XXL', 'XXL', 'XXL', 5),
(6, 'MEN', 'XXXL', 'XXXL', 'XXXL', 6),
(7, 'MEN', 'ONE SIZE', 'ONE SIZE', 'ONE SIZE', 7);
