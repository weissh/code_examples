DROP TABLE IF EXISTS `SIZES_Shirts`;
CREATE TABLE IF NOT EXISTS `SIZES_Shirts` (
  `sizeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('KIDS','WOMEN','MEN') COLLATE utf8_unicode_ci NOT NULL,
  `uk` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `us` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eur` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sizeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT AUTO_INCREMENT=20 ;

--
-- Dumping data for table `SIZES_Shirts`
--

INSERT INTO `SIZES_Shirts` (`sizeId`, `type`, `uk`, `us`, `eur`, `priority`) VALUES
(1, 'MEN', '15', '15', '15', 1),
(2, 'MEN', '15.5', '15.5', '15.5', 2),
(3, 'MEN', '16', '16', '16', 3),
(4, 'MEN', '16.5', '16.5', '16.5', 4),
(5, 'MEN', '17', '17', '17', 5),
(13, 'MEN', 'S', 'S', 'S', 6),
(14, 'MEN', 'M', 'M', 'M', 7),
(15, 'MEN', 'L', 'L', 'L', 8),
(16, 'MEN', 'XL', 'XL', 'XL', 9),
(17, 'MEN', 'XXL', 'XXL', 'XXL', 10),
(18, 'MEN', 'XXXL', 'XXXL', 'XXXL', 11),
(19, 'MEN', 'ONE SIZE', 'ONE SIZE', 'ONE SIZE', 12);
