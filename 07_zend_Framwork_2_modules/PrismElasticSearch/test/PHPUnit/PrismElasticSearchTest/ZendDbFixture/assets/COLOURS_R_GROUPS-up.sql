DROP TABLE IF EXISTS `COLOURS_R_GROUPS`;
CREATE TABLE IF NOT EXISTS `COLOURS_R_GROUPS` (
  `colourGroupId` int(10) unsigned NOT NULL,
  `colourId` int(10) unsigned NOT NULL,
  `coloursRGroupsId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`coloursRGroupsId`),
  KEY `colourGroupId` (`colourGroupId`) USING BTREE,
  KEY `colourId` (`colourId`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=499 ;

--
-- Dumping data for table `COLOURS_R_GROUPS`
--

INSERT INTO `COLOURS_R_GROUPS` (`colourGroupId`, `colourId`, `coloursRGroupsId`) VALUES
(4, 4, 4),
(5, 5, 130),
(6, 6, 131),
(7, 7, 132),
(8, 8, 133),
(10, 10, 135),
(11, 11, 136),
(12, 12, 137),
(13, 13, 138),
(14, 14, 139),
(15, 15, 140);
