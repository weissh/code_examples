
DROP TABLE IF EXISTS `COLOURS_Groups_Definitions`;
CREATE TABLE IF NOT EXISTS `COLOURS_Groups_Definitions` (
  `colourGroupDefinitionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `colourGroupId` int(10) unsigned NOT NULL,
  `languageId` int(10) unsigned NOT NULL,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`colourGroupDefinitionId`),
  KEY `languageId` (`languageId`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `colourGroupId` (`colourGroupId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=155 ;

--
-- Dumping data for table `COLOURS_Groups_Definitions`
--

INSERT INTO `COLOURS_Groups_Definitions` (`colourGroupDefinitionId`, `colourGroupId`, `languageId`, `name`) VALUES
(1, 1, 1, 'No Colour'),
(2, 2, 1, 'Aubergine'),
(3, 3, 1, 'Beige'),
(4, 4, 1, 'Black'),
(5, 5, 1, 'Blue'),
(6, 6, 1, 'Brown'),
(7, 7, 1, 'Burgundy'),
(8, 8, 1, 'Camel');
