DROP TABLE IF EXISTS `PRODUCTS_R_ATTRIBUTES`;
CREATE TABLE IF NOT EXISTS `PRODUCTS_R_ATTRIBUTES` (
  `productAttributeId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `productId` int(10) unsigned NOT NULL,
  `manufacturerId` int(10) unsigned DEFAULT NULL,
  `taxId` int(10) unsigned NOT NULL,
  `quantity` int(10) DEFAULT '0',
  `ean13` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `ecoTax` decimal(17,2) NOT NULL DEFAULT '0.00',
  `sku` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `supplierReference` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','DELETED') CHARACTER SET utf8 NOT NULL DEFAULT 'INACTIVE',
  `indexed` tinyint(1) NOT NULL DEFAULT '0',
  `isDefaultProduct` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`productAttributeId`),
  KEY `manufacturerId` (`manufacturerId`),
  KEY `taxId` (`taxId`),
  KEY `created` (`created`),
  KEY `FK_PRODUCTS_R_ATTRIBUTES_PRODUCTS` (`productId`),
  KEY `sku` (`sku`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `PRODUCTS_R_ATTRIBUTES`
--

INSERT INTO `PRODUCTS_R_ATTRIBUTES` (`productAttributeId`, `productId`, `manufacturerId`, `taxId`, `quantity`, `ean13`, `ecoTax`, `sku`, `supplierReference`, `status`, `indexed`, `isDefaultProduct`, `created`, `modified`) VALUES
(1, 1, 1, 0, 0, '5055347347580', '0.00', 'ALESDEBRW060', 'BER01', 'INACTIVE', 0, 1, '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(2, 1, 1, 0, 0, '5055347347597', '0.00', 'ALESDEBRW065', 'BER01', 'INACTIVE', 0, 0, '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(3, 1, 1, 0, 0, '5055347347603', '0.00', 'ALESDEBRW070', 'BER01', 'INACTIVE', 0, 0, '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(4, 1, 1, 0, 0, '5055347347610', '0.00', 'ALESDEBRW075', 'BER01', 'INACTIVE', 0, 0, '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(5, 1, 1, 0, 0, '5055347347627', '0.00', 'ALESDEBRW080', 'BER01', 'INACTIVE', 0, 0, '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(6, 1, 1, 0, 0, '5055347347634', '0.00', 'ALESDEBRW085', 'BER01', 'INACTIVE', 0, 0, '2015-01-28 17:33:26', '2015-01-28 10:33:30'),
(7, 1, 1, 0, 0, '5055347347641', '0.00', 'ALESDEBRW090', 'BER01', 'INACTIVE', 0, 0, '2015-01-28 17:33:26', '2015-01-28 10:33:30');
