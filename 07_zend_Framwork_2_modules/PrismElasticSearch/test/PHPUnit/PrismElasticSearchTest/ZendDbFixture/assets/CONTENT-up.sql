DROP TABLE IF EXISTS `CONTENT`;
CREATE TABLE IF NOT EXISTS `CONTENT` (
  `contentId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `contentParentId` int(11) unsigned NOT NULL DEFAULT '0',
  `contentTypeId` int(11) NOT NULL,
  `categoryId` int(11) DEFAULT '0',
  `websiteId` int(11) unsigned NOT NULL DEFAULT '0',
  `treeDepth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `position` int(11) unsigned NOT NULL DEFAULT '0',
  `status` enum('ACTIVE','INACTIVE','DELETED') NOT NULL DEFAULT 'ACTIVE',
  `isLandingPage` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`contentId`),
  KEY `contentAndParentContent` (`contentParentId`,`contentId`),
  KEY `FK_CONTENT_CONTENT_Type` (`contentTypeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=106 ;

--
-- Dumping data for table `CONTENT`
--

INSERT INTO `CONTENT` (`contentId`, `contentParentId`, `contentTypeId`, `categoryId`, `websiteId`, `treeDepth`, `position`, `status`, `isLandingPage`, `created`, `modified`) VALUES
(1, 0, 1, NULL, 1, 0, 0, 'ACTIVE', 0, '2014-10-29 13:06:08', '2015-01-14 05:42:21');
