DROP TABLE IF EXISTS `CONTENT_Blocks`;
CREATE TABLE IF NOT EXISTS `CONTENT_Blocks` (
  `contentBlockId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contentBlockJsonContent` text,
  `contentBlockElasticSearch` text,
  `templateManagerBlockId` int(10) unsigned NOT NULL,
  `contentDefinitionsId` int(10) unsigned NOT NULL,
  `hideOnMobile` tinyint(1) DEFAULT '0',
  `status` enum('ACTIVE','INACTIVE','DELETED') DEFAULT 'ACTIVE',
  PRIMARY KEY (`contentBlockId`),
  KEY `FK_CONTENT_Blocks_TEMPLATE_MANAGER_Blocks` (`templateManagerBlockId`),
  KEY `FK_CONTENT_Blocks_CONTENT_Definitions` (`contentDefinitionsId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table that connects the template content blocks to specific page' AUTO_INCREMENT=9058 ;

--
-- Dumping data for table `CONTENT_Blocks`
--

INSERT INTO `CONTENT_Blocks` (`contentBlockId`, `contentBlockJsonContent`, `contentBlockElasticSearch`, `templateManagerBlockId`, `contentDefinitionsId`, `hideOnMobile`, `status`) VALUES
(1, '{"Text":["<h1 style=\\"color: #b5ce2f;\\">Customer Service</h1>\\n<p>If you have any queries regarding our services or how to use our website, our Customer Care Team is here to help. We operate during the hours of 9am - 5.30pm, Monday - Friday, excluding bank holidays.</p>\\n<p>Email: customerservices@oliversweeney.com<br />Returns Address: Oliver Sweeney, Columbia House, Apollo Rise, Southwood, Farnborough, GU14 0GT</p>\\n<p>We endeavour to get back to all email queries within 2 working days. For any urgent matters please call 0800 622 6030 from a landline or 0333 577 6030 from a mobile.</p>","text"]}', 'Customer Service\nIf you have any queries regarding our services or how to use our website, our Customer Care Team is here to help. We operate during the hours of 9am - 5.30pm, Monday - Friday, excluding bank holidays.\nEmail: customerservices@oliversweeney.comReturns Address: Oliver Sweeney, Columbia House, Apollo Rise, Southwood, Farnborough, GU14 0GT\nWe endeavour to get back to all email queries within 2 working days. For any urgent matters please call 0800 622 6030 from a landline or 0333 577 6030 from a mobile. ', 393, 1, 0, 'DELETED'),
(2, '', '', 394, 1, 0, 'ACTIVE'),
(3, '', '', 397, 1, 0, 'ACTIVE'),
(4, '', '', 398, 1, 0, 'ACTIVE'),
(5, '', '', 401, 1, 0, 'ACTIVE'),
(6, '', '', 402, 1, 0, 'ACTIVE'),
(7, '', '', 403, 1, 0, 'DELETED'),
(8, '', 'Customer Service
If you have any queries regarding our services or how to use our website, our Customer Care Team is here to help. We operate during the hours of 9am - 5.30pm, Monday - Friday, excluding bank holidays.
Email: customerservices@oliversweeney.comReturns Address: Oliver Sweeney, Columbia House, Apollo Rise, Southwood, Farnborough, GU14 0GT
We endeavour to get back to all email queries within 2 working days. For any urgent matters please call 0800 622 6030 from a landline or 0333 577 6030 from a mobile. ', 402, 1, 0, 'ACTIVE'),
(9, '', 'Customer Service
If you have any queries regarding our services or how to use our website, our Customer Care Team is here to help. We operate during the hours of 9am - 5.30pm, Monday - Friday, excluding bank holidays.
Email: customerservices@oliversweeney.comReturns Address: Oliver Sweeney, Columbia House, Apollo Rise, Southwood, Farnborough, GU14 0GT
We endeavour to get back to all email queries within 2 working days. For any urgent matters please call 0800 622 6030 from a landline or 0333 577 6030 from a mobile. ', 402, 1, 0, 'ACTIVE');
