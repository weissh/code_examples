<?php

namespace PrismElasticSearchTest\ZendDbFixture;

use PrismElasticSearchTest\Bootstrap;

class FixtureManager
{
    protected $dbAdapter;

    public function __construct($adapterName)
    {
        $this->dbAdapter = Bootstrap::getServiceManager()->get($adapterName);
    }

    public function getDbAdapter()
    {
        return $this->dbAdapter;
    }

    public function execute($file)
    {
        $this->getDbAdapter()->query(file_get_contents(__DIR__.'/assets/'.$file), 'execute');
    }
}
