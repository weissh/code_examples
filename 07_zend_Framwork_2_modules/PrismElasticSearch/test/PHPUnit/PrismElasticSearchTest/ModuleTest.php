<?php

namespace PrismElasticSearchTest;

use PrismElasticSearch\Module;
use PHPUnit_Framework_TestCase;
use Zend\Console\Console;

/**
 * @author haniw
 */
class ModuleTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Module
     */
    protected $module;

    public function setUp()
    {
        $this->module = new Module();
    }

    public function testGetConfig()
    {
        $config = $this->module->getConfig();
        $expected = include __DIR__.'/../../../config/module.config.php';

        $this->assertEquals($expected, $config);
    }

    public function testGetAutoloaderConfig()
    {
        $config = $this->module->getAutoloaderConfig();

        $this->assertTrue(is_array($config));
        $this->assertArrayHasKey('Zend\Loader\StandardAutoloader', $config);
    }
}
