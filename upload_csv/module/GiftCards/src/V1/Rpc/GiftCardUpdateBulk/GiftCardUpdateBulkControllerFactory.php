<?php
namespace GiftCards\V1\Rpc\GiftCardUpdateBulk;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class GiftCardUpdateBulkControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $giftCardMapper = $container->get('GiftCardMapper');
        $cardEntity = $container->get('GiftCardEntity');
        $apiResponse = $container->get('ApiResponse');
        $bulkFilesFactory = $container->get('BulkFilesFactory');

        // Retrieve the InputFilterManager + addCard filter validator
        $filters = $container->get('InputFilterManager');
        $addCardFilter = $filters->get('GiftCards\\V1\\Rpc\\GiftCardUpdate\\Validator');

        return new GiftCardUpdateBulkController($giftCardMapper, $cardEntity, $apiResponse, $addCardFilter, $bulkFilesFactory );
    }
}
