<?php

namespace GiftCards\V1\Rpc\GiftCardUpdateBulk;

use Exception;
use GiftCards\V1\Rpc\Entities\SpecialCases\MissingCard;
use GiftCards\V1\Rpc\Entities\GiftCardEntity;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\ApiTools\ContentNegotiation\ViewModel;
use Laminas\ApiTools\Hal\Entity as HalEntity;
use Laminas\InputFilter\InputFilter;
use GiftCards\V1\Rpc\Entities\ApiResponse;
use GiftCards\V1\Rpc\Mappers\GiftCardMapper;
use GiftCards\V1\Rpc\Services\Exceptions\ApiException;
use GiftCards\V1\Rpc\Services\Files\Bulks\BulkAbstract;
use GiftCards\V1\Rpc\Services\Files\Exceptions\CsvException;
use GiftCards\V1\Rpc\Services\Files\Exceptions\FileManipulationException;
use GiftCards\V1\Rpc\Services\Files\Exceptions\ValidationException;
use GiftCards\V1\Rpc\Services\Files\Interfaces\SplCsvGetter;
use GiftCards\V1\Rpc\Services\Files\Bulks\Factory;

class GiftCardUpdateBulkController extends AbstractActionController
{

    /**
     *
     * @var GiftCardMapper
     */
    protected $giftCardMapper;

    /**
     *
     * @var ApiResponse
     */
    protected $apiResponse;

    /**
     *
     * @var GiftCardEntity
     */
    protected $cardEntity;

    /**
     *
     * @var InputFilter
     */
    protected $updateCardValidators;

    /**
     *
     * @var Factory;
     */
    protected $bulkFilesFactory;

    ///////////////
    // init outputs

    /**
     *
     * @var int
     */
    protected $total_gift_card_updated = 0;

    /**
     *
     * @var int
     */
    protected $total_failure = 0;

    /**
     *
     * @var array
     */
    protected $updated_gift_cards = Array();

    /**
     *
     * @var array
     */
    protected $failed_to_update_gift_cards = Array(
        'validation' => Array(),
        'exceptions' => Array()
    );

    /**
     *
     * @param GiftCardMapper $giftCardMapper
     * @param GiftCardEntity $entity
     * @param ApiResponse $apiResponse
     * @param InputFilter $filter
     */
    public function __construct(
        GiftCardMapper $giftCardMapper,
        GiftCardEntity $entity,
        ApiResponse $apiResponse,
        InputFilter $filter,
        Factory $bulkFilesFactory
    )
    {
        $this->giftCardMapper = $giftCardMapper;
        $this->apiResponse = $apiResponse;
        $this->cardEntity = $entity;
        $this->updateCardValidators = $filter;
        $this->bulkFilesFactory = $bulkFilesFactory;
    }

    /**
     * Store updated Card bulk load to the persistent storage
     * @return ViewModel
     */
    public function giftCardUpdateBulkAction()
    {
        // Oauth2 client code
        $clientId = $this->getidentity()->getAuthenticationIdentity()['client_id'];

        // basic file ops
        try {

            $file = $this->getPreparedFileObject();
            $splFile = $this->getSplCsvObject($file);

            $this->apiResponse->setSuccess();

        } catch (Exception $e) {

            $this->setGlobalErrorResponse($e);

            // general error - finnish
            return $this->getViewModel($clientId);
        }

        ////////////////////////
        // file manipulation done, we can process content

        // loop the file and perform actions on EACH LINE
        foreach ($splFile as $lineNo => $line) {

            $lineNoString = 'Line_' . ($lineNo + 1); // file line identification
            unset($inputData); // cleanup before new iteration

            try {

                $inputData = $file->lineToArray($line); // export line to array

                $this->lineValidation($inputData); // current line validation

                // save card
                $this->saveUpdatedCard($inputData, $clientId);

                // passed
                $this->total_gift_card_updated++;
                $this->updated_gift_cards[] = $inputData['barcode'];

            } catch (Exception $e) { // failure

                $this->total_failure++;
                $lineIdent = (!empty($inputData['barcode'])) ? $inputData['barcode'] : $lineNoString; // try to use Barcode for identification
                $this->setInternalErrorResponse($e, $lineIdent);
            }

        }

        // finalize
        return $this->getViewModel($clientId);

    }

    /**
     * Payload miracle to return hal-json
     * @param string $clientId
     * @return ViewModel
     */
    protected function getViewModel($clientId)
    {

        return new ViewModel(array(
            'payload' => $this->getHalEntity($clientId),
        ));
    }

    /**
     * Mapping of internal variables into proper HAL entity
     * @param string $clientId
     * @return HalEntity
     */
    protected function getHalEntity($clientId)
    {

        // HAL wrapper
        $halEntity = new HalEntity(
            Array(
                'response' => $this->apiResponse->toArray(),
                'total_gift_card_updated' => $this->total_gift_card_updated,
                'total_failure' => $this->total_failure,
                'updated_gift_cards' => $this->updated_gift_cards,
                'failed_to_update_gift_cards' => $this->failed_to_update_gift_cards
            ),
            $clientId // random ID to fullfil HAL reqs.
        );

        return $halEntity;

    }

    /**
     * Check current line against giftcardUpdate method validators
     * @param array $inputData
     * @throws ValidationException
     */
    protected function lineValidation(array $inputData)
    {

        $validator = clone $this->updateCardValidators;
        $validator->setData($inputData);

        // validation failed - entity not processed
        if (!$validator->isValid()) {
            throw new ValidationException($validator->getMessages()); // special exception type accepts array as input
        }
    }

    /**
     * Receive uploaded file and move it to final location <br>
     * Create and return file object
     * @return BulkAbstract
     * @throws FileManipulationException
     */
    protected function getPreparedFileObject()
    {

        // input file - temp location
        $inputFilter = $this->getEvent()->getParam('Laminas\ApiTools\ContentValidation\InputFilter');
        $fileData = $inputFilter->getValues()['bulk']; // validated on the Apigility level

        // File object + set name
//        $file = $this->bulkFilesFactory->getFileObject(
//            'CardsUpdateFile',
//            APPLICATION_PATH . '/' . $fileData['tmp_name']
//        ); // new CardsUpdateFile();

        $file = $this->bulkFilesFactory->getFileObject(
            'CardsUpdateFile',
             $fileData['tmp_name']
        ); // new CardsUpdateFile();


        $file->rename($fileData['name']);

        // move to final dest.
        $file->moveToStorage();

        return $file;
    }

    /**
     * CReates "CSV ready" Spl file object (traversable)
     * @param SplCsvGetter $file
     * @return Traversable
     */
    protected function getSplCsvObject(SplCsvGetter $file)
    {

        // open as SplFileObject
        $splFile = $file->getAsSplFile();
        $splFile->setFlags($splFile::READ_CSV | $splFile::READ_AHEAD | $splFile::SKIP_EMPTY | $splFile::DROP_NEW_LINE);

        return $splFile;
    }

    /**
     * Function will set global API response in case of Excpetion (based on exception type)
     * @param Exception $e
     */
    protected function setGlobalErrorResponse(Exception $e)
    {

        if ($e instanceof FileManipulationException) { // known problem

            $this->apiResponse->setResponseCode('007');
            $this->apiResponse->setResponseDescription('CSV file manipulation failed with message: ' . $e->getMessage());

        } else { // other general failure

            $this->apiResponse->setResponseCode('007');
            $this->apiResponse->setResponseDescription('We could not process CSV file. Please check the uploaded file.');
            // @ToDo: logging ??
        }
    }

    /**
     * Save input data to persistent storage
     * @param array $inputData
     * @param string $clientId
     * @throws ApiException|Exception
     */
    protected function saveUpdatedCard(array $inputData, $clientId)
    {

        // data received
        $inputEntity = clone $this->cardEntity;
        $inputEntity->setClientCode($clientId);
        $inputEntity->setBarcode($inputData['barcode']);
        $inputEntity->setIsPhysical($inputData['is_physical']);
        $inputEntity->setStartDate($inputData['start_date']);
        $inputEntity->setEndDate($inputData['end_date']);
        $inputEntity->setChannelsVector($inputData['channels']);

        // save gift card
        $this->giftCardMapper->updateGiftCard($inputEntity, $clientId);
    }

    /**
     * Prepares internal response in case of sigle GiftCard processing failure
     * @param Exception $e
     * @param string $lineIdent Identification of the failure line
     */
    protected function setInternalErrorResponse(Exception $e, $lineIdent)
    {

        if ($e instanceof ApiException) {

            $this->failed_to_update_gift_cards['exceptions'][$lineIdent] = Array(
                'responseCode' => $e->getCode(),
                'responseDescription' => $e->getMessage()
            );

        } elseif ($e instanceof CsvException) {

            $this->failed_to_update_gift_cards['exceptions'][$lineIdent] = Array(
                'responseCode' => $e->getCode(),
                'responseDescription' => $e->getMessage()
            );

        } elseif ($e instanceof ValidationException) {   // validation failed

            $this->failed_to_update_gift_cards['validation'][$lineIdent] = $e->getValidationErrors();

        } else { // other general failure

            $ae = new ApiException(ApiException::GENERAL_FAILURE);
            $this->failed_to_update_gift_cards['exceptions'][$lineIdent] = Array(
                'responseCode' => $ae->getCode(),
                'responseDescription' => $ae->getMessage()
            );
            // @ToDo: logging ??
        }
    }
}
