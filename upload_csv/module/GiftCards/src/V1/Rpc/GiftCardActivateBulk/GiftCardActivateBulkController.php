<?php

namespace GiftCards\V1\Rpc\GiftCardActivateBulk;


use Exception;
use GiftCards\V1\Rpc\Entities\SpecialCases\MissingCard;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\ApiTools\ContentNegotiation\ViewModel;
use Laminas\ApiTools\Hal\Entity as HalEntity;
use Laminas\InputFilter\InputFilter;
use GiftCards\V1\Rpc\Entities\ApiResponse;
use GiftCards\V1\Rpc\Entities\GiftCardActivateEntity;
use GiftCards\V1\Rpc\Mappers\GiftCardMapper;
use GiftCards\V1\Rpc\Services\Exceptions\ApiException;
use GiftCards\V1\Rpc\Services\Files\Bulks\BulkAbstract;
use GiftCards\V1\Rpc\Services\Files\Exceptions\CsvException;
use GiftCards\V1\Rpc\Services\Files\Exceptions\FileManipulationException;
use GiftCards\V1\Rpc\Services\Files\Exceptions\ValidationException;
use GiftCards\V1\Rpc\Services\Files\Interfaces\SplCsvGetter;
use GiftCards\V1\Rpc\Services\Files\Bulks\Factory;


class GiftCardActivateBulkController extends AbstractActionController
{
    /**
     *
     * @var GiftCardMapper
     */
    protected $giftCardMapper;

    /**
     *
     * @var ApiResponse
     */
    protected $apiResponse;

    /**
     *
     * @var GiftCardActivateEntity
     */
    protected $cardEntity;

    /**
     *
     * @var InputFilter
     */
    protected $activateCardValidators;

    /**
     *
     * @var Factory;
     */
    protected $bulkFilesFactory;

    ///////////////
    // init outputs

    /**
     *
     * @var int
     */
    protected $total_activated = 0;

    /**
     *
     * @var int
     */
    protected $failed_to_activate = 0;

    /**
     *
     * @var array
     */
    protected $list_activated_gift_cards = Array();

    /**
     *
     * @var array
     */
    protected $failed_to_activate_list = Array(
        'validation' => Array(),
        'exceptions' => Array()
    );

    /**
     *
     * @param GiftCardMapper $giftCardMapper
     * @param GiftCardActivateEntity $entity
     * @param ApiResponse $apiResponse
     * @param InputFilter $filter
     */
    public function __construct(GiftCardMapper $giftCardMapper, GiftCardActivateEntity $entity, ApiResponse $apiResponse, InputFilter $filter, Factory $bulkFilesFactory)
    {
        $this->giftCardMapper = $giftCardMapper;
        $this->apiResponse = $apiResponse;
        $this->cardEntity = $entity;
        $this->activateCardValidators = $filter;
        $this->bulkFilesFactory = $bulkFilesFactory;
    }

    public function giftCardActivateBulkAction()
    {
        $clientId = $this->getidentity()->getAuthenticationIdentity()['client_id'];

        // basic file ops
        try {

            $file = $this->getPreparedFileObject();
            $splFile = $this->getSplCsvObject($file);

            $this->apiResponse->setSuccess();

        } catch (Exception $e) {

            $this->setGlobalErrorResponse($e);

            // general error - finnish
            return $this->getViewModel($clientId);
        }

        ////////////////////////
        // file manipulation done, we can process content

        // loop the file and perform actions on EACH LINE
        foreach ($splFile as $lineNo => $line) {

            $lineNoString = 'Line_' . ($lineNo + 1); // file line identification
            unset($inputData); // cleanup before new iteration

            try {

                $inputData = $file->lineToArray($line); // export line to array

                $this->lineValidation($inputData); // current line validation

                // activate card
                $this->activateCard($inputData, $clientId);

                // passed
                $this->total_activated++;
                $this->list_activated_gift_cards[] = $inputData['barcode'];

            } catch (Exception $e) { // failure

                $this->failed_to_activate++;
                $lineIdent = (!empty($inputData['barcode'])) ? $inputData['barcode'] : $lineNoString; // try to use Barcode for identification
                $this->setInternalErrorResponse($e, $lineIdent);
            }

        }

        // finalize
        return $this->getViewModel($clientId);
    }

    /**
     * Receive uploaded file and move it to final location <br>
     * Create and return file object
     * @return BulkAbstract
     * @throws FileManipulationException
     */
    protected function getPreparedFileObject()
    {

        // input file - temp location
        $inputFilter = $this->getEvent()->getParam('Laminas\ApiTools\ContentValidation\InputFilter');
        $fileData = $inputFilter->getValues()['bulk']; // validated on the Apigility level

        // File object + set name
        //$file = $this->bulkFilesFactory->getFileObject('CardsActivateFile', APPLICATION_PATH . '/' . $fileData['tmp_name']); // new CardsActivateFile();
        $file = $this->bulkFilesFactory->getFileObject('CardsActivateFile', $fileData['tmp_name']); // new CardsActivateFile();
        $file->rename($fileData['name']);

        // move to final dest.
        $file->moveToStorage();

        return $file;
    }

    protected function getSplCsvObject(SplCsvGetter $file)
    {

        // open as SplFileObject
        $splFile = $file->getAsSplFile();
        $splFile->setFlags($splFile::READ_CSV | $splFile::READ_AHEAD | $splFile::SKIP_EMPTY | $splFile::DROP_NEW_LINE);

        return $splFile;
    }

    /**
     * Function will set global API response in case of Excpetion (based on exception type)
     * @param Exception $e
     */
    protected function setGlobalErrorResponse(Exception $e)
    {

        if ($e instanceof FileManipulationException) { // known problem

            $this->apiResponse->setResponseCode('007');
            $this->apiResponse->setResponseDescription('CSV file manipulation failed with message: ' . $e->getMessage());

        } else { // other general failure

            $this->apiResponse->setResponseCode('007');
            $this->apiResponse->setResponseDescription('We could not process CSV file. Please check the uploaded file.');
            // @ToDo: logging ??
        }
    }

    /**
     * Check current line against giftcardActivate method validators
     * @param array $inputData
     * @throws ValidationException
     */
    protected function lineValidation(array $inputData)
    {

        $validator = clone $this->activateCardValidators;
        $validator->setData($inputData);

        // validation failed - entity not processed
        if (!$validator->isValid()) {
            throw new ValidationException($validator->getMessages()); // special exception type accepts array as input
        }
    }

    /**
     * Activate card in storage
     * @param array $inputData
     * @param string $clientId
     * @throws ApiException|Exception
     */
    protected function activateCard(array $inputData, $clientId)
    {

        // data received
        $inputEntity = clone $this->cardEntity;
        $inputEntity->setClientCode($clientId);
        $inputEntity->setBarcode($inputData['barcode']);

        // activate card
        $outputEntity = $this->giftCardMapper->activate($inputEntity);
        if ($outputEntity instanceof MissingCard) {
            throw new ApiException($this->giftCardMapper->getInternalResponse('GIFT_CARD_NOT_FOUND'));
        }
    }

    /**
     * Prepares internal response in case of sigle GiftCard processing failure
     * @param Exception $e
     * @param string $lineIdent Identification of the failure line
     */
    protected function setInternalErrorResponse(Exception $e, $lineIdent)
    {

        if ($e instanceof ApiException) {

            $this->failed_to_activate_list['exceptions'][$lineIdent] = Array(
                'responseCode' => $e->getCode(),
                'responseDescription' => $e->getMessage()
            );

        } elseif ($e instanceof CsvException) {

            $this->failed_to_activate_list['exceptions'][$lineIdent] = Array(
                'responseCode' => $e->getCode(),
                'responseDescription' => $e->getMessage()
            );

        } elseif ($e instanceof ValidationException) {   // validation failed

            $this->failed_to_activate_list['validation'][$lineIdent] = $e->getValidationErrors();

        } else { // other general failure

            $ae = new ApiException(ApiException::GENERAL_FAILURE);
            $this->failed_to_activate_list['exceptions'][$lineIdent] = Array(
                'responseCode' => $ae->getCode(),
                'responseDescription' => $ae->getMessage()
            );
            // @ToDo: logging ??
        }
    }

    /**
     * Payload miracle to return hal-json
     * @param string $clientId
     * @return ViewModel
     */
    protected function getViewModel($clientId)
    {

        return new ViewModel(array(
            'payload' => $this->getHalEntity($clientId),
        ));
    }

    /**
     * Mapping of internal variables into proper HAL entity
     * @param string $clientId
     * @return HalEntity
     */
    protected function getHalEntity($clientId)
    {

        // HAL wrapper
        $halEntity = new HalEntity(
            Array(
                'response' => $this->apiResponse->toArray(),
                'total_activated' => $this->total_activated,
                'failed_to_activate' => $this->failed_to_activate,
                'list_activated_gift_cards' => $this->list_activated_gift_cards,
                'failed_to_activate_list' => $this->failed_to_activate_list
            ),
            $clientId // random ID to fulfil HAL reqs.
        );

        return $halEntity;

    }

}
