<?php

namespace GiftCards\V1\Rpc\GiftCardActivateBulk;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class GiftCardActivateBulkControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $giftCardMapper = $container->get('GiftCardMapper');
        $cardEntity = $container->get('GiftCardActivateEntity');
        $apiResponse = $container->get('ApiResponse');
        $bulkFilesFactory = $container->get('BulkFilesFactory');

        // Retrieve the InputFilterManager + addCard filter validator
        $filters = $container->get('InputFilterManager');
        $delCardFilter = $filters->get('GiftCards\\V1\\Rpc\\GiftCardActivate\\Validator');

        return new GiftCardActivateBulkController($giftCardMapper, $cardEntity, $apiResponse, $delCardFilter, $bulkFilesFactory);
    }
}
