<?php
namespace GiftCards\V1\Rpc\Entities;

class GiftCardTransactionFailureEntity extends AbstractEntity
{
    /**
     * @var int
     */
    protected $giftCardsTransactionFailureId;

    /**
     * @var int
     */
    protected $cardId;

    /**
     * @var string
     */
    protected $clientCode;

    /**
     * @var int
     */
    protected $transactionTypeId;

    /**
     * @var string
     */
    protected $responseCode;

    /**
     * @var string
     */
    protected $responseDescription;

    /**
     * @var string
     */
    protected $request;

    /**
     * @var string
     */
    protected $created;

    /**
     * @var string
     */
    protected $modified;

    public function setCardId($cardId)
    {
        $this->cardId = $cardId;
        return $this;
    }

    /**
     * @return int
     */
    public function getCardId()
    {
        return $this->cardId;
    }

    /**
     * @param $giftCardsTransactionFailureId
     * @return GiftCardTransactionFailureEntity
     */
    public function setGiftCardsTransactionFailureId($giftCardsTransactionFailureId)
    {
        $this->giftCardsTransactionFailureId = $giftCardsTransactionFailureId;
        return $this;
    }

    /**
     * @return int
     */
    public function getGiftCardsTransactionFailureId()
    {
        return $this->giftCardsTransactionFailureId;
    }

    /**
     * @param $clientCode
     * @return GiftCardTransactionFailureEntity
     */
    public function setClientCode($clientCode)
    {
        $this->clientCode = $clientCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientCode()
    {
        return $this->clientCode;
    }

    /**
     * @param $transactionTypeId
     * @return GiftCardTransactionFailureEntity
     */
    public function setTransactionTypeId($transactionTypeId)
    {
        $this->transactionTypeId = $transactionTypeId;
        return $this;
    }

    /**
     * @return int
     */
    public function getTransactionTypeId()
    {
        return $this->transactionTypeId;
    }

    /**
     * @param $responseCode
     * @return GiftCardTransactionFailureEntity
     */
    public function setResponseCode($responseCode)
    {
        $this->responseCode = $responseCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * @param $responseDescription
     * @return GiftCardTransactionFailureEntity
     */
    public function setResponseDescription($responseDescription)
    {
        $this->responseDescription = $responseDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getResponseDescription()
    {
        return $this->responseDescription;
    }

    /**
     * @param $request
     * @return GiftCardTransactionFailureEntity
     */
    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return string
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param $created
     * @return GiftCardTransactionFailureEntity
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return string
     */
    public function getModified()
    {
        return $this->modified;
    }
}
