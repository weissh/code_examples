<?php

namespace GiftCards\V1\Rpc\Entities;

use Laminas\Paginator\Paginator;

/**
 * Shared utils for entity
 */
abstract class AbstractCollection extends Paginator
                                    implements HalExtractInterface
{
    
    /**
     * Will remove NULL values and empty arrays.
     * @var boolean
     */
    protected $forceCleanup = FALSE;

   /**
    * Extract protected properties from items using getters
    * @param bool $is_paginated
    * @return array
    */
   public function toArray($is_paginated = false) {

       $items = $this->setCurrentItems($is_paginated);

       if( empty($items) ) {
           return Array();
       }
       
       foreach ($items as $name => $item) {

            // if value is not scalar, try to extract
            if($item instanceof HalExtractInterface) {
                
                if($this->getCleanupFlag()) {
                   $item->enforceCleanup();
                } else {
                   $item->disableCleanup(); 
                }
                
                $items[$name] = $item->toArray();
                
                if($this->getCleanupFlag() && empty($items[$name]) ) {
                    unset($items[$name]);
                }
 
            }

        }

        return $items;

   }

    /**
     * @param $is_paginated
     * @return array|\Traversable
     */
   private function setCurrentItems($is_paginated)
   {
       if ($is_paginated) {
           $items = iterator_to_array($this->getCurrentItems());
       } else {
           $items = $this->getAdapter()->getItems();
       }

       return $items;
   }
   
   /**
    * Will remove NULL values and empty arrays on toArray conversion.
    */
   public function enforceCleanup() {
       $this->forceCleanup = TRUE;
   }
   
   /**
    * Will preserve all values, including nulls
    */
   public function disableCleanup() {
       $this->forceCleanup = FALSE;
   }

   /**
    * 
    * @return boolean
    */
   public function getCleanupFlag() {
       return $this->forceCleanup;
   }

}
