<?php

namespace GiftCards\V1\Rpc\Entities;

/**
 * Client Settings entity
 */
class ClientSettingsEntity extends AbstractEntity
{
    /**
     *
     * @var int 
     */
    protected $id = 0;

    /**
     *
     * @var string 
     */
    protected $periodName = '';
    
    /**
     *
     * @var string 
     */
    protected $periodDescription = '';
    
    /**
     *
     * @var int 
     */
    protected $expiryPeriodQuantity = 0;
    
    /**
     *
     * @var int
     */
    protected $restrictToCurrency = 0;
    
    /**
     *
     * @var int
     */
    protected $multiChannelSupport = 0;
    
    /**
     *
     * @var int 
     */
    protected $redemptionSupport = 0;
    
    /**
     *
     * @var int 
     */
    protected $expiryOnZeroBalance = 0;
    
    /**
     *
     * @var int 
     */
    protected $refundToZeroBalance = 0;
    
    /**
     *
     * @var int 
     */
    protected $maximumRedemptionUsed = 0;
    
    /**
     *
     * @var float 
     */
    protected $minimumInitialLoad = 0;
    
    /**
     *
     * @var float 
     */
    protected $maximumInitialLoad = 0;
    
    /**
     * 
     * @var string
     */
    protected $barcodeValidationRule = null;
    
    /**
     *
     * @var int 
     */
    protected $logAction = 0;
    
    /**
     * 
     * @var int
     */
    protected $barcodeNumOfChar;
    
    /**
     * 
     * @var string
     */
    protected $barcodeGenerateRule;
    
    /**
     * 
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    public function getPeriodName() {
        return $this->periodName;
    }

    /**
     * 
     * @return string
     */
    public function getPeriodDescription() {
        return $this->periodDescription;
    }

    /**
     * 
     * @return int
     */
    public function getExpiryPeriodQuantity() {
        return $this->expiryPeriodQuantity;
    }

    /**
     * 
     * @return int
     */
    public function getRestrictToCurrency() {
        return $this->restrictToCurrency;
    }

    /**
     * 
     * @return int
     */
    public function getMultiChannelSupport() {
        return $this->multiChannelSupport;
    }

    /**
     * 
     * @return int
     */
    public function getRedemptionSupport() {
        return $this->redemptionSupport;
    }

    /**
     * 
     * @return int
     */
    public function getExpiryOnZeroBalance() {
        return $this->expiryOnZeroBalance;
    }

    /**
     * 
     * @return int
     */
    public function getRefundToZeroBalance() {
        return $this->refundToZeroBalance;
    }

    /**
     * 
     * @return int
     */
    public function getMaximumRedemptionUsed() {
        return $this->maximumRedemptionUsed;
    }

    /**
     * 
     * @return float
     */
    public function getMinimumInitialLoad() {
        return $this->minimumInitialLoad;
    }

    /**
     * 
     * @return float
     */
    public function getMaximumInitialLoad() {
        return $this->maximumInitialLoad;
    }

    /**
     * 
     * @return int
     */
    public function getLogAction() {
        return $this->logAction;
    }

    /**
     * 
     * @param int $id
     * @return ClientSettingsEntity
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * 
     * @param string $periodName
     * @return ClientSettingsEntity
     */
    public function setPeriodName($periodName) {
        $this->periodName = $periodName;
        return $this;
    }

    /**
     * 
     * @param string $periodDescription
     * @return ClientSettingsEntity
     */
    public function setPeriodDescription($periodDescription) {
        $this->periodDescription = $periodDescription;
        return $this;
    }

    /**
     * 
     * @param int $expiryPeriodQuantity
     * @return ClientSettingsEntity
     */
    public function setExpiryPeriodQuantity($expiryPeriodQuantity) {
        $this->expiryPeriodQuantity = $expiryPeriodQuantity;
        return $this;
    }

    /**
     * 
     * @param int $restrictToCurrency
     * @return ClientSettingsEntity
     */
    public function setRestrictToCurrency($restrictToCurrency) {
        $this->restrictToCurrency = $restrictToCurrency;
        return $this;
    }

    /**
     * 
     * @param int $multiChannelSupport
     * @return ClientSettingsEntity
     */
    public function setMultiChannelSupport($multiChannelSupport) {
        $this->multiChannelSupport = $multiChannelSupport;
        return $this;
    }

    /**
     * 
     * @param int $redemptionSupport
     * @return ClientSettingsEntity
     */
    public function setRedemptionSupport($redemptionSupport) {
        $this->redemptionSupport = $redemptionSupport;
        return $this;
    }

    /**
     * 
     * @param int $expiryOnZeroBalance
     * @return ClientSettingsEntity
     */
    public function setExpiryOnZeroBalance($expiryOnZeroBalance) {
        $this->expiryOnZeroBalance = $expiryOnZeroBalance;
        return $this;
    }

    /**
     * 
     * @param int $refundToZeroBalance
     * @return ClientSettingsEntity
     */
    public function setRefundToZeroBalance($refundToZeroBalance) {
        $this->refundToZeroBalance = $refundToZeroBalance;
        return $this;
    }

    /**
     * 
     * @param int $maximumRedemptionUsed
     * @return ClientSettingsEntity
     */
    public function setMaximumRedemptionUsed($maximumRedemptionUsed) {
        $this->maximumRedemptionUsed = $maximumRedemptionUsed;
        return $this;
    }

    /**
     * 
     * @param float $minimumInitialLoad
     * @return ClientSettingsEntity
     */
    public function setMinimumInitialLoad($minimumInitialLoad) {
        $this->minimumInitialLoad = $minimumInitialLoad;
        return $this;
    }

    /**
     * 
     * @param float $maximumInitialLoad
     * @return ClientSettingsEntity
     */
    public function setMaximumInitialLoad($maximumInitialLoad) {
        $this->maximumInitialLoad = $maximumInitialLoad;
        return $this;
    }

    /**
     * 
     * @param int $logAction
     * @return ClientSettingsEntity
     */
    public function setLogAction($logAction) {
        $this->logAction = $logAction;
        return $this;
    }
    /**
     * @return string $barcodeValidationRule
     */
    public function getBarcodeValidationRule()
    {
        return $this->barcodeValidationRule;
    }

    /**
     * @param string $barcodeValidationRule
     */
    public function setBarcodeValidationRule($barcodeValidationRule)
    {
        $this->barcodeValidationRule = $barcodeValidationRule;
    }

    /**
     * @return int $barcodeNumOfChar
     */
    public function getBarcodeNumOfChar()
    {
        return $this->barcodeNumOfChar;
    }

    /**
     * @return string $barcodeGenerateRule
     */
    public function getBarcodeGenerateRule()
    {
        return $this->barcodeGenerateRule;
    }

    /**
     * @param number $barcodeNumOfChar
     */
    public function setBarcodeNumOfChar($barcodeNumOfChar)
    {
        $this->barcodeNumOfChar = $barcodeNumOfChar;
    }

    /**
     * @param string $barcodeGenerateRule
     */
    public function setBarcodeGenerateRule($barcodeGenerateRule)
    {
        $this->barcodeGenerateRule = $barcodeGenerateRule;
    }
  
}
