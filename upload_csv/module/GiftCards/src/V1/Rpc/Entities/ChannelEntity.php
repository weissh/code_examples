<?php

namespace GiftCards\V1\Rpc\Entities;

/**
 * Channel Entity
 */
class ChannelEntity extends AbstractEntity
{

    /**
     *
     * @var int
     */
    protected $id = 0;

    /**
     *
     * @var string
     */
    protected $name = '';

    /**
     *
     * @var string
     */
    protected $description = '';

    /**
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     *
     * @param string $id
     * @return ChannelEntity
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @param string $name
     * @return ChannelEntity
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @param string $description
     * @return ChannelEntity
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }
   
}
