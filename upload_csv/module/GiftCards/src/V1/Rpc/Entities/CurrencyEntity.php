<?php

namespace GiftCards\V1\Rpc\Entities;

/**
 * Currency entity
 */
class CurrencyEntity extends AbstractEntity
{
    
    /**
     *
     * @var string 
     */
    protected $code = '';
    
    /**
     *
     * @var int 
     */
    protected $decimals = 0;
    
    /**
     * 
     * @return string
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * 
     * @return int
     */
    public function getDecimals() {
        return $this->decimals;
    }

    /**
     * 
     * @param string $code
     * @return CurrencyEntity
     */
    public function setCode($code) {
        $this->code = $code;
        return $this;
    }

    /**
     * 
     * @param int $decimals
     * @return CurrencyEntity
     */
    public function setDecimals($decimals) {
        $this->decimals = $decimals;
        return $this;
    }


    
}
