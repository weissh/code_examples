<?php
namespace GiftCards\V1\Rpc\Entities;

class GiftCardDamagedEntity extends AbstractEntity
{
    /**
     *
     * @var string /^[a-zA-Z0-9 .\\\\-]+$/
     */
    protected $barcode;

    /**
     *
     * @var string
     */
    protected $clientCode;

    /**
     *
     * @var int
     */
    protected $giftCardStatusesId;

    /**
     * @param int $barcode
     * @return GiftCardDamagedEntity
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @param string $clientCode
     * @return GiftCardDamagedEntity
     */
    public function setClientCode($clientCode)
    {
        $this->clientCode = $clientCode;
        return $this;
    }

    /**
     * @return string $clientId
     */
    public function getClientCode()
    {
        return $this->clientCode;
    }

    /**
     * @return number
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @return string $giftCardStatusesId
     */
    public function getGiftCardStatusesId()
    {
        return $this->giftCardStatusesId;
    }

    /**
     * @param int $giftCardStatusesId
     * @return GiftCardDamagedEntity
     */
    public function setGiftCardStatusesId($giftCardStatusesId)
    {
        $this->giftCardStatusesId = $giftCardStatusesId;
        return $this;
    }
}
