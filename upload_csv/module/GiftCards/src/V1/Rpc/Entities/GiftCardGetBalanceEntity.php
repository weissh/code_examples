<?php
namespace GiftCards\V1\Rpc\Entities;

class GiftCardGetBalanceEntity extends AbstractEntity
{

    /**
     *
     * @var string /^[a-zA-Z0-9 .\\\\-]+$/
     */
    protected $barcode = '';

    /**
     *
     * @var float
     */
    protected $amount = 0.00;

    /**
     *
     * @var string
     */
    protected $currencyCode = '';

    /**
    *
    * @var string
    */
    protected $status = '';
    
    /**
     * 
     * @var string
     */
    protected $clientCode = '';
    /**
     * @return string $barcode
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @return float $amount
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string $currencyCode
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @return int $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string $clientCode
     */
    public function getClientCode()
    {
        return $this->clientCode;
    }

    /**
     * @param string $barcode
     * @return GiftCardGetBalanceEntity
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @param float $amount
     * @return GiftCardGetBalanceEntity
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @param string $currencyCode
     * @return GiftCardGetBalanceEntity
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
        return $this;
    }

    /**
     * @param string $status
     * @return GiftCardGetBalanceEntity
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param string $clientCode
     * @return GiftCardGetBalanceEntity
     */
    public function setClientCode($clientCode)
    {
        $this->clientCode = $clientCode;
        return $this;
    }

}
