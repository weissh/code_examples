<?php
namespace GiftCards\V1\Rpc\Entities;

class GiftCardStolenEntity extends AbstractEntity
{
    /**
     *
     * @var string /^[a-zA-Z0-9 .\\\\-]+$/
     */
    protected $barcode;

    /**
     *
     * @var string
     */
    protected $clientCode;

    /**
     *
     * @var int
     */
    protected $giftCardStatusesId;

    /**
     * @param int $barcode
     * @return GiftCardStolenEntity
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @param string $clientCode
     * @return GiftCardStolenEntity
     */
    public function setClientCode($clientCode)
    {
        $this->clientCode = $clientCode;
        return $this;
    }

    /**
     * @return int $clientId
     */
    public function getClientCode()
    {
        return $this->clientCode;
    }

    /**
     * @return string
     *
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @return int $giftCardStatusesId
     */
    public function getGiftCardStatusesId()
    {
        return $this->giftCardStatusesId;
    }

    /**
     * @param number $giftCardStatusesId
     * @return GiftCardStolenEntity
     */
    public function setGiftCardStatusesId($giftCardStatusesId)
    {
        $this->giftCardStatusesId = $giftCardStatusesId;
        return $this;
    }
}
