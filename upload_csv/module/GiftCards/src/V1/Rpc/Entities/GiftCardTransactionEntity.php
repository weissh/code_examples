<?php
namespace GiftCards\V1\Rpc\Entities;

/**
 * The Entities for gift_cards_transactions table
 *
 * @author haniw
 *
 */
class GiftCardTransactionEntity extends AbstractEntity
{

    /**
     *
     * @var int
     */
    protected $giftCardsTransactionId;

    /**
     *
     * @var int
     */
    protected $cardId;

    /**
     *
     * @var string
     */
    protected $channelCode;

    /**
     *
     * @var int
     */
    protected $transactionTypeId;

    /**
     *
     * @var float
     */
    protected $amount;

    /**
     *
     * @var float
     */
    protected $conversionRate;

    /**
     *
     * @var string
     */
    protected $requestFrom;

    /**
     *
     * @var $string
     */
    protected $created;

    /**
     *
     * @var string
     */
    protected $currencyCode = '';

    /**
     *
     * @var string
     */
    protected $externalId;


    /**
     *
     * @return string $currencyCode
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     * @return GiftCardTransactionEntity
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
        return $this;
    }

    /**
     *
     * @return int $giftCardsTransactionId
     */
    public function getGiftCardsTransactionId()
    {
        return $this->giftCardsTransactionId;
    }

    /**
     *
     * @return int $cardId
     */
    public function getCardId()
    {
        return $this->cardId;
    }

    /**
     *
     * @return int $channelCode
     */
    public function getChannelCode()
    {
        return $this->channelCode;
    }

    /**
     *
     * @return int $transactionTypeId
     */
    public function getTransactionTypeId()
    {
        return $this->transactionTypeId;
    }

    /**
     *
     * @return float $amount
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     *
     * @return float $conversionRate
     */
    public function getConversionRate()
    {
        return $this->conversionRate;
    }

    /**
     *
     * @return string $requestFrom
     */
    public function getRequestFrom()
    {
        return $this->requestFrom;
    }

    /**
     *
     * @return string $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $giftCardsTransactionId
     * @return GiftCardTransactionEntity
     */
    public function setGiftCardsTransactionId($giftCardsTransactionId)
    {
        $this->giftCardsTransactionId = $giftCardsTransactionId;
        return $this;
    }

    /**
     * @param int $cardId
     * @return GiftCardTransactionEntity
     */
    public function setCardId($cardId)
    {
        $this->cardId = $cardId;
        return $this;
    }

    /**
     *
     * @param string $channelCode
     * @return GiftCardTransactionEntity
     */
    public function setChannelCode($channelCode)
    {
        $this->channelCode = $channelCode;
        return $this;
    }

    /**
     * @param $transactionTypeId
     * @return GiftCardTransactionEntity
     */
    public function setTransactionTypeId($transactionTypeId)
    {
        $this->transactionTypeId = $transactionTypeId;
        return $this;
    }

    /**
     * @param float $amount
     * @return GiftCardTransactionEntity
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     *
     * @param float $conversionRate
     * @return GiftCardTransactionEntity
     */
    public function setConversionRate($conversionRate)
    {
        $this->conversionRate = $conversionRate;
        return $this;
    }

    /**
     *
     * @param string $requestFrom
     * @return GiftCardTransactionEntity
     */
    public function setRequestFrom($requestFrom)
    {
        $this->requestFrom = $requestFrom;
        return $this;
    }

    /**
     *
     * @param string $created
     * @return GiftCardTransactionEntity
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }
    /**
     * @return string $externalId
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

}
