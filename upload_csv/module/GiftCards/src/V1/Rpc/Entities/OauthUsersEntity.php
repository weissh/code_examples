<?php
namespace GiftCards\V1\Rpc\Entities;

/**
 * OauthUsersEntity entity
 */
class OauthUsersEntity extends AbstractEntity
{
    /**
     * @var string
     */
    protected $username;
    /**
     * @var string
     */
    protected $password;
    /**
     * @var string
     */
    protected $firstName;
    /**
     * @var string
     */
    protected $lastName;
    /**
     * @var string
     */
    protected $role;

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param $firstName
     * @return OauthUsersEntity
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @param $lastName
     * @return OauthUsersEntity
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @param $password
     * @return OauthUsersEntity
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param $role
     * @return OauthUsersEntity
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @param $username
     * @return OauthUsersEntity
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }
}
