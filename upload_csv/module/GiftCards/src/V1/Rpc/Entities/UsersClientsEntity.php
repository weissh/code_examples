<?php
namespace GiftCards\V1\Rpc\Entities;

/**
 * UsersClients entity
 */
class UsersClientsEntity extends AbstractEntity
{
    
    /**
     * 
     * @var int
     */
    protected $userClientId;
    /**
     * 
     * @var string
     */
    protected $clientCode;
    /**
     *
     * @var string
     */
    protected $userName;
    /**
     *
     * @var string
     */
    protected $status;
   
    /**
     *
     * @var string datetime
     */
    protected $created;
    /**
     *
     * @var string datetime
     */
    protected $modified;
    
    /**
     * @return int $userClientId
     */
    public function getUserClientId()
    {
        return $this->userClientId;
    }

    /**
     * @return string $clientCode
     */
    public function getClientCode()
    {
        return $this->clientCode;
    }

    /**
     * @return string $userName
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return string $modified
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param int $userClientId
     */
    public function setUserClientId($userClientId)
    {
        $this->userClientId = $userClientId;
    }

    /**
     * @param string $clientCode
     */
    public function setClientCode($clientCode)
    {
        $this->clientCode = $clientCode;
    }

    /**
     * @param string $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @param string $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @param string $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

}
