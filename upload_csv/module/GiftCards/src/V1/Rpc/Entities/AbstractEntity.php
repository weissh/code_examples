<?php

namespace GiftCards\V1\Rpc\Entities;

/**
 * Shared utils for entity
 */
abstract class AbstractEntity implements HalExtractInterface
{

    /**
     * Will remove NULL values and empty arrays.
     * @var boolean
     */
    protected $forceCleanup = FALSE;
    
    
   /**
    * Extract protected method using getters
    * Function is able to clean unused (empty) values
    * @return array
    */ 
   public function toArray() {
       
       $result = Array();
       
       foreach (get_object_vars($this) as $name => $val) {
                
            $method = 'get' . ucfirst($name); // getter 

            if (!method_exists($this, $method)) { // only getter access allowed
                continue;
            }
            
            // don't export blank values, if requested
            if( $this->forceCleanup && empty( $this->$method()) ) {
                continue;
            }
            
            $result[$name] = $this->$method(); 
            
            // if value is not scalar, try to extract
            if($result[$name] instanceof HalExtractInterface) {
                
                if($this->forceCleanup) {
                   $result[$name]->enforceCleanup();
                } else {
                   $result[$name]->disableCleanup(); 
                }
                
                $result[$name] = $result[$name]->toArray(); 

                if($this->forceCleanup && empty($result[$name]) ) {
                    unset($result[$name]);
                }
 
            }

            
        }

        return $result;
       
   }
   
   /**
    * Will remove NULL values and empty arrays on toArray conversion.
    */
   public function enforceCleanup() {
       $this->forceCleanup = TRUE;
   }
   
   /**
    * Will preserve all values, including nulls
    */
   public function disableCleanup() {
       $this->forceCleanup = FALSE;
   }

   /**
    * 
    * @return boolean
    */
   public function getCleanupFlag() {
       return $this->forceCleanup;
   }



}
