<?php

namespace GiftCards\V1\Rpc\Entities;

/**
 * Client Settings entity
 */
class ClientSettingsUpdateEntity extends AbstractEntity
{
  
    /**
     * 
     * @var int
     */
    protected $clientSettingId;
    
    /**
     *
     * @var string
     */
    protected $clientCode;
    
    /**
     *
     * @var int
     */
    protected $expiryPeriodUnitLookupId = 1;
    
    /**
     *
     * @var int
     */
    protected $expiryPeriodQuantity;
    
    /**
     *
     * @var int
     */
    protected $restrictToCurrency;
    
    /**
     *
     * @var int
     */
    protected $multiChannelSupport;
    
    /**
     *
     * @var int
     */
    protected $redemptionSupport;
    
    /**
     *
     * @var int
     */
    protected $autoExpiryOnZeroBalance;
    
    /**
     *
     * @var int
     */
    protected $allowRefundToZeroBalance;
    
    /**
     *
     * @var int
     */
    protected $redemptionMaximumCardNumberUsed;
    
    /**
     *
     * @var float
     */
    protected $minimumInitialLoad;
    
    /**
     *
     * @var float
     */
    protected $maximumInitialLoad;
    
    /**
     *
     * @var int
     */
    protected $logAction = 1;
    
    /**
     *
     * @var string MYSQL date
     */
    protected $created;
    
    /**
     * 
     * @var string
     */
    protected $clientSettingsStatus = 'ACTIVE';
    
    /**
     *
     * @var string
     */
    protected $barcodeValidationRule = null;
    
    /**
     * @return int $clientSettingId
     */
    public function getClientSettingId()
    {
        return $this->clientSettingId;
    }

    /**
     * @return string $clientCode
     */
    public function getClientCode()
    {
        return $this->clientCode;
    }

    /**
     * @return string $expiryPeriodUnitLookupId
     */
    public function getExpiryPeriodUnitLookupId()
    {
        return $this->expiryPeriodUnitLookupId;
    }

    /**
     * @return int $expiryPeriodQuantity
     */
    public function getExpiryPeriodQuantity()
    {
        return $this->expiryPeriodQuantity;
    }

    /**
     * @return int $restrictToCurrency
     */
    public function getRestrictToCurrency()
    {
        return $this->restrictToCurrency;
    }

    /**
     * @return int $multiChannelSupport
     */
    public function getMultiChannelSupport()
    {
        return $this->multiChannelSupport;
    }

    /**
     * @return int $redemptionSupport
     */
    public function getRedemptionSupport()
    {
        return $this->redemptionSupport;
    }

    /**
     * @return int $autoExpiryOnZeroBalance
     */
    public function getAutoExpiryOnZeroBalance()
    {
        return $this->autoExpiryOnZeroBalance;
    }

    /**
     * @return int $allowRefundToZeroBalance
     */
    public function getAllowRefundToZeroBalance()
    {
        return $this->allowRefundToZeroBalance;
    }

    /**
     * @return int $redemptionMaximumCardNumberUsed
     */
    public function getRedemptionMaximumCardNumberUsed()
    {
        return $this->redemptionMaximumCardNumberUsed;
    }

    /**
     * @return float $minimumInitialLoad
     */
    public function getMinimumInitialLoad()
    {
        return $this->minimumInitialLoad;
    }

    /**
     * @return float $maximumInitialLoad
     */
    public function getMaximumInitialLoad()
    {
        return $this->maximumInitialLoad;
    }

    /**
     * @return int $logAction
     */
    public function getLogAction()
    {
        return $this->logAction;
    }

    /**
     * @return string $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param number $clientSettingId
     */
    public function setClientSettingId($clientSettingId)
    {
        $this->clientSettingId = $clientSettingId;
    }

    /**
     * @param string $clientCode
     */
    public function setClientCode($clientCode)
    {
        $this->clientCode = $clientCode;
    }

    /**
     * @param number $expiryPeriodUnitLookupId
     */
    public function setExpiryPeriodUnitLookupId($expiryPeriodUnitLookupId)
    {
        $this->expiryPeriodUnitLookupId = $expiryPeriodUnitLookupId;
    }

    /**
     * @param number $expiryPeriodQuantity
     */
    public function setExpiryPeriodQuantity($expiryPeriodQuantity)
    {
        $this->expiryPeriodQuantity = $expiryPeriodQuantity;
    }

    /**
     * @param number $restrictToCurrency
     */
    public function setRestrictToCurrency($restrictToCurrency)
    {
        $this->restrictToCurrency = $restrictToCurrency;
    }

    /**
     * @param number $multiChannelSupport
     */
    public function setMultiChannelSupport($multiChannelSupport)
    {
        $this->multiChannelSupport = $multiChannelSupport;
    }

    /**
     * @param number $redemptionSupport
     */
    public function setRedemptionSupport($redemptionSupport)
    {
        $this->redemptionSupport = $redemptionSupport;
    }

    /**
     * @param number $autoExpiryOnZeroBalance
     */
    public function setAutoExpiryOnZeroBalance($autoExpiryOnZeroBalance)
    {
        $this->autoExpiryOnZeroBalance = $autoExpiryOnZeroBalance;
    }

    /**
     * @param number $allowRefundToZeroBalance
     */
    public function setAllowRefundToZeroBalance($allowRefundToZeroBalance)
    {
        $this->allowRefundToZeroBalance = $allowRefundToZeroBalance;
    }

    /**
     * @param number $redemptionMaximumCardNumberUsed
     */
    public function setRedemptionMaximumCardNumberUsed($redemptionMaximumCardNumberUsed)
    {
        $this->redemptionMaximumCardNumberUsed = $redemptionMaximumCardNumberUsed;
    }

    /**
     * @param number $minimumInitialLoad
     */
    public function setMinimumInitialLoad($minimumInitialLoad)
    {
        $this->minimumInitialLoad = $minimumInitialLoad;
    }

    /**
     * @param number $maximumInitialLoad
     */
    public function setMaximumInitialLoad($maximumInitialLoad)
    {
        $this->maximumInitialLoad = $maximumInitialLoad;
    }

    /**
     * @param number $logAction
     */
    public function setLogAction($logAction)
    {
        $this->logAction = $logAction;
    }

    /**
     * @param string $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }
    
    /**
     * @return int $clientSettingsStatus
     */
    public function getClientSettingsStatus()
    {
        return $this->clientSettingsStatus;
    }

    /**
     * @param number $clientSettingsStatus
     */
    public function setClientSettingsStatus($clientSettingsStatus)
    {
        $this->clientSettingsStatus = $clientSettingsStatus;
    }
    /**
     * @return string $barcodeValidationRule
     */
    public function getBarcodeValidationRule()
    {
        return $this->barcodeValidationRule;
    }

    /**
     * @param string $barcodeValidationRule
     */
    public function setBarcodeValidationRule($barcodeValidationRule)
    {
        $this->barcodeValidationRule = $barcodeValidationRule;
    }
    
}
