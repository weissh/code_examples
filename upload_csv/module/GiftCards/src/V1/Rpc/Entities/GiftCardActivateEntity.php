<?php
namespace GiftCards\V1\Rpc\Entities;

class GiftCardActivateEntity extends AbstractEntity
{
    /**
     *
     * @var string /^[a-zA-Z0-9 .\\\\-]+$/
     */
    protected $barcode;

    /**
     *
     * @var string
     */
    protected $clientCode;

    /**
     *
     * @var int
     */
    protected $giftCardStatusesId;

    /**
     * @param int $barcode
     * @return GiftCardActivateEntity
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @param string $clientCode
     * @return GiftCardActivateEntity
     */
    public function setClientCode($clientCode)
    {
        $this->clientCode = $clientCode;
        return $this;
    }

    /**
     * @return int $clientId
     */
    public function getClientCode()
    {
        return $this->clientCode;
    }

    /**
     * @return int
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @return int $giftCardStatusesId
     */
    public function getGiftCardStatusesId()
    {
        return $this->giftCardStatusesId;
    }

    /**
     * @param int $giftCardStatusesId
     * @return GiftCardActivateEntity
     */
    public function setGiftCardStatusesId($giftCardStatusesId)
    {
        $this->giftCardStatusesId = $giftCardStatusesId;
        return $this;
    }
}
