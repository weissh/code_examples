<?php
namespace GiftCards\V1\Rpc\Entities\SpecialCases;

use GiftCards\V1\Rpc\Entities\GiftCardGetBalanceEntity;

/**
 * Not Existing client entity
 */
class MissingGiftCardGetBalanceEntity extends GiftCardGetBalanceEntity
{
}
