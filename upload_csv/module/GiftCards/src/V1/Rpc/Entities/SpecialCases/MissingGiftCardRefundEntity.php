<?php

namespace GiftCards\V1\Rpc\Entities\SpecialCases;

use GiftCards\V1\Rpc\Entities\GiftCardRefundEntity;

class MissingGiftCardRefundEntity extends GiftCardRefundEntity
{}
