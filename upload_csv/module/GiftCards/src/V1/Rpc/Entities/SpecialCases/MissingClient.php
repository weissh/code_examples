<?php

namespace GiftCards\V1\Rpc\Entities\SpecialCases;

use GiftCards\V1\Rpc\Entities\ClientEntity;

/**
 * Not Existing client entity
 */
class MissingClient extends ClientEntity
{}
