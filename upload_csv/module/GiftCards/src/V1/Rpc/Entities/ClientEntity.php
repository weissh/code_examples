<?php

namespace GiftCards\V1\Rpc\Entities;

use GiftCards\V1\Rpc\Services\Paginator\Adapters\DummyAdapter;


/**
 * Client entity
 */
class ClientEntity extends AbstractEntity
{

    /**
     *
     * @var string
     */
    protected $id = '';

    /**
     *
     * @var string
     */
    protected $name = '';

    /**
     *
     * @var string
     */
    protected $status = '';

    /**
     *
     * @var ClientSettingsEntity
     */
    protected $settings;

    /**
     *
     * @var AbstractCollection
     */
    protected $channels;

    /**
     *
     * @var AbstractCollection
     */
    protected $currencies;
    
    /**
     * Defaults - to always match interface
     */
    public function __construct() {
        $this->setSettings( new ClientSettingsEntity() );
        $this->setChannels( new DummyCollection( new DummyAdapter() ));
        $this->setCurrencies( new DummyCollection( new DummyAdapter() ));
    }

    /**
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     *
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     *
     * @return ClientSettingsEntity
     */
    public function getSettings() {
        return $this->settings;
    }

    /**
     *
     * @return ChannelsCollection
     */
    public function getChannels() {
        return $this->channels;
    }

    /**
     *
     * @return CurrenciesCollection
     */
    public function getCurrencies() {
        return $this->currencies;
    }

    /**
     *
     * @param string $id
     * @return ClientEntity
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @param string $name
     * @return ClientEntity
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @param string $status
     * @return ClientEntity
     */
    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    /**
     *
     * @param ClientSettingsEntity $settings
     * @return ClientEntity
     */
    public function setSettings(ClientSettingsEntity $settings) {
        $this->settings = $settings;
        return $this;
    }

    /**
     *
     * @param AbstractCollection $channels
     * @return ClientEntity
     */
    public function setChannels(AbstractCollection $channels) {
        $this->channels = $channels;
        return $this;
    }

    /**
     *
     * @param AbstractCollection $currencies
     * @return ClientEntity
     */
    public function setCurrencies(AbstractCollection $currencies) {
        $this->currencies = $currencies;
        return $this;
    }
}
