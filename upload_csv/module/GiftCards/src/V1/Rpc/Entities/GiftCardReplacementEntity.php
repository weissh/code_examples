<?php

namespace GiftCards\V1\Rpc\Entities;

/**
 * Gift Card Replacement Entity
 */
class GiftCardReplacementEntity extends AbstractEntity
{
    /**
     *
     * @var string /^[a-zA-Z0-9 .\\\\-]+$/
     */
    protected $id;

    /**
     * @var int
     */
    protected $oldBarcode;

    /**
     * @var int
     */
    protected $newBarcode;

    /**
     *
     * @var string mysql datetime
     */
    protected $created;

    /**
     *
     * @param int $id
     * @return GiftCardReplacementEntity
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @param int $oldBarcode
     * @return GiftCardReplacementEntity
     */
    public function setOldBarcode($oldBarcode)
    {
        $this->oldBarcode = $oldBarcode;
        return $this;
    }

    /**
     * @return int
     */
    public function getOldBarcode()
    {
        return $this->oldBarcode;
    }

    /**
     *
     * @param int $newBarcode
     * @return GiftCardReplacementEntity
     */
    public function setNewBarcode($newBarcode)
    {
        $this->newBarcode = $newBarcode;
        return $this;
    }

    /**
     * @return int
     */
    public function getNewBarcode()
    {
        return $this->newBarcode;
    }

    /**
     * @param string $created
     * @return GiftCardReplacementEntity
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }
}
