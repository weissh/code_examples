<?php

namespace GiftCards\V1\Rpc\Entities;

class BarcodeEntity extends AbstractEntity
{
    /**
     * 
     * @var string
     */
    protected $barcode;
    /**
     * 
     * @var string
     */
    protected $clientCode;
    /**
     * 
     * @var string
     */
    protected $created;
    /**
     * 
     * @var string
     */
    protected $modified;
    /**
     * @return string $barcode
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @return string $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return string $modified
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param string $barcode
     * @return BarcodeEntity
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @param string $created
     * @return BarcodeEntity
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @param string $modified
     * @return BarcodeEntity
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
        return $this;
    }
    
    /**
     * @return string $clientCode
     */
    public function getClientCode()
    {
        return $this->clientCode;
    }

    /**
     * @param string $clientCode
     * @return BarcodeEntity
     */
    public function setClientCode($clientCode)
    {
        $this->clientCode = $clientCode;
        return $this;
    }

}