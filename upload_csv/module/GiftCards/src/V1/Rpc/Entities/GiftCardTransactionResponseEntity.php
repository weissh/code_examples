<?php
namespace GiftCards\V1\Rpc\Entities;

/**
 * The entities from API call /gift-card-transaction
 * 
 * @author haniw
 *        
 */
class GiftCardTransactionResponseEntity extends AbstractEntity
{
    /**
     * 
     * @var int
     */
    protected $transactionId;

    /**
     *
     * @var string /^[a-zA-Z0-9 .\\\\-]+$/
     */
    protected $barcode = '';

    /**
     *
     * @var float
     */
    protected $currentBalance;

    /**
     * To be used with validation gift card against client settings
     *
     * @var boolean
     */
    protected $isPasstValidation;
    
    /**
     * 
     * @var float
     */
    protected $paymentTaken;
    
    /**
     * 
     * @var float
     */
    protected $paymentRemaining;
    
    /**
     * 
     * @var string
     */
    protected $currencyCode;
    
    /**
     * 
     * @var string
     */
    protected $externalId;
    
    /**
     * @return int $transactionId
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param int $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return string $barcode
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @return float $currentBalance
     */
    public function getCurrentBalance()
    {
        return $this->currentBalance;
    }

    /**
     * @return int $isPasstValidation
     */
    public function getIsPasstValidation()
    {
        return $this->isPasstValidation;
    }

    /**
     * @return int $paymentTaken
     */
    public function getPaymentTaken()
    {
        return $this->paymentTaken;
    }

    /**
     * @return float $paymentRemaining
     */
    public function getPaymentRemaining()
    {
        return $this->paymentRemaining;
    }

    /**
     * @return string $currencyCode
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param string $barcode
     * @return GiftCardTransactionResponseEntity
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @param float $currentBalance
     * @return GiftCardTransactionResponseEntity
     */
    public function setCurrentBalance($currentBalance)
    {
        $this->currentBalance = $currentBalance;
        return $this;
    }

    /**
     * @param boolean $isPasstValidation
     * @return GiftCardTransactionResponseEntity
     */
    public function setIsPasstValidation($isPasstValidation)
    {
        $this->isPasstValidation = $isPasstValidation;
        return $this;
    }

    /**
     * @param float $paymentTaken
     * @return GiftCardTransactionResponseEntity
     */
    public function setPaymentTaken($paymentTaken)
    {
        $this->paymentTaken = $paymentTaken;
        return $this;
    }

    /**
     * @param float $paymentRemaining
     * @return GiftCardTransactionResponseEntity
     */
    public function setPaymentRemaining($paymentRemaining)
    {
        $this->paymentRemaining = $paymentRemaining;
        return $this;
    }

    /**
     * @param string $currencyCode
     * @return GiftCardTransactionResponseEntity
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
        return $this;
    }
    /**
     * @return string $externalId
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }
}
