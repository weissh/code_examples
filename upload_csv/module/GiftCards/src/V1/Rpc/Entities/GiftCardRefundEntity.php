<?php
namespace GiftCards\V1\Rpc\Entities;

/**
 * The entities from API call /gift-card-transaction
 *
 * @author haniw
 *        
 */
class GiftCardRefundEntity extends AbstractEntity
{
    /**
     *
     * @var int
     */
    protected $transactionId;

    /**
     *
     * @var string /^[a-zA-Z0-9 .\\\\-]+$/
     */
    protected $barcode;


    /**
     *
     * @var float
     */
    protected $currentBalance;

    /**
     *
     * @var string
     */
    protected $currencyCode;

    /**
     *
     * @var float
     */
    protected $refundedAmount;
    
    /**
     *
     * @var string
     */
    protected $externalId;

    /**
     *
     * @return the $barcode
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     *
     * @return float $currentBalance
     */
    public function getCurrentBalance()
    {
        return $this->currentBalance;
    }

    /**
     *
     * @return string $currencyCode
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     *
     * @return float $refundedAmount
     */
    public function getRefundedAmount()
    {
        return $this->refundedAmount;
    }

    /**
     *
     * @param string $barcode
     * @return GiftCardRefundEntity
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     *
     * @param float $currentBalance
     * @return GiftCardRefundEntity
     */
    public function setCurrentBalance($currentBalance)
    {
        $this->currentBalance = $currentBalance;
        return $this;
    }


    /**
     *
     * @param string $currencyCode
     * @return GiftCardRefundEntity
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
        return $this;
    }

    /**
     *
     * @param unknown $refundedAmount
     * @return GiftCardRefundEntity
     */
    public function setRefundedAmount($refundedAmount)
    {
        $this->refundedAmount = $refundedAmount;
        return $this;
    }
    /**
     * @return int $transactionId
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @return string $externalId
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param number $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

}
