<?php
namespace GiftCards\V1\Rpc\Entities;

class TransactionTypeEntity extends AbstractEntity
{

    /**
     *
     * @var int
     */
    protected $transactionTypeId;

    /**
     *
     * @var string
     */
    protected $transactionName;

    /**
     *
     * @var string
     */
    protected $creditOrDebit;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string mysql datetime
     */
    protected $created;

    /**
     *
     * @return int $transactionTypeId
     */
    public function getTransactionTypeId()
    {
        return $this->transactionTypeId;
    }

    /**
     *
     * @return string $transactionName
     */
    public function getTransactionName()
    {
        return $this->transactionName;
    }

    /**
     *
     * @return string $creditOrDebit
     */
    public function getCreditOrDebit()
    {
        return $this->creditOrDebit;
    }

    /**
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     *
     * @return string $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     *
     * @param int $transactionTypeId
     * @return TransactionTypeEntity
     */
    public function setTransactionTypeId($transactionTypeId)
    {
        $this->transactionTypeId = $transactionTypeId;
        return $this;
    }

    /**
     *
     * @param string $transactionName
     * @return TransactionTypeEntity
     */
    public function setTransactionName($transactionName)
    {
        $this->transactionName = $transactionName;
        return $this;
    }

    /**
     *
     * @param string $creditOrDebit
     * @return TransactionTypeEntity
     */
    public function setCreditOrDebit($creditOrDebit)
    {
        $this->creditOrDebit = $creditOrDebit;
        return $this;
    }

    /**
     *
     * @param string $status
     * @return TransactionTypeEntity
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     *
     * @param string $created
     * @return TransactionTypeEntity
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }
}
