<?php

namespace GiftCards\V1\Rpc\Services\Paginator\Adapters;

use Laminas\Paginator\Adapter\ArrayAdapter;


/**
 * Dummy paginator with overwritten getItems()
 */
class DummyAdapter extends ArrayAdapter
{

    /**
     * No content (or some test content)
     */
    public function __construct( $content = Array() ) {
        parent::__construct( $content );
    }

    /**
     * Dummy items generation
     * Compatible with AdapterInterface
     * @param int $offset
     * @param int $itemCountPerPage
     * @return array
     */
    public function getItems($offset = 0, $itemCountPerPage = 99999999)
    {
        return parent::getItems($offset, $itemCountPerPage);
    }
}
