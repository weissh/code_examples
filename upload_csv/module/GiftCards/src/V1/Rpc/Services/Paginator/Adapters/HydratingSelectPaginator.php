<?php

namespace GiftCards\V1\Rpc\Services\Paginator\Adapters;

use stdClass;

use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Sql;
use Laminas\Paginator\Adapter\DbSelect as SelectPaginator;
use Laminas\Hydrator\HydratorInterface;

/**
 * Specialized Zend\Paginator\Adapter\DbSeelect adapter instance for returning
 * hydrated entities.
 */
class HydratingSelectPaginator extends SelectPaginator
{
    /**
     * @var object
     */
    protected $entityPrototype;
    
    /**
     * @var HydratorInterface
     */
    protected $hydrator;
  
    
    /**
     * @param Select $select
     * @param Sql $sql
     * @param HydratorInterface $hydrator
     * @param null|mixed $entityPrototype A prototype entity to use with the hydrator
     */
    public function __construct(Select $select,Sql $sql,  HydratorInterface $hydrator, $entityPrototype = null)
    {
        parent::__construct($select, $sql);
        $this->hydrator = $hydrator;
        $this->entityPrototype = $entityPrototype ?: new stdClass;
    }

    
    /**
     * Map result set to collection of items (entity collection)
     * @param int $offset = 0
     * @param int $itemCountPerPage = NULL
     * @return array
     */
    public function getItems($offset = 0, $itemCountPerPage = NULL)
    {
        
        // pull data from DB
        $select = clone $this->select;
        $select->offset($offset);
        
        if($itemCountPerPage) {
            $select->limit($itemCountPerPage);
        }

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();
       
        // map result to entity collection
        $collection = Array();
        foreach ($result as $item) {
            $collection[] =  $this->hydrator->hydrate($item, clone $this->entityPrototype);
        }
        
        return $collection;

    }
}
