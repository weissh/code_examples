<?php
namespace GiftCards\V1\Rpc\Services\Identification;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * 
 * @author haniw
 *        
 */
class Identification extends AbstractPlugin
{
    protected $identity;
    
    public function __construct($identity){
        $this->identity = (object) $identity;
        return $this->identity;
    }
    
    public function getClientId(){
        if (property_exists($this->identity, 'client_id')) {
            return $this->identity->client_id;
        }
    }
    
    public function getUserId(){
        if (property_exists($this->identity, 'user_id')) {
            return $this->identity->user_id;
        }
    }
    
    public function getAccessToken(){
        if (property_exists($this->identity, 'access_token')) {
            return $this->identity->access_token;
        }
    }
    
    public function getExpires(){
        if (property_exists($this->identity, 'expires')) {
            return $this->identity->expires;
        }
    }
    
    public function getScope(){
        if (property_exists($this->identity, 'scope')) {
            return $this->identity->scope;
        }
    }
    
    
}
