<?php
namespace GiftCards\V1\Rpc\Services\Identification;

use GiftCards\V1\Rpc\Mappers\UsersClientsMapper;
use GiftCards\V1\Rpc\Mappers\OauthUsersMapper;
use Zend\Db\Adapter\Adapter;

/**
 * 
 * @author haniw
 * 
 * 
 *        
 */
class Permissions
{
    /**
     * 
     * @var string
     */
    protected $userId;
    
    /**
     * 
     * @var string
     */
    protected $clientId;

    /**
     * @var Adapter
     */
    protected $adapter;

    /**
     * @var OauthUsersMapper
     */
    protected $oauthUsersMapper;
    
    /**
     * 
     * @var UsersClientsMapper
     */
    protected $usersClientsMapper;

    /**
     * Permissions constructor.
     * @param OauthUsersMapper $oauthUsersMapper
     * @param $usersClientsMapper
     */
    public function __construct(OauthUsersMapper $oauthUsersMapper, $usersClientsMapper){
        $this->oauthUsersMapper = $oauthUsersMapper;
        $this->usersClientsMapper = $usersClientsMapper;
    }

    /**
     * @params string|null $userId
     *
     * @return string role of logged user by 'role' field in oauth_users table
     * with passed $userId which is a 'username' in oauth_users
     *
     */
    public function getRole($userId = null)
    {
        // just in case in other app parts, $userId need to be passed to grab the role
        // by the userId
        // You need to know, that for 'authorization' part in Module::onBootstrap,
        // the setUserId() already called when this class call isUserAllowedToClient()
        // before this function called.
        if (null !== $userId) {
            $this->setUserId($userId);
        }

        return $this->oauthUsersMapper->getRole($this->getUserId());
    }
    
    /**
     * @return the $userId
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return the $clientId
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param string $userId
     * @return self
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @param string $clientId
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }
    
    /**
     * Check if the user have permission to manipulate with client gift cards or to use the API call
     * @param string $userId
     * @param string $clientId
     */
    public function isUserAllowedToClient($userId, $clientId)
    {
        $isAllowed  = false;
        
        $this->setUserId($userId);
        $this->setClientId($clientId);
        
        if($this->usersClientsMapper->isUsersHasPermissionToClientDetails($this->getUserId(), $this->getClientId()) === true){
            $isAllowed  = true;
        }
        
        return $isAllowed;
    }
    
    
    
}
