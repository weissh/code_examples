<?php

namespace GiftCards\V1\Rpc\Services\Exceptions;

use Exception;

/**
 * Exception specific for Rpc API calls, using predefined responses.
 *
 * @author hw
 */
class ApiException extends Exception {

    
    const RECORD_NOT_FOUND = [
        "response_code" => "404",
        "response_message" => "We could not find any record for given parameter."
    ];
    
    const GENERAL_FAILURE = [
        "response_code" => "500",
        "response_message" => "Internal error."
    ];


    /**
     * ApiException constructor.
     * @param array $error
     */
    public function __construct(array $error = self::GENERAL_FAILURE) {
        
        $code = empty($error['response_code']) ? NULL : $error['response_code'];
        $msg = empty($error['response_message']) ? NULL : $error['response_message'];
        
        parent::__construct($msg, $code);
        
    }

}
