<?php

namespace GiftCards\V1\Rpc\Services\Files\Exceptions;

use Exception;

/**
 * Exception thrown during the Csv file parsing
 *
 * @author hw
 */
class CsvException extends Exception {
    
    // default CSV response code
    protected $code = 585;  
    
}
