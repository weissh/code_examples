<?php

namespace GiftCards\V1\Rpc\Services\Files\Bulks;



/**
 * Manipulation with Gift Card bulk creation
 *
 * @author hw
 */
class CardsAddFile extends BulkAbstract
{
    
    /**
     * Final file destination folder prototype
     * @var string 
     */
    protected $destinationDirProto = '/data/bulk/cardsadd/{year}/{month}/{day}/';
    
    /**
     * Final file name
     * @var string 
     */
    protected $destinationFileProto = '{filename}_{timestamp}';
    
    /**
     * Map of CSV fields into Array keys
     * @var array 
     */
    protected $csvMapping = [
            0 => 'barcode',
            1 => 'currency_code',
            2 => 'initial_load',
            3 => 'start_date',
            4 => ['channels'], // does mean, that output should be array
            5 => 'is_physical',
            6 => 'promo_code',
            7 => 'expiry_period_in_month',
    ];
    
    /**
     * Delimiter of arrays in csv
     * @var string 
     */
    protected $csvArrayDelimiter = '|';

}
