<?php

namespace GiftCards\V1\Rpc\Services\Files\Interfaces;

/**
 * Classes with this interface can save file to it's final destination
 * @author hw
 */
interface StorageAwareInterface {
    
    /**
     * Move file to it's final destination
     * @throws Exception
     */
    public function moveToStorage();
    
    
}
