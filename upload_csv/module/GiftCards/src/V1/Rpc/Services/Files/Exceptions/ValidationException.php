<?php

namespace GiftCards\V1\Rpc\Services\Files\Exceptions;

use Exception;

/**
 * Exception Wrapper for input filter validation errors
 *
 * @author hw
 */
class ValidationException extends Exception {
    
    /**
     *
     * @var array 
     */
    protected $validationErrors = Array();
    
    /**
     * Enter output from InputValidator->getMessages()
     * @param array $validationErrors
     */
    public function __construct($validationErrors = Array()) {
        $this->validationErrors = $validationErrors;
        
        parent::__construct();
    }
    
    /**
     * 
     * @return array
     */
    public function getValidationErrors() {
        return $this->validationErrors;
    }

    /**
     * 
     * @param array $validationErrors
     */
    public function setValidationErrors(array $validationErrors) {
        $this->validationErrors = $validationErrors;
    }
    
}
