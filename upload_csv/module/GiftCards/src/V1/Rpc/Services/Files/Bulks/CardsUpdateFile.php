<?php

namespace GiftCards\V1\Rpc\Services\Files\Bulks;



/**
 * Manipulation with Gift Card bulk creation
 *
 * @author hw
 */
class CardsUpdateFile extends BulkAbstract
{
    
    /**
     * Final file destination folder prototype
     * @var string 
     */
    protected $destinationDirProto = '/data/bulk/cardsupdate/{year}/{month}/{day}/';
    
    /**
     * Final file name
     * @var string 
     */
    protected $destinationFileProto = '{filename}_{timestamp}';
    
    /**
     * Map of CSV fields into Array keys
     * @var array 
     */
    protected $csvMapping = [
            0 => 'barcode',
            1 => 'start_date',
            2 => 'end_date',
            3 => ['channels'], // does mean, that output should be array
            4 => 'is_physical',
    ];
    
    /**
     * Delimiter of arrays in csv
     * @var string 
     */
    protected $csvArrayDelimiter = '|';
    
}
