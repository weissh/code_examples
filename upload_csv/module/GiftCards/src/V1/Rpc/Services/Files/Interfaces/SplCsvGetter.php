<?php

namespace GiftCards\V1\Rpc\Services\Files\Interfaces;

/**
 * Classes with this interface can provide file content as Spl object, set for CSV parsing
 * @author hw
 */
interface SplCsvGetter {
    
    /**
     * Returns traversable file descriptior as SplFileObject <br>
     * WARNING: once file is open by this function, you wont be able to manipulate it in the FileSystem !
     * @return SplFileObject
     */
    public function getAsSplFile();
    
    
}
