<?php

namespace GiftCards\V1\Rpc\Services\Files\Interfaces;

/**
 * Classes with this interface can save file to it's final destination
 * @author hw
 */
interface CsvLineMapper {
    
    /**
     * Swap numeric keys into named keys 
     * @param array $line
     * @return array
     */
    public function lineToArray(array $line);
    
    
}
