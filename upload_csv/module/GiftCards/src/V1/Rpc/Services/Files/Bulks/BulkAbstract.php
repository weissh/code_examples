<?php

namespace GiftCards\V1\Rpc\Services\Files\Bulks;

use GiftCards\V1\Rpc\Services\Files\Exceptions\CsvException;
use GiftCards\V1\Rpc\Services\Files\Exceptions\FileManipulationException;
use GiftCards\V1\Rpc\Services\Files\File;
use GiftCards\V1\Rpc\Services\Files\Interfaces\CsvLineMapper;
use GiftCards\V1\Rpc\Services\Files\Interfaces\StorageAwareInterface;

/**
 * Manipulation with any bulk file
 *
 * @author hw
 */
abstract class BulkAbstract extends File
                        implements StorageAwareInterface, CsvLineMapper
{
    
    /**
     * Final default file destination folder prototype
     * @var string 
     */
    protected $destinationDirProto = '/data/bulk/default/{year}/{month}/{day}/';
    
    /**
     * Final default file name
     * @var string 
     */
    protected $destinationFileProto = '{filename}_{timestamp}';
    
    /**
     * Map of CSV fields into Array keys - default is empty
     * @var array 
     */
    protected $csvMapping = NULL;
    
    /**
     * Delimiter of arrays in csv - default
     * @var string 
     */
    protected $csvArrayDelimiter = '|';
    
    /**
     * Move add bulk file to it's final destination
     * Funtion is using following placeholders in any combination: {year}|{month}|{day}|{timestamp}|{filename}
     * @throws FileManipulationException
     */
    public function moveToStorage() {
        
        // prepare new paths
        $folder =  str_replace('{year}', Date('Y') , $this->destinationDirProto);
        $folder =  str_replace('{month}', Date('m') ,$folder);
        $folder =  str_replace('{day}', Date('d') ,$folder);
        
        // move
        $this->moveToFolder(APPLICATION_PATH . $folder);
        
        // prepare name
        $name = str_replace('{timestamp}', time() , $this->destinationFileProto);
        $name = str_replace('{filename}', $this->getName() , $name);
        
        // rename
        $this->rename($name);

    }
    
    /**
     * Swap numeric keys into named keys 
     * @param array $line
     * @return array
     * @throws CsvException
     */
    public function lineToArray(array $line) {
        
        $this->checkCsvMap();

        // handle user who submit via generated/sample csv file
        // which he/she open in ms word, and remove heading
        // it will make last have empty enclosure "''"
        if (end($line) === "''") {
            unset($line[count($line) - 1]);
        }
        
        $countLine = count($line);
        $countCsvMapping = count($this->csvMapping);

        // count line < mapping, mark as null
        if ($countLine < $countCsvMapping) {
            for($i = $countLine; $i < $countCsvMapping; $i++) {
                $line[$i] = null;
            }
            $countLine = $countCsvMapping;
        }

        // count line > mapping, throw exception
        if( $countLine > $countCsvMapping) {
            throw new CsvException('CSV line does not match requested parameters.');
        }

        $data = Array();
        
        foreach($line as $key => $item) {

            $item = str_replace("'", "", $item);

            if( is_array($this->csvMapping[$key]) ) { // array
                
                $namedKey = reset($this->csvMapping[$key]);
                $item = $this->explodeArrays($item);
  
            } else { // scalar
                $namedKey = $this->csvMapping[$key];
            }
           
            $data[$namedKey] = $item;
        }
        
        return $data;
    }

    /**
     * Based on delimiter, function will explode "string arrays" from CSV line
     * @param string $string
     * @return array
     * @throws CsvException
     */
    protected function explodeArrays($string) {
        
        $this->checkCsvMap();
        
        // empty value to empty array
        if( empty($string)) {
            return Array();
        }
        
        // else we use values
        $exploded = explode($this->csvArrayDelimiter, $string);
        return is_array($exploded) ? $exploded : Array($string);
    }
    
    /**
     * Verifies, that CSV map is array
     * @throws CsvException
     */
    protected function checkCsvMap() {
        if(!is_array($this->csvMapping)) {
            throw new CsvException('Csv map not found.');
        }
    }
    
}
