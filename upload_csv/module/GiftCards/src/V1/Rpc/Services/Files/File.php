<?php

namespace GiftCards\V1\Rpc\Services\Files;

use GiftCards\V1\Rpc\Services\Files\Exceptions\FileManipulationException;
use GiftCards\V1\Rpc\Services\Files\Interfaces\SplCsvGetter;

use SplFileObject;

/**
 * General file manipulation service
 *
 * @author hw
 */
class File
        implements SplCsvGetter
{
    
    /**
     *
     * @var string
     */
    protected $filePath;
    
    /**
     *
     * @var string
     */
    protected $path;
    
    /**
     *
     * @var string
     */
    protected $name;
    
    /**
     * 
     * @param string $filePath
     * @throws FileManipulationException
     */
    public function __construct($filePath) {

        if(!is_readable($filePath)) {
            throw new FileManipulationException('File is not readable.');
        }
        
        $this->filePath = $filePath;
        
        $this->setFileInfo();
    }
    
    /**
     * Sets internal file info.
     */
    protected function setFileInfo() {
        
        $path_parts = pathinfo($this->filePath);
        $this->path = $path_parts['dirname'];
        $this->name = $path_parts['basename'];
    }
    
    /**
     * Current file folder
     * @return string
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Current file name
     * @return string
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * 
     * @return string
     */
    public function getFile() {
        return $this->path . '/' . $this->name;
    }

    /**
     * Returns traversable file descriptior as SplFileObject <br>
     * WARNING: once file is open by this function, you wont be able to manipulate it in the FileSystem !
     * @return SplFileObject
     */
    public function getAsSplFile() {
        return new SplFileObject($this->getFile());
    }

    /**
     * Move the file to new directory.
     * @param string $path
     * @throws FileManipulationException
     */    
    public function moveToFolder($path) {
        
        // create if not exists
        if (!is_dir($path)) {
            @mkdir($path, 0775, true);
        }
        
        // move file
        $oldPath = $this->path .'/' . $this->name;
        $newPath = $path .'/' . $this->name;
        $res = @rename($oldPath, $newPath);
        
        if(!$res) {
            throw new FileManipulationException('Moving to new directory failed.');
        }
        
        $this->path = $path;
    }

    /**
     * Rename file to new name
     * @param string $name (without path)
     * @throws FileManipulationException
     */
    public function rename($name) {
        
        $oldName = $this->path .'/' . $this->name;
        $newName = $this->path .'/' . $name;
        $res = @rename($oldName, $newName);
        
        if(!$res) {
            throw new FileManipulationException('Renaming of file failed.');
        }
        
        $this->name = $name;
    }
    
}
