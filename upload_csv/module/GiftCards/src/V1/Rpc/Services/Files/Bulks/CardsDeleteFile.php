<?php

namespace GiftCards\V1\Rpc\Services\Files\Bulks;



/**
 * Manipulation with Gift Card bulk delete
 *
 * @author hw
 */
class CardsDeleteFile extends BulkAbstract
{
    
    /**
     * Final file destination folder prototype
     * @var string 
     */
    protected $destinationDirProto = '/data/bulk/cardsdelete/{year}/{month}/{day}/';
    
    /**
     * Final file name
     * @var string 
     */
    protected $destinationFileProto = '{filename}_{timestamp}';
    
    /**
     * Map of CSV fields into Array keys
     * @var array 
     */
    protected $csvMapping = [
            0 => 'barcode',
    ];
    
}
