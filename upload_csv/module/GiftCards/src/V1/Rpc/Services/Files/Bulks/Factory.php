<?php

namespace GiftCards\V1\Rpc\Services\Files\Bulks;

use Exception;

use GiftCards\V1\Rpc\Services\Files\Bulks;

/**
 * Factory for file bulk objects
 *
 * @author hw
 */
class Factory
{
    
    /**
     * 
     * @param string $type
     * @param string $path
     * @return Bulks\BulkAbstract
     * @throws Exception
     */
    public function getFileObject($type, $path) {
        
        switch($type) {
            case 'CardsAddFile': return new Bulks\CardsAddFile($path);
            case 'CardsUpdateFile': return new Bulks\CardsUpdateFile($path);
            case 'CardsDeleteFile': return new Bulks\CardsDeleteFile($path);
            case 'CardsActivateFile': return new Bulks\CardsActivateFile($path);
            case 'CardsSuspendFile': return new Bulks\CardsSuspendFile($path);
            default:
                throw new Exception('Unknown bulk file type.');
        }
    }
    
    
}
