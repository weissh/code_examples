<?php

namespace GiftCards\V1\Rpc\Services\Files\Exceptions;

use Exception;

/**
 * Exception thrown during the file manipulation on FS
 *
 * @author hw
 */
class FileManipulationException extends Exception {
    
}
