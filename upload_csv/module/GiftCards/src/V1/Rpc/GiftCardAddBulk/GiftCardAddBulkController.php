<?php

namespace GiftCards\V1\Rpc\GiftCardAddBulk;

use Exception;
use GiftCards\V1\Rpc\Entities\ApiResponse;
use GiftCards\V1\Rpc\Entities\GiftCardEntity;
use GiftCards\V1\Rpc\Mappers\GiftCardMapper;
use GiftCards\V1\Rpc\Services\Exceptions\ApiException;
use GiftCards\V1\Rpc\Services\Files\Bulks\CardsAddFile;
use GiftCards\V1\Rpc\Services\Files\Exceptions\CsvException;
use GiftCards\V1\Rpc\Services\Files\Exceptions\FileManipulationException;
use GiftCards\V1\Rpc\Services\Files\Exceptions\ValidationException;
use GiftCards\V1\Rpc\Services\Files\Interfaces\SplCsvGetter;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\ApiTools\ContentNegotiation\ViewModel;
use Laminas\ApiTools\Hal\Entity as HalEntity;
use Laminas\InputFilter\InputFilter;
use GiftCards\V1\Rpc\Services\Files\Bulks\Factory;


class GiftCardAddBulkController extends AbstractActionController
{

    /**
     *
     * @var GiftCardMapper
     */
    protected $giftCardMapper;

    /**
     *
     * @var ApiResponse
     */
    protected $apiResponse;

    /**
     *
     * @var GiftCardEntity
     */
    protected $cardEntity;

    /**
     *
     * @var InputFilter
     */
    protected $addCardValidators;

    /**
     *
     * @var Factory;
     */
    protected $bulkFilesFactory;

    ///////////////
    // init outputs

    /**
     *
     * @var int
     */
    protected $total_gift_card_created = 0;

    /**
     *
     * @var int
     */
    protected $total_failure = 0;

    /**
     *
     * @var array
     */
    protected $created_gift_cards = Array();

    /**
     *
     * @var array
     */
    protected $failed_gift_cards = Array(
        'validation' => Array(),
        'exceptions' => Array()
    );

    /**
     *
     * @param GiftCardMapper $giftCardMapper
     * @param GiftCardEntity $entity
     * @param ApiResponse $apiResponse
     * @param InputFilter $filter
     * @param Factory $bulkFilesFactory
     */
    public function __construct(GiftCardMapper $giftCardMapper,
                                GiftCardEntity $entity,
                                ApiResponse $apiResponse,
                                InputFilter $filter,
                                Factory $bulkFilesFactory)
    {
        $this->giftCardMapper = $giftCardMapper;
        $this->apiResponse = $apiResponse;
        $this->cardEntity = $entity;
        $this->addCardValidators = $filter;
        $this->bulkFilesFactory = $bulkFilesFactory;
    }

    /**
     * Store new Card bulk load to the persistent storage
     * @return ViewModel
     */
    public function giftCardAddBulkAction()
    {
        // Oauth2 client code
        $clientId = $this->getidentity()->getAuthenticationIdentity()['client_id'];

        // basic file ops
        try {
            $file = $this->getPreparedFileObject();
            $splFile = $this->getSplCsvObject($file);

            $this->apiResponse->setSuccess();

        } catch (Exception $e) {
            $this->setGlobalErrorResponse($e);

            // general error - finnish
            return $this->getViewModel($clientId);
        }

        ////////////////////////
        // file manipulation done, we can process content

        // loop the file and perform actions on EACH LINE
        foreach ($splFile as $lineNo => $line) {

            $lineNoString = 'Line_' . ($lineNo + 1); // file line identification
            unset($inputData); // cleanup before new iteration

            try {

                $inputData = $file->lineToArray($line); // export line to array

                $this->lineValidation($inputData); // current line validation

                // save card
                $this->saveNewCard($inputData, $clientId);

                // passed
                $this->total_gift_card_created++;
                $this->created_gift_cards[] = $inputData['barcode'];

            } catch (Exception $e) { // failure

                $this->total_failure++;
                $lineIdent = (!empty($inputData['barcode'])) ? $inputData['barcode'] : $lineNoString; // try to use Barcode for identification
                $this->setInternalErrorResponse($e, $lineIdent);
            }

        }

        // finalize
        return $this->getViewModel($clientId);

    }

    /**
     * Payload miracle to return hal-json
     * @param string $clientId
     * @return ViewModel
     */
    protected function getViewModel($clientId)
    {

        return new ViewModel(array(
            'payload' => $this->getHalEntity($clientId),
        ));
    }

    /**
     * Mapping of internal variables into proper HAL entity
     * @param string $clientId
     * @return HalEntity
     */
    protected function getHalEntity($clientId)
    {

        // HAL wrapper
        $halEntity = new HalEntity(
            Array(
                'response' => $this->apiResponse->toArray(),
                'total_gift_card_created' => $this->total_gift_card_created,
                'total_failure' => $this->total_failure,
                'created_gift_cards' => $this->created_gift_cards,
                'failed_gift_cards' => $this->failed_gift_cards
            ),
            $clientId // random ID to fullfil HAL reqs.
        );

        return $halEntity;

    }

    /**
     * Check current line against giftcardAdd method validators
     * @param array $inputData
     * @throws ValidationException
     */
    protected function lineValidation(array $inputData)
    {

        $validator = clone $this->addCardValidators;
        $validator->setData($inputData);

        // validation failed - entity not processed
        if (!$validator->isValid()) {
            throw new ValidationException($validator->getMessages()); // special exception type accepts array as input
        }
    }

    /**
     * Receive uploaded file and vove it to final location <br>
     * Create and return file object
     * @return CardsAddFile
     * @throws FileManipulationException | Exception
     */
    protected function getPreparedFileObject()
    {

        // input file - temp location
        $inputFilter = $this->getEvent()->getParam('Laminas\ApiTools\ContentValidation\InputFilter');
        $fileData = $inputFilter->getValues()['bulk']; // validated on the Apigility level

        // File object + set name
        //$file = $this->bulkFilesFactory->getFileObject('CardsAddFile', APPLICATION_PATH . '/' . $fileData['tmp_name']); // new CardsAddFile();
        $file = $this->bulkFilesFactory->getFileObject('CardsAddFile', $fileData['tmp_name']); // new CardsAddFile();
        $file->rename($fileData['name']);

        // move to final dest.
        $file->moveToStorage();

        return $file;
    }

    /**
     * Creates "CSV ready" Spl file object (traversable)
     * @param SplCsvGetter $file
     * @return Traversable
     */
    protected function getSplCsvObject(SplCsvGetter $file)
    {

        // open as SplFileObject
        $splFile = $file->getAsSplFile();
        $splFile->setFlags($splFile::READ_CSV | $splFile::READ_AHEAD | $splFile::SKIP_EMPTY | $splFile::DROP_NEW_LINE);

        return $splFile;
    }

    /**
     * Function will set global API response in case of Excpetion (based on exception type)
     * @param Exception $e
     */
    protected function setGlobalErrorResponse(Exception $e)
    {

        if ($e instanceof FileManipulationException) { // known problem

            $this->apiResponse->setResponseCode('007');
            $this->apiResponse->setResponseDescription('CSV file manipulation failed with message: ' . $e->getMessage());

        } else { // other general failure

            $this->apiResponse->setResponseCode('007');
            $this->apiResponse->setResponseDescription('We could not process CSV file. Please check the uploaded file.');
            // @ToDo: logging ??
        }
    }

    /**
     * Save input data to persistent storage
     * @param array $inputData
     * @param string $clientId
     * @throws ApiException|Exception
     */
    protected function saveNewCard(array $inputData, $clientId)
    {

        // data received
        $inputEntity = clone $this->cardEntity;
        $inputEntity->setClientCode($clientId);
        $inputEntity->setBarcode($inputData['barcode']);
        $inputEntity->setInitialLoad($inputData['initial_load']);
        $inputEntity->setIsPhysical($inputData['is_physical']);
        $inputEntity->setStartDate($inputData['start_date']);
        $inputEntity->setCurrencyCode($inputData['currency_code']);
        $inputEntity->setChannelsVector($inputData['channels']);
        $inputEntity->setMarketingId($inputData['promo_code']);
        $inputEntity->setExpiryPeriodInMonth($inputData['expiry_period_in_month']);

        // save gift card
        $this->giftCardMapper->addGiftCard($inputEntity);
    }

    /**
     * Prepares internal response in case of sigle GiftCard processing failure
     * @param Exception $e
     * @param string $lineIdent Identification of the failure line
     */
    protected function setInternalErrorResponse(Exception $e, $lineIdent)
    {

        if ($e instanceof ApiException) {

            $this->failed_gift_cards['exceptions'][$lineIdent] = Array(
                'responseCode' => $e->getCode(),
                'responseDescription' => $e->getMessage()
            );

        } elseif ($e instanceof CsvException) {

            $this->failed_gift_cards['exceptions'][$lineIdent] = Array(
                'responseCode' => $e->getCode(),
                'responseDescription' => $e->getMessage()
            );

        } elseif ($e instanceof ValidationException) {   // validation failed

            $this->failed_gift_cards['validation'][$lineIdent] = $e->getValidationErrors();

        } else { // other general failure

            $ae = new ApiException(ApiException::GENERAL_FAILURE);
            $this->failed_gift_cards['exceptions'][$lineIdent] = Array(
                'responseCode' => $ae->getCode(),
                'responseDescription' => $ae->getMessage()
            );
            // @ToDo: logging ??
        }
    }
}

