<?php

namespace GiftCards\V1\Rpc\Mappers;

use DateTime;
use Exception;
use stdClass;
use Laminas\Db\Adapter\Adapter as DbAdapter;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Sql;
use Laminas\Db\Sql\Where;
use Laminas\Form\Annotation\Instance;
use GiftCards\V1\Rpc\Entities\AbstractEntity;
use GiftCards\V1\Rpc\Entities\ChannelsCollection;
use GiftCards\V1\Rpc\Entities\ClientEntity;
use GiftCards\V1\Rpc\Entities\GiftCardActivateEntity;
use GiftCards\V1\Rpc\Entities\GiftCardDeleteEntity;
use GiftCards\V1\Rpc\Entities\GiftCardEntity;
use GiftCards\V1\Rpc\Entities\GiftCardGetBalanceEntity;
use GiftCards\V1\Rpc\Entities\GiftCardReplacementEntity;
use GiftCards\V1\Rpc\Entities\GiftCardSuspendEntity;
use GiftCards\V1\Rpc\Entities\GiftCardTransactionEntity;
use GiftCards\V1\Rpc\Entities\SpecialCases;
use GiftCards\V1\Rpc\Services\Exceptions\ApiException;
use GiftCards\V1\Rpc\Services\Paginator\Adapters\HydratingSelectPaginator;
use GiftCards\V1\Rpc\Entities\GiftCardsCollection;

// The gift card mapper
class GiftCardMapper extends AbstractMapper
{

    const DEFAULT_CARD_STATUS = 2; // INACTIVE

    const DEFAULT_TYPE_ID = 1; // Physical

    const DATE_FORMAT = 'Y-m-d';

    /**
     * Default table name
     *
     * @var string
     */
    const TAB_GIFT_CARD = 'gift_cards';

    /**
     *
     * @var string
     */
    const TAB_GIFT_STATUS = 'gift_card_statuses';

    /**
     *
     * @var string
     */
    const TAB_GIFT_CHANNELS = 'gift_cards_r_channels';

    /**
     *
     * @var string
     */
    const TAB_EXTERNAL_DATA = 'gift_card_external_data';

    /**
     * mapping array for api call /gift-card-balance
     *
     * @var array
     */
    const GET_BALANCE = [
        "barcode" => "barcode",
        "client_code" => "clientCode",
        "current_balance" => "amount",
        "currency_code" => "currencyCode",
        "name" => "status"
    ];

    /**
     * mapping for GiftCardEntity
     *
     * @var array
     */
    const GIFT_CARD = [
        "gift_card_id" => "giftCardId",
        "client_code" => "clientCode",
        "client_setting_id" => "clientSettingId",
        "gift_card_types_id" => "giftCardTypesId",
        "gift_card_status_id" => "giftCardStatusesId",
        "currency_code" => "currencyCode",
        "barcode" => "barcode",
        "is_physical" => "isPhysical",
        "initial_load" => "initialLoad",
        "current_balance" => "currentBalance",
        "start_date" => "startDate",
        "end_date" => "endDate",
        "created" => "created",
        "name" => "status",
        "marketing_id" => "marketingId",
    ];

    /**
     * Mapping for GiftCardTransactionEntity used for api call /gift-card-transaction
     *
     * @var array
     */
    const GIFT_CARD_TRANSACTION = [
        "barcode" => "barcode",
        "currentBalance" => "currentBalance",
        "paymentTaken" => "paymentTaken",
        "paymentRemaining" => "paymentRemaining",
        "currencyCode" => "currencyCode",
        "isPassValidation" => "isPassValidation"
    ];

    /**
     * Mapping for GiftCardTransactionEntity used for api call /gift-card-transaction
     *
     * @var array
     */
    const GIFT_CARD_REFUND = [
        "barcode" => "barcode",
        "currentBalance" => "currentBalance",
        "refundAmount" => "refundAmount",
        "currencyCode" => "currencyCode",
        "isPassValidation" => "isPassValidation"
    ];

    /**
     *
     * @var DbAdapter
     */
    protected $dbAdapter;

    /**
     *
     * @var Instance
     */
    protected $sql;

    /**
     *
     * @var ClientMapper
     */
    protected $clientMapper;

    /**
     *
     * @var TransactionTypeMapper
     */
    protected $transactionTypeMapper;

    /**
     *
     * @var GiftCardReplacementMapper
     */
    protected $giftCardReplacementMapper;

    /**
     * To be used to validate debit action
     *
     * @var string
     */
    protected $debit = 'DEBIT';

    /**
     * To be used to validate credit action
     *
     * @var string
     */
    protected $credit = 'CREDIT';

    /**
     *
     * @var GiftCardExternalDataMapper
     */
    protected $giftCardExternalDataMapper;

    public function __construct(
        DbAdapter $dbAdapter,
        ClientMapper $clientMapper,
        TransactionTypeMapper $transactionTypeMapper,
        GiftCardReplacementMapper $giftCardReplacementMapper,
        GiftCardExternalDataMapper $giftCardExternalDataMapper
    )
    {
        $this->dbAdapter = $dbAdapter;
        $this->clientMapper = $clientMapper;
        $this->sql = new Sql($this->dbAdapter);
        $this->transactionTypeMapper = $transactionTypeMapper;
        $this->giftCardReplacementMapper = $giftCardReplacementMapper;
        $this->giftCardExternalDataMapper = $giftCardExternalDataMapper;
    }

    /**
     * Get card balance by barcode
     * @param string $barcode
     * @param string $clientCode
     * @return GiftCardGetBalanceEntity
     * @throws Exception
     * @throws ApiException
     */
    public function getCardBalanceResponse($barcode, $clientCode)
    {
        $notFound = false;
        try {
            $giftCardBalanceEntity = new GiftCardGetBalanceEntity();

            $query = $this->queryGetGiftCardDetails($barcode, $clientCode);
            $statement = $this->sql->prepareStatementForSqlObject($query);
            $result = $statement->execute();

            // if not found
            if ($result->count() !== 1) {
                $notFound = true;
            } else {
                $this->hydrateDataEntity($result, static::GET_BALANCE, $giftCardBalanceEntity);
            }
        } catch (Exception $e) {
            throw new ApiException(static::INTERNAL_RESPONSE_SQL_ERROR);
        }

        // on purpose
        if ($notFound) {
            throw new ApiException(static::INTERNAL_RESPONSE_GIFT_CARD_NOT_FOUND);
        }

        return $giftCardBalanceEntity;
    }

    /**
     * Build the query for get gift card balance
     *
     * @param int $barcode
     * @return Select
     */
    protected function queryGetGiftCardDetails($barcode, $clientCode)
    {
        $query = null;

        // conditions
        $where = new Where();
        $where->equalTo('barcode', $barcode);
        $where->equalTo('client_code', $clientCode);

        $query = new Select();
        $query->from(self::TAB_GIFT_CARD)
            ->join(self::TAB_GIFT_STATUS,
                self::TAB_GIFT_CARD . '.gift_card_status_id = ' . self::TAB_GIFT_STATUS . '.gift_card_status_id')
            ->where($where);

        return $query;
    }

    /**
     * Get the full details of the gift card <br>
     * Returning MissingCard if not found
     *
     * @param string $barcode
     * @return GiftCardEntity
     * @throws ApiException|Exception
     */
    public function getGiftCardDetails($barcode, $clientCode)
    {
        $giftCardEntity = new GiftCardEntity();
        $query = $this->queryGetGiftCardDetails($barcode, $clientCode);

        try {
            // execute statement
            $statement = $this->sql->prepareStatementForSqlObject($query);
            $result = $statement->execute();
        } catch (Exception $e) {
            throw new ApiException(static::INTERNAL_RESPONSE_SQL_ERROR);
        }

        // if not found
        if ($result->count() !== 1) {
            return new SpecialCases\MissingCard();
        }

        $this->hydrateDataEntity($result, static::GIFT_CARD, $giftCardEntity);

        // attach card channels and settings
        $this->clientMapper->getChannelsMapper()->loadByGiftCard($giftCardEntity);
        $giftCardEntity->setGiftCardSettings($this->clientMapper->getSettingsById($giftCardEntity->getClientSettingId()));
        $giftCardEntity->setClientSettingId(NULL); // we don't need this anymore, everything loaded above

        return $giftCardEntity;
    }

    /**
     * @param $keyName
     * @param $keyValue
     * @param $clientCode
     * @return GiftCardsCollection
     * @throws ApiException
     */
    public function getGiftCardDetailsByExternalData($keyName, $keyValue, $clientCode)
    {
        $query = $this->queryGetGiftCardDetailsByExternalData($keyName, $keyValue, $clientCode);

        $hydrator = $this->getHydratorWithStrategy(static::GIFT_CARD);
        $paginatorSelectAdapter = new HydratingSelectPaginator(
            $query,
            $this->sql,
            $hydrator,
            new GiftCardEntity()
        );

        if ($paginatorSelectAdapter->count() === 0) {
            throw new ApiException(static::INTERNAL_RESPONSE_GIFT_CARD_NOT_FOUND);
        }

        $collection = new GiftCardsCollection($paginatorSelectAdapter);
        return $collection;
    }

    /**
     * Build the query for getGiftCardDetailsByExternalData
     *
     * @param int $barcode
     * @return Select
     */
    protected function queryGetGiftCardDetailsByExternalData($keyName, $keyValue, $clientCode)
    {
        $query = null;

        // conditions
        $where = new Where();
        $where->equalTo('key_name', $keyName);
        $where->equalTo('key_value', $keyValue);
        $where->equalTo('client_code', $clientCode);


        $query = new Select();
        $query->from(self::TAB_GIFT_CARD)
            ->join(self::TAB_EXTERNAL_DATA,
                self::TAB_GIFT_CARD . '.gift_card_id = ' . self::TAB_EXTERNAL_DATA . '.gift_card_id',
                [])
            ->where($where);

        return $query;
    }

    /**
     * Get the full details of the gift card with array conditions
     *
     * @param array $conditions
     * @return \GiftCards\V1\Rpc\Entities\GiftCardEntity
     * @throws ApiException
     */
    public function getGiftCardDetailsWithConditions(array $conditions)
    {
        $giftCardEntity = new GiftCardEntity();

        $query = new Select();
        $query->from(self::TAB_GIFT_CARD)
            ->join(
                self::TAB_GIFT_STATUS,
                self::TAB_GIFT_CARD . '.gift_card_status_id = ' . self::TAB_GIFT_STATUS . '.gift_card_status_id')
            ->where($conditions);

        try {
            $statement = $this->sql->prepareStatementForSqlObject($query);
            // execute statement
            $result = $statement->execute();
        } catch (Exception $e) {
            throw new ApiException(static::INTERNAL_RESPONSE_SQL_ERROR);
        }

        // if not found
        if ($result->count() !== 1) {
            $giftCardEntity = new SpecialCases\MissingCard();
            return $giftCardEntity;
        }

        $this->hydrateDataEntity($result, static::GIFT_CARD, $giftCardEntity);

        // attach card channels
        $this->clientMapper->getChannelsMapper()->loadByGiftCard($giftCardEntity);

        return $giftCardEntity;
    }

    /**
     * Verify, that barcode is not in use
     * @param $barcode
     * @param $clientCode
     * @return bool
     * @throws ApiException
     */
    public function isUniqueBarcode($barcode, $clientCode)
    {
        $card = $this->getGiftCardDetails($barcode, $clientCode);
        // is not (found in system)
        if (!$card instanceof SpecialCases\MissingCard) {
            return false;
        }
        // it is
        return true;

    }

    /**
     * @param GiftCardEntity $card
     * @param ClientEntity $clientEntity
     * @return bool
     */
    public function isInMaxAndMinInitialLoad(GiftCardEntity $card, $clientEntity)
    {
        if ($card->getInitialLoad() >= $clientEntity->getSettings()->getMinimumInitialLoad()
            && $card->getInitialLoad() <= $clientEntity->getSettings()->getMaximumInitialLoad()
        ) {
            return true;
        }

        return false;
    }

    /**
     * Validate if card belong to client
     * @param GiftCardEntity $giftCard
     * @param string $requestClientCode
     * @return bool
     */
    public function isCardBelongToClient(GiftCardEntity $giftCard, $requestClientCode)
    {
        $result = false;
        if (strtoupper($giftCard->getClientCode()) === strtoupper($requestClientCode)) {
            $result = true;
        }
        return $result;
    }

    /**
     * Check if the gift card is
     *
     * @param GiftCardEntity $giftCard
     * @return boolean
     */
    public function isGiftCardActive(GiftCardEntity $giftCard)
    {
        $result = false;
        if ($giftCard->getGiftCardStatusesId() == 1) {
            $result = true;
        }
        return $result;
    }

    /**
     * VAlidate if currency supported
     * @param GiftCardEntity $giftCard
     * @param string $requestCurrencyCode
     * @return boolean
     */
    public function isSupportedCurrency(GiftCardEntity $giftCard, $requestCurrencyCode)
    {
        $result = false;
        if ($giftCard->getCurrencyCode() == $requestCurrencyCode) {
            $result = true;
        }
        return $result;
    }

    /**
     * Validate if channel is supported
     * @param int $giftCardId
     * @param string $requestChannel
     * @return boolean
     * @throws ApiException
     */
    public function isSupportedChannel($giftCardId, $requestChannel)
    {
        $isSupported = false;

        $query = $this->queryGetGiftCardChannels($giftCardId, $requestChannel);

        try {
            // execute statement
            $statement = $this->sql->prepareStatementForSqlObject($query);
            $result = $statement->execute();
        } catch (Exception $e) {
            // @todo log error and send mail to admin
            throw new ApiException(static::INTERNAL_RESPONSE_SQL_ERROR);
        }

        if ($result->count() > 0) {
            $isSupported = true;
        }
        return $isSupported;
    }

    /**
     * Build the query for get gift card channels
     * @param int $giftCardId
     * @param string $channel
     * @return null|Select
     */
    protected function queryGetGiftCardChannels($giftCardId, $channel)
    {
        $query = null;

        // conditions
        $where = new Where();
        $where->equalTo('channel_code', $channel);
        $where->equalTo('gift_card_id', $giftCardId);
        $where->equalTo('status', 'ACTIVE');

        $query = new Select();
        $query->from(self::TAB_GIFT_CHANNELS)->where($where);

        return $query;
    }

    /**
     * is the transaction date between start date and Ending date of the gift card
     *
     * @param GiftCardEntity $giftCard
     * @return boolean
     */
    public function isInValidDate(GiftCardEntity $giftCard)
    {
        $today = time();
        $result = false;
        if ($today >= strtotime($giftCard->getStartDate()) && $today <= strtotime($giftCard->getEndDate())) {
            $result = true;
        }
        return $result;
    }

    /**
     * Calculate the transaction Taken payment and remaining payment
     *
     * @param GiftCardEntity $giftCard
     * @param stdClass $request
     * @return stdClass
     *          float currentBalance, float paymentTaken,
     *          float paymentRemaining, string currencyCode
     * @throws ApiException
     */
    public function calculateTransactionNewCurrentBalance(GiftCardEntity $giftCard, stdClass $request)
    {
        $newCurrentBalance = new stdClass();

        $transactionType = $this->transactionTypeMapper->getByTransactionName($request->transaction_type);

        $actionType = $transactionType->getCreditOrDebit();

        if (strtoupper($actionType) === $this->debit) {
            $debitCalculationResult = $this->debitCalculation($giftCard, $request, $actionType);

            if ($debitCalculationResult->isError === true) {
                throw new ApiException(static::INTERNAL_RESPONSE_TRANSACTION_FAILURE);
            }
        }

        $newCurrentBalance->currentBalance = $debitCalculationResult->currentBalance;
        $newCurrentBalance->paymentTaken = $debitCalculationResult->paymentTaken;
        $newCurrentBalance->paymentRemaining = $debitCalculationResult->paymentRemaining;
        $newCurrentBalance->currencyCode = $request->currency_code;

        return $newCurrentBalance;
    }

    /**
     * Calculate Credit for refund action
     * @param GiftCardEntity $giftCard
     * @param stdClass $request
     * @return stdClass
     * @throws ApiException
     */
    public function calculateRefundNewCurrentBalance(GiftCardEntity $giftCard, $request)
    {
        $newCurrentBalance = new stdClass();

        $transactionType = $this->transactionTypeMapper->getByTransactionName($request->transaction_type);

        $actionType = $transactionType->getCreditOrDebit();

        if (strtoupper($actionType) === $this->credit) {
            $newBalance = $giftCard->getCurrentBalance() + $request->amount;
        } else {
            throw new ApiException(static::INTERNAL_RESPONSE_TRANSACTION_FAILURE);
        }

        $newCurrentBalance->currentBalance = $newBalance;
        $newCurrentBalance->refundedAmount = $request->amount;
        $newCurrentBalance->currencyCode = $request->currency_code;

        return $newCurrentBalance;
    }

    /**
     * Process the calculation of Debit
     *
     * @param GiftCardEntity $giftCard
     * @param stdClass $request
     * @param string $actionType
     * @return stdClass boolean isError, float paymentTaken, float paymentRemaining
     */
    protected function debitCalculation(GiftCardEntity $giftCard, $request, $actionType)
    {

        $result = new stdClass();
        $result->isError = true;
        $result->paymentTaken = 0.00;
        $result->paymentRemaining = 0.00;

        if (isset($request->restrict_amount_to) && $request->restrict_amount_to > 0) {
            $debitAmount = $request->restrict_amount_to;
        } else {
            $debitAmount = $request->amount;
        }

        $currentBalance = $giftCard->getCurrentBalance();

        if (strtoupper($actionType) === $this->debit) {
            $newBalance = $currentBalance - $debitAmount;

            if ($newBalance < 0) {
                $result->paymentTaken = $currentBalance;
                $result->paymentRemaining = $newBalance;
                $result->currentBalance = 0.00;
            } else {
                $result->currentBalance = $newBalance;
                $result->paymentTaken = $debitAmount;
            }

            $result->isError = false;

        }

        return $result;
    }

    /**
     * Update current balance
     * @param float $newCurrentBalance
     * @param string $barcode
     * @return boolean
     * @throws ApiException
     */
    public function updateCurrentBalance($newCurrentBalance, $barcode)
    {
        try {

            $update = $this->sql->update(self::TAB_GIFT_CARD);
            $update->where(array(
                'barcode' => $barcode
            ));
            $update->set(array(
                'current_balance' => $newCurrentBalance
            ));

            // execute statement
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $statement->execute();

            return TRUE;

        } catch (Exception $e) {
            // @todo log error and send mail to admin
            throw new ApiException(static::INTERNAL_RESPONSE_TRANSACTION_FAILURE);
        }


    }

    /**
     * Save new gift card to the database
     *
     * @param GiftCardEntity $card
     * @param $isReplacement
     * @throws ApiException
     * @throws Exception
     */
    public function addGiftCard(GiftCardEntity $card, $isReplacement = false)
    {

        // Check for client and load full data.
        $clientEnt = $this->clientMapper->getClientById($card->getClientCode());

        // client not found
        if ($clientEnt instanceof SpecialCases\MissingClient) {
            throw new ApiException(static::INTERNAL_RESPONSE_CLIENT_NOT_FOUND);
        }

        // wrong currency
        if (!$this->clientMapper->isCurrencyBelongToClient(clone $clientEnt, $card->getCurrencyCode())) {
            throw new ApiException(static::INTERNAL_RESPONSE_CLIENT_UNSUPPORTED_CURRENCY);
        }

        // validate initial load if card is not Replacement
        if(!$isReplacement){
            // is in maximum and minimum initial load
            if (!$this->isInMaxAndMinInitialLoad($card, $clientEnt)) {
                throw new ApiException(static::INTERNAL_RESPONSE_CLIENT_INVALID_INITIAL_LOAD);
            }
        }

        // card already exists
        if (!$this->isUniqueBarcode($card->getBarcode(), $card->getClientCode())) {
            throw new ApiException(static::INTERNAL_RESPONSE_CARD_NOT_UNIQUE);
        }

        // get correct channels for new card
        $channels = $this->prepareChannelsForCard(clone $card, clone $this->clientMapper, clone $clientEnt);

        $expiry_period_in_month = $card->getExpiryPeriodInMonth();
        if (!is_numeric($expiry_period_in_month)) {
            $expiry_period_in_month = $clientEnt->getSettings()->getExpiryPeriodQuantity();
        }

        // additional data to be saved
        $card->setClientSettingId($clientEnt->getSettings()->getId())
            ->setEndDate(new Expression(sprintf('DATE_ADD( DATE("%s") ,INTERVAL %d MONTH)', $card->getStartDate(), $expiry_period_in_month)))
            ->setGiftCardChannels($channels);

        // save new gift card after
        if($isReplacement){
            $this->saveNewCard($card, true);
        }else{
            $this->saveNewCard($card);
        }

        //if external id then add transaction record
        $this->saveInitialTransaction($card);

        // if external data was added add them into gift_card_external_data
        $this->saveExternalData($card);
    }

    /**
     * Add the initial load into transaction table
     * @param GiftCardEntity $card
     * @throws ApiException
     */
    protected function saveInitialTransaction(GiftCardEntity $card)
    {
        if ($card->getExternalId() !== null) {
            $cardDetails = $this->getGiftCardDetails($card->getBarcode(), $card->getClientCode());

            $transactionMapper = new TransactionsMapper($this->dbAdapter, $this->transactionTypeMapper);
            $transactionMapper->saveInitialTransaction(
                $cardDetails->getGiftCardId(),
                $cardDetails->getInitialLoad(),
                $card->getExternalId());
        }
        return;
    }

    /**
     * Add external data related to card currently it used to add customer email address and custoemr id
     * @param GiftCardEntity $card
     * @throws ApiException
     */
    protected function saveExternalData(GiftCardEntity $card)
    {
        $cardDetails = $this->getGiftCardDetails($card->getBarcode(), $card->getClientCode());

        $this->giftCardExternalDataMapper->saveExternalData($cardDetails->getGiftCardId(), $card->getCustomerEmail(), $card->getCustomerId());
        return;
    }

    /**
     * This service will decide which channels belongs to this card
     * In case on empty input, all default channels are used
     * All added channels are verified against client
     *
     * @param GiftCardEntity $card
     * @param ClientMapper $clientMapper
     * @param ClientEntity $clientEnt
     * @return ChannelsCollection
     * @throws ApiException
     */
    protected function prepareChannelsForCard(GiftCardEntity $card, ClientMapper $clientMapper, ClientEntity $clientEnt)
    {

        // decide channels strategy - empty value mean all client suported channels
        if (empty($card->getChannelsVector())) {
            return $clientEnt->getChannels(); // don't need to verify
        }

        // provided channels (as vector) must be mapped and verified
        $channels = $clientMapper->getChannelsMapper()->getCollectionFromVector($card->getChannelsVector());

        // verify all channels
        foreach ($channels as $channel) {

            if (!$clientMapper->isChannelBelongToClient($clientEnt, $channel)) {
                throw new ApiException(static::INTERNAL_RESPONSE_CLIENT_UNSUPPORTED_CHANNEL); // check failed
            }
        }

        return $channels; // check passed

    }

    /**
     * Validate all passed CARD values and save changes to DB.
     * @param GiftCardEntity $newCard
     * @throws ApiException
     */
    public function updateGiftCard(GiftCardEntity $newCard, $clientId)
    {

        // card barcode exists ?
        $oldCard = $this->getGiftCardDetails($newCard->getBarcode(), $clientId);

        // not found
        if ($oldCard instanceof SpecialCases\MissingCard) {
            throw new ApiException(static::INTERNAL_RESPONSE_GIFT_CARD_NOT_FOUND);
        }

        // client full data.
        $clientEnt = $this->clientMapper->getClientById($oldCard->getClientCode());

        // wrong currency
        if (!$this->clientMapper->isCurrencyBelongToClient(clone $clientEnt, $oldCard->getCurrencyCode())) {
            throw new ApiException(static::INTERNAL_RESPONSE_CLIENT_UNSUPPORTED_CURRENCY);
        }

        // not update currency code, same as before
        $newCard->setCurrencyCode($oldCard->getCurrencyCode());

        // get correct channels for new card
        $channels = $this->prepareChannelsForCard(clone $newCard, clone $this->clientMapper, clone $clientEnt);

        // end date verification - Using predefined string date formats
        $startDate = DateTime::createFromFormat(self::DATE_FORMAT, $newCard->getStartDate());
        $endDate = DateTime::createFromFormat(self::DATE_FORMAT, $newCard->getEndDate());

        if (!$this->isCardEndDateBigger($startDate, $endDate)) {
            throw new ApiException(static::INTERNAL_RESPONSE_CARD_INVALID_ENDDATE);
        }

        // attach channels + id
        $newCard->setGiftCardChannels($channels)
            ->setGiftCardId($oldCard->getGiftCardId());

        // save changes to db
        $this->updateCardValues($newCard);

    }

    /**
     * Verification of end date - must be bigger that start date
     *
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @return boolean
     */
    protected function isCardEndDateBigger(DateTime $startDate, DateTime $endDate)
    {

        if ($startDate <= $endDate) {
            return true;
        }

        return false;

    }

    /**
     * Attach missing default values for new entry.
     * @param GiftCardEntity $card
     */
    protected function prepareNewCardValues(GiftCardEntity $card)
    {

        // add missing values
        $card->setGiftCardStatusesId(self::DEFAULT_CARD_STATUS)
            ->setCurrentBalance($card->getInitialLoad())
            ->setGiftCardTypesId(self::DEFAULT_TYPE_ID)
            ->setCreated(new Expression('NOW()'))
            ->setStartDate(new Expression(sprintf('DATE("%s")', $card->getStartDate())));

    }

    /**
     * Attach missing default values for replacement entry.
     * @param GiftCardEntity $card
     */
    protected function prepareReplacementCardValues(GiftCardEntity $card)
    {
        // add missing values
        $card->setGiftCardStatusesId( self::DEFAULT_CARD_STATUS )
            ->setGiftCardTypesId( self::DEFAULT_TYPE_ID )
            ->setCreated( new Expression('NOW()') )
            ->setStartDate( new Expression( sprintf('DATE("%s")', $card->getStartDate() ) ));
    }


    /**
     * This funnction is temporary
     *
     * @ToDo we need to adjust hydrator class to take only values defined in map
     * @param array $data
     * @return array
     */
    protected function cleanupNewCardValues(array $data)
    {

        unset($data['channelsVector']);
        unset($data['giftCardChannels']); // handled separately
        unset($data['name']);
        unset($data['giftCardSettings']); // no needed
        unset($data['externalId']);
        unset($data['customerEmail']);
        unset($data['customerId']);
        unset($data['expiryPeriodInMonth']);
        return array_filter($data);


    }

    /**
     * Save new GiftCard data to database, including channels
     * Running in the transaction.
     *
     * @param GiftCardEntity $card
     * @return boolean
     * @throws ApiException
     */
    protected function saveNewCard(GiftCardEntity $card, $isReplacement = false)
    {

        // fill values
        if(!$isReplacement){
            $this->prepareNewCardValues($card);
        }else{
            $this->prepareReplacementCardValues($card);
        }

        // updating more tables
        $this->dbAdapter->getDriver()->getConnection()->beginTransaction();

        try {

            // extract data and save
            $hydrator = $this->getHydratorWithStrategy(self::GIFT_CARD);
            $data = $hydrator->extract($card);

            // @ToDo temporary
            $data = $this->cleanupNewCardValues($data);

            $insert = $this->sql->insert(self::TAB_GIFT_CARD);
            $insert->values($data);

            $statement = $this->sql->prepareStatementForSqlObject($insert);
            $statement->execute();

            // generated ID of the card
            $cardId = $this->dbAdapter->getDriver()->getLastGeneratedValue();

            // add channels to gift card
            foreach ($card->getGiftCardChannels() as $channel) {
                $this->clientMapper->getChannelsMapper()->addChannelToCard($channel, $cardId);
            }


            $this->dbAdapter->getDriver()->getConnection()->commit();
            return TRUE;

        } catch (Exception $e) {

            $this->dbAdapter->getDriver()->getConnection()->rollback();

            throw new ApiException(static::INTERNAL_RESPONSE_SQL_ERROR);
        }

    }

    /**
     * Save card changes to the database.
     * Running in transaction.
     * @param GiftCardEntity $card
     * @return boolean
     * @throws ApiException
     * @throws Exception
     */
    public function updateCardValues(GiftCardEntity $card)
    {

        // updating more tables
        $this->dbAdapter->getDriver()->getConnection()->beginTransaction();

        try {

            // extract data and save
            $data = Array(
                'is_physical' => $card->getIsPhysical(),
                'start_date' => new Expression(sprintf('DATE("%s")', $card->getStartDate())),
                'end_date' => new Expression(sprintf('DATE("%s")', $card->getEndDate()))
            );

            // conditions
            $where = new Where();
            $where->equalTo('barcode', $card->getBarcode());

            // query build
            $update = $this->sql->update(self::TAB_GIFT_CARD)
                ->set($data)
                ->where($where);

            $statement = $this->sql->prepareStatementForSqlObject($update);
            $statement->execute();

            //Replace all channels
            $this->replaceCardChannelsInStorage($card);

            $this->dbAdapter->getDriver()->getConnection()->commit();

        } catch (Exception $e) {

            $this->dbAdapter->getDriver()->getConnection()->rollback();

            throw new ApiException(static::INTERNAL_RESPONSE_SQL_ERROR);
        }

        return true;
    }

    /**
     * Replace all channels by new
     * Not running in Transaction !
     * @ToDo this function need to be refactored (?)
     * @param GiftCardEntity $card card with requested channels
     * @return boolean
     */
    public function replaceCardChannelsInStorage(GiftCardEntity $card)
    {

        // replace card channels
        $this->clientMapper->getChannelsMapper()->deleteAllCardChannels($card->getGiftCardId());
        foreach ($card->getGiftCardChannels() as $channel) {
            $this->clientMapper->getChannelsMapper()->addChannelToCard($channel, $card->getGiftCardId());
        }

        return true;

    }

    /**
     * Update status card
     *
     * @param AbstractEntity $card
     * @param int $statusId
     * @param array $error_response
     * @return AbstractEntity
     * @throws ApiException
     *
     */
    private function updateStatusCard(AbstractEntity $card, $statusId, array $error_response)
    {
        if ($card->getBarcode() === null || $card->getClientCode() === null) {
            throw new ApiException($error_response);
        }

        // first, check without status id
        // if not found, will use default $error_response
        $giftCardEntity = $this->getGiftCardDetailsWithConditions([
            'barcode' => $card->getBarcode(),
            'client_code' => $card->getClientCode(),
        ]);

        if ($giftCardEntity instanceof SpecialCases\MissingCard) {
            throw new ApiException(static::INTERNAL_RESPONSE_GIFT_CARD_NOT_FOUND);
        }

        $card->setGiftCardStatusesId($statusId);

        // check if the card already activated/suspended/deleted
        $giftCardEntity = $this->getGiftCardDetailsWithConditions([
            'barcode' => $card->getBarcode(),
            'client_code' => $card->getClientCode(),
            self::TAB_GIFT_STATUS . '.gift_card_status_id' => $statusId
        ]);

        if (!$giftCardEntity instanceof SpecialCases\MissingCard) {
            return $card;
        }

        $this->validateAlreadyBroken($card);

        $update = $this->sql->update(self::TAB_GIFT_CARD);
        $update->where(array(
            'barcode' => $card->getBarcode(),
            'client_code' => $card->getClientCode(),
        ));
        $update->set(array(
            'gift_card_status_id' => $statusId,
            'modified' => date('Y-m-d H:i:s')
        ));

        try {
            // execute statement
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
        } catch (Exception $e) {
            throw new ApiException(static::INTERNAL_RESPONSE_SQL_ERROR);
        }

        return $card;
    }

    /**
     * Activate card
     *
     * @param AbstractEntity $card
     *
     * @return AbstractEntity
     * @throws ApiException
     */
    public function activate(AbstractEntity $card)
    {
        return $this->updateStatusCard($card, 1, static::INTERNAL_RESPONSE_COULDNOT_ACTIVATE);
    }

    /**
     * Suspend = inactive card
     *
     * @param AbstractEntity $card
     *
     * @return AbstractEntity
     * @throws ApiException
     */
    public function suspend(AbstractEntity $card)
    {
        return $this->updateStatusCard($card, 2, static::INTERNAL_RESPONSE_COULDNOT_SUSPEND);
    }

    /**
     * Delete card
     *
     * @param AbstractEntity $card
     *
     * @return AbstractEntity
     * @throws ApiException
     */
    public function delete(AbstractEntity $card)
    {
        return $this->updateStatusCard($card, 3, static::INTERNAL_RESPONSE_COULDNOT_DELETE);
    }

    /**
     * Set internal error based on broken Card
     *
     * @param AbstractEntity $card
     * @throws ApiException
     */
    private function brokenErrorSet(AbstractEntity $card)
    {
        switch ($card->getGiftCardStatusesId()) {
            case 3:
                throw new ApiException(static::INTERNAL_RESPONSE_COULDNOT_DELETE_ALREADY_DELETED);
                break;
            case 4:
                throw new ApiException(static::INTERNAL_RESPONSE_COULDNOT_STOLE_ALREADY_STOLEN);
                break;
            case 5:
                throw new ApiException(static::INTERNAL_RESPONSE_COULDNOT_LOSE_ALREADY_LOST);
                break;
            default:
                throw new ApiException(static::INTERNAL_RESPONSE_COULDNOT_DAMAGE_ALREADY_DAMAGED);
                break;
        }
    }

    /**
     * Check if card already broken
     *
     * @param AbstractEntity $card
     * @throws ApiException
     *
     */
    private function validateAlreadyBroken(AbstractEntity $card)
    {
        $giftCardEntityBrokenCard = $this->getGiftCardDetailsWithConditions([
            'barcode' => $card->getBarcode(),
            'client_code' => $card->getClientCode(),
            self::TAB_GIFT_STATUS . '.gift_card_status_id IN (3, 4, 5, 6)',
        ]);

        if (!$giftCardEntityBrokenCard instanceof SpecialCases\MissingCard) {
            $this->brokenErrorSet($giftCardEntityBrokenCard);
        }

        return;
    }

    /**
     * Set old barcode to new status and creating new card and transfer funds to new card
     *
     * @param AbstractEntity $card
     * @param int $new_card_barcode
     * @param int $gift_card_status_id
     * @param array $error_response
     * @return GiftCardEntity
     * @throws ApiException
     */
    private function duplicateAndMove(AbstractEntity $card, $new_card_barcode, $gift_card_status_id, array $error_response)
    {
        if ($card->getBarcode() === null || $card->getClientCode() === null) {
            throw new ApiException($error_response);
        }

        // card barcode exists ?
        $giftCardEntity = $this->getGiftCardDetailsWithConditions([
            'barcode' => $card->getBarcode(),
            'client_code' => $card->getClientCode()
        ]);

        if ($giftCardEntity instanceof SpecialCases\MissingCard) {
            throw new ApiException(static::INTERNAL_RESPONSE_GIFT_CARD_NOT_FOUND);
        } else {
            $this->validateAlreadyBroken($card);

            if (!$this->isUniqueBarcode($new_card_barcode, $card->getClientCode())) {
                throw new ApiException(static::INTERNAL_RESPONSE_CARD_NOT_UNIQUE);
            }

            $result = new stdClass();
            $result->isError = true;

            // next process step 1: set status to stolen
            $update = $this->sql->update(self::TAB_GIFT_CARD);
            $update->where(array(
                'barcode' => $card->getBarcode(),
                'client_code' => $card->getClientCode()
            ));
            $update->set(array(
                'gift_card_status_id' => $gift_card_status_id,
                'modified' => date('Y-m-d H:i:s')
            ));

            try {
                // execute statement
                $statement = $this->sql->prepareStatementForSqlObject($update);
                $statement->execute();

                $result->isError = false;
            } catch (Exception $e) {
                throw new ApiException(static::INTERNAL_RESPONSE_SQL_ERROR);
                // @todo log error and send mail to admin
            }

            // next process step 2: add gift card with new card
            if (false === $result->isError) {
                $inputEntity = new GiftCardEntity;
                // set entity data for insert
                $inputEntity->setClientCode($giftCardEntity->getClientCode());
                $inputEntity->setInitialLoad($giftCardEntity->getInitialLoad());
                $inputEntity->setCurrentBalance($giftCardEntity->getCurrentBalance());
                $inputEntity->setIsPhysical($giftCardEntity->getIsPhysical());
                $inputEntity->setStartDate($giftCardEntity->getStartDate());
                $inputEntity->setCurrencyCode($giftCardEntity->getCurrencyCode());
                $inputEntity->setChannelsVector($giftCardEntity->getChannelsVector());

                // set new insert with new barcode provided
                $inputEntity->setBarcode($new_card_barcode);

                //insert process
                $this->addGiftCard($inputEntity, true);

                // update new record status to previous status
                if ($giftCardEntity->getGiftCardStatusesId() === '1') {
                    $this->activate($inputEntity);
                } else {
                    $this->suspend($inputEntity);
                }

                // next process step 3: add new entry to gift_cards_replacement
                $giftcardReplacementEntity = new GiftcardReplacementEntity();
                $giftcardReplacementEntity->setOldBarcode($card->getBarcode());
                $giftcardReplacementEntity->setNewBarcode($new_card_barcode);

                $this->giftCardReplacementMapper->add($giftcardReplacementEntity);

                // return new card details
                $giftCardEntity = $inputEntity;
            }
        }

        return $giftCardEntity;
    }

    /**
     * Set old barcode to stolen and creating new card and transfer funds to new card
     *
     * @param AbstractEntity $card
     * @param int $new_card_barcode
     * @return GiftCardEntity
     * @throws ApiException
     */
    public function stoleGiftCard(AbstractEntity $card, $new_card_barcode)
    {
        return $this->duplicateAndMove($card, $new_card_barcode, 4, static::INTERNAL_RESPONSE_COULDNOT_STOLE);
    }

    /**
     * Set old barcode to damaged and creating new card and transfer funds to new card
     *
     * @param AbstractEntity $card
     * @param int $new_card_barcode
     * @return GiftCardEntity
     * @throws ApiException
     */
    public function damageGiftCard(AbstractEntity $card, $new_card_barcode)
    {
        return $this->duplicateAndMove($card, $new_card_barcode, 6, static::INTERNAL_RESPONSE_COULDNOT_DAMAGED);
    }

    /**
     * Set old barcode to lost and creating new card and transfer funds to new card
     *
     * @param AbstractEntity $card
     * @param int $new_card_barcode
     * @return GiftCardEntity
     * @throws ApiException
     */
    public function lostGiftCard(AbstractEntity $card, $new_card_barcode)
    {
        return $this->duplicateAndMove($card, $new_card_barcode, 5, static::INTERNAL_RESPONSE_COULDNOT_LOST);
    }

    /**
     * Get all client cards.
     *
     * @param $clientCode
     * @param int $page
     * @param int $count_per_page
     * @param int $return_all
     * @param null $dateFrom
     * @param null $dateTo
     * @return GiftCardsCollection
     * @throws ApiException
     */
    public function fetchAllClientCards($clientCode, $page = 1, $count_per_page = 30, $return_all = 0, $dateFrom = null, $dateTo = null)
    {
        $select = $this->queryFetchAllClientCards($clientCode, $dateFrom, $dateTo);

        $hydrator = $this->getHydratorWithStrategy(static::GIFT_CARD);
        $paginatorSelectAdapter = new HydratingSelectPaginator(
            $select,
            $this->sql,
            $hydrator,
            new GiftCardEntity()
        );

        if ($paginatorSelectAdapter->count() === 0) {
            throw new ApiException(static::INTERNAL_RESPONSE_REPORT_DATA_NOT_FOUND);
        }

        $collection = new GiftCardsCollection($paginatorSelectAdapter);
        if ($return_all == 0) {
            $collection->setCurrentPageNumber($page);
            $collection->setItemCountPerPage($count_per_page);
        }
        return $collection;
    }

    /**
     * Build the query for fetchAllClientCards()
     *
     * @param string $clientCode
     * @return Select
     */
    protected function queryFetchAllClientCards($clientCode, $dateFrom = null, $dateTo = null)
    {
        $query = null;

        // conditions
        $where = new Where();
        $where->equalTo('client_code', $clientCode);

        if (isset($dateFrom) && $dateFrom !== '') {
            $dateFrom = $dateFrom . ' 00:00:00';
            $where->greaterThanOrEqualTo(self::TAB_GIFT_CARD . '.created', $dateFrom);
        }

        if (isset($dateTo) && $dateTo !== '') {
            $dateTo = $dateTo . ' 23:59:59';
            $where->lessThanOrEqualTo(self::TAB_GIFT_CARD . '.created', $dateTo);
        }


        $query = new Select();
        $query->from(self::TAB_GIFT_CARD)
            ->join(self::TAB_GIFT_STATUS,
                self::TAB_GIFT_CARD . '.gift_card_status_id = ' . self::TAB_GIFT_STATUS . '.gift_card_status_id',
                array('name'))
            ->where($where)
            ->order('created DESC');

        return $query;
    }
}
