<?php
namespace GiftCards\V1\Rpc\Mappers;

use Exception;
use Laminas\Db\Adapter\Driver\Pdo\Result;
use Laminas\Hydrator\NamingStrategy\MapNamingStrategy;
use Laminas\Hydrator\ClassMethodsHydrator;
use GiftCards\V1\Rpc\Entities\HalExtractInterface;


/**
 * Shared / enforced mapper methods
 */
abstract class AbstractMapper
{

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_NORMAL = array(
        "response_code" => "000",
        "response_message" => "Normal"
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_GIFT_CARD_NOT_FOUND = array(
        "response_code" => "430",
        "response_message" => "We could not find the details of the requested gift card."
    );

    /**
     * @var array
     */
    const INTERNAL_RESPONSE_REPORT_DATA_NOT_FOUND = array(
        "response_code" => "430",
        "response_message" => "We could not find the details of the requested report."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_COULDNOT_DELETE = array(
        "response_code" => "431",
        "response_message" => "We could not update gift card status to delete."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_COULDNOT_SUSPEND = array(
        "response_code" => "432",
        "response_message" => "We could not update gift card status to suspend."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_COULDNOT_ACTIVATE = array(
        "response_code" => "433",
        "response_message" => "We could not update gift card status to active."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_COULDNOT_STOLE = array(
        "response_code" => "434",
        "response_message" => "We could not update gift card status to stolen"
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_COULDNOT_LOST = array(
        "response_code" => "435",
        "response_message" => "We could not update gift card status to lost"
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_COULDNOT_DAMAGED = array(
        "response_code" => "436",
        "response_message" => "We could not update gift card status to damaged"
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_COULDNOT_DELETE_ALREADY_DELETED = array(
        "response_code" => "437",
        "response_message" => "Sorry we could not process, your request this card was already set as deleted. Please contact adminitrator."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_COULDNOT_STOLE_ALREADY_STOLEN = array(
        "response_code" => "438",
        "response_message" => "Sorry we could not process, your request this card was already set as stolen. Please contact adminitrator."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_COULDNOT_LOSE_ALREADY_LOST = array(
        "response_code" => "439",
        "response_message" => "Sorry we could not process, your request this card was already set as lost. Please contact adminitrator."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_COULDNOT_DAMAGE_ALREADY_DAMAGED = array(
        "response_code" => "440",
        "response_message" => "Sorry we could not process, your request this card was already set as damaged. Please contact adminitrator."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_GIFT_CARD_INACTIVE = array(
        "response_code" => "441",
        "response_message" => "The gift card is INACTIVE. Please Aactivate your card first."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_GIFT_CARD_UNSUPPORTED_CHANNEL = array(
        "response_code" => "450",
        "response_message" => "Unsupported used channel for this gift card."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_GIFT_CARD_UNSUPPORTED_CURRENCY = array(
        "response_code" => "460",
        "response_message" => "Unsupported used currency for this gift card."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_GIFT_CARD_INVALID_END_START_DATE = array(
        "response_code" => "470",
        "response_message" => "Incorrect gift card starting or ending date"
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_GIFT_CARD_INSUFFICIENT_FUNDS = array(
        "response_code" => "480",
        "response_message" => "Insufficient funds"
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_WRONG_CLIENT_CODE = array(
        "response_code" => "440",
        "response_message" => "Client code do not match. Please Check your input."
    );
    
    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_PERMISSION_DENIED = array(
        "response_code" => "440",
        "response_message" => "Permission denied."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_SQL_ERROR = array(
        "response_code" => "530",
        "response_message" => "We could not process your request. Please try again later."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_SQL_NOUPDATE = array(
        "response_code" => "531",
        "response_message" => "Record has no been updated or no changes detected."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_TRANSACTION_FAILURE = array(
        "response_code" => "540",
        "response_message" => "We could not process your request. Please try again later."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_CLIENT_NOT_FOUND = array(
        "response_code" => "404",
        "response_message" => "Client not found."
    );
    
    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_CSETTINGS_NOT_FOUND = array(
        "response_code" => "405",
        "response_message" => "Cliend settings not found."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_CLIENT_UNSUPPORTED_CURRENCY = array(
        "response_code" => "551",
        "response_message" => "Invalid currency for given client."
    );
    
    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_CARD_NOT_UNIQUE = array(
        "response_code" => "552",
        "response_message" => "Given barcode already exists in system."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_CLIENT_UNSUPPORTED_CHANNEL = array(
        "response_code" => "553",
        "response_message" => "Invalid channel for given client."
    );

    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_CARD_INVALID_ENDDATE = array(
        "response_code" => "554",
        "response_message" => "Gift Card end date must be bigger that start date."
    );
    
    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_CLIENTID_NOT_SET = array(
        "response_code" => "555",
        "response_message" => "You must provide client ID!"
    );
    
    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_CLIENT_DATA_NOT_LOADED = array(
        "response_code" => "556",
        "response_message" => "Cliend details are not loaded."
    );
    
    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_CLISETID_NOT_SET= array(
        "response_code" => "557",
        "response_message" => "Cliend settings ID not set."
    );
    
    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_PERMISSIONS_VAULES_NOT_SET= array(
        "response_code" => "558",
        "response_message" => "CliendId and UserId are not set."
    );
    
    /**
     *
     * @var array
     */
    const INTERNAL_RESPONSE_CARDID_NOT_SET= array(
        "response_code" => "559",
        "response_message" => "Card ID must be defined."
    );
    

    const INTERNAL_RESPONSE_CLIENT_INVALID_INITIAL_LOAD = array(
        "response_code" => "560",
        "response_message" => "Invalid initial load for given client."
    );
    
    const INTERNAL_RESPONSE_INVALID_BARCODE_FORMAT = array(
        "response_code" => "561",
        "response_message" => "Invalid barcode format set by client."
    );
    
    const INTERNAL_RESPONSE_DATA_NOT_PROVIDED_SEARCH = array(
        "response_code" => "562",
        "response_message" => "Invalid request. No data was provided."
    );


    /**
     *
     * Default table name
     *
     * @return string
     */
    public function getTableName()
    {
        return static::TAB_DEFAULT;
    }

    /**
     * Default entity map
     *
     * @return array
     */
    public function getMap()
    {
        return static::MAP_DEFAULT;
    }

    /**
     * Hydrate the data into entity class
     *
     * @param Result $result
     * @param array $mapNamingStrategy
     * @param HalExtractInterface $entityClass
     */
    protected function hydrateDataEntity(Result $result, array $mapNamingStrategy, HalExtractInterface $entityClass)
    {
        // hydrate
        $hydrator = $this->getHydratorWithStrategy($mapNamingStrategy);
        $hydrator->hydrate($result->current(), $entityClass);
    }

    /**
     * Attach naming strategy to ClassMethodsHydrator and get it prepared.
     *
     * @param array $mapNamingStrategy
     * @return ClassMethodsHydrator
     */
    protected function getHydratorWithStrategy(Array $mapNamingStrategy)
    {
        // ClassMethodsHydrator setup
        $hydrator = new ClassMethodsHydrator();
        $hydrator->setNamingStrategy(MapNamingStrategy::createFromHydrationMap($mapNamingStrategy));
        return $hydrator;
    }
    
    /**
     * Returns any used internal response constant, if exists.
     * @param string $constName
     * @return array
     * @throws Exception
     */
    public function getInternalResponse($constName) {
        // we are talking ONLY about internal responses
        if (strpos($constName, 'INTERNAL_RESPONSE_' ) === false) {
            $constName = 'INTERNAL_RESPONSE_' . $constName;
        }

        if( !defined( "static::$constName" ) ) {
            throw new Exception('Internal response undefined!');
        }

        return constant("static::$constName");
    }
}
