<?php
namespace GiftCards\V1\Rpc\GiftCardSuspendBulk;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class GiftCardSuspendBulkControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $giftCardMapper = $container->get('GiftCardMapper');
        $cardEntity = $container->get('GiftCardSuspendEntity');
        $apiResponse = $container->get('ApiResponse');
        $bulkFilesFactory = $container->get('BulkFilesFactory');

        // Retrieve the InputFilterManager + addCard filter validator
        $filters = $container->get('InputFilterManager');
        $delCardFilter = $filters->get('GiftCards\\V1\\Rpc\\GiftCardSuspend\\Validator');

        return new GiftCardSuspendBulkController($giftCardMapper, $cardEntity, $apiResponse, $delCardFilter, $bulkFilesFactory);
    }
}
