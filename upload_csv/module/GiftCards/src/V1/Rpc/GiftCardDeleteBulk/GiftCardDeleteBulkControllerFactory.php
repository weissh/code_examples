<?php

namespace GiftCards\V1\Rpc\GiftCardDeleteBulk;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class GiftCardDeleteBulkControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $giftCardMapper = $container->get('GiftCardMapper');
        $cardEntity = $container->get('GiftCardDeleteEntity');
        $apiResponse = $container->get('ApiResponse');
        $bulkFilesFactory = $container->get('BulkFilesFactory');

        // Retrieve the InputFilterManager + addCard filter validator
        $filters = $container->get('InputFilterManager');
        $delCardFilter = $filters->get('GiftCards\\V1\\Rpc\\GiftCardDelete\\Validator');

        return new GiftCardDeleteBulkController($giftCardMapper, $cardEntity, $apiResponse, $delCardFilter, $bulkFilesFactory);
    }
}
