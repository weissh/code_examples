<?php
namespace GiftCards\V1\Rpc\Factories\Adapters;

use GiftCards\V1\Rpc\Services\Paginator\Adapters\DummyAdapter;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Dummy (empty) paginator adapter to be used for initial collections.
 */
class DummyPaginatorAdapterFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DummyAdapter
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {

        return new DummyAdapter();

    }
}
