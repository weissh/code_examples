<?php

namespace GiftCards\V1\Rpc\Factories\Mappers;

use GiftCards\V1\Rpc\Mappers\TransactionFailureMapper;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class TransactionFailureMapperFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return TransactionFailureMapper
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dbAdapter = $container->get('db-data');
        $giftCardMapper = $container->get('GiftCardMapper');
        $transactionTypeMapper = $container->get('TransactionTypeMapper');

        return new TransactionFailureMapper($dbAdapter, $giftCardMapper, $transactionTypeMapper);
    }
}
