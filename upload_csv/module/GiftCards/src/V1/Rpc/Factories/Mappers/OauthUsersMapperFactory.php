<?php

namespace GiftCards\V1\Rpc\Factories\Mappers;

use GiftCards\V1\Rpc\Mappers\OauthUsersMapper;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class OauthUsersMapperFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return OauthUsersMapper|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new OauthUsersMapper($container->get('db-oauth'));
    }
}
