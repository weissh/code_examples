<?php
namespace GiftCards\V1\Rpc\Factories\Mappers;

use GiftCards\V1\Rpc\Mappers\TransactionTypeMapper;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class TransactionTypeMapperFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return TransactionTypeMapper|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dbAdapter = $container->get('db-data');
        return new TransactionTypeMapper($dbAdapter);
    }
}
