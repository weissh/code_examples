<?php

namespace GiftCards\V1\Rpc\Factories\Mappers;

use GiftCards\V1\Rpc\Mappers\GiftCardMapper;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;


class GiftCardMapperFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return GiftCardMapper|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dbAdapter = $container->get('db-data');
        $transactionTypeMapper = $container->get('TransactionTypeMapper');
        $clientMapper = $container->get('ClientMapper');
        $giftCardReplacementMapper = $container->get('GiftCardReplacementMapper');
        $giftCardExternalDataMapper = $container->get('GiftCardExternalDataMapper');

        return new GiftCardMapper(
            $dbAdapter,
            $clientMapper,
            $transactionTypeMapper,
            $giftCardReplacementMapper,
            $giftCardExternalDataMapper
        );

    }
}
