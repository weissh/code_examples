<?php
namespace GiftCards\V1\Rpc\Factories\Mappers;

use GiftCards\V1\Rpc\Mappers\BarcodeMapper;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class BarcodeMapperFactory implements FactoryInterface
{

    /**
     *
     * @param ServiceLocatorInterface $serviceLocator            
     * @return ClientMapper
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dbAdapter = $container->get('db-data');
        
        $mapper = new BarcodeMapper($dbAdapter);
        
        return $mapper;
    }
}
