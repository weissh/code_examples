<?php
namespace GiftCards\V1\Rpc\Factories\Mappers;

use GiftCards\V1\Rpc\Mappers\ChannelsMapper;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ChannelsMapperFactory implements FactoryInterface
{
    
    /**
     * 
     * @param ServiceLocatorInterface $serviceLocator
     * @return ChannelsMapper
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {

        $dbAdapter = $container->get('db-data');
        $channelMapper = new ChannelsMapper($dbAdapter);

        return  $channelMapper;

    }
}
