<?php
namespace GiftCards\V1\Rpc\Factories\Mappers;

use GiftCards\V1\Rpc\Mappers\TransactionsMapper;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class TransactionsMapperFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return TransactionsMapper
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dbAdapter = $container->get('db-data');
        $transactionTypeMapper = $container->get('TransactionTypeMapper');
        return new TransactionsMapper($dbAdapter, $transactionTypeMapper);
    }
}
