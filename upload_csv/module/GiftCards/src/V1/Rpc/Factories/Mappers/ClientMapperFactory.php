<?php

namespace GiftCards\V1\Rpc\Factories\Mappers;

use GiftCards\V1\Rpc\Mappers\ClientMapper;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ClientMapperFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ClientMapper|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {

        $dbAdapter = $container->get('db-data');
        $channelsMapper = $container->get('ChannelsMapper');
        $currencyMapper = $container->get('CurrencyMapper');

        return new ClientMapper($dbAdapter, $channelsMapper, $currencyMapper);

    }
}
