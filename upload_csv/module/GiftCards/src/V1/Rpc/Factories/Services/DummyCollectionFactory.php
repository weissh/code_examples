<?php

namespace GiftCards\V1\Rpc\Factories\Services;

use GiftCards\V1\Rpc\Entities\DummyCollection;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Dummy (empty) collection.
 */
class DummyCollectionFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DummyCollection|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {

        $adapter = $container->get('DummyPaginatorAdapter');
        return new DummyCollection($adapter);

    }
}
