<?php

namespace GiftCards\V1\Rpc\Factories\Services\Barcode;

use GiftCards\V1\Rpc\Services\Barcode\BarcodeGenerator;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Permission Factory to inject dependencies.
 */
class BarcodeGeneratorFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BarcodeGenerator|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $cardMapper = $container->get('GiftCardMapper');
        $clientMapper = $container->get('ClientMapper');
        $barcodeMapper = $container->get('BarcodeMapper');
        return new BarcodeGenerator($clientMapper, $cardMapper, $barcodeMapper);

    }
}
