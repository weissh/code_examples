<?php

namespace GiftCards\V1\Rpc\Factories\Services\Identification;

use GiftCards\V1\Rpc\Services\Identification\Identification;
use Laminas\Mvc\Controller\PluginManager;

/**
 * Identification Factory for Identification plugin
 */
class IdentificationFactory
{
    /**
     *
     * @param PluginManager $manager
     * @return Identification
     */
    public function __invoke($manager)
    {
        $plugin = $manager->get('ControllerPluginManager');
        $identityPlugin = $plugin->get('getIdentity');

        return new Identification($identityPlugin->__invoke()->getAuthenticationIdentity());
    }
}
