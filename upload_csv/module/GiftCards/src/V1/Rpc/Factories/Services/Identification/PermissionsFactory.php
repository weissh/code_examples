<?php

namespace GiftCards\V1\Rpc\Factories\Services\Identification;

use GiftCards\V1\Rpc\Services\Identification\Permissions;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Permission Factory to inject dependencies.
 */
class PermissionsFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return Permissions|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {

        $oauthUsersMapper = $container->get('OauthUsersMapper');
        $usersClientsMapper = $container->get('UsersClientsMapper');
        return new Permissions($oauthUsersMapper, $usersClientsMapper);

    }
}
