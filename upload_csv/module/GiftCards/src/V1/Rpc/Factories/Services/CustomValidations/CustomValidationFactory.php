<?php

namespace GiftCards\V1\Rpc\Factories\Services\CustomValidations;

use GiftCards\V1\Rpc\Services\CustomValidations\CustomValidation;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

/**
 * Permission Factory to inject dependencies.
 */
class CustomValidationFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return CustomValidation|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {

        $clientsMapper = $container->get('ClientMapper');
        return new CustomValidation($clientsMapper);

    }
}
