#!/usr/bin/env bash

echo "-- Install MYSQL  --"

wget -O /tmp/RPM-GPG-KEY-mysql https://repo.mysql.com/RPM-GPG-KEY-mysql >> /var/log/build.log 2>&1
sudo apt-key add /tmp/RPM-GPG-KEY-mysql >> /var/log/build.log 2>&1

echo "-- Adding MySQL repo --"
echo "deb http://repo.mysql.com/apt/debian/ stretch mysql-5.7" | sudo tee /etc/apt/sources.list.d/mysql.list >> /var/log/build.log 2>&1


echo "-- Updating package lists after adding MySQL repo --"
sudo aptitude update -y >> /var/log/build.log 2>&1

sudo debconf-set-selections <<< "mysql-community-server mysql-community-server/root-pass password $1"
sudo debconf-set-selections <<< "mysql-community-server mysql-community-server/re-root-pass password $1"

echo "-- Installing MySQL server --"
sudo aptitude install -y mysql-server >> /var/log/build.log 2>&1

echo "-- Configure MySQL server --"
sudo sed -i "s/.*bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/mysql.conf.d/mysqld.cnf >> /var/log/build.log 2>&1

sudo tee -a /etc/mysql/mysql.conf.d/mysqld.cnf << END
sql_mode=''
END


sudo service mysql restart