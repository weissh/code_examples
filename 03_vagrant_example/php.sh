#!/usr/bin/env bash

echo "-- Installing Apache --"
sudo aptitude install -y apache2 >> /var/log/build.log 2>&1

echo "-- Enabling mod rewrite --"
sudo a2enmod rewrite >> /var/log/build.log 2>&1

echo "-- Configure Apache --"
sudo sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf

echo "-- Installing PHP 7.3 --"
sudo aptitude install -y php libapache2-mod-php php-mysql php-zip php-xdebug php-redis php-mbstring php-xml php-intl php-tokenizer php-gd php-imagick php-bcmath >> /var/log/build.log 2>&1

echo "-- Configure xDebug (idekey = PHP_STORM) --"
sudo tee -a /etc/php/7.3/mods-available/xdebug.ini << END
xdebug.remote_host=10.0.2.2
xdebug.remote_connect_back=1
xdebug.remote_port=9000
xdebug.remote_enable=1
xdebug.remote_handler=dbgp
xdebug.remote_mode=req
xdebug.idekey=PHPSTORM
END

echo "-- Setup PHP error handling --"
#sudo sed -i 's/display_errors = Off/display_errors = On/g' /etc/php/7.3/apache2/php.ini >> /var/log/build.log 2>&1
#sudo sed -i 's/display_startup_errors = Off/display_startup_errors = On/g' /etc/php/7.3/apache2/php.ini >> /var/log/build.log 2>&1

echo "-- Restarting Apache --"
sudo /etc/init.d/apache2 restart >> /var/log/build.log 2>&1

echo "-- Installing Composer --"
sudo curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

echo "-- Disable opcache for apigility projects remove if opcache needed --"
sudo tee -a /etc/php/7.3/mods-available/opcache.ini << END
opcache.enable=0
END
