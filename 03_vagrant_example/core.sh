#!/usr/bin/env bash

echo "-- Set vagrant log file --"
sudo touch /var/log/build.log
sudo chmod 777 /var/log/build.log

echo "-- Debian update / upgrade --"
sudo apt-get update >> /var/log/build.log 2>&1
sudo apt-get upgrade >> /var/log/build.log 2>&1

echo "-- Setup localtime --"
# Set timezone to Belgrade/Serbia -> you will probably need to change this
sudo unlink /etc/localtime
sudo ln -s /usr/share/zoneinfo/Europe/London /etc/localtime

echo "-- Installing debianconf --"
sudo apt-get install -y debconf-utils >> /var/log/build.log 2>&1

echo "-- Installing dirmngr --"
sudo apt-get install dirmngr >> /var/log/build.log 2>&1

echo "-- Installing aptitude --"
sudo apt-get -y install aptitude >> /var/log/build.log 2>&1

echo "-- Updating package lists --"
sudo aptitude update -y >> /var/log/build.log 2>&1

echo "-- Updating system --"
sudo aptitude safe-upgrade -y >> /var/log/build.log 2>&1

echo "-- Installing curl --"
sudo aptitude install -y curl >> /var/log/build.log 2>&1

echo "-- Installing vim --"
sudo aptitude install -y vim >> /var/log/build.log 2>&1

echo "-- Installing git --"
sudo aptitude install -y git >> /var/log/build.log 2>&1

echo "-- Add coloring to VIM editor --"
sudo update-alternatives --set editor /usr/bin/vim.basic >> /var/log/build.log 2>&1
sudo sed -i 's/^"syntax on$/syntax on/;s/^"set /set /;s/^set compatible/"set compatible/;s/^set mouse/"set mouse/;' /etc/vim/vimrc >> /var/log/build.log 2>&1

echo "-- Installing nodejs --"
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
cnodejs npm >> /var/log/build.log 2>&1

echo "-- Installing bower --"
npm install -g bower

echo "-- Installing yarn --"
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update >> /var/log/build.log 2>&1
sudo aptitude install -y yarn

