#!/usr/bin/env bash

echo "-- Add proejct Api VirtualHost --"
echo "<VirtualHost *:80>
    ServerAdmin weissh@hotmail.com
    ServerName proejct-api.local
    DocumentRoot /var/www/proejct-api/current/public

    <Directory /var/www/proejct-api/current/public>
        Options Indexes FollowSymlinks MultiViews
        AllowOverride All
        Require all granted
        DirectoryIndex index.php
    </Directory>

    ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
    <Directory /usr/lib/cgi-bin>
        AllowOverride None
        Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
        Order allow,deny
        Allow from all
    </Directory>

    ## Logging
    ErrorLog "/var/log/apache2/proejct-api_error.log"
    ServerSignature Off
    CustomLog "/var/log/apache2/proejct-api_access.log" combined

</VirtualHost>"  | sudo tee /etc/apache2/sites-available/proejct-api.conf >> /var/log/build.log 2>&1


echo "-- Add proejct Admin VirtualHost --"
echo "<VirtualHost *:80>
    ServerAdmin weissh@hotmail.com
    ServerName proejct-admin.local
    DocumentRoot /var/www/proejct-admin/current/public

    <Directory /var/www/proejct-admin/current/public>
        Options Indexes FollowSymlinks MultiViews
        AllowOverride All
        Require all granted
        DirectoryIndex index.php
    </Directory>

    ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
    <Directory /usr/lib/cgi-bin>
        AllowOverride None
        Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
        Order allow,deny
        Allow from all
    </Directory>

    ## Logging
    ErrorLog "/var/log/apache2/proejct-admin_error.log"
    ServerSignature Off
    CustomLog "/var/log/apache2/proejct-admin_access.log" combined

</VirtualHost>"  | sudo tee /etc/apache2/sites-available/proejct-admin.conf >> /var/log/build.log 2>&1


sudo a2enmod rewrite
sudo a2dissite 000-default.conf
sudo a2ensite impactvouchers-api.conf
sudo a2ensite impactvouchers-admin.conf
sudo service apache2 restart


echo "-- Create project DBs --"

mysql -u root -proot  --execute="CREATE DATABASE proejct_oauth CHARACTER SET utf8 COLLATE utf8_general_ci;"
mysql -u root -proot  --execute="CREATE DATABASE proejct_data CHARACTER SET utf8 COLLATE utf8_general_ci;"


echo "-- Add DB user - Used by application --"
mysql -uroot -proot --execute="CREATE USER 'appUser'@'%' IDENTIFIED BY 'appUser123';"
mysql -uroot -proot --execute="GRANT ALL PRIVILEGES ON *.* TO 'appUser'@'%' WITH GRANT OPTION;"


