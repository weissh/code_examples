<?php
return [
    'GiftCards\\V1\\Rpc\\GetClient\\Controller' => [
        'description' => 'Get client full data',
        'POST' => [
            'description' => 'Load full client data based on the client ID from Oauth',
            'request' => 'none',
            'response' => '{
  "response": {
    "responseCode": "000",
    "responseDescription": "Normal"
  },
  "data": {
    "id": "lts",
    "name": "Long Tall Sally",
    "status": "ACTIVE",
    "settings": {
      "id": "1",
      "periodName": "Month",
      "periodDescription": "perdiod by Month",
      "expiryPeriodQuantity": "36",
      "restrictToCurrency": "1",
      "multiChannelSupport": "1",
      "redemptionSupport": "1",
      "expiryOnZeroBalance": "1",
      "refundToZeroBalance": "1",
      "maximumRedemptionUsed": "1",
      "minimumInitialLoad": "10.00",
      "maximumInitialLoad": "500.00",
      "logAction": "1"
    },
    "channels": [
      {
        "id": "WEB",
        "name": "WEB",
        "description": "E-commerce Website"
      },
      {
        "id": "PHONE",
        "name": "Phone",
        "description": "On phone order using Spectrum"
      },
      {
        "id": "STORE",
        "name": "In-Store",
        "description": "In clients stores using prism EPOS"
      }
    ],
    "currencies": [
      {
        "code": "GBP",
        "decimals": "2"
      },
      {
        "code": "USD",
        "decimals": "2"
      },
      {
        "code": "CAD",
        "decimals": "2"
      }
    ]
  },
  "_links": []
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardGetBalance\\Controller' => [
        'description' => 'Get the gift card balance',
        'POST' => [
            'description' => 'Get the gift card balance',
            'request' => '{
"barcode":"06546546546"
}',
            'response' => '{
  "response": {
    "responseCode": "000",
    "responseDescription": "Normal"
  },
  "data": {
    "barcode": "123456789",
    "amount": "550.50",
    "currencyCode": "GBP",
    "status": "ACTIVE"
  },
  "_links": []
}

{
  "response": {
    "responseCode": "430",
    "responseDescription": "We could not find the details of the requested gift card."
  },
  "data": {
    "barcode": null,
    "amount": null,
    "currencyCode": null,
    "status": null
  },
  "_links": []
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardTransaction\\Controller' => [
        'description' => 'Make payment using gift card',
        'POST' => [
            'description' => 'Make payment or refund using gift card',
            'request' => '{
"barcode": "123456789",
"external_id": "6789",
"currency_code" : "GBP",
"amount" : "50.00",
"channel" : "STORE",
"request_from" : "C180",
"transaction_datetime" : "2015-10-20 09:05:10",
"transaction_type" : "REDEMPTION"
}',
            'response' => '{
        "response":
        {
            "responseCode": "000",
            "responseDescription": "Normal"
        },
        "data":
        {
            "transactionId": "57",
            "barcode": "123456789",
            "currentBalance": 350,
            "isPassedValidation": true,
            "paymentTaken": "50.00",
            "paymentRemaining": 0,
            "currencyCode": "GBP",
            "externalId": "6789"
        },
        "_links":
        [
        ]
    }

{
  "response": {
    "responseCode": "460",
    "responseDescription": "Unsupported used currency for this gift card."
  },
  "data": {
    "barcode": "123456789",
    "currentBalance": null,
    "isPassedValidation": false,
    "paymentTaken": null,
    "paymentRemaining": null,
    "currencyCode": null
  },
  "_links": []
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardSearch\\Controller' => [
        'POST' => [
            'description' => 'get gift card details by customer id or email address.
This should make call into Spectrum to get the barcode of the customer this may result with multiple gift card result',
            'request' => '{
"barcode": "123456789"
}

or 

{
"customer_email": "hani.weiss@whistl.co.uk"
}

or 

{
"customer_id": "123456789"
}',
            'response' => '{
        "response":
        {
            "responseCode": "000",
            "responseDescription": "Normal"
        },
        "data":
        {
            "giftCardId": "3",
            "clientCode": "LTS",
            "giftCardTypesId": "1",
            "giftCardStatusesId": "1",
            "currencyCode": "GBP",
            "barcode": "123456789",
            "isPhysical": "1",
            "initialLoad": "600.50",
            "currentBalance": "450.00",
            "startDate": "2015-10-28 16:34:45",
            "endDate": "2018-10-28 16:34:49",
            "created": "2015-10-21 14:16:00",
            "giftCardSettings":
            {
                "id": "1",
                "periodName": "Month",
                "periodDescription": "perdiod by Month",
                "expiryPeriodQuantity": "36",
                "restrictToCurrency": "1",
                "multiChannelSupport": "1",
                "redemptionSupport": "1",
                "expiryOnZeroBalance": "1",
                "refundToZeroBalance": "1",
                "maximumRedemptionUsed": "1",
                "minimumInitialLoad": "10.00",
                "maximumInitialLoad": "500.00",
                "logAction": "1"
            },
            "giftCardChannels":
            [
                {
                    "id": "PHONE",
                    "name": "Phone",
                    "description": "On phone order using Spectrum"
                },
                {
                    "id": "STORE",
                    "name": "In-Store",
                    "description": "In clients stores using prism EPOS"
                },
                {
                    "id": "WEB",
                    "name": "WEB",
                    "description": "E-commerce Website"
                }
            ],
            "status": "ACTIVE"
        },
        "_links":
        [
        ]
    }

or a collection 



    {
        "response":
        {
            "responseCode": "000",
            "responseDescription": "Normal"
        },
        "count_per_page": 30,
        "count_in_current_page": 2,
        "page": 1,
        "total": 2,
        "data":
        [
            {
                "giftCardId": "3",
                "clientCode": "LTS",
                "clientSettingId": "1",
                "giftCardTypesId": "1",
                "giftCardStatusesId": "1",
                "currencyCode": "GBP",
                "barcode": "123456789",
                "isPhysical": "1",
                "initialLoad": "600.50",
                "currentBalance": "551.00",
                "startDate": "2015-10-28 00:00:00",
                "endDate": "2018-10-28 00:00:00",
                "created": "2015-10-28 16:35:00",
                "giftCardSettings":
                {
                    "id": 0,
                    "periodName": "",
                    "periodDescription": "",
                    "expiryPeriodQuantity": 0,
                    "restrictToCurrency": 0,
                    "multiChannelSupport": 0,
                    "redemptionSupport": 0,
                    "expiryOnZeroBalance": 0,
                    "refundToZeroBalance": 0,
                    "maximumRedemptionUsed": 0,
                    "minimumInitialLoad": 0,
                    "maximumInitialLoad": 0,
                    "barcodeValidationRule": null,
                    "logAction": 0,
                    "barcodeNumOfChar": null,
                    "barcodeGenerateRule": null
                },
                "giftCardChannels":
                [
                ],
                "channelsVector":
                [
                ],
                "status": "",
                "externalId": null,
                "customerEmail": null,
                "customerId": null
            },
            {
                "giftCardId": "127",
                "clientCode": "LTS",
                "clientSettingId": "48",
                "giftCardTypesId": "1",
                "giftCardStatusesId": "2",
                "currencyCode": "GBP",
                "barcode": "54654555571",
                "isPhysical": "1",
                "initialLoad": "50.00",
                "currentBalance": "50.00",
                "startDate": "2015-10-15 00:00:00",
                "endDate": "2018-04-15 00:00:00",
                "created": "2016-03-18 12:34:09",
                "giftCardSettings":
                {
                    "id": 0,
                    "periodName": "",
                    "periodDescription": "",
                    "expiryPeriodQuantity": 0,
                    "restrictToCurrency": 0,
                    "multiChannelSupport": 0,
                    "redemptionSupport": 0,
                    "expiryOnZeroBalance": 0,
                    "refundToZeroBalance": 0,
                    "maximumRedemptionUsed": 0,
                    "minimumInitialLoad": 0,
                    "maximumInitialLoad": 0,
                    "barcodeValidationRule": null,
                    "logAction": 0,
                    "barcodeNumOfChar": null,
                    "barcodeGenerateRule": null
                },
                "giftCardChannels":
                [
                ],
                "channels":
                [
                ],
                "status": "",
                "externalId": null,
                "customerEmail": null,
                "customerId": null
            }
        ],
        "_links":
        [
        ]',
        ],
        'GET' => [
            'description' => 'get gift card details by barcode
/gift-card-search/[barcode]',
            'response' => '{
        "response":
        {
            "responseCode": "000",
            "responseDescription": "Normal"
        },
        "data":
        {
            "giftCardId": "3",
            "clientCode": "LTS",
            "giftCardTypesId": "1",
            "giftCardStatusesId": "1",
            "currencyCode": "GBP",
            "barcode": "123456789",
            "isPhysical": "1",
            "initialLoad": "600.50",
            "currentBalance": "450.00",
            "startDate": "2015-10-28 16:34:45",
            "endDate": "2018-10-28 16:34:49",
            "created": "2015-10-21 14:16:00",
            "giftCardSettings":
            {
                "id": "1",
                "periodName": "Month",
                "periodDescription": "perdiod by Month",
                "expiryPeriodQuantity": "36",
                "restrictToCurrency": "1",
                "multiChannelSupport": "1",
                "redemptionSupport": "1",
                "expiryOnZeroBalance": "1",
                "refundToZeroBalance": "1",
                "maximumRedemptionUsed": "1",
                "minimumInitialLoad": "10.00",
                "maximumInitialLoad": "500.00",
                "logAction": "1"
            },
            "giftCardChannels":
            [
                {
                    "id": "PHONE",
                    "name": "Phone",
                    "description": "On phone order using Spectrum"
                },
                {
                    "id": "STORE",
                    "name": "In-Store",
                    "description": "In clients stores using prism EPOS"
                },
                {
                    "id": "WEB",
                    "name": "WEB",
                    "description": "E-commerce Website"
                }
            ],
            "status": "ACTIVE"
        },
        "_links":
        [
        ]
    }',
        ],
        'description' => 'search for git card buy barcode or customer id or email address',
    ],
    'GiftCards\\V1\\Rpc\\GiftCardAdd\\Controller' => [
        'description' => 'Service will check input data and save posted Gift Card',
        'POST' => [
            'description' => 'Mandatory params - all except channels.
By Default all (client\'s) channels are attached',
            'request' => '{
"barcode": "9658458LPOK",           //Card number generated by Magic (used as bar-code on physical cards   
"currency_code": "GBP",             //Voucher Currency   
"initial_load": "50.00",            //Card number generated by Magic (used as bar-code on physical cards   
"start_date": "2015-10-15",         // Starting data of gift voucher the expiry date will be calculated automatically from "Expiry Period Quantity" set by client (expiry_date = start_date + expiry_period_quantity)
"channels": ["WEB","PHONE"],        // Allowed channels to be used 
"is_physical": "1",                 // 1 = physical gift card, 0 = e-voucher
"external_id":"465465455",          //This an spectrum order Id used for search by order number
"customer_email":"hani.weiss@whistl.co.uk",   // Client email address used for search by client email address
"customer_id":"132122222",          // Client id used for search by client id
"promo_code" : "Deben0816"          // Recently added to allow track the card "for marketing use"
"expiry_period_in_month": "3"       // Recently added to allow override the default expiry date (expiry_date = start_date + expiry_period_in_month
 
 }',
            'response' => '{
  "response": {
    "responseCode": "000",
    "responseDescription": "Normal"
  },
  "data": {
    "giftCardId": "15",
    "clientCode": "LTS",
    "clientSettingId": "1",
    "giftCardTypesId": "1",
    "giftCardStatusesId": "2",
    "marketingId": "DEB0716",
    "currencyCode": "GBP",
    "barcode": "000022112221212124",
    "isPhysical": "1",
    "initialLoad": "50.00",
    "currentBalance": "50.00",
    "startDate": "2015-10-15 00:00:00",
    "endDate": "2018-10-15 00:00:00",
    "created": "2015-10-21 14:16:00",
    "giftCardChannels": [
      {
        "id": "PHONE",
        "name": "Phone",
        "description": "On phone order using Spectrum"
      },
      {
        "id": "WEB",
        "name": "WEB",
        "description": "E-commerce Website"
      }
    ],
    "responseCode": "000",
    "responseDescription": "Normal",
    "status": "INACTIVE"
  },
  "_links": []
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardUpdate\\Controller' => [
        'description' => 'Update gift card details',
        'POST' => [
            'description' => 'Update gift card details',
            'request' => '{
"barcode": "000022112221212122",
"start_date": "2015-10-15",
"end_date": "2015-12-15",
"channels": ["WEB","PHONE"],
"is_physical": "1"
}',
            'response' => '{
  "response": {
    "responseCode": "000",
    "responseDescription": "Normal"
  },
  "data": {
    "giftCardId": "15",
    "clientCode": "LTS",
    "clientSettingId": "1",
    "giftCardTypesId": "1",
    "giftCardStatusesId": "2",
    "currencyCode": "GBP",
    "barcode": "000022112221212124",
    "isPhysical": "1",
    "initialLoad": "50.00",
    "currentBalance": "50.00",
    "startDate": "2015-10-15 00:00:00",
    "endDate": "2018-10-15 00:00:00",
    "created": "2015-10-21 14:16:00",
    "giftCardChannels": [
      {
        "id": "PHONE",
        "name": "Phone",
        "description": "On phone order using Spectrum"
      },
      {
        "id": "WEB",
        "name": "WEB",
        "description": "E-commerce Website"
      }
    ],
    "responseCode": "000",
    "responseDescription": "Normal",
    "status": "INACTIVE"
  },
  "_links": []
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardActivate\\Controller' => [
        'description' => 'Activate gift card',
        'POST' => [
            'description' => 'Activate gift card',
            'request' => '{
   "barcode": "123456789"
}',
            'response' => '{
                        {
              "barcode": "123456789",
              "client_code": "LTS",
              "response_code": "000",
              "response_code_message": "Normal",
              "_links": []
            }
        }',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardSuspend\\Controller' => [
        'description' => 'Suspend gift card',
        'POST' => [
            'description' => 'Suspend gift card',
            'request' => '{
"barcode":"123456789"
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardDelete\\Controller' => [
        'description' => 'Delete gift card',
        'POST' => [
            'description' => 'Delete gift card',
            'request' => '{
"barcode":"123456789"
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardStolen\\Controller' => [
        'description' => 'Process stolen gift card',
        'POST' => [
            'description' => 'Process stolen gift card',
            'request' => '{
   "barcode":"123456789",
   "new_card_barcode":"123456781"
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardDamaged\\Controller' => [
        'description' => 'Process damaged gift card',
        'POST' => [
            'description' => 'Process damaged gift card',
            'request' => '{
   "barcode": "123456789",
   "new_card_barcode": "123456781"
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardLost\\Controller' => [
        'description' => 'Process lost gift card',
        'POST' => [
            'description' => 'Process lost gift card',
            'request' => '{
   "barcode": "123456789",
   "new_card_barcode": "123456781"
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardGetBalanceXml\\Controller' => [
        'description' => 'Return voucher balance in xml format',
        'POST' => [
            'description' => 'Return voucher balance in xml format',
            'request' => '<?xml version="1.0" encoding="UTF-8"?><VOUCHER_BALANCE_REQ><VOUCHER_CODE>123456789</VOUCHER_CODE></VOUCHER_BALANCE_REQ>',
            'response' => '<?xml version="1.0" encoding="UTF-8"?>
<VOUCHER_BALANCE_RESPONSE><STATUS>Success</STATUS><SUB_STATUS>000</SUB_STATUS><REASON>Normal</REASON><BALANCE><VOUCHER_CODE>123456789</VOUCHER_CODE><AVAILABLE_BALANCE>550.50</AVAILABLE_BALANCE><CURRENCY>GBP</CURRENCY></BALANCE></VOUCHER_BALANCE_RESPONSE>',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardAddBulk\\Controller' => [
        'description' => 'Bulk Gift Card creation - CSV file upload',
        'POST' => [
            'description' => 'Pass (upload) file with name \'bulk\' in CSV format described in request body',
            'request' => 'barcode,currency_code,initial_load,start_date,channels,is_physical,promo_code,expiry_period_in_month (remove for real upload !)
123456789,GBP,50.00,2015-10-15,,1,DEB0716,3
987654321,EUR,49.99,2015-10-15,WEB,0,DEB0717,3
111222333666,GPB,100,2015-10-15,WEB|PHONE,0,DEB0718,6',
            'response' => '{
  "response": {
    "responseCode": "000",
    "responseDescription": "Normal"
  },
  "total_gift_card_created": 1,
  "total_failure": 4,
  "created_gift_cards": [
    "98765432100"
  ],
  "failed_gift_cards": {
    "validation": {
      "2145645454": {
        "initial_load": [
          "Enter float number"
        ]
      }
    },
    "exceptions": {
      "12345678988": {
        "responseCode": 552,
        "responseDescription": "Given barcode already exists in system."
      },
      "111222333666": {
        "responseCode": 551,
        "responseDescription": "Invalid currency for given client."
      },
      "Line_5": {
        "responseCode": 585,
        "responseDescription": "CSV line does not match requested parameters."
      }
    }
  },
  "_links": []
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardUpdateBulk\\Controller' => [
        'description' => 'Bulk Gift Cards update - CSV file upload',
        'POST' => [
            'description' => 'Pass (upload) file with name \'bulk\' in CSV format described in request body',
            'request' => 'barcode,start_date,end_date,channels,is_physical (description - to be removed)
123456789,2015-10-15,2016-10-15,,1',
            'response' => '{
  "response": {
    "responseCode": "000",
    "responseDescription": "Normal"
  },
  "total_gift_card_updated": 1,
  "total_failure": 3,
  "updated_gift_cards": [
    "987654321"
  ],
  "failed_to_update_gift_cards": {
    "validation": {
      "11122233367": {
        "start_date": [
          "Enter date in format YYYY-MM-DD"
        ]
      }
    },
    "exceptions": {
      "123456789888": {
        "responseCode": 430,
        "responseDescription": "We could not find the details of the requested gift card."
      },
      "111222333666": {
        "responseCode": 430,
        "responseDescription": "We could not find the details of the requested gift card."
      }
    }
  },
  "_links": []
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardTransactionHistory\\Controller' => [
        'description' => 'Returns list of transactions for given Card in some period',
        'POST' => [
            'description' => 'Enter barcode (mandatory) and start date / end date (optional)',
            'request' => '{
"barcode": "123456789",
"start_date": "2015-01-01",
"end_date": "2015-12-31"
}',
            'response' => '{
        "response":
        {
            "responseCode": "000",
            "responseDescription": "Normal"
        },
        "data":
        [
            {
                "giftCardsTransactionId": "18",
                "cardId": "3",
                "channelCode": "STORE",
                "transactionTypeId": "6",
                "amount": "50.00",
                "conversionRate": "1.00",
                "requestFrom": "C180",
                "created": "2015-11-11 11:04:43"
            },
            {
                "giftCardsTransactionId": "19",
                "cardId": "3",
                "channelCode": "STORE",
                "transactionTypeId": "4",
                "amount": "50.00",
                "conversionRate": "1.00",
                "requestFrom": "C180",
                "created": "2015-11-11 11:16:40"
            }
        ],
        "_links":
        [
        ]
    }',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardTransactionsFailure\\Controller' => [
        'description' => 'Fetch Transaction Failure',
        'POST' => [
            'description' => 'Enter Page Variables and Requested Search Data',
            'request' => '{
"dateFrom": "2016-01-01",
"dateTo": "2016-01-02",
"page": 1,
"count_per_page": 2,
"transaction_type": 4,
"is_paginated": 1
}',
            'response' => '{
        "response":
        {
            "responseCode": "000",
            "responseDescription": "Normal"
        },
        "count_per_page": 2,
        "count_in_current_page": 1,
        "page": 1,
        "total": 100,
        "data":
        [
            {
                "giftCardsTransactionFailureId": "18",
                "cardId": "3",
                "channelCode": "STORE",
                "transactionTypeId": "6",
                "responseCode": "403",
                "responseDescription": "We could not find the details of the requested gift card.",
                "request": "C180",
                "created": "2015-11-11 11:04:43"
            },
            {
                "giftCardsTransactionFailureId": "19",
                "cardId": "3",
                "channelCode": "STORE",
                "transactionTypeId": "6",
                "responseCode": "403",
                "responseDescription": "We could not find the details of the requested gift card.",
                "request": "C180",
                "created": "2015-11-11 11:04:43"
            }
        ],
        "_links":
        [
        ]
    }',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardDeleteBulk\\Controller' => [
        'description' => 'Bulk Gift Card removal - CSV file upload',
        'POST' => [
            'description' => 'Pass (upload) file with name \'bulk\' in CSV format described in request body',
            'request' => 'barcode (remove for real upload !)
123456789
987654321',
            'response' => '{
  "response": {
    "responseCode": "000",
    "responseDescription": "Normal"
  },
  "total_deleted": 3,
  "failed_to_delete": 1,
  "list_deleted_gift_cards": [
    "123456789",
    "987654321",
    "9321212122"
  ],
  "failed_to_delete_list": {
    "validation": [],
    "exceptions": {
      "123456789888": {
        "responseCode": 430,
        "responseDescription": "We could not find the details of the requested gift card."
      }
    }
  },
  "_links": []
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardSuspendBulk\\Controller' => [
        'description' => 'Bulk Gift Card suspending - CSV file upload',
        'POST' => [
            'description' => 'Pass (upload) file with name \'bulk\' in CSV format described in request body',
            'request' => 'barcode (remove for real upload !)
123456789
987654321',
            'response' => '{
  "response": {
    "responseCode": "000",
    "responseDescription": "Normal"
  },
  "total_suspended": 1,
  "failed_to_suspend": 2,
  "list_suspended_gift_cards": [
    "9321212122"
  ],
  "failed_to_suspend_list": {
    "validation": [],
    "exceptions": {
      "987654321": {
        "responseCode": 437,
        "responseDescription": "Sorry we could not process, your request this card was already set as deleted. Please contact adminitrator."
      },
      "123456789888": {
        "responseCode": 430,
        "responseDescription": "We could not find the details of the requested gift card."
      }
    }
  },
  "_links": []
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\ClientUpdateSettings\\Controller' => [
        'POST' => [
            'description' => 'API call to update client settings',
            'request' => '{
"expiry_period_quantity": "30",
"restrict_to_currency":"1",
"multi_channel_support":"1",
"redemption_support":"1",
"expiry_on_zero_balance":"1",
"refund_to_zero_balance":"1",
"maximum_redemption_used":"100",
"minimum_initial_load":"22",
"maximum_initial_load":"622",
"barcode_validation_rule": "/^[a-zA-Z0-9]+$/",
"barcode_generate_rule": "numeric",
"barcode_num_of_char_rule": "15"
}',
            'response' => '{
  "response": {
    "responseCode": "000",
    "responseDescription": "Normal"
  },
  "data": {
    "id": "40",
    "periodName": "Month",
    "periodDescription": "perdiod by Month",
    "expiryPeriodQuantity": "30",
    "restrictToCurrency": "1",
    "multiChannelSupport": "1",
    "redemptionSupport": "1",
    "expiryOnZeroBalance": "1",
    "refundToZeroBalance": "1",
    "maximumRedemptionUsed": "100",
    "minimumInitialLoad": "1.00",
    "maximumInitialLoad": "1.00",
    "barcodeValdationRule": "/^[a-zA-Z0-9]+$/",
    "logAction": "1"
  },
  "_links": []
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\ClientGetCards\\Controller' => [
        'POST' => [
            'description' => 'List client cards',
            'request' => '{
"page": "1",
"count_per_page": "100",
"dateFrom":"2015-11-01",
"dateTo":"2016-03-07"
}',
            'response' => '{
        "response":
        {
            "responseCode": "000",
            "responseDescription": "Normal"
        },
        "count_per_page": 1,
        "count_in_current_page": 1,
        "page": 1,
        "total": 33,
        "data":
        [
            {
                "giftCardId": "72",
                "clientCode": "LTS",
                "clientSettingId": "1",
                "giftCardTypesId": "1",
                "giftCardStatusesId": "2",
                "currencyCode": "GBP",
                "barcode": "1980",
                "isPhysical": "1",
                "initialLoad": "1000.00",
                "currentBalance": "1000.00",
                "startDate": "2016-02-29 00:00:00",
                "endDate": "2019-02-28 00:00:00",
                "created": "2016-03-01 17:09:13",
                "giftCardSettings":
                {
                    "id": 0,
                    "periodName": "",
                    "periodDescription": "",
                    "expiryPeriodQuantity": 0,
                    "restrictToCurrency": 0,
                    "multiChannelSupport": 0,
                    "redemptionSupport": 0,
                    "expiryOnZeroBalance": 0,
                    "refundToZeroBalance": 0,
                    "maximumRedemptionUsed": 0,
                    "minimumInitialLoad": 0,
                    "maximumInitialLoad": 0,
                    "logAction": 0
                },
                "giftCardChannels":
                [
                ],
                "channelsVector":
                [
                ],
                "status": "INACTIVE"
            }
        ]
    }',
        ],
        'description' => 'List client cards',
    ],
    'GiftCards\\V1\\Rpc\\ReportsTransactions\\Controller' => [
        'POST' => [
            'description' => 'Get the full report of all transaction (redeemed and refunded) in period of time',
            'request' => '{
"dateFrom": "2015-01-02",
"dateTo": "2016-04-04",
"page":"2",
"count_per_page":"1",
"is_paginated":"1"
}',
            'response' => '{
  "response": {
    "responseCode": "000",
    "responseDescription": "Normal"
  },
  "count_per_page": "1",
  "count_in_current_page": 1,
  "page": "2",
  "total": 37,
  "data": [
    {
      "giftCardsTransactionId": "48",
      "cardId": "3",
      "channelCode": "STORE",
      "transactionTypeId": "6",
      "amount": "650.00",
      "conversionRate": "1.00",
      "requestFrom": "C180",
      "created": "2016-03-03 10:01:23"
    }
  ],
  "_links": []
}',
        ],
    ],
    'GiftCards\\V1\\Rpc\\GiftCardBarcodeGenerator\\Controller' => [
        'POST' => [
            'description' => 'generate a bar code string  to be used with add card API',
            'request' => '{
"numberOfBarcodes": "10"
}',
            'response' => '{
  "response": {
    "responseCode": "000",
    "responseDescription": "Normal"
  },
  "data": [
    [
      "RCT43R9MAHATQVDCNTV9"
    ]
  ],
  "_links": []
}',
        ],
    ],
];
