<?php
return [
    'controllers' => [
        'factories' => [
            'GiftCards\\V1\\Rpc\\GetClient\\Controller' => \GiftCards\V1\Rpc\GetClient\GetClientControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardGetBalance\\Controller' => \GiftCards\V1\Rpc\GiftCardGetBalance\GiftCardGetBalanceControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardTransfer\\Controller' => \GiftCards\V1\Rpc\GiftCardTransfer\GiftCardTransferControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardTransaction\\Controller' => \GiftCards\V1\Rpc\GiftCardTransaction\GiftCardTransactionControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardSearch\\Controller' => \GiftCards\V1\Rpc\GiftCardSearch\GiftCardSearchControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardAdd\\Controller' => \GiftCards\V1\Rpc\GiftCardAdd\GiftCardAddControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardUpdate\\Controller' => \GiftCards\V1\Rpc\GiftCardUpdate\GiftCardUpdateControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardActivate\\Controller' => \GiftCards\V1\Rpc\GiftCardActivate\GiftCardActivateControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardSuspend\\Controller' => \GiftCards\V1\Rpc\GiftCardSuspend\GiftCardSuspendControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardDelete\\Controller' => \GiftCards\V1\Rpc\GiftCardDelete\GiftCardDeleteControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardStolen\\Controller' => \GiftCards\V1\Rpc\GiftCardStolen\GiftCardStolenControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardDamaged\\Controller' => \GiftCards\V1\Rpc\GiftCardDamaged\GiftCardDamagedControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardLost\\Controller' => \GiftCards\V1\Rpc\GiftCardLost\GiftCardLostControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardRefund\\Controller' => \GiftCards\V1\Rpc\GiftCardRefund\GiftCardRefundControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardGetBalanceXml\\Controller' => \GiftCards\V1\Rpc\GiftCardGetBalanceXml\GiftCardGetBalanceXmlControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardUpdateBulk\\Controller' => \GiftCards\V1\Rpc\GiftCardUpdateBulk\GiftCardUpdateBulkControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardTransactionHistory\\Controller' => \GiftCards\V1\Rpc\GiftCardTransactionHistory\GiftCardTransactionHistoryControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardTransactionsFailure\\Controller' => \GiftCards\V1\Rpc\GiftCardTransactionsFailure\GiftCardTransactionsFailureControllerFactory::class,
            'GiftCards\\V1\\Rpc\\ClientUpdateSettings\\Controller' => \GiftCards\V1\Rpc\ClientUpdateSettings\ClientUpdateSettingsControllerFactory::class,
            'GiftCards\\V1\\Rpc\\ClientGetCards\\Controller' => \GiftCards\V1\Rpc\ClientGetCards\ClientGetCardsControllerFactory::class,
            'GiftCards\\V1\\Rpc\\ReportsTransactions\\Controller' => \GiftCards\V1\Rpc\ReportsTransactions\ReportsTransactionsControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardBarcodeGenerator\\Controller' => \GiftCards\V1\Rpc\GiftCardBarcodeGenerator\GiftCardBarcodeGeneratorControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardAddBulk\\Controller' => \GiftCards\V1\Rpc\GiftCardAddBulk\GiftCardAddBulkControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardDeleteBulk\\Controller' => \GiftCards\V1\Rpc\GiftCardDeleteBulk\GiftCardDeleteBulkControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardSuspendBulk\\Controller' => \GiftCards\V1\Rpc\GiftCardSuspendBulk\GiftCardSuspendBulkControllerFactory::class,
            'GiftCards\\V1\\Rpc\\GiftCardActivateBulk\\Controller' => \GiftCards\V1\Rpc\GiftCardActivateBulk\GiftCardActivateBulkControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            \GiftCards\V1\Rpc\Entities\ApiResponse::class => \Laminas\ServiceManager\Factory\InvokableFactory::class,
            \GiftCards\V1\Rpc\Entities\ClientEntity::class => \Laminas\ServiceManager\Factory\InvokableFactory::class,
            \GiftCards\V1\Rpc\Entities\ClientSettingsUpdateEntity::class => \Laminas\ServiceManager\Factory\InvokableFactory::class,
            \GiftCards\V1\Rpc\Entities\GiftCardEntity::class => \Laminas\ServiceManager\Factory\InvokableFactory::class,
            \GiftCards\V1\Rpc\Entities\GiftCardSuspendEntity::class => \Laminas\ServiceManager\Factory\InvokableFactory::class,
            \GiftCards\V1\Rpc\Entities\GiftCardStolenEntity::class => \Laminas\ServiceManager\Factory\InvokableFactory::class,
            \GiftCards\V1\Rpc\Entities\GiftCardLostEntity::class => \Laminas\ServiceManager\Factory\InvokableFactory::class,
            \GiftCards\V1\Rpc\Entities\GiftCardDeleteEntity::class => \Laminas\ServiceManager\Factory\InvokableFactory::class,
            \GiftCards\V1\Rpc\Entities\GiftCardDamagedEntity::class => \Laminas\ServiceManager\Factory\InvokableFactory::class,
            \GiftCards\V1\Rpc\Entities\GiftCardActivateEntity::class => \Laminas\ServiceManager\Factory\InvokableFactory::class,
            \GiftCards\V1\Rpc\Services\Files\Bulks\Factory::class => \Laminas\ServiceManager\Factory\InvokableFactory::class,
            \GiftCards\V1\Rpc\Mappers\ClientMapper::class => \GiftCards\V1\Rpc\Factories\Mappers\ClientMapperFactory::class,
            \GiftCards\V1\Rpc\Mappers\GiftCardMapper::class => \GiftCards\V1\Rpc\Factories\Mappers\GiftCardMapperFactory::class,
            \GiftCards\V1\Rpc\Mappers\ChannelsMapper::class => \GiftCards\V1\Rpc\Factories\Mappers\ChannelsMapperFactory::class,
            \GiftCards\V1\Rpc\Mappers\CurrencyMapper::class => \GiftCards\V1\Rpc\Factories\Mappers\CurrencyMapperFactory::class,
            \GiftCards\V1\Rpc\Mappers\TransactionsMapper::class => \GiftCards\V1\Rpc\Factories\Mappers\TransactionsMapperFactory::class,
            \GiftCards\V1\Rpc\Mappers\TransactionTypeMapper::class => \GiftCards\V1\Rpc\Factories\Mappers\TransactionTypeMapperFactory::class,
            \GiftCards\V1\Rpc\Mappers\GiftCardReplacementMapper::class => \GiftCards\V1\Rpc\Factories\Mappers\GiftCardReplacementMapperFactory::class,
            \GiftCards\V1\Rpc\Services\Paginator\Adapters\DummyAdapter::class => \GiftCards\V1\Rpc\Factories\Adapters\DummyPaginatorAdapterFactory::class,
            \GiftCards\V1\Rpc\Entities\DummyCollection::class => \GiftCards\V1\Rpc\Factories\Services\DummyCollectionFactory::class,
            \GiftCards\V1\Rpc\Services\Identification\Permissions::class => \GiftCards\V1\Rpc\Factories\Services\Identification\PermissionsFactory::class,
            \GiftCards\V1\Rpc\Mappers\UsersClientsMapper::class => \GiftCards\V1\Rpc\Factories\Mappers\UsersClientsMapperFactory::class,
            \GiftCards\V1\Rpc\Mappers\TransactionFailureMapper::class => \GiftCards\V1\Rpc\Factories\Mappers\TransactionFailureMapperFactory::class,
            \GiftCards\V1\Rpc\Services\CustomValidations\CustomValidation::class => \GiftCards\V1\Rpc\Factories\Services\CustomValidations\CustomValidationFactory::class,
            \GiftCards\V1\Rpc\Services\Barcode\BarcodeGenerator::class => \GiftCards\V1\Rpc\Factories\Services\Barcode\BarcodeGeneratorFactory::class,
            \GiftCards\V1\Rpc\Mappers\BarcodeMapper::class => \GiftCards\V1\Rpc\Factories\Mappers\BarcodeMapperFactory::class,
            \GiftCards\V1\Rpc\Mappers\GiftCardExternalDataMapper::class => \GiftCards\V1\Rpc\Factories\Mappers\GiftCardExternalDataMapperFactory::class,
            \GiftCards\V1\Rpc\Mappers\OauthUsersMapper::class => \GiftCards\V1\Rpc\Factories\Mappers\OauthUsersMapperFactory::class,
        ],
        'aliases' => [
            'ApiResponse' => \GiftCards\V1\Rpc\Entities\ApiResponse::class,
            'ClientEntity' => \GiftCards\V1\Rpc\Entities\ClientEntity::class,
            'ClientSettingsUpdateEntity' => \GiftCards\V1\Rpc\Entities\ClientSettingsUpdateEntity::class,
            'GiftCardEntity' => \GiftCards\V1\Rpc\Entities\GiftCardEntity::class,
            'GiftCardSuspendEntity' => \GiftCards\V1\Rpc\Entities\GiftCardSuspendEntity::class,
            'GiftCardStolenEntity' => \GiftCards\V1\Rpc\Entities\GiftCardStolenEntity::class,
            'GiftCardLostEntity' => \GiftCards\V1\Rpc\Entities\GiftCardLostEntity::class,
            'GiftCardDeleteEntity' => \GiftCards\V1\Rpc\Entities\GiftCardDeleteEntity::class,
            'GiftCardDamagedEntity' => \GiftCards\V1\Rpc\Entities\GiftCardDamagedEntity::class,
            'GiftCardActivateEntity' => \GiftCards\V1\Rpc\Entities\GiftCardActivateEntity::class,
            'BulkFilesFactory' => \GiftCards\V1\Rpc\Services\Files\Bulks\Factory::class,
            'ClientMapper' => \GiftCards\V1\Rpc\Mappers\ClientMapper::class,
            'GiftCardMapper' => \GiftCards\V1\Rpc\Mappers\GiftCardMapper::class,
            'ChannelsMapper' => \GiftCards\V1\Rpc\Mappers\ChannelsMapper::class,
            'CurrencyMapper' => \GiftCards\V1\Rpc\Mappers\CurrencyMapper::class,
            'TransactionsMapper' => \GiftCards\V1\Rpc\Mappers\TransactionsMapper::class,
            'TransactionTypeMapper' => \GiftCards\V1\Rpc\Mappers\TransactionTypeMapper::class,
            'GiftCardReplacementMapper' => \GiftCards\V1\Rpc\Mappers\GiftCardReplacementMapper::class,
            'DummyPaginatorAdapter' => \GiftCards\V1\Rpc\Services\Paginator\Adapters\DummyAdapter::class,
            'DummyCollection' => \GiftCards\V1\Rpc\Entities\DummyCollection::class,
            'UserPermissions' => \GiftCards\V1\Rpc\Services\Identification\Permissions::class,
            'UsersClientsMapper' => \GiftCards\V1\Rpc\Mappers\UsersClientsMapper::class,
            'TransactionFailureMapper' => \GiftCards\V1\Rpc\Mappers\TransactionFailureMapper::class,
            'CustomValidation' => \GiftCards\V1\Rpc\Services\CustomValidations\CustomValidation::class,
            'BarcodeGenerator' => \GiftCards\V1\Rpc\Services\Barcode\BarcodeGenerator::class,
            'BarcodeMapper' => \GiftCards\V1\Rpc\Mappers\BarcodeMapper::class,
            'GiftCardExternalDataMapper' => \GiftCards\V1\Rpc\Mappers\GiftCardExternalDataMapper::class,
            'OauthUsersMapper' => \GiftCards\V1\Rpc\Mappers\OauthUsersMapper::class,
            'RecordTransactionFailure' => \GiftCards\V1\Rpc\Mappers\TransactionFailureMapper::class,
        ],
    ],
    'router' => [
        'routes' => [
            'gift-cards.rpc.get-client' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/getclient',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GetClient\\Controller',
                        'action' => 'getClient',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-get-balance' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/get-balance',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardGetBalance\\Controller',
                        'action' => 'giftCardGetBalance',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-transfer' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-card-transfer',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardTransfer\\Controller',
                        'action' => 'giftCardTransfer',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-transaction' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-card-transaction',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardTransaction\\Controller',
                        'action' => 'giftCardTransaction',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-search' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-card-search[/:barcode]',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardSearch\\Controller',
                        'action' => 'giftCardSearch',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-add' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-card-add',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardAdd\\Controller',
                        'action' => 'giftCardAdd',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-update' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-card-update',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardUpdate\\Controller',
                        'action' => 'giftCardUpdate',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-activate' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-card-activate',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardActivate\\Controller',
                        'action' => 'giftCardActivate',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-suspend' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-card-suspend',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardSuspend\\Controller',
                        'action' => 'giftCardSuspend',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-delete' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-card-delete',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardDelete\\Controller',
                        'action' => 'giftCardDelete',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-stolen' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-card-stolen',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardStolen\\Controller',
                        'action' => 'giftCardStolen',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-damaged' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-card-damaged',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardDamaged\\Controller',
                        'action' => 'giftCardDamaged',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-lost' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-card-lost',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardLost\\Controller',
                        'action' => 'giftCardLost',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-refund' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-card-refund',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardRefund\\Controller',
                        'action' => 'giftCardRefund',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-get-balance-xml' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/get-balance-xml',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardGetBalanceXml\\Controller',
                        'action' => 'giftCardGetBalanceXml',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-add-bulk' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-cards-bulk/gift-card-add',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardAddBulk\\Controller',
                        'action' => 'giftCardAddBulk',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-update-bulk' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-cards-bulk/gift-card-update',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardUpdateBulk\\Controller',
                        'action' => 'giftCardUpdateBulk',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-transaction-history' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-card-transaction-history',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardTransactionHistory\\Controller',
                        'action' => 'giftCardTransactionHistory',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-transactions-failure' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/get-transactions-failure',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardTransactionsFailure\\Controller',
                        'action' => 'giftCardTransactionsFailure',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-delete-bulk' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-cards-bulk/delete',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardDeleteBulk\\Controller',
                        'action' => 'giftCardDeleteBulk',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-suspend-bulk' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-cards-bulk/suspend',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardSuspendBulk\\Controller',
                        'action' => 'giftCardSuspendBulk',
                    ],
                ],
            ],
            'gift-cards.rpc.gift-card-activate-bulk' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-cards-bulk/activate',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardActivateBulk\\Controller',
                        'action' => 'giftCardActivateBulk',
                    ],
                ],
            ],
            'gift-cards.rpc.client-update-settings' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/client-update-settings',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\ClientUpdateSettings\\Controller',
                        'action' => 'clientUpdateSettings',
                    ],
                ],
            ],
            'gift-cards.rpc.client-get-cards' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/client-cards',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\ClientGetCards\\Controller',
                        'action' => 'clientGetCards',
                    ],
                ],
            ],
            'gift-cards.rpc.reports-transactions' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/reports-transactions',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\ReportsTransactions\\Controller',
                        'action' => 'reportsTransactions',
                    ],
                ],
            ],
            'gift-cards.rpc.gif-card-barcode-generator' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/gift-card-new-voucher-code',
                    'defaults' => [
                        'controller' => 'GiftCards\\V1\\Rpc\\GiftCardBarcodeGenerator\\Controller',
                        'action' => 'giftCardBarcodeGenerator',
                    ],
                ],
            ],
        ],
    ],
    'api-tools-versioning' => [
        'uri' => [
            0 => 'gift-cards.rpc.get-client',
            1 => 'gift-cards.rpc.gift-card-get-balance',
            2 => 'gift-cards.rpc.gift-card-transfer',
            3 => 'gift-cards.rpc.gift-card-transaction',
            4 => 'gift-cards.rpc.gift-card-search',
            5 => 'gift-cards.rpc.gift-card-add',
            6 => 'gift-cards.rpc.gift-card-update',
            7 => 'gift-cards.rpc.gift-card-activate',
            8 => 'gift-cards.rpc.gift-card-suspend',
            9 => 'gift-cards.rpc.gift-card-delete',
            10 => 'gift-cards.rpc.gift-card-stolen',
            11 => 'gift-cards.rpc.gift-card-damaged',
            12 => 'gift-cards.rpc.gift-card-lost',
            13 => 'gift-cards.rpc.gift-card-refund',
            14 => 'gift-cards.rpc.gift-card-get-balance-xml',
            15 => 'gift-cards.rpc.gift-card-add-bulk',
            16 => 'gift-cards.rpc.gift-card-update-bulk',
            17 => 'gift-cards.rpc.gift-card-transaction-history',
            18 => 'gift-cards.rpc.gift-card-transactions-failure',
            19 => 'gift-cards.rpc.gift-card-delete-bulk',
            20 => 'gift-cards.rpc.gift-card-suspend-bulk',
            21 => 'gift-cards.rpc.gift-card-activate-bulk',
            22 => 'gift-cards.rpc.client-update-settings',
            23 => 'gift-cards.rpc.client-get-cards',
            24 => 'gift-cards.rpc.reports-transactions',
            25 => 'gift-cards.rpc.gif-card-barcode-generator',
        ],
    ],
    'api-tools-rpc' => [
        'GiftCards\\V1\\Rpc\\GetClient\\Controller' => [
            'service_name' => 'GetClient',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.get-client',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardGetBalance\\Controller' => [
            'service_name' => 'GiftCardGetBalance',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-get-balance',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardTransfer\\Controller' => [
            'service_name' => 'GiftCardTransfer',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-transfer',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardTransaction\\Controller' => [
            'service_name' => 'GiftCardTransaction',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-transaction',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardSearch\\Controller' => [
            'service_name' => 'GiftCardSearch',
            'http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-search',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardAdd\\Controller' => [
            'service_name' => 'GiftCardAdd',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-add',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardUpdate\\Controller' => [
            'service_name' => 'GiftCardUpdate',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-update',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardActivate\\Controller' => [
            'service_name' => 'GiftCardActivate',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-activate',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardSuspend\\Controller' => [
            'service_name' => 'GiftCardSuspend',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-suspend',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardDelete\\Controller' => [
            'service_name' => 'GiftCardDelete',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-delete',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardStolen\\Controller' => [
            'service_name' => 'GiftCardStolen',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-stolen',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardDamaged\\Controller' => [
            'service_name' => 'GiftCardDamaged',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-damaged',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardLost\\Controller' => [
            'service_name' => 'GiftCardLost',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-lost',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardRefund\\Controller' => [
            'service_name' => 'GiftCardRefund',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-refund',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardGetBalanceXml\\Controller' => [
            'service_name' => 'GiftCardGetBalanceXml',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-get-balance-xml',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardAddBulk\\Controller' => [
            'service_name' => 'GiftCardAddBulk',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-add-bulk',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardUpdateBulk\\Controller' => [
            'service_name' => 'GiftCardUpdateBulk',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-update-bulk',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardTransactionHistory\\Controller' => [
            'service_name' => 'GiftCardTransactionHistory',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-transaction-history',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardTransactionsFailure\\Controller' => [
            'service_name' => 'GiftCardTransactionsFailure',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-transactions-failure',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardDeleteBulk\\Controller' => [
            'service_name' => 'GiftCardDeleteBulk',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-delete-bulk',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardSuspendBulk\\Controller' => [
            'service_name' => 'GiftCardSuspendBulk',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-suspend-bulk',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardActivateBulk\\Controller' => [
            'service_name' => 'GiftCardActivateBulk',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gift-card-activate-bulk',
        ],
        'GiftCards\\V1\\Rpc\\ClientUpdateSettings\\Controller' => [
            'service_name' => 'ClientUpdateSettings',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.client-update-settings',
        ],
        'GiftCards\\V1\\Rpc\\ClientGetCards\\Controller' => [
            'service_name' => 'ClientGetCards',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.client-get-cards',
        ],
        'GiftCards\\V1\\Rpc\\ReportsTransactions\\Controller' => [
            'service_name' => 'ReportsTransactions',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.reports-transactions',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardBarcodeGenerator\\Controller' => [
            'service_name' => 'GiftCardBarcodeGenerator',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'gift-cards.rpc.gif-card-barcode-generator',
        ],
    ],
    'api-tools-content-negotiation' => [
        'controllers' => [
            'GiftCards\\V1\\Rpc\\GetClient\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardGetBalance\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardTransfer\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardTransaction\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardSearch\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardAdd\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardUpdate\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardActivate\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardSuspend\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardDelete\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardStolen\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardDamaged\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardLost\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardRefund\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardGetBalanceXml\\Controller' => 'Documentation',
            'GiftCards\\V1\\Rpc\\GiftCardAddBulk\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardUpdateBulk\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardTransactionHistory\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardTransactionsFailure\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardDeleteBulk\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardSuspendBulk\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardActivateBulk\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\ClientUpdateSettings\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\ClientGetCards\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\ReportsTransactions\\Controller' => 'HalJson',
            'GiftCards\\V1\\Rpc\\GiftCardBarcodeGenerator\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'GiftCards\\V1\\Rpc\\GetClient\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardGetBalance\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardTransfer\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardTransaction\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardSearch\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardAdd\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardUpdate\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardActivate\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardSuspend\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardDelete\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardStolen\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardDamaged\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardLost\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardRefund\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardGetBalanceXml\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
                3 => 'application/xml',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardAddBulk\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardUpdateBulk\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardTransactionHistory\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardTransactionsFailure\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardDeleteBulk\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardSuspendBulk\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardActivateBulk\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\ClientUpdateSettings\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\ClientGetCards\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\ReportsTransactions\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardBarcodeGenerator\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
        ],
        'content_type_whitelist' => [
            'GiftCards\\V1\\Rpc\\GetClient\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardGetBalance\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardTransfer\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardTransaction\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardSearch\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardAdd\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardUpdate\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardActivate\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardSuspend\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardDelete\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardStolen\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardDamaged\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardLost\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardRefund\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardGetBalanceXml\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'application/xml',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardAddBulk\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'multipart/form-data',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardUpdateBulk\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'multipart/from-data',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardTransactionHistory\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardTransactionsFailure\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardDeleteBulk\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'multipart/form-data',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardSuspendBulk\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'multipart/form-data',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardActivateBulk\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
                2 => 'multipart/form-data',
            ],
            'GiftCards\\V1\\Rpc\\ClientUpdateSettings\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\ClientGetCards\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\ReportsTransactions\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
            'GiftCards\\V1\\Rpc\\GiftCardBarcodeGenerator\\Controller' => [
                0 => 'application/vnd.gift-cards.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'api-tools-content-validation' => [
        'GiftCards\\V1\\Rpc\\GiftCardGetBalance\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardGetBalance\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardTransfer\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardTransfer\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardTransaction\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardTransaction\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardSearch\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardSearch\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardAdd\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardAdd\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardUpdate\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardUpdate\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardActivate\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardActivate\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardSuspend\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardSuspend\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardDelete\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardDelete\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardStolen\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardStolen\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardDamaged\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardDamaged\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardLost\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardLost\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardRefund\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardRefund\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardGetBalanceXml\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardGetBalanceXml\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardAddBulk\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardAddBulk\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardUpdateBulk\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardUpdateBulk\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardTransactionHistory\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardTransactionHistory\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardTransactionsFailure\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardTransactionsFailure\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardDeleteBulk\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardDeleteBulk\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardSuspendBulk\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardSuspendBulk\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardActivateBulk\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardActivateBulk\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\ClientUpdateSettings\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\ClientUpdateSettings\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\ClientGetCards\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\ClientGetCards\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\ReportsTransactions\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\ReportsTransactions\\Validator',
        ],
        'GiftCards\\V1\\Rpc\\GiftCardBarcodeGenerator\\Controller' => [
            'input_filter' => 'GiftCards\\V1\\Rpc\\GiftCardBarcodeGenerator\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'GiftCards\\V1\\Rpc\\GiftCardGetBalance\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode',
                'description' => 'Gift Card Barcode',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardTransfer\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode_to',
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode_from',
            ],
            2 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\I18n\Validator\Alpha::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'currency_code',
            ],
            3 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\I18n\Validator\IsFloat::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'amount',
            ],
            4 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\I18n\Validator\Alpha::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'channel',
            ],
            5 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\I18n\Validator\Alnum::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'request_from',
            ],
            6 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\I18n\Validator\DateTime::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'transaction_datetime',
            ],
            7 => [
                'required' => true,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'transaction_type',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardTransaction\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode',
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\I18n\Validator\Alpha::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'currency_code',
            ],
            2 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\I18n\Validator\IsFloat::class,
                        'options' => [],
                    ],
                    2 => [
                        'name' => \Laminas\Validator\GreaterThan::class,
                        'options' => [
                            'message' => 'The amount can not be less then 0.00',
                            'min' => '0',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'amount',
            ],
            3 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\I18n\Validator\Alnum::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'channel',
            ],
            4 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\I18n\Validator\Alnum::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'request_from',
            ],
            5 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^(\\d{4})-(\\d{2})-(\\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'transaction_datetime',
            ],
            6 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/REDEMPTION/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'transaction_type',
            ],
            7 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\I18n\Validator\Alnum::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'external_id',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardSearch\\Validator' => [
            0 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode',
            ],
            1 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\I18n\Validator\Alnum::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'customer_id',
            ],
            2 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\EmailAddress::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'customer_email',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardAdd\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode',
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\I18n\Validator\Alpha::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\StringLength::class,
                        'options' => [
                            'min' => '3',
                            'max' => '3',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Filter\StringToUpper::class,
                        'options' => [],
                    ],
                ],
                'name' => 'currency_code',
            ],
            2 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\I18n\Validator\IsFloat::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'initial_load',
            ],
            3 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/1|0/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'is_physical',
            ],
            4 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Date::class,
                        'options' => [
                            'format' => 'Y-m-d',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'start_date',
            ],
            5 => [
                'required' => false,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Filter\ToNull::class,
                        'options' => [],
                    ],
                ],
                'name' => 'channels',
            ],
            6 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\I18n\Validator\Alnum::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\StringLength::class,
                        'options' => [
                            'min' => '1',
                            'max' => '35',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'external_id',
            ],
            7 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\EmailAddress::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'customer_email',
            ],
            8 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\I18n\Validator\Alnum::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\StringLength::class,
                        'options' => [
                            'min' => '3',
                            'max' => '10',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'promo_code',
            ],
            9 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\I18n\Validator\IsInt::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Between::class,
                        'options' => [
                            'min' => '1',
                            'max' => '99',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'expiry_period_in_month',
            ],
            10 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\I18n\Validator\Alnum::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\StringLength::class,
                        'options' => [
                            'min' => '1',
                            'max' => '35',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'customer_id',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardUpdate\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode',
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/1|0/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'is_physical',
            ],
            2 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Date::class,
                        'options' => [
                            'format' => 'Y-m-d',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'start_date',
            ],
            3 => [
                'required' => true,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\ToNull::class,
                        'options' => [],
                    ],
                ],
                'name' => 'channels',
            ],
            4 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Date::class,
                        'options' => [
                            'format' => 'Y-m-d',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'end_date',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardActivate\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardSuspend\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardDelete\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardStolen\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode',
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'new_card_barcode',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardDamaged\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode',
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'new_card_barcode',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardLost\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode',
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'new_card_barcode',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardRefund\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode',
                'description' => '',
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\I18n\Validator\Alnum::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Filter\StringToUpper::class,
                        'options' => [],
                    ],
                ],
                'name' => 'currency_code',
            ],
            2 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\I18n\Validator\IsFloat::class,
                        'options' => [],
                    ],
                    2 => [
                        'name' => \Laminas\Validator\GreaterThan::class,
                        'options' => [
                            'message' => 'The amount can not be less then 0.00',
                            'min' => '0',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'amount',
            ],
            3 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\I18n\Validator\Alnum::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'channel',
            ],
            4 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\I18n\Validator\Alnum::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'request_from',
            ],
            5 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^(\\d{4})-(\\d{2})-(\\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'transaction_datetime',
            ],
            6 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/REFUND/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'transaction_type',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardGetBalanceXml\\Validator' => [
            0 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardAddBulk\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\File\UploadFile::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
//                    0 => [
//                        'name' => \Laminas\Filter\File\RenameUpload::class,
//                        'options' => [
//                            'randomize' => true,
//                            'target' => 'data/tmp',
//                        ],
//                    ],
                ],
                'name' => 'bulk',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardUpdateBulk\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\File\UploadFile::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
//                    0 => [
//                        'name' => \Laminas\Filter\File\RenameUpload::class,
//                        'options' => [
//                            'randomize' => true,
//                            'target' => 'data/tmp',
//                        ],
//                    ],
                ],
                'name' => 'bulk',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardTransactionHistory\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/^[a-zA-Z0-9 .\\\\-]+$/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode',
            ],
            1 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Date::class,
                        'options' => [
                            'format' => 'Y-m-d',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'start_date',
            ],
            2 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Date::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'end_date',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardTransactionsFailure\\Validator' => [
            0 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Date::class,
                        'options' => [
                            'format' => 'Y-m-d',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'dateFrom',
            ],
            1 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Date::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'dateTo',
            ],
            2 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Digits::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'page',
            ],
            3 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Digits::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'count_per_page',
            ],
            4 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Digits::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'transaction_type',
            ],
            5 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Digits::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'is_paginated',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardDeleteBulk\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\File\UploadFile::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
//                    0 => [
//                        'name' => \Laminas\Filter\File\RenameUpload::class,
//                        'options' => [
//                            'randomize' => true,
//                            'target' => 'data/tmp',
//                        ],
//                    ],
                ],
                'name' => 'bulk',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardSuspendBulk\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\File\UploadFile::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
//                    0 => [
//                        'name' => \Laminas\Filter\File\RenameUpload::class,
//                        'options' => [
//                            'randomize' => true,
//                            'target' => 'data/tmp',
//                        ],
//                    ],
                ],
                'name' => 'bulk',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardActivateBulk\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\File\UploadFile::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
//                    0 => [
//                        'name' => \Laminas\Filter\File\RenameUpload::class,
//                        'options' => [
//                            'randomize' => true,
//                            'target' => 'data/tmp',
//                        ],
//                    ],
                ],
                'name' => 'bulk',
            ],
        ],
        'GiftCards\\V1\\Rpc\\ClientUpdateSettings\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\I18n\Validator\IsInt::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'expiry_period_quantity',
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/1|0/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'restrict_to_currency',
            ],
            2 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/1|0/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'multi_channel_support',
            ],
            3 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/1|0/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'redemption_support',
            ],
            4 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/1|0/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'expiry_on_zero_balance',
            ],
            5 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/1|0/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'refund_to_zero_balance',
            ],
            6 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Between::class,
                        'options' => [
                            'message' => 'Please use value between 0-255',
                            'min' => '0',
                            'max' => '255',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'maximum_redemption_used',
            ],
            7 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\I18n\Validator\IsFloat::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'minimum_initial_load',
            ],
            8 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\I18n\Validator\IsFloat::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'maximum_initial_load',
            ],
            9 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/0|1/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'log_action',
                'continue_if_empty' => true,
                'allow_empty' => true,
            ],
            10 => [
                'required' => false,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'barcode_validation_rule',
                'allow_empty' => true,
            ],
        ],
        'GiftCards\\V1\\Rpc\\ClientGetCards\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Digits::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'page',
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Digits::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'count_per_page',
            ],
            2 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/1|0/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'return_all',
            ],
            3 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Date::class,
                        'options' => [
                            'format' => 'Y-m-d',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'dateFrom',
            ],
            4 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Date::class,
                        'options' => [
                            'format' => 'Y-m-d',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'dateTo',
            ],
        ],
        'GiftCards\\V1\\Rpc\\ReportsTransactions\\Validator' => [
            0 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Regex::class,
                        'options' => [
                            'pattern' => '/0|1/',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'is_paginated',
            ],
            1 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Date::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'dateFrom',
            ],
            2 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Date::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'dateTo',
            ],
            3 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Digits::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'page',
            ],
            4 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Digits::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'count_per_page',
            ],
        ],
        'GiftCards\\V1\\Rpc\\GiftCardBarcodeGenerator\\Validator' => [
            0 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\Digits::class,
                        'options' => [],
                    ],
                    1 => [
                        'name' => \Laminas\Validator\Between::class,
                        'options' => [
                            'min' => '0',
                            'max' => '5000',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'numberOfBarcodes',
                'continue_if_empty' => true,
            ],
        ],
    ],
    'api-tools-mvc-auth' => [
        'authorization' => [
            'GiftCards\\V1\\Rpc\\GiftCardBarcodeGenerator\\Controller' => [
                'actions' => [
                    'giftCardBarcodeGenerator' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardGetBalance\\Controller' => [
                'actions' => [
                    'giftCardGetBalance' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GetClient\\Controller' => [
                'actions' => [
                    'getClient' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardTransfer\\Controller' => [
                'actions' => [
                    'giftCardTransfer' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardTransaction\\Controller' => [
                'actions' => [
                    'giftCardTransaction' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardSearch\\Controller' => [
                'actions' => [
                    'giftCardSearch' => [
                        'GET' => true,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardAdd\\Controller' => [
                'actions' => [
                    'giftCardAdd' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardUpdate\\Controller' => [
                'actions' => [
                    'giftCardUpdate' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardActivate\\Controller' => [
                'actions' => [
                    'giftCardActivate' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardSuspend\\Controller' => [
                'actions' => [
                    'giftCardSuspend' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardDelete\\Controller' => [
                'actions' => [
                    'giftCardDelete' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardStolen\\Controller' => [
                'actions' => [
                    'giftCardStolen' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardDamaged\\Controller' => [
                'actions' => [
                    'giftCardDamaged' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardLost\\Controller' => [
                'actions' => [
                    'giftCardLost' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardRefund\\Controller' => [
                'actions' => [
                    'giftCardRefund' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardGetBalanceXml\\Controller' => [
                'actions' => [
                    'giftCardGetBalanceXml' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardAddBulk\\Controller' => [
                'actions' => [
                    'giftCardAddBulk' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardUpdateBulk\\Controller' => [
                'actions' => [
                    'giftCardUpdateBulk' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardTransactionHistory\\Controller' => [
                'actions' => [
                    'giftCardTransactionHistory' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardTransactionsFailure\\Controller' => [
                'actions' => [
                    'giftCardTransactionsFailure' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardDeleteBulk\\Controller' => [
                'actions' => [
                    'giftCardDeleteBulk' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardSuspendBulk\\Controller' => [
                'actions' => [
                    'giftCardSuspendBulk' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\GiftCardActivateBulk\\Controller' => [
                'actions' => [
                    'giftCardActivateBulk' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\ClientUpdateSettings\\Controller' => [
                'actions' => [
                    'clientUpdateSettings' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\ClientGetCards\\Controller' => [
                'actions' => [
                    'clientGetCards' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
            'GiftCards\\V1\\Rpc\\ReportsTransactions\\Controller' => [
                'actions' => [
                    'reportsTransactions' => [
                        'GET' => false,
                        'POST' => true,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
        ],
    ],
];
