<?php
namespace  GiftCards\V1\Rpc\Services\Barcode;

use GiftCards\V1\Rpc\Mappers\ClientMapper;
use Laminas\Math\Rand;
use GiftCards\V1\Rpc\Mappers\GiftCardMapper;
use GiftCards\V1\Rpc\Mappers\BarcodeMapper;

class BarcodeGenerator
{
    /**
     * 
     * @var ClientMapper
     */
    protected $clientMapper;
    /**
     * 
     * @var GiftCardMapper
     */
    protected $cardMapper;
    /**
     * 
     * @var BarcodeMapper
     */
    protected $barcodeMapper;
    /**
     *
     * @var int
     */
    protected $defaultBarcodeNumOfChar = 20;
    /**
     *
     * @var string
     */
    protected $defaultBarcodeGenerateRule = 'alphanumeric';
    /**
     * 
     * @var string
     */
    protected $numericString = '0123456789';
    /**
     * 
     * @var string
     */
    protected $alphanumericString = '2346789BDFGHJLNPRSTVWYZ';
    
    /**
     * Inject dependencies 
     * @param ClientMapper $clientMapper
     */
    public function __construct(ClientMapper $clientMapper, GiftCardMapper $cardMapper, BarcodeMapper $barcodeMapper)
    {
        $this->clientMapper = $clientMapper;
        $this->cardMapper = $cardMapper;
        $this->barcodeMapper = $barcodeMapper;
    }
    
    /**
     * Generate valid barcode based on client settings
     * @param string $clientId
     */
    public function generateNewBarcode($clientId, $numberOfBarcods)
    {
        $barcode = [];
        $this->setClientSettings($clientId);
        
        for ($i = 0; $i < $numberOfBarcods; $i++) {
            //loop untill the code was not exist or preselected
            $barcodeExist = true;
            while ($barcodeExist === true) {
                $barcode[$i] = $this->barcodeString();
                $isUnique = $this->cardMapper->isUniqueBarcode($barcode[$i], $clientId);
                $isUniqueInBarcodeTab = $this->barcodeMapper->isUniqueBarcode($barcode[$i], $clientId);
                if($isUnique === true && $isUniqueInBarcodeTab === true){
                    $this->barcodeMapper->save($barcode[$i], $clientId);
                    $barcodeExist = false;
                }
            }    
        }
        
        return [$barcode];  
    }
    /**
     * return barcode string 
     * @return NULL|string
     */
    protected function barcodeString()
    {
        $barcode = null;
        
        if($this->defaultBarcodeGenerateRule == 'alphanumeric'){
            $barcode = Rand::getString($this->defaultBarcodeNumOfChar, $this->alphanumericString);
        }else{
            $barcode = Rand::getString($this->defaultBarcodeNumOfChar, $this->numericString);
        }
        
        return $barcode;
    }

    /**
     * Set the client settings fro barcode generator
     * @param string $clientId
     * @throws \GiftCards\V1\Rpc\Services\Exceptions\ApiException
     */
    protected function setClientSettings($clientId)
    {
       $settings = $this->clientMapper->getSettingsByClientId($clientId);
       
       if(null !== $settings->getBarcodeGenerateRule() && $settings->getBarcodeGenerateRule() != '')
       {
           $this->defaultBarcodeGenerateRule = $settings->getBarcodeGenerateRule();
       }
       
       if(null !== $settings->getBarcodeNumOfChar() && $settings->getBarcodeNumOfChar() != '')
       {
           $this->defaultBarcodeNumOfChar = $settings->getBarcodeNumOfChar();
       }
       
       return;
    }
}
