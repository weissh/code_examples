<?php
namespace GiftCards\V1\Rpc\Services\CustomValidations;

use GiftCards\V1\Rpc\Mappers\ClientMapper;
use GiftCards\V1\Rpc\Entities\ClientSettingsEntity;
use GiftCards\V1\Rpc\Services\Exceptions\ApiException;
use Laminas\Validator\Regex;
/**
 * 
 * @author haniw
 * 
 * 
 *        
 */
class CustomValidation
{
    /**
     * 
     * @var string
     */
    protected $clientId;
    
    /**
     * 
     * @var ClientMapper
     */
    protected $clientMapper;
    
    /**
     * 
     * @var ClientSettingsEntity
     */
    protected $clientSettings;
    
    public function __construct(ClientMapper $clientMapper){
        $this->clientMapper = $clientMapper;
    }

    /**
     * Validate given barcode to client rule
     * @param string $clientId
     * @param string $barcode
     * @return bool
     * @throws ApiException
     */
    public function validBarcodeFormat($clientId, $barcode)
    {
        $isValid  = false;
        
        $this->clientId = $clientId;
        $clientBarcodeValidationRule = $this->getClientBarcodeValidationRule();
        
        if($clientBarcodeValidationRule == null)
        {
           return true; 
        }
        
        // validation rule are set 
        $validator = new Regex(array('pattern' => $clientBarcodeValidationRule));
        if($validator->isValid($barcode)){
             $isValid  = true;
        }
        
        return $isValid;
    }

    /**
     * Get client setting from clients_settings
     * @return string
     * @throws ApiException
     */
    protected function getClientBarcodeValidationRule()
    {
        $rule = null;
        
        $this->clientSettings = $this->clientMapper->getSettingsByClientId($this->clientId);
        
        if($this->clientSettings instanceof ClientSettingsEntity){
            $rule = $this->clientSettings->getBarcodeValidationRule();
        }
                
        return $rule;
    }
    
}
