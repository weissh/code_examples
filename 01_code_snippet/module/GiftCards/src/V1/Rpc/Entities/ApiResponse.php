<?php

namespace GiftCards\V1\Rpc\Entities;

use Exception;

/**
 * Response to be passed to HAL response.
 * Response is ERROR by default.
 */
class ApiResponse extends AbstractEntity {

    /**
     *
     * @var string
     */
    protected $responseCode = '500';

    /**
     *
     * @var string
     */
    protected $responseDescription = "Internal error";

    /**
     * Set from Exception
     * @param Exception $e
     */
     public function setFromException(Exception $e) {

         $this->responseCode = $e->getCode();
         $this->responseDescription = $e->getMessage();
     }

    /**
     * Set normal response.
     */
    public function setSuccess() {

        $this->responseCode = "000";
        $this->responseDescription = "Normal";
    }


    /**
     * Internal API response code
     * @return string
     */
    public function getResponseCode() {
        return $this->responseCode;
    }

    /**
     * Internal API response description
     * @return string
     */
    public function getResponseDescription() {
        return $this->responseDescription;
    }

    /**
     *
     * @param string $responseCode
     * @return ApiResponse
     */
    public function setResponseCode($responseCode) {
        $this->responseCode = $responseCode;
        return $this;
    }

    /**
     *
     * @param string $responseDescription
     * @return ApiResponse
     */
    public function setResponseDescription($responseDescription) {
        $this->responseDescription = $responseDescription;
        return $this;
    }


}
