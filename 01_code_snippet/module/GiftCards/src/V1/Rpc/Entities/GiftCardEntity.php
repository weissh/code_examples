<?php
namespace GiftCards\V1\Rpc\Entities;

use GiftCards\V1\Rpc\Services\Paginator\Adapters\DummyAdapter;

class GiftCardEntity extends AbstractEntity
{

    /**
     *
     * @var int
     */
    protected $giftCardId = 0;

    /**
     *
     * @var string
     */
    protected $clientCode = '';

    /**
     *
     * @var int
     */
    protected $clientSettingId = 0;

    /**
     *
     * @var int
     */
    protected $giftCardTypesId = 0;

    /**
     *
     * @var int
     */
    protected $giftCardStatusesId = 0;

    /**
     * @var string
     */
    protected $marketingId = '000';

    /**
     *
     * @var string
     */
    protected $currencyCode = '';

    /**
     *
     * @var string /^[a-zA-Z0-9 .\\\\-]+$/
     */
    protected $barcode = '';

    /**
     *
     * @var int
     */
    protected $isPhysical = 0;

    /**
     *
     * @var float
     */
    protected $initialLoad = 0;

    /**
     *
     * @var float
     */
    protected $currentBalance = 0;

    /**
     *
     * @var string msyql datetime
     */
    protected $startDate = '';

    /**
     *
     * @var string mysql datetime
     */
    protected $endDate = '';

    /**
     *
     * @var string mysql datetime
     */
    protected $created = '';

    /**
     *
     * @var ClientSettingsEntity
     */
    protected $giftCardSettings;

    /**
     *
     * @var AbstractCollection
     */
    protected $giftCardChannels;
    
    /**
     *
     * @var array 
     */
    protected $channelsVector = [];

    /**
     *
     * @var string
     */
    protected $status = '';
    
    /**
     * 
     * @var string
     */
    protected $externalId; 
    
    /**
     * 
     * @var string
     */
    protected $customerEmail;
    
    /**
     * 
     * @var string
     */
    protected $customerId;

    /**
     * @var int
     */
    protected $expiryPeriodInMonth;
    
    /**
     * Defaults - to always match interface
     */
    public function __construct() {
        $this->setGiftCardSettings( new ClientSettingsEntity() );
        $this->setGiftCardChannels( new DummyCollection( new DummyAdapter() )); 
    }

    
    /**
     * @return int $giftCardId
     */
    public function getGiftCardId()
    {
        return $this->giftCardId;
    }

    /**
     * @return string $clientId
     */
    public function getClientCode()
    {
        return $this->clientCode;
    }

    /**
     * @return int $clientSettingId
     */
    public function getClientSettingId()
    {
        return $this->clientSettingId;
    }

    /**
     * @return int $giftCardTypesId
     */
    public function getGiftCardTypesId()
    {
        return $this->giftCardTypesId;
    }

    /**
     * @return int $giftCardStatusesId
     */
    public function getGiftCardStatusesId()
    {
        return $this->giftCardStatusesId;
    }

    /**
     * @return string $currencyCode
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @return string $barcode
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @return int $isPhysical
     */
    public function getIsPhysical()
    {
        return $this->isPhysical;
    }

    /**
     * @return float $initialLoad
     */
    public function getInitialLoad()
    {
        return $this->initialLoad;
    }

    /**
     * @return float $currentBalance
     */
    public function getCurrentBalance()
    {
        return $this->currentBalance;
    }

    /**
     * @return string $startDate
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return string $endDate
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @return string $created
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return  ClientSettingsEntity
     */
    public function getGiftCardSettings()
    {
        return $this->giftCardSettings;
    }

    /**
     * @return AbstractCollection
     */
    public function getGiftCardChannels()
    {
        return $this->giftCardChannels;
    }

    /**
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $giftCardId
     * @return GiftCardEntity
     */
    public function setGiftCardId($giftCardId)
    {
        $this->giftCardId = $giftCardId;
        return $this;
    }

    /**
     * @param string $clientCode
     * @return GiftCardEntity
     */
    public function setClientCode($clientCode)
    {
        $this->clientCode = $clientCode;
        return $this;
    }

    /**
     * @param int $clientSettingId
     * @return GiftCardEntity
     */
    public function setClientSettingId($clientSettingId)
    {
        $this->clientSettingId = $clientSettingId;
        return $this;
    }

    /**
     * @param int $giftCardTypesId
     * @return GiftCardEntity
     */
    public function setGiftCardTypesId($giftCardTypesId)
    {
        $this->giftCardTypesId = $giftCardTypesId;
        return $this;
    }

    /**
     * @param int $giftCardStatusesId
     * @return GiftCardEntity
     */
    public function setGiftCardStatusesId($giftCardStatusesId)
    {
        $this->giftCardStatusesId = $giftCardStatusesId;
        return $this;
    }

    /**
     * @param string $currencyCode
     * @return GiftCardEntity
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
        return $this;
    }

    /**
     * @param string $barcode
     * @return GiftCardEntity
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @param int $isPhysical
     * @return GiftCardEntity
     */
    public function setIsPhysical($isPhysical)
    {
        $this->isPhysical = $isPhysical;
        return $this;
    }

    /**
     * @param float $initialLoad
     * @return GiftCardEntity
     */
    public function setInitialLoad($initialLoad)
    {
        $this->initialLoad = $initialLoad;
        return $this;
    }

    /**
     * 
     * @param float $currentBalance
     * @return GiftCardEntity
     */
    public function setCurrentBalance($currentBalance)
    {
        $this->currentBalance = $currentBalance;
        return $this;
    }

    /**
     * format YYYY-MM-DD
     * @param string $startDate
     * @return GiftCardEntity
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @param string $endDate
     * @return GiftCardEntity
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @param string $created
     * @return GiftCardEntity
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @param ClientSettingsEntity $giftCardSettings
     * @return GiftCardEntity
     */
    public function setGiftCardSettings(ClientSettingsEntity $giftCardSettings)
    {
        $this->giftCardSettings = $giftCardSettings;
        return $this;
    }

    /**
     * @param AbstractCollection
     * @return GiftCardEntity
     */
    public function setGiftCardChannels(AbstractCollection $giftCardChannels)
    {
        $this->giftCardChannels = $giftCardChannels;
        return $this;
    }

    /**
     * 
     * @param string $status
     * @return GiftCardEntity
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Extracted values of channels
     * @return array
     */
    public function getChannelsVector() {
        return $this->channelsVector;
    }

    /**
     * Extracted values of channels
     * @param array $channelsVector
     * @return GiftCardEntity
     */
    public function setChannelsVector($channelsVector) {
        $this->channelsVector = $channelsVector;
        return $this;
    }

    /**
     * @return string $externalId
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @return string $customerEmail
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * @return string $customerId
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @return string the $marketingId
     */
    public function getMarketingId()
    {
        return $this->marketingId;
    }

    /**
     * @return int $expiryPeriodInMonth
     */
    public function getExpiryPeriodInMonth()
    {
        return $this->expiryPeriodInMonth;
    }

    /**
     * @param string $externalId
     * @return GiftCardEntity
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
        return $this;
    }

    /**
     * @param string $customerEmail
     * @return GiftCardEntity
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;
        return $this;
    }

    /**
     * @param string $customerId
     * @return GiftCardEntity
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
        return $this;
    }

    /**
     * @param string $marketingId
     * @return GiftCardEntity
     */
    public function setMarketingId($marketingId)
    {
        $this->marketingId = $marketingId;
        return $this;
    }

    /**
     * @param string $expiryPeriodInMonth
     * @return GiftCardEntity
     */
    public function setExpiryPeriodInMonth($expiryPeriodInMonth)
    {
        $this->expiryPeriodInMonth = $expiryPeriodInMonth;
        return $this;
    }
}
