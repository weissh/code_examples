<?php

namespace GiftCards\V1\Rpc\Entities;

/**
 *
 * Explicitly tell, that toArray is must have :)
 */
interface HalExtractInterface {
    
    
    /**
     * @return array
     */
    public function toArray();
    
    /**
    * Will remove NULL values and empty arrays on toArray conversion.
    */
    public function enforceCleanup();
   
   /**
    * Will preserve all values, including nulls
    */
    public function disableCleanup();

   /**
    * 
    * @return boolean
    */
    public function getCleanupFlag();
    
    
}
