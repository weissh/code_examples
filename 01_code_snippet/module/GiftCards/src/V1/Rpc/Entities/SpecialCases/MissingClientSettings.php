<?php

namespace GiftCards\V1\Rpc\Entities\SpecialCases;

use GiftCards\V1\Rpc\Entities\ClientSettingsEntity;

/**
 * Not existing Client Settings 
 */
class MissingClientSettings extends ClientSettingsEntity
{}
