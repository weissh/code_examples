<?php
namespace GiftCards\V1\Rpc\Entities\SpecialCases;

use GiftCards\V1\Rpc\Entities\GiftCardTransactionResponseEntity;

/**
 * Not Existing client entity
 */
class MissingGiftCardTransactionResponseEntity extends GiftCardTransactionResponseEntity
{
}
