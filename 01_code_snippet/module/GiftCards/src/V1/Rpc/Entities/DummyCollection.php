<?php
namespace GiftCards\V1\Rpc\Entities;

use Laminas\Paginator\AdapterAggregateInterface;

/**
 * Collection placeholder (empty)
 */
class DummyCollection extends AbstractCollection
{
    /**
     *
     * @var AdapterAggregateInterface
     */
    protected $adapter;

    public function __construct($adapter)
    {
        $this->adapter = $adapter;
        parent::__construct($this->adapter);
    }
}
