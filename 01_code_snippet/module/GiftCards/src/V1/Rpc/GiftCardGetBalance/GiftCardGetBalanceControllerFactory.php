<?php
namespace GiftCards\V1\Rpc\GiftCardGetBalance;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class GiftCardGetBalanceControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $giftCardMapper = $container->get('GiftCardMapper');
        $apiResponse = $container->get('ApiResponse');

        return new GiftCardGetBalanceController($giftCardMapper, $apiResponse);
    }
}
