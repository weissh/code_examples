<?php

namespace GiftCards\V1\Rpc\GiftCardGetBalance;

use Exception;
use Laminas\ApiTools\ContentNegotiation\ViewModel;
use Laminas\ApiTools\Hal\Entity;
use Laminas\Mvc\Controller\AbstractActionController;
use GiftCards\V1\Rpc\Entities\ApiResponse;
use GiftCards\V1\Rpc\Entities\GiftCardGetBalanceEntity;
use GiftCards\V1\Rpc\Mappers\GiftCardMapper;
use GiftCards\V1\Rpc\Services\Exceptions\ApiException;

class GiftCardGetBalanceController extends AbstractActionController
{
    /**
     *
     * @var GiftCardMapper
     */
    protected $giftCardMapper;

    /**
     *
     * @var ApiResponse
     */
    protected $apiResponse;

    /**
     *
     * @var GiftCardGetBalanceEntity
     */
    protected $giftCardGetBalanceEntity;

    protected $clientId;

    /**
     * Inject GiftCardMapper, TransactionsMapper, ApiResponse
     *
     * @param GiftCardMapper $giftCardMapper
     * @param ApiResponse $apiResponse
     */
    public function __construct(GiftCardMapper $giftCardMapper, ApiResponse $apiResponse)
    {
        $this->giftCardMapper = $giftCardMapper;
        $this->apiResponse = $apiResponse;
        $this->giftCardGetBalanceEntity = new GiftCardGetBalanceEntity();
    }

    /**
     * @return ViewModel
     */
    public function giftCardGetBalanceAction()
    {
        $clientId = $this->getidentity()->getAuthenticationIdentity()['client_id'];
        $barcode = $this->bodyParam('barcode');

        try {

            $balance = $this->giftCardMapper->getCardBalanceResponse($barcode, $clientId);

            $this->apiResponse->setSuccess();
            $this->giftCardGetBalanceEntity = $balance;

        } catch (ApiException $e) { // internal response
            $this->apiResponse->setFromException($e);
        } catch (Exception $e) {// general failure
            $this->apiResponse->setFromException(new ApiException(ApiException::GENERAL_FAILURE));
        }

        $halEntity = $this->buildResponse($barcode);

        return new ViewModel(array(
            'payload' => $halEntity
        ));
    }

    /**
     * Build the response in case of:
     *
     * @param string $barcode
     * @return Entity
     */
    protected function buildResponse($barcode)
    {
        $halEntity = new Entity(Array(
            'response' => $this->apiResponse->toArray(),
            'data' => $this->giftCardGetBalanceEntity->toArray()
        ), $barcode);

        return $halEntity;
    }
}
