<?php

namespace GiftCards\V1\Rpc\GiftCardAdd;

use Exception;
use Laminas\ApiTools\Hal\Entity as HalEntity;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\ApiTools\ContentNegotiation\ViewModel;
use GiftCards\V1\Rpc\Entities\ApiResponse;
use GiftCards\V1\Rpc\Services\Exceptions\ApiException;
use GiftCards\V1\Rpc\Entities\GiftCardEntity;
use GiftCards\V1\Rpc\Mappers\GiftCardMapper;
use GiftCards\V1\Rpc\Services\CustomValidations\CustomValidation;

class GiftCardAddController extends AbstractActionController
{
    /**
     *
     * @var GiftCardMapper
     */
    protected $giftCardMapper;

    /**
     *
     * @var ApiResponse
     */
    protected $apiResponse;

    /**
     *
     * @var GiftCardEntity
     */
    protected $cardEntity;

    /**
     *
     * @var CustomValidation
     */
    protected $customValidation;

    /**
     *
     * @param GiftCardMapper $giftCardMapper
     * @param GiftCardEntity $entity
     * @param ApiResponse $apiResponse
     * @param CustomValidation $customValidation
     */
    public function __construct(GiftCardMapper $giftCardMapper,
                                GiftCardEntity $entity,
                                ApiResponse $apiResponse,
                                CustomValidation $customValidation)
    {
        $this->giftCardMapper = $giftCardMapper;
        $this->apiResponse = $apiResponse;
        $this->cardEntity = $entity;
        $this->customValidation = $customValidation;
    }

    public function giftCardAddAction()
    {
        $clientId = $this->getidentity()->getAuthenticationIdentity()['client_id'];

        // data received
        $inputEntity = $this->getInputEntity($clientId);

        try {

            if ($this->customValidation->validBarcodeFormat($clientId, $inputEntity->getBarcode()) == false) {
                //@todo send email / log for unauthorized request
                throw new ApiException($this->giftCardMapper->getInternalResponse('INVALID_BARCODE_FORMAT'));
            }

            // save gift card
            $this->giftCardMapper->addGiftCard($inputEntity);

            // load saved data
            $this->cardEntity = $this->giftCardMapper->getGiftCardDetails($inputEntity->getBarcode(), $clientId);

            // success
            $this->apiResponse->setSuccess();

        } catch (ApiException $e) {
            $this->apiResponse->setFromException($e);
        } catch (Exception $e) { // general failure
            $this->apiResponse->setFromException(new ApiException(ApiException::GENERAL_FAILURE));
        }

        // remove nulls
        $this->cardEntity->enforceCleanup();

        // finalize
        return $this->getViewModel($inputEntity->getBarcode());

    }

    /**
     * Map user's input to entity
     * @param string $clientId
     * @return GiftCardEntity
     */
    protected function getInputEntity($clientId)
    {

        // data received
        $inputEntity = clone $this->cardEntity;
        $inputEntity->setClientCode($clientId);
        $inputEntity->setBarcode($this->bodyParam('barcode'));
        $inputEntity->setInitialLoad($this->bodyParam('initial_load'));
        $inputEntity->setIsPhysical($this->bodyParam('is_physical'));
        $inputEntity->setStartDate($this->bodyParam('start_date'));
        $inputEntity->setCurrencyCode($this->bodyParam('currency_code'));
        $inputEntity->setChannelsVector($this->bodyParam('channels'));
        $inputEntity->setExternalId($this->bodyParam('external_id'));
        $inputEntity->setCustomerEmail($this->bodyParam('customer_email'));
        $inputEntity->setCustomerId($this->bodyParam('customer_id'));
        $inputEntity->setMarketingId($this->bodyParam('promo_code'));
        $inputEntity->setExpiryPeriodInMonth($this->bodyParam('expiry_period_in_month'));

        return $inputEntity;

    }

    /**
     * Payload miracle to return hal-json
     * @param string $barcode
     * @return ViewModel
     */
    protected function getViewModel($barcode)
    {

        return new ViewModel(array(
            'payload' => $this->getHalEntity($barcode),
        ));
    }

    /**
     * Mapping of internal variables into proper HAL entity
     * @param string $barcode
     * @return HalEntity
     */
    protected function getHalEntity($barcode)
    {

        // HAL wrapper
        $halEntity = new HalEntity(
            Array(
                'response' => $this->apiResponse->toArray(),
                'data' => $this->cardEntity->toArray()
            ),
            $barcode
        );

        return $halEntity;

    }
}
