<?php
namespace GiftCards\V1\Rpc\GiftCardAdd;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class GiftCardAddControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $giftCardMapper = $container->get('GiftCardMapper');
        $cardEntity = $container->get('GiftCardEntity');
        $apiResponse = $container->get('ApiResponse');
        $customValidation = $container->get('CustomValidation');

        return new GiftCardAddController($giftCardMapper,
            $cardEntity, $apiResponse, $customValidation);
    }
}
