# Copyright © 2020 by Hani Weiss 

All rights reserved. 

## The purpose of this publication is to demonstrate my PHP knowledge and coding skills.

No part of this publication may be reproduced, distributed, 
or transmitted in any form or by any means, including photocopying, 
recording, or other electronic or mechanical methods, without 
the prior written permission of the publisher, except in the case 
of brief quotations embodied in critical reviews and certain 
other noncommercial uses permitted by copyright law.
 
For permission requests, write to the publisher, 
addressed “Attention: Permissions Coordinator,” at the address below.

Mr. Hani Weiss
Email: weissh@hotmail.com
LICENSE.md
COPYRIGHT.md