Hani Weiss work/code snippets
==============================

Folder: 01_code_snippet
----------------------
This folder contains small part of an application to demonstrate my Zend framework3 work.
The application is a small micro service allow the customer To manage gift vouchers.


[Link to the code](https://gitlab.com/weissh/work_examples/-/tree/master/01_code_snippet/module/GiftCards/src/V1/Rpc).
- Controllers examples:
    - [Get voucher balance controller](https://gitlab.com/weissh/work_examples/-/blob/master/01_code_snippet/module/GiftCards/src/V1/Rpc/GiftCardGetBalance/GiftCardGetBalanceController.php)
    - [Add new voucher controller](https://gitlab.com/weissh/work_examples/-/blob/master/01_code_snippet/module/GiftCards/src/V1/Rpc/GiftCardAdd/GiftCardAddController.php)
- Mappers Examples:
    - [Voucher mapper (model)](https://gitlab.com/weissh/work_examples/-/blob/master/01_code_snippet/module/GiftCards/src/V1/Rpc/Mappers/GiftCardMapper.php)
    - [Abstract mapper class](https://gitlab.com/weissh/work_examples/-/blob/master/01_code_snippet/module/GiftCards/src/V1/Rpc/Mappers/AbstractMapper.php)
- Service Example:
    - [Voucher string generator](https://gitlab.com/weissh/work_examples/-/blob/master/01_code_snippet/module/GiftCards/src/V1/Rpc/Mappers/AbstractMapper.php)         
    - [Voucher string validator](https://gitlab.com/weissh/work_examples/-/blob/master/01_code_snippet/module/GiftCards/src/V1/Rpc/Services/CustomValidations/CustomValidation.php)
    - [Api exception service ](https://gitlab.com/weissh/work_examples/-/blob/master/01_code_snippet/module/GiftCards/src/V1/Rpc/Services/Exceptions/ApiException.php)
    
    
The application was built while ago but recently I was upgrading Zend library to the latest version (Laminas).
As well as moving the application from PHP 5.4 to PHP 7.4

Folder: 02_screenshots_voucher_project
--------------------------------------
This section contains screenshots from the above project.
- More code snippets all code is developed by me and it demonstrate working with: factories, [Entities](https://gitlab.com/weissh/work_examples/-/blob/master/02_screenshots_voucher_project/001_code.png), [dependency injection](https://gitlab.com/weissh/work_examples/-/blob/master/02_screenshots_voucher_project/002_code.png), [Mappers](https://gitlab.com/weissh/work_examples/-/blob/master/02_screenshots_voucher_project/003_code.png) and [Services](https://gitlab.com/weissh/work_examples/-/blob/master/02_screenshots_voucher_project/005_code.png)  
- Example of request/respond represented in Postman [Add voucher error response](https://gitlab.com/weissh/work_examples/-/blob/master/02_screenshots_voucher_project/006_postman.png), [Add voucher success response](https://gitlab.com/weissh/work_examples/-/blob/master/02_screenshots_voucher_project/007_postman.png), [Voucher balance response](https://gitlab.com/weissh/work_examples/-/blob/master/02_screenshots_voucher_project/008_postman.png) 

Folder: 03_vagrant_example
--------------------------
In this section I demonstrate my ability to work with vagrant/Virtual machines for development and production use

Folder: 04_capistrano_deployment_example
-----------------------------------
To demonstrate ability to create a deployment scripts to different environments (DEV, UAT, QA, PRODUCTION)

Folder: 05_business_opportunities_report
---------------------------------------
To demonstrate ability to create business analysis and identifying business opportunities

Folder: 06_project_specification_report
---------------------------------------
To demonstrate ability to create project specification based on clients requirements.

Folder: 07_Zend_Framwork_2_modules
----------------------------------
Team collaboration example. The models were created by my team and I about four years ago.
That example includes PHP unit tests and doctrine ORM and Connection (API module) to elastic search.


Folder: upload CSV
----------------------------------
 code example for working with CSV files.  this example show how to upload CSV files to dynamicly generated folders on the server then script will validate the content on the 
 CSV file line by line and last step it insert the data into database.